# Login
```
curl -H "Content-Type: application/json" -X POST -d '{"username":"admin", "password":"passw0rd"}' http://localhost:8080/api/v1/auth/login

```
# Kubernetes Resources
## Admin
### Namespaces (list, create, get)
```
curl http://localhost:8080/api/v1/kubernetes/namespaces
curl -H "Content-Type: application/json" -X POST -d '{"name":"ns-test001"}' http://localhost:8080/api/v1/kubernetes/namespaces
curl http://localhost:8080/api/v1/kubernetes/namespaces/ns-test001
```

### Nodes
```
curl http://localhost:8080/api/v1/kubernetes/nodes
curl http://localhost:8080/api/v1/kubernetes/nodes/ip-172-31-1-142
```

## Services and discovery
### Services
```
curl http://localhost:8080/api/v1/kubernetes/services
```