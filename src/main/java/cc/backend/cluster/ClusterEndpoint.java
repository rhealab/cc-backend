package cc.backend.cluster;

import cc.backend.EnnProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2018/2/5.
 */
@RestController
@Path("clusters")
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "Cluster", description = "Cluster info", produces = "application/json")
public class ClusterEndpoint {

    private EnnProperties properties;

    @Inject
    public ClusterEndpoint(EnnProperties properties) {
        this.properties = properties;
    }

    @GET
    @ApiOperation(value = "Get cluster name list.", responseContainer = "List", response = String.class)
    public Response getClusterList() {
        return Response.ok(properties.getClusters().keySet()).build();
    }

    @GET
    @Path("default")
    @ApiOperation(value = "Get default cluster name.", response = String.class)
    public Response getDefaultCluster() {
        return Response.ok(properties.getDefaultCluster()).build();
    }
}
