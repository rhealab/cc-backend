package cc.backend.cluster;

import cc.backend.EnnProperties;
import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2018/2/5.
 */
@Component
public class ClusterValidator {
    private final EnnProperties properties;

    @Inject
    public ClusterValidator(EnnProperties properties) {
        this.properties = properties;
    }

    public void validateClusterExists(String cluster) {
        if (!properties.getClusters().keySet().contains(cluster)) {
            throw new CcException(BackendReturnCodeNameConstants.CLUSTER_NOT_EXISTS, ImmutableMap.of("cluster", cluster));
        }
    }
}
