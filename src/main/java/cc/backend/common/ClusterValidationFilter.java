package cc.backend.common;

import cc.backend.audit.request.Constants;
import cc.backend.cluster.ClusterValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2018/2/5.
 */
//@Priority(Priorities.AUTHORIZATION - 10)
//@Provider
//@Named
public class ClusterValidationFilter implements ContainerRequestFilter {
    private static final Logger logger = LoggerFactory.getLogger(ClusterValidationFilter.class);

    private final ClusterValidator clusterValidator;

    @Context
    private ResourceInfo resourceInfo;

    @Inject
    public ClusterValidationFilter(ClusterValidator clusterValidator) {
        this.clusterValidator = clusterValidator;
    }

    @Override
    public void filter(ContainerRequestContext requestContext) {
        String cluster = requestContext.getHeaderString(Constants.CLUSTER_NAME);
        if (StringUtils.isEmpty(cluster)) {
            logger.info("cluster not specified, use default");
            return;
        }
        clusterValidator.validateClusterExists(cluster);
    }
}
