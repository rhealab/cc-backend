package cc.backend.common;

import cc.backend.EnnProperties;
import cc.backend.common.utils.OkHttpClientUtils;
import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.kubernetes.apps.data.App;
import cc.backend.kubernetes.apps.dto.DependencyEntity;
import cc.backend.kubernetes.apps.dto.DependencyWrapper;
import cc.lib.retcode.CcException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/29.
 */
@Component
public class DependencyCheckerClient {
    private static final Logger logger = LoggerFactory.getLogger(DependencyCheckerClient.class);

    @Inject
    private EnnProperties properties;

    public List<DependencyEntity> getDepApps(String namespaceName, App app) {
        String getDepAppsUrl = String.format("/apps/%s/namespaces/%s/instance/%s/prereq",
                app.getPredefineType(), namespaceName, app.getName());

        Request request = new Request.Builder()
                .url(properties.getCurrentCc().getDependencyChecker() + getDepAppsUrl)
                .get()
                .build();

        byte[] data = exec(request);

        ObjectMapper mapper = new ObjectMapper();
        DependencyWrapper wrapper;
        try {
            wrapper = mapper.readValue(data, DependencyWrapper.class);
        } catch (IOException e) {
            logger.error("error message : {}", e.getMessage());
            throw new CcException(BackendReturnCodeNameConstants.DEPENDENCY_WRAPPER_UNMARSHALING_ERROR);
        }
        return Optional.ofNullable(wrapper).map(DependencyWrapper::getEntityList).orElse(null);
    }

    public void deleteTemplateMeta(String namespace, List<String> instances) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            byte[] data = mapper.writeValueAsBytes(instances);

            String url = String.format("/apps/namespace/%s/instances", namespace);
            Request request = new Request.Builder()
                    .url(properties.getCurrentCc().getTemplate().getUrl() + url)
                    .delete(RequestBody.create(MediaType.parse("application/json"), data))
                    .build();

            exec(request);
        } catch (JsonProcessingException e) {
            logger.error("error message : {}", e.getMessage());
            throw new CcException(BackendReturnCodeNameConstants.MARSHALING_TO_JSON_ERROR);
        }
    }

    public void deleteDependencyMeta(String appType, String namespace, String instanceName, boolean cascade) {
        String url = String.format("/apps/%s/namespaces/%s/instance/%s/dependency?cascade=%s", appType, namespace, instanceName, cascade);
        Request request = new Request.Builder()
                .url(properties.getCurrentCc().getDependencyChecker() + url)
                .delete()
                .build();

        exec(request);
    }

    private byte[] exec(Request request) {
        try (Response response = OkHttpClientUtils.getUnsafeOkHttpClient().newCall(request).execute()) {
            if (response.isSuccessful()) {
                ResponseBody body = response.body();
                if (body != null) {
                    return body.bytes();
                } else {
                    return new byte[0];
                }
            } else {
                logger.error("http call: {} failed with message {}", request.toString(), response.toString());
                throw new CcException(BackendReturnCodeNameConstants.DEPENDENCY_CHECKER_SERVICE_ERROR);
            }
        } catch (IOException e) {
            logger.error("http call: {} failed with exception {}", request.toString(), e.getMessage());
            throw new CcException(BackendReturnCodeNameConstants.DEPENDENCY_CHECKER_SERVICE_ERROR);
        }
    }
}
