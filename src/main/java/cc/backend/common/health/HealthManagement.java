package cc.backend.common.health;

import cc.backend.common.DependencyCheckerClient;
import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import javax.inject.Named;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by mye(Xizhe Ye) on 18-3-1.
 */
@Named
public class HealthManagement {
  @Value("${spring.datasource.url}")
  private String url;

  @Value("${spring.datasource.username}")
  private String username;

  @Value("${spring.datasource.password}")
  private String password;

  private static final org.slf4j.Logger logger = LoggerFactory.getLogger(HealthManagement.class);


  public void healthCheck() {
    try {
      Class.forName("com.mysql.jdbc.Driver").newInstance();

      String sql = "select id from app limit 1";

      try(Connection conn = DriverManager.getConnection(url, username, password);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql)) {
      }
    } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e) {
      throw new CcException(BackendReturnCodeNameConstants.DATABASE_CANNOT_CONN, ImmutableMap.of("error", e.getMessage()));
    }
  }

}
