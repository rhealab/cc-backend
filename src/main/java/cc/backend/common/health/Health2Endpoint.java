package cc.backend.common.health;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;

/**
 * @author wangchunyang@gmail.com
 */

@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("health")
@Api(value = "Health", description = "cc-backend health check.", produces = "application/json")
public class Health2Endpoint {
    @Inject
    HealthManagement healthManagement;



    //TODO : to remove this
    @GET
    @ApiOperation(value = "Get cc-backend health status.", response = Health.class)
    public Health health() {
        Health health = new Health();
        health.setName("cc-backend");
        health.setVersion("1.0.0");
        health.setDeveloper("cc team");
        health.setMessage("Welcome to Container Management Console!");
        health.setStatus("OK");
        health.setTimestamp(new Date());
        return health;
    }

    @GET
    @Path("deep")
    @ApiOperation(value = "Get  health status.", response = Health.class)
    public Response deepHealthCheck() {
        healthManagement.healthCheck();
        return Response.ok().build();
    }

}
