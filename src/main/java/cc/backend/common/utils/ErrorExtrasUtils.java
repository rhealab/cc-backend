package cc.backend.common.utils;

import cc.backend.retcode.ReturnCodeResourceType;

import java.util.HashMap;
import java.util.Map;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2018/1/11.
 */
public class ErrorExtrasUtils {
    public static Map<String, Object> composeErrorExtras(String namespaceName,
                                                         String appName,
                                                         String resourceName,
                                                         ReturnCodeResourceType resourceType) {
        Map<String, Object> extras = new HashMap<>(6);
        extras.put("namespaceName", namespaceName);
        extras.put("appName", appName);
        extras.put(resourceType.getName(), resourceName);
        extras.put("resourceType", resourceType);
        return extras;
    }

    public static Map<String, Object> composeErrorExtras(String namespaceName,
                                                         String appName) {
        Map<String, Object> extras = composeErrorExtras(namespaceName);
        extras.put("appName", appName);
        return extras;
    }

    public static Map<String, Object> composeErrorExtras(String namespaceName) {
        Map<String, Object> extras = new HashMap<>(6);
        extras.put("namespaceName", namespaceName);
        return extras;
    }
}
