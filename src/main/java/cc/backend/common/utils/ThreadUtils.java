package cc.backend.common.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/4.
 */
public class ThreadUtils {
    public static ExecutorService cachedThreadPool;
    public static ExecutorService scheduledThreadPool;

    static {
        cachedThreadPool = Executors.newCachedThreadPool();
        scheduledThreadPool = Executors.newScheduledThreadPool(20);
    }

    public static ExecutorService getCachedThreadPool() {
        return cachedThreadPool;
    }

    public static ExecutorService getScheduledThreadPool() {
        return scheduledThreadPool;
    }
}
