package cc.backend.common.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/2/23.
 */
public class DateUtils {
    public final static String K8S_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    public static Date parseK8sDate(String k8sTimestampStr) {
        Date date = null;
        if (k8sTimestampStr != null) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat(K8S_DATE_FORMAT);
                date = sdf.parse(k8sTimestampStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return date;
    }

    public static long getAge(Date createdOn) {
        long age = 0;
        if (createdOn != null) {
            age = System.currentTimeMillis() - createdOn.getTime();
        }
        return age;
    }
}
