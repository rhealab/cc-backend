package cc.backend.common.utils;

import java.util.Random;

/**
 * @author wangchunyang@gmail.com
 */
public class Strings {
    private static final Random RANDOM = new Random();

    public static String shortLowerRandomString() {
        return shortRandomString(true);
    }

    public static String shortRandomString(boolean isLowerCase) {
        String randomString = randomString(8);

        if (isLowerCase) {
            return randomString.toLowerCase();
        }
        return randomString;
    }

    public static String randomString(int length) {
        if (length == 0) {
            return "";
        } else if (length < 0) {
            throw new IllegalArgumentException("Requested random string length " + length + " is less than 0.");
        }
        int end = 'z' + 1;
        int start = ' ';

        char[] buffer = new char[length];
        int gap = end - start;

        while (length-- != 0) {
            char ch = (char) (RANDOM.nextInt(gap) + start);
            if (Character.isLetter(ch)
                    || Character.isDigit(ch)) {
                if (ch >= 56320 && ch <= 57343) {
                    if (length == 0) {
                        length++;
                    } else {
                        // low surrogate, insert high surrogate after putting it in
                        buffer[length] = ch;
                        length--;
                        buffer[length] = (char) (55296 + RANDOM.nextInt(128));
                    }
                } else if (ch >= 55296 && ch <= 56191) {
                    if (length == 0) {
                        length++;
                    } else {
                        // high surrogate, insert low surrogate before putting it in
                        buffer[length] = (char) (56320 + RANDOM.nextInt(128));
                        length--;
                        buffer[length] = ch;
                    }
                } else if (ch >= 56192 && ch <= 56319) {
                    // private high surrogate, no effing clue, so skip it
                    length++;
                } else {
                    buffer[length] = ch;
                }
            } else {
                length++;
            }
        }
        return new String(buffer);
    }
}
