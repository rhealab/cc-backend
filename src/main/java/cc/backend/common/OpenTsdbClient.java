package cc.backend.common;

import cc.backend.EnnProperties;
import cc.backend.common.utils.OkHttpClientUtils;
import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.keystone.client.FreeMarkerTemplate;
import cc.lib.retcode.CcException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cc.backend.common.utils.OkHttpClientUtils.JSON;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/7.
 */
@Named
public class OpenTsdbClient {
    private static final Logger logger = LoggerFactory.getLogger(OpenTsdbClient.class);

    @Inject
    private EnnProperties properties;

    @Inject
    private FreeMarkerTemplate freeMarkerTemplate;

    public List<Metrics> getNsCpuMetrics(String namespaceName) {
        return getNamespaceMetrics(MetricsType.CPU, namespaceName);
    }

    public List<Metrics> getNsMemoryMetrics(String namespaceName) {
        return getNamespaceMetrics(MetricsType.MEMORY, namespaceName);
    }

    public List<Metrics> getClusterCpuMetrics(String opentsdbClusterName) {
        return getClusterMetrics(MetricsType.CPU, opentsdbClusterName);
    }

    public List<Metrics> getClusterMemoryMetrics(String opentsdbClusterName) {
        return getClusterMetrics(MetricsType.MEMORY, opentsdbClusterName);
    }

    public List<Metrics> getNodeCpuMetrics(String nodeName) {
        return getNodeMetrics(MetricsType.CPU, nodeName);
    }

    public List<Metrics> getNodeMemoryMetrics(String nodeName) {
        return getNodeMetrics(MetricsType.MEMORY, nodeName);
    }

    public enum Scope {
        NS("namespace"),
        Node("host"),
        Cluster("cluster_name");

        String realName;

        Scope(String name) {
            this.realName = name;
        }

        public String getRealName() {
            return realName;
        }
    }

    /**
     * @param metricsType   wanted metrics's type
     * @param namespaceName the k8s namespace
     * @return list of Metrics or null if error happened
     */
    public List<Metrics> getNamespaceMetrics(MetricsType metricsType, String namespaceName) {
        return getMetrics(metricsType, Scope.NS, namespaceName);
    }


    /**
     * @param metricsType wanted metrics's type
     * @param nodeName    the k8s nodeName example: 10.19.137.140
     * @return list of Metrics or null if error happened
     */
    public List<Metrics> getNodeMetrics(MetricsType metricsType, String nodeName) {
        return getMetrics(metricsType, Scope.Node, nodeName);
    }

    public List<Metrics> getNodeMetrics(MetricsType metricsType, Scope scope, String scopeName) {
        int delaySeconds = properties.getCurrentOpentsdb().getDelaySeconds();
        Map<String, Object> map = new HashMap<>(5);
        map.put("start", 30 + delaySeconds);
        map.put("end", delaySeconds);
        map.put("metric", metricsType.getRealName());
        if (metricsType == MetricsType.CPU) {
            map.put("rate", true);
        } else {
            map.put("rate", false);
        }
        map.put(scope.getRealName(), scopeName);
        return queryNode(map);
    }

    /**
     * @param metricsType         wanted metrics's type
     * @param opentsdbClusterName the opentsdb monitored k8s cluster name example: shanghai
     * @return list of Metrics or null if error happened
     */
    public List<Metrics> getClusterMetrics(MetricsType metricsType, String opentsdbClusterName) {
        return getMetrics(metricsType, Scope.Cluster, opentsdbClusterName);
    }

    private List<Metrics> getMetrics(MetricsType metricsType, Scope scope, String scopeName) {
        int delaySeconds = properties.getCurrentOpentsdb().getDelaySeconds();
        long millis = System.currentTimeMillis();
        Map<String, Object> map = new HashMap<>(5);
        map.put("start", millis - (delaySeconds + 10) * 1000);
        map.put("end", millis - delaySeconds * 1000);
        map.put("metric", metricsType.getRealName());
        if (metricsType == MetricsType.CPU) {
            map.put("rate", true);
        } else {
            map.put("rate", false);
        }
        map.put(scope.getRealName(), scopeName);
        if (scope == Scope.Cluster || scope == Scope.Node) {
            map.put("container_name", "/");
        }
        return query(map);
    }

    private List<Metrics> queryNode(Map<String, Object> queryParamsMap) {
        String url = properties.getCurrentOpentsdb().getUrl() + "/query";
        String json = freeMarkerTemplate.render("opentsdb_query_node.json", queryParamsMap);
        return request(url, json);
    }

    private List<Metrics> query(Map<String, Object> queryParamsMap) {
        String url = properties.getCurrentOpentsdb().getUrl() + "/query";
        String json = freeMarkerTemplate.render("opentsdb_query_request.json", queryParamsMap);
        return request(url, json);
    }

    private List<Metrics> request(String url, String json) {
        OkHttpClient okHttpClient = OkHttpClientUtils.getUnsafeOkHttpClient();

        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        Response response = null;
        try {
            response = okHttpClient.newCall(request).execute();

            ResponseBody responseBody = response.body();
            if (response.isSuccessful() && responseBody != null) {
                ObjectMapper mapper = new ObjectMapper();
                mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                List<Metrics> metricsList = mapper.readValue(responseBody.bytes(), new TypeReference<List<Metrics>>() {
                });
                responseBody.close();
                return metricsList;
            }
            logger.error(String.format("Failed to get metrics, msg: %s", response.message()));
        } catch (IOException e) {
            throw new CcException(BackendReturnCodeNameConstants.OPENTSDB_SERVER_ERROR,
                    ImmutableMap.of("msg", e.getMessage()));
        } finally {
            if (response != null && response.body() != null) {
                response.body().close();
            }
        }

        return null;
    }
}