package cc.backend.common;

import java.util.List;
import java.util.Map;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/7.
 */
public class Metrics {
    private String metric;
    private Map<String, String> tags;
    private List<String> aggregateTags;
    private Map<String, Double> dps;

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public Map<String, String> getTags() {
        return tags;
    }

    public void setTags(Map<String, String> tags) {
        this.tags = tags;
    }

    public List<String> getAggregateTags() {
        return aggregateTags;
    }

    public void setAggregateTags(List<String> aggregateTags) {
        this.aggregateTags = aggregateTags;
    }

    public Map<String, Double> getDps() {
        return dps;
    }

    public void setDps(Map<String, Double> dps) {
        this.dps = dps;
    }

    @Override
    public String toString() {
        return "Metrics{" +
                "metric='" + metric + '\'' +
                ", tags=" + tags +
                ", aggregateTags=" + aggregateTags +
                ", dps=" + dps +
                '}';
    }
}
