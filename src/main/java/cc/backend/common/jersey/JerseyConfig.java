package cc.backend.common.jersey;

import cc.lib.retcode.EnableReturnCode;
import cc.lib.security.EnableSecurity;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Path;
import javax.ws.rs.ext.Provider;
import java.lang.annotation.Annotation;

/**
 * https://github.com/spring-projects/spring-boot/issues/1345
 *
 * @author wangchunyang@gmail.com
 */
@Component
@ApplicationPath("api/v1")
@EnableReturnCode
@EnableSecurity
public class JerseyConfig extends ResourceConfig {
    private static final Logger logger = LoggerFactory.getLogger(JerseyConfig.class);

    @Autowired
    private ApplicationContext context;

    public JerseyConfig() {
        property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);

        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setTitle("CC Backend API");
        beanConfig.setDescription("CC Backend REST APIs.</br>" +
                "  All errors will return a code and detail info." +
                "  See definitions at" +
                "  [cc_arch](https://docs.google.com/presentation/d/1FGCAFaxxY4Gq9huQuo8QJHS8GsxgyN-A0LfrnyUS49o/edit#slide=id.gc6f73a04f_0_0)");
        beanConfig.setVersion("v1");
        beanConfig.setHost("10.19.137.140:32210");
        beanConfig.setSchemes(new String[]{"http"});
        beanConfig.setBasePath("/gw/be/api/v1");
        beanConfig.setResourcePackage("cc.backend");
        beanConfig.setContact("NormanWang06@gmail.com (wangjinxin)");
        beanConfig.setScan();
        register(ApiListingResource.class);
        register(SwaggerSerializers.class);
    }

    @PostConstruct
    public void setup() {
        scan(Path.class);
        scan(Provider.class);
    }

    private void scan(Class<? extends Annotation> annotationType) {
        context.getBeansWithAnnotation(annotationType).forEach((name, bean) -> {
            logger.info("Register JAX-RS component -> {}", bean.getClass().getName());
            register(bean);
        });
    }
}
