package cc.backend.common.jersey;

import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.lib.retcode.ReturnCodeResponseBuilder;
import cc.lib.retcode.mapper.ReturnCodeMapper;
import io.fabric8.kubernetes.client.KubernetesClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.ws.rs.Priorities;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;

/**
 * By default, when the {@code @Priority} annotation is absent on a component,
 * for which a priority should be applied, the {@link Priorities#USER} priority value is used.
 *
 * @author wangchunyang@gmail.com
 */
@Component
@ReturnCodeMapper
public class KubernetesExceptionMapper implements ExceptionMapper<KubernetesClientException> {
    private static final Logger logger = LoggerFactory.getLogger(KubernetesExceptionMapper.class);

    @Override
    public Response toResponse(KubernetesClientException e) {

        int statusCode = e.getCode();
        if (statusCode == Response.Status.BAD_REQUEST.getStatusCode()) {
            return new ReturnCodeResponseBuilder()
                    .codeName(BackendReturnCodeNameConstants.PROPERTY_INVALID)
                    .details(e.getMessage())
                    .build();
        }

        if (statusCode == Response.Status.NOT_FOUND.getStatusCode()) {
            return new ReturnCodeResponseBuilder()
                    .codeName(BackendReturnCodeNameConstants.KUBERNETES_RESOURCE_NOT_EXISTS)
                    .details(e.getMessage())
                    .build();
        }

        if (statusCode == Response.Status.CONFLICT.getStatusCode()) {
            return new ReturnCodeResponseBuilder()
                    .codeName(BackendReturnCodeNameConstants.KUBERNETES_RESOURCE_ALREADY_EXISTS)
                    .details(e.getMessage())
                    .build();
        }

        if (statusCode == 422) {
            return new ReturnCodeResponseBuilder()
                    .codeName(BackendReturnCodeNameConstants.KUBERNETES_RESOURCE_FIELD_ERROR)
                    .details(e.getMessage())
                    .build();
        }

        if (Response.Status.fromStatusCode(statusCode) == null) {
            // Note: some of http code was not maintained in Jersey Response.Status, such as 422.
            // In this case, let's set status with SERVER_ERROR
            statusCode = INTERNAL_SERVER_ERROR.getStatusCode();
        }
        return new ReturnCodeResponseBuilder()
                .codeName(BackendReturnCodeNameConstants.KUBERNETES_SERVER_ERROR)
                .httpStatusCode(statusCode)
                .details(e.getMessage())
                .build();
    }
}

