package cc.backend.common.jersey;

import cc.lib.retcode.config.ReturnCodeFilePathConfigurer;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/8/1.
 */
@Component
public class BackendReturnCodeFilePathConfig implements ReturnCodeFilePathConfigurer {
    @Override
    public void addFilePath(List<String> list) {
        list.add("code/backend.json");
        list.add("code/keystone.json");
    }
}
