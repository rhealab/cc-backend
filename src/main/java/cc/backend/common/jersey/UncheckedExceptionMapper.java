package cc.backend.common.jersey;

import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.lib.retcode.CcException;
import cc.lib.retcode.ReturnCodeResponseBuilder;
import cc.lib.retcode.mapper.ReturnCodeMapper;
import com.google.common.util.concurrent.UncheckedExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.ws.rs.Priorities;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * By default, when the {@code @Priority} annotation is absent on a component,
 * for which a priority should be applied, the {@link Priorities#USER} priority value is used.
 *
 * @author wangchunyang@gmail.com
 */
@Component
@ReturnCodeMapper
public class UncheckedExceptionMapper implements ExceptionMapper<UncheckedExecutionException> {
    private static final Logger logger = LoggerFactory.getLogger(UncheckedExceptionMapper.class);

    @Override
    public Response toResponse(UncheckedExecutionException e) {
        //TODO better way to handle this exception
        CcException exception = null;
        Throwable cause = e.getCause();
        if (cause instanceof CcException) {
            exception = (CcException) cause;
        }

        Throwable causeCause = cause.getCause();
        if (causeCause instanceof CcException) {
            exception = (CcException) causeCause;
        }
        if (exception != null) {
            return new ReturnCodeResponseBuilder()
                    .codeName(exception.getError())
                    .payload(exception.getPayload())
                    .build();
        }

        return new ReturnCodeResponseBuilder()
                .codeName(BackendReturnCodeNameConstants.SERVER_ERROR)
                .details(e.getMessage())
                .build();
    }
}

