package cc.backend.common.jersey;

import cc.lib.retcode.ReturnCode;
import com.google.common.collect.ImmutableMap;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.jaxrs.Reader;
import io.swagger.jaxrs.config.ReaderListener;
import io.swagger.models.Operation;
import io.swagger.models.Path;
import io.swagger.models.Response;
import io.swagger.models.Swagger;
import io.swagger.models.parameters.HeaderParameter;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/8/14.
 */
@Component
@SwaggerDefinition
public class SwaggerFilter implements ReaderListener {
    @Override
    public void beforeScan(Reader reader, Swagger swagger) {
    }

    @Override
    public void afterScan(Reader reader, Swagger swagger) {
        Map<String, Path> paths = swagger.getPaths();
        if (paths == null) {
            return;
        }

        Collection<Path> values = paths.values();

        HeaderParameter authorization = new HeaderParameter();
        authorization.setName("Authorization");
        authorization.setRequired(true);
        authorization.setType("string");

        Response reqErrRes = new Response();
        reqErrRes.setDescription("request error");
        ReturnCode returnCode = new ReturnCode();
        returnCode.setMessage("string");
        returnCode.setName("string");
        reqErrRes.setExamples(ImmutableMap.of("", returnCode));


        Response serverErrRes = new Response();
        serverErrRes.setDescription("server error");
        serverErrRes.setExamples(ImmutableMap.of("", returnCode));

        for (Path path : values) {
            List<Operation> operations = path.getOperations();

            if (operations == null) {
                continue;
            }

            for (Operation operation : operations) {
                operation.addParameter(authorization);

                operation.addResponse("4xx", reqErrRes);
                operation.addResponse("5xx", serverErrRes);
            }
        }
    }
}
