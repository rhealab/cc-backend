package cc.backend.common;

import cc.backend.kubernetes.namespaces.validator.NamespaceValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import java.util.List;

/**
 * The filter will try extract the namespace name from URL path.
 * If found, it will validate the namespace status and whether it exists or not.
 *
 * @author wangchunyang@gmail.com
 */
@Priority(Priorities.AUTHORIZATION - 10)
@Provider
@Named
public class NamespaceStatusValidationFilter implements ContainerRequestFilter {
    private static final Logger logger = LoggerFactory.getLogger(NamespaceStatusValidationFilter.class);
    @Context
    private ResourceInfo resourceInfo;

    @Inject
    private NamespaceValidator namespaceValidator;

    @Override
    public void filter(ContainerRequestContext requestContext) {
        MultivaluedMap<String, String> pathParameters = requestContext.getUriInfo().getPathParameters();
        List<String> namespaceNames = pathParameters.get("namespaceName");
        if (CollectionUtils.isEmpty(namespaceNames)) {
            return;
        }

        NamespaceStatusNotCheck annotation = resourceInfo.getResourceMethod().getAnnotation(NamespaceStatusNotCheck.class);
        if (annotation != null) {
            return;
        }
        annotation = resourceInfo.getResourceClass().getAnnotation(NamespaceStatusNotCheck.class);
        if (annotation != null) {
            return;
        }

        // The namespace is found in request URL.
        String namespaceName = namespaceNames.get(0);
        namespaceValidator.validateNamespaceStatus(namespaceName);
    }
}
