package cc.backend.common;

import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.StringWriter;

/**
 * @author wangchunyang@gmail.com
 */
//@Component
public class FreeMarkerTemplate {
    private Configuration cfg;

    @PostConstruct
    public void init() {
        cfg = new Configuration(Configuration.VERSION_2_3_25);
        cfg.setClassLoaderForTemplateLoading(this.getClass().getClassLoader(), "freemarker");
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
    }

    public String render(String templateFile, Object model) {
        try {
            Template template = cfg.getTemplate(templateFile);
            StringWriter writer = new StringWriter();
            template.process(model, writer);
            String str = writer.toString();
            writer.close();
            return str;
        } catch (IOException | TemplateException e) {
            throw new CcException(BackendReturnCodeNameConstants.RENDER_FREEMARKER_TEMPLATE_ERROR,
                    ImmutableMap.of("templateFile", templateFile,
                            "msg", e.getMessage()));
        }
    }
}