package cc.backend.common;

import com.google.gson.Gson;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/26.
 */
public class GsonFactory {
    private static Gson gson = new Gson();

    public static Gson getGson() {
        return gson;
    }
}
