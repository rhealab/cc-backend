package cc.backend.common;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/7.
 */
public enum MetricsType {
    CPU("cpu_total"),
    MEMORY("memory_working_set"),
    MEMORY_WORKING_SET("memory_working_set"),
    DISK_CAPACITY("disk_capacity"),
    DISK_USAGE("disk_usage"),
    CEPH_RBD(""),
    HOSTPATH(""),
    CEPH_FS(""),
    CEPH_POOL(""),
    NFS(""),
    EBS(""),
    EFS("");

    String realName;

    MetricsType(String name) {
        this.realName = name;
    }

    public String getRealName() {
        return realName;
    }
}
