package cc.backend.datasource.hibernate;

import cc.backend.BackendApplication;
import cc.backend.EnnProperties;
import cc.backend.datasource.hibernate.schema.update.MultiCreateSchemaUpdate;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.boot.spi.MetadataImplementor;
import org.hibernate.cfg.Environment;
import org.hibernate.engine.jdbc.connections.spi.AbstractDataSourceBasedMultiTenantConnectionProviderImpl;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.Entity;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/8.
 */
public class DataSourceBasedMultiTenantConnectionProviderImpl extends AbstractDataSourceBasedMultiTenantConnectionProviderImpl
        implements ApplicationListener<ContextRefreshedEvent> {

    private Map<String, DataSource> map;

    @Autowired
    private EnnProperties properties;

    public DataSourceBasedMultiTenantConnectionProviderImpl(Map<String, DataSource> map) {
        super();
        this.map = map;
    }

    public Map<String, DataSource> getDataSourceMap() {
        return map;
    }

    @Override
    protected DataSource selectAnyDataSource() {
        return map.get(properties.getDefaultHibernate());
    }

    @Override
    protected DataSource selectDataSource(String tenantIdentifier) {
        return map.get(tenantIdentifier);
    }

    public DataSource getDefaultDataSource() {
        return map.get(properties.getDefaultHibernate());
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        init();
    }

    private void init() {

        Reflections reflections = new Reflections(BackendApplication.class.getPackage().getName());

        Set<Class<?>> entities = reflections.getTypesAnnotatedWith(Entity.class);
        Set<Class<? extends JpaRepository>> repositories = reflections.getSubTypesOf(JpaRepository.class);

        properties.getHibernate().forEach((key, value) -> {
            Map<String, String> map = new HashMap<>(7);
            map.put(Environment.HBM2DDL_AUTO, value.getDdlAuto());
            map.put(Environment.DIALECT, value.getDialect());
            map.put(Environment.DRIVER, value.getDriverClassName());
            map.put(Environment.URL, value.getUrl());
            map.put(Environment.USER, value.getUsername());
            map.put(Environment.PASS, value.getPassword());
            map.put(Environment.SHOW_SQL, String.valueOf(value.isShowSql()));
            map.put(Environment.IMPLICIT_NAMING_STRATEGY, "org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy");
            map.put(Environment.PHYSICAL_NAMING_STRATEGY, "org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy");
            StandardServiceRegistry ssr = new StandardServiceRegistryBuilder()
                    .applySettings(map)
                    .build();

            MetadataSources metaDataSource = new MetadataSources(ssr);

            for (Class entity : entities) {
                metaDataSource.addAnnotatedClass(entity);
            }

            for (Class repository : repositories) {
                metaDataSource.addAnnotatedClass(repository);
            }

            final MetadataImplementor metadata = (MetadataImplementor) metaDataSource.getMetadataBuilder()
                    .applyImplicitNamingStrategy(SpringImplicitNamingStrategy.INSTANCE)
                    .applyPhysicalNamingStrategy(new SpringPhysicalNamingStrategy())
                    .build();

            metadata.validate();
            MultiCreateSchemaUpdate su = new MultiCreateSchemaUpdate(ssr, metadata);
            su.setHaltOnError(true);
            su.setDelimiter(";");
            su.setFormat(true);
            su.execute(true, true);
            StandardServiceRegistryBuilder.destroy(ssr);
        });
    }
}