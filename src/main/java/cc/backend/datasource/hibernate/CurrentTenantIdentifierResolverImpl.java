package cc.backend.datasource.hibernate;

import cc.backend.EnnProperties;
import cc.backend.audit.request.EnnContext;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/8.
 */
public class CurrentTenantIdentifierResolverImpl implements CurrentTenantIdentifierResolver {

    @Autowired
    private EnnProperties properties;

    @Override
    public String resolveCurrentTenantIdentifier() {
        return EnnContext.getClusterName(properties.getDefaultCluster());
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return false;
    }
}
