package cc.backend.audit.request;

import org.apache.commons.lang3.StringUtils;

/**
 * @author wangchunyang@gmail.com
 */
public class ClusterExtractor {
    public static String extract(String requestURL) {
        String clusterName = StringUtils.substringBetween(requestURL, "clusters/", "/");
        if (StringUtils.isBlank(clusterName)) {
            clusterName = StringUtils.substringAfter(requestURL, "clusters/");
        }
        return StringUtils.isBlank(clusterName) ? null : clusterName;
    }
}
