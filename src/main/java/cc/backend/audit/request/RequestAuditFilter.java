package cc.backend.audit.request;

import cc.backend.RabbitConfig;
import com.google.common.base.Stopwatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 * @author wangchunyang@gmail.com
 */
@Priority(Priorities.AUTHENTICATION - 10)
@Named
@Provider
public class RequestAuditFilter implements ContainerRequestFilter, ContainerResponseFilter {
    private static final Logger logger = LoggerFactory.getLogger(RequestAuditFilter.class);
    private static final String STOPWATCH = "X_STOPWATCH";

    private final RabbitMessagingTemplate messagingTemplate;

    @Context
    private HttpServletRequest servletRequest;

    @Inject
    public RequestAuditFilter(RabbitMessagingTemplate messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    @Override
    public void filter(ContainerRequestContext requestContext) {
        requestContext.setProperty(STOPWATCH, Stopwatch.createStarted());
        EnnContext.setRequestId(requestContext.getHeaderString(Constants.REQ_ID));
        EnnContext.setUserId(requestContext.getHeaderString(Constants.USER_ID));
        EnnContext.setNamespaceName(NamespaceExtractor.extract(requestContext.getUriInfo().getPath()));
        EnnContext.setClusterName(requestContext.getHeaderString(Constants.CLUSTER_NAME));
        EnnContext.setClientType(requestContext.getHeaderString(Constants.CLIENT_TYPE));
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
        logger.debug("Entry");
        RequestAuditMessage.Builder builder = new RequestAuditMessage.Builder();
        Stopwatch stopwatch = (Stopwatch) requestContext.getProperty(STOPWATCH);
        RequestAuditMessage message = builder.service(RequestAuditMessage.ServiceType.BACKEND)
                .elapsed(elapsed(stopwatch))
                .url(requestContext.getUriInfo().getAbsolutePath().toString())
                .clusterName(EnnContext.getClusterName())
                .namespaceName(EnnContext.getNamespaceName())
                .requestId(EnnContext.getRequestId())
                .userId(EnnContext.getUserId())
                .clientType(EnnContext.getClientType())
                .httpMethod(requestContext.getRequest().getMethod())
                .httpStatus(responseContext.getStatus())
                .clientIp(servletRequest.getRemoteAddr())
                .build();
        CompletableFuture.runAsync(() -> {
            logger.info("RequestAuditMessage={}", message);
            messagingTemplate.convertAndSend(RabbitConfig.REQUEST_AUDIT_Q, message);
        });
    }

    private Long elapsed(Stopwatch stopwatch) {
        if (stopwatch == null) {
            return null;
        }
        return stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
    }
}
