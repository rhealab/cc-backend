package cc.backend.audit.request;

import cc.backend.kubernetes.namespaces.messages.IMessage;
import org.apache.commons.lang3.StringUtils;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author wangchunyang@gmail.com
 */
public class EnnContext {
    private static ThreadLocal<ConcurrentHashMap<String, String>> threadLocal = ThreadLocal.withInitial(ConcurrentHashMap::new);

    public static void setRequestId(String requestId) {
        if (StringUtils.isNotBlank(requestId)) {
            threadLocal.get().put("requestId", requestId);
        }
    }

    public static String getRequestId() {
        return threadLocal.get().getOrDefault("requestId", "");
    }

    public static void setUserId(String userId) {
        if (StringUtils.isNotBlank(userId)) {
            threadLocal.get().put("userId", userId);
        }
    }

    public static String getUserId() {
        return threadLocal.get().getOrDefault("userId", "");
    }

    public static void setClientType(String clientType) {
        if (!org.springframework.util.StringUtils.isEmpty(clientType)) {
            threadLocal.get().put("clientType", clientType);
        }
    }

    public static String getClientType() {
        return threadLocal.get().getOrDefault("clientType", Constants.CLIENT_TYPE_OTHERS);
    }

    public static void setNamespaceName(String namespaceName) {
        if (StringUtils.isNotBlank(namespaceName)) {
            threadLocal.get().put("namespaceName", namespaceName);
        }
    }

    public static String getNamespaceName() {
        return threadLocal.get().get("namespaceName");
    }

    // TODO
    public static void setClusterName(String clusterName) {
//        if (StringUtils.isNotBlank(clusterName)) {
//            threadLocal.get().put("clusterName", clusterName);
//        }
    }

    public static String getClusterName() {
//        return threadLocal.get().get("clusterName");
        return "sh";
    }

    public static String getClusterName(String defaultCluster) {
//        return threadLocal.get().getOrDefault("clusterName", defaultCluster);
        return defaultCluster;
    }

    public static void setContext(IMessage message) {
        setUserId(message.getUserId());
        setRequestId(message.getRequestId());
        setNamespaceName(message.getNamespaceName());
        setClusterName(message.getClusterName());
    }
}
