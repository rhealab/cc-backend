package cc.backend.audit.request;

import cc.backend.common.GsonFactory;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author wangchunyang@gmail.com
 */
public class RequestAuditMessage {
    private ServiceType service;
    private String requestId;
    private String userId;
    private String clientType;
    private String clusterName;
    private String namespaceName;
    private String url;
    private String httpMethod;
    private int httpStatus;
    private String clientIp;
    private Date startTime;
    private long elapsed;
    private String extras;

    public ServiceType getService() {
        return service;
    }

    public void setService(ServiceType service) {
        this.service = service;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getElapsed() {
        return elapsed;
    }

    public void setElapsed(long elapsed) {
        this.elapsed = elapsed;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(int httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    @Override
    public String toString() {
        return "RequestAuditMessage{" +
                "service=" + service +
                ", requestId='" + requestId + '\'' +
                ", userId='" + userId + '\'' +
                ", clientType='" + clientType + '\'' +
                ", clusterName='" + clusterName + '\'' +
                ", namespaceName='" + namespaceName + '\'' +
                ", url='" + url + '\'' +
                ", httpMethod='" + httpMethod + '\'' +
                ", httpStatus=" + httpStatus +
                ", clientIp='" + clientIp + '\'' +
                ", startTime=" + startTime +
                ", elapsed=" + elapsed +
                ", extras='" + extras + '\'' +
                '}';
    }

    public enum ServiceType {
        API_SERVER, BACKEND, GATEWAY
    }

    public static class Builder {
        private ServiceType service;
        private String requestId;
        private String userId;
        private String clientType;
        private String clusterName;
        private String namespaceName;
        private String url;
        private String httpMethod;
        private int httpStatus;
        private String clientIp;
        private Date startTime;
        private long elapsed;
        private Map<String, Object> extras = new HashMap<>();

        public Builder service(ServiceType service) {
            this.service = service;
            return this;
        }

        public Builder requestId(String requestId) {
            this.requestId = requestId;
            return this;
        }

        public Builder userId(String userId) {
            this.userId = userId;
            return this;
        }

        public Builder clientType(String clientType) {
            this.clientType = clientType;
            return this;
        }

        public Builder clusterName(String clusterName) {
            this.clusterName = clusterName;
            return this;
        }

        public Builder namespaceName(String namespaceName) {
            this.namespaceName = namespaceName;
            return this;
        }

        public Builder url(String url) {
            this.url = url;
            return this;
        }

        public Builder httpMethod(String httpMethod) {
            this.httpMethod = httpMethod;
            return this;
        }

        public Builder httpStatus(int httpStatus) {
            this.httpStatus = httpStatus;
            return this;
        }

        public Builder clientIp(String clientIp) {
            this.clientIp = clientIp;
            return this;
        }

        public Builder startTime(Date startTime) {
            this.startTime = startTime;
            return this;
        }

        public Builder elapsed(Long elapsed) {
            if (elapsed != null && elapsed > 0) {
                this.elapsed = elapsed;
                this.startTime = Date.from(Instant.now().minus(elapsed, ChronoUnit.MILLIS));
            }
            return this;
        }

        public Builder extra(String key, Object value) {
            if (key != null && value != null) {
                this.extras.put(key, value);
            }
            return this;
        }

        public RequestAuditMessage build() {
            RequestAuditMessage m = new RequestAuditMessage();
            m.setService(service);
            m.setRequestId(requestId);
            m.setUserId(userId);
            m.setClientType(clientType);
            m.setClusterName(clusterName);
            m.setNamespaceName(namespaceName);
            m.setUrl(url);
            m.setHttpMethod(httpMethod);
            m.setHttpStatus(httpStatus);
            m.setClientIp(clientIp);
            m.setStartTime(startTime);
            m.setElapsed(elapsed);
            m.setExtras(GsonFactory.getGson().toJson(extras));
            return m;
        }
    }
}
