package cc.backend.audit.request;

/**
 * @author wangchunyang@gmail.com
 */
public interface Constants {
    String REQ_ID = "x-gw-req-id";
    String USER_ID = "x-gw-user-id";
    String CLUSTER_NAME = "x-gw-cluster-name";
    String CLIENT_TYPE = "x-client-type";
    String CLIENT_TYPE_OTHERS = "others";
}
