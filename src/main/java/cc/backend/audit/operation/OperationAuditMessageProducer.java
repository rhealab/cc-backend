package cc.backend.audit.operation;

import cc.backend.RabbitConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

/**
 * @author wangchunyang@gmail.com
 */
@Component
public class OperationAuditMessageProducer {
    private static final Logger logger = LoggerFactory.getLogger(OperationAuditMessageProducer.class);

    @Inject
    private RabbitMessagingTemplate messagingTemplate;

    public void send(OperationAuditMessage message) {
        CompletableFuture.runAsync(() -> {
            logger.info("OperationAuditMessage={}", message);
            messagingTemplate.convertAndSend(RabbitConfig.OPERATION_AUDIT_Q, message);
        });
    }
}
