package cc.backend.audit.operation;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/3/30.
 */
public enum OperationType {
    CREATE,
    DELETE,
    UPDATE,
    LOGIN,
    LOGOUT
}
