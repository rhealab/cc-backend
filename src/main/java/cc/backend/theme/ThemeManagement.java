package cc.backend.theme;

import cc.backend.audit.request.EnnContext;
import cc.backend.theme.data.SysTheme;
import cc.backend.theme.data.SysThemeRepository;
import cc.backend.theme.data.Theme;
import cc.backend.theme.data.ThemeDto;
import cc.backend.theme.data.ThemeRepository;
import com.google.common.collect.Lists;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.Optional;

/**
 * Created by mye(Xizhe Ye) on 18-3-20.
 */
@Named
public class ThemeManagement {
  @Inject
  private ThemeRepository themeRepository;

  @Inject
  private SysThemeRepository sysThemeRepository;

  public Theme setTheme(ThemeDto themeDto) {
    return themeRepository.save(Theme.from(themeDto));
  }

  public ThemeDto getTheme() {
    String userId = EnnContext.getUserId();
    Theme theme = themeRepository.findByUserId(userId);
    if (theme != null) {
      return ThemeDto.from(theme);
    } else {
      List<SysTheme> sysThemeList = sysThemeRepository.findAll();
      return ThemeDto.fromSys(sysThemeList != null && sysThemeList.size() > 0 ? sysThemeList.get(0) : null);
    }
  }

  @Transactional
  public SysTheme setSysTheme(SysTheme sysTheme) {
    if (sysThemeRepository.findAll().size() > 0) {
      sysThemeRepository.updateSysThemeById(sysTheme.getTheme(), sysTheme.isDark(), 0L);
      return sysTheme;
    }
    return sysThemeRepository.save(sysTheme);
  }

  public ThemeDto getSysTheme() {
    SysTheme sysTheme = sysThemeRepository.findAll()
        .stream()
        .findFirst()
        .orElse(null);

    return ThemeDto.fromSys(sysTheme);
  }

  public void deleteSysTheme() {
    sysThemeRepository.deleteAll();
  }

}
