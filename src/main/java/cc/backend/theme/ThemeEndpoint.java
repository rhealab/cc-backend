package cc.backend.theme;

import cc.backend.sys.component.ComponentDto;
import cc.backend.theme.data.SysTheme;
import cc.backend.theme.data.Theme;
import cc.backend.theme.data.ThemeDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by mye(Xizhe Ye) on 18-3-20.
 */

@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("theme")
@Api(value = "Theme", description = "set UI theme",
    produces = "application/json")
public class ThemeEndpoint {
  @Inject
  private ThemeManagement themeManagement;

  @GET
  @ApiOperation(value = "Get user theme", response = ThemeDto.class)
  public Response getTheme() {
    return Response.ok(themeManagement.getTheme()).build();
  }

  @POST
  @ApiOperation(value = "Set user theme", response = ThemeDto.class)
  public Response setTheme(@Valid ThemeDto themeDto) {
    Theme theme = themeManagement.setTheme(themeDto);
    return Response.ok(ThemeDto.from(theme)).build();
  }


  @GET
  @Path("sys")
  @ApiOperation(value = "Get system theme", response = ThemeDto.class)
  public Response getSysTheme() {
    return Response.ok(themeManagement.getSysTheme()).build();
  }

  @POST
  @Path("sys")
  @ApiOperation(value = "Set sys theme", response = SysTheme.class)
  public Response setSysTheme(@Valid SysTheme sysTheme) {
    SysTheme retTheme = themeManagement.setSysTheme(sysTheme);
    return Response.ok(retTheme).build();
  }

  @DELETE
  @Path("sys")
  @ApiOperation(value = "delete sys theme", response = SysTheme.class)
  public Response deleteSysTheme() {
    themeManagement.deleteSysTheme();
    return Response.ok().build();
  }

}
