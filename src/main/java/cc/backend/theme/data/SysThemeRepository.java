package cc.backend.theme.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by mye(Xizhe Ye) on 18-3-20.
 */
public interface SysThemeRepository extends JpaRepository<SysTheme, String> {
  @Modifying(clearAutomatically = true)
  @Query("update SysTheme st set st.theme = :theme, st.dark = :dark where st.id = :id")
  @Transactional(rollbackFor = Exception.class)
  void updateSysThemeById(@Param(value = "theme") String theme, @Param("dark") boolean dark, @Param("id") long id);
}
