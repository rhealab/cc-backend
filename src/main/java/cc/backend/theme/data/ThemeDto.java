package cc.backend.theme.data;

/**
 * Created by mye(Xizhe Ye) on 18-3-20.
 */
public class ThemeDto {
  private String theme;
  private boolean dark;

  public String getTheme() {
    return theme;
  }

  public void setTheme(String theme) {
    this.theme = theme;
  }

  public boolean isDark() {
    return dark;
  }

  public void setDark(boolean dark) {
    this.dark = dark;
  }

  public static ThemeDto from(Theme theme) {
    if (theme == null) return null;

    ThemeDto themeDto = new ThemeDto();
    themeDto.setTheme(theme.getTheme());
    themeDto.setDark(theme.isDark());
    return themeDto;
  }

  public static ThemeDto fromSys(SysTheme sysTheme) {
    if (sysTheme == null) {
      return null;
    }
    ThemeDto themeDto = new ThemeDto();
    themeDto.setTheme(sysTheme.getTheme());
    themeDto.setDark(sysTheme.isDark());
    return themeDto;
  }
}
