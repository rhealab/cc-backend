package cc.backend.theme.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by mye(Xizhe Ye) on 18-3-20.
 */
@Entity
public class SysTheme {
  @Id
  @Column(name = "id")
  private long id = 0L;

  private String theme;
  private boolean dark;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getTheme() {
    return theme;
  }

  public void setTheme(String theme) {
    this.theme = theme;
  }

  public boolean isDark() {
    return dark;
  }

  public void setDark(boolean dark) {
    this.dark = dark;
  }
}
