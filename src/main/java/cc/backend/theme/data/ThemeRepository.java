package cc.backend.theme.data;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by mye(Xizhe Ye) on 18-3-20.
 */
public interface ThemeRepository extends JpaRepository<Theme, String> {
  Theme findByUserId(String userId);
}
