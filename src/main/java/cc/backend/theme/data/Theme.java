package cc.backend.theme.data;

import cc.backend.audit.request.EnnContext;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.validation.constraints.NotNull;

/**
 * Created by mye(Xizhe Ye) on 18-3-20.
 */
@Entity
public class Theme {
  @Id
  private String userId;
  @NotNull
  private String theme;
  private boolean dark;

  @PrePersist
  public void saveUserId() {
    this.userId = EnnContext.getUserId();
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getTheme() {
    return theme;
  }

  public void setTheme(String theme) {
    this.theme = theme;
  }

  public boolean isDark() {
    return dark;
  }

  public void setDark(boolean dark) {
    this.dark = dark;
  }

  public static Theme from(ThemeDto themeDto) {
    Theme theme = new Theme();
    theme.setUserId(EnnContext.getUserId());
    theme.setTheme(themeDto.getTheme());
    theme.setDark(themeDto.isDark());
    return theme;
  }
}
