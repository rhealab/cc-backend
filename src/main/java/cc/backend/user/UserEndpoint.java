package cc.backend.user;

import cc.backend.user.data.User;
import cc.backend.user.data.UserRepository;
import cc.backend.user.data.UserStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author wangchunyang@gmail.com
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("users")
@Api(value = "User", description = "Deprecated. please use cc-account api. Info about users.",
        produces = "application/json")
@Deprecated
public class UserEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(UserEndpoint.class);

    @Inject
    private UserRepository userRepository;

    @GET
    @ApiOperation(value = "List all users info.", responseContainer = "List", response = User.class)
    public Response getAllUsers() {
        List<User> userList = userRepository.findByStatus(UserStatus.VALID);
        return Response.ok(userList).build();
    }

    @GET
    @Path("{userId}")
    @ApiOperation(value = "Get user info by id.", responseContainer = "List", response = User.class)
    public Response getUserById(@PathParam("userId") String userId) {
        User user = userRepository.findOne(userId);
        return Response.ok(user).build();
    }
}