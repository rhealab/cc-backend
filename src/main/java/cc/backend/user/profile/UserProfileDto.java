package cc.backend.user.profile;

import cc.backend.audit.request.EnnContext;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/5/11.
 */
public class UserProfileDto {
    private UserLanguage language;

    public UserLanguage getLanguage() {
        return language;
    }

    public void setLanguage(UserLanguage language) {
        this.language = language;
    }

    public static UserProfile to(UserProfileDto dto) {
        UserProfile userProfile = new UserProfile();
        userProfile.setLanguage(dto.getLanguage());
        userProfile.setUserId(EnnContext.getUserId());
        return userProfile;
    }
}
