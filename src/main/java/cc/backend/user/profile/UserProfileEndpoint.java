package cc.backend.user.profile;

import cc.backend.audit.request.EnnContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/5/11.
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("user_profile")
@Api(value = "User", description = "Deprecated. please use cc-account api. Info about users.", produces = "application/json")
@Deprecated
public class UserProfileEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(UserProfileEndpoint.class);

    @Inject
    private UserProfileRepository userProfileRepository;

    @POST
    @ApiOperation(value = "Set user profile.")
    public Response setUserProfile(UserProfile userProfile) {
        String userId = EnnContext.getUserId();
        userProfile.setUserId(userId);
        if (StringUtils.isEmpty(userProfile.getClusterName())) {
            userProfile.setClusterName(EnnContext.getClusterName());
        }
        userProfileRepository.save(userProfile);
        return Response.ok().build();
    }

    @GET
    @ApiOperation(value = "Get user profile.", response = UserProfile.class)
    public Response getUserProfile() {
        String userId = EnnContext.getUserId();
        UserProfile userProfile = userProfileRepository.findOne(userId);
        if (userProfile == null) {
            userProfile = new UserProfile();
            userProfile.setUserId(userId);
            userProfile.setLanguage(UserLanguage.zh);
            userProfile.setClusterName(EnnContext.getClusterName());
        }
        return Response.ok(userProfile).build();
    }
}
