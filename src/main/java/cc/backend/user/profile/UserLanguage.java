package cc.backend.user.profile;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/5/11.
 */
public enum UserLanguage {
    zh, en
}
