package cc.backend.user.profile;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/5/11.
 */
public interface UserProfileRepository extends JpaRepository<UserProfile, String> {
}
