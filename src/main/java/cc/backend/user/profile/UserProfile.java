package cc.backend.user.profile;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/5/11.
 */
@Entity
public class UserProfile {
    @Id
    private String userId;

    @Enumerated(EnumType.STRING)
    private UserLanguage language;

    private String clusterName;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public UserLanguage getLanguage() {
        return language;
    }

    public void setLanguage(UserLanguage language) {
        this.language = language;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    @Override
    public String toString() {
        return "UserProfile{" +
                "userId='" + userId + '\'' +
                ", language=" + language +
                ", clusterName='" + clusterName + '\'' +
                '}';
    }
}
