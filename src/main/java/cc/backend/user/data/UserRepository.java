package cc.backend.user.data;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author wangchunyang@gmail.com
 */
public interface UserRepository extends JpaRepository<User, String> {
    List<User> findByUsernameIn(List<String> usernames);

    List<User> findByUserIdIn(List<String> userIds);

    Long countByStatus(UserStatus userStatus);

    List<User> findByStatus(UserStatus userStatus);
}
