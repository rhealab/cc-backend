package cc.backend.role.assignment.services;

import cc.backend.audit.request.EnnContext;
import cc.backend.role.assignment.RoleAssignmentDto;
import cc.backend.role.assignment.data.AssignmentExtrasRepository;
import cc.backend.role.assignment.data.RoleAssignmentExtras;
import cc.keystone.client.KeystoneRoleAssignmentManagement;
import cc.keystone.client.domain.RoleAssignment;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/2/15.
 */
@Component
public class RoleAssignmentManagement {

    @Inject
    private KeystoneRoleAssignmentManagement keystoneRoleAssignmentManagement;

    @Inject
    private AssignmentExtrasRepository assignmentExtrasRepository;

    public List<RoleAssignmentDto> getRoleAssignments(String userId) {
        List<RoleAssignment> assignments = keystoneRoleAssignmentManagement
                .getUserRoleAssignments(EnnContext.getClusterName(), userId);
        List<RoleAssignmentExtras> extrasList = assignmentExtrasRepository.findByUserId(userId);
        return composeRoleAssignmentsDto(assignments, extrasList);
    }

    private List<RoleAssignmentDto> composeRoleAssignmentsDto(List<RoleAssignment> assignments,
                                                              List<RoleAssignmentExtras> extrasList) {
        Map<String, RoleAssignmentExtras> extrasMap = extrasList.stream()
                .collect(Collectors.toMap(RoleAssignmentExtras::getNamespace, extras -> extras));
        return assignments.stream()
                .map(RoleAssignmentDto::from)
                .peek(dto -> {
                    RoleAssignmentExtras extras = extrasMap.get(dto.getNamespace());
                    if (extras == null) {
                        return;
                    }
                    dto.setAssignedBy(extras.getAssignedBy());
                    dto.setAssignedDate(extras.getAssignedOn());
                    dto.setStatus(extras.getStatus());
                })
                .collect(Collectors.toList());
    }
}
