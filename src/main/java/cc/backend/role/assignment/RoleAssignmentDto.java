package cc.backend.role.assignment;

import cc.backend.role.assignment.data.RoleAssignmentExtras;
import cc.keystone.client.domain.RoleAssignment;

import java.util.Date;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/1/24.
 */
public class RoleAssignmentDto {

    private String userId;
    private String username;
    private String namespace;
    private String roleName;

    private Date assignedDate;
    private String assignedBy;

    private boolean enabled = true;
    private RoleAssignmentExtras.Status status;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Date getAssignedDate() {
        return assignedDate;
    }

    public void setAssignedDate(Date assignedDate) {
        this.assignedDate = assignedDate;
    }

    public String getAssignedBy() {
        return assignedBy;
    }

    public void setAssignedBy(String assignedBy) {
        this.assignedBy = assignedBy;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public RoleAssignmentExtras.Status getStatus() {
        return status;
    }

    public void setStatus(RoleAssignmentExtras.Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "RoleAssignmentDto{" +
                "userId='" + userId + '\'' +
                ", username='" + username + '\'' +
                ", namespace='" + namespace + '\'' +
                ", roleName='" + roleName + '\'' +
                ", assignedDate=" + assignedDate +
                ", assignedBy='" + assignedBy + '\'' +
                ", enabled=" + enabled +
                ", status=" + status +
                '}';
    }

    public static RoleAssignmentDto from(RoleAssignment assignment) {
        RoleAssignmentDto dto = new RoleAssignmentDto();
        dto.setUserId(assignment.getUserId());
        dto.setUsername(assignment.getUserName());
        dto.setNamespace(assignment.getProject());
        dto.setStatus(RoleAssignmentExtras.Status.ENABLED);
        dto.setRoleName(assignment.getRole().name().toLowerCase());
        return dto;
    }
}
