package cc.backend.role.assignment.data;

import cc.backend.audit.request.EnnContext;
import cc.keystone.client.domain.RoleType;

import javax.persistence.*;
import java.util.Date;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/2/10.
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"userId", "namespace"}))
public class RoleAssignmentExtras {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String userId;
    @Enumerated(EnumType.STRING)
    private RoleType role;
    private String namespace;

    @Enumerated(EnumType.STRING)
    private ActionType actionType;

    private Date assignedOn;
    private String assignedBy;

    @Enumerated(EnumType.STRING)
    private Status status = Status.ENABLED;

    private Date lastUpdatedOn;
    private String lastUpdatedBy;
    private Date createdOn;
    private String createdBy;

    @PreUpdate
    public void onUpdate() {
        Date date = new Date();
        lastUpdatedBy = EnnContext.getUserId();
        lastUpdatedOn = date;

        assignedBy = EnnContext.getUserId();
        assignedOn = date;
    }

    @PrePersist
    public void onCreate() {
        Date date = new Date();

        createdBy = EnnContext.getUserId();
        createdOn = date;

        assignedBy = EnnContext.getUserId();
        assignedOn = date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public RoleType getRole() {
        return role;
    }

    public void setRole(RoleType role) {
        this.role = role;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public Date getAssignedOn() {
        return assignedOn;
    }

    public void setAssignedOn(Date assignedOn) {
        this.assignedOn = assignedOn;
    }

    public String getAssignedBy() {
        return assignedBy;
    }

    public void setAssignedBy(String assignedBy) {
        this.assignedBy = assignedBy;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(Date lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public String toString() {
        return "RoleAssignmentExtras{" +
                "id=" + id +
                ", userId='" + userId + '\'' +
                ", role=" + role +
                ", namespace='" + namespace + '\'' +
                ", actionType=" + actionType +
                ", assignedOn=" + assignedOn +
                ", assignedBy='" + assignedBy + '\'' +
                ", status=" + status +
                ", lastUpdatedOn=" + lastUpdatedOn +
                ", lastUpdatedBy='" + lastUpdatedBy + '\'' +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                '}';
    }

    public enum ActionType {
        ASSIGN, UNASSIGN, UPDATE
    }

    public enum Status {
        ENABLED, DISABLED
    }
}
