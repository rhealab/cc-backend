package cc.backend.role.assignment.data;

import cc.backend.role.assignment.data.RoleAssignmentExtras.ActionType;
import cc.keystone.client.domain.RoleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/2/10.
 */
public interface AssignmentExtrasRepository extends JpaRepository<RoleAssignmentExtras, Long> {

    /**
     * find a user's assignment by role and namespace
     * eg: find a user's sys admin assignment is: null, user, SYS_ADMIN
     *
     * @param namespace the namespace name
     * @param userId    the user id
     * @param role      the role type
     * @return the assignment, may be null
     */
    RoleAssignmentExtras findByNamespaceAndUserIdAndRole(String namespace,
                                                         String userId,
                                                         RoleType role);

    /**
     * find a user's assignment in a namespace
     *
     * @param namespace the namespace
     * @param userId    the user id
     * @return the assignment, may be null
     */
    RoleAssignmentExtras findByNamespaceAndUserId(String namespace,
                                                  String userId);

    /**
     * find some user's assignment in a namespace
     *
     * @param namespace  the namespace
     * @param userIdList the user id list
     * @return the assignment list
     */
    List<RoleAssignmentExtras> findByNamespaceAndUserIdIn(String namespace,
                                                          List<String> userIdList);

    /**
     * find a user's role assignment list in a cluster
     *
     * @param userId the user id
     * @return the assignment list
     */
    List<RoleAssignmentExtras> findByUserId(String userId);

    /**
     * find a namespace's assignment by user id and action type
     *
     * @param userId     the user id
     * @param actionType the action type
     * @return the assignment list
     */
    List<RoleAssignmentExtras> findByUserIdAndActionType(String userId, ActionType actionType);

    /**
     * find a namespace's assignment by action type (like get a ASSIGN action type in namespace)
     * TODO may be deprecated
     *
     * @param namespace  the namespace
     * @param actionType the action type
     * @return the assignment list
     */
    List<RoleAssignmentExtras> findByNamespaceAndActionType(String namespace,
                                                            ActionType actionType);

    /**
     * find all assignment by role. like find all sys admin
     *
     * @param role the role type
     * @return the assignment list
     */
    List<RoleAssignmentExtras> findByRole(RoleType role);

    /**
     * delete a namespace's assignment
     *
     * @param namespace the namespace
     */
    @Transactional(rollbackFor = Exception.class)
    void deleteByNamespace(String namespace);
}