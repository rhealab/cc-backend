package cc.backend.role.assignment;

import cc.backend.audit.request.EnnContext;
import cc.backend.role.assignment.services.RoleAssignmentManagement;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/1/19.
 * <p>
 * =================================
 * can only get current user's info
 * =================================
 */

@Named
@Path("role_assignments")
@Api(value = "Role", description = "Deprecated. please use cc-account api.Operation about role.", produces = "application/json")
@Produces(MediaType.APPLICATION_JSON)
@Deprecated
public class RoleAssignmentEndpoint {
    @Inject
    RoleAssignmentManagement roleAssignmentManagement;

    @GET
    @ApiOperation(value = "Get current user's role assignments in whole system.",
            responseContainer = "List", response = RoleAssignmentDto.class)
    public Response getMyRoleAssignments() {
        List<RoleAssignmentDto> dtos = roleAssignmentManagement.getRoleAssignments(EnnContext.getUserId());
        return Response.ok(dtos).build();
    }
}
