package cc.backend.sys.externallink.data;

import javax.persistence.*;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/23.
 */
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"name"})})
public class ExternalLink {
    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String type;

    private String url;

    @Column(name = "enabled", columnDefinition = "TINYINT(1) DEFAULT 0", nullable = false)
    private Boolean enabled;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "ExternalLink{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", url='" + url + '\'' +
                ", enabled=" + enabled +
                '}';
    }
}
