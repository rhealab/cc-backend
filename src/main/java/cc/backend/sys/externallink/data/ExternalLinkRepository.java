package cc.backend.sys.externallink.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/23.
 */
public interface ExternalLinkRepository extends JpaRepository<ExternalLink, Long> {
    /**
     * find a external link by name
     *
     * @param name the external link name
     * @return the external link
     */
    ExternalLink findByName(String name);

    /**
     * delete a external link by name
     *
     * @param name the external link name
     */
    @Transactional(rollbackFor = Exception.class)
    void deleteByName(String name);
}