package cc.backend.sys.externallink;


import cc.backend.sys.externallink.data.ExternalLink;
import cc.backend.sys.externallink.dto.ExternalLinkCreateRequest;
import cc.backend.sys.externallink.dto.ExternalLinkResponse;
import cc.backend.sys.externallink.dto.ExternalLinkUpdateRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/21.
 */
@Named
@Path("sys/external_link")
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "System", description = "Operation about system.", produces = "application/json")
public class ExternalLinkEndpoint {
    @Inject
    private ExternalLinkManagement management;

    @Inject
    private ExternalLinkValidator validator;

    @GET
    @ApiOperation(value = "Get system's external link list.", responseContainer = "List", response = ExternalLinkResponse.class)
    public Response getExternalLinkList() {
        return Response.ok(ExternalLinkResponse.from(management.getExternalLinkList())).build();
    }

    @GET
    @Path("{id}")
    @ApiOperation(value = "Get system's external link list.", response = ExternalLinkResponse.class)
    public Response getExternalLinkById(@PathParam("id") Long id) {
        return Response.ok(ExternalLinkResponse.from(validator.validateExternalLinkShouldExists(id))).build();
    }

    @GET
    @Path("{externalLinkName}/by_name")
    @ApiOperation(value = "Get system's external link list.", response = ExternalLinkResponse.class)
    public Response getExternalLinkByName(@PathParam("externalLinkName") String externalLinkName) {
        return Response.ok(ExternalLinkResponse.from(validator.validateExternalLinkShouldExists(externalLinkName))).build();
    }

    @POST
    @ApiOperation(value = "Create a system's external link.", response = ExternalLinkResponse.class)
    public Response createExternalLink(@Valid ExternalLinkCreateRequest request) {
        validator.validateExternalLinkShouldNotExists(request.getName());
        ExternalLink link = management.createExternalLink(request.newExternalLink());
        return Response.ok(ExternalLinkResponse.from(link)).build();
    }

    @PUT
    @Path("{id}")
    @ApiOperation(value = "Create a system's external link.")
    public Response updateExternalLink(@PathParam("id") Long id, @Valid ExternalLinkUpdateRequest request) {
        ExternalLink link = validator.validateExternalLinkShouldExists(id);
        management.updateExternalLink(ExternalLinkUpdateRequest.newLink(request, link));
        return Response.ok().build();
    }

    @PUT
    @Path("{externalLinkName}/by_name")
    @ApiOperation(value = "Create a system's external link.")
    public Response updateExternalLinkByName(@PathParam("externalLinkName") String externalLinkName,
                                             @Valid ExternalLinkUpdateRequest request) {
        ExternalLink link = validator.validateExternalLinkShouldExists(externalLinkName);
        if (request.getName() != null && !request.getName().equals(externalLinkName)) {
            validator.validateExternalLinkShouldNotExists(request.getName());
        }
        management.updateExternalLink(ExternalLinkUpdateRequest.newLink(request, link));
        return Response.ok().build();
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(value = "Delete a system's external link by id.")
    public Response deleteExternalLink(@PathParam("id") Long id) {
        validator.validateExternalLinkShouldExists(id);
        management.deleteExternalLinkById(id);
        return Response.ok().build();
    }

    @DELETE
    @Path("{externalLinkName}/by_name")
    @ApiOperation(value = "Delete a system's external link by name.")
    public Response deleteExternalLink(@PathParam("externalLinkName") String externalLinkName) {
        validator.validateExternalLinkShouldExists(externalLinkName);
        management.deleteExternalLinkByName(externalLinkName);
        return Response.ok().build();
    }
}
