package cc.backend.sys.externallink;


import cc.backend.sys.externallink.data.ExternalLink;
import cc.backend.sys.externallink.data.ExternalLinkRepository;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/21.
 */
@Component
public class ExternalLinkManagement {
    @Inject
    private ExternalLinkRepository repository;

    public List<ExternalLink> getExternalLinkList() {
        return repository.findAll();
    }

    public ExternalLink getExternalLinkById(Long id) {
        return repository.findOne(id);
    }

    public ExternalLink getExternalLinkByName(String name) {
        return repository.findByName(name);
    }

    public ExternalLink createExternalLink(ExternalLink link) {
        return repository.save(link);
    }

    public ExternalLink updateExternalLink(ExternalLink link) {
        return repository.save(link);
    }

    public void deleteExternalLinkByName(String externalLinkName) {
        repository.deleteByName(externalLinkName);
    }

    public void deleteExternalLinkById(Long id) {
        repository.delete(id);
    }
}
