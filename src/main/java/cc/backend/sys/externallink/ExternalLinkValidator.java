package cc.backend.sys.externallink;

import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.sys.externallink.data.ExternalLink;
import cc.backend.sys.externallink.data.ExternalLinkRepository;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/21.
 */
@Component
public class ExternalLinkValidator {
    @Inject
    private ExternalLinkRepository repository;

    public ExternalLink validateExternalLinkShouldExists(Long id) {
        ExternalLink link = repository.findOne(id);
        if (link == null) {
            throw new CcException(BackendReturnCodeNameConstants.EXTERNAL_LINK_NOT_EXISTS,
                    ImmutableMap.of("id", id));
        }
        return link;
    }

    public ExternalLink validateExternalLinkShouldExists(String name) {
        ExternalLink link = repository.findByName(name);
        if (link == null) {
            throw new CcException(BackendReturnCodeNameConstants.EXTERNAL_LINK_NOT_EXISTS,
                    ImmutableMap.of("name", name));
        }
        return link;
    }

    public void validateExternalLinkShouldNotExists(String name) {
        ExternalLink link = repository.findByName(name);
        if (link != null) {
            throw new CcException(BackendReturnCodeNameConstants.EXTERNAL_LINK_ALREADY_EXISTS,
                    ImmutableMap.of("name", name));
        }
    }
}
