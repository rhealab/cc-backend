package cc.backend.sys.externallink.dto;

import cc.backend.sys.externallink.data.ExternalLink;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/23.
 */
public class ExternalLinkUpdateRequest {

    private Long id;

    private String name;

    private String url;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "ExternalLinkUpdateRequest{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                '}';
    }

    public static ExternalLink newLink(ExternalLinkUpdateRequest request, ExternalLink link) {
        if (request.getName() != null) {
            link.setName(request.getName());
        }

        if (request.getUrl() != null) {
            link.setUrl(request.getUrl());
        }

        return link;
    }
}
