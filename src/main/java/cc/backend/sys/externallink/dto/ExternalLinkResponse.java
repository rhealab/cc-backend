package cc.backend.sys.externallink.dto;

import cc.backend.sys.externallink.data.ExternalLink;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/23.
 */
public class ExternalLinkResponse {
    private Long id;
    private String name;

    private String url;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "ExternalLinkUpdateRequest{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                '}';
    }

    public static List<ExternalLinkResponse> from(List<ExternalLink> linkList) {
        return linkList.stream()
                .map(ExternalLinkResponse::from)
                .collect(Collectors.toList());
    }

    public static ExternalLinkResponse from(ExternalLink link) {
        ExternalLinkResponse response = new ExternalLinkResponse();

        response.setId(link.getId());
        response.setName(link.getName());
        response.setUrl(link.getUrl());

        return response;
    }
}
