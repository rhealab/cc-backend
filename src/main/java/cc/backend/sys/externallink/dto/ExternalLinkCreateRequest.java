package cc.backend.sys.externallink.dto;

import cc.backend.sys.externallink.data.ExternalLink;

import javax.validation.constraints.NotNull;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/23.
 */
public class ExternalLinkCreateRequest {
    @NotNull
    private String name;
    @NotNull
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "ExternalLinkCreateRequest{" +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                '}';
    }

    public ExternalLink newExternalLink() {
        ExternalLink link = new ExternalLink();
        link.setName(getName());
        link.setType("");
        link.setUrl(getUrl());
        link.setEnabled(true);
        return link;
    }
}
