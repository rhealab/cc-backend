package cc.backend.sys.resource;

import cc.backend.sys.resource.service.ResourceCountManagement;
import cc.backend.kubernetes.namespaces.dto.ResourceCount;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/15.
 */
@Component
@Path("sys/resource_count")
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "System", description = "Operation about system.", produces = "application/json")
public class ResourceCountEndpoint {

    @Inject
    private ResourceCountManagement management;

    @GET
    @ApiOperation(value = "Get system resource count.", response = ResourceCount.class)
    public Response getSysResourceCount() {
        ResourceCount resourceCount = management.getSystemResourceCount();
        return Response.ok(resourceCount).build();
    }
}