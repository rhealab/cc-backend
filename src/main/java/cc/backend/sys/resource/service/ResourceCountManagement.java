package cc.backend.sys.resource.service;

import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.apps.data.AppRepository;
import cc.backend.kubernetes.namespaces.dto.ResourceCount;
import cc.backend.user.data.UserRepository;
import cc.backend.user.data.UserStatus;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;


/**
 * Created by xzy on 2017/2/14.
 *
 * @author yanzhixiang modified on 2017-6-7
 */

@Component
public class ResourceCountManagement {
    @Inject
    protected KubernetesClientManager clientManager;

    @Inject
    protected AppRepository appRepository;

    @Inject
    protected UserRepository userRepository;

    public ResourceCount getSystemResourceCount() {
        ResourceCount resourceCount = new ResourceCount();

        resourceCount.setAppNum(appRepository.count());

        List<Deployment> deployments = clientManager.getClient()
                .extensions()
                .deployments()
                .list()
                .getItems();
        resourceCount.setDeploymentNum(deployments == null ? 0 : deployments.size());

        List<Service> services = clientManager.getClient()
                .services()
                .list()
                .getItems();
        resourceCount.setServiceNum(services == null ? 0 : services.size());

        List<Pod> pods = clientManager.getClient()
                .pods()
                .list()
                .getItems();
        resourceCount.setPodNum(pods == null ? 0 : pods.size());

        resourceCount.setUserNum(userRepository.countByStatus(UserStatus.VALID));
        return resourceCount;
    }
}
