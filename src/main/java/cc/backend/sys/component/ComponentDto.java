package cc.backend.sys.component;

import cc.backend.sys.component.data.SiteConfig;

/**
 * @author mrzihan.tang@gmail.com on 07/09/2017.
 */
public class ComponentDto {
    private String version;
    private boolean ceph;
    private boolean nfs;
    private boolean es;
    private boolean openTSDB;
    private boolean prometheus;
    private boolean aws;
    private String baseDomainName;
    private String outSideName;
    private String inSideName;

    public ComponentDto() {
        this.version = "enncloud";
        this.ceph = true;
        this.nfs = false;
        this.es = true;
        this.openTSDB = true;
        this.prometheus = true;
        this.aws = false;
        this.baseDomainName = "";
        this.outSideName = "";
        this.inSideName = "";
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public boolean isCeph() {
        return ceph;
    }

    public void setCeph(boolean ceph) {
        this.ceph = ceph;
    }

    public boolean isNfs() {
        return nfs;
    }

    public void setNfs(boolean nfs) {
        this.nfs = nfs;
    }

    public boolean isEs() {
        return es;
    }

    public void setEs(boolean es) {
        this.es = es;
    }

    public boolean isOpenTSDB() {
        return openTSDB;
    }

    public void setOpenTSDB(boolean openTSDB) {
        this.openTSDB = openTSDB;
    }

    public boolean isPrometheus() {
        return prometheus;
    }

    public void setPrometheus(boolean prometheus) {
        this.prometheus = prometheus;
    }

    public boolean isAws() {
        return aws;
    }

    public void setAws(boolean aws) {
        this.aws = aws;
    }

    public String getBaseDomainName() {
        return baseDomainName;
    }

    public void setBaseDomainName(String baseDomainName) {
        this.baseDomainName = baseDomainName;
    }

    public String getOutSideName() {
        return outSideName;
    }

    public void setOutSideName(String outSideName) {
        this.outSideName = outSideName;
    }

    public String getInSideName() {
        return inSideName;
    }

    public void setInSideName(String inSideName) {
        this.inSideName = inSideName;
    }

    public SiteConfig toSiteConfig() {
        SiteConfig ret = new SiteConfig();
        ret.setVersion(this.version);
        ret.setCeph(this.ceph);
        ret.setNfs(this.nfs);
        ret.setOpenTSDB(this.openTSDB);
        ret.setEs(this.es);
        ret.setPrometheus(this.prometheus);
        ret.setAws(this.aws);
        ret.setBaseDomainName(this.baseDomainName);
        ret.setInSideName(this.inSideName);
        ret.setOutSideName(this.outSideName);
        return ret;
    }
}

