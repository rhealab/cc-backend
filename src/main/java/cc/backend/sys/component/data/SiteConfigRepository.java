package cc.backend.sys.component.data;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author mrzihan.tang@gmail.com on 13/09/2017.
 */
public interface SiteConfigRepository extends JpaRepository<SiteConfig,String> {

}
