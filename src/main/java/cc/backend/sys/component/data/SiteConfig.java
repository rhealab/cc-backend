package cc.backend.sys.component.data;

import cc.backend.sys.component.ComponentDto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author mrzihan.tang@gmail.com on 13/09/2017.
 */
@Entity
public class SiteConfig {
    @Id
    private String version;

    @Column(name = "ceph", columnDefinition = "TINYINT(1)", nullable = false)
    private boolean ceph;
    @Column(name = "nfs", columnDefinition = "TINYINT(1) DEFAULT 0", nullable = false)
    private boolean nfs;
    @Column(name = "es", columnDefinition = "TINYINT(1)", nullable = false)
    private boolean es;
    @Column(name = "openTSDB", columnDefinition = "TINYINT(1)", nullable = false)
    private boolean openTSDB;
    @Column(name = "prometheus", columnDefinition = "TINYINT(1)", nullable = false)
    private boolean prometheus;
    @Column(name = "aws", columnDefinition = "TINYINT(1) DEFAULT 0", nullable = false)
    private boolean aws;

    @Column(name = "base_domain_name")
    private String baseDomainName;
    @Column(name = "out_side_name")
    private String outSideName;
    @Column(name = "in_side_name")
    private String inSideName;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public boolean isCeph() {
        return ceph;
    }

    public void setCeph(boolean ceph) {
        this.ceph = ceph;
    }

    public boolean isNfs() {
        return nfs;
    }

    public void setNfs(boolean nfs) {
        this.nfs = nfs;
    }

    public boolean isEs() {
        return es;
    }

    public void setEs(boolean es) {
        this.es = es;
    }

    public boolean isOpenTSDB() {
        return openTSDB;
    }

    public void setOpenTSDB(boolean openTSDB) {
        this.openTSDB = openTSDB;
    }

    public boolean isPrometheus() {
        return prometheus;
    }

    public void setPrometheus(boolean prometheus) {
        this.prometheus = prometheus;
    }

    public boolean isAws() {
        return aws;
    }

    public void setAws(boolean aws) {
        this.aws = aws;
    }

    public String getBaseDomainName() {
        return baseDomainName;
    }

    public void setBaseDomainName(String baseDomainName) {
        this.baseDomainName = baseDomainName;
    }

    public String getOutSideName() {
        return outSideName;
    }

    public void setOutSideName(String outSideName) {
        this.outSideName = outSideName;
    }

    public String getInSideName() {
        return inSideName;
    }

    public void setInSideName(String inSideName) {
        this.inSideName = inSideName;
    }

    @Override
    public String toString() {
        return "SiteConfig{" +
                "version='" + version + '\'' +
                ", ceph=" + ceph +
                ", nfs=" + nfs +
                ", es=" + es +
                ", openTSDB=" + openTSDB +
                ", prometheus=" + prometheus +
                ", aws=" + aws +
                '}';
    }

    public ComponentDto toComponentDto() {
        ComponentDto ret = new ComponentDto();
        ret.setCeph(this.ceph);
        ret.setNfs(this.nfs);
        ret.setEs(this.es);
        ret.setOpenTSDB(this.openTSDB);
        ret.setPrometheus(this.prometheus);
        ret.setVersion(this.version);
        ret.setAws(this.aws);
        ret.setBaseDomainName(this.baseDomainName);
        ret.setInSideName(this.inSideName);
        ret.setOutSideName(this.outSideName);
        return ret;
    }
}
