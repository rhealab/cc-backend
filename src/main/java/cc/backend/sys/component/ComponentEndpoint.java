package cc.backend.sys.component;


import cc.backend.sys.component.data.SiteConfig;
import cc.backend.sys.component.data.SiteConfigRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author mrzihan.tang@gmail.com on 07/09/2017.
 */
@Named
@Path("sys/component")
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "System", description = "Operation about system.", produces = "application/json")
public class ComponentEndpoint {
    @Inject
    private SiteConfigRepository siteConfigRepository;

    @GET
    @ApiOperation(value = "Get system config.", response = ComponentDto.class)
    public Response getStatus() {
        SiteConfig c = siteConfigRepository.findOne("current");
        return Response.ok(c == null ? new ComponentDto() : c.toComponentDto()).build();
    }

    @POST
    @ApiOperation(value = "Set system config.", response = ComponentDto.class)
    public Response setStatus(ComponentDto component) {
        SiteConfig c = component.toSiteConfig();
        siteConfigRepository.save(c);
        // @TODO in case we will support manage multiple settings in the future.
        if (!c.getVersion().equals("current")) {
            c.setVersion("current");
            siteConfigRepository.save(c);
        }
        return Response.ok(c.toComponentDto()).build();
    }
}
