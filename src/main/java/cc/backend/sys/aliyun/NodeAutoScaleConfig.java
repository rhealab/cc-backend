package cc.backend.sys.aliyun;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/22.
 */
public class NodeAutoScaleConfig {
    @JsonProperty("MaxCreateNode")
    private int maxCreateNode;
    @JsonProperty("CreateRequestCpuAbove")
    private int createRequestCpuAbove;
    @JsonProperty("CreateLimitCpuAbove")
    private int createLimitCpuAbove;
    @JsonProperty("CreateRequestMemAbove")
    private int createRequestMemAbove;
    @JsonProperty("CreateLimitMemAbove")
    private int createLimitMemAbove;
    @JsonProperty("DeleteRequestCpuUnder")
    private int deleteRequestCpuUnder;
    @JsonProperty("DeleteLimitCpuUnder")
    private int deleteLimitCpuUnder;
    @JsonProperty("DeleteRequestMemUnder")
    private int deleteRequestMemUnder;
    @JsonProperty("DeleteLimitMemUnder")
    private int deleteLimitMemUnder;

    public int getMaxCreateNode() {
        return maxCreateNode;
    }

    public void setMaxCreateNode(int maxCreateNode) {
        this.maxCreateNode = maxCreateNode;
    }

    public int getCreateRequestCpuAbove() {
        return createRequestCpuAbove;
    }

    public void setCreateRequestCpuAbove(int createRequestCpuAbove) {
        this.createRequestCpuAbove = createRequestCpuAbove;
    }

    public int getCreateLimitCpuAbove() {
        return createLimitCpuAbove;
    }

    public void setCreateLimitCpuAbove(int createLimitCpuAbove) {
        this.createLimitCpuAbove = createLimitCpuAbove;
    }

    public int getCreateRequestMemAbove() {
        return createRequestMemAbove;
    }

    public void setCreateRequestMemAbove(int createRequestMemAbove) {
        this.createRequestMemAbove = createRequestMemAbove;
    }

    public int getCreateLimitMemAbove() {
        return createLimitMemAbove;
    }

    public void setCreateLimitMemAbove(int createLimitMemAbove) {
        this.createLimitMemAbove = createLimitMemAbove;
    }

    public int getDeleteRequestCpuUnder() {
        return deleteRequestCpuUnder;
    }

    public void setDeleteRequestCpuUnder(int deleteRequestCpuUnder) {
        this.deleteRequestCpuUnder = deleteRequestCpuUnder;
    }

    public int getDeleteLimitCpuUnder() {
        return deleteLimitCpuUnder;
    }

    public void setDeleteLimitCpuUnder(int deleteLimitCpuUnder) {
        this.deleteLimitCpuUnder = deleteLimitCpuUnder;
    }

    public int getDeleteRequestMemUnder() {
        return deleteRequestMemUnder;
    }

    public void setDeleteRequestMemUnder(int deleteRequestMemUnder) {
        this.deleteRequestMemUnder = deleteRequestMemUnder;
    }

    public int getDeleteLimitMemUnder() {
        return deleteLimitMemUnder;
    }

    public void setDeleteLimitMemUnder(int deleteLimitMemUnder) {
        this.deleteLimitMemUnder = deleteLimitMemUnder;
    }

    @Override
    public String toString() {
        return "NodeAutoScaleConfig{" +
                "maxCreateNode=" + maxCreateNode +
                ", createRequestCpuAbove=" + createRequestCpuAbove +
                ", createLimitCpuAbove=" + createLimitCpuAbove +
                ", createRequestMemAbove=" + createRequestMemAbove +
                ", createLimitMemAbove=" + createLimitMemAbove +
                ", deleteRequestCpuUnder=" + deleteRequestCpuUnder +
                ", deleteLimitCpuUnder=" + deleteLimitCpuUnder +
                ", deleteRequestMemUnder=" + deleteRequestMemUnder +
                ", deleteLimitMemUnder=" + deleteLimitMemUnder +
                '}';
    }
}
