package cc.backend.sys.aliyun;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/22.
 */
public class NodeAutoScaleConfigUpdateRequest {
    private Integer maxCreateNode;
    private Integer createRequestCpuAbove;
    private Integer createLimitCpuAbove;
    private Integer createRequestMemAbove;
    private Integer createLimitMemAbove;
    private Integer deleteRequestCpuUnder;
    private Integer deleteLimitCpuUnder;
    private Integer deleteRequestMemUnder;
    private Integer deleteLimitMemUnder;

    public Integer getMaxCreateNode() {
        return maxCreateNode;
    }

    public void setMaxCreateNode(Integer maxCreateNode) {
        this.maxCreateNode = maxCreateNode;
    }

    public Integer getCreateRequestCpuAbove() {
        return createRequestCpuAbove;
    }

    public void setCreateRequestCpuAbove(Integer createRequestCpuAbove) {
        this.createRequestCpuAbove = createRequestCpuAbove;
    }

    public Integer getCreateLimitCpuAbove() {
        return createLimitCpuAbove;
    }

    public void setCreateLimitCpuAbove(Integer createLimitCpuAbove) {
        this.createLimitCpuAbove = createLimitCpuAbove;
    }

    public Integer getCreateRequestMemAbove() {
        return createRequestMemAbove;
    }

    public void setCreateRequestMemAbove(Integer createRequestMemAbove) {
        this.createRequestMemAbove = createRequestMemAbove;
    }

    public Integer getCreateLimitMemAbove() {
        return createLimitMemAbove;
    }

    public void setCreateLimitMemAbove(Integer createLimitMemAbove) {
        this.createLimitMemAbove = createLimitMemAbove;
    }

    public Integer getDeleteRequestCpuUnder() {
        return deleteRequestCpuUnder;
    }

    public void setDeleteRequestCpuUnder(Integer deleteRequestCpuUnder) {
        this.deleteRequestCpuUnder = deleteRequestCpuUnder;
    }

    public Integer getDeleteLimitCpuUnder() {
        return deleteLimitCpuUnder;
    }

    public void setDeleteLimitCpuUnder(Integer deleteLimitCpuUnder) {
        this.deleteLimitCpuUnder = deleteLimitCpuUnder;
    }

    public Integer getDeleteRequestMemUnder() {
        return deleteRequestMemUnder;
    }

    public void setDeleteRequestMemUnder(Integer deleteRequestMemUnder) {
        this.deleteRequestMemUnder = deleteRequestMemUnder;
    }

    public Integer getDeleteLimitMemUnder() {
        return deleteLimitMemUnder;
    }

    public void setDeleteLimitMemUnder(Integer deleteLimitMemUnder) {
        this.deleteLimitMemUnder = deleteLimitMemUnder;
    }

    @Override
    public String toString() {
        return "NodeAutoScaleDto{" +
                "maxCreateNode=" + maxCreateNode +
                ", createRequestCpuAbove=" + createRequestCpuAbove +
                ", createLimitCpuAbove=" + createLimitCpuAbove +
                ", createRequestMemAbove=" + createRequestMemAbove +
                ", createLimitMemAbove=" + createLimitMemAbove +
                ", deleteRequestCpuUnder=" + deleteRequestCpuUnder +
                ", deleteLimitCpuUnder=" + deleteLimitCpuUnder +
                ", deleteRequestMemUnder=" + deleteRequestMemUnder +
                ", deleteLimitMemUnder=" + deleteLimitMemUnder +
                '}';
    }
}
