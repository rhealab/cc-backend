package cc.backend.sys.aliyun;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/22.
 */
@Component
@Path("sys/node_auto_scale")
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "System", description = "AliCloud auto scale config.", produces = "application/json")
public class NodeAutoScaleConfigEndpoint {
    @Inject
    private NodeAutoScaleConfigClient client;

    @GET
    @ApiOperation(value = "Get AliCloud auto scale config.", responseContainer = "List",
            response = NodeAutoScaleDto.class)
    public Response getNodeAutoScaleConfig() {
        return Response.ok(NodeAutoScaleDto.from(client.getConfig())).build();
    }

    @POST
    @ApiOperation(value = "Set AliCloud auto scale config.")
    public Response setNodeAutoScaleConfig(NodeAutoScaleConfigUpdateRequest request) {
        client.setConfig(request);
        return Response.ok().build();
    }
}
