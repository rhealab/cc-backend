package cc.backend.sys.aliyun;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/22.
 */
public class NodeAutoScaleDto {
    private int maxCreateNode;
    private int createRequestCpuAbove;
    private int createLimitCpuAbove;
    private int createRequestMemAbove;
    private int createLimitMemAbove;
    private int deleteRequestCpuUnder;
    private int deleteLimitCpuUnder;
    private int deleteRequestMemUnder;
    private int deleteLimitMemUnder;

    public int getMaxCreateNode() {
        return maxCreateNode;
    }

    public void setMaxCreateNode(int maxCreateNode) {
        this.maxCreateNode = maxCreateNode;
    }

    public int getCreateRequestCpuAbove() {
        return createRequestCpuAbove;
    }

    public void setCreateRequestCpuAbove(int createRequestCpuAbove) {
        this.createRequestCpuAbove = createRequestCpuAbove;
    }

    public int getCreateLimitCpuAbove() {
        return createLimitCpuAbove;
    }

    public void setCreateLimitCpuAbove(int createLimitCpuAbove) {
        this.createLimitCpuAbove = createLimitCpuAbove;
    }

    public int getCreateRequestMemAbove() {
        return createRequestMemAbove;
    }

    public void setCreateRequestMemAbove(int createRequestMemAbove) {
        this.createRequestMemAbove = createRequestMemAbove;
    }

    public int getCreateLimitMemAbove() {
        return createLimitMemAbove;
    }

    public void setCreateLimitMemAbove(int createLimitMemAbove) {
        this.createLimitMemAbove = createLimitMemAbove;
    }

    public int getDeleteRequestCpuUnder() {
        return deleteRequestCpuUnder;
    }

    public void setDeleteRequestCpuUnder(int deleteRequestCpuUnder) {
        this.deleteRequestCpuUnder = deleteRequestCpuUnder;
    }

    public int getDeleteLimitCpuUnder() {
        return deleteLimitCpuUnder;
    }

    public void setDeleteLimitCpuUnder(int deleteLimitCpuUnder) {
        this.deleteLimitCpuUnder = deleteLimitCpuUnder;
    }

    public int getDeleteRequestMemUnder() {
        return deleteRequestMemUnder;
    }

    public void setDeleteRequestMemUnder(int deleteRequestMemUnder) {
        this.deleteRequestMemUnder = deleteRequestMemUnder;
    }

    public int getDeleteLimitMemUnder() {
        return deleteLimitMemUnder;
    }

    public void setDeleteLimitMemUnder(int deleteLimitMemUnder) {
        this.deleteLimitMemUnder = deleteLimitMemUnder;
    }

    @Override
    public String toString() {
        return "NodeAutoScaleDto{" +
                "maxCreateNode=" + maxCreateNode +
                ", createRequestCpuAbove=" + createRequestCpuAbove +
                ", createLimitCpuAbove=" + createLimitCpuAbove +
                ", createRequestMemAbove=" + createRequestMemAbove +
                ", createLimitMemAbove=" + createLimitMemAbove +
                ", deleteRequestCpuUnder=" + deleteRequestCpuUnder +
                ", deleteLimitCpuUnder=" + deleteLimitCpuUnder +
                ", deleteRequestMemUnder=" + deleteRequestMemUnder +
                ", deleteLimitMemUnder=" + deleteLimitMemUnder +
                '}';
    }

    public static NodeAutoScaleDto from(NodeAutoScaleConfig config) {
        NodeAutoScaleDto dto = new NodeAutoScaleDto();

        dto.setMaxCreateNode(config.getMaxCreateNode());

        dto.setCreateLimitCpuAbove(config.getCreateLimitCpuAbove());
        dto.setCreateLimitMemAbove(config.getCreateLimitMemAbove());
        dto.setCreateRequestCpuAbove(config.getCreateRequestCpuAbove());
        dto.setCreateRequestMemAbove(config.getCreateRequestMemAbove());

        dto.setDeleteLimitCpuUnder(config.getDeleteLimitCpuUnder());
        dto.setDeleteLimitMemUnder(config.getDeleteLimitMemUnder());
        dto.setDeleteRequestCpuUnder(config.getDeleteRequestCpuUnder());
        dto.setDeleteRequestMemUnder(config.getDeleteRequestMemUnder());

        return dto;
    }
}
