package cc.backend.sys.aliyun;

import cc.backend.EnnProperties;
import cc.backend.common.utils.OkHttpClientUtils;
import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.lib.retcode.CcException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;

/**
 * server doc https://docs.google.com/document/d/147JqlrESFMmyy7JGLg4HH0H_ucl5h9n157wZTGu2otg/edit
 *
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/22.
 */
@Component
public class NodeAutoScaleConfigClient {
    private static final Logger logger = LoggerFactory.getLogger(NodeAutoScaleConfigClient.class);

    @Inject
    private EnnProperties properties;

    public NodeAutoScaleConfig getConfig() {
        return exec("/ClusterManagerConfig", "GET", null, NodeAutoScaleConfig.class);
    }

    public void setConfig(NodeAutoScaleConfigUpdateRequest request) {
        NodeAutoScaleConfig current = getConfig();
        List<BiConsumer<NodeAutoScaleConfigUpdateRequest, NodeAutoScaleConfig>> functionList = sort(request, current);
        setConfigInternal(functionList, request, current);
    }

    private List<BiConsumer<NodeAutoScaleConfigUpdateRequest, NodeAutoScaleConfig>> sort(NodeAutoScaleConfigUpdateRequest request,
                                                                                         NodeAutoScaleConfig current) {
        List<BiConsumer<NodeAutoScaleConfigUpdateRequest, NodeAutoScaleConfig>> functionList = new ArrayList<>();

        functionList.add(this::setMaxCreateNode);

        if (request.getCreateRequestCpuAbove() != null
                && request.getCreateRequestCpuAbove() < current.getDeleteRequestCpuUnder()) {
            functionList.add(this::setDeleteRequestCpuUnder);
            functionList.add(this::setCreateRequestCpuAbove);
        } else {
            functionList.add(this::setCreateRequestCpuAbove);
            functionList.add(this::setDeleteRequestCpuUnder);
        }

        if (request.getCreateLimitCpuAbove() != null
                && request.getCreateLimitCpuAbove() < current.getDeleteLimitCpuUnder()) {
            functionList.add(this::setDeleteLimitCpuUnder);
            functionList.add(this::setCreateLimitCpuAbove);
        } else {
            functionList.add(this::setCreateLimitCpuAbove);
            functionList.add(this::setDeleteLimitCpuUnder);
        }

        if (request.getCreateRequestMemAbove() != null
                && request.getCreateRequestMemAbove() < current.getDeleteRequestMemUnder()) {
            functionList.add(this::setDeleteRequestMemUnder);
            functionList.add(this::setCreateRequestMemAbove);
        } else {
            functionList.add(this::setCreateRequestMemAbove);
            functionList.add(this::setDeleteRequestMemUnder);
        }

        if (request.getCreateLimitMemAbove() != null
                && request.getCreateLimitMemAbove() < current.getDeleteLimitMemUnder()) {
            functionList.add(this::setDeleteLimitMemUnder);
            functionList.add(this::setCreateLimitMemAbove);
        } else {
            functionList.add(this::setCreateLimitMemAbove);
            functionList.add(this::setDeleteLimitMemUnder);
        }

        return functionList;
    }

    private void setConfigInternal(List<BiConsumer<NodeAutoScaleConfigUpdateRequest, NodeAutoScaleConfig>> functionList,
                                   NodeAutoScaleConfigUpdateRequest request,
                                   NodeAutoScaleConfig current) {
        functionList.forEach(function -> function.accept(request, current));
    }

    private void setMaxCreateNode(NodeAutoScaleConfigUpdateRequest request, NodeAutoScaleConfig current) {
        if (shouldUpdate(request.getMaxCreateNode(), current.getMaxCreateNode())) {
            NodeAutoScaleResponseCode code = exec("/ClusterManagerConfig/set/MaxCreateNode/" + request.getMaxCreateNode(),
                    "GET", null, NodeAutoScaleResponseCode.class);
            if (code.getCode() != 0) {
                throw new CcException(BackendReturnCodeNameConstants.ALI_CLOUD_NODE_SCALE_CONFIG_NOT_VALID,
                        ImmutableMap.of("msg", code.getMsg(),
                                "detail", code.getData()));
            }
        }
    }

    private void setCreateRequestCpuAbove(NodeAutoScaleConfigUpdateRequest request, NodeAutoScaleConfig current) {
        if (shouldUpdate(request.getCreateRequestCpuAbove(), current.getCreateRequestCpuAbove())) {
            NodeAutoScaleResponseCode code = exec("/ClusterManagerConfig/set/CreateRequestCpuAbove/" + request.getCreateRequestCpuAbove(),
                    "GET", null, NodeAutoScaleResponseCode.class);
            if (code.getCode() != 0) {
                throw new CcException(BackendReturnCodeNameConstants.ALI_CLOUD_NODE_SCALE_CONFIG_NOT_VALID,
                        ImmutableMap.of("msg", code.getMsg(),
                                "detail", code.getData()));
            }
        }
    }

    private void setCreateLimitCpuAbove(NodeAutoScaleConfigUpdateRequest request, NodeAutoScaleConfig current) {
        if (shouldUpdate(request.getCreateLimitCpuAbove(), current.getCreateLimitCpuAbove())) {
            NodeAutoScaleResponseCode code = exec("/ClusterManagerConfig/set/CreateLimitCpuAbove/" + request.getCreateLimitCpuAbove(),
                    "GET", null, NodeAutoScaleResponseCode.class);
            if (code.getCode() != 0) {
                throw new CcException(BackendReturnCodeNameConstants.ALI_CLOUD_NODE_SCALE_CONFIG_NOT_VALID,
                        ImmutableMap.of("msg", code.getMsg(),
                                "detail", code.getData()));
            }
        }
    }

    private void setCreateRequestMemAbove(NodeAutoScaleConfigUpdateRequest request, NodeAutoScaleConfig current) {
        if (shouldUpdate(request.getCreateRequestMemAbove(), current.getCreateRequestMemAbove())) {
            NodeAutoScaleResponseCode code = exec("/ClusterManagerConfig/set/CreateRequestMemAbove/" + request.getCreateRequestMemAbove(),
                    "GET", null, NodeAutoScaleResponseCode.class);
            if (code.getCode() != 0) {
                throw new CcException(BackendReturnCodeNameConstants.ALI_CLOUD_NODE_SCALE_CONFIG_NOT_VALID,
                        ImmutableMap.of("msg", code.getMsg(),
                                "detail", code.getData()));
            }
        }
    }

    private void setCreateLimitMemAbove(NodeAutoScaleConfigUpdateRequest request, NodeAutoScaleConfig current) {
        if (shouldUpdate(request.getCreateLimitMemAbove(), current.getCreateLimitMemAbove())) {
            NodeAutoScaleResponseCode code = exec("/ClusterManagerConfig/set/CreateLimitMemAbove/" + request.getCreateLimitMemAbove(),
                    "GET", null, NodeAutoScaleResponseCode.class);
            if (code.getCode() != 0) {
                throw new CcException(BackendReturnCodeNameConstants.ALI_CLOUD_NODE_SCALE_CONFIG_NOT_VALID,
                        ImmutableMap.of("msg", code.getMsg(),
                                "detail", code.getData()));
            }
        }
    }

    private void setDeleteRequestCpuUnder(NodeAutoScaleConfigUpdateRequest request, NodeAutoScaleConfig current) {
        if (shouldUpdate(request.getDeleteRequestCpuUnder(), current.getDeleteRequestCpuUnder())) {
            NodeAutoScaleResponseCode code = exec("/ClusterManagerConfig/set/DeleteRequestCpuUnder/" + request.getDeleteRequestCpuUnder(),
                    "GET", null, NodeAutoScaleResponseCode.class);
            if (code.getCode() != 0) {
                throw new CcException(BackendReturnCodeNameConstants.ALI_CLOUD_NODE_SCALE_CONFIG_NOT_VALID,
                        ImmutableMap.of("msg", code.getMsg(),
                                "detail", code.getData()));
            }
        }
    }

    private void setDeleteLimitCpuUnder(NodeAutoScaleConfigUpdateRequest request, NodeAutoScaleConfig current) {
        if (shouldUpdate(request.getDeleteLimitCpuUnder(), current.getDeleteLimitCpuUnder())) {
            NodeAutoScaleResponseCode code = exec("/ClusterManagerConfig/set/DeleteLimitCpuUnder/" + request.getDeleteLimitCpuUnder(),
                    "GET", null, NodeAutoScaleResponseCode.class);
            if (code.getCode() != 0) {
                throw new CcException(BackendReturnCodeNameConstants.ALI_CLOUD_NODE_SCALE_CONFIG_NOT_VALID,
                        ImmutableMap.of("msg", code.getMsg(),
                                "detail", code.getData()));
            }
        }
    }

    private void setDeleteRequestMemUnder(NodeAutoScaleConfigUpdateRequest request, NodeAutoScaleConfig current) {
        if (shouldUpdate(request.getDeleteRequestMemUnder(), current.getDeleteRequestMemUnder())) {
            NodeAutoScaleResponseCode code = exec("/ClusterManagerConfig/set/DeleteRequestMemUnder/" + request.getDeleteRequestMemUnder(),
                    "GET", null, NodeAutoScaleResponseCode.class);
            if (code.getCode() != 0) {
                throw new CcException(BackendReturnCodeNameConstants.ALI_CLOUD_NODE_SCALE_CONFIG_NOT_VALID,
                        ImmutableMap.of("msg", code.getMsg(),
                                "detail", code.getData()));
            }
        }
    }

    private void setDeleteLimitMemUnder(NodeAutoScaleConfigUpdateRequest request, NodeAutoScaleConfig current) {
        if (shouldUpdate(request.getDeleteLimitMemUnder(), current.getDeleteLimitMemUnder())) {
            NodeAutoScaleResponseCode code = exec("/ClusterManagerConfig/set/DeleteLimitMemUnder/" + request.getDeleteLimitMemUnder(),
                    "GET", null, NodeAutoScaleResponseCode.class);
            if (code.getCode() != 0) {
                throw new CcException(BackendReturnCodeNameConstants.ALI_CLOUD_NODE_SCALE_CONFIG_NOT_VALID,
                        ImmutableMap.of("msg", code.getMsg(),
                                "detail", code.getData()));
            }
        }
    }

    private boolean shouldUpdate(Integer request, int current) {
        return request != null
                && request != -1
                && request != current;
    }

    public <T> T exec(String subUrl,
                      String requestMethod,
                      RequestBody requestBody,
                      Class<T> clazz) {
        EnnProperties.Cluster.Ali ali = properties.getCurrentCluster().getAli();
        Request request = new Request.Builder()
                .url(ali.getNodeAutoScale().getUrl() + subUrl)
                .method(requestMethod, requestBody)
                .header("Authorization", ali.getNodeAutoScale().getBasicToken())
                .build();

        try {
            Response response = OkHttpClientUtils
                    .getUnsafeOkHttpClient()
                    .newCall(request)
                    .execute();

            ResponseBody body = response.body();
            if (response.isSuccessful() && body != null) {
                ObjectMapper mapper = new ObjectMapper();
                mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                return mapper.readValue(body.bytes(), clazz);
            }

            if (body != null) {
                body.close();
            }

            throw new CcException(BackendReturnCodeNameConstants.ALI_CLOUD_NODE_SCALE_CONFIG_SERVER_ERROR,
                    ImmutableMap.of("msg", response.message(),
                            "code", response.code()));
        } catch (IOException e) {
            throw new CcException(BackendReturnCodeNameConstants.ALI_CLOUD_NODE_SCALE_CONFIG_SERVER_ERROR,
                    ImmutableMap.of("msg", Optional.ofNullable(e.getMessage()).orElse(""),
                            "cause", e.getCause() == null ? "" : e.getCause()));
        }
    }
}
