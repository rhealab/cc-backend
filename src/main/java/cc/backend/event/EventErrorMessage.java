package cc.backend.event;


import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSourceType;
import cc.backend.kubernetes.namespaces.messages.IMessage;

import java.io.Serializable;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/15.
 */
public class EventErrorMessage implements Serializable, IMessage {
    private String clusterName;
    private String namespaceName;
    private String userId;
    private String requestId;

    private EventSourceType eventSourceType;
    private String eventSourceName;
    private EventName eventName;

    private Object payload;

    @Override
    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    @Override
    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public EventSourceType getEventSourceType() {
        return eventSourceType;
    }

    public void setEventSourceType(EventSourceType eventSourceType) {
        this.eventSourceType = eventSourceType;
    }

    public String getEventSourceName() {
        return eventSourceName;
    }

    public void setEventSourceName(String eventSourceName) {
        this.eventSourceName = eventSourceName;
    }

    public EventName getEventName() {
        return eventName;
    }

    public void setEventName(EventName eventName) {
        this.eventName = eventName;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }

    public static class Builder {
        private String clusterName;
        private String namespaceName;
        private String userId;
        private String requestId;

        private EventSourceType eventSourceType;
        private String eventSourceName;
        private EventName eventName;

        private Object payload;

        public Builder clusterName(String clusterName) {
            this.clusterName = clusterName;
            return this;
        }

        public Builder namespaceName(String namespaceName) {
            this.namespaceName = namespaceName;
            return this;
        }

        public Builder userId(String userId) {
            this.userId = userId;
            return this;
        }

        public Builder requestId(String requestId) {
            this.requestId = requestId;
            return this;
        }

        public Builder eventSourceType(EventSourceType eventSourceType) {
            this.eventSourceType = eventSourceType;
            return this;
        }

        public Builder eventSourceName(String eventSourceName) {
            this.eventSourceName = eventSourceName;
            return this;
        }

        public Builder eventName(EventName eventName) {
            this.eventName = eventName;
            return this;
        }

        public Builder payload(Object payload) {
            this.payload = payload;
            return this;
        }

        public EventErrorMessage build() {
            EventErrorMessage m = new EventErrorMessage();
            m.setClusterName(clusterName);
            m.setNamespaceName(namespaceName);
            m.setRequestId(requestId);
            m.setUserId(userId);
            m.setEventSourceType(eventSourceType);
            m.setEventSourceName(eventSourceName);
            m.setEventName(eventName);
            m.setPayload(payload == null ? "" : payload);
            return m;
        }
    }

    @Override
    public String toString() {
        return "EventErrorMessage{" +
                "clusterName='" + clusterName + '\'' +
                ", namespaceName='" + namespaceName + '\'' +
                ", userId='" + userId + '\'' +
                ", requestId='" + requestId + '\'' +
                ", eventSourceType=" + eventSourceType +
                ", eventSourceName='" + eventSourceName + '\'' +
                ", eventName=" + eventName +
                ", payload=" + payload +
                '}';
    }
}