package cc.backend.event;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/14.
 */
public interface IStatusChecker<T, S> {

    /**
     * start a delay job to check status
     *
     * @param clusterName   the cluster name
     * @param namespaceName the namespace name
     * @param resource      the resource name
     */
    void start(String clusterName, String namespaceName, T resource);

    /**
     * cancel a delay job
     *
     * @param clusterName   the cluster name
     * @param namespaceName the namespace name
     * @param resource      the resource name
     */
    void cancel(String clusterName, String namespaceName, T resource);

    /**
     * check status immediately
     *
     * @param clusterName   the cluster name
     * @param namespaceName the namespace name
     * @param resource      the resource name
     * @return the resource status
     */
    S check(String clusterName, String namespaceName, T resource);

    /**
     * check a resource status, when status is not a final status, will set the status to a final status
     * final status include: XXX_FAILED, XXX_SUCCESS
     *
     * @param clusterName   the cluster name
     * @param namespaceName the namespace name
     * @param resource      the resource name
     */
    void finalCheck(String clusterName, String namespaceName, T resource);
}
