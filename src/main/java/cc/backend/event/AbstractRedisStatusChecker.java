package cc.backend.event;

import cc.backend.EnnProperties;
import cc.backend.audit.request.EnnContext;
import cc.backend.event.data.*;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/14.
 */
public abstract class AbstractRedisStatusChecker<T, S> implements IStatusChecker<T, S>,
        EventErrorMessageConsumer.Observer {
    @Inject
    private EventRepository eventRepository;

    @Inject
    private EventSourceRepository eventSourceRepository;

    @Inject
    private EventErrorMessageConsumer eventErrorMessageConsumer;

    @Inject
    protected EnnProperties properties;

    @Inject
    private RedisTemplate<String, String> template;

    private ScheduledExecutorService executorService;

    @PostConstruct
    public void init() {
        executorService = Executors.newScheduledThreadPool(5);
        eventErrorMessageConsumer.register(this);
    }

    @Override
    public void start(String clusterName,String namespaceName, T resource) {
        executorService.schedule(() -> finalCheck(clusterName, namespaceName, resource),
                getStatusCheckPeriodMillis(), TimeUnit.MILLISECONDS);

        template.opsForValue().set(composeRedisKey(clusterName, namespaceName, resource), "true",
                getStatusCheckPeriodMillis(), TimeUnit.MILLISECONDS);
    }

    /**
     * compose a redis key for check is running
     *
     * @param clusterName   the cluster name
     * @param namespaceName the namespace name
     * @param resource      the resource name
     * @return the redis key
     */
    public abstract String composeRedisKey(String clusterName, String namespaceName, T resource);

    /**
     * get the status check delay millis
     *
     * @return the delay millis
     */
    public abstract long getStatusCheckPeriodMillis();

    @Override
    public void finalCheck(String clusterName, String namespaceName, T resource) {
        EnnContext.setClusterName(clusterName);
        doFinalCheck(clusterName, namespaceName, resource);
        cancel(clusterName, namespaceName, resource);
    }

    /**
     * do the final check
     *
     * @param clusterName   the cluster name
     * @param namespaceName the namespace name
     * @param resource      the resource name
     */
    protected abstract void doFinalCheck(String clusterName, String namespaceName, T resource);

    @Override
    public void onError(EventErrorMessage message) {
        EnnContext.setContext(message);
        if (message.getEventSourceType() == getEventSourceType() && shouldHandleError(message)) {
            handleError(message);
        }
    }

    /**
     * handle the error from other server
     *
     * @param message the error message
     */
    public abstract void handleError(EventErrorMessage message);

    /**
     * judge should handle the error message
     *
     * @param message the error message
     * @return true is should, false is should not
     */
    public abstract boolean shouldHandleError(EventErrorMessage message);

    /**
     * get the event source type
     *
     * @return the event source type
     */
    public abstract EventSourceType getEventSourceType();

    @Override
    public void cancel(String clusterName, String namespaceName, T resource) {
        template.delete(composeRedisKey(clusterName, namespaceName, resource));
    }

    protected boolean isRunning(String clusterName, String namespaceName, T resource) {
        return template.hasKey(composeRedisKey(clusterName, namespaceName, resource));
    }

    public EventSource findEventSource(String namespaceName,
                                       EventSourceType sourceType,
                                       String sourceName) {
        return eventSourceRepository
                .findTopByNamespaceNameAndSourceTypeAndSourceNameOrderByIdDesc(namespaceName,
                        sourceType, sourceName);
    }

    public List<Event> findEvents(EventSource eventSource) {
        return eventRepository.findBySourceId(eventSource.getId());
    }

    public List<Event> findEvents(String namespaceName,
                                  EventSourceType sourceType,
                                  String sourceName) {
        EventSource eventSource = findEventSource(namespaceName, sourceType, sourceName);
        return eventRepository.findBySourceId(eventSource.getId());
    }

    @PreDestroy
    public void destroy() {
        executorService.shutdown();
        eventErrorMessageConsumer.unRegister(this);
    }
}
