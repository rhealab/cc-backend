package cc.backend.event;

import cc.backend.audit.request.EnnContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static cc.backend.RabbitConfig.EVENT_ERROR_Q;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/15.
 */
@Component
public class EventErrorMessageConsumer {
    private static final Logger logger = LoggerFactory.getLogger(EventErrorMessageConsumer.class);

    private List<Observer> observers = Collections.synchronizedList(new ArrayList<>());

    @RabbitListener(queues = EVENT_ERROR_Q)
    public void onEventError(EventErrorMessage message) {
        logger.info("RabbitMQ: received message - q={}, message={}", EVENT_ERROR_Q, message);
        EnnContext.setContext(message);
        onReceive(message);
    }

    private void onReceive(EventErrorMessage message) {
        for (Observer observer : observers) {
            observer.onError(message);
        }
    }

    public void register(Observer observer) {
        observers.add(observer);
    }

    public void unRegister(Observer observer) {
        observers.remove(observer);
    }

    public interface Observer {
        /**
         * on receive a error message
         *
         * @param message the error message
         */
        void onError(EventErrorMessage message);
    }

    @PreDestroy
    private void onDestroy() {
        observers.clear();
    }
}
