package cc.backend.event.data;

import cc.backend.common.GsonFactory;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/2.
 */
@Entity
public class Event<T> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Enumerated(EnumType.STRING)
    private EventName eventName;

    private long sourceId;

    @Column(name = "payload", length = 2000)
    private String payloadJson;

    @Transient
    private T payload;

    @CreationTimestamp
    private Date createdOn;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public EventName getEventName() {
        return eventName;
    }

    public void setEventName(EventName eventName) {
        this.eventName = eventName;
    }

    public long getSourceId() {
        return sourceId;
    }

    public void setSourceId(long sourceId) {
        this.sourceId = sourceId;
    }

    public String getPayloadJson() {
        return payloadJson;
    }

    public void setPayloadJson(String payloadJson) {
        this.payloadJson = payloadJson;
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public static class Builder<T> {
        private EventName eventName;
        private long sourceId;
        private T payload;

        public Builder<T> eventName(EventName eventName) {
            this.eventName = eventName;
            return this;
        }

        public Builder<T> sourceId(long sourceId) {
            this.sourceId = sourceId;
            return this;
        }

        public Builder<T> payload(T payload) {
            this.payload = payload;
            return this;
        }

        public Event<T> build() {
            Event<T> m = new Event<>();
            m.setEventName(eventName);
            m.setSourceId(sourceId);
            m.setPayload(payload);
            m.setPayloadJson(GsonFactory.getGson().toJson(payload));
            return m;
        }
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", eventName=" + eventName +
                ", sourceId=" + sourceId +
                ", payloadJson='" + payloadJson + '\'' +
                ", payload=" + payload +
                ", createdOn=" + createdOn +
                '}';
    }
}
