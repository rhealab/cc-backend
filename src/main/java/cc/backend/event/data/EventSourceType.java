package cc.backend.event.data;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/2.
 */
public enum EventSourceType {

    /**
     * namespace create type
     */
    NS_CREATE,


    /**
     * namespace delete type
     */
    NS_DELETE,


    /**
     * namespace update type
     */
    NS_UPDATE,


    /**
     * storage create type
     */
    STORAGE_CREATE,


    /**
     * storage update type
     */
    STORAGE_UPDATE,


    /**
     * storage delete type
     */
    STORAGE_DELETE
}