package cc.backend.event.data;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/2.
 */
public interface EventRepository extends JpaRepository<Event, Long> {

    /**
     * find a event source's all events by source id.
     *
     * @param sourceId the source id
     * @return the event list
     */
    List<Event> findBySourceId(long sourceId);

    /**
     * find a event by source id and the event name
     *
     * @param sourceId  the source id
     * @param eventName the event name
     * @return the event
     */
    Event findBySourceIdAndEventName(long sourceId, EventName eventName);
}