package cc.backend.event.data;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/2.
 */
public interface EventSourceRepository extends JpaRepository<EventSource, Long> {
    /**
     * find a source's newest event source
     *
     * @param namespaceName the namespace name
     * @param sourceType    the source type
     * @param sourceName    the source name
     * @param <T>           the source object
     * @return the event source
     */
    <T> EventSource<T> findTopByNamespaceNameAndSourceTypeAndSourceNameOrderByIdDesc(String namespaceName,
                                                                                     EventSourceType sourceType,
                                                                                     String sourceName);
}