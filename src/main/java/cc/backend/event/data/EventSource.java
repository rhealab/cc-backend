package cc.backend.event.data;

import cc.backend.audit.request.EnnContext;
import cc.backend.common.GsonFactory;

import javax.persistence.*;
import java.util.Date;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/12.
 */
@Entity
public class EventSource<T> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String requestId;
    private String userId;
    private String namespaceName;

    @Enumerated(EnumType.STRING)
    private EventSourceType sourceType;
    private String sourceName;

    @Column(name = "payload", length = 2000)
    private String payloadJson;

    @Transient
    private T payload;

    private Date createdOn;
    private String createdBy;

    @PrePersist
    public void onCreate() {
        createdBy = EnnContext.getUserId();
        createdOn = new Date();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    public EventSourceType getSourceType() {
        return sourceType;
    }

    public void setSourceType(EventSourceType sourceType) {
        this.sourceType = sourceType;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getPayloadJson() {
        return payloadJson;
    }

    public void setPayloadJson(String payloadJson) {
        this.payloadJson = payloadJson;
    }

    @Transient
    public T getPayload() {
        return payload;
    }

    @Transient
    public void setPayload(T payload) {
        this.payload = payload;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public static class Builder<T> {
        private String requestId;
        private String userId;
        private String namespaceName;
        private EventSourceType sourceType;
        private String sourceName;
        private T payload;

        public Builder<T> requestId(String requestId) {
            this.requestId = requestId;
            return this;
        }

        public Builder<T> userId(String userId) {
            this.userId = userId;
            return this;
        }

        public Builder<T> namespaceName(String namespaceName) {
            this.namespaceName = namespaceName;
            return this;
        }

        public Builder<T> sourceType(EventSourceType sourceType) {
            this.sourceType = sourceType;
            return this;
        }

        public Builder<T> sourceName(String sourceName) {
            this.sourceName = sourceName;
            return this;
        }

        public Builder<T> payload(T payload) {
            this.payload = payload;
            return this;
        }

        public EventSource<T> build() {
            EventSource<T> m = new EventSource<>();
            m.setRequestId(requestId != null ? requestId : EnnContext.getRequestId());
            m.setUserId(userId != null ? userId : EnnContext.getUserId());
            m.setNamespaceName(namespaceName != null ? namespaceName : EnnContext.getNamespaceName());
            m.setSourceType(sourceType);
            m.setSourceName(sourceName);
            m.setPayload(payload);
            m.setPayloadJson(GsonFactory.getGson().toJson(payload));
            return m;
        }
    }

    @Override
    public String toString() {
        return "EventSource{" +
                "id=" + id +
                ", requestId='" + requestId + '\'' +
                ", userId='" + userId + '\'' +
                ", namespaceName='" + namespaceName + '\'' +
                ", sourceType=" + sourceType +
                ", sourceName='" + sourceName + '\'' +
                ", payloadJson='" + payloadJson + '\'' +
                ", payload=" + payload +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                '}';
    }
}
