package cc.backend.event.processor;

import cc.backend.event.data.*;

import javax.inject.Inject;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/6.
 */
public abstract class AbstractProcessor<C, U, D> implements ICreateProcessor<C>,
        IUpdateProcessor<U>, IDeleteProcessor<D> {

    @Inject
    private EventRepository eventRepository;

    @Inject
    private EventSourceRepository eventSourceRepository;

    /**
     * ====== create =======
     *
     * @param eventSource the event source
     */
    @Override
    public void create(EventSource<C> eventSource) {
        doCreate(eventSource);
        saveEvent(composeCreateEvent(eventSource));

        if (getNextCreateProcessor() != null) {
            getNextCreateProcessor().create(eventSource);
        }
    }

    protected <T> Event<T> composeCreateEvent(EventSource<T> eventSource) {
        return composeEvent(eventSource, getCreateEventName());
    }

    @Override
    public EventName getCreateEventName() {
        return null;
    }

    @Override
    public ICreateProcessor<C> getNextCreateProcessor() {
        return null;
    }


    /**
     * ====== update =======
     *
     * @param eventSource the event source
     */
    @Override
    public void update(EventSource<U> eventSource) {
        if (needToUpdate(eventSource)) {
            doUpdate(eventSource);
            saveEvent(composeUpdateEvent(eventSource));
        }

        if (getNextUpdateProcessor() != null) {
            getNextUpdateProcessor().update(eventSource);
        }
    }

    protected <T> Event<T> composeUpdateEvent(EventSource<T> eventSource) {
        return composeEvent(eventSource, getUpdateEventName());
    }

    @Override
    public boolean needToUpdate(EventSource<U> eventSource) {
        return true;
    }

    @Override
    public EventName getUpdateEventName() {
        return null;
    }

    @Override
    public IUpdateProcessor<U> getNextUpdateProcessor() {
        return null;
    }

    /**
     * ====== delete =======
     *
     * @param eventSource the event source
     */
    @Override
    public void delete(EventSource<D> eventSource) {
        if (needToDelete(eventSource)) {
            doDelete(eventSource);
            saveEvent(composeDeleteEvent(eventSource));
        }

        if (getNextDeleteProcessor() != null) {
            getNextDeleteProcessor().delete(eventSource);
        }
    }

    protected <T> Event<T> composeDeleteEvent(EventSource<T> eventSource) {
        return composeEvent(eventSource, getDeleteEventName());
    }

    @Override
    public boolean needToDelete(EventSource<D> eventSource) {
        return false;
    }

    protected boolean isCreateEventExists(EventSource eventSource, EventSourceType sourceType) {
        return getEvent(eventSource.getNamespaceName(),
                sourceType,
                eventSource.getSourceName(),
                getCreateEventName()) != null;
    }

    @Override
    public EventName getDeleteEventName() {
        return null;
    }

    @Override
    public IDeleteProcessor<D> getNextDeleteProcessor() {
        return null;
    }


    protected void doCreate(EventSource<C> eventSource) {
    }

    protected void doUpdate(EventSource<U> eventSource) {
    }

    protected void doDelete(EventSource<D> eventSource) {
    }


    public void saveEvent(Event event) {
        eventRepository.save(event);
    }

    public Event getEvent(String namespaceName,
                          EventSourceType sourceType,
                          String sourceName,
                          EventName eventName) {
        EventSource<Object> nsCreateEventSource = eventSourceRepository
                .findTopByNamespaceNameAndSourceTypeAndSourceNameOrderByIdDesc(namespaceName,
                        sourceType, sourceName);
        return eventRepository.findBySourceIdAndEventName(nsCreateEventSource.getId(), eventName);
    }

    static <T> Event<T> composeEvent(EventSource<T> eventSource, EventName eventName) {
        return new Event.Builder<T>()
                .eventName(eventName)
                .sourceId(eventSource.getId())
                .payload(eventSource.getPayload())
                .build();
    }
}
