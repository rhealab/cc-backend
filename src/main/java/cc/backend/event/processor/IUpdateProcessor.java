package cc.backend.event.processor;

import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/6.
 */
public interface IUpdateProcessor<T> {
    /**
     * update a event
     *
     * @param eventSource the event source
     */
    void update(EventSource<T> eventSource);

    /**
     * get the event name
     *
     * @return event name
     */
    EventName getUpdateEventName();

    /**
     * should to update the event
     *
     * @param eventSource the event source
     * @return should update or not
     */
    boolean needToUpdate(EventSource<T> eventSource);

    /**
     * get the next processor
     *
     * @return the next processor or null
     */
    IUpdateProcessor<T> getNextUpdateProcessor();
}
