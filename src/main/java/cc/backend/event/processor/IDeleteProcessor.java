package cc.backend.event.processor;

import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/6.
 */
public interface IDeleteProcessor<T> {
    /**
     * delete a event
     *
     * @param eventSource the event source
     */
    void delete(EventSource<T> eventSource);

    /**
     * get the event name
     *
     * @return event name
     */
    EventName getDeleteEventName();

    /**
     * get the next processor
     *
     * @return the next processor or null
     */
    IDeleteProcessor<T> getNextDeleteProcessor();

    /**
     * should to delete the event
     *
     * @param eventSource the event source
     * @return should delete or not
     */
    boolean needToDelete(EventSource<T> eventSource);
}
