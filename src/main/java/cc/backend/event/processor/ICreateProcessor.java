package cc.backend.event.processor;

import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/6.
 */
public interface ICreateProcessor<T> {
    /**
     * create a event
     *
     * @param eventSource the event source
     */
    void create(EventSource<T> eventSource);

    /**
     * get the event name
     *
     * @return event name
     */
    EventName getCreateEventName();

    /**
     * get the next processor
     *
     * @return the next processor or null
     */
    ICreateProcessor<T> getNextCreateProcessor();
}
