package cc.backend;

import cc.keystone.client.KeystoneAutoConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2016/12/12.
 */
@SpringBootApplication
@EnableAspectJAutoProxy
@Import(KeystoneAutoConfig.class)
public class BackendApplication implements CommandLineRunner {
    private static final Logger logger = LoggerFactory.getLogger(BackendApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(BackendApplication.class, args);
    }

    @Override
    public void run(String... args) {
        logger.debug("Starting BackendApplication ...");
    }
}