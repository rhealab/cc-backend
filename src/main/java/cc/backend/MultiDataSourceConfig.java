package cc.backend;

import cc.backend.datasource.hibernate.CurrentTenantIdentifierResolverImpl;
import cc.backend.datasource.hibernate.DataSourceBasedMultiTenantConnectionProviderImpl;
import org.hibernate.MultiTenancyStrategy;
import org.hibernate.cfg.Environment;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.autoconfigure.transaction.TransactionManagerCustomizers;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.jta.JtaTransactionManager;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/7.
 */
//@Configuration
//@EnableConfigurationProperties({EnnProperties.class, JpaProperties.class})
public class MultiDataSourceConfig extends HibernateJpaAutoConfiguration {

    @Inject
    private EnnProperties properties;

    @Inject
    private CurrentTenantIdentifierResolverImpl resolver;

    @Inject
    private DataSourceBasedMultiTenantConnectionProviderImpl provider;

    public MultiDataSourceConfig(DataSource dataSource,
                                 JpaProperties jpaProperties,
                                 ObjectProvider<JtaTransactionManager> jtaTransactionManager,
                                 ObjectProvider<TransactionManagerCustomizers> transactionManagerCustomizers) {
        super(dataSource, jpaProperties, jtaTransactionManager, transactionManagerCustomizers);
    }

    @Override
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder factoryBuilder) {
        Map<String, Object> vendorProperties = getVendorProperties();
        customizeVendorProperties(vendorProperties);
        vendorProperties.put(Environment.MULTI_TENANT, MultiTenancyStrategy.DATABASE);
        vendorProperties.put(Environment.MULTI_TENANT_CONNECTION_PROVIDER, provider);
        vendorProperties.put(Environment.MULTI_TENANT_IDENTIFIER_RESOLVER, resolver);
        return factoryBuilder
                .dataSource(provider.getDefaultDataSource())
                .packages(getPackagesToScan())
                .properties(vendorProperties)
                .jta(isJta())
                .build();
    }

    @Bean
    public CurrentTenantIdentifierResolverImpl currentTenantIdentifierResolver() {
        return new CurrentTenantIdentifierResolverImpl();
    }

    @Bean(name = "multiTenantProvider")
    @RefreshScope
    public DataSourceBasedMultiTenantConnectionProviderImpl dataSourceBasedMultiTenantConnectionProvider()
            throws SQLException {
        return new DataSourceBasedMultiTenantConnectionProviderImpl(initAllDataSource());
    }

    private Map<String, DataSource> initAllDataSource() throws SQLException {
        Map<String, EnnProperties.Hibernate> hibernate = properties.getHibernate();
        Map<String, DataSource> map = new HashMap<>(hibernate.size());
        for (Map.Entry<String, EnnProperties.Hibernate> entry : hibernate.entrySet()) {
            if (map.put(entry.getKey(), getDataSource(entry.getValue())) != null) {
                throw new IllegalStateException("Duplicate key");
            }
        }
        return map;
    }

    private DataSource getDataSource(EnnProperties.Hibernate properties) throws SQLException {
        initDB(properties);
        return DataSourceBuilder
                .create()
                .url(properties.getUrl())
                .username(properties.getUsername())
                .password(properties.getPassword())
                .driverClassName(properties.getDriverClassName())
                .build();
    }

    private void initDB(EnnProperties.Hibernate properties) throws SQLException {
        java.util.Properties info = new java.util.Properties();
        info.setProperty("user", properties.getUsername());
        info.setProperty("password", properties.getPassword());
        Connection connection = DriverManager.getConnection(properties.getBaseUrl(), info);
        Statement stmt = connection.createStatement();
        stmt.executeUpdate("CREATE DATABASE IF NOT EXISTS " + properties.getDb());
    }
}
