package cc.backend.retcode;

import cc.lib.retcode.config.ReturnCodeNameConstants;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/8/1.
 */
public class BackendReturnCodeNameConstants extends ReturnCodeNameConstants {
    public static final String KUBERNETES_SERVER_ERROR = "kubernetes_server_error";
    public static final String KUBERNETES_RESOURCE_NOT_EXISTS = "kubernetes_resource_not_exists";
    public static final String KUBERNETES_RESOURCE_ALREADY_EXISTS = "kubernetes_resource_already_exists";
    public static final String KUBERNETES_RESOURCE_FIELD_ERROR = "kubernetes_resource_field_error";

    public static final String OPENTSDB_SERVER_ERROR = "opentsdb_server_error";

    public static final String NAMESPACE_NAME_NOT_VALID = "namespace_name_not_valid";

    public static final String NAMESPACE_CPU_REQUESTS_NOT_VALID = "namespace_cpu_requests_not_valid";
    public static final String NAMESPACE_CPU_LIMITS_NOT_VALID = "namespace_cpu_limits_not_valid";
    public static final String NAMESPACE_CPU_REQUESTS_MORE_THAN_LIMITS = "namespace_cpu_requests_more_than_limits";

    public static final String NAMESPACE_MEMORY_REQUESTS_BYTES_NOT_VALID = "namespace_memory_requests_bytes_not_valid";
    public static final String NAMESPACE_MEMORY_LIMITS_BYTES_NOT_VALID = "namespace_memory_limits_bytes_not_valid";
    public static final String NAMESPACE_MEMORY_REQUESTS_MORE_THAN_LIMITS = "namespace_memory_requests_more_than_limits";

    public static final String NAMESPACE_HOSTPATH_STORAGE_BYTES_NOT_VALID = "namespace_hostpath_storage_bytes_not_valid";
    public static final String NAMESPACE_CEPH_RBD_STORAGE_BYTES_NOT_VALID = "namespace_ceph_rbd_storage_bytes_not_valid";
    public static final String NAMESPACE_EBS_STORAGE_BYTES_NOT_VALID = "namespace_ebs_storage_bytes_not_valid";

    public static final String NAMESPACE_ALREADY_EXISTS = "namespace_already_exists";
    public static final String NAMESPACE_NOT_EXISTS = "namespace_not_exists";

    public static final String NAMESPACE_STATUS_NOT_SUPPORT_GET = "namespace_status_not_support_get";
    public static final String NAMESPACE_STATUS_NOT_SUPPORT_UPDATE = "namespace_status_not_support_update";
    public static final String NAMESPACE_STATUS_NOT_SUPPORT_DELETE = "namespace_status_not_support_delete";

    public static final String NAMESPACE_CPU_REQUESTS_TOO_HIGH = "namespace_cpu_requests_too_high";
    public static final String NAMESPACE_MEMORY_REQUESTS_TOO_HIGH = "namespace_memory_requests_too_high";
    public static final String NAMESPACE_HOSTPATH_STORAGE_TOO_HIGH = "namespace_hostpath_storage_too_high";
    public static final String NAMESPACE_CEPH_RBD_STORAGE_TOO_HIGH = "namespace_ceph_rbd_storage_too_high";

    public static final String NAMESPACE_CEPH_RBD_STORAGE_TOO_LOW = "namespace_ceph_rbd_storage_too_low";
    public static final String NAMESPACE_HOSTPATH_STORAGE_TOO_LOW = "namespace_hostpath_storage_too_low";
    public static final String NAMESPACE_EBS_STORAGE_TOO_LOW = "namespace_ebs_storage_too_low";

    public static final String NAMESPACE_MEMORY_REQUESTS_TOO_LOW = "namespace_memory_requests_too_low";
    public static final String NAMESPACE_MEMORY_LIMITS_TOO_LOW = "namespace_memory_limits_too_low";
    public static final String NAMESPACE_CPU_REQUESTS_TOO_LOW = "namespace_cpu_requests_too_low";
    public static final String NAMESPACE_CPU_LIMITS_TOO_LOW = "namespace_cpu_limits_too_low";

    public static final String NAMESPACE_DELETE_ERROR = "namespace_delete_error";

    //role grant
    public static final String NO_ACCESS_TO_UPDATE_ROLE = "no_access_to_update_role";
    public static final String ROLE_ASSIGNMENT_ALREADY_EXISTS = "role_assignment_already_exists";
    public static final String ROLE_ASSIGNMENT_USER_NOT_EXISTS = "role_assignment_user_not_exists";
    public static final String ROLE_ASSIGNMENT_NOT_EXISTS = "role_assignment_not_exists";
    public static final String LEGACY_NAMESPACE_NO_QUOTA = "legacy_namespace_no_quota";

    public static final String APP_NOT_EXISTS = "app_not_exists";
    public static final String APP_ALREADY_EXISTS = "app_already_exists";

    public static final String NAMESPACE_NOT_ALLOW_CRITICAL_POD = "namespace_not_allow_critical_pod";

    public static final String DEPLOYMENT_NOT_ALLOW_CRITICAL_POD = "deployment_not_allow_critical_pod";
    public static final String DEPLOYMENT_NOT_EXISTS_IN_APP = "deployment_not_exists_in_app";
    public static final String DEPLOYMENT_PVC_NAME_NOT_MATCHED_STORAGE = "deployment_pvc_name_not_matched_storage";
    public static final String DEPLOYMENT_CEPH_RBD_STORAGE_OVER_QUOTA = "deployment_ceph_rbd_storage_over_quota";
    public static final String DEPLOYMENT_HOSTPATH_STORAGE_OVER_QUOTA = "deployment_hostpath_storage_over_quota";
    public static final String DEPLOYMENT_ALREADY_EXISTS = "deployment_already_exists";
    public static final String DEPLOYMENT_CPU_REQUESTS_MORE_THAN_LIMITS = "deployment_cpu_requests_more_than_limits";
    public static final String DEPLOYMENT_MEMORY_REQUESTS_MORE_THAN_LIMITS = "deployment_memory_requests_more_than_limits";

    public static final String FORM_DEPLOYMENT_STORAGE_ALREADY_EXISTS = "form_deployment_storage_already_exists";
    public static final String FORM_DEPLOYMENT_STORAGE_NOT_EXISTS = "form_deployment_storage_not_exists";
    public static final String FORM_DEPLOYMENT_STORAGE_TYPE_NOT_SUPPORT = "form_deployment_storage_type_not_support";
    public static final String FORM_DEPLOYMENT_CEPH_RBD_STORAGE_OVER_QUOTA = "form_deployment_ceph_rbd_storage_over_quota";
    public static final String FORM_DEPLOYMENT_HOSTPATH_STORAGE_OVER_QUOTA = "form_deployment_hostpath_storage_over_quota";

    public static final String NODE_NOT_EXISTS = "node_not_exists";

    public static final String SERVICE_NOT_EXISTS_IN_APP = "service_not_exists_in_app";
    public static final String SERVICE_ALREADY_EXISTS = "service_already_exists";

    public static final String STORAGE_WITH_ID_NOT_EXISTS = "storage_with_id_not_exists";
    public static final String STORAGE_WITH_NAME_NOT_EXISTS = "storage_with_name_not_exists";
    public static final String STORAGE_ALREADY_EXISTS = "storage_already_exists";
    public static final String STORAGE_TYPE_NOT_SUPPORT = "storage_type_not_support";
    public static final String STORAGE_TYPE_NOT_SUPPORT_UPDATE = "storage_type_not_support_update";
    public static final String STORAGE_OVER_QUOTA = "storage_over_quota";
    public static final String STORAGE_QUOTA_TOO_LOW = "storage_quota_too_low";
    public static final String STORAGE_IN_USE = "storage_in_use";
    public static final String STORAGE_STATUS_NOT_SUPPORT_DELETE = "storage_status_not_support_delete";
    public static final String STORAGE_STATUS_NOT_SUPPORT_UPDATE = "storage_status_not_support_update";
    public static final String HOSTPATH_STORAGE_UPDATE_UNSHARED_NOT_SUPPORT = "hostpath_storage_update_unshared_not_support";

    public static final String STATEFULSET_NOT_EXISTS_IN_APP = "statefulset_not_exists_in_app";
    public static final String STATEFULSET_NOT_SUPPORTED = "statefulset_not_supported";
    public static final String STATEFULSET_ALREADY_EXISTS = "statefulset_already_exists";
    public static final String STATEFULSET_CPU_REQUESTS_MORE_THAN_LIMITS = "statefulset_cpu_requests_more_than_limits";
    public static final String STATEFULSET_MEMORY_REQUESTS_MORE_THAN_LIMITS = "statefulset_memory_requests_more_than_limits";
    public static final String STATEFULSET_PVC_NAME_NOT_MATCHED_STORAGE = "statefulset_pvc_name_not_matched_storage";
    public static final String STATEFULSET_CEPH_RBD_STORAGE_OVER_QUOTA = "statefulset_ceph_rbd_storage_over_quota";
    public static final String STATEFULSET_HOSTPATH_STORAGE_OVER_QUOTA = "statefulset_hostpath_storage_over_quota";

    public static final String FORM_STATEFULSET_STORAGE_NOT_EXISTS = "form_statefulset_storage_not_exists";
    public static final String FORM_STATEFULSET_STORAGE_ALREADY_EXISTS = "form_statefulset_storage_already_exists";
    public static final String FORM_STATEFULSET_HOSTPATH_STORAGE_OVER_QUOTA = "form_statefulset_hostpath_storage_over_quota";
    public static final String FORM_STATEFULSET_CEPH_RBD_STORAGE_OVER_QUOTA = "form_statefulset_ceph_rbd_storage_over_quota";

    public static final String STORAGECLASS_CREATE_ERROR = "storageclass_create_error";
    public static final String STORAGECLASS_DELETE_ERROR = "storageclass_delete_error";

    public static final String SEND_STORAGE_CREATE_MESSAGE_ERROR = "send_storage_create_message_error";
    public static final String SEND_INIT_NFS_MESSAGE_ERROR = "send_init_nfs_message_error";

    public static final String CREATE_OKHTTPCLIENT_ERROR = "create_okhttpclient_error";

    public static final String RENDER_FREEMARKER_TEMPLATE_ERROR = "render_freemarker_template_error";

    public static final String CONVERT_STATEFULSET_TO_JSON_ERROR = "convert_statefulset_to_json_error";
    public static final String CONVERT_STORAGECLASS_TO_JSON_ERROR = "convert_storageclass_to_json_error";

    public static final String DEPLOYMENT_CPU_LIMITS_OVER_NAMESPACE_QUOTA = "deployment_cpu_limits_over_namespace_quota";
    public static final String DEPLOYMENT_CPU_REQUESTS_OVER_NAMESPACE_QUOTA = "deployment_cpu_requests_over_namespace_quota";
    public static final String DEPLOYMENT_MEMORY_LIMITS_OVER_NAMESPACE_QUOTA = "deployment_memory_limits_over_namespace_quota";
    public static final String DEPLOYMENT_MEMORY_REQUESTS_OVER_NAMESPACE_QUOTA = "deployment_memory_requests_over_namespace_quota";
    public static final String STATEFULSET_CPU_LIMITS_OVER_NAMESPACE_QUOTA = "statefulset_cpu_limits_over_namespace_quota";
    public static final String STATEFULSET_CPU_REQUESTS_OVER_NAMESPACE_QUOTA = "statefulset_cpu_requests_over_namespace_quota";
    public static final String STATEFULSET_MEMORY_LIMITS_OVER_NAMESPACE_QUOTA = "statefulset_memory_limits_over_namespace_quota";
    public static final String STATEFULSET_MEMORY_REQUESTS_OVER_NAMESPACE_QUOTA = "statefulset_memory_requests_over_namespace_quota";


    public static final String FORM_STATEFULSET_STORAGE_TYPE_NOT_SUPPORT = "form_statefulset_storage_type_not_support";

    public static final String EVENT_NOT_EXISTS = "event_not_exists";

    public static final String SAME_KEY_DIFFERENT_VALUE_ERROR = "same_key_different_value_error";

    public static final String DEPENDENCY_CHECKER_SERVICE_ERROR = "dependency_checker_service_error";

    public static final String DEPENDENCY_WRAPPER_UNMARSHALING_ERROR = "dependency_wrapper_unmarshaling_error";

    public static final String MARSHALING_TO_JSON_ERROR = "marshaling_to_json_error";

    public static final String MIN_CPU_LARGER_THAN_DEFAULT_REQUEST_CPU = "min_cpu_larger_than_default_request_cpu";
    public static final String DEFAULT_REQUEST_CPU_LARGER_THAN_DEFAULT_LIMIT_CPU = "default_request_cpu_larger_than_default_limit_cpu";
    public static final String MIN_MEMORY_LARGER_THAN_DEFAULT_REQUEST_MEMORY = "min_memory_larger_than_default_request_memory";
    public static final String DEFAULT_REQUEST_MEMORY_LARGER_THAN_DEFAULT_LIMIT_MEMORY = "default_request_memory_larger_than_default_limit_memory";
    public static final String NODE_PORT_CONFLICT = "node_port_conflict";
    public static final String EXTERNAL_IP_CONFLICT = "external_ip_conflict";
    public static final String NODE_PORT_NOT_IN_RIGHT_RANGE = "node_port_not_in_right_range";
    public static final String RANDOM_NODEPORT_MAX_ATTEMPTS = "random_nodeport_max_attempts";


    public static final String EXTERNAL_LINK_NOT_EXISTS = "external_link_not_exists";
    public static final String EXTERNAL_LINK_ALREADY_EXISTS = "external_link_already_exists";

    public static final String ALI_CLOUD_NODE_SCALE_CONFIG_SERVER_ERROR = "ali_cloud_node_scale_config_server_error";
    public static final String ALI_CLOUD_NODE_SCALE_CONFIG_NOT_VALID = "ali_cloud_node_scale_config_not_valid";
    public static final String AWS_EBS_STORAGE_CAN_NOT_CHANGE_SMALL = "aws_ebs_storage_can_not_change_small";

    public static final String SCHEDULING_SERVER_ERROR = "scheduling_server_error";

    public static final String CONTAINER_CPU_NOT_VALID = "container_cpu_not_valid";
    public static final String CONTAINER_MEMORY_NOT_VALID = "container_memory_not_valid";
    public static final String STORAGE_CAN_NOT_CHANGE_SMALL = "storage_can_not_change_small";

    public static final String CLUSTER_NOT_EXISTS = "cluster_not_exists";

    public static final String DATABASE_CANNOT_CONN = "database_cannot_connected";

    public static final String UNKNOWN_INGRESS_CLASS = "unknown_ingress_class";
    public static final String INVALID_HOST = "invalid_host";
    public static final String INGRESS_NAME_CAN_NOT_BE_EMPTY = "ingress_name_can_not_be_empty";
    public static final String INGRESS_ALREADY_EXISTS = "ingress_already_exists";
}
