package cc.backend.retcode;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/3/30.
 */
public enum ReturnCodeResourceType {
    /**
     * the return code resource type
     */
    NAMESPACE("namespace"),
    APP("app"),
    SECRET("secret"),
    SERVICE("service"),
    DEPLOYMENT("deployment"),
    DEPLOYMENT_MIXTURE("deploymentMixture"),
    STATEFUL_SET("statefulSet"),
    STATEFUL_SET_MIXTURE("statefulSetMixture"),
    NODE("node"),
    STORAGE("storage");

    String name;

    ReturnCodeResourceType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
