package cc.backend.kubernetes;

import cc.backend.EnnProperties;
import cc.backend.audit.request.EnnContext;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/28.
 */
@Component
@RefreshScope
public class AwsClientManager {

    @Inject
    private EnnProperties properties;

    private Map<String, AmazonEC2> ec2ClientMap;

    @PostConstruct
    private void init() {
        ec2ClientMap = properties.getClusters().entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> createClient(entry.getValue().getEbs())));
    }

    public AmazonEC2 createClient(EnnProperties.Cluster.Ebs ebs) {
        AWSCredentials credentials = new BasicAWSCredentials(ebs.getAccessKey(), ebs.getSecretKey());

        return AmazonEC2ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(ebs.getRegion())
                .build();
    }

    public AmazonEC2 getEC2Client() {
        return getEC2Client(EnnContext.getClusterName());
    }

    public AmazonEC2 getEC2Client(String clusterName) {
        return ec2ClientMap.get(clusterName);
    }
}
