package cc.backend.kubernetes.namespaces;

import cc.backend.kubernetes.namespaces.services.NamespaceNamesManagement;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/22.
 */
@Component
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes/namespaces_name")
@Api(value = "Namespace", description = "Operation about namespace.", produces = "application/json")
public class NamespaceNamesEndpoint {

    @Inject
    private NamespaceNamesManagement management;

    @GET
    @ApiOperation(value = "List my owned namespaces name.(will return all namespace names when no auth)",
            responseContainer = "List", response = String.class)
    public Response getNamespaceNames() {
        return Response.ok(management.getNamespaceNames()).build();
    }
}
