package cc.backend.kubernetes.namespaces;

import cc.backend.audit.request.EnnContext;
import cc.backend.common.NamespaceStatusNotCheck;
import cc.backend.common.utils.ThreadUtils;
import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.event.data.EventSource;
import cc.backend.kubernetes.namespaces.dto.NamespaceCreateRequest;
import cc.backend.kubernetes.namespaces.dto.RoleAssignmentMaintainResponse;
import cc.backend.kubernetes.namespaces.services.SysAdminNamespacesManagement;
import cc.backend.kubernetes.namespaces.validator.NamespaceValidator;
import cc.lib.retcode.ReturnCode;
import cc.lib.retcode.ReturnCodeResponseBuilder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/5/12.
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("sys/kubernetes/namespaces")
@Api(value = "Namespace", description = "Operation about namespace.", produces = "application/json")
public class SysAdminNamespacesEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(SysAdminNamespacesEndpoint.class);

    @Inject
    private SysAdminNamespacesManagement management;

    @Inject
    private NamespaceValidator validator;

    @POST
    @ApiOperation(value = "Create namespace and assign namespace admin at the same time.", response = RoleAssignmentMaintainResponse.class)
    public Response createNamespace(NamespaceCreateRequest request) {
        validator.validateNamespaceParams(request);
        validator.validateNamespaceShouldNotExists(request.getName());

        //TODO validate cluster resources is enough or not

        EventSource<NamespaceCreateRequest> eventSource = management.initNamespaceForCreate(request);
        String userId = EnnContext.getUserId();
        String requestId = EnnContext.getRequestId();
        String namespace = EnnContext.getNamespaceName();
        String cluster = EnnContext.getClusterName();
        ThreadUtils.getCachedThreadPool().submit(() -> {
            EnnContext.setUserId(userId);
            EnnContext.setRequestId(requestId);
            EnnContext.setNamespaceName(namespace);
            EnnContext.setClusterName(cluster);
            management.createNamespace(request, eventSource);
        });

        return Response.ok().build();
    }

    @DELETE
    @Path("{namespaceName}")
    @NamespaceStatusNotCheck
    @ApiOperation(value = "Delete the namespace", response = ReturnCode.class)
    public Response deleteNamespace(@PathParam("namespaceName") String namespaceName) {
        boolean isLegacy = validator.validateNamespaceStatusForDelete(namespaceName);
        boolean deleted = true;
        if (!isLegacy) {
            EventSource<String> eventSource = management.initNamespaceForDelete(namespaceName);

            String userId = EnnContext.getUserId();
            String requestId = EnnContext.getRequestId();
            String namespace = EnnContext.getNamespaceName();
            String cluster = EnnContext.getClusterName();
            ThreadUtils.getCachedThreadPool().submit(() -> {
                EnnContext.setUserId(userId);
                EnnContext.setRequestId(requestId);
                EnnContext.setNamespaceName(namespace);
                EnnContext.setClusterName(cluster);
                management.deleteNamespace(namespaceName, eventSource);
            });
        } else {
            validator.validateNamespaceShouldExists(namespaceName);
            deleted = management.deleteLegacyNamespace(namespaceName);
        }

        if (!deleted) {
            return new ReturnCodeResponseBuilder().codeName(BackendReturnCodeNameConstants.NAMESPACE_DELETE_ERROR).build();
        } else {
            return new ReturnCodeResponseBuilder().codeName(BackendReturnCodeNameConstants.OK).build();
        }
    }
}
