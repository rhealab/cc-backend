package cc.backend.kubernetes.namespaces.messages;

import java.io.Serializable;

/**
 * @author wangchunyang@gmail.com
 */
public class CephUserDeletedMessage implements Serializable, IMessage {
    private String clusterName;
    private String namespaceName;
    private String userId;
    private String requestId;

    @Override
    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    @Override
    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @Override
    public String toString() {
        return "CephUserDeletedMessage{" +
                "clusterName='" + clusterName + '\'' +
                ", namespaceName='" + namespaceName + '\'' +
                ", userId='" + userId + '\'' +
                ", requestId='" + requestId + '\'' +
                '}';
    }

    public static CephUserDeletedMessage from(NamespaceDeletedMessage message) {
        CephUserDeletedMessage cephUserDeletedMessage = new CephUserDeletedMessage();
        cephUserDeletedMessage.setClusterName(message.getClusterName());
        cephUserDeletedMessage.setNamespaceName(message.getNamespaceName());
        cephUserDeletedMessage.setRequestId(message.getRequestId());
        cephUserDeletedMessage.setUserId(message.getUserId());

        return cephUserDeletedMessage;
    }
}
