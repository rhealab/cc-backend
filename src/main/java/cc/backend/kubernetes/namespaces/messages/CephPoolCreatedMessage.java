package cc.backend.kubernetes.namespaces.messages;

import java.io.Serializable;

/**
 * @author wangchunyang@gmail.com
 */
public class CephPoolCreatedMessage implements Serializable, IMessage {
    private String clusterName;
    private String namespaceName;
    private long rbdAmountBytes;
    private String userId;
    private String requestId;

    @Override
    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    @Override
    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    public long getRbdAmountBytes() {
        return rbdAmountBytes;
    }

    public void setRbdAmountBytes(long rbdAmountBytes) {
        this.rbdAmountBytes = rbdAmountBytes;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @Override
    public String toString() {
        return "CephPoolCreatedMessage{" +
                "clusterName='" + clusterName + '\'' +
                ", namespaceName='" + namespaceName + '\'' +
                ", rbdAmountBytes=" + rbdAmountBytes +
                ", userId='" + userId + '\'' +
                ", requestId='" + requestId + '\'' +
                '}';
    }
}