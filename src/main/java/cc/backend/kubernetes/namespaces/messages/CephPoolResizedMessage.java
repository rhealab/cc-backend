package cc.backend.kubernetes.namespaces.messages;

import java.io.Serializable;

/**
 * @author wangchunyang@gmail.com
 */
public class CephPoolResizedMessage implements Serializable, IMessage {
    private String clusterName;
    private String namespaceName;
    private String userId;
    private String requestId;
    private long rbdAmountBytes;

    @Override
    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    @Override
    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public long getRbdAmountBytes() {
        return rbdAmountBytes;
    }

    public void setRbdAmountBytes(long rbdAmountBytes) {
        this.rbdAmountBytes = rbdAmountBytes;
    }

    @Override
    public String toString() {
        return "CephPoolResizedMessage{" +
                "clusterName='" + clusterName + '\'' +
                ", namespaceName='" + namespaceName + '\'' +
                ", userId='" + userId + '\'' +
                ", requestId='" + requestId + '\'' +
                ", rbdAmountBytes=" + rbdAmountBytes +
                '}';
    }

    public static CephPoolResizedMessage from(NamespaceResizeMessage resizeMessage) {
        CephPoolResizedMessage resizedMessage = new CephPoolResizedMessage();
        resizedMessage.setClusterName(resizeMessage.getClusterName());
        resizedMessage.setNamespaceName(resizeMessage.getNamespaceName());
        resizedMessage.setRequestId(resizeMessage.getRequestId());
        resizedMessage.setUserId(resizeMessage.getUserId());
        resizedMessage.setRbdAmountBytes(resizeMessage.getRbdAmountBytes());

        return resizedMessage;
    }
}