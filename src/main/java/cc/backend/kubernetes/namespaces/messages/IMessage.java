package cc.backend.kubernetes.namespaces.messages;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/6.
 */
public interface IMessage {
    /**
     * get message's user id
     *
     * @return the user id
     */
    String getUserId();

    /**
     * get message's request id
     *
     * @return the request id
     */
    String getRequestId();

    /**
     * get message's namespace name
     *
     * @return the namespace name
     */
    String getNamespaceName();

    /**
     * get message's cluster name
     *
     * @return the cluster name
     */
    String getClusterName();
}
