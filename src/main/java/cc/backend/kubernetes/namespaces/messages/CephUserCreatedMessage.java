package cc.backend.kubernetes.namespaces.messages;

import java.io.Serializable;

/**
 * @author wangchunyang@gmail.com
 */
public class CephUserCreatedMessage implements Serializable, IMessage {
    private String userId;
    private String requestId;
    private String clusterName;
    private String namespaceName;
    private String username;
    private String poolName;
    private String keyring;

    @Override
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @Override
    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    @Override
    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPoolName() {
        return poolName;
    }

    public void setPoolName(String poolName) {
        this.poolName = poolName;
    }

    public String getKeyring() {
        return keyring;
    }

    public void setKeyring(String keyring) {
        this.keyring = keyring;
    }

    public CephUserCreatedMessage namespace(String namespaceName) {
        this.namespaceName = namespaceName;
        return this;
    }

    public CephUserCreatedMessage user(String username) {
        this.username = username;
        return this;
    }

    public CephUserCreatedMessage pool(String poolName) {
        this.poolName = poolName;
        return this;
    }

    public CephUserCreatedMessage keyring(String keyring) {
        this.keyring = keyring;
        return this;
    }

    @Override
    public String toString() {
        return "CephUserCreatedMessage{" +
                "userId='" + userId + '\'' +
                ", requestId='" + requestId + '\'' +
                ", clusterName='" + clusterName + '\'' +
                ", namespaceName='" + namespaceName + '\'' +
                ", username='" + username + '\'' +
                ", poolName='" + poolName + '\'' +
                ", keyring='" + keyring + '\'' +
                '}';
    }
}
