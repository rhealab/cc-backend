package cc.backend.kubernetes.namespaces.validator;

import cc.backend.EnnProperties;
import cc.backend.audit.request.EnnContext;
import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.kubernetes.Constants;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.namespaces.data.NamespaceExtras;
import cc.backend.kubernetes.namespaces.data.NamespaceExtrasRepository;
import cc.backend.kubernetes.namespaces.data.NamespaceStatus;
import cc.backend.kubernetes.namespaces.dto.NamespaceCreateRequest;
import cc.backend.kubernetes.node.dto.NodeSumDto;
import cc.backend.kubernetes.node.services.NodesManagement;
import cc.keystone.client.KeystoneProjectManagement;
import cc.keystone.client.domain.KeystoneProject;
import cc.lib.retcode.CcException;
import io.fabric8.kubernetes.api.model.Namespace;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

import static cc.backend.kubernetes.Constants.NAMESPACE_MIN_CPU;
import static cc.backend.kubernetes.Constants.NAMESPACE_MIN_MEMORY_BYTES;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/5/16.
 */
@Component
public class NamespaceValidator {

    private static final Logger logger = LoggerFactory.getLogger(NamespaceValidator.class);

    @Inject
    private KeystoneProjectManagement keystoneProjectManagement;

    @Inject
    private KubernetesClientManager clientManager;

    @Inject
    private NamespaceExtrasRepository namespaceExtrasRepository;

    @Inject
    private NodesManagement nodesManagement;

    @Inject
    protected EnnProperties properties;

    public void validateNamespaceParams(NamespaceCreateRequest request) {
        if (request.getName() == null || !request.getName().matches(Constants.RESOURCE_NAME_PATTERN)) {
            throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_NAME_NOT_VALID);
        }

        if (request.getCpuLimit() < NAMESPACE_MIN_CPU) {
            throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_CPU_LIMITS_NOT_VALID);
        }

        if (request.getCpuRequest() < NAMESPACE_MIN_CPU) {
            throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_CPU_REQUESTS_NOT_VALID);
        }

        if (request.getCpuLimit() < request.getCpuRequest()) {
            throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_CPU_REQUESTS_MORE_THAN_LIMITS);
        }

        if (request.getMemoryLimitBytes() < NAMESPACE_MIN_CPU) {
            throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_MEMORY_LIMITS_BYTES_NOT_VALID);
        }

        if (request.getMemoryRequestBytes() < NAMESPACE_MIN_CPU) {
            throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_MEMORY_REQUESTS_BYTES_NOT_VALID);
        }

        if (request.getMemoryLimitBytes() < request.getMemoryRequestBytes()) {
            throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_MEMORY_REQUESTS_MORE_THAN_LIMITS);
        }

        if (request.getHostPathStorageBytes() < NAMESPACE_MIN_MEMORY_BYTES) {
            throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_HOSTPATH_STORAGE_BYTES_NOT_VALID);
        }

        if (properties.getCurrentCeph().isEnabled() && request.getPoolBytes() < NAMESPACE_MIN_MEMORY_BYTES) {
            throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_CEPH_RBD_STORAGE_BYTES_NOT_VALID);
        }

        if (properties.getCurrentEbs().isEnabled()) {
            if (request.getEbsBytes() == null || request.getEbsBytes() < NAMESPACE_MIN_MEMORY_BYTES) {
                throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_EBS_STORAGE_BYTES_NOT_VALID);
            }
        }
    }

    public void validateNamespaceShouldNotExists(String namespaceName) {
        List<String> systemNamespaces = properties.getCurrentKubernetes().getSystemNamespaces();
        if (systemNamespaces.stream().anyMatch(name -> name.equals(namespaceName))) {
            throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_ALREADY_EXISTS);
        }

        NamespaceExtras namespaceExtras =
                namespaceExtrasRepository.findByNameAndDeletedIsFalse(namespaceName);
        if (namespaceExtras != null) {
            throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_ALREADY_EXISTS);
        }

        Namespace namespace = clientManager.getClient()
                .namespaces()
                .withName(namespaceName)
                .get();
        if (namespace != null) {
            throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_ALREADY_EXISTS);
        }
    }

    public void validateNamespaceQuotaForCreate(NamespaceCreateRequest request) {
        NodeSumDto nodeSum = nodesManagement.getNodeSum();
        if (request.getCpuRequest() > nodeSum.getCpuSum() - nodeSum.getCpuRequests()) {
            throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_CPU_REQUESTS_TOO_HIGH);
        }

        if (request.getMemoryRequestBytes() > nodeSum.getMemorySumBytes() - nodeSum.getMemoryRequestsBytes()) {
            throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_MEMORY_REQUESTS_TOO_HIGH);
        }

        if (request.getHostPathStorageBytes() > nodeSum.getHostPathSumBytes() - nodeSum.getHostPathRequestsBytes()) {
            throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_HOSTPATH_STORAGE_TOO_HIGH);
        }

        if (request.getPoolBytes() > nodeSum.getRbdSumBytes() - nodeSum.getRbdRequestsBytes()) {
            throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_CEPH_RBD_STORAGE_TOO_HIGH);
        }
    }

    public void validateNamespaceShouldExists(String namespaceName) {
        List<String> systemNamespaces = properties.getCurrentKubernetes().getSystemNamespaces();
        if (systemNamespaces.stream().anyMatch(name -> name.equals(namespaceName))) {
            throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_NOT_EXISTS);
        }

        if (properties.isAuthEnabled()) {
            KeystoneProject project = keystoneProjectManagement.getProject(EnnContext.getClusterName(), namespaceName);
            if (project == null) {
                throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_NOT_EXISTS);
            }
        } else {
            Namespace namespace = clientManager.getClient().namespaces().withName(namespaceName).get();
            if (namespace == null) {
                throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_NOT_EXISTS);
            }
        }
    }

    /**
     * validate namespace status is support delete or not
     *
     * @param namespaceName the namespace for delete
     * @return true if namespace is legacy, false if not a legacy namespace
     */
    public boolean validateNamespaceStatusForDelete(String namespaceName) {
        NamespaceExtras namespaceExtras =
                namespaceExtrasRepository.findByNameAndDeletedIsFalse(namespaceName);
        if (namespaceExtras == null) {
            return true;
        }

        if (namespaceExtras.getStatus() != NamespaceStatus.CREATE_SUCCESS
                && namespaceExtras.getStatus() != NamespaceStatus.UPDATE_SUCCESS
                && namespaceExtras.getStatus() != NamespaceStatus.UPDATE_FAILED
                && namespaceExtras.getStatus() != NamespaceStatus.CREATE_FAILED) {
            throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_STATUS_NOT_SUPPORT_DELETE);
        }
        return false;
    }

    public void validateNamespaceStatus(String namespaceName) {
        NamespaceExtras namespaceExtras =
                namespaceExtrasRepository.findByNameAndDeletedIsFalse(namespaceName);
        if (namespaceExtras != null) {
            if (namespaceExtras.getStatus() != NamespaceStatus.CREATE_SUCCESS
                    && namespaceExtras.getStatus() != NamespaceStatus.UPDATE_SUCCESS
                    && namespaceExtras.getStatus() != NamespaceStatus.UPDATE_PENDING
                    && namespaceExtras.getStatus() != NamespaceStatus.UPDATE_FAILED) {
                throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_STATUS_NOT_SUPPORT_GET);
            }
        } else {
            validateNamespaceShouldExists(namespaceName);
        }
    }

    public boolean isLegacy(String namespaceName) {
        return namespaceExtrasRepository.findByNameAndDeletedIsFalse(namespaceName) == null;
    }

    public boolean isLegacy(NamespaceExtras extras) {
        return extras == null;
    }
}
