package cc.backend.kubernetes.namespaces.validator;

import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.namespaces.data.NamespaceExtras;
import cc.backend.kubernetes.namespaces.data.NamespaceExtrasRepository;
import cc.backend.kubernetes.namespaces.data.NamespaceStatus;
import cc.backend.kubernetes.namespaces.dto.NamespaceNaming;
import cc.backend.kubernetes.namespaces.dto.NamespaceUpdateRequest;
import cc.backend.kubernetes.storage.StorageRepository;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageType;
import cc.backend.kubernetes.utils.UnitUtils;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.Quantity;
import io.fabric8.kubernetes.api.model.ResourceQuota;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cc.backend.kubernetes.Constants.*;
import static cc.backend.kubernetes.storage.domain.StorageType.*;
import static cc.backend.kubernetes.utils.UnitUtils.parseNamespaceStorageQuota;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/2/24.
 */
@Component
public class NamespaceStatsValidator {
    @Inject
    protected KubernetesClientManager clientManager;

    @Inject
    private StorageRepository storageRepository;

    @Inject
    protected NamespaceExtrasRepository namespaceExtrasRepository;

    public ResourceQuota validateNamespaceForUpdate(NamespaceUpdateRequest request) {
        String namespaceName = request.getName();
        String quotaName = NamespaceNaming.quotaName(namespaceName);

        NamespaceExtras namespaceExtras =
                namespaceExtrasRepository.findByNameAndDeletedIsFalse(namespaceName);

        validateNamespaceStatusForUpdate(namespaceExtras);

        if (namespaceExtras != null) {
            validateStorageQuota(namespaceExtras, request);
        }

        ResourceQuota quota = clientManager.getClient()
                .resourceQuotas()
                .inNamespace(namespaceName)
                .withName(quotaName)
                .get();

        if (quota == null) {
            return null;
        }
        validateCpuQuota(namespaceName, quota, request);
        validateMemoryQuota(namespaceName, quota, request);
        return quota;
    }

    public void validateNamespaceStatusForUpdate(NamespaceExtras namespaceExtras) {
        if (namespaceExtras.getStatus() == NamespaceStatus.UPDATE_PENDING) {
            throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_STATUS_NOT_SUPPORT_UPDATE,
                    ImmutableMap.of("namespace", namespaceExtras.getName(),
                            "status", namespaceExtras.getStatus()));
        }
    }

    /**
     * @param namespaceExtras the namespace extras
     * @param request         the namespace update request
     */
    protected void validateStorageQuota(NamespaceExtras namespaceExtras,
                                        NamespaceUpdateRequest request) {
        Long newRbdBytes = request.getCephPoolBytes();
        Long newHostpathBytes = request.getHostPathStorageBytes();
        Long newEbsBytes = request.getEbsBytes();
        if (newRbdBytes == null && newHostpathBytes == null && newEbsBytes == null) {
            return;
        }

        long currentRbdBytes = namespaceExtras.getPoolBytes();
        if (newRbdBytes != null && newRbdBytes < currentRbdBytes) {
            long reservedBytes = getReservedBytes(namespaceExtras.getName(), RBD);
            if (reservedBytes > newRbdBytes) {
                throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_CEPH_RBD_STORAGE_TOO_LOW,
                        ImmutableMap.of("namespace", namespaceExtras.getName(),
                                "reservedBytes", reservedBytes,
                                "newBytes", newRbdBytes));
            }
        }

        long currentHostpathBytes = namespaceExtras.getHostpathStorageBytes();
        if (newHostpathBytes != null && newHostpathBytes < currentHostpathBytes) {
            long reservedHostpathBytes = getReservedBytes(namespaceExtras.getName(), HostPath);
            if (reservedHostpathBytes > newHostpathBytes) {
                throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_HOSTPATH_STORAGE_TOO_LOW,
                        ImmutableMap.of("namespace", namespaceExtras.getName(),
                                "reservedBytes", reservedHostpathBytes,
                                "newBytes", newHostpathBytes));
            }
        }


        long currentEbsBytes = namespaceExtras.getEbsBytes();
        if (newEbsBytes != null && newEbsBytes < currentEbsBytes) {
            long reservedEbsBytes = getReservedBytes(namespaceExtras.getName(), EBS);
            if (reservedEbsBytes > newEbsBytes) {
                throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_EBS_STORAGE_TOO_LOW,
                        ImmutableMap.of("namespace", namespaceExtras.getName(),
                                "reservedBytes", reservedEbsBytes,
                                "newBytes", newEbsBytes));
            }
        }
    }

    protected void validateCpuQuota(String namespaceName,
                                    ResourceQuota currentQuota,
                                    NamespaceUpdateRequest request) {
        Double newCpuLimits = request.getCpuLimit();
        Double newCpuRequests = request.getCpuRequest();
        if (newCpuLimits == null && newCpuRequests == null) {
            return;
        }

        Map<String, Quantity> currentMap = currentQuota.getSpec().getHard();
        double currentCpuLimits = UnitUtils.parseK8sCpuQuantity(currentMap.get(K_NAMESPACE_CPU_LIMITS));
        double currentCpuRequests = UnitUtils.parseK8sCpuQuantity(currentMap.get(K_NAMESPACE_CPU_REQUESTS));

        double cpuLimits = newCpuLimits == null ? currentCpuLimits : newCpuLimits;
        double cpuRequests = newCpuRequests == null ? currentCpuRequests : newCpuRequests;

        if (cpuLimits < cpuRequests) {
            throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_CPU_REQUESTS_MORE_THAN_LIMITS,
                    ImmutableMap.of("namespace", namespaceName,
                            "requests", cpuRequests,
                            "limits", cpuLimits));
        }

        Map<String, Quantity> usedMap = currentQuota.getStatus().getUsed();

        if (newCpuLimits != null && newCpuLimits < currentCpuLimits) {
            double reservedCpuLimits = UnitUtils.parseK8sCpuQuantity(usedMap.get(K_NAMESPACE_CPU_LIMITS));
            if (cpuLimits < reservedCpuLimits) {
                throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_CPU_LIMITS_TOO_LOW,
                        ImmutableMap.of("namespace", namespaceName,
                                "limits", newCpuLimits,
                                "reservedLimits", reservedCpuLimits));
            }
        }

        if (newCpuRequests != null && newCpuRequests < currentCpuRequests) {
            double reservedCpuRequests = UnitUtils.parseK8sCpuQuantity(usedMap.get(K_NAMESPACE_CPU_REQUESTS));
            if (cpuRequests < reservedCpuRequests) {
                throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_CPU_REQUESTS_TOO_LOW,
                        ImmutableMap.of("namespace", namespaceName,
                                "requests", newCpuRequests,
                                "reservedRequests", reservedCpuRequests));
            }
        }
    }

    protected void validateMemoryQuota(String namespaceName,
                                       ResourceQuota currentQuota,
                                       NamespaceUpdateRequest request) {
        Long newMemoryLimitBytes = request.getMemoryLimitBytes();
        Long newMemoryRequestBytes = request.getMemoryRequestBytes();
        if (newMemoryLimitBytes == null && newMemoryRequestBytes == null) {
            return;
        }

        Map<String, Quantity> currentMap = currentQuota.getSpec().getHard();
        long currentMemoryLimitBytes = UnitUtils.parseK8sMemoryQuantity(currentMap.get(K_NAMESPACE_MEMORY_LIMITS));
        long currentMemoryRequestsBytes = UnitUtils.parseK8sMemoryQuantity(currentMap.get(K_NAMESPACE_MEMORY_REQUESTS));

        long memoryLimitsBytes = newMemoryLimitBytes == null ? currentMemoryLimitBytes : newMemoryLimitBytes;
        long memoryRequestsBytes = newMemoryRequestBytes == null ? currentMemoryRequestsBytes : newMemoryRequestBytes;

        if (memoryLimitsBytes < memoryRequestsBytes) {
            throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_MEMORY_REQUESTS_MORE_THAN_LIMITS,
                    ImmutableMap.of("namespace", namespaceName,
                            "requestsBytes", memoryRequestsBytes,
                            "limitsBytes", memoryLimitsBytes));
        }

        Map<String, Quantity> usedMap = currentQuota.getStatus().getUsed();

        if (newMemoryLimitBytes != null && newMemoryLimitBytes < currentMemoryLimitBytes) {
            long reservedMemoryLimitsBytes = UnitUtils.parseK8sMemoryQuantity(usedMap.get(K_NAMESPACE_MEMORY_LIMITS));
            if (memoryLimitsBytes < reservedMemoryLimitsBytes) {
                throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_MEMORY_LIMITS_TOO_LOW,
                        ImmutableMap.of("namespace", namespaceName,
                                "limitsBytes", memoryLimitsBytes,
                                "reservedLimitsBytes", reservedMemoryLimitsBytes));
            }
        }

        if (newMemoryRequestBytes != null && newMemoryRequestBytes < currentMemoryRequestsBytes) {
            long reservedMemoryRequestsBytes = UnitUtils.parseK8sMemoryQuantity(usedMap.get(K_NAMESPACE_MEMORY_REQUESTS));
            if (memoryRequestsBytes < reservedMemoryRequestsBytes) {
                throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_MEMORY_REQUESTS_TOO_LOW,
                        ImmutableMap.of("namespace", namespaceName,
                                "requestsBytes", memoryRequestsBytes,
                                "reservedRequestsBytes", reservedMemoryRequestsBytes));
            }
        }
    }


    //TODO how to get hostpath reserved bytes ???
    protected long getReservedBytes(String namespaceName, StorageType storageType) {
        long reservedBytes = 0;
        List<Storage> storageList =
                storageRepository.findByNamespaceNameAndStorageType(namespaceName, storageType);
        for (Storage s : storageList) {
            reservedBytes += s.getAmountBytes();
        }

        return reservedBytes;
    }

    private void validateLegacyStorageQuota(String namespaceName,
                                            ResourceQuota currentQuota,
                                            NamespaceUpdateRequest request) {
        Long newRbdBytes = request.getCephPoolBytes();
        Long newHostpathBytes = request.getHostPathStorageBytes();
        if (newRbdBytes == null && newHostpathBytes == null) {
            return;
        }

        Map<String, String> labels = currentQuota.getMetadata().getLabels();

        if (newRbdBytes != null) {
            long currentRbdBytes = 0;
            if (labels != null && labels.get(NAMESPACE_QUOTA_RBD_STORAGE_LABEL) != null) {
                currentRbdBytes = parseNamespaceStorageQuota(labels.get(NAMESPACE_QUOTA_RBD_STORAGE_LABEL));
            }

            if (newRbdBytes < currentRbdBytes) {
                long reservedBytes = getReservedBytes(namespaceName, RBD);
                if (reservedBytes > newRbdBytes) {
                    throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_CEPH_RBD_STORAGE_TOO_LOW,
                            ImmutableMap.of("namespace", namespaceName,
                                    "reservedBytes", reservedBytes,
                                    "newBytes", newRbdBytes));
                }
            }
        }

        if (newHostpathBytes != null) {
            long currentHostpathBytes = 0;
            if (labels != null && labels.get(NAMESPACE_QUOTA_HOSTPATH_STORAGE_LABEL) != null) {
                currentHostpathBytes = parseNamespaceStorageQuota(labels.get(NAMESPACE_QUOTA_HOSTPATH_STORAGE_LABEL));
            }
            if (newHostpathBytes < currentHostpathBytes) {
                long reservedHostpathBytes = getReservedBytes(namespaceName, HostPath);
                if (reservedHostpathBytes > newHostpathBytes) {
                    throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_HOSTPATH_STORAGE_TOO_LOW,
                            ImmutableMap.of("namespace", namespaceName,
                                    "reservedBytes", reservedHostpathBytes,
                                    "newBytes", newHostpathBytes));
                }
            }
        }
    }

    public void validateQuotaForLegacyCreate(NamespaceUpdateRequest request) {
        Double cpuLimits = request.getCpuLimit();
        Double cpuRequests = request.getCpuRequest();
        Long memoryLimits = request.getMemoryLimitBytes();
        Long memoryRequests = request.getMemoryRequestBytes();
        Long hostPathStorageBytes = request.getHostPathStorageBytes();

        Map<String, String> errors = new HashMap<>();
        if (cpuLimits == null) {
            errors.put("cpuLimits", "cpuLimits can not null");
        }
        if (cpuRequests == null) {
            errors.put("cpuRequests", "cpuRequests can not null");
        }
        if (memoryLimits == null) {
            errors.put("memoryLimits", "memoryLimits can not null");
        }
        if (memoryRequests == null) {
            errors.put("memoryRequests", "memoryRequests can not null");
        }
        if (hostPathStorageBytes == null) {
            errors.put("hostPathStorageBytes", "hostPathStorageBytes can not null");
        }

        if (!errors.isEmpty()) {
            throw new CcException(BackendReturnCodeNameConstants.PROPERTY_INVALID,
                    ImmutableMap.of("namespace", request.getName(),
                            "detail", errors));
        }

        if (cpuLimits < cpuRequests) {
            throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_CPU_REQUESTS_MORE_THAN_LIMITS,
                    ImmutableMap.of("namespace", request.getName(),
                            "requests", cpuRequests,
                            "limits", cpuLimits));
        }

        if (memoryLimits < memoryRequests) {
            throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_MEMORY_REQUESTS_MORE_THAN_LIMITS,
                    ImmutableMap.of("namespace", request.getName(),
                            "requestsBytes", memoryRequests,
                            "limitsBytes", memoryLimits));
        }
    }
}
