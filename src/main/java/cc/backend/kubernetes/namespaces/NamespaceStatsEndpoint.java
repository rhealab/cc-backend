package cc.backend.kubernetes.namespaces;

import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.event.data.EventSource;
import cc.backend.kubernetes.namespaces.dto.MetricsDto;
import cc.backend.kubernetes.namespaces.dto.NamespaceStatsResponse;
import cc.backend.kubernetes.namespaces.dto.NamespaceUpdateRequest;
import cc.backend.kubernetes.namespaces.services.NamespaceMetricsManagement;
import cc.backend.kubernetes.namespaces.services.NamespaceStatsManagement;
import cc.backend.kubernetes.namespaces.validator.NamespaceStatsValidator;
import cc.backend.kubernetes.namespaces.validator.NamespaceValidator;
import cc.lib.retcode.CcException;
import io.fabric8.kubernetes.api.model.ResourceQuota;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/2/24.
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes/namespace_stats")
@Api(value = "NamespaceStats", description = "Operation about namespace statistics.", produces = "application/json")
public class NamespaceStatsEndpoint {

    @Inject
    private NamespaceStatsManagement statsManagement;

    @Inject
    private NamespaceMetricsManagement metricsManagement;

    @Inject
    private NamespaceValidator namespaceValidator;

    @Inject
    private NamespaceStatsValidator statsValidator;

    @GET
    @Path("{namespaceName}/new")
    @ApiOperation(value = "Get namespace resources statistics.", response = NamespaceStatsResponse.class)
    public Response getNamespaceStats(@PathParam("namespaceName") String namespaceName) {
        ResourceQuota resourceQuota =
                statsManagement.getNamespaceResourceQuota(namespaceName);
        if (resourceQuota == null) {
            throw new CcException(BackendReturnCodeNameConstants.LEGACY_NAMESPACE_NO_QUOTA);
        }
        return Response.ok(statsManagement.composeNamespaceStatsDto(resourceQuota)).build();
    }

    @GET
    @Path("{namespaceName}")
    @ApiOperation(value = "Get namespace resources statistics.", response = NamespaceStatsResponse.class)
    public Response getNamespaceStatsLegacy(@PathParam("namespaceName") String namespaceName) {
        ResourceQuota resourceQuota =
                statsManagement.getNamespaceResourceQuota(namespaceName);
        if (resourceQuota == null) {
            throw new CcException(BackendReturnCodeNameConstants.LEGACY_NAMESPACE_NO_QUOTA);
        }
        return Response.ok(statsManagement.composeNamespaceStatsDto(resourceQuota)).build();
    }

    @PUT
    @Path("{namespaceName}")
    @ApiOperation(value = "Update namespace quota")
    public Response updateNamespaceQuota(@PathParam("namespaceName") String namespaceName,
                                         @Valid NamespaceUpdateRequest request) {
        request.setName(namespaceName);
        if (request.getCephPoolBytes() == null) {
            request.setCephPoolBytes(request.getPoolBytes());
        }

        ResourceQuota originQuota = statsValidator.validateNamespaceForUpdate(request);
        if (originQuota == null) {
            /*
            TODO should remove this create logic
             */
            statsValidator.validateQuotaForLegacyCreate(request);
            statsManagement.createQuotaForLegacy(request);
        } else {
            if (namespaceValidator.isLegacy(namespaceName)) {
                statsManagement.updateLegacyNamespaceQuota(request, originQuota);
            } else {
                EventSource<NamespaceUpdateRequest> eventSource = statsManagement.initForUpdate(request);
                statsManagement.updateNamespaceQuota(request, eventSource, originQuota);
            }
        }
        return Response.ok().build();
    }

    @GET
    @Path("{namespaceName}/compute")
    @ApiOperation(value = "Get namespace compute resource current usage info.", responseContainer = "List"
            , response = MetricsDto.class)
    public Response getComputeStats(@PathParam("namespaceName") String namespaceName) {
        return Response.ok(metricsManagement.getComputeMetrics(namespaceName)).build();
    }

    @GET
    @Path("{namespaceName}/storage")
    @ApiOperation(value = "Get namespace storage resource current usage info.", responseContainer = "List",
            response = MetricsDto.class)
    public Response getStorageStats(@PathParam("namespaceName") String namespaceName) {
        return Response.ok(metricsManagement.getStorageMetrics(namespaceName)).build();
    }
}
