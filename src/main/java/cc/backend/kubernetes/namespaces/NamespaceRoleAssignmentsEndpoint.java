package cc.backend.kubernetes.namespaces;

import cc.backend.audit.request.EnnContext;
import cc.backend.kubernetes.namespaces.dto.RoleAssignmentMaintainRequest;
import cc.backend.kubernetes.namespaces.dto.RoleAssignmentMaintainResponse;
import cc.backend.kubernetes.namespaces.services.NamespaceRoleAssignmentManagement;
import cc.backend.role.assignment.RoleAssignmentDto;
import cc.keystone.client.domain.AssignInfo;
import cc.keystone.client.domain.AssignResult;
import cc.keystone.client.domain.RoleType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/2/24.
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes/namespaces")
@Api(value = "Namespace", description = "Operation about namespace.", produces = "application/json")
public class NamespaceRoleAssignmentsEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(NamespaceRoleAssignmentsEndpoint.class);

    @Inject
    private NamespaceRoleAssignmentManagement management;

    @POST
    @Path("{namespaceName}/role_assignments")
    @ApiOperation(value = "Update user role in namespace.", response = RoleAssignmentMaintainResponse.class)
    public Response nsUserRoleUpdate(@PathParam("namespaceName") String namespaceName,
                                     @Valid RoleAssignmentMaintainRequest request) {
        List<AssignInfo> updateList = request.getUpdateList();
        RoleAssignmentMaintainResponse response = new RoleAssignmentMaintainResponse();
        if (updateList != null) {
            List<AssignResult> results = management.updateNsUserRoles(namespaceName, updateList);
            response.setUpdateList(results);
        }
        return Response.ok(response).build();
    }

    @POST
    @Path("{namespaceName}/role_assignments/maintain")
    @ApiOperation(value = "maintain user role in namespace.", response = RoleAssignmentMaintainResponse.class)
    public Response nsUserRoleMaintain(@PathParam("namespaceName") String namespaceName,
                                       @Valid RoleAssignmentMaintainRequest request) {

        List<AssignInfo> addList = Optional.ofNullable(request.getAddList())
                .orElseGet(ArrayList::new);

        List<AssignInfo> updateList = Optional.ofNullable(request.getUpdateList())
                .orElseGet(ArrayList::new);

        List<AssignInfo> removeList = Optional.ofNullable(request.getRemoveList())
                .orElseGet(ArrayList::new);

        List<AssignResult> addResult = management.grantRoles(namespaceName, addList);
        List<AssignResult> updateResult = management.updateNsUserRoles(namespaceName, updateList);
        List<AssignResult> removeResult = management.revokeRoles(namespaceName, removeList);

        RoleAssignmentMaintainResponse response = new RoleAssignmentMaintainResponse();
        response.setAddList(addResult);
        response.setUpdateList(updateResult);
        response.setRemoveList(removeResult);

        return Response.ok(response).build();
    }

    @POST
    @Path("{namespaceName}/role_assignments/admin")
    @ApiOperation(value = "Assign/UnAssign ns admin in namespace.", response = RoleAssignmentMaintainResponse.class)
    public Response nsAdminMaintain(@PathParam("namespaceName") String namespaceName,
                                    @Valid RoleAssignmentMaintainRequest request) {
        List<AssignInfo> removeList = Optional.ofNullable(request.getRemoveList())
                .orElseGet(ArrayList::new)
                .stream()
                .peek(assignInfo -> assignInfo.setRole(RoleType.NS_ADMIN))
                .collect(Collectors.toList());

        List<AssignInfo> addList = Optional.ofNullable(request.getAddList())
                .orElseGet(ArrayList::new)
                .stream()
                .peek(assignInfo -> assignInfo.setRole(RoleType.NS_ADMIN))
                .collect(Collectors.toList());

        List<AssignResult> assignResults = management.grantRoles(namespaceName, addList);
        List<AssignResult> unAssignResults = management.revokeRoles(namespaceName, removeList);

        RoleAssignmentMaintainResponse response = new RoleAssignmentMaintainResponse();
        response.setAddList(assignResults);
        response.setRemoveList(unAssignResults);

        return Response.ok(response).build();
    }

    @POST
    @Path("{namespaceName}/role_assignments/developer")
    @ApiOperation(value = "Assign/UnAssign developer in namespace.", response = RoleAssignmentMaintainResponse.class)
    public Response developerMaintain(@PathParam("namespaceName") String namespaceName,
                                      @Valid RoleAssignmentMaintainRequest request) {
        List<AssignInfo> removeList = Optional.ofNullable(request.getRemoveList())
                .orElseGet(ArrayList::new)
                .stream()
                .peek(assignInfo -> assignInfo.setRole(RoleType.DEVELOPER))
                .collect(Collectors.toList());

        List<AssignInfo> addList = Optional.ofNullable(request.getAddList())
                .orElseGet(ArrayList::new)
                .stream()
                .peek(assignInfo -> assignInfo.setRole(RoleType.DEVELOPER))
                .collect(Collectors.toList());

        List<AssignResult> assignResults = management.grantRoles(namespaceName, addList);
        List<AssignResult> unAssignResults = management.revokeRoles(namespaceName, removeList);

        RoleAssignmentMaintainResponse response = new RoleAssignmentMaintainResponse();
        response.setAddList(assignResults);
        response.setRemoveList(unAssignResults);

        return Response.ok(response).build();
    }

    @GET
    @Path("{namespaceName}/users")
    @ApiOperation(value = "Get the namespace users with role.", responseContainer = "List",
            response = RoleAssignmentDto.class)
    public Response getNamespaceUsers(@PathParam("namespaceName") String namespaceName) {
        List<RoleAssignmentDto> dtos = management.getNamespaceRoleAssignment(namespaceName, EnnContext.getUserId());
        return Response.ok(dtos).build();
    }
}
