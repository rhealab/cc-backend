package cc.backend.kubernetes.namespaces.services;

import cc.backend.EnnProperties;
import cc.backend.RabbitConfig;
import cc.backend.audit.operation.OperationAuditMessage;
import cc.backend.audit.operation.OperationAuditMessageProducer;
import cc.backend.audit.operation.OperationType;
import cc.backend.audit.operation.ResourceType;
import cc.backend.audit.request.EnnContext;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceRepository;
import cc.backend.event.data.EventSourceType;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.namespaces.data.NamespaceExtras;
import cc.backend.kubernetes.namespaces.data.NamespaceExtrasRepository;
import cc.backend.kubernetes.namespaces.data.NamespaceStatus;
import cc.backend.kubernetes.namespaces.dto.NamespaceNaming;
import cc.backend.kubernetes.namespaces.dto.NamespaceStatsResponse;
import cc.backend.kubernetes.namespaces.dto.NamespaceUpdateRequest;
import cc.backend.kubernetes.namespaces.event.CephPoolProcessor;
import cc.backend.kubernetes.namespaces.event.NamespaceProcessor;
import cc.backend.kubernetes.namespaces.messages.CephPoolResizedMessage;
import cc.backend.kubernetes.namespaces.messages.NamespaceResizeMessage;
import cc.backend.kubernetes.namespaces.status.NamespaceUpdateStatusChecker;
import cc.backend.kubernetes.storage.HostPathStorageManager;
import cc.backend.kubernetes.storage.StorageRepository;
import cc.backend.kubernetes.storage.StorageWorkloadRepository;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageType;
import cc.backend.kubernetes.storage.domain.StorageWorkload;
import cc.backend.kubernetes.storage.usage.entity.*;
import cc.backend.kubernetes.utils.UnitUtils;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.*;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static cc.backend.kubernetes.Constants.*;
import static cc.backend.kubernetes.storage.domain.StorageType.*;
import static cc.backend.kubernetes.utils.UnitUtils.parseK8sCpuQuantity;
import static cc.backend.kubernetes.utils.UnitUtils.parseK8sMemoryQuantity;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/2/24.
 */
@Component
public class NamespaceStatsManagement {
    private static final Logger logger = LoggerFactory.getLogger(NamespaceStatsManagement.class);

    @Inject
    private RabbitMessagingTemplate messagingTemplate;

    @Inject
    protected KubernetesClientManager clientManager;

    @Inject
    protected CephImageUsageRepository imageUsageRepository;

    @Inject
    protected CephfsDirectoryUsageRepository cephfsDirectoryUsageRepository;

    @Inject
    private CephGlobalUsageRepository cephGlobalUsageRepository;

    @Inject
    protected NfsDirectoryUsageRepository nfsDirectoryUsageRepository;

    @Inject
    protected NfsGlobalUsageRepository nfsGlobalUsageRepository;

    @Inject
    private StorageRepository storageRepository;

    @Inject
    private HostPathStorageManager hostPathStorageManager;

    @Inject
    protected NamespaceExtrasRepository namespaceExtrasRepository;

    @Inject
    protected OperationAuditMessageProducer operationAuditMessageProducer;

    @Inject
    private CephPoolProcessor cephPoolProcessor;

    @Inject
    private EventSourceRepository eventSourceRepository;

    @Inject
    private NamespaceUpdateStatusChecker namespaceUpdateStatusChecker;

    @Inject
    private StorageWorkloadRepository storageWorkloadRepository;

    @Inject
    private EnnProperties properties;

    @Inject
    private NamespaceProcessor namespaceProcessor;

    public ResourceQuota getNamespaceResourceQuota(String namespaceName) {
        return clientManager.getClient()
                .resourceQuotas()
                .inNamespace(namespaceName)
                .withName(NamespaceNaming.quotaName(namespaceName))
                .get();
    }

    public NamespaceStatsResponse composeNamespaceStatsDto(ResourceQuota quota) {
        if (quota == null) {
            return null;
        }
        String namespaceName = quota.getMetadata().getNamespace();
        NamespaceStatsResponse.Builder builder = new NamespaceStatsResponse.Builder();

        Map<String, Quantity> hard = quota.getStatus().getHard();
        Map<String, Quantity> used = quota.getStatus().getUsed();

        builder.name(namespaceName)
                .cpuRequests(parseK8sCpuQuantity(hard.get(K_NAMESPACE_CPU_REQUESTS)))
                .cpuRequestsReserved(parseK8sCpuQuantity(used.get(K_NAMESPACE_CPU_REQUESTS)))
                .cpuLimits(parseK8sCpuQuantity(hard.get(K_NAMESPACE_CPU_LIMITS)))
                .cpuLimitsReserved(parseK8sCpuQuantity(used.get(K_NAMESPACE_CPU_LIMITS)))
                .memoryRequestsBytes(parseK8sMemoryQuantity(hard.get(K_NAMESPACE_MEMORY_REQUESTS)))
                .memoryRequestsReservedBytes(parseK8sMemoryQuantity(used.get(K_NAMESPACE_MEMORY_REQUESTS)))
                .memoryLimitsBytes(parseK8sMemoryQuantity(hard.get(K_NAMESPACE_MEMORY_LIMITS)))
                .memoryLimitsReservedBytes(parseK8sMemoryQuantity(used.get(K_NAMESPACE_MEMORY_LIMITS)));

        NamespaceExtras namespaceExtras =
                namespaceExtrasRepository.findByNameAndDeletedIsFalse(namespaceName);
        if (namespaceExtras != null) {
            builder.allowHostpath(namespaceExtras.getAllowHostpath());
            builder.allowPrivilege(namespaceExtras.getAllowPrivilege());
            builder.allowCriticalPod(namespaceExtras.getAllowCriticalPod());
            /*
            only normal namespace can have storage info
             */
            composeStorageInfo(builder, namespaceExtras);
        } else {
            builder.hostPathBytes(getNamespaceStorageTotalBytes(namespaceName, HostPath));
            builder.hostPathReservedBytes(getHostPathStorageReservedBytes(namespaceName));
            builder.hostPathUsedBytes(getHostpathStorageUsedBytes(namespaceName));
        }

        return builder.build();
    }

    private void composeStorageInfo(NamespaceStatsResponse.Builder builder,
                                    NamespaceExtras namespaceExtras) {

        long hostPathBytes = namespaceExtras.getHostpathStorageBytes();
        if (hostPathBytes > 0) {
            long hostPathReservedBytes = getHostPathStorageReservedBytes(namespaceExtras.getName());
            long hostpathUsedBytes = getHostpathStorageUsedBytes(namespaceExtras.getName());

            builder.hostPathBytes(hostPathBytes);
            builder.hostPathReservedBytes(hostPathReservedBytes);
            builder.hostPathUsedBytes(hostpathUsedBytes);
        }

        if (namespaceExtras.isCephInitialized()) {
            composeCephStorageInfo(builder, namespaceExtras.getName(), namespaceExtras.getPoolBytes());
        }

        if (namespaceExtras.isNfsInitialized()) {
            composeNfsStorageInfo(builder, namespaceExtras.getName());
        }

        composeEbsStorageInfo(builder, namespaceExtras);
    }

    protected void composeCephStorageInfo(NamespaceStatsResponse.Builder builder,
                                          String namespaceName,
                                          long cephPoolBytes) {
        if (cephPoolBytes > 0) {
            builder.cephPoolBytes(cephPoolBytes);

            List<CephImageUsage> imageUsageList =
                    imageUsageRepository.findByNamespaceName(namespaceName);
            long rbdUsedBytes = imageUsageList.stream()
                    .mapToLong(CephImageUsage::getBytesUsed)
                    .sum();
            builder.rbdReservedBytes(getStorageReservedBytes(namespaceName, RBD)
                    + getStorageReservedBytes(namespaceName, StorageClass));
            builder.rbdUsedBytes(rbdUsedBytes);

            CephGlobalUsage globalUsage = cephGlobalUsageRepository.findLatest();
            if (globalUsage != null) {
                builder.cephFsBytes(globalUsage.getCephfsTotalBytes());
            }

            CephfsDirectoryUsage cephfsNamespaceUsage =
                    cephfsDirectoryUsageRepository.findOne(namespaceName);
            if (cephfsNamespaceUsage != null) {
                builder.cephFsUsedBytes(cephfsNamespaceUsage.getBytesUsed());
            }
            builder.cephFsReservedBytes(getStorageReservedBytes(namespaceName, CephFS));
        }
    }

    protected void composeNfsStorageInfo(NamespaceStatsResponse.Builder builder,
                                         String namespaceName) {
        NfsDirectoryUsage nfsDirectoryUsage =
                nfsDirectoryUsageRepository.findOne(namespaceName);
        if (nfsDirectoryUsage != null) {
            builder.nfsUsedBytes(nfsDirectoryUsage.getBytesUsed());
        }

        NfsGlobalUsage globalUsage = nfsGlobalUsageRepository.findLatest();
        if (globalUsage != null) {
            builder.nfsBytes(globalUsage.getTotalBytes());
        }
    }

    protected void composeEbsStorageInfo(NamespaceStatsResponse.Builder builder,
                                         NamespaceExtras extras) {
        builder.ebsBytes(extras.getEbsBytes());
        builder.ebsUsedBytes(0L);
        builder.ebsReservedBytes(getStorageReservedBytes(extras.getName(), EBS));
        //TODO
    }

    @Transactional(rollbackFor = Exception.class)
    public EventSource<NamespaceUpdateRequest> initForUpdate(NamespaceUpdateRequest request) {
        String namespaceName = request.getName();

        EventSource<NamespaceUpdateRequest> eventSource = new EventSource.Builder<NamespaceUpdateRequest>()
                .namespaceName(namespaceName)
                .sourceType(EventSourceType.NS_UPDATE)
                .sourceName(namespaceName)
                .payload(request)
                .build();
        EventSource<NamespaceUpdateRequest> savedEventSource = eventSourceRepository.save(eventSource);
        namespaceExtrasRepository.updateStatusByNamespace(namespaceName,
                NamespaceStatus.UPDATE_PENDING, EnnContext.getUserId());
        namespaceUpdateStatusChecker.start(EnnContext.getClusterName(), namespaceName, namespaceName);
        return savedEventSource;
    }

    public void updateNamespaceQuota(NamespaceUpdateRequest request,
                                     EventSource<NamespaceUpdateRequest> eventSource,
                                     ResourceQuota originQuota) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        String namespaceName = request.getName();

        namespaceProcessor.update(eventSource);
        namespaceUpdateStatusChecker.check(EnnContext.getClusterName(), namespaceName, namespaceName);

        //TODO should move to a correct place
        if (request.getEbsBytes() != null) {
            namespaceExtrasRepository.updateEbsStorageQuotaByNamespace(namespaceName, request.getEbsBytes());
        }

        sendStorageResizeMessage(request);
        sendNamespaceUpdateAuditMessage(request, stopwatch, originQuota);
    }

    private void sendStorageResizeMessage(NamespaceUpdateRequest request) {
        if (!properties.getCurrentCeph().isEnabled() || request.getCephPoolBytes() == null) {
            return;
        }

        NamespaceResizeMessage message = new NamespaceResizeMessage();
        message.setClusterName(EnnContext.getClusterName());
        message.setNamespaceName(request.getName());
        message.setUserId(EnnContext.getUserId());
        message.setRequestId(EnnContext.getRequestId());
        message.setRbdAmountBytes(request.getCephPoolBytes());
        messagingTemplate.convertAndSend(RabbitConfig.NAMESPACE_RESIZE_Q, message);
    }

    private void sendNamespaceUpdateAuditMessage(NamespaceUpdateRequest request,
                                                 Stopwatch stopwatch,
                                                 ResourceQuota originQuota) {
        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        NamespaceExtras namespaceExtras =
                namespaceExtrasRepository.findByNameAndDeletedIsFalse(request.getName());
        String quotaName = NamespaceNaming.quotaName(request.getName());
        sendHostPathStorageUpdateAuditMessage(request, namespaceExtras, quotaName, elapsed);
        Map<String, Quantity> originHard = originQuota.getSpec().getHard();
        sendCpuUpdateAuditMessage(request, originHard, quotaName, elapsed);
        sendMemoryUpdateAuditMessage(request, originHard, quotaName, elapsed);
    }

    @RabbitListener(queues = RabbitConfig.CEPH_POOL_RESIZED_Q)
    public void onCephPoolResized(CephPoolResizedMessage message) {
        EnnContext.setContext(message);

        String clusterName = message.getClusterName();
        String namespaceName = message.getNamespaceName();
        logger.info("RabbitMQ: received CephPoolResizedMessage - namespace={}",
                clusterName, namespaceName);

        if (shouldHandleUpdateEvent(namespaceName)) {
            EventSource<CephPoolResizedMessage> eventSource = getNsUpdateEventSource(namespaceName, message);
            cephPoolProcessor.update(eventSource);
            namespaceUpdateStatusChecker.check(EnnContext.getClusterName(), namespaceName, namespaceName);
        }
    }

    private boolean shouldHandleUpdateEvent(String namespaceName) {
        NamespaceExtras namespaceExtras =
                namespaceExtrasRepository.findByNameAndDeletedIsFalse(namespaceName);
        return namespaceExtras != null && namespaceExtras.getStatus() == NamespaceStatus.UPDATE_PENDING;
    }

    private <T> EventSource<T> getNsUpdateEventSource(String namespaceName,
                                                      T payload) {
        EventSource<T> eventSource = eventSourceRepository
                .findTopByNamespaceNameAndSourceTypeAndSourceNameOrderByIdDesc(namespaceName,
                        EventSourceType.NS_UPDATE, namespaceName);
        eventSource.setPayload(payload);
        return eventSource;
    }

    protected void sendMemoryUpdateAuditMessage(NamespaceUpdateRequest request,
                                                Map<String, Quantity> originHard,
                                                String quotaName,
                                                long elapsed) {
        Map<String, String> memoryExtras = new HashMap<>(4);

        Long newMemoryLimitBytes = request.getMemoryLimitBytes();
        long oldMemoryLimitsBytes = UnitUtils.parseK8sMemoryQuantity(originHard.get(K_NAMESPACE_MEMORY_LIMITS));
        if (newMemoryLimitBytes != null && newMemoryLimitBytes != oldMemoryLimitsBytes) {
            memoryExtras.put("newLimitsBytes", String.valueOf(newMemoryLimitBytes));
            memoryExtras.put("oldLimitsBytes", String.valueOf(oldMemoryLimitsBytes));
        }

        Long newMemoryRequestBytes = request.getMemoryRequestBytes();
        long oldMemoryRequestsBytes = UnitUtils.parseK8sMemoryQuantity(originHard.get(K_NAMESPACE_MEMORY_REQUESTS));
        if (newMemoryRequestBytes != null && newMemoryRequestBytes != oldMemoryRequestsBytes) {
            memoryExtras.put("newRequestsBytes", String.valueOf(newMemoryRequestBytes));
            memoryExtras.put("oldRequestsBytes", String.valueOf(oldMemoryRequestsBytes));
        }

        if (!memoryExtras.isEmpty()) {
            OperationAuditMessage message = new OperationAuditMessage.Builder()
                    .elapsed(elapsed)
                    .namespaceName(request.getName())
                    .operationType(OperationType.UPDATE)
                    .resourceType(ResourceType.MEMORY)
                    .resourceName(quotaName)
                    .extras(memoryExtras)
                    .build();
            operationAuditMessageProducer.send(message);
        }
    }

    protected void sendCpuUpdateAuditMessage(NamespaceUpdateRequest request,
                                             Map<String, Quantity> originHard,
                                             String quotaName,
                                             long elapsed) {
        Map<String, String> cpuExtras = new HashMap<>(4);

        Double newCpuLimits = request.getCpuLimit();
        double oldCpuLimits = UnitUtils.parseK8sCpuQuantity(originHard.get(K_NAMESPACE_CPU_LIMITS));
        if (newCpuLimits != null && newCpuLimits != oldCpuLimits) {
            cpuExtras.put("newLimits", String.valueOf(newCpuLimits));
            cpuExtras.put("oldLimits", String.valueOf(oldCpuLimits));
        }

        Double newCpuRequests = request.getCpuRequest();
        double oldCpuRequests = UnitUtils.parseK8sCpuQuantity(originHard.get(K_NAMESPACE_CPU_REQUESTS));
        if (newCpuRequests != null && newCpuRequests != oldCpuRequests) {
            cpuExtras.put("newRequests", String.valueOf(newCpuRequests));
            cpuExtras.put("oldRequests", String.valueOf(oldCpuRequests));
        }

        if (!cpuExtras.isEmpty()) {
            OperationAuditMessage message = new OperationAuditMessage.Builder()
                    .elapsed(elapsed)
                    .namespaceName(request.getName())
                    .operationType(OperationType.UPDATE)
                    .resourceType(ResourceType.CPU)
                    .resourceName(quotaName)
                    .extras(cpuExtras)
                    .build();
            operationAuditMessageProducer.send(message);
        }
    }

    private void sendHostPathStorageUpdateAuditMessage(NamespaceUpdateRequest request,
                                                       NamespaceExtras namespaceExtras,
                                                       String quotaName,
                                                       long elapsed) {
        if (request.getHostPathStorageBytes() != null
                && request.getHostPathStorageBytes() != namespaceExtras.getHostpathStorageBytes()) {
            OperationAuditMessage message = new OperationAuditMessage.Builder()
                    .elapsed(elapsed)
                    .namespaceName(request.getName())
                    .operationType(OperationType.UPDATE)
                    .resourceType(ResourceType.STORAGE_HOSTPATH)
                    .resourceName(quotaName)
                    .extras(ImmutableMap.of("hostPathSize", request.getHostPathStorageBytes()))
                    .build();
            operationAuditMessageProducer.send(message);
        }
    }

    public long getNamespaceStorageAvailableBytes(String namespaceName,
                                                  StorageType storageType) {
        return getNamespaceStorageTotalBytes(namespaceName, storageType)
                - getStorageReservedBytes(namespaceName, storageType);
    }

    public long getNamespaceStorageTotalBytes(String namespaceName,
                                              StorageType storageType) {
        NamespaceExtras namespaceExtras =
                namespaceExtrasRepository.findByNameAndDeletedIsFalse(namespaceName);
        if (namespaceExtras != null) {
            if (HostPath == storageType) {
                return namespaceExtras.getHostpathStorageBytes();
            } else if (EBS == storageType) {
                return namespaceExtras.getEbsBytes();
            } else {
                return namespaceExtras.getPoolBytes();
            }
        } else {
            return getLegacyNamespaceStorageTotalBytes(namespaceName, storageType);
        }
    }

    private long getLegacyNamespaceStorageTotalBytes(String namespaceName,
                                                     StorageType storageType) {
        ResourceQuota quota = clientManager.getClient()
                .resourceQuotas()
                .inNamespace(namespaceName)
                .withName(NamespaceNaming.quotaName(namespaceName))
                .get();

        return Optional.ofNullable(quota)
                .map(ResourceQuota::getMetadata)
                .map(ObjectMeta::getLabels)
                .map(labels -> labels.get(HostPath == storageType ? NAMESPACE_QUOTA_HOSTPATH_STORAGE_LABEL : NAMESPACE_QUOTA_RBD_STORAGE_LABEL))
                .map(UnitUtils::parseNamespaceStorageQuota)
                .orElse(0L);
    }

    public long getStorageReservedBytes(String namespaceName,
                                        StorageType storageType) {
        if (storageType == HostPath) {
            return getHostPathStorageReservedBytes(namespaceName);
        }

        List<Storage> storageList =
                storageRepository.findByNamespaceNameAndStorageType(namespaceName, storageType);
        return storageList.stream()
                .mapToLong(Storage::getAmountBytes)
                .sum();
    }

    /**
     * TODO has problems in multi instance or concurrency situation
     * FIXME should add stateful set support
     * <p>
     * get the namespace hostpath storage reserved bytes
     *
     * @param namespaceName the namespace
     * @return the reserved bytes
     */
    protected long getHostPathStorageReservedBytes(String namespaceName) {
        Map<String, Deployment> deploymentMap = null;
        Map<Long, List<StorageWorkload>> storageWorkloadMap = null;

        long reservedBytes = 0;

        List<Storage> hostPathStorageList =
                storageRepository.findByNamespaceNameAndStorageType(namespaceName, HostPath);

        for (Storage storage : hostPathStorageList) {
            if (!storage.isUnshared()) {
                reservedBytes += storage.getAmountBytes();
                continue;
            }

            if (storageWorkloadMap == null) {
                storageWorkloadMap = getStorageWorkloadMap(hostPathStorageList);
            }

            List<StorageWorkload> storageWorkloadList = storageWorkloadMap.get(storage.getId());
            if (CollectionUtils.isEmpty(storageWorkloadList)) {
                reservedBytes += storage.getAmountBytes();
                continue;
            }

            if (deploymentMap == null) {
                deploymentMap = getDeploymentMap(namespaceName);
            }

            int totalReplicas = 0;
            for (StorageWorkload storageWorkload : storageWorkloadList) {
                Deployment deployment = deploymentMap.get(storageWorkload.getWorkloadName());
                if (deployment == null) {
                    continue;
                }

                Integer replicas = deployment.getSpec().getReplicas();
                if (replicas == null || replicas == 0) {
                    continue;
                }
                totalReplicas += replicas;
            }
            if (totalReplicas == 0) {
                totalReplicas = 1;
            }
            reservedBytes += totalReplicas * storage.getAmountBytes();
        }
        return reservedBytes;
    }

    protected Map<Long, List<StorageWorkload>> getStorageWorkloadMap(List<Storage> storageList) {
        List<Long> storageIdList = Optional.ofNullable(storageList)
                .orElse(new ArrayList<>())
                .stream()
                .map(Storage::getId)
                .collect(Collectors.toList());

        List<StorageWorkload> storageWorkloadList = new ArrayList<>();
        if (!storageIdList.isEmpty()) {
            storageWorkloadList = storageWorkloadRepository.findByStorageIdIn(storageIdList);
        }

        return storageWorkloadList.stream()
                .collect(Collectors.groupingBy(StorageWorkload::getStorageId));
    }

    protected long getHostpathStorageUsedBytes(String namespaceName) {
        List<PersistentVolume> pvList = clientManager.getClient()
                .persistentVolumes()
                .withLabel(PV_NAMESPACE_LABEL, namespaceName)
                .list()
                .getItems();

        return Optional.ofNullable(pvList)
                .orElseGet(ArrayList::new)
                .stream()
                .filter(pv -> pv.getSpec().getHostPath() != null)
                .mapToLong(pv -> hostPathStorageManager.getHostpathPvUsedBytes(pv))
                .sum();
    }

    private Map<String, Deployment> getDeploymentMap(String namespaceName) {
        //get all deployments
        List<Deployment> deploymentList = clientManager.getClient()
                .extensions()
                .deployments()
                .inNamespace(namespaceName)
                .list()
                .getItems();

        //map all deployment with name as the key
        return Optional.ofNullable(deploymentList)
                .orElseGet(ArrayList::new)
                .stream()
                .collect(Collectors.toMap(deployment -> deployment.getMetadata().getName(), deployment -> deployment));
    }

    public void updateLegacyNamespaceQuota(NamespaceUpdateRequest request,
                                           ResourceQuota currentQuota) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        String namespaceName = request.getName();
        String quotaName = request.quotaName();

        Map<String, String> newLabels = composeNewLabels(request, currentQuota);
        Map<String, Quantity> newHard = composeNewHard(request, currentQuota);

        KubernetesClient client = clientManager.getClient();
        client.resourceQuotas()
                .inNamespace(namespaceName)
                .withName(quotaName)
                .edit()
                .editMetadata()
                .withLabels(newLabels)
                .endMetadata()
                .editSpec()
                .withHard(newHard)
                .endSpec()
                .done();
        logger.info("Update namespace - {} quota to - {}", namespaceName, request.toString());

        updateLegacyNamespace(request);
        logger.info("Update namespace privilege and hostpath allow - {} to - {}",
                namespaceName, request.toString());

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        sendHostPathStorageUpdateAuditMessage(request, quotaName, elapsed);
        Map<String, Quantity> originHard = currentQuota.getSpec().getHard();
        sendCpuUpdateAuditMessage(request, originHard, quotaName, elapsed);
        sendMemoryUpdateAuditMessage(request, originHard, quotaName, elapsed);
    }

    public void updateLegacyNamespace(NamespaceUpdateRequest request) {
        Boolean allowHostpath = request.getAllowHostpath();
        Boolean allowPrivilege = request.getAllowPrivilege();
        Boolean allowCriticalPod = request.getAllowCriticalPod();
        if (allowHostpath == null && allowPrivilege == null && allowCriticalPod == null) {
            return;
        }

        NamespaceFluent.MetadataNested<DoneableNamespace> edit = clientManager.getClient()
                .namespaces()
                .withName(request.getName())
                .edit()
                .editMetadata();
        if (allowHostpath != null) {
            edit.addToAnnotations(NAMESPACE_ALLOW_HOSTPATH_ANNOTATION, allowHostpath.toString());
        }

        if (allowPrivilege != null) {
            edit.addToAnnotations(NAMESPACE_ALLOW_PRIVILEGE_ANNOTATION, allowPrivilege.toString());
        }

        if (allowCriticalPod != null) {
            edit.addToAnnotations(NAMESPACE_ALLOW_CRITICAL_POD_ANNOTATION, allowCriticalPod.toString());
        }

        edit.endMetadata().done();
    }

    @Deprecated
    private void sendHostPathStorageUpdateAuditMessage(NamespaceUpdateRequest request,
                                                       String quotaName,
                                                       long elapsed) {
        if (request.getHostPathStorageBytes() != null) {
            OperationAuditMessage message = new OperationAuditMessage.Builder()
                    .elapsed(elapsed)
                    .namespaceName(request.getName())
                    .operationType(OperationType.UPDATE)
                    .resourceType(ResourceType.STORAGE_HOSTPATH)
                    .resourceName(quotaName)
                    .extras(ImmutableMap.of("hostPathSize", request.getHostPathStorageBytes()))
                    .build();
            operationAuditMessageProducer.send(message);
        }
    }

    private Map<String, String> composeNewLabels(NamespaceUpdateRequest request,
                                                 ResourceQuota currentQuota) {
        Map<String, String> labels = currentQuota.getMetadata().getLabels();
        if (labels == null) {
            labels = new HashMap<>(0);
        }

        Long poolBytes = request.getCephPoolBytes();
        Long hostPathStorageBytes = request.getHostPathStorageBytes();

        // legacy namespace not support ceph storage
        if (poolBytes != null) {
            labels.put(NAMESPACE_QUOTA_RBD_STORAGE_LABEL, String.valueOf(poolBytes));
        }

        if (hostPathStorageBytes != null) {
            labels.put(NAMESPACE_QUOTA_HOSTPATH_STORAGE_LABEL, String.valueOf(hostPathStorageBytes));
        }

        return labels;
    }

    private Map<String, Quantity> composeNewHard(NamespaceUpdateRequest request,
                                                 ResourceQuota currentQuota) {
        Map<String, Quantity> hard = currentQuota.getSpec().getHard();
        if (hard == null) {
            hard = new HashMap<>(0);
        }

        if (request.getCpuRequest() != null) {
            hard.put(K_NAMESPACE_CPU_REQUESTS, UnitUtils.toK8sCpuQuantity(request.getCpuRequest()));
        }
        if (request.getCpuLimit() != null) {
            hard.put(K_NAMESPACE_CPU_LIMITS, UnitUtils.toK8sCpuQuantity(request.getCpuLimit()));
        }

        if (request.getMemoryRequestBytes() != null) {
            hard.put(K_NAMESPACE_MEMORY_REQUESTS, UnitUtils.toK8sMemoryQuantity(request.getMemoryRequestBytes()));
        }
        if (request.getMemoryLimitBytes() != null) {
            hard.put(K_NAMESPACE_MEMORY_LIMITS, UnitUtils.toK8sMemoryQuantity(request.getMemoryLimitBytes()));
        }

        return hard;
    }

    public void createQuotaForLegacy(NamespaceUpdateRequest request) {
        ResourceQuota resourceQuota = composeResourceQuota(request);
        clientManager.getClient()
                .resourceQuotas()
                .inNamespace(request.getName())
                .create(resourceQuota);
    }

    private ResourceQuota composeResourceQuota(NamespaceUpdateRequest updateRequest) {
        String namespaceName = updateRequest.getName();
        ResourceQuota quota = new ResourceQuota();

        ObjectMeta metadata = new ObjectMeta();
        metadata.setName(updateRequest.quotaName());
        metadata.setNamespace(namespaceName);
        quota.setMetadata(metadata);

        ResourceQuotaSpec spec = new ResourceQuotaSpec();
        Map<String, Quantity> hard = new HashMap<>(4);

        hard.put(K_NAMESPACE_CPU_REQUESTS, UnitUtils.toK8sCpuQuantity(updateRequest.getCpuRequest()));
        hard.put(K_NAMESPACE_CPU_LIMITS, UnitUtils.toK8sCpuQuantity(updateRequest.getCpuLimit()));
        hard.put(K_NAMESPACE_MEMORY_REQUESTS, UnitUtils.toK8sMemoryQuantity(updateRequest.getMemoryRequestBytes()));
        hard.put(K_NAMESPACE_MEMORY_LIMITS, UnitUtils.toK8sMemoryQuantity(updateRequest.getMemoryLimitBytes()));

        spec.setHard(hard);
        quota.setSpec(spec);

        return quota;
    }
}
