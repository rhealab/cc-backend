package cc.backend.kubernetes.namespaces.services;

import cc.backend.EnnProperties;
import cc.backend.audit.request.EnnContext;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.namespaces.data.NamespaceExtras;
import cc.backend.kubernetes.namespaces.data.NamespaceExtrasRepository;
import cc.backend.role.assignment.data.AssignmentExtrasRepository;
import cc.backend.role.assignment.data.RoleAssignmentExtras;
import cc.keystone.client.KeystoneProjectManagement;
import io.fabric8.kubernetes.api.model.Namespace;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static cc.backend.kubernetes.Constants.DELAY_MILLIS;


/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/22.
 */
@Component
public class NamespaceNamesManagement {

    @Inject
    private KeystoneProjectManagement keystoneProjectManagement;

    @Inject
    private NamespaceExtrasRepository namespaceExtrasRepository;

    @Inject
    private AssignmentExtrasRepository assignmentExtrasRepository;

    @Inject
    private EnnProperties properties;

    @Inject
    private KubernetesClientManager clientManager;

    public List<String> getNamespaceNames() {
        if (properties.isAuthEnabled()) {
            return getMyNamespaceNames();
        } else {
            return getAllNamespaceNames();
        }
    }

    public List<String> getMyNamespaceNames() {
        Date date = Date.from(Instant.now().minusMillis(DELAY_MILLIS));
        String clusterName = EnnContext.getClusterName();
        String userId = EnnContext.getUserId();

        /*
        project in keystone, contains legacy, part of delete failed, delete pending namespaces
         */
        List<String> userProjects = keystoneProjectManagement.getUserProjects(clusterName, userId);

        /*
        namespaces the user assigned in console
         */
        List<String> assignedNamespaces = assignmentExtrasRepository
                .findByUserIdAndActionType(userId, RoleAssignmentExtras.ActionType.ASSIGN)
                .stream()
                .filter(extras -> extras.getNamespace() != null)
                .filter(extras -> extras.getStatus() == RoleAssignmentExtras.Status.ENABLED)
                .map(RoleAssignmentExtras::getNamespace)
                .collect(Collectors.toList());

        /*
        namespaces deleted in 5 minutes and other status namespaces
         */
        List<String> namespaces = namespaceExtrasRepository
                .findByNameInAndDeletedIsFalseOrDeletedOnAfter(assignedNamespaces, date)
                .stream()
                .map(NamespaceExtras::getName)
                .collect(Collectors.toList());

        /*
        remove all then add all in case duplicate
         */
        userProjects.removeAll(namespaces);
        userProjects.addAll(namespaces);

        return userProjects;
    }

    public List<String> getAllNamespaceNames() {

        List<Namespace> namespaces = clientManager.getClient()
                .namespaces()
                .list()
                .getItems();
        List<String> namespaceNames = Optional.ofNullable(namespaces)
                .orElseGet(ArrayList::new)
                .stream()
                .map(namespace -> namespace.getMetadata().getName())
                .collect(Collectors.toList());
        Date date = Date.from(Instant.now().minusMillis(DELAY_MILLIS));
        /*
        namespaces deleted in 5 minutes and other status namespaces
         */
        List<String> consoleNamespaces = namespaceExtrasRepository
                .findByNameInAndDeletedIsFalseOrDeletedOnAfter(namespaceNames, date)
                .stream()
                .map(NamespaceExtras::getName)
                .collect(Collectors.toList());

        namespaceNames.removeAll(consoleNamespaces);
        namespaceNames.addAll(consoleNamespaces);

        return namespaceNames;
    }
}
