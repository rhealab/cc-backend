package cc.backend.kubernetes.namespaces.services;

import cc.backend.EnnProperties;
import cc.backend.audit.request.EnnContext;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.apps.data.AppRepository;
import cc.backend.kubernetes.namespaces.data.*;
import cc.backend.kubernetes.namespaces.dto.NamespaceDetailDto;
import cc.backend.kubernetes.namespaces.dto.NamespaceDto;
import cc.backend.kubernetes.namespaces.dto.NamespaceType;
import cc.backend.kubernetes.namespaces.dto.ResourceCount;
import cc.backend.kubernetes.namespaces.validator.NamespaceValidator;
import cc.backend.kubernetes.service.data.ServiceExtrasRepository;
import cc.backend.kubernetes.workload.deployment.data.DeploymentExtrasRepository;
import cc.keystone.client.KeystoneProjectManagement;
import cc.keystone.client.KeystoneRoleAssignmentManagement;
import cc.keystone.client.domain.KeystoneProject;
import cc.keystone.client.domain.RoleAssignment;
import cc.keystone.client.domain.RoleType;
import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.Pod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static cc.backend.kubernetes.Constants.*;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/2/14.
 * support legacy namespace
 */
@Component
public class MyNamespacesManagement {
    private static final Logger logger = LoggerFactory.getLogger(MyNamespacesManagement.class);

    @Inject
    protected KubernetesClientManager clientManager;

    @Inject
    protected KeystoneRoleAssignmentManagement keystoneRoleAssignmentManagement;

    @Inject
    protected KeystoneProjectManagement keystoneProjectManagement;

    @Inject
    protected NamespaceExtrasRepository namespaceExtrasRepository;

    @Inject
    protected UserNamespaceAccessInfoRepository accessInfoRepository;

    @Inject
    private DeploymentExtrasRepository deploymentExtrasRepository;

    @Inject
    private ServiceExtrasRepository serviceExtrasRepository;

    @Inject
    private AppRepository appRepository;

    @Inject
    protected EnnProperties properties;

    @Inject
    protected NamespaceValidator validator;

    /**
     * get user's namespace list in a cluster
     * <p>
     * sys admin can get all projects
     * TODO frontend to check if a sys admin may better !
     *
     * @return the namespace list
     */
    public List<NamespaceDto> getMyNamespaceList() {
        if (!properties.isAuthEnabled()) {
            return getNamespaceListNoAuth();
        }

        String clusterName = EnnContext.getClusterName();
        String userId = EnnContext.getUserId();

        List<String> projectList;
        List<NamespaceExtras> extrasList;
        Date date = Date.from(Instant.now().minusMillis(DELAY_MILLIS));
        if (!keystoneRoleAssignmentManagement.isSysAdmin(clusterName, userId)) {
            projectList = keystoneProjectManagement.getUserProjects(clusterName, userId);
            extrasList = namespaceExtrasRepository.findByNameInAndDeletedIsFalseOrDeletedOnAfter(projectList, date);
        } else {
            projectList = keystoneProjectManagement.getProjects(clusterName)
                    .stream()
                    .map(KeystoneProject::getName)
                    .collect(Collectors.toList());
            extrasList = namespaceExtrasRepository.findByDeletedIsFalseOrDeletedOnAfter(date);
        }

        List<RoleAssignment> assignments = keystoneRoleAssignmentManagement.getUserRoleAssignments(clusterName, userId);
        List<UserNamespaceAccessInfo> accessInfoList = accessInfoRepository.findByUserId(userId);

        return composeNamespaceDtoList(projectList, extrasList, assignments, accessInfoList);
    }

    public List<NamespaceDto> getNamespaceListNoAuth() {
        List<Namespace> namespaces = clientManager.getClient()
                .namespaces()
                .list()
                .getItems();

        List<String> namespaceNames = Optional.ofNullable(namespaces)
                .orElseGet(ArrayList::new)
                .stream()
                .map(namespace -> namespace.getMetadata().getName())
                .collect(Collectors.toList());

        List<UserNamespaceAccessInfo> accessInfoList = accessInfoRepository.findByUserId(null);
        Date date = Date.from(Instant.now().minusMillis(DELAY_MILLIS));
        List<NamespaceExtras> extrasList = namespaceExtrasRepository.findByDeletedIsFalseOrDeletedOnAfter(date);

        return composeNamespaceDtoList(namespaceNames, extrasList, new ArrayList<>(), accessInfoList);
    }

    private List<NamespaceDto> composeNamespaceDtoList(List<String> projectList,
                                                       List<NamespaceExtras> extrasList,
                                                       List<RoleAssignment> assignments,
                                                       List<UserNamespaceAccessInfo> accessInfoList) {
        Map<String, NamespaceDto> dtoMap = projectList.stream()
                .map(project -> {
                    NamespaceDto dto = new NamespaceDto();
                    dto.setName(project);
                    dto.setType(NamespaceType.Legacy);
                    dto.setStatus(NamespaceStatus.CREATE_SUCCESS);
                    return dto;
                })
                .collect(Collectors.toMap(NamespaceDto::getName, dto -> dto));

        //remove system namespace like 'admin'
        properties.getCurrentKubernetes().getSystemNamespaces().forEach(dtoMap::remove);

        extrasList.forEach(extras -> {
            NamespaceDto namespaceDto = dtoMap.get(extras.getName());

            if (namespaceDto == null) {
                namespaceDto = new NamespaceDto();
                namespaceDto.setName(extras.getName());
                dtoMap.put(extras.getName(), namespaceDto);
            }

            namespaceDto.setStatus(extras.getStatus());
            namespaceDto.setType(NamespaceType.Normal);
            namespaceDto.setCreatedBy(extras.getCreatedBy());
            namespaceDto.setCreatedOn(extras.getCreatedOn());
            namespaceDto.setAllowHostpath(extras.getAllowHostpath());
            namespaceDto.setAllowPrivilege(extras.getAllowPrivilege());
            namespaceDto.setAllowCriticalPod(extras.getAllowCriticalPod());
        });

        assignments.forEach(assignment -> {
            NamespaceDto namespaceDto = dtoMap.get(assignment.getProject());
            if (namespaceDto != null) {
                namespaceDto.setMyRoleName(assignment.getRole().name().toLowerCase());
            }
        });

        //merge user access namespace time
        accessInfoList.forEach(accessInfo -> {
            NamespaceDto namespaceDto = dtoMap.get(accessInfo.getNamespace());
            if (namespaceDto != null) {
                namespaceDto.setLastAccessOn(accessInfo.getAccessTime());
            }
        });

        dtoMap.values().stream()
                .filter(dto -> dto.getType() == NamespaceType.Legacy)
                .forEach(dto -> {
                    dto.setAllowHostpath(false);
                    dto.setAllowPrivilege(false);
                    dto.setAllowCriticalPod(false);
                    Namespace namespace = getNamespace(dto.getName());
                    if (namespace == null || namespace.getMetadata().getAnnotations() == null) {
                        return;
                    }

                    Map<String, String> annotations = namespace.getMetadata().getAnnotations();
                    dto.setAllowHostpath("true".equals(annotations.get(NAMESPACE_ALLOW_HOSTPATH_ANNOTATION)));
                    dto.setAllowPrivilege("true".equals(annotations.get(NAMESPACE_ALLOW_PRIVILEGE_ANNOTATION)));
                    dto.setAllowCriticalPod("true".equals(annotations.get(NAMESPACE_ALLOW_CRITICAL_POD_ANNOTATION)));
                });

        return new ArrayList<>(dtoMap.values());
    }

    public Namespace getNamespace(String name) {
        return clientManager.getClient().namespaces().withName(name).get();
    }

    public NamespaceDetailDto getNamespaceDetail(String namespaceName) {
        String clusterName = EnnContext.getClusterName();
        String userId = EnnContext.getUserId();

        NamespaceDetailDto dto = new NamespaceDetailDto();

        dto.setName(namespaceName);
        dto.setStatus(NamespaceStatus.CREATE_SUCCESS);
        dto.setType(NamespaceType.Legacy);

        if (properties.isAuthEnabled()) {
            dto.setNsAdminList(keystoneRoleAssignmentManagement.getProjectNsAdmin(clusterName, namespaceName));
        }

        NamespaceExtras namespaceExtras =
                namespaceExtrasRepository.findByNameAndDeletedIsFalse(namespaceName);

        UserNamespaceAccessInfo accessInfo =
                accessInfoRepository.findByUserIdAndNamespace(userId, namespaceName);

        if (namespaceExtras != null) {
            dto.setStatus(namespaceExtras.getStatus());
            dto.setType(NamespaceType.Normal);
            dto.setCreatedBy(namespaceExtras.getCreatedBy());
            dto.setCreatedOn(namespaceExtras.getCreatedOn());
            dto.setAllowHostpath(namespaceExtras.getAllowHostpath());
            dto.setAllowPrivilege(namespaceExtras.getAllowPrivilege());
            dto.setAllowCriticalPod(namespaceExtras.getAllowCriticalPod());
            dto.setHostPathBytes(namespaceExtras.getHostpathStorageBytes());
            dto.setCephPoolBytes(namespaceExtras.getPoolBytes());
            dto.setEbsBytes(namespaceExtras.getEbsBytes());
        } else {
            Namespace namespace = clientManager.getClient()
                    .namespaces()
                    .withName(namespaceName)
                    .get();
            Map<String, String> annotations = namespace.getMetadata().getAnnotations();
            if (annotations != null) {
                dto.setAllowHostpath("true".equals(annotations.get(NAMESPACE_ALLOW_HOSTPATH_ANNOTATION)));
                dto.setAllowPrivilege("true".equals(annotations.get(NAMESPACE_ALLOW_PRIVILEGE_ANNOTATION)));
                dto.setAllowCriticalPod("true".equals(annotations.get(NAMESPACE_ALLOW_CRITICAL_POD_ANNOTATION)));
            } else {
                dto.setAllowHostpath(false);
                dto.setAllowPrivilege(false);
                dto.setAllowCriticalPod(false);
            }
        }

        if (properties.isAuthEnabled()) {
            List<RoleType> roleInNamespace = keystoneRoleAssignmentManagement
                    .getUserRoleInNamespace(clusterName, namespaceName, userId);
            if (!roleInNamespace.isEmpty()) {
                dto.setMyRoleName(roleInNamespace.get(0).name().toLowerCase());
            }
        }

        if (accessInfo != null) {
            dto.setLastAccessOn(accessInfo.getAccessTime());
        }

        return dto;
    }

    public ResourceCount getNamespaceResourceCount(String namespaceName) {
        ResourceCount resourceCount = new ResourceCount();
        resourceCount.setAppNum(appRepository.countByNamespace(namespaceName));
        resourceCount.setDeploymentNum(deploymentExtrasRepository.countByNamespace(namespaceName));
        resourceCount.setServiceNum(serviceExtrasRepository.countByNamespace(namespaceName));
        List<Pod> pods = clientManager.getClient().pods().inNamespace(namespaceName).list().getItems();
        resourceCount.setPodNum(pods == null ? 0 : pods.size());

        if (properties.isAuthEnabled()) {
            List<RoleAssignment> assignments =
                    keystoneRoleAssignmentManagement.getProjectRoleAssignments(EnnContext.getClusterName(), namespaceName);
            resourceCount.setUserNum(assignments == null ? 0 : assignments.size());
        }

        return resourceCount;
    }

    public List<RoleType> getUserRoleInNamespace(String namespaceName) {
        if (properties.isAuthEnabled()) {
            return keystoneRoleAssignmentManagement.getUserRoleInNamespace(EnnContext.getClusterName(),
                    namespaceName, EnnContext.getUserId());
        }
        return new ArrayList<>();
    }

    public void setAccessInfo(String namespaceName) {
        String userId = EnnContext.getUserId();
        UserNamespaceAccessInfo info = accessInfoRepository.findByUserIdAndNamespace(userId, namespaceName);
        Date currentDate = new Date();
        if (info == null) {
            info = new UserNamespaceAccessInfo();
            info.setNamespace(namespaceName);
            info.setUserId(userId);
        }
        info.setAccessTime(currentDate);
        accessInfoRepository.save(info);
    }
}