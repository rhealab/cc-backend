package cc.backend.kubernetes.namespaces.services;

import cc.backend.EnnProperties;
import cc.backend.common.Metrics;
import cc.backend.common.MetricsType;
import cc.backend.common.OpenTsdbClient;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.namespaces.data.NamespaceExtras;
import cc.backend.kubernetes.namespaces.data.NamespaceExtrasRepository;
import cc.backend.kubernetes.namespaces.dto.MetricsDto;
import cc.backend.kubernetes.namespaces.dto.NamespaceNaming;
import cc.backend.kubernetes.storage.usage.entity.*;
import com.google.common.collect.ImmutableList;
import io.fabric8.kubernetes.api.model.Quantity;
import io.fabric8.kubernetes.api.model.ResourceQuota;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static cc.backend.kubernetes.Constants.*;
import static cc.backend.kubernetes.utils.UnitUtils.*;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/2/24.
 */
@Component
public class NamespaceMetricsManagement {
    private static final Logger logger = LoggerFactory.getLogger(NamespaceMetricsManagement.class);

    @Inject
    private KubernetesClientManager clientManager;

    @Inject
    private NamespaceStatsManagement namespaceStatsManagement;

    @Inject
    private CephPoolUsageRepository poolUsageRepository;

    @Inject
    private CephfsDirectoryUsageRepository cephfsDirectoryUsageRepository;

    @Inject
    private CephGlobalUsageRepository cephGlobalUsageRepository;

    @Inject
    private NfsDirectoryUsageRepository nfsDirectoryUsageRepository;

    @Inject
    private NfsGlobalUsageRepository nfsGlobalUsageRepository;

    @Inject
    private OpenTsdbClient openTsdbClient;

    @Inject
    private NamespaceExtrasRepository namespaceExtrasRepository;

    @Inject
    private EnnProperties properties;

    public List<MetricsDto> getComputeMetrics(String namespaceName) {
        ResourceQuota resourceQuota = clientManager.getClient()
                .resourceQuotas()
                .inNamespace(namespaceName)
                .withName(NamespaceNaming.quotaName(namespaceName))
                .get();

        MetricsDto cpuMetricsDto;
        MetricsDto memoryMetricsDto;
        if (resourceQuota != null) {
            Map<String, Quantity> hard = resourceQuota.getSpec().getHard();
            double totalCpu = parseK8sCpuQuantity(hard.get(K_NAMESPACE_CPU_LIMITS));
            long totalMemory = parseK8sMemoryQuantity(hard.get(K_NAMESPACE_MEMORY_LIMITS));

            List<Metrics> cpuMetrics = openTsdbClient.getNsCpuMetrics(namespaceName);
            List<Metrics> memoryMetrics = openTsdbClient.getNsMemoryMetrics(namespaceName);

            cpuMetricsDto = MetricsDto.from(cpuMetrics, totalCpu, MetricsType.CPU);
            memoryMetricsDto = MetricsDto.from(memoryMetrics, totalMemory, MetricsType.MEMORY);
        } else {
            cpuMetricsDto = MetricsDto.newErrorInstance(MetricsType.CPU);
            memoryMetricsDto = MetricsDto.newErrorInstance(MetricsType.MEMORY);
        }

        return ImmutableList.of(cpuMetricsDto, memoryMetricsDto);
    }

    public List<MetricsDto> getStorageMetrics(String namespaceName) {
        NamespaceExtras namespaceExtras = namespaceExtrasRepository.findByNameAndDeletedIsFalse(namespaceName);

        MetricsDto poolMetricsDto = getPoolMetricsDto(namespaceExtras);
        MetricsDto nfsMetricsDto = getNfsMetricsDto(namespaceName);

        return ImmutableList.<MetricsDto>builder()
                .add(getHostpathMetricsDto(namespaceExtras))
                .add(poolMetricsDto)
                .add(getCephFsMetricsDto(namespaceName))
                .add(getNfsMetricsDto(namespaceName))
                .add(getEbsMetricsDto(namespaceName))
                .add(MetricsDto.from(nfsMetricsDto.getUsed(), nfsMetricsDto.getTotal(), MetricsType.EFS))
                .add(MetricsDto.from(poolMetricsDto.getUsed(), poolMetricsDto.getTotal(), MetricsType.CEPH_POOL))
                .build();
    }

    private MetricsDto getHostpathMetricsDto(NamespaceExtras namespaceExtras) {
        if (namespaceExtras != null) {
            long hostPathTotalBytes = namespaceExtras.getHostpathStorageBytes();
            long hostPathUsedBytes = namespaceStatsManagement.getHostpathStorageUsedBytes(namespaceExtras.getName());
            return MetricsDto.from(hostPathUsedBytes, hostPathTotalBytes, MetricsType.HOSTPATH);
        } else {
            return MetricsDto.newErrorInstance(MetricsType.HOSTPATH);
        }
    }

    private MetricsDto getPoolMetricsDto(NamespaceExtras namespaceExtras) {
        if (namespaceExtras != null) {
            CephPoolUsage poolUsage = poolUsageRepository.findByNamespaceName(namespaceExtras.getName());
            long poolTotalBytes = namespaceExtras.getPoolBytes();
            long poolUsedBytes = 0;
            if (poolUsage != null) {
                poolUsedBytes = poolUsage.getBytesUsed();
            }
            return MetricsDto.from(poolUsedBytes, poolTotalBytes, MetricsType.CEPH_RBD);
        } else {
            return MetricsDto.newErrorInstance(MetricsType.CEPH_RBD);
        }
    }

    private MetricsDto getCephFsMetricsDto(String namespaceName) {
        long cephFsTotalBytes = 0;
        long cephFsUsedBytes = 0;
        CephGlobalUsage globalUsage = cephGlobalUsageRepository.findLatest();
        if (globalUsage != null) {
            cephFsTotalBytes = globalUsage.getTotalBytes();
        }
        CephfsDirectoryUsage cephfsNamespaceUsage =
                cephfsDirectoryUsageRepository.findOne(namespaceName);
        if (cephfsNamespaceUsage != null) {
            cephFsUsedBytes = cephfsNamespaceUsage.getBytesUsed();
        }
        return MetricsDto.from(cephFsUsedBytes, cephFsTotalBytes, MetricsType.CEPH_FS);
    }

    private MetricsDto getNfsMetricsDto(String namespaceName) {
        long nfsTotalBytes = 0;
        long nfsUsedBytes = 0;
        NfsGlobalUsage nfsGlobalUsage = nfsGlobalUsageRepository.findLatest();
        if (nfsGlobalUsage != null) {
            nfsTotalBytes = nfsGlobalUsage.getTotalBytes();
        }
        NfsDirectoryUsage nfsDirectoryUsage =
                nfsDirectoryUsageRepository.findOne(namespaceName);
        if (nfsDirectoryUsage != null) {
            nfsUsedBytes = nfsDirectoryUsage.getBytesUsed();
        }
        return MetricsDto.from(nfsUsedBytes, nfsTotalBytes, MetricsType.NFS);
    }

    private MetricsDto getEbsMetricsDto(String namespaceName) {
        long ebsTotalBytes = properties.getCurrentEbs().getTotalBytes();
        //FIXME should have a way to get usage
        long ebsUsedBytes = 0;
        return MetricsDto.from(ebsUsedBytes, ebsTotalBytes, MetricsType.EBS);
    }

    @Deprecated
    public List<MetricsDto> getLegacyStorageMetrics(String namespaceName) {
        NamespaceExtras namespaceExtras =
                namespaceExtrasRepository.findByNameAndDeletedIsFalse(namespaceName);
        if (namespaceExtras != null) {
            return getStorageMetrics(namespaceName);
        }

        List<MetricsDto> dtos = new ArrayList<>();
        MetricsDto hostPathMetricsDto;
        MetricsDto rbdMetricsDto;
        MetricsDto cephFsMetricsDto;
        MetricsDto nfsMetricsDto;
        MetricsDto ebsMetricsDto;

        ResourceQuota resourceQuota = clientManager.getClient()
                .resourceQuotas()
                .inNamespace(namespaceName)
                .withName(NamespaceNaming.quotaName(namespaceName))
                .get();

        if (resourceQuota != null && resourceQuota.getMetadata().getLabels() != null) {
            Map<String, String> labels = resourceQuota.getMetadata().getLabels();

            long hostPathTotalBytes = parseNamespaceStorageQuota(labels.get(NAMESPACE_QUOTA_HOSTPATH_STORAGE_LABEL));
            long hostPathUsedBytes = namespaceStatsManagement.getHostpathStorageUsedBytes(namespaceName);
            hostPathMetricsDto = MetricsDto.from(hostPathUsedBytes, hostPathTotalBytes, MetricsType.HOSTPATH);

            CephPoolUsage poolUsage =
                    poolUsageRepository.findByNamespaceName(namespaceName);
            long rbdTotalBytes = parseNamespaceStorageQuota(labels.get(NAMESPACE_QUOTA_RBD_STORAGE_LABEL));
            long rbdUsedBytes = 0;
            if (poolUsage != null) {
                rbdUsedBytes = poolUsage.getBytesUsed();
            }
            rbdMetricsDto = MetricsDto.from(rbdUsedBytes, rbdTotalBytes, MetricsType.CEPH_RBD);

            cephFsMetricsDto = getCephFsMetricsDto(namespaceName);
            nfsMetricsDto = getNfsMetricsDto(namespaceName);
            ebsMetricsDto = getEbsMetricsDto(namespaceName);
        } else {
            hostPathMetricsDto = MetricsDto.newErrorInstance(MetricsType.HOSTPATH);
            rbdMetricsDto = MetricsDto.newErrorInstance(MetricsType.CEPH_RBD);
            cephFsMetricsDto = MetricsDto.newErrorInstance(MetricsType.CEPH_FS);
            nfsMetricsDto = MetricsDto.newErrorInstance(MetricsType.NFS);
            ebsMetricsDto = MetricsDto.newErrorInstance(MetricsType.EBS);
        }
        dtos.add(hostPathMetricsDto);
        dtos.add(rbdMetricsDto);
        dtos.add(cephFsMetricsDto);
        dtos.add(nfsMetricsDto);
        dtos.add(MetricsDto.from(nfsMetricsDto.getUsed(), nfsMetricsDto.getTotal(), MetricsType.EFS));
        dtos.add(ebsMetricsDto);

        return dtos;
    }
}
