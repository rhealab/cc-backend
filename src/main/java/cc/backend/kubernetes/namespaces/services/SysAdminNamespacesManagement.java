package cc.backend.kubernetes.namespaces.services;

import cc.backend.EnnProperties;
import cc.backend.RabbitConfig;
import cc.backend.audit.operation.OperationAuditMessage;
import cc.backend.audit.operation.OperationAuditMessageProducer;
import cc.backend.audit.operation.OperationType;
import cc.backend.audit.operation.ResourceType;
import cc.backend.audit.request.EnnContext;
import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceRepository;
import cc.backend.event.data.EventSourceType;
import cc.backend.kubernetes.CustomKubernetesClient;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.namespaces.data.NamespaceExtras;
import cc.backend.kubernetes.namespaces.data.NamespaceExtrasRepository;
import cc.backend.kubernetes.namespaces.data.NamespaceStatus;
import cc.backend.kubernetes.namespaces.dto.NamespaceCreateRequest;
import cc.backend.kubernetes.namespaces.dto.NamespaceNaming;
import cc.backend.kubernetes.namespaces.event.*;
import cc.backend.kubernetes.namespaces.messages.*;
import cc.backend.kubernetes.namespaces.status.NamespaceCreationStatusChecker;
import cc.backend.kubernetes.namespaces.status.NamespaceDeletionStatusChecker;
import cc.keystone.client.KeystoneProjectManagement;
import cc.lib.retcode.CcException;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.StorageClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static cc.backend.kubernetes.Constants.*;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/5/12.
 */
@Component
public class SysAdminNamespacesManagement {
    private static final Logger logger = LoggerFactory.getLogger(SysAdminNamespacesManagement.class);

    @Inject
    private NamespaceExtrasRepository namespaceExtrasRepository;

    @Inject
    private RabbitMessagingTemplate messagingTemplate;

    @Inject
    private OperationAuditMessageProducer operationAuditMessageProducer;

    @Inject
    private NamespaceCreationStatusChecker namespaceCreationStatusChecker;

    @Inject
    private NamespaceDeletionStatusChecker namespaceDeletionStatusChecker;

    @Inject
    private EventSourceRepository eventSourceRepository;

    @Inject
    private NamespaceProcessor namespaceProcessor;

    @Inject
    private EnnProperties properties;

    @Inject
    private NsExtrasProcessor nsExtrasProcessor;

    @Inject
    private CephPoolProcessor cephPoolProcessor;

    @Inject
    private CephFsDirProcessor cephFsDirProcessor;

    @Inject
    private CephUserProcessor cephUserProcessor;

    @Inject
    private NfsDirProcessor nfsDirProcessor;

    @Inject
    private KeystoneProjectManagement keystoneProjectManagement;

    @Inject
    private KubernetesClientManager clientManager;

    @Transactional(rollbackFor = Exception.class)
    public EventSource<NamespaceCreateRequest> initNamespaceForCreate(NamespaceCreateRequest request) {
        logger.info("initialize namespace extras for create - {} in cluster - {}.",
                request.getNamespaceName(), EnnContext.getClusterName());
        String namespaceName = request.getName();
        EventSource<NamespaceCreateRequest> eventSource = new EventSource.Builder<NamespaceCreateRequest>()
                .namespaceName(namespaceName)
                .sourceType(EventSourceType.NS_CREATE)
                .sourceName(namespaceName)
                .payload(request)
                .build();
        EventSource<NamespaceCreateRequest> savedEventSource = eventSourceRepository.save(eventSource);
        nsExtrasProcessor.create(savedEventSource);
        namespaceCreationStatusChecker.start(EnnContext.getClusterName(), namespaceName, namespaceName);
        return savedEventSource;
    }

    public void createNamespace(NamespaceCreateRequest request,
                                EventSource<NamespaceCreateRequest> eventSource) {
        logger.info("start create namespace - {} in cluster - {}.", request.getName());
        Stopwatch stopwatch = Stopwatch.createStarted();

        namespaceProcessor.create(eventSource);

        if (properties.getCurrentEbs().isEnabled()) {
            createEbsStorageClass(request.getNamespaceName());
        }

        sendStorageInitMessage(request);

        sendAuditMessage(request, stopwatch);
    }

    @Inject
    private CustomKubernetesClient client;

    //TODO should move to a correct place and consider conflict with ceph rbd storage class
    private void createEbsStorageClass(String namespaceName) {
        StorageClass storageClass = new StorageClass();

        ObjectMeta meta = new ObjectMeta();
        meta.setName(NamespaceNaming.storageClassName(namespaceName));
        meta.setLabels(ImmutableMap.of("namespaceName", namespaceName));
        storageClass.setMetadata(meta);

        Map<String, String> parameters = new HashMap<>(2);
        parameters.put(STORAGE_CLASS_TYPE, EBS_TYPE_SC1);
        parameters.put(STORAGE_CLASS_ZONE, properties.getCurrentEbs().getZone());
        storageClass.setParameters(parameters);

        storageClass.setProvisioner("kubernetes.io/aws-ebs");

        client.createStorageClass(storageClass);

    }

    private void sendStorageInitMessage(NamespaceCreateRequest request) {
        String clusterName = EnnContext.getClusterName();
        NamespaceCreatedMessage message = new NamespaceCreatedMessage();
        message.setUserId(EnnContext.getUserId());
        message.setRequestId(EnnContext.getRequestId());
        message.setNamespaceName(request.getName());
        message.setClusterName(clusterName);
        message.setRbdAmountBytes(request.getPoolBytes());

        if (properties.getCurrentCeph().isEnabled()) {
            try {
                messagingTemplate.convertAndSend(RabbitConfig.NAMESPACE_CREATED_Q, message);
            } catch (MessagingException e) {
                logger.info(String.format("Failed to create storage: %s", e));
                throw new CcException(BackendReturnCodeNameConstants.SEND_STORAGE_CREATE_MESSAGE_ERROR,
                        ImmutableMap.of("msg", e.getFailedMessage()));
            }
        }

        if (properties.getCurrentNfs().isEnabled()) {
            try {
                messagingTemplate.convertAndSend(RabbitConfig.NAMESPACE_INIT_NFS_Q, message);
            } catch (MessagingException e) {
                logger.info(String.format("Failed to init nfs: %s", e));
                throw new CcException(BackendReturnCodeNameConstants.SEND_INIT_NFS_MESSAGE_ERROR,
                        ImmutableMap.of("msg", e.getFailedMessage()));
            }
        }
    }

    private void sendAuditMessage(NamespaceCreateRequest request, Stopwatch stopwatch) {
        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        String namespaceName = request.getName();
        Map<String, Object> extras = new HashMap<>(6);
        extras.put("cpuRequest", request.getCpuRequest());
        extras.put("cpuLimit", request.getCpuLimit());
        extras.put("memoryRequestBytes", request.getMemoryRequestBytes());
        extras.put("memoryLimitBytes", request.getMemoryLimitBytes());
        extras.put("localStorageBytes", request.getHostPathStorageBytes());
        extras.put("remoteStorageBytes", request.getPoolBytes());

        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .elapsed(elapsed)
                .namespaceName(namespaceName)
                .operationType(OperationType.CREATE)
                .resourceType(ResourceType.NAMESPACE)
                .resourceName(namespaceName)
                .extras(extras)
                .build();
        operationAuditMessageProducer.send(message);
    }

    @RabbitListener(queues = RabbitConfig.CEPH_POOL_CREATED_Q)
    public void onCephPoolCreated(CephPoolCreatedMessage message) {
        EnnContext.setContext(message);

        String clusterName = message.getClusterName();
        String namespaceName = message.getNamespaceName();
        logger.info("RabbitMQ: received CephPoolCreatedMessage cluster={}, namespace={}, bytes={}",
                clusterName, namespaceName, message.getRbdAmountBytes());

        EventSource<CephPoolCreatedMessage> eventSource = getNsCreateEventSource(namespaceName, message);
        if (shouldHandleCreateEvent(eventSource, message)) {
            cephPoolProcessor.create(eventSource);
            namespaceCreationStatusChecker.check(EnnContext.getClusterName(), namespaceName, namespaceName);
        }
    }

    @RabbitListener(queues = RabbitConfig.CEPH_FS_DIR_CREATED_Q)
    public void onCephFsDirCreated(CephDirCreatedMessage message) {
        EnnContext.setContext(message);

        String clusterName = message.getClusterName();
        String namespaceName = message.getNamespaceName();
        logger.info("RabbitMQ: received CephDirCreatedMessage namespace={}, cluster={}", namespaceName, clusterName);

        EventSource<CephDirCreatedMessage> eventSource = getNsCreateEventSource(namespaceName, message);
        if (shouldHandleCreateEvent(eventSource, message)) {
            cephFsDirProcessor.create(eventSource);
            namespaceCreationStatusChecker.check(EnnContext.getClusterName(), namespaceName, namespaceName);
        }
    }

    @RabbitListener(queues = RabbitConfig.CEPH_USER_CREATED_Q)
    public void onCephUserCreated(CephUserCreatedMessage message) {
        EnnContext.setContext(message);

        String clusterName = message.getClusterName();
        String namespaceName = message.getNamespaceName();
        logger.info("RabbitMQ: received message - cephUser.username={}, cluster={}", message.getUsername(), clusterName);

        EventSource<CephUserCreatedMessage> eventSource = getNsCreateEventSource(namespaceName, message);
        if (shouldHandleCreateEvent(eventSource, message)) {
            Stopwatch stopwatch = Stopwatch.createStarted();
            cephUserProcessor.create(eventSource);
            sendSecretCreatedAudit(message, stopwatch);
            namespaceCreationStatusChecker.check(EnnContext.getClusterName(), namespaceName, namespaceName);
        }
    }

    private void sendSecretCreatedAudit(CephUserCreatedMessage message, Stopwatch stopwatch) {
        OperationAuditMessage auditMessage = new OperationAuditMessage.Builder()
                .elapsed(stopwatch.stop().elapsed(TimeUnit.MILLISECONDS))
                .namespaceName(message.getNamespaceName())
                .operationType(OperationType.CREATE)
                .resourceType(ResourceType.SECRET)
                .resourceName(message.getUsername())
                .build();
        operationAuditMessageProducer.send(auditMessage);
    }

    @RabbitListener(queues = RabbitConfig.NFS_DIR_CREATED_Q)
    public void onNfsDirCreated(NfsDirCreatedMessage message) {
        EnnContext.setContext(message);

        String clusterName = message.getClusterName();
        String namespaceName = message.getNamespaceName();
        logger.info("RabbitMQ: received NfsDirCreatedMessage namespace={},cluster={}", namespaceName, clusterName);

        EventSource<NfsDirCreatedMessage> eventSource = getNsCreateEventSource(namespaceName, message);
        if (shouldHandleCreateEvent(eventSource, message)) {
            nfsDirProcessor.create(eventSource);
            namespaceCreationStatusChecker.check(EnnContext.getClusterName(), namespaceName, namespaceName);
        }
    }

    private boolean shouldHandleCreateEvent(EventSource eventSource, IMessage message) {
        NamespaceExtras namespaceExtras = namespaceExtrasRepository
                .findByNameAndDeletedIsFalse(eventSource.getNamespaceName());
        return namespaceExtras != null
                && namespaceExtras.getStatus() == NamespaceStatus.CREATE_PENDING
                && eventSource.getRequestId().equals(message.getRequestId());
    }

    private <T> EventSource<T> getNsCreateEventSource(String namespaceName,
                                                      T payload) {
        EventSource<T> eventSource = eventSourceRepository
                .findTopByNamespaceNameAndSourceTypeAndSourceNameOrderByIdDesc(namespaceName,
                        EventSourceType.NS_CREATE, namespaceName);
        eventSource.setPayload(payload);
        return eventSource;
    }

    /**
     * =======================================================================
     */

    @Transactional(rollbackFor = Exception.class)
    public EventSource<String> initNamespaceForDelete(String namespaceName) {
        EventSource<String> eventSource = new EventSource.Builder<String>()
                .namespaceName(namespaceName)
                .sourceType(EventSourceType.NS_DELETE)
                .sourceName(namespaceName)
                .payload(namespaceName)
                .build();
        EventSource<String> savedEventSource = eventSourceRepository.save(eventSource);
        namespaceExtrasRepository.updateStatusByNamespace(namespaceName,
                NamespaceStatus.DELETE_PENDING, EnnContext.getUserId());
        namespaceDeletionStatusChecker.start(EnnContext.getClusterName(), namespaceName, namespaceName);
        return savedEventSource;
    }

    public boolean deleteNamespace(String namespaceName,
                                   EventSource<String> eventSource) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        namespaceProcessor.delete(eventSource);
        namespaceDeletionStatusChecker.check(EnnContext.getClusterName(), namespaceName, namespaceName);

        sendStorageDeleteMessage(namespaceName);

        if (properties.getCurrentEbs().isEnabled()) {
            deleteEbsStorageClass(namespaceName);
        }
        sendDeleteAuditMessage(namespaceName, stopwatch);

        return true;
    }

    //TODO should move to a correct place and consider conflict with ceph rbd storage class
    private void deleteEbsStorageClass(String namespaceName) {
        client.deleteStorageClass(namespaceName);
    }

    protected void sendStorageDeleteMessage(String namespaceName) {
        NamespaceDeletedMessage message = new NamespaceDeletedMessage();
        message.setUserId(EnnContext.getUserId());
        message.setRequestId(EnnContext.getRequestId());
        message.setClusterName(EnnContext.getClusterName());
        message.setNamespaceName(namespaceName);

        if (properties.getCurrentCeph().isEnabled()) {
            try {
                messagingTemplate.convertAndSend(RabbitConfig.NAMESPACE_DELETED_Q, message);
            } catch (MessagingException e) {
                logger.error(String.format("Failed to delete ceph: %s", e));
            }
        }

        if (properties.getCurrentNfs().isEnabled()) {
            try {
                messagingTemplate.convertAndSend(RabbitConfig.NAMESPACE_RELEASE_NFS_Q, message);
            } catch (MessagingException e) {
                logger.error(String.format("Failed to delete nfs: %s", e));
            }
        }
    }

    protected void sendDeleteAuditMessage(String namespaceName, Stopwatch stopwatch) {
        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .elapsed(elapsed)
                .namespaceName(namespaceName)
                .operationType(OperationType.DELETE)
                .resourceType(ResourceType.NAMESPACE)
                .resourceName(namespaceName)
                .build();
        operationAuditMessageProducer.send(message);
    }

    @RabbitListener(queues = RabbitConfig.CEPH_POOL_DELETED_Q)
    public void onCephPoolDeleted(CephPoolDeletedMessage message) {
        EnnContext.setContext(message);

        String clusterName = message.getClusterName();
        String namespaceName = message.getNamespaceName();
        logger.info("RabbitMQ: received CephPoolDeletedMessage - cluster={} namespace={}", clusterName, namespaceName);

        EventSource<CephPoolDeletedMessage> eventSource = getNsDeleteEventSource(namespaceName, message);
        if (shouldHandleDeleteEvent(eventSource, message)) {
            cephPoolProcessor.delete(eventSource);
            namespaceDeletionStatusChecker.check(EnnContext.getClusterName(), namespaceName, namespaceName);
        }
    }

    @RabbitListener(queues = RabbitConfig.CEPH_FS_DIR_DELETED_Q)
    public void onCephFsDirDeleted(CephDirDeletedMessage message) {
        EnnContext.setContext(message);

        String clusterName = message.getClusterName();
        String namespaceName = message.getNamespaceName();
        logger.info("RabbitMQ: received CephDirDeletedMessage - namespace={}, cluster={}", namespaceName, clusterName);

        EventSource<CephDirDeletedMessage> eventSource = getNsDeleteEventSource(namespaceName, message);
        if (shouldHandleDeleteEvent(eventSource, message)) {
            cephFsDirProcessor.delete(eventSource);
            namespaceDeletionStatusChecker.check(EnnContext.getClusterName(), namespaceName, namespaceName);
        }
    }

    @RabbitListener(queues = RabbitConfig.CEPH_USER_DELETED_Q)
    public void onCephUserDeleted(CephUserDeletedMessage message) {
        EnnContext.setContext(message);

        String clusterName = message.getClusterName();
        String namespaceName = message.getNamespaceName();
        logger.info("RabbitMQ: received CephUserDeletedMessage - namespace={}, cluster={}", namespaceName, clusterName);

        EventSource<CephUserDeletedMessage> eventSource = getNsDeleteEventSource(namespaceName, message);
        if (shouldHandleDeleteEvent(eventSource, message)) {
            cephUserProcessor.delete(eventSource);
            namespaceDeletionStatusChecker.check(EnnContext.getClusterName(), namespaceName, namespaceName);
        }
    }

    @RabbitListener(queues = RabbitConfig.NFS_DIR_DELETED_Q)
    public void onNfsDirDeleted(NfsDirDeletedMessage message) {
        EnnContext.setContext(message);

        String clusterName = message.getClusterName();
        String namespaceName = message.getNamespaceName();
        logger.info("RabbitMQ: received NfsDirDeletedMessage - namespace={}", namespaceName);

        EventSource<NfsDirDeletedMessage> eventSource = getNsDeleteEventSource(namespaceName, message);
        if (shouldHandleDeleteEvent(eventSource, message)) {
            nfsDirProcessor.delete(eventSource);
            namespaceDeletionStatusChecker.check(EnnContext.getClusterName(), namespaceName, namespaceName);
        }
    }

    private boolean shouldHandleDeleteEvent(EventSource eventSource, IMessage message) {
        NamespaceExtras namespaceExtras =
                namespaceExtrasRepository.findByNameAndDeletedIsFalse(message.getNamespaceName());
        return namespaceExtras != null
                && namespaceExtras.getStatus() == NamespaceStatus.DELETE_PENDING
                && eventSource.getRequestId().equals(message.getRequestId());
    }

    private <T> EventSource<T> getNsDeleteEventSource(String namespaceName,
                                                      T payload) {
        EventSource<T> eventSource = eventSourceRepository
                .findTopByNamespaceNameAndSourceTypeAndSourceNameOrderByIdDesc(namespaceName,
                        EventSourceType.NS_DELETE, namespaceName);
        eventSource.setPayload(payload);
        return eventSource;
    }

    public boolean deleteLegacyNamespace(String namespaceName) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        if (properties.isAuthEnabled()) {
            keystoneProjectManagement.deleteProject(EnnContext.getClusterName(), namespaceName);
        }

        Boolean delete = clientManager.getClient()
                .namespaces()
                .withName(namespaceName)
                .delete();

        nsExtrasProcessor.deleteExtrasInfo(namespaceName);

        sendStorageDeleteMessage(namespaceName);

        sendDeleteAuditMessage(namespaceName, stopwatch);

        return delete != null && delete;
    }
}