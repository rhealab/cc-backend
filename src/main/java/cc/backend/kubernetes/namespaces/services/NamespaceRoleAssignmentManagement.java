package cc.backend.kubernetes.namespaces.services;

import cc.backend.audit.operation.OperationAuditMessage;
import cc.backend.audit.operation.OperationAuditMessageProducer;
import cc.backend.audit.operation.OperationType;
import cc.backend.audit.operation.ResourceType;
import cc.backend.audit.request.EnnContext;
import cc.backend.kubernetes.namespaces.RoleAssignmentHelper;
import cc.backend.role.assignment.RoleAssignmentDto;
import cc.backend.role.assignment.data.AssignmentExtrasRepository;
import cc.backend.role.assignment.data.RoleAssignmentExtras;
import cc.backend.user.data.User;
import cc.backend.user.data.UserRepository;
import cc.keystone.client.KeystoneRoleAssignmentManagement;
import cc.keystone.client.domain.AssignInfo;
import cc.keystone.client.domain.AssignResult;
import cc.keystone.client.domain.RoleAssignment;
import cc.keystone.client.domain.RoleType;
import cc.keystone.client.utils.HttpStatus;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static cc.backend.role.assignment.data.RoleAssignmentExtras.ActionType.ASSIGN;
import static cc.backend.role.assignment.data.RoleAssignmentExtras.ActionType.UNASSIGN;
import static cc.keystone.client.domain.AssignInfo.Operation.*;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/2/14.
 */
@Component
public class NamespaceRoleAssignmentManagement {
    private static final Logger logger = LoggerFactory.getLogger(NamespaceRoleAssignmentManagement.class);

    @Inject
    private KeystoneRoleAssignmentManagement keystoneRoleAssignmentManagement;

    @Inject
    private AssignmentExtrasRepository assignmentExtrasRepository;

    @Inject
    private OperationAuditMessageProducer operationAuditMessageProducer;

    @Inject
    private UserRepository userRepository;

    public List<AssignResult> updateNsUserRoles(String namespaceName, List<AssignInfo> updateList) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        updateList = RoleAssignmentHelper.cleanAssignInfo(updateList, null);
        updateList.forEach(assignInfo -> assignInfo.setOperation(Update));

        List<AssignResult> results = keystoneRoleAssignmentManagement
                .batchRoleGrant(EnnContext.getClusterName(), namespaceName, updateList);
        saveUpdateExtras(namespaceName, results);

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        sendAuditMessage(elapsed, OperationType.UPDATE, updateList, results);

        return results;
    }

    public List<AssignResult> grantRoles(String namespaceName,
                                         List<AssignInfo> assignInfoList) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        assignInfoList = RoleAssignmentHelper.cleanAssignInfo(assignInfoList);
        assignInfoList.forEach(assignInfo -> assignInfo.setOperation(Add));

        List<AssignResult> results = keystoneRoleAssignmentManagement
                .batchRoleGrant(EnnContext.getClusterName(), namespaceName, assignInfoList);
        saveAssignExtras(namespaceName, results);

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        sendAuditMessage(elapsed, OperationType.CREATE, assignInfoList, results);
        return results;
    }

    public List<AssignResult> revokeRoles(String namespaceName,
                                          List<AssignInfo> assignInfoList) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        assignInfoList = RoleAssignmentHelper.cleanAssignInfo(assignInfoList);
        assignInfoList.forEach(assignInfo -> assignInfo.setOperation(Remove));

        List<AssignResult> results = keystoneRoleAssignmentManagement
                .batchRoleGrant(EnnContext.getClusterName(), namespaceName, assignInfoList);
        saveUnAssignExtras(namespaceName, results);

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        sendAuditMessage(elapsed, OperationType.DELETE, assignInfoList, results);

        return results;
    }

    /**
     * =======================================================================
     */
    private void saveUpdateExtras(String namespaceName, List<AssignResult> results) {
        saveAssignExtras(namespaceName, results);
    }

    private void saveAssignExtras(String namespaceName, List<AssignResult> results) {
        List<RoleAssignmentExtras> extrasList = results.stream()
                .filter(result -> result.getHttpStatus() >= HttpStatus.OK.getStatusCode())
                .filter(result -> result.getHttpStatus() < HttpStatus.BAD_REQUEST.getStatusCode())
                .map(result -> {
                    RoleAssignmentExtras extras = assignmentExtrasRepository
                            .findByNamespaceAndUserId(namespaceName, result.getUserId());

                    if (extras == null) {
                        extras = new RoleAssignmentExtras();
                        extras.setNamespace(namespaceName);
                        extras.setUserId(result.getUserId());
                    }

                    extras.setRole(result.getRole());
                    extras.setActionType(ASSIGN);
                    return extras;
                })
                .collect(Collectors.toList());
        assignmentExtrasRepository.save(extrasList);
    }

    private void saveUnAssignExtras(String namespaceName, List<AssignResult> results) {
        List<String> userIdList = results.stream()
                .filter(result -> result.getHttpStatus() >= HttpStatus.OK.getStatusCode())
                .filter(result -> result.getHttpStatus() < HttpStatus.BAD_REQUEST.getStatusCode()
                        /*
                        if role deleted successfully from keystone,or role don't exists in keystone(user deleted from ldap
                        and keystone,but still exists in assignmentExtras),then set actionType to UNASSIGN.
                         */
                        || HttpStatus.NOT_FOUND.getStatusCode() == result.getHttpStatus())
                .map(AssignResult::getUserId)
                .collect(Collectors.toList());

        List<RoleAssignmentExtras> extrasList = new ArrayList<>();
        if (!userIdList.isEmpty()) {
            extrasList = assignmentExtrasRepository
                    .findByNamespaceAndUserIdIn(namespaceName, userIdList);
        }

        List<RoleAssignmentExtras> changedExtrasList = extrasList.stream()
                .peek(extra -> extra.setActionType(UNASSIGN))
                .collect(Collectors.toList());
        assignmentExtrasRepository.save(changedExtrasList);
    }

    /**
     * get a namespace's role assignment
     * pure sys admin can only view ns admin in namespace, TODO should split this to sys admin endpoint
     *
     * @param namespaceName the namespace name
     * @param userId        the user id
     * @return the role assignment list
     */
    public List<RoleAssignmentDto> getNamespaceRoleAssignment(String namespaceName,
                                                              String userId) {
        String clusterName = EnnContext.getClusterName();
        List<RoleAssignmentExtras> extrasList = assignmentExtrasRepository
                .findByNamespaceAndActionType(namespaceName, RoleAssignmentExtras.ActionType.ASSIGN);
        List<RoleAssignment> assignments =
                keystoneRoleAssignmentManagement.getProjectRoleAssignments(clusterName, namespaceName);

        if (!keystoneRoleAssignmentManagement.isProjectMember(clusterName, namespaceName, userId)) {
            extrasList = extrasList.stream()
                    .filter(extras -> RoleType.NS_ADMIN == extras.getRole())
                    .collect(Collectors.toList());

            /*
            if keystone and role_assignment_extras is consistent,then delete this.
             */
            assignments = assignments.stream()
                    .filter(assignment -> RoleType.NS_ADMIN == assignment.getRole())
                    .collect(Collectors.toList());
        }


        List<String> disabledUserIdList = extrasList.stream()
                .filter(extras -> extras.getStatus() == RoleAssignmentExtras.Status.DISABLED)
                .map(RoleAssignmentExtras::getUserId)
                .collect(Collectors.toList());

        List<User> disabledUserList = userRepository.findByUserIdIn(disabledUserIdList);
        return composeRoleAssignmentsDto(disabledUserList, extrasList, assignments);
    }

    /**
     * compose role assignment dto
     * <p>
     * When a user is deleted from ldap, 2 things will happen
     * 1. keystone will delete role assignment
     * 2. set user's status to DISABLED in our database
     * <p>
     * so, we should consider these deleted user's role assignment,
     * and tell user in the namespace, this user is disabled
     *
     * @param disabledUserList disabled user list
     * @param extrasList       role assignment extras info list (when a user is deleted, only this record can be seen)
     * @param assignments      the role assignment from keystone
     * @return the dto list
     */
    private List<RoleAssignmentDto> composeRoleAssignmentsDto(List<User> disabledUserList,
                                                              List<RoleAssignmentExtras> extrasList,
                                                              List<RoleAssignment> assignments) {
        Map<String, RoleAssignmentDto> dtoMap = assignments.stream()
                .map(assignment -> {
                    RoleAssignmentDto dto = new RoleAssignmentDto();
                    dto.setUserId(assignment.getUserId());
                    dto.setNamespace(assignment.getProject());
                    dto.setUsername(assignment.getUserName());
                    dto.setRoleName(assignment.getRole().name().toLowerCase());
                    dto.setStatus(RoleAssignmentExtras.Status.ENABLED);
                    return dto;
                })
                .collect(Collectors.toMap(RoleAssignmentDto::getUserId, dto -> dto));

        extrasList.forEach(extras -> {
            RoleAssignmentDto dto = dtoMap.get(extras.getUserId());
            if (dto != null) {
                dto.setAssignedBy(extras.getAssignedBy());
                dto.setAssignedDate(extras.getAssignedOn());
                return;
            }

            if (extras.getStatus() == RoleAssignmentExtras.Status.DISABLED) {
                dto = new RoleAssignmentDto();
                dto.setUserId(extras.getUserId());
                dto.setNamespace(extras.getNamespace());
                dto.setRoleName(extras.getRole().name().toLowerCase());
                dto.setAssignedBy(extras.getAssignedBy());
                dto.setAssignedDate(extras.getAssignedOn());
                dto.setStatus(extras.getStatus());
                dtoMap.put(extras.getUserId(), dto);
            }
        });

        disabledUserList.forEach(user -> {
            RoleAssignmentDto dto = dtoMap.get(user.getUserId());
            if (dto != null) {
                dto.setUsername(user.getUsername());
            }
        });

        return new ArrayList<>(dtoMap.values());
    }

    private void sendAuditMessage(long elapsed,
                                  OperationType operationType,
                                  List<AssignInfo> requests,
                                  List<AssignResult> results) {
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .elapsed(elapsed)
                .operationType(operationType)
                .resourceType(ResourceType.ROLE_ASSIGNMENT)
                .extras(ImmutableMap.of("details", requests.toString(),
                        "results", results.toString()))
                .build();
        operationAuditMessageProducer.send(message);
    }
}