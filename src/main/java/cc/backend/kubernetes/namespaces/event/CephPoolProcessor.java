package cc.backend.kubernetes.namespaces.event;

import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceType;
import cc.backend.event.processor.AbstractProcessor;
import cc.backend.event.processor.ICreateProcessor;
import cc.backend.event.processor.IDeleteProcessor;
import cc.backend.kubernetes.namespaces.data.NamespaceExtrasRepository;
import cc.backend.kubernetes.namespaces.messages.CephPoolCreatedMessage;
import cc.backend.kubernetes.namespaces.messages.CephPoolDeletedMessage;
import cc.backend.kubernetes.namespaces.messages.CephPoolResizedMessage;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/13.
 */
@Component
public class CephPoolProcessor extends AbstractProcessor<CephPoolCreatedMessage, CephPoolResizedMessage, CephPoolDeletedMessage> {

    @Inject
    private K8sStorageClassProcessor storageClassProcessor;

    @Inject
    private NamespaceExtrasRepository namespaceExtrasRepository;

    @Override
    protected void doUpdate(EventSource<CephPoolResizedMessage> eventSource) {
        namespaceExtrasRepository.updateCephPoolStorageQuotaByNamespace(eventSource.getNamespaceName(),
                eventSource.getPayload().getRbdAmountBytes());
    }

    @Override
    public EventName getCreateEventName() {
        return EventName.CREATE_CEPH_POOL;
    }

    @Override
    public ICreateProcessor<CephPoolCreatedMessage> getNextCreateProcessor() {
        return storageClassProcessor;
    }

    @Override
    public EventName getUpdateEventName() {
        return EventName.UPDATE_CEPH_POOL;
    }

    @Override
    public EventName getDeleteEventName() {
        return EventName.DELETE_CEPH_POOL;
    }

    @Override
    public IDeleteProcessor<CephPoolDeletedMessage> getNextDeleteProcessor() {
        return storageClassProcessor;
    }

    @Override
    public boolean needToDelete(EventSource<CephPoolDeletedMessage> eventSource) {
        return getEvent(eventSource.getNamespaceName(),
                EventSourceType.NS_CREATE,
                eventSource.getSourceName(),
                getCreateEventName()) != null;
    }
}
