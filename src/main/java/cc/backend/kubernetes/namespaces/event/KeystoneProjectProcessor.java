package cc.backend.kubernetes.namespaces.event;

import cc.backend.EnnProperties;
import cc.backend.audit.request.EnnContext;
import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceType;
import cc.backend.event.processor.AbstractProcessor;
import cc.backend.event.processor.ICreateProcessor;
import cc.backend.event.processor.IDeleteProcessor;
import cc.backend.kubernetes.namespaces.dto.NamespaceCreateRequest;
import cc.keystone.client.KeystoneProjectManagement;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/2.
 */
@Component
public class KeystoneProjectProcessor extends AbstractProcessor<NamespaceCreateRequest,
        Void, String> {

    private final KeystoneProjectManagement keystoneProjectManagement;

    private final KeystoneRoleProcessor roleProcessor;

    private final EnnProperties properties;

    @Inject
    public KeystoneProjectProcessor(KeystoneProjectManagement keystoneProjectManagement,
                                    KeystoneRoleProcessor roleProcessor,
                                    EnnProperties properties) {
        this.keystoneProjectManagement = keystoneProjectManagement;
        this.roleProcessor = roleProcessor;
        this.properties = properties;
    }

    @Override
    protected void doCreate(EventSource<NamespaceCreateRequest> eventSource) {
        if (properties.isAuthEnabled()) {
            keystoneProjectManagement.createProject(EnnContext.getClusterName(), eventSource.getNamespaceName());
        }
    }

    @Override
    protected void doDelete(EventSource<String> eventSource) {
        if (properties.isAuthEnabled()) {
            keystoneProjectManagement.deleteProject(EnnContext.getClusterName(), eventSource.getNamespaceName());
        }
    }

    @Override
    public boolean needToDelete(EventSource<String> eventSource) {
        return getEvent(eventSource.getNamespaceName(),
                EventSourceType.NS_CREATE,
                eventSource.getSourceName(),
                EventName.CREATE_KEYSTONE_PROJECT) != null;
    }

    @Override
    public EventName getCreateEventName() {
        return EventName.CREATE_KEYSTONE_PROJECT;
    }

    @Override
    public EventName getDeleteEventName() {
        return EventName.DELETE_KEYSTONE_PROJECT;
    }

    @Override
    public ICreateProcessor<NamespaceCreateRequest> getNextCreateProcessor() {
        return roleProcessor;
    }

    @Override
    public IDeleteProcessor<String> getNextDeleteProcessor() {
        return roleProcessor;
    }
}
