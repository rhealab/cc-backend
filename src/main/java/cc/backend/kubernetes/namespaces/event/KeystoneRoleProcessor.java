package cc.backend.kubernetes.namespaces.event;

import cc.backend.EnnProperties;
import cc.backend.event.data.Event;
import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceType;
import cc.backend.event.processor.AbstractProcessor;
import cc.backend.event.processor.ICreateProcessor;
import cc.backend.event.processor.IDeleteProcessor;
import cc.backend.kubernetes.namespaces.dto.NamespaceCreateRequest;
import cc.backend.kubernetes.namespaces.services.NamespaceRoleAssignmentManagement;
import cc.keystone.client.domain.AssignInfo;
import cc.keystone.client.domain.RoleType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/2.
 */
@Component
public class KeystoneRoleProcessor extends AbstractProcessor<NamespaceCreateRequest, Void, String> {
    private static final Logger logger = LoggerFactory.getLogger(KeystoneRoleProcessor.class);

    private final NamespaceRoleAssignmentManagement namespaceRoleAssignmentManagement;

    private final NsExtrasProcessor extrasProcessor;

    private final NsSchedulingProcessor nsSchedulingProcessor;

    private final EnnProperties properties;

    @Inject
    public KeystoneRoleProcessor(NamespaceRoleAssignmentManagement namespaceRoleAssignmentManagement,
                                 NsExtrasProcessor extrasProcessor,
                                 NsSchedulingProcessor nsSchedulingProcessor,
                                 EnnProperties properties) {
        this.namespaceRoleAssignmentManagement = namespaceRoleAssignmentManagement;
        this.extrasProcessor = extrasProcessor;
        this.nsSchedulingProcessor = nsSchedulingProcessor;
        this.properties = properties;
    }

    @Override
    public void create(EventSource<NamespaceCreateRequest> eventSource) {
        if (properties.isAuthEnabled()) {
            try {
                List<AssignInfo> assignInfoList = composeAssignInfoList(eventSource.getPayload().getAdminList());
                namespaceRoleAssignmentManagement.grantRoles(eventSource.getNamespaceName(), assignInfoList);
            } catch (Exception e) {
                logger.error("namespace - {} role assign error - {}", eventSource.getNamespaceName(), e);
            }
        }
        super.create(eventSource);
    }

    @Override
    public void delete(EventSource<String> eventSource) {
        if (needToDelete(eventSource)) {
            doDelete(eventSource);
            Event<String> event = composeDeleteEvent(eventSource);
            event.setPayload("ignored because of delete keystone project");
            saveEvent(event);
        }

        if (getNextDeleteProcessor() != null) {
            getNextDeleteProcessor().delete(eventSource);
        }
    }

    private List<AssignInfo> composeAssignInfoList(List<String> userIdList) {
        List<AssignInfo> assignInfoList = new ArrayList<>();
        if (userIdList != null) {
            for (String userId : userIdList) {
                AssignInfo assignInfo = new AssignInfo();
                assignInfo.setUserId(userId);
                assignInfo.setRole(RoleType.NS_ADMIN);
                assignInfoList.add(assignInfo);
            }
        }
        return assignInfoList;
    }

    @Override
    public EventName getCreateEventName() {
        return EventName.CREATE_KEYSTONE_ROLE_ASSIGN;
    }

    @Override
    public EventName getDeleteEventName() {
        return EventName.DELETE_KEYSTONE_ROLE_ASSIGN;
    }

    @Override
    public ICreateProcessor<NamespaceCreateRequest> getNextCreateProcessor() {
        return nsSchedulingProcessor;
    }

    @Override
    public IDeleteProcessor<String> getNextDeleteProcessor() {
        return extrasProcessor;
    }

    @Override
    public boolean needToDelete(EventSource<String> eventSource) {
        return getEvent(eventSource.getNamespaceName(),
                EventSourceType.NS_CREATE,
                eventSource.getSourceName(),
                EventName.CREATE_KEYSTONE_ROLE_ASSIGN) != null;
    }
}
