package cc.backend.kubernetes.namespaces.event;

import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceType;
import cc.backend.event.processor.AbstractProcessor;
import cc.backend.kubernetes.namespaces.messages.NfsDirCreatedMessage;
import cc.backend.kubernetes.namespaces.messages.NfsDirDeletedMessage;
import org.springframework.stereotype.Component;

/**
 * Created by NormanWang06@gmail.com (jinxin) on 2017/6/13.
 */
@Component
public class NfsDirProcessor extends AbstractProcessor<NfsDirCreatedMessage, Void, NfsDirDeletedMessage> {

    @Override
    public EventName getCreateEventName() {
        return EventName.CREATE_NFS_DIR;
    }

    @Override
    public EventName getDeleteEventName() {
        return EventName.DELETE_NFS_DIR;
    }

    @Override
    public boolean needToDelete(EventSource<NfsDirDeletedMessage> eventSource) {
        return getEvent(eventSource.getNamespaceName(),
                EventSourceType.NS_CREATE,
                eventSource.getSourceName(),
                getCreateEventName()) != null;
    }
}
