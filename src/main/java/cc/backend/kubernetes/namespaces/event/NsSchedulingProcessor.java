package cc.backend.kubernetes.namespaces.event;

import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;
import cc.backend.event.processor.AbstractProcessor;
import cc.backend.kubernetes.namespaces.dto.NamespaceCreateRequest;
import cc.backend.kubernetes.scheduling.services.SchedulingManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/13.
 */
@Component
public class NsSchedulingProcessor extends AbstractProcessor<NamespaceCreateRequest, Void, String> {
    private static final Logger logger = LoggerFactory.getLogger(KeystoneRoleProcessor.class);

    @Inject
    private SchedulingManagement schedulingService;

    @Override
    protected void doCreate(EventSource<NamespaceCreateRequest> eventSource) {
        try {
            schedulingService.addLabelsToNamespaces(eventSource.getNamespaceName(), schedulingService.getDefaultLabels());
        } catch (Exception e) {
            logger.error("namespace - {} labels add error - {}", eventSource.getNamespaceName(), e);
        }
    }

    //TODO check if delete failed will has problem
    @Override
    protected void doDelete(EventSource<String> eventSource) {
        try {
            schedulingService.deleteAllLabelsOfNamespace(eventSource.getNamespaceName());
        } catch (Exception e) {
            logger.error("namespace - {} labels delete error - {}", eventSource.getNamespaceName(), e);
        }
    }

    @Override
    public EventName getCreateEventName() {
        return EventName.CREATE_K8S_NS_SCHEDULE;
    }

    @Override
    public EventName getDeleteEventName() {
        return EventName.DELETE_K8S_NS_SCHEDULE;
    }
}
