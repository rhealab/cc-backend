package cc.backend.kubernetes.namespaces.event;

import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceType;
import cc.backend.event.processor.AbstractProcessor;
import cc.backend.event.processor.ICreateProcessor;
import cc.backend.event.processor.IDeleteProcessor;
import cc.backend.kubernetes.namespaces.messages.CephUserCreatedMessage;
import cc.backend.kubernetes.namespaces.messages.CephUserDeletedMessage;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * Created by NormanWang06@gmail.com (jinxin) on 2017/6/13.
 */
@Component
public class CephUserProcessor extends AbstractProcessor<CephUserCreatedMessage, Void, CephUserDeletedMessage> {

    @Inject
    private K8sSecretProcessor secretProcessor;

    @Override
    public EventName getCreateEventName() {
        return EventName.CREATE_CEPH_USER;
    }

    @Override
    public ICreateProcessor<CephUserCreatedMessage> getNextCreateProcessor() {
        return secretProcessor;
    }

    @Override
    public EventName getDeleteEventName() {
        return EventName.DELETE_CEPH_USER;
    }

    @Override
    public IDeleteProcessor<CephUserDeletedMessage> getNextDeleteProcessor() {
        return secretProcessor;
    }

    @Override
    public boolean needToDelete(EventSource<CephUserDeletedMessage> eventSource) {
        return getEvent(eventSource.getNamespaceName(),
                EventSourceType.NS_CREATE,
                eventSource.getSourceName(),
                getCreateEventName()) != null;
    }
}
