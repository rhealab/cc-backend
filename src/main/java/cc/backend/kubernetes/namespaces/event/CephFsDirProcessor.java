package cc.backend.kubernetes.namespaces.event;

import cc.backend.event.processor.AbstractProcessor;
import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSourceType;
import cc.backend.event.data.EventSource;
import cc.backend.kubernetes.namespaces.messages.CephDirCreatedMessage;
import cc.backend.kubernetes.namespaces.messages.CephDirDeletedMessage;
import org.springframework.stereotype.Component;

/**
 * Created by NormanWang06@gmail.com (jinxin) on 2017/6/13.
 */
@Component
public class CephFsDirProcessor extends AbstractProcessor<CephDirCreatedMessage, Void, CephDirDeletedMessage> {

    @Override
    public EventName getCreateEventName() {
        return EventName.CREATE_CEPH_FS_DIR;
    }

    @Override
    public EventName getDeleteEventName() {
        return EventName.DELETE_CEPH_FS_DIR;
    }

    @Override
    public boolean needToDelete(EventSource<CephDirDeletedMessage> eventSource) {
        return getEvent(eventSource.getNamespaceName(),
                EventSourceType.NS_CREATE,
                eventSource.getSourceName(),
                getCreateEventName()) != null;
    }
}
