package cc.backend.kubernetes.namespaces.event;

import cc.backend.event.data.Event;
import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;
import cc.backend.event.processor.AbstractProcessor;
import cc.backend.kubernetes.namespaces.data.NamespaceExtras;
import cc.backend.kubernetes.namespaces.data.NamespaceExtrasRepository;
import cc.backend.kubernetes.namespaces.dto.NamespaceUpdateRequest;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * Created by NormanWang06@gmail.com (jinxin) on 2017/6/8.
 */
@Component
public class NsHostpathQuotaProcessor extends AbstractProcessor<Void, NamespaceUpdateRequest, Void> {
    @Inject
    private NamespaceExtrasRepository namespaceExtrasRepository;

    public void updateWithQuotaCheck(EventSource<NamespaceUpdateRequest> eventSource, NamespaceExtras namespaceExtras) {
        Event<NamespaceUpdateRequest> event = composeUpdateEvent(eventSource);

        NamespaceUpdateRequest request = eventSource.getPayload();
        if (!shouldIgnore(request, namespaceExtras)) {
            namespaceExtrasRepository.updateHostpathStorageQuotaByNamespace(request.getName(),
                    request.getHostPathStorageBytes());
        } else {
            event.setPayloadJson("ignored with hostpath bytes not changed");
        }

        saveEvent(event);
    }

    @Override
    protected void doUpdate(EventSource<NamespaceUpdateRequest> eventSource) {
        NamespaceUpdateRequest request = eventSource.getPayload();
        namespaceExtrasRepository.updateHostpathStorageQuotaByNamespace(request.getName(),
                request.getHostPathStorageBytes());
    }

    // when hostpath bytes is less than 0 or not changed should be ignored
    private boolean shouldIgnore(NamespaceUpdateRequest request, NamespaceExtras namespaceExtras) {
        return request.getHostPathStorageBytes() == null
                || namespaceExtras.getHostpathStorageBytes() == request.getHostPathStorageBytes();
    }

    @Override
    public boolean needToUpdate(EventSource<NamespaceUpdateRequest> eventSource) {
        NamespaceUpdateRequest request = eventSource.getPayload();
        return request.getHostPathStorageBytes() != null;
    }

    @Override
    public EventName getUpdateEventName() {
        return EventName.UPDATE_NS_HOSTPATH_QUOTA;
    }
}
