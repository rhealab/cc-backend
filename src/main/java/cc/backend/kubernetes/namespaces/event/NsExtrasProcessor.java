package cc.backend.kubernetes.namespaces.event;

import cc.backend.EnnProperties;
import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceType;
import cc.backend.event.processor.AbstractProcessor;
import cc.backend.event.processor.IDeleteProcessor;
import cc.backend.kubernetes.apps.services.AppManagement;
import cc.backend.kubernetes.workload.deployment.data.DeploymentExtrasRepository;
import cc.backend.kubernetes.namespaces.data.NamespaceExtras;
import cc.backend.kubernetes.namespaces.data.NamespaceExtrasRepository;
import cc.backend.kubernetes.namespaces.data.NamespaceStatus;
import cc.backend.kubernetes.namespaces.data.UserNamespaceAccessInfoRepository;
import cc.backend.kubernetes.namespaces.dto.NamespaceCreateRequest;
import cc.backend.kubernetes.service.data.ServiceExtrasRepository;
import cc.backend.kubernetes.storage.StorageManagement;
import cc.backend.role.assignment.data.AssignmentExtrasRepository;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Optional;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/2.
 */
@Component
public class NsExtrasProcessor extends AbstractProcessor<NamespaceCreateRequest, NamespaceCreateRequest, String> {

    @Inject
    private UserNamespaceAccessInfoRepository accessInfoRepository;

    @Inject
    private AssignmentExtrasRepository assignmentExtrasRepository;

    @Inject
    private AppManagement appManagement;

    @Inject
    private ServiceExtrasRepository serviceExtrasRepository;

    @Inject
    private DeploymentExtrasRepository deploymentExtrasRepository;

    @Inject
    private StorageManagement storageManagement;

    @Inject
    private NamespaceExtrasRepository namespaceExtrasRepository;

    @Inject
    private EnnProperties properties;

    @Inject
    private NsSchedulingProcessor nsSchedulingProcessor;

    @Override
    protected void doCreate(EventSource<NamespaceCreateRequest> eventSource) {
        createNamespaceExtras(eventSource.getPayload());
    }

    private void createNamespaceExtras(NamespaceCreateRequest request) {
        NamespaceExtras namespaceExtras = new NamespaceExtras();
        namespaceExtras.setName(request.getName());
        namespaceExtras.setStatus(NamespaceStatus.CREATE_PENDING);
        namespaceExtras.setHostpathStorageBytes(request.getHostPathStorageBytes());
        namespaceExtras.setPoolBytes(request.getPoolBytes());
        namespaceExtras.setEbsBytes(Optional.ofNullable(request.getEbsBytes()).orElse(0L));

        namespaceExtras.setAllowHostpath(request.isAllowHostpath());
        namespaceExtras.setAllowPrivilege(request.isAllowPrivilege());
        namespaceExtras.setAllowCriticalPod(request.isAllowCriticalPod());

        namespaceExtras.setCephInitialized(properties.getCurrentCeph().isEnabled());
        namespaceExtras.setNfsInitialized(properties.getCurrentNfs().isEnabled());

        namespaceExtrasRepository.save(namespaceExtras);
    }

    @Override
    protected void doDelete(EventSource<String> eventSource) {
        deleteExtrasInfo(eventSource.getNamespaceName());
    }

    /**
     * delete namespace's resource extra info in db
     * <p>
     * FIXME should not delete these info should set the delete flag to true
     *
     * @param namespaceName the namespace for delete
     */
    public void deleteExtrasInfo(String namespaceName) {
        accessInfoRepository.deleteByNamespace(namespaceName);
//        assignmentExtrasRepository.deleteByNamespace(namespaceName);
        appManagement.deleteAppByNamespace(namespaceName);
        storageManagement.deleteStorageByNamespace(namespaceName);
        serviceExtrasRepository.deleteByNamespace(namespaceName);
        deploymentExtrasRepository.deleteByNamespace(namespaceName);
    }

    @Override
    public boolean needToDelete(EventSource<String> eventSource) {
        return getEvent(eventSource.getNamespaceName(),
                EventSourceType.NS_CREATE,
                eventSource.getSourceName(),
                getCreateEventName()) != null;
    }

    @Override
    public EventName getCreateEventName() {
        return EventName.CREATE_NS_EXTRAS;
    }

    @Override
    public EventName getDeleteEventName() {
        return EventName.DELETE_NS_EXTRAS;
    }

    @Override
    public IDeleteProcessor<String> getNextDeleteProcessor() {
        return nsSchedulingProcessor;
    }
}
