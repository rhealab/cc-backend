package cc.backend.kubernetes.namespaces.event;

import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceType;
import cc.backend.event.processor.AbstractProcessor;
import cc.backend.event.processor.ICreateProcessor;
import cc.backend.event.processor.IDeleteProcessor;
import cc.backend.event.processor.IUpdateProcessor;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.utils.ResourceLabelHelper;
import cc.backend.kubernetes.namespaces.dto.NamespaceCreateRequest;
import cc.backend.kubernetes.namespaces.dto.NamespaceUpdateRequest;
import io.fabric8.kubernetes.api.model.DoneableNamespace;
import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.NamespaceFluent;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

import static cc.backend.kubernetes.Constants.*;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/13.
 */
@Component
public class K8sNsProcessor extends AbstractProcessor<NamespaceCreateRequest, NamespaceUpdateRequest, String> {

    private final KubernetesClientManager clientManager;

    private final K8sNsQuotaProcessor quotaProcessor;

    @Inject
    public K8sNsProcessor(KubernetesClientManager clientManager, K8sNsQuotaProcessor quotaProcessor) {
        this.clientManager = clientManager;
        this.quotaProcessor = quotaProcessor;
    }

    @Override
    protected void doCreate(EventSource<NamespaceCreateRequest> eventSource) {
        NamespaceCreateRequest request = eventSource.getPayload();

        Namespace namespace = new Namespace();
        ObjectMeta metadata = new ObjectMeta();

        Map<String, String> annotations = new HashMap<>();

        if (request.isAllowHostpath()) {
            annotations.put(NAMESPACE_ALLOW_HOSTPATH_ANNOTATION, "true");
        }

        if (request.isAllowPrivilege()) {
            annotations.put(NAMESPACE_ALLOW_PRIVILEGE_ANNOTATION, "true");
        }

        if (request.isAllowCriticalPod()) {
            annotations.put(NAMESPACE_ALLOW_CRITICAL_POD_ANNOTATION, "true");
        }

        metadata.setName(eventSource.getNamespaceName());
        metadata.setAnnotations(annotations);
        namespace.setMetadata(metadata);
        ResourceLabelHelper.addLabel(namespace, NAMESPACE_CREATED_BY_LABEL, CONSOLE_NAME);

        clientManager.getClient().namespaces().create(namespace);
    }

    @Override
    protected void doUpdate(EventSource<NamespaceUpdateRequest> eventSource) {
        NamespaceUpdateRequest request = eventSource.getPayload();

        Boolean allowHostpath = request.getAllowHostpath();
        Boolean allowPrivilege = request.getAllowPrivilege();
        Boolean allowCriticalPod = request.getAllowCriticalPod();

        NamespaceFluent.MetadataNested<DoneableNamespace> edit = clientManager.getClient()
                .namespaces()
                .withName(request.getName())
                .edit()
                .editMetadata();
        if (allowHostpath != null) {
            edit.addToAnnotations(NAMESPACE_ALLOW_HOSTPATH_ANNOTATION, allowHostpath.toString());
        }

        if (allowPrivilege != null) {
            edit.addToAnnotations(NAMESPACE_ALLOW_PRIVILEGE_ANNOTATION, allowPrivilege.toString());
        }

        if (allowCriticalPod != null) {
            edit.addToAnnotations(NAMESPACE_ALLOW_CRITICAL_POD_ANNOTATION, allowCriticalPod.toString());
        }

        edit.endMetadata().done();
    }

    @Override
    protected void doDelete(EventSource<String> eventSource) {
        KubernetesClient client = clientManager.getClient();
        client.namespaces().withName(eventSource.getNamespaceName()).delete();
    }

    @Override
    public EventName getCreateEventName() {
        return EventName.CREATE_K8S_NS;
    }

    @Override
    public EventName getUpdateEventName() {
        return EventName.UPDATE_K8S_NS;
    }

    @Override
    public EventName getDeleteEventName() {
        return EventName.DELETE_K8S_NS;
    }

    @Override
    public ICreateProcessor<NamespaceCreateRequest> getNextCreateProcessor() {
        return quotaProcessor;
    }

    @Override
    public IUpdateProcessor<NamespaceUpdateRequest> getNextUpdateProcessor() {
        return quotaProcessor;
    }

    @Override
    public IDeleteProcessor<String> getNextDeleteProcessor() {
        return quotaProcessor;
    }

    @Override
    public boolean needToUpdate(EventSource<NamespaceUpdateRequest> eventSource) {
        NamespaceUpdateRequest request = eventSource.getPayload();
        return request.getAllowHostpath() != null ||
                request.getAllowPrivilege() != null ||
                request.getAllowCriticalPod() != null;
    }

    @Override
    public boolean needToDelete(EventSource<String> eventSource) {
        return getEvent(eventSource.getNamespaceName(),
                EventSourceType.NS_CREATE,
                eventSource.getSourceName(),
                EventName.CREATE_K8S_NS) != null;
    }
}
