package cc.backend.kubernetes.namespaces.event;

import cc.backend.event.processor.AbstractProcessor;
import cc.backend.event.data.Event;
import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceType;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.namespaces.messages.CephUserCreatedMessage;
import cc.backend.kubernetes.namespaces.messages.CephUserDeletedMessage;
import com.google.common.collect.ImmutableMap;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Base64;

/**
 * Created by NormanWang06@gmail.com (jinxin) on 2017/6/2.
 */
@Component
public class K8sSecretProcessor extends AbstractProcessor<CephUserCreatedMessage,
        Void, CephUserDeletedMessage> {

    @Inject
    private KubernetesClientManager clientManager;

    @Override
    protected void doCreate(EventSource<CephUserCreatedMessage> eventSource) {
        CephUserCreatedMessage message = eventSource.getPayload();
        String namespaceName = message.getNamespaceName();
        String username = message.getUsername();
        createCephUserSecret(namespaceName, username, message.getKeyring());
    }

    private void createCephUserSecret(String namespace, String username, String keyring) {
        clientManager.getClient().secrets().inNamespace(namespace).createNew()
                .withNewMetadata().withName(username).endMetadata()
                .withApiVersion("v1")
                .withType("kubernetes.io/rbd")
                .withData(ImmutableMap.of("key", Base64.getEncoder().encodeToString(keyring.getBytes())))
                .done();
    }

    @Override
    public void delete(EventSource<CephUserDeletedMessage> eventSource) {
        if (needToDelete(eventSource)) {
            doDelete(eventSource);
            Event<CephUserDeletedMessage> event = composeDeleteEvent(eventSource);
            event.setPayloadJson("ignored because of delete namespace");
            saveEvent(event);
        }
    }

    @Override
    public boolean needToDelete(EventSource<CephUserDeletedMessage> eventSource) {
        return getEvent(eventSource.getNamespaceName(),
                EventSourceType.NS_CREATE,
                eventSource.getSourceName(),
                getCreateEventName()) != null;
    }

    @Override
    public EventName getCreateEventName() {
        return EventName.CREATE_K8S_SECRET;
    }

    @Override
    public EventName getDeleteEventName() {
        return EventName.DELETE_K8S_SECRET;
    }

}
