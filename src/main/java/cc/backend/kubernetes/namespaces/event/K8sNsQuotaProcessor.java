package cc.backend.kubernetes.namespaces.event;

import cc.backend.event.data.Event;
import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceType;
import cc.backend.event.processor.AbstractProcessor;
import cc.backend.event.processor.ICreateProcessor;
import cc.backend.event.processor.IDeleteProcessor;
import cc.backend.event.processor.IUpdateProcessor;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.namespaces.dto.NamespaceCreateRequest;
import cc.backend.kubernetes.namespaces.dto.NamespaceUpdateRequest;
import cc.backend.kubernetes.utils.UnitUtils;
import io.fabric8.kubernetes.api.model.Quantity;
import io.fabric8.kubernetes.api.model.ResourceQuota;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

import static cc.backend.kubernetes.Constants.*;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/2.
 */
@Component
public class K8sNsQuotaProcessor extends AbstractProcessor<NamespaceCreateRequest,
        NamespaceUpdateRequest, String> {

    private final KubernetesClientManager clientManager;

    private final K8sNsLimitRangeProcessor k8sNsLimitRangeProcessor;

    private final NsHostpathQuotaProcessor nsHostpathQuotaProcessor;

    @Inject
    public K8sNsQuotaProcessor(KubernetesClientManager clientManager,
                               K8sNsLimitRangeProcessor k8sNsLimitRangeProcessor,
                               NsHostpathQuotaProcessor nsHostpathQuotaProcessor) {
        this.clientManager = clientManager;
        this.k8sNsLimitRangeProcessor = k8sNsLimitRangeProcessor;
        this.nsHostpathQuotaProcessor = nsHostpathQuotaProcessor;
    }

    @Override
    protected void doCreate(EventSource<NamespaceCreateRequest> eventSource) {
        NamespaceCreateRequest request = eventSource.getPayload();
        KubernetesClient client = clientManager.getClient();
        client.resourceQuotas()
                .inNamespace(request.getName())
                .create(request.composeResourceQuota());
    }

    @Override
    protected void doUpdate(EventSource<NamespaceUpdateRequest> eventSource) {
        NamespaceUpdateRequest request = eventSource.getPayload();

        KubernetesClient client = clientManager.getClient();

        String namespaceName = request.getName();
        String quotaName = request.quotaName();

        ResourceQuota currentQuota = client.resourceQuotas()
                .inNamespace(namespaceName)
                .withName(quotaName)
                .get();

        Map<String, Quantity> newHard = composeNewHard(request, currentQuota);

        client.resourceQuotas()
                .inNamespace(namespaceName)
                .withName(quotaName)
                .edit()
                .editMetadata()
                .endMetadata()
                .editSpec()
                .withHard(newHard)
                .endSpec()
                .done();
    }

    private Map<String, Quantity> composeNewHard(NamespaceUpdateRequest request,
                                                 ResourceQuota currentQuota) {
        Map<String, Quantity> hard = currentQuota.getSpec().getHard();
        if (hard == null) {
            hard = new HashMap<>(4);
        }

        if (request.getCpuRequest() != null) {
            hard.put(K_NAMESPACE_CPU_REQUESTS, UnitUtils.toK8sCpuQuantity(request.getCpuRequest()));
        }
        if (request.getCpuLimit() != null) {
            hard.put(K_NAMESPACE_CPU_LIMITS, UnitUtils.toK8sCpuQuantity(request.getCpuLimit()));
        }

        if (request.getMemoryRequestBytes() != null) {
            hard.put(K_NAMESPACE_MEMORY_REQUESTS, UnitUtils.toK8sMemoryQuantity(request.getMemoryRequestBytes()));
        }
        if (request.getMemoryLimitBytes() != null) {
            hard.put(K_NAMESPACE_MEMORY_LIMITS, UnitUtils.toK8sMemoryQuantity(request.getMemoryLimitBytes()));
        }

        return hard;
    }

    @Override
    public void delete(EventSource<String> eventSource) {
        if (needToDelete(eventSource)) {
            doDelete(eventSource);
            Event<String> event = composeDeleteEvent(eventSource);
            event.setPayload("ignored because of delete namespace");
            saveEvent(event);
        }

        if (getNextDeleteProcessor() != null) {
            getNextDeleteProcessor().delete(eventSource);
        }
    }

    @Override
    public boolean needToUpdate(EventSource<NamespaceUpdateRequest> eventSource) {
        return true;
    }

    @Override
    public boolean needToDelete(EventSource<String> eventSource) {
        return getEvent(eventSource.getNamespaceName(),
                EventSourceType.NS_CREATE,
                eventSource.getSourceName(),
                EventName.CREATE_K8S_NS_QUOTA) != null;
    }

    @Override
    public EventName getCreateEventName() {
        return EventName.CREATE_K8S_NS_QUOTA;
    }

    @Override
    public EventName getUpdateEventName() {
        return EventName.UPDATE_K8S_NS_QUOTA;
    }

    @Override
    public EventName getDeleteEventName() {
        return EventName.DELETE_K8S_NS_QUOTA;
    }

    @Override
    public ICreateProcessor<NamespaceCreateRequest> getNextCreateProcessor() {
        return k8sNsLimitRangeProcessor;
    }

    @Override
    public IUpdateProcessor<NamespaceUpdateRequest> getNextUpdateProcessor() {
        return nsHostpathQuotaProcessor;
    }

    @Override
    public IDeleteProcessor<String> getNextDeleteProcessor() {
        return k8sNsLimitRangeProcessor;
    }
}
