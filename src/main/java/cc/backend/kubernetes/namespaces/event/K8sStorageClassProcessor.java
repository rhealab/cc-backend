package cc.backend.kubernetes.namespaces.event;

import cc.backend.EnnProperties;
import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceType;
import cc.backend.event.processor.AbstractProcessor;
import cc.backend.kubernetes.CustomKubernetesClient;
import cc.backend.kubernetes.namespaces.dto.NamespaceNaming;
import cc.backend.kubernetes.namespaces.messages.CephPoolCreatedMessage;
import cc.backend.kubernetes.namespaces.messages.CephPoolDeletedMessage;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.StorageClass;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

import static cc.backend.kubernetes.Constants.*;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/6/2.
 */
@Component
public class K8sStorageClassProcessor extends AbstractProcessor<CephPoolCreatedMessage, Void, CephPoolDeletedMessage> {

    @Inject
    private CustomKubernetesClient client;

    @Inject
    private EnnProperties properties;

    @Override
    protected void doCreate(EventSource<CephPoolCreatedMessage> eventSource) {
        CephPoolCreatedMessage message = eventSource.getPayload();
        String namespaceName = message.getNamespaceName();
        boolean succeed = createStorageClass(namespaceName);
        if (!succeed) {
            throw new CcException(BackendReturnCodeNameConstants.STORAGECLASS_CREATE_ERROR,
                    ImmutableMap.of("storageclass", namespaceName));
        }
    }

    public boolean createStorageClass(String namespaceName) {
        StorageClass storageClass = new StorageClass();

        ObjectMeta meta = new ObjectMeta();
        meta.setName(NamespaceNaming.storageClassName(namespaceName));
        meta.setLabels(ImmutableMap.of("namespaceName", namespaceName));
        storageClass.setMetadata(meta);

        Map<String, String> parameters = new HashMap<>(7);
        parameters.put(STORAGE_CLASS_ADMIN_ID, namespaceName);
        parameters.put(STORAGE_CLASS_ADMIN_SECRET_NAME, namespaceName);
        parameters.put(STORAGE_CLASS_ADMIN_SECRET_NAMESPACE, namespaceName);
        parameters.put(STORAGE_CLASS_MONITORS, properties.getCurrentCeph().getMonitorsString());
        parameters.put(STORAGE_CLASS_POOL, NamespaceNaming.cephPoolName(namespaceName));
        parameters.put(STORAGE_CLASS_USER_ID, namespaceName);
        parameters.put(STORAGE_CLASS_USER_SECRET_NAME, namespaceName);
        storageClass.setParameters(parameters);

        storageClass.setProvisioner("kubernetes.io/rbd");

        return client.createStorageClass(storageClass);
    }

    @Override
    protected void doDelete(EventSource<CephPoolDeletedMessage> eventSource) {
        String namespaceName = eventSource.getNamespaceName();
        boolean succeed = deleteStorageClass(namespaceName);
        if (!succeed) {
            throw new CcException(BackendReturnCodeNameConstants.STORAGECLASS_DELETE_ERROR,
                    ImmutableMap.of("storageclass", namespaceName));
        }
    }

    public boolean deleteStorageClass(String namespaceName) {
        return client.deleteStorageClass(namespaceName);
    }

    @Override
    public boolean needToDelete(EventSource<CephPoolDeletedMessage> eventSource) {
        return getEvent(eventSource.getNamespaceName(),
                EventSourceType.NS_CREATE,
                eventSource.getSourceName(),
                getCreateEventName()) != null;
    }

    @Override
    public EventName getCreateEventName() {
        return EventName.CREATE_K8S_STORAGE_CLASS;
    }

    @Override
    public EventName getDeleteEventName() {
        return EventName.DELETE_K8S_STORAGE_CLASS;
    }

}
