package cc.backend.kubernetes.namespaces.event;

import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceType;
import cc.backend.event.processor.AbstractProcessor;
import cc.backend.event.processor.ICreateProcessor;
import cc.backend.event.processor.IDeleteProcessor;
import cc.backend.kubernetes.Constants;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.limitrange.data.SysLimitRange;
import cc.backend.kubernetes.limitrange.data.SysLimitRangeRepository;
import cc.backend.kubernetes.namespaces.dto.NamespaceCreateRequest;
import cc.backend.kubernetes.namespaces.dto.NamespaceUpdateRequest;
import cc.backend.kubernetes.utils.UnitUtils;
import com.google.common.collect.ImmutableList;
import io.fabric8.kubernetes.api.model.*;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

/**
 * @author xzy on 17-11-7.
 */
@Component
public class K8sNsLimitRangeProcessor extends AbstractProcessor<NamespaceCreateRequest,
        NamespaceUpdateRequest, String> {
    private static final Logger logger = LoggerFactory.getLogger(K8sNsLimitRangeProcessor.class);

    private final KubernetesClientManager clientManager;

    private final SysLimitRangeRepository limitRangeRepository;

    private final KeystoneProjectProcessor projectProcessor;

    @Inject
    public K8sNsLimitRangeProcessor(KubernetesClientManager clientManager,
                                    SysLimitRangeRepository limitRangeRepository,
                                    KeystoneProjectProcessor projectProcessor) {
        this.clientManager = clientManager;
        this.limitRangeRepository = limitRangeRepository;
        this.projectProcessor = projectProcessor;
    }

    @PostConstruct
    private void init() {
        SysLimitRange sysLimitRange = limitRangeRepository.findOne(1L);
        if (sysLimitRange == null) {
            sysLimitRange = new SysLimitRange();

            sysLimitRange.setId(1);
            sysLimitRange.setMinCpu(0.2);
            sysLimitRange.setMinMemory(268435456L);

            sysLimitRange.setDefaultRequestCpu(0.3);
            sysLimitRange.setDefaultRequestMemory(536870912L);

            sysLimitRange.setDefaultLimitCpu(1);
            sysLimitRange.setDefaultLimitMemory(4294967296L);

            limitRangeRepository.save(sysLimitRange);
        }
    }

    @Override
    protected void doCreate(EventSource<NamespaceCreateRequest> eventSource) {
        KubernetesClient client = clientManager.getClient();

        LimitRange limitRange = composeLimitRange();
        if (limitRange != null) {
            logger.info("create ns limit range");
            limitRange = client.limitRanges()
                    .inNamespace(eventSource.getNamespaceName())
                    .create(limitRange);
            logger.info("{}", limitRange);
        }
    }

    private LimitRange composeLimitRange() {
        LimitRange limitRange = new LimitRange();
        limitRange.setApiVersion("v1");
        limitRange.setKind("LimitRange");

        ObjectMeta objectMeta = new ObjectMeta();
        objectMeta.setName("limitrange");
        limitRange.setMetadata(objectMeta);

        LimitRangeSpec limitRangeSpec = new LimitRangeSpec();
        LimitRangeItem limitRangeItem = new LimitRangeItem();

        limitRangeItem.setType(Constants.K8S_LIMITRANGE_CONTAINER_TYPE);

        SysLimitRange sysLimitRange = limitRangeRepository.findOne(1L);
        Map<String, Quantity> min = new HashMap<>(2);
        min.put(Constants.K_CPU, UnitUtils.toK8sCpuQuantity(sysLimitRange.getMinCpu()));
        min.put(Constants.K_MEMORY, UnitUtils.toK8sMemoryQuantity(sysLimitRange.getMinMemory()));
        limitRangeItem.setMin(min);

        Map<String, Quantity> defaultRequest = new HashMap<>(2);
        defaultRequest.put(Constants.K_CPU, UnitUtils.toK8sCpuQuantity(sysLimitRange.getDefaultRequestCpu()));
        defaultRequest.put(Constants.K_MEMORY, UnitUtils.toK8sMemoryQuantity(sysLimitRange.getDefaultRequestMemory()));
        limitRangeItem.setDefaultRequest(defaultRequest);

        Map<String, Quantity> defaultLimit = new HashMap<>(2);
        defaultLimit.put(Constants.K_CPU, UnitUtils.toK8sCpuQuantity(sysLimitRange.getDefaultLimitCpu()));
        defaultLimit.put(Constants.K_MEMORY, UnitUtils.toK8sMemoryQuantity(sysLimitRange.getDefaultLimitMemory()));
        limitRangeItem.setDefault(defaultLimit);

        limitRangeSpec.setLimits(ImmutableList.of(limitRangeItem));
        limitRange.setSpec(limitRangeSpec);
        return limitRange;
    }

    @Override
    public void delete(EventSource<String> eventSource) {
        if (needToDelete(eventSource)) {
            doDelete(eventSource);
            cc.backend.event.data.Event<String> event = composeDeleteEvent(eventSource);
            event.setPayloadJson("ignored because of delete namespace");
            saveEvent(event);
        }

        if (getNextDeleteProcessor() != null) {
            getNextDeleteProcessor().delete(eventSource);
        }
    }

    @Override
    public boolean needToDelete(EventSource<String> eventSource) {
        return getEvent(eventSource.getNamespaceName(),
                EventSourceType.NS_CREATE,
                eventSource.getSourceName(),
                getCreateEventName()) != null;
    }

    @Override
    public EventName getCreateEventName() {
        return EventName.CREATE_K8S_NS_LIMITRANGE;
    }

    @Override
    public EventName getDeleteEventName() {
        return EventName.DELETE_K8S_NS_LIMITRANGE;
    }

    @Override
    public ICreateProcessor<NamespaceCreateRequest> getNextCreateProcessor() {
        return projectProcessor;
    }

    @Override
    public IDeleteProcessor<String> getNextDeleteProcessor() {
        return projectProcessor;
    }
}
