package cc.backend.kubernetes.namespaces.event;

import cc.backend.event.data.EventSource;
import cc.backend.event.processor.AbstractProcessor;
import cc.backend.kubernetes.namespaces.dto.NamespaceCreateRequest;
import cc.backend.kubernetes.namespaces.dto.NamespaceUpdateRequest;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/13.
 */
@Component
public class NamespaceProcessor extends AbstractProcessor<NamespaceCreateRequest, NamespaceUpdateRequest, String> {

    private final K8sNsProcessor k8sNsProcessor;

    @Inject
    public NamespaceProcessor(K8sNsProcessor k8sNsProcessor) {
        this.k8sNsProcessor = k8sNsProcessor;
    }

    @Override
    public void create(EventSource<NamespaceCreateRequest> eventSource) {
        k8sNsProcessor.create(eventSource);
    }

    @Override
    public void update(EventSource<NamespaceUpdateRequest> eventSource) {
        k8sNsProcessor.update(eventSource);
    }

    @Override
    public void delete(EventSource<String> eventSource) {
        k8sNsProcessor.delete(eventSource);
    }
}
