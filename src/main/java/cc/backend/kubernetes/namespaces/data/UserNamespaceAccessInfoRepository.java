package cc.backend.kubernetes.namespaces.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/1/23.
 */
public interface UserNamespaceAccessInfoRepository extends JpaRepository<UserNamespaceAccessInfo, Long> {
    /**
     * find a user's all namespace access info
     *
     * @param userId the user id
     * @return the user's access info list
     */
    List<UserNamespaceAccessInfo> findByUserId(String userId);

    /**
     * find a user in a namespace's access info
     *
     * @param userId        the user id
     * @param namespaceName the namespace
     * @return the access info or null if not exists
     */
    UserNamespaceAccessInfo findByUserIdAndNamespace(String userId, String namespaceName);

    /**
     * delete a namespace's all access info
     *
     * @param namespaceName the namespace need to delete
     */
    @Transactional(rollbackFor = Exception.class)
    void deleteByNamespace(String namespaceName);
}
