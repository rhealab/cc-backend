package cc.backend.kubernetes.namespaces.data;

/**
 * Created by xzy on 2017/6/5.
 */
public enum NamespaceStatus {
    CREATE_SUCCESS,
    CREATE_PENDING,
    CREATE_FAILED,
    DELETE_PENDING,
    DELETE_SUCCESS,
    DELETE_FAILED,
    UPDATE_PENDING,
    UPDATE_FAILED,
    UPDATE_SUCCESS
}
