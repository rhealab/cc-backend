package cc.backend.kubernetes.namespaces.data;

import cc.backend.audit.request.EnnContext;

import javax.persistence.*;
import java.util.Date;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/1/23.
 */
@Entity
public class UserNamespaceAccessInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String userId;

    private String namespace;

    private Date accessTime;
    private Date lastUpdatedOn;
    private String lastUpdatedBy;
    private String createdBy;
    private Date createdOn;

    @PreUpdate
    public void onUpdate() {
        lastUpdatedBy = EnnContext.getUserId();
        lastUpdatedOn = new Date();
    }

    @PrePersist
    public void onCreate() {
        createdBy = EnnContext.getUserId();
        createdOn = new Date();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public Date getAccessTime() {
        return accessTime;
    }

    public void setAccessTime(Date accessTime) {
        this.accessTime = accessTime;
    }

    public Date getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(Date lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public String toString() {
        return "UserNamespaceAccessInfo{" +
                "id=" + id +
                ", userId='" + userId + '\'' +
                ", namespace='" + namespace + '\'' +
                ", accessTime=" + accessTime +
                ", lastUpdatedOn=" + lastUpdatedOn +
                ", lastUpdatedBy='" + lastUpdatedBy + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                '}';
    }
}
