package cc.backend.kubernetes.namespaces.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/2/14.
 */
public interface NamespaceExtrasRepository extends JpaRepository<NamespaceExtras, Long> {
    /**
     * find a cluster's namespace extras info by namespace name list
     *
     * @param nameList the namespace name list
     * @param lessThan delete time less than
     * @return the extras info list
     */
    List<NamespaceExtras> findByNameInAndDeletedIsFalseOrDeletedOnAfter(List<String> nameList, Date lessThan);

    /**
     * find a cluster's all namespaces' extras info
     *
     * @param lessThan delete time less than
     * @return the extras info list
     */
    List<NamespaceExtras> findByDeletedIsFalseOrDeletedOnAfter(Date lessThan);

    /**
     * find a namespace extras in a cluster
     *
     * @param name the namespace name
     * @return the namespace extras info
     */
    NamespaceExtras findByNameAndDeletedIsFalse(String name);

    /**
     * update the namespace's status in a cluster
     *
     * @param namespaceName the namespace name
     * @param status        the status for update
     * @param lastUpdatedBy the operator
     */
    @Modifying(clearAutomatically = true)
    @Query("UPDATE NamespaceExtras a " +
            "SET a.status=:status, a.lastUpdatedBy=:lastUpdatedBy, a.lastUpdatedOn=NOW()" +
            "WHERE a.name=:namespaceName AND a.deleted=false")
    @Transactional(rollbackFor = Exception.class)
    void updateStatusByNamespace(@Param("namespaceName") String namespaceName,
                                 @Param("status") NamespaceStatus status,
                                 @Param("lastUpdatedBy") String lastUpdatedBy);

    /**
     * delete a cluster's namespace by name
     *
     * @param name      the namespace name
     * @param deletedBy the operator id
     */
    @Modifying(clearAutomatically = true)
    @Query("UPDATE NamespaceExtras a " +
            "SET a.deleted=true,a.deletedOn=NOW(),a.status='DELETE_SUCCESS', a.deletedBy=:deletedBy " +
            "WHERE a.name=:namespaceName AND a.deleted=false ")
    @Transactional(rollbackFor = Exception.class)
    void deleteByName(@Param("namespaceName") String name,
                      @Param("deletedBy") String deletedBy);

    /**
     * update a namespace's hostpath storage bytes
     *
     * @param namespaceName        the namespace name
     * @param hostpathStorageBytes the hostpath storage bytes for update
     */
    @Modifying(clearAutomatically = true)
    @Query("UPDATE NamespaceExtras a SET a.hostpathStorageBytes=:hostpathStorageBytes " +
            "WHERE a.name=:namespaceName AND a.deleted=false ")
    @Transactional(rollbackFor = Exception.class)
    void updateHostpathStorageQuotaByNamespace(@Param("namespaceName") String namespaceName,
                                               @Param("hostpathStorageBytes") long hostpathStorageBytes);

    /**
     * update a namespace's pool bytes
     *
     * @param namespaceName the namespace name
     * @param poolBytes     the pool bytes for update
     */
    @Modifying(clearAutomatically = true)
    @Query("UPDATE NamespaceExtras a SET a.poolBytes=:poolBytes " +
            "WHERE a.name=:namespaceName AND a.deleted=false ")
    @Transactional(rollbackFor = Exception.class)
    void updateCephPoolStorageQuotaByNamespace(@Param("namespaceName") String namespaceName,
                                               @Param("poolBytes") long poolBytes);

    /**
     * update a namespace's ebs bytes
     *
     * @param namespaceName the namespace name
     * @param ebsBytes      the ebs bytes for update
     */
    @Modifying(clearAutomatically = true)
    @Query("UPDATE NamespaceExtras a SET a.ebsBytes=:ebsBytes " +
            "WHERE a.name=:namespaceName and a.deleted=false ")
    @Transactional(rollbackFor = Exception.class)
    void updateEbsStorageQuotaByNamespace(@Param("namespaceName") String namespaceName,
                                          @Param("ebsBytes") long ebsBytes);
}