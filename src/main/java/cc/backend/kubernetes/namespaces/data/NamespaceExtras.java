package cc.backend.kubernetes.namespaces.data;

import cc.backend.audit.request.EnnContext;

import javax.persistence.*;
import java.util.Date;

/**
 * @author wangjinxin on 2017/2/14.
 *
 * columnDefinition = "bit(1) default 0" in mysql worked, other db is not tested
 *
 */
@Entity
@Table(indexes = {@Index(name = "namespace_extras_i1", columnList = "name")})
public class NamespaceExtras {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;
    private long hostpathStorageBytes;
    private long poolBytes;
    private long ebsBytes;

    @Column(columnDefinition = "bit(1) default 0")
    private boolean allowPrivilege;
    @Column(columnDefinition = "bit(1) default 0")
    private boolean allowHostpath;
    @Column(columnDefinition = "bit(1) default 0")
    private boolean allowCriticalPod;

    @Column(columnDefinition = "bit(1) default 1")
    private boolean cephInitialized = true;
    @Column(columnDefinition = "bit(1) default 0")
    private boolean nfsInitialized = false;

    @Enumerated(EnumType.STRING)
    private NamespaceStatus status;

    private Date lastUpdatedOn;
    private String lastUpdatedBy;
    private Date createdOn;
    private String createdBy;
    private Date deletedOn;
    private String deletedBy;

    private boolean deleted;

    @PreUpdate
    public void onUpdate() {
        lastUpdatedBy = EnnContext.getUserId();
        lastUpdatedOn = new Date();
    }

    @PrePersist
    public void onCreate() {
        createdBy = EnnContext.getUserId();
        createdOn = new Date();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getHostpathStorageBytes() {
        return hostpathStorageBytes;
    }

    public void setHostpathStorageBytes(long hostpathStorageBytes) {
        this.hostpathStorageBytes = hostpathStorageBytes;
    }

    public long getPoolBytes() {
        return poolBytes;
    }

    public void setPoolBytes(long poolBytes) {
        this.poolBytes = poolBytes;
    }

    public long getEbsBytes() {
        return ebsBytes;
    }

    public void setEbsBytes(long ebsBytes) {
        this.ebsBytes = ebsBytes;
    }

    public boolean getAllowPrivilege() {
        return allowPrivilege;
    }

    public void setAllowPrivilege(boolean allowPrivilege) {
        this.allowPrivilege = allowPrivilege;
    }

    public boolean getAllowHostpath() {
        return allowHostpath;
    }

    public void setAllowHostpath(boolean allowHostpath) {
        this.allowHostpath = allowHostpath;
    }

    public boolean getAllowCriticalPod() {
        return allowCriticalPod;
    }

    public void setAllowCriticalPod(boolean allowCriticalPod) {
        this.allowCriticalPod = allowCriticalPod;
    }

    public boolean isCephInitialized() {
        return cephInitialized;
    }

    public void setCephInitialized(boolean cephInitialized) {
        this.cephInitialized = cephInitialized;
    }

    public boolean isNfsInitialized() {
        return nfsInitialized;
    }

    public void setNfsInitialized(boolean nfsInitialized) {
        this.nfsInitialized = nfsInitialized;
    }

    public Date getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(Date lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public NamespaceStatus getStatus() {
        return status;
    }

    public void setStatus(NamespaceStatus status) {
        this.status = status;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "NamespaceExtras{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", hostpathStorageBytes=" + hostpathStorageBytes +
                ", poolBytes=" + poolBytes +
                ", ebsBytes=" + ebsBytes +
                ", allowPrivilege=" + allowPrivilege +
                ", allowHostpath=" + allowHostpath +
                ", allowCriticalPod=" + allowCriticalPod +
                ", cephInitialized=" + cephInitialized +
                ", nfsInitialized=" + nfsInitialized +
                ", status=" + status +
                ", lastUpdatedOn=" + lastUpdatedOn +
                ", lastUpdatedBy='" + lastUpdatedBy + '\'' +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                ", deletedOn=" + deletedOn +
                ", deletedBy='" + deletedBy + '\'' +
                ", deleted=" + deleted +
                '}';
    }
}
