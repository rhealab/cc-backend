package cc.backend.kubernetes.namespaces;

import cc.backend.kubernetes.namespaces.dto.NamespaceDetailDto;
import cc.backend.kubernetes.namespaces.dto.NamespaceDto;
import cc.backend.kubernetes.namespaces.dto.ResourceCount;
import cc.backend.kubernetes.namespaces.services.MyNamespacesManagement;
import cc.keystone.client.domain.RoleType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * @author wangchunyang@gmail.com
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes/namespaces")
@Api(value = "Namespace", description = "Operation about namespace.", produces = "application/json")
public class MyNamespacesEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(MyNamespacesEndpoint.class);

    @Inject
    private MyNamespacesManagement management;

    @GET
    @ApiOperation(value = "List all namespaces info.", responseContainer = "List", response = NamespaceDto.class)
    public Response getMyNamespaces() {
        List<NamespaceDto> namespaceDtos = management.getMyNamespaceList();
        return Response.ok(namespaceDtos).build();
    }

    @GET
    @Path("{namespaceName}")
    @ApiOperation(value = "Get namespace detail.", response = NamespaceDetailDto.class)
    public Response getMyNamespaceDetail(@PathParam("namespaceName") String namespaceName) {
        NamespaceDetailDto namespaceDto = management.getNamespaceDetail(namespaceName);
        return Response.ok(namespaceDto).build();
    }

    @GET
    @Path("{namespaceName}/resource_count")
    @ApiOperation(value = "Get namespace resource count.", response = ResourceCount.class)
    public Response getNamespaceResourceCount(@PathParam("namespaceName") String namespaceName) {
        ResourceCount resourceCount =
                management.getNamespaceResourceCount(namespaceName);
        return Response.ok(resourceCount).build();
    }

    @GET
    @Path("{namespaceName}/my_roles")
    @ApiOperation(value = "Get user role in namespace.", responseContainer = "List", response = String.class)
    public Response getMyRoleInNamespace(@PathParam("namespaceName") String namespaceName) {
        List<RoleType> myRoles = management.getUserRoleInNamespace(namespaceName);
        List<String> roles = new ArrayList<>();
        for (RoleType roleType : myRoles) {
            roles.add(roleType.name().toLowerCase());
        }
        return Response.ok(roles).build();
    }

    @PUT
    @Path("{namespaceName}/access")
    @ApiOperation(value = "Update namespace access time.")
    public Response setAccessTime(@PathParam("namespaceName") String namespaceName) {
        management.setAccessInfo(namespaceName);
        return Response.ok().build();
    }
}
