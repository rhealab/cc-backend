package cc.backend.kubernetes.namespaces.utils;

import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.namespaces.dto.NamespaceNaming;
import cc.backend.kubernetes.utils.UnitUtils;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.Quantity;
import io.fabric8.kubernetes.api.model.ResourceQuota;

import java.util.Map;

import static cc.backend.kubernetes.Constants.*;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2018/1/11.
 */
public class NamespaceUtils {
    public static Map<String, Number> getNamespaceComputeAvailable(KubernetesClientManager clientManager,
                                                                   String namespaceName) {
        ResourceQuota resourceQuota = clientManager.getClient()
                .resourceQuotas()
                .inNamespace(namespaceName)
                .withName(NamespaceNaming.quotaName(namespaceName))
                .get();

        return getNamespaceComputeAvailable(resourceQuota);
    }

    public static Map<String, Number> getNamespaceComputeAvailable(ResourceQuota resourceQuota) {
        Map<String, Quantity> hard = resourceQuota.getStatus().getHard();
        Map<String, Quantity> used = resourceQuota.getStatus().getUsed();

        double cpuLimitsAvailable = UnitUtils.parseK8sCpuQuantity(hard.get(K_NAMESPACE_CPU_LIMITS)) -
                UnitUtils.parseK8sCpuQuantity(used.get(K_NAMESPACE_CPU_LIMITS));

        double cpuRequestsAvailable = UnitUtils.parseK8sCpuQuantity(hard.get(K_NAMESPACE_CPU_REQUESTS)) -
                UnitUtils.parseK8sCpuQuantity(used.get(K_NAMESPACE_CPU_REQUESTS));

        long memoryLimitsAvailable = UnitUtils.parseK8sMemoryQuantity(hard.get(K_NAMESPACE_MEMORY_LIMITS)) -
                UnitUtils.parseK8sMemoryQuantity(used.get(K_NAMESPACE_MEMORY_LIMITS));

        long memoryRequestsAvailable = UnitUtils.parseK8sMemoryQuantity(hard.get(K_NAMESPACE_MEMORY_REQUESTS))
                - UnitUtils.parseK8sMemoryQuantity(used.get(K_NAMESPACE_MEMORY_REQUESTS));

        return ImmutableMap.<String, Number>builder()
                .put(K_QUOTA_CPU_LIMITS, cpuLimitsAvailable)
                .put(K_QUOTA_CPU_REQUESTS, cpuRequestsAvailable)
                .put(K_QUOTA_MEMORY_LIMITS, memoryLimitsAvailable)
                .put(K_QUOTA_MEMORY_REQUESTS, memoryRequestsAvailable)
                .build();
    }
}
