package cc.backend.kubernetes.namespaces.dto;

import java.util.Optional;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/2/22.
 */
public class NamespaceStatsResponse {
    private String name;

    private Double cpuRequests;
    private Double cpuLimits;
    private Double cpuRequestsReserved;
    private Double cpuLimitsReserved;

    private Long memoryRequestsBytes;
    private Long memoryLimitsBytes;
    private Long memoryRequestsReservedBytes;
    private Long memoryLimitsReservedBytes;

    private Long hostPathBytes;
    private Long hostPathReservedBytes;
    private Long hostPathUsedBytes;

    private Long cephPoolBytes;

    private Long rbdBytes;
    private Long rbdReservedBytes;
    private Long rbdUsedBytes;

    private Long cephFsBytes;
    private Long cephFsReservedBytes;
    private Long cephFsUsedBytes;

    private Long nfsBytes;
    private Long nfsReservedBytes;
    private Long nfsUsedBytes;

    private Long efsBytes;
    private Long efsReservedBytes;
    private Long efsUsedBytes;

    private Long ebsBytes;
    private Long ebsReservedBytes;
    private Long ebsUsedBytes;

    @Deprecated
    private Boolean allowPrivilege;
    @Deprecated
    private Boolean allowHostpath;
    @Deprecated
    private Boolean allowCriticalPod;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getCpuRequests() {
        return cpuRequests;
    }

    public void setCpuRequests(Double cpuRequests) {
        this.cpuRequests = cpuRequests;
    }

    public Double getCpuLimits() {
        return cpuLimits;
    }

    public void setCpuLimits(Double cpuLimits) {
        this.cpuLimits = cpuLimits;
    }

    public Double getCpuRequestsReserved() {
        return cpuRequestsReserved;
    }

    public void setCpuRequestsReserved(Double cpuRequestsReserved) {
        this.cpuRequestsReserved = cpuRequestsReserved;
    }

    public Double getCpuLimitsReserved() {
        return cpuLimitsReserved;
    }

    public void setCpuLimitsReserved(Double cpuLimitsReserved) {
        this.cpuLimitsReserved = cpuLimitsReserved;
    }

    public Long getMemoryRequestsBytes() {
        return memoryRequestsBytes;
    }

    public void setMemoryRequestsBytes(Long memoryRequestsBytes) {
        this.memoryRequestsBytes = memoryRequestsBytes;
    }

    public Long getMemoryLimitsBytes() {
        return memoryLimitsBytes;
    }

    public void setMemoryLimitsBytes(Long memoryLimitsBytes) {
        this.memoryLimitsBytes = memoryLimitsBytes;
    }

    public Long getMemoryRequestsReservedBytes() {
        return memoryRequestsReservedBytes;
    }

    public void setMemoryRequestsReservedBytes(Long memoryRequestsReservedBytes) {
        this.memoryRequestsReservedBytes = memoryRequestsReservedBytes;
    }

    public Long getMemoryLimitsReservedBytes() {
        return memoryLimitsReservedBytes;
    }

    public void setMemoryLimitsReservedBytes(Long memoryLimitsReservedBytes) {
        this.memoryLimitsReservedBytes = memoryLimitsReservedBytes;
    }

    public Long getHostPathBytes() {
        return hostPathBytes;
    }

    public void setHostPathBytes(Long hostPathBytes) {
        this.hostPathBytes = hostPathBytes;
    }

    public Long getHostPathReservedBytes() {
        return hostPathReservedBytes;
    }

    public void setHostPathReservedBytes(Long hostPathReservedBytes) {
        this.hostPathReservedBytes = hostPathReservedBytes;
    }

    public Long getHostPathUsedBytes() {
        return hostPathUsedBytes;
    }

    public void setHostPathUsedBytes(Long hostPathUsedBytes) {
        this.hostPathUsedBytes = hostPathUsedBytes;
    }

    public Long getCephPoolBytes() {
        return cephPoolBytes;
    }

    public void setCephPoolBytes(Long cephPoolBytes) {
        this.cephPoolBytes = cephPoolBytes;
    }

    public Long getRbdReservedBytes() {
        return rbdReservedBytes;
    }

    public void setRbdReservedBytes(Long rbdReservedBytes) {
        this.rbdReservedBytes = rbdReservedBytes;
    }

    public Long getRbdUsedBytes() {
        return rbdUsedBytes;
    }

    public void setRbdUsedBytes(Long rbdUsedBytes) {
        this.rbdUsedBytes = rbdUsedBytes;
    }

    public Long getCephFsReservedBytes() {
        return cephFsReservedBytes;
    }

    public void setCephFsReservedBytes(Long cephFsReservedBytes) {
        this.cephFsReservedBytes = cephFsReservedBytes;
    }

    public Long getCephFsUsedBytes() {
        return cephFsUsedBytes;
    }

    public void setCephFsUsedBytes(Long cephFsUsedBytes) {
        this.cephFsUsedBytes = cephFsUsedBytes;
    }

    public Long getNfsBytes() {
        return nfsBytes;
    }

    public void setNfsBytes(Long nfsBytes) {
        this.nfsBytes = nfsBytes;
    }

    public Long getNfsUsedBytes() {
        return nfsUsedBytes;
    }

    public void setNfsUsedBytes(Long nfsUsedBytes) {
        this.nfsUsedBytes = nfsUsedBytes;
    }

    public Boolean getAllowPrivilege() {
        return allowPrivilege;
    }

    public void setAllowPrivilege(Boolean allowPrivilege) {
        this.allowPrivilege = allowPrivilege;
    }

    public Boolean getAllowHostpath() {
        return allowHostpath;
    }

    public void setAllowHostpath(Boolean allowHostpath) {
        this.allowHostpath = allowHostpath;
    }

    public Boolean getAllowCriticalPod() {
        return allowCriticalPod;
    }

    public void setAllowCriticalPod(Boolean allowCriticalPod) {
        this.allowCriticalPod = allowCriticalPod;
    }

    public Long getRbdBytes() {
        return rbdBytes;
    }

    public void setRbdBytes(Long rbdBytes) {
        this.rbdBytes = rbdBytes;
    }

    public Long getCephFsBytes() {
        return cephFsBytes;
    }

    public void setCephFsBytes(Long cephFsBytes) {
        this.cephFsBytes = cephFsBytes;
    }

    public Long getNfsReservedBytes() {
        return nfsReservedBytes;
    }

    public void setNfsReservedBytes(Long nfsReservedBytes) {
        this.nfsReservedBytes = nfsReservedBytes;
    }

    public Long getEbsBytes() {
        return ebsBytes;
    }

    public Long getEfsBytes() {
        return efsBytes;
    }

    public void setEfsBytes(Long efsBytes) {
        this.efsBytes = efsBytes;
    }

    public Long getEfsReservedBytes() {
        return efsReservedBytes;
    }

    public void setEfsReservedBytes(Long efsReservedBytes) {
        this.efsReservedBytes = efsReservedBytes;
    }

    public Long getEfsUsedBytes() {
        return efsUsedBytes;
    }

    public void setEfsUsedBytes(Long efsUsedBytes) {
        this.efsUsedBytes = efsUsedBytes;
    }

    public void setEbsBytes(Long ebsBytes) {
        this.ebsBytes = ebsBytes;
    }

    public Long getEbsReservedBytes() {
        return ebsReservedBytes;
    }

    public void setEbsReservedBytes(Long ebsReservedBytes) {
        this.ebsReservedBytes = ebsReservedBytes;
    }

    public Long getEbsUsedBytes() {
        return ebsUsedBytes;
    }

    public void setEbsUsedBytes(Long ebsUsedBytes) {
        this.ebsUsedBytes = ebsUsedBytes;
    }

    @Override
    public String toString() {
        return "NamespaceStatsResponse{" +
                "name='" + name + '\'' +
                ", cpuRequests=" + cpuRequests +
                ", cpuLimits=" + cpuLimits +
                ", cpuRequestsReserved=" + cpuRequestsReserved +
                ", cpuLimitsReserved=" + cpuLimitsReserved +
                ", memoryRequestsBytes=" + memoryRequestsBytes +
                ", memoryLimitsBytes=" + memoryLimitsBytes +
                ", memoryRequestsReservedBytes=" + memoryRequestsReservedBytes +
                ", memoryLimitsReservedBytes=" + memoryLimitsReservedBytes +
                ", hostPathBytes=" + hostPathBytes +
                ", hostPathReservedBytes=" + hostPathReservedBytes +
                ", hostPathUsedBytes=" + hostPathUsedBytes +
                ", cephPoolBytes=" + cephPoolBytes +
                ", rbdBytes=" + rbdBytes +
                ", rbdReservedBytes=" + rbdReservedBytes +
                ", rbdUsedBytes=" + rbdUsedBytes +
                ", cephFsBytes=" + cephFsBytes +
                ", cephFsReservedBytes=" + cephFsReservedBytes +
                ", cephFsUsedBytes=" + cephFsUsedBytes +
                ", nfsBytes=" + nfsBytes +
                ", nfsReservedBytes=" + nfsReservedBytes +
                ", nfsUsedBytes=" + nfsUsedBytes +
                ", ebsBytes=" + ebsBytes +
                ", ebsReservedBytes=" + ebsReservedBytes +
                ", ebsUsedBytes=" + ebsUsedBytes +
                ", allowPrivilege=" + allowPrivilege +
                ", allowHostpath=" + allowHostpath +
                ", allowCriticalPod=" + allowCriticalPod +
                '}';
    }

    public static class Builder {
        private String name;
        private Double cpuRequests;
        private Double cpuLimits;
        private Double cpuRequestsReserved;
        private Double cpuLimitsReserved;
        private Long memoryRequestsBytes;
        private Long memoryLimitsBytes;
        private Long memoryRequestsReservedBytes;
        private Long memoryLimitsReservedBytes;

        private Long hostPathBytes;
        private Long hostPathReservedBytes;
        private Long hostPathUsedBytes;

        private Long cephPoolBytes;

        private Long rbdBytes;
        private Long rbdReservedBytes;
        private Long rbdUsedBytes;

        private Long cephFsBytes;
        private Long cephFsReservedBytes;
        private Long cephFsUsedBytes;

        private Long nfsBytes;
        private Long nfsReservedBytes;
        private Long nfsUsedBytes;

        private Long ebsBytes;
        private Long ebsReservedBytes;
        private Long ebsUsedBytes;

        private Boolean allowPrivilege;
        private Boolean allowHostpath;
        private Boolean allowCriticalPod;

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder cpuRequests(Double cpuRequests) {
            this.cpuRequests = cpuRequests;
            return this;
        }

        public Builder cpuLimits(Double cpuLimits) {
            this.cpuLimits = cpuLimits;
            return this;
        }

        public Builder cpuRequestsReserved(Double cpuRequestsReserved) {
            this.cpuRequestsReserved = cpuRequestsReserved;
            return this;
        }

        public Builder cpuLimitsReserved(Double cpuLimitsReserved) {
            this.cpuLimitsReserved = cpuLimitsReserved;
            return this;
        }

        public Builder memoryRequestsBytes(Long memoryRequestsBytes) {
            this.memoryRequestsBytes = memoryRequestsBytes;
            return this;
        }

        public Builder memoryLimitsBytes(Long memoryLimitsBytes) {
            this.memoryLimitsBytes = memoryLimitsBytes;
            return this;
        }

        public Builder memoryRequestsReservedBytes(Long memoryRequestsReservedBytes) {
            this.memoryRequestsReservedBytes = memoryRequestsReservedBytes;
            return this;
        }

        public Builder memoryLimitsReservedBytes(Long memoryLimitsReservedBytes) {
            this.memoryLimitsReservedBytes = memoryLimitsReservedBytes;
            return this;
        }

        public Builder hostPathBytes(Long hostPathBytes) {
            this.hostPathBytes = hostPathBytes;
            return this;
        }

        public Builder hostPathReservedBytes(Long hostPathReservedBytes) {
            this.hostPathReservedBytes = hostPathReservedBytes;
            return this;
        }

        public Builder hostPathUsedBytes(Long hostPathUsedBytes) {
            this.hostPathUsedBytes = hostPathUsedBytes;
            return this;
        }

        public Builder cephPoolBytes(Long cephPoolBytes) {
            this.cephPoolBytes = cephPoolBytes;
            return this;
        }

        public Builder rbdBytes(Long rbdBytes) {
            this.rbdBytes = rbdBytes;
            return this;
        }

        public Builder rbdReservedBytes(Long rbdReservedBytes) {
            this.rbdReservedBytes = rbdReservedBytes;
            return this;
        }

        public Builder rbdUsedBytes(Long rbdUsedBytes) {
            this.rbdUsedBytes = rbdUsedBytes;
            return this;
        }

        public Builder cephFsBytes(Long cephFsBytes) {
            this.cephFsBytes = cephFsBytes;
            return this;
        }

        public Builder cephFsReservedBytes(Long cephFsReservedBytes) {
            this.cephFsReservedBytes = cephFsReservedBytes;
            return this;
        }

        public Builder cephFsUsedBytes(Long cephFsUsedBytes) {
            this.cephFsUsedBytes = cephFsUsedBytes;
            return this;
        }

        public Builder nfsBytes(Long nfsBytes) {
            this.nfsBytes = nfsBytes;
            return this;
        }

        public Builder nfsReservedBytes(Long nfsReservedBytes) {
            this.nfsReservedBytes = nfsReservedBytes;
            return this;
        }

        public Builder nfsUsedBytes(Long nfsUsedBytes) {
            this.nfsUsedBytes = nfsUsedBytes;
            return this;
        }

        public Builder ebsBytes(Long ebsBytes) {
            this.ebsBytes = ebsBytes;
            return this;
        }

        public Builder ebsReservedBytes(Long ebsReservedBytes) {
            this.ebsReservedBytes = ebsReservedBytes;
            return this;
        }

        public Builder ebsUsedBytes(Long ebsUsedBytes) {
            this.ebsUsedBytes = ebsUsedBytes;
            return this;
        }

        public Builder allowPrivilege(Boolean allowPrivilege) {
            this.allowPrivilege = allowPrivilege;
            return this;
        }

        public Builder allowHostpath(Boolean allowHostpath) {
            this.allowHostpath = allowHostpath;
            return this;
        }

        public Builder allowCriticalPod(Boolean allowCriticalPod) {
            this.allowCriticalPod = allowCriticalPod;
            return this;
        }

        public NamespaceStatsResponse build() {
            NamespaceStatsResponse response = new NamespaceStatsResponse();
            response.setName(name);
            response.setCpuLimits(cpuLimits);
            response.setCpuLimitsReserved(cpuLimitsReserved);
            response.setCpuRequests(cpuRequests);
            response.setCpuRequestsReserved(cpuRequestsReserved);

            response.setMemoryLimitsBytes(memoryLimitsBytes);
            response.setMemoryLimitsReservedBytes(memoryLimitsReservedBytes);
            response.setMemoryRequestsBytes(memoryRequestsBytes);
            response.setMemoryRequestsReservedBytes(memoryRequestsReservedBytes);

            response.setHostPathBytes(hostPathBytes);
            response.setHostPathReservedBytes(hostPathReservedBytes);
            response.setHostPathUsedBytes(hostPathUsedBytes);

            response.setCephPoolBytes(cephPoolBytes);

            response.setRbdBytes(Optional.ofNullable(rbdBytes).orElse(cephPoolBytes));
            response.setRbdReservedBytes(rbdReservedBytes);
            response.setRbdUsedBytes(rbdUsedBytes);

            response.setCephFsBytes(cephFsBytes);
            response.setCephFsReservedBytes(cephFsReservedBytes);
            response.setCephFsUsedBytes(cephFsUsedBytes);

            response.setNfsBytes(nfsBytes);
            response.setNfsReservedBytes(nfsReservedBytes);
            response.setNfsUsedBytes(nfsUsedBytes);

            response.setEfsBytes(nfsBytes);
            response.setEfsReservedBytes(nfsReservedBytes);
            response.setEfsUsedBytes(nfsUsedBytes);

            response.setEbsBytes(ebsBytes);
            response.setEbsReservedBytes(ebsReservedBytes);
            response.setEbsUsedBytes(ebsUsedBytes);

            response.setAllowPrivilege(allowPrivilege);
            response.setAllowHostpath(allowHostpath);
            response.setAllowCriticalPod(allowCriticalPod);

            return response;
        }
    }
}
