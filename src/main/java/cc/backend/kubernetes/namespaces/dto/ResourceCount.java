package cc.backend.kubernetes.namespaces.dto;

/**
 * Created by NormanWang06@gmail.com (jinxin) on 2017/6/15.
 */
public class ResourceCount {
    private long appNum;
    private long deploymentNum;
    private long serviceNum;
    private long podNum;
    private long userNum;

    public long getAppNum() {
        return appNum;
    }

    public void setAppNum(long appNum) {
        this.appNum = appNum;
    }

    public long getDeploymentNum() {
        return deploymentNum;
    }

    public void setDeploymentNum(long deploymentNum) {
        this.deploymentNum = deploymentNum;
    }

    public long getServiceNum() {
        return serviceNum;
    }

    public void setServiceNum(long serviceNum) {
        this.serviceNum = serviceNum;
    }

    public long getPodNum() {
        return podNum;
    }

    public void setPodNum(long podNum) {
        this.podNum = podNum;
    }

    public long getUserNum() {
        return userNum;
    }

    public void setUserNum(long userNum) {
        this.userNum = userNum;
    }

    @Override
    public String toString() {
        return "ResourceCount{" +
                "appNum=" + appNum +
                ", deploymentNum=" + deploymentNum +
                ", serviceNum=" + serviceNum +
                ", podNum=" + podNum +
                ", userNum=" + userNum +
                '}';
    }
}
