package cc.backend.kubernetes.namespaces.dto;


import cc.keystone.client.domain.AssignInfo;

import javax.validation.Valid;
import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/26.
 */
public class RoleAssignmentMaintainRequest {
    private List<AssignInfo> addList;
    private List<AssignInfo> removeList;
    @Valid
    private List<AssignInfo> updateList;

    public List<AssignInfo> getAddList() {
        return addList;
    }

    public void setAddList(List<AssignInfo> addList) {
        this.addList = addList;
    }

    public List<AssignInfo> getRemoveList() {
        return removeList;
    }

    public void setRemoveList(List<AssignInfo> removeList) {
        this.removeList = removeList;
    }

    public List<AssignInfo> getUpdateList() {
        return updateList;
    }

    public void setUpdateList(List<AssignInfo> updateList) {
        this.updateList = updateList;
    }

    public enum Operation {
        Add, Remove, Update
    }
}
