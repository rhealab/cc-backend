package cc.backend.kubernetes.namespaces.dto;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/5/16.
 */
public enum NamespaceType {
    Legacy, Normal
}
