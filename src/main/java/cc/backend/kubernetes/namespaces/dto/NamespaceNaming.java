package cc.backend.kubernetes.namespaces.dto;

/**
 * @author wangchunyang@gmail.com
 */
public interface NamespaceNaming {

    String getNamespaceName();

    static String quotaName(String namespaceName) {
        return String.format("%s-quota", namespaceName);
    }

    // currently, we made a agreement that pool name is k8s.+namespace name eg: k8s.default
    static String cephPoolName(String namespaceName) {
        return String.format("k8s.%s", namespaceName);
    }

    // currently, we made a agreement that ceph user name namespace name
    static String cephUserName(String namespaceName) {
        return namespaceName;
    }

    // currently, we made a agreement that ceph user secret name is namespace name
    static String cephSecretName(String namespaceName) {
        return namespaceName;
    }

    default String quotaName() {
        return quotaName(getNamespaceName());
    }

    default String cephPoolName() {
        return cephPoolName(getNamespaceName());
    }

    default String cephUserName() {
        return cephUserName(getNamespaceName());
    }

    default String cephSecretName() {
        return cephUserName(getNamespaceName());
    }

    static String storageClassName(String namespaceName){
        return namespaceName;
    }
}
