package cc.backend.kubernetes.namespaces.dto;

import java.util.Optional;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/2/22.
 */
public class NamespaceStatsResponseDeprecated {
    private String name;

    private double cpuRequest;
    private double cpuLimit;
    private double cpuRequestReserved;
    private double cpuLimitReserved;

    private long memoryRequestBytes;
    private long memoryLimitBytes;
    private long memoryRequestReservedBytes;
    private long memoryLimitReservedBytes;

    private long hostPathBytes;
    private long hostPathReservedBytes;
    private long hostPathUsedBytes;

    private long cephPoolBytes;

    private long rbdBytes;
    private long rbdReservedBytes;
    private long rbdUsedBytes;

    private long cephFsBytes;
    private long cephFsReservedBytes;
    private long cephFsUsedBytes;

    private long nfsBytes;
    private long nfsUsedBytes;

    @Deprecated
    private long localStorageBytes;
    @Deprecated
    private long localStorageReservedBytes;
    @Deprecated
    private long localStorageUsedBytes;
    @Deprecated
    private long remoteStorageBytes;
    @Deprecated
    private long remoteStorageReservedBytes;
    @Deprecated
    private long remoteStorageUsedBytes;
    @Deprecated
    private boolean allowPrivilege;
    @Deprecated
    private boolean allowHostpath;
    @Deprecated
    private boolean allowCriticalPod;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCpuRequest() {
        return cpuRequest;
    }

    public void setCpuRequest(double cpuRequest) {
        this.cpuRequest = cpuRequest;
    }

    public double getCpuLimit() {
        return cpuLimit;
    }

    public void setCpuLimit(double cpuLimit) {
        this.cpuLimit = cpuLimit;
    }

    public double getCpuRequestReserved() {
        return cpuRequestReserved;
    }

    public void setCpuRequestReserved(double cpuRequestReserved) {
        this.cpuRequestReserved = cpuRequestReserved;
    }

    public double getCpuLimitReserved() {
        return cpuLimitReserved;
    }

    public void setCpuLimitReserved(double cpuLimitReserved) {
        this.cpuLimitReserved = cpuLimitReserved;
    }

    public long getMemoryRequestBytes() {
        return memoryRequestBytes;
    }

    public void setMemoryRequestBytes(long memoryRequestBytes) {
        this.memoryRequestBytes = memoryRequestBytes;
    }

    public long getMemoryLimitBytes() {
        return memoryLimitBytes;
    }

    public void setMemoryLimitBytes(long memoryLimitBytes) {
        this.memoryLimitBytes = memoryLimitBytes;
    }

    public long getMemoryRequestReservedBytes() {
        return memoryRequestReservedBytes;
    }

    public void setMemoryRequestReservedBytes(long memoryRequestReservedBytes) {
        this.memoryRequestReservedBytes = memoryRequestReservedBytes;
    }

    public long getMemoryLimitReservedBytes() {
        return memoryLimitReservedBytes;
    }

    public void setMemoryLimitReservedBytes(long memoryLimitReservedBytes) {
        this.memoryLimitReservedBytes = memoryLimitReservedBytes;
    }

    public long getHostPathBytes() {
        return hostPathBytes;
    }

    public void setHostPathBytes(long hostPathBytes) {
        this.hostPathBytes = hostPathBytes;
    }

    public long getHostPathReservedBytes() {
        return hostPathReservedBytes;
    }

    public void setHostPathReservedBytes(long hostPathReservedBytes) {
        this.hostPathReservedBytes = hostPathReservedBytes;
    }

    public long getHostPathUsedBytes() {
        return hostPathUsedBytes;
    }

    public void setHostPathUsedBytes(long hostPathUsedBytes) {
        this.hostPathUsedBytes = hostPathUsedBytes;
    }

    public long getCephPoolBytes() {
        return cephPoolBytes;
    }

    public void setCephPoolBytes(long cephPoolBytes) {
        this.cephPoolBytes = cephPoolBytes;
    }

    public long getRbdBytes() {
        return rbdBytes;
    }

    public void setRbdBytes(long rbdBytes) {
        this.rbdBytes = rbdBytes;
    }

    public long getRbdReservedBytes() {
        return rbdReservedBytes;
    }

    public void setRbdReservedBytes(long rbdReservedBytes) {
        this.rbdReservedBytes = rbdReservedBytes;
    }

    public long getRbdUsedBytes() {
        return rbdUsedBytes;
    }

    public void setRbdUsedBytes(long rbdUsedBytes) {
        this.rbdUsedBytes = rbdUsedBytes;
    }

    public long getCephFsBytes() {
        return cephFsBytes;
    }

    public void setCephFsBytes(long cephFsBytes) {
        this.cephFsBytes = cephFsBytes;
    }

    public long getCephFsReservedBytes() {
        return cephFsReservedBytes;
    }

    public void setCephFsReservedBytes(long cephFsReservedBytes) {
        this.cephFsReservedBytes = cephFsReservedBytes;
    }

    public long getCephFsUsedBytes() {
        return cephFsUsedBytes;
    }

    public void setCephFsUsedBytes(long cephFsUsedBytes) {
        this.cephFsUsedBytes = cephFsUsedBytes;
    }

    public long getNfsBytes() {
        return nfsBytes;
    }

    public void setNfsBytes(long nfsBytes) {
        this.nfsBytes = nfsBytes;
    }

    public long getNfsUsedBytes() {
        return nfsUsedBytes;
    }

    public void setNfsUsedBytes(long nfsUsedBytes) {
        this.nfsUsedBytes = nfsUsedBytes;
    }

    public long getLocalStorageBytes() {
        return localStorageBytes;
    }

    public void setLocalStorageBytes(long localStorageBytes) {
        this.localStorageBytes = localStorageBytes;
    }

    public long getLocalStorageReservedBytes() {
        return localStorageReservedBytes;
    }

    public void setLocalStorageReservedBytes(long localStorageReservedBytes) {
        this.localStorageReservedBytes = localStorageReservedBytes;
    }

    public long getLocalStorageUsedBytes() {
        return localStorageUsedBytes;
    }

    public void setLocalStorageUsedBytes(long localStorageUsedBytes) {
        this.localStorageUsedBytes = localStorageUsedBytes;
    }

    public long getRemoteStorageBytes() {
        return remoteStorageBytes;
    }

    public void setRemoteStorageBytes(long remoteStorageBytes) {
        this.remoteStorageBytes = remoteStorageBytes;
    }

    public long getRemoteStorageReservedBytes() {
        return remoteStorageReservedBytes;
    }

    public void setRemoteStorageReservedBytes(long remoteStorageReservedBytes) {
        this.remoteStorageReservedBytes = remoteStorageReservedBytes;
    }

    public long getRemoteStorageUsedBytes() {
        return remoteStorageUsedBytes;
    }

    public void setRemoteStorageUsedBytes(long remoteStorageUsedBytes) {
        this.remoteStorageUsedBytes = remoteStorageUsedBytes;
    }

    public boolean isAllowPrivilege() {
        return allowPrivilege;
    }

    public void setAllowPrivilege(boolean allowPrivilege) {
        this.allowPrivilege = allowPrivilege;
    }

    public boolean isAllowHostpath() {
        return allowHostpath;
    }

    public void setAllowHostpath(boolean allowHostpath) {
        this.allowHostpath = allowHostpath;
    }

    public boolean isAllowCriticalPod() {
        return allowCriticalPod;
    }

    public void setAllowCriticalPod(boolean allowCriticalPod) {
        this.allowCriticalPod = allowCriticalPod;
    }

    @Override
    public String toString() {
        return "NamespaceStatsResponseDeprecated{" +
                "name='" + name + '\'' +
                ", cpuRequest=" + cpuRequest +
                ", cpuLimit=" + cpuLimit +
                ", cpuRequestReserved=" + cpuRequestReserved +
                ", cpuLimitReserved=" + cpuLimitReserved +
                ", memoryRequestBytes=" + memoryRequestBytes +
                ", memoryLimitBytes=" + memoryLimitBytes +
                ", memoryRequestReservedBytes=" + memoryRequestReservedBytes +
                ", memoryLimitReservedBytes=" + memoryLimitReservedBytes +
                ", hostPathBytes=" + hostPathBytes +
                ", hostPathReservedBytes=" + hostPathReservedBytes +
                ", hostPathUsedBytes=" + hostPathUsedBytes +
                ", cephPoolBytes=" + cephPoolBytes +
                ", rbdBytes=" + rbdBytes +
                ", rbdReservedBytes=" + rbdReservedBytes +
                ", rbdUsedBytes=" + rbdUsedBytes +
                ", cephFsBytes=" + cephFsBytes +
                ", cephFsReservedBytes=" + cephFsReservedBytes +
                ", cephFsUsedBytes=" + cephFsUsedBytes +
                ", nfsBytes=" + nfsBytes +
                ", nfsUsedBytes=" + nfsUsedBytes +
                ", localStorageBytes=" + localStorageBytes +
                ", localStorageReservedBytes=" + localStorageReservedBytes +
                ", localStorageUsedBytes=" + localStorageUsedBytes +
                ", remoteStorageBytes=" + remoteStorageBytes +
                ", remoteStorageReservedBytes=" + remoteStorageReservedBytes +
                ", remoteStorageUsedBytes=" + remoteStorageUsedBytes +
                ", allowPrivilege=" + allowPrivilege +
                ", allowHostpath=" + allowHostpath +
                ", allowCriticalPod=" + allowCriticalPod +
                '}';
    }

    public static NamespaceStatsResponseDeprecated from(NamespaceStatsResponse response) {
        NamespaceStatsResponseDeprecated deprecated = new NamespaceStatsResponseDeprecated();
        deprecated.setName(response.getName());

        deprecated.setCpuRequest(Optional.ofNullable(response.getCpuRequests()).orElse(0.0));
        deprecated.setCpuLimit(Optional.ofNullable(response.getCpuLimits()).orElse(0.0));
        deprecated.setCpuRequestReserved(Optional.ofNullable(response.getCpuRequestsReserved()).orElse(0.0));
        deprecated.setCpuLimitReserved(Optional.ofNullable(response.getCpuLimitsReserved()).orElse(0.0));

        deprecated.setMemoryRequestBytes(Optional.ofNullable(response.getMemoryRequestsBytes()).orElse(0L));
        deprecated.setMemoryLimitBytes(Optional.ofNullable(response.getMemoryLimitsBytes()).orElse(0L));
        deprecated.setMemoryRequestReservedBytes(Optional.ofNullable(response.getMemoryRequestsReservedBytes()).orElse(0L));
        deprecated.setMemoryLimitReservedBytes(Optional.ofNullable(response.getMemoryLimitsReservedBytes()).orElse(0L));

        deprecated.setHostPathBytes(Optional.ofNullable(response.getHostPathBytes()).orElse(0L));
        deprecated.setHostPathReservedBytes(Optional.ofNullable(response.getHostPathReservedBytes()).orElse(0L));
        deprecated.setHostPathUsedBytes(Optional.ofNullable(response.getHostPathUsedBytes()).orElse(0L));

        deprecated.setCephPoolBytes(Optional.ofNullable(response.getCephPoolBytes()).orElse(0L));

        deprecated.setRbdBytes(Optional.ofNullable(response.getRbdBytes()).orElse(0L));
        deprecated.setRbdReservedBytes(Optional.ofNullable(response.getRbdReservedBytes()).orElse(0L));
        deprecated.setRbdUsedBytes(Optional.ofNullable(response.getRbdUsedBytes()).orElse(0L));

        deprecated.setCephFsBytes(Optional.ofNullable(response.getCephFsBytes()).orElse(0L));
        deprecated.setCephFsReservedBytes(Optional.ofNullable(response.getCephFsReservedBytes()).orElse(0L));
        deprecated.setCephFsUsedBytes(Optional.ofNullable(response.getCephFsUsedBytes()).orElse(0L));

        deprecated.setNfsBytes(Optional.ofNullable(response.getNfsBytes()).orElse(0L));
        deprecated.setNfsUsedBytes(Optional.ofNullable(response.getNfsUsedBytes()).orElse(0L));

        deprecated.setLocalStorageBytes(deprecated.getHostPathBytes());
        deprecated.setLocalStorageReservedBytes(deprecated.getHostPathReservedBytes());
        deprecated.setLocalStorageUsedBytes(deprecated.getHostPathUsedBytes());
        deprecated.setRemoteStorageBytes(deprecated.getRbdBytes());
        deprecated.setRemoteStorageReservedBytes(deprecated.getRbdReservedBytes());
        deprecated.setRemoteStorageUsedBytes(deprecated.getRbdUsedBytes());

        deprecated.setAllowCriticalPod(response.getAllowCriticalPod());
        deprecated.setAllowHostpath(response.getAllowHostpath());
        deprecated.setAllowPrivilege(response.getAllowPrivilege());

        return deprecated;
    }
}
