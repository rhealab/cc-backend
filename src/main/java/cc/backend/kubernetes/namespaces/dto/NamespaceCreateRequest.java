package cc.backend.kubernetes.namespaces.dto;

import cc.backend.kubernetes.Constants;
import cc.backend.kubernetes.utils.UnitUtils;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.Quantity;
import io.fabric8.kubernetes.api.model.ResourceQuota;
import io.fabric8.kubernetes.api.model.ResourceQuotaSpec;

import javax.validation.constraints.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cc.backend.kubernetes.Constants.*;

/**
 * Created by xzy on 2017/2/22.
 */
public class NamespaceCreateRequest implements NamespaceNaming {
    @NotNull(message = "namespace name can not be null")
    @Pattern(regexp = Constants.RESOURCE_NAME_PATTERN)
    @Size(max = 100, message = "namespace length can not over 100 characters")
    private String name;

    private List<String> adminList;

    @DecimalMin(value = "0.1", message = "namespace cpu requests must larger than 0.1")
    @NotNull
    private double cpuRequest;
    @DecimalMin(value = "0.1", message = "namespace cpu limits must larger than 0.1")
    @NotNull
    private double cpuLimit;

    @Min(value = 4 * 1024 * 1024, message = "memory at least 4M.")
    @NotNull
    private long memoryRequestBytes;

    @Min(value = 4 * 1024 * 1024, message = "memory at least 4M.")
    @NotNull
    private long memoryLimitBytes;

    @Min(1024 * 1024)
    @JsonProperty("localStorageBytes")
    private long hostPathStorageBytes;

    @JsonProperty("remoteStorageBytes")
    private long poolBytes;

    private boolean allowPrivilege;

    private boolean allowHostpath;

    private boolean allowCriticalPod;

    @JsonProperty("ebsStorageBytes")
    private Long ebsBytes;

    public String getName() {
        return name;
    }

    @Override
    public String getNamespaceName() {
        return getName();
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getAdminList() {
        return adminList;
    }

    public void setAdminList(List<String> adminList) {
        this.adminList = adminList;
    }

    public double getCpuRequest() {
        return cpuRequest;
    }

    public void setCpuRequest(double cpuRequest) {
        this.cpuRequest = cpuRequest;
    }

    public double getCpuLimit() {
        return cpuLimit;
    }

    public void setCpuLimit(double cpuLimit) {
        this.cpuLimit = cpuLimit;
    }

    public long getMemoryRequestBytes() {
        return memoryRequestBytes;
    }

    public void setMemoryRequestBytes(long memoryRequestBytes) {
        this.memoryRequestBytes = memoryRequestBytes;
    }

    public long getMemoryLimitBytes() {
        return memoryLimitBytes;
    }

    public void setMemoryLimitBytes(long memoryLimitBytes) {
        this.memoryLimitBytes = memoryLimitBytes;
    }

    public long getHostPathStorageBytes() {
        return hostPathStorageBytes;
    }

    public void setHostPathStorageBytes(long hostPathStorageBytes) {
        this.hostPathStorageBytes = hostPathStorageBytes;
    }

    public long getPoolBytes() {
        return poolBytes;
    }

    public void setPoolBytes(long poolBytes) {
        this.poolBytes = poolBytes;
    }

    public boolean isAllowPrivilege() {
        return allowPrivilege;
    }

    public void setAllowPrivilege(boolean allowPrivilege) {
        this.allowPrivilege = allowPrivilege;
    }

    public boolean isAllowHostpath() {
        return allowHostpath;
    }

    public void setAllowHostpath(boolean allowHostpath) {
        this.allowHostpath = allowHostpath;
    }

    public boolean isAllowCriticalPod() {
        return allowCriticalPod;
    }

    public void setAllowCriticalPod(boolean allowCriticalPod) {
        this.allowCriticalPod = allowCriticalPod;
    }

    public Long getEbsBytes() {
        return ebsBytes;
    }

    public void setEbsBytes(Long ebsBytes) {
        this.ebsBytes = ebsBytes;
    }

    public ResourceQuota composeResourceQuota() {
        ResourceQuota quota = new ResourceQuota();

        quota.setApiVersion("v1");
        quota.setKind("ResourceQuota");

        ObjectMeta metadata = new ObjectMeta();
        metadata.setName(quotaName());
        metadata.setNamespace(name);
        quota.setMetadata(metadata);

        ResourceQuotaSpec spec = new ResourceQuotaSpec();
        Map<String, Quantity> hard = new HashMap<>();

        hard.put(K_NAMESPACE_CPU_REQUESTS, UnitUtils.toK8sCpuQuantity(cpuRequest));
        hard.put(K_NAMESPACE_CPU_LIMITS, UnitUtils.toK8sCpuQuantity(cpuLimit));
        hard.put(K_NAMESPACE_MEMORY_REQUESTS, UnitUtils.toK8sMemoryQuantity(memoryRequestBytes));
        hard.put(K_NAMESPACE_MEMORY_LIMITS, UnitUtils.toK8sMemoryQuantity(memoryLimitBytes));

        spec.setHard(hard);
        quota.setSpec(spec);

        return quota;
    }

    @Override
    public String toString() {
        return "NamespaceCreateRequest{" +
                "name='" + name + '\'' +
                ", adminList=" + adminList +
                ", cpuRequest=" + cpuRequest +
                ", cpuLimit=" + cpuLimit +
                ", memoryRequestBytes=" + memoryRequestBytes +
                ", memoryLimitBytes=" + memoryLimitBytes +
                ", hostPathStorageBytes=" + hostPathStorageBytes +
                ", poolBytes=" + poolBytes +
                ", allowPrivilege=" + allowPrivilege +
                ", allowHostpath=" + allowHostpath +
                ", allowCriticalPod=" + allowCriticalPod +
                ", ebsBytes=" + ebsBytes +
                '}';
    }
}
