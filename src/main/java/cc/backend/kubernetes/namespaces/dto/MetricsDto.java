package cc.backend.kubernetes.namespaces.dto;

import cc.backend.common.Metrics;
import cc.backend.common.MetricsType;

import java.util.List;
import java.util.Map;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/7.
 */
public class MetricsDto {
    private MetricsType type;
    private double percent;
    private double used;
    private double total;

    public MetricsType getType() {
        return type;
    }

    public void setType(MetricsType type) {
        this.type = type;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    public double getUsed() {
        return used;
    }

    public void setUsed(double used) {
        this.used = used;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "MetricsDto{" +
                "type=" + type +
                ", percent=" + percent +
                ", used=" + used +
                ", total=" + total +
                '}';
    }

    /**
     * negative percent means error
     * get a error metrics
     *
     * @return the dto
     */
    public static MetricsDto newErrorInstance(MetricsType type) {
        MetricsDto dto = new MetricsDto();
        dto.setPercent(-1);
        dto.setType(type);
        return dto;
    }

    public static MetricsDto from(List<Metrics> metricsList, double total, MetricsType type) {
        MetricsDto dto = new MetricsDto();
        if (metricsList != null) {
            double used = metricsList.stream()
                    .map(metrics -> metrics.getDps().entrySet())
                    .mapToDouble(entries -> entries.stream().mapToDouble(Map.Entry::getValue).average().orElse(0))
                    .sum();
            dto.setTotal(total);
            dto.setUsed(used);
            if (total != 0) {
                dto.setPercent(used / total);
            } else {
                dto.setPercent(-1);
            }
        } else {
            dto.setPercent(-1);
            dto.setTotal(total);
            dto.setUsed(-1);
        }

        dto.setType(type);

        return dto;
    }

    public static MetricsDto from(List<Metrics> metricsList, double total, String nodeName, MetricsType type) {
        MetricsDto dto = new MetricsDto();
        if (metricsList != null) {
            double used = metricsList.stream()
                    .filter(metrics -> nodeName.equals(metrics.getTags().get("host")))
                    .map(metrics -> metrics.getDps().entrySet())
                    .mapToDouble(entries -> entries.stream().mapToDouble(Map.Entry::getValue).average().orElse(0))
                    .sum();
            dto.setTotal(total);
            dto.setUsed(used);
            if (total != 0) {
                dto.setPercent(used / total);
            } else {
                dto.setPercent(-1);
            }
        } else {
            dto.setPercent(-1);
            dto.setTotal(total);
            dto.setUsed(-1);
        }

        dto.setType(type);

        return dto;
    }

    public static MetricsDto from(double used, double total, MetricsType type) {
        MetricsDto dto = new MetricsDto();

        dto.setTotal(total);
        dto.setUsed(used);
        if (total != 0) {
            dto.setPercent(used / total);
        } else {
            dto.setPercent(-1);
        }

        dto.setType(type);
        return dto;
    }
}
