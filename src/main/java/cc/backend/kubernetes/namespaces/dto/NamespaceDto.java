package cc.backend.kubernetes.namespaces.dto;

import cc.backend.kubernetes.namespaces.data.NamespaceStatus;

import java.util.Date;

/**
 * Created by xzy on 2017/2/14.
 */
public class NamespaceDto {
    private String name;
    @Deprecated
    private String uid = "";
    private Date createdOn;
    private String createdBy;
    private NamespaceStatus status;

    private Date lastAccessOn;
    private String myRoleName;
    private NamespaceType type = NamespaceType.Normal;

    private Boolean allowPrivilege;
    private Boolean allowHostpath;
    private Boolean allowCriticalPod;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public NamespaceStatus getStatus() {
        return status;
    }

    public void setStatus(NamespaceStatus status) {
        this.status = status;
    }

    public Date getLastAccessOn() {
        return lastAccessOn;
    }

    public void setLastAccessOn(Date lastAccessOn) {
        this.lastAccessOn = lastAccessOn;
    }

    public String getMyRoleName() {
        return myRoleName;
    }

    public void setMyRoleName(String myRoleName) {
        this.myRoleName = myRoleName;
    }

    public NamespaceType getType() {
        return type;
    }

    public void setType(NamespaceType type) {
        this.type = type;
    }

    public Boolean getAllowPrivilege() {
        return allowPrivilege;
    }

    public void setAllowPrivilege(Boolean allowPrivilege) {
        this.allowPrivilege = allowPrivilege;
    }

    public Boolean getAllowHostpath() {
        return allowHostpath;
    }

    public void setAllowHostpath(Boolean allowHostpath) {
        this.allowHostpath = allowHostpath;
    }

    public Boolean getAllowCriticalPod() {
        return allowCriticalPod;
    }

    public void setAllowCriticalPod(Boolean allowCriticalPod) {
        this.allowCriticalPod = allowCriticalPod;
    }

    @Override
    public String toString() {
        return "NamespaceDto{" +
                "name='" + name + '\'' +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                ", status=" + status +
                ", lastAccessOn=" + lastAccessOn +
                ", myRoleName='" + myRoleName + '\'' +
                ", type=" + type +
                ", allowPrivilege=" + allowPrivilege +
                ", allowHostpath=" + allowHostpath +
                ", allowCriticalPod=" + allowCriticalPod +
                '}';
    }
}
