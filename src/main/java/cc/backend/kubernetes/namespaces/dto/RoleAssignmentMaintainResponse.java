package cc.backend.kubernetes.namespaces.dto;

import cc.keystone.client.domain.AssignResult;

import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/26.
 */
public class RoleAssignmentMaintainResponse {
    private List<AssignResult> addList;
    private List<AssignResult> removeList;
    private List<AssignResult> updateList;

    public List<AssignResult> getAddList() {
        return addList;
    }

    public void setAddList(List<AssignResult> addList) {
        this.addList = addList;
    }

    public List<AssignResult> getRemoveList() {
        return removeList;
    }

    public void setRemoveList(List<AssignResult> removeList) {
        this.removeList = removeList;
    }

    public List<AssignResult> getUpdateList() {
        return updateList;
    }

    public void setUpdateList(List<AssignResult> updateList) {
        this.updateList = updateList;
    }

    @Override
    public String toString() {
        return "RoleAssignmentMaintainResponse{" +
                "addList=" + addList +
                ", removeList=" + removeList +
                ", updateList=" + updateList +
                '}';
    }
}
