package cc.backend.kubernetes.namespaces.dto;

import cc.backend.kubernetes.namespaces.data.NamespaceStatus;

import java.util.Date;
import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/2/14.
 */
public class NamespaceDetailDto {
    private String name;
    @Deprecated
    private String uid = "";
    private Date createdOn;
    private String createdBy;
    private NamespaceStatus status;

    private List<String> nsAdminList;

    private Date lastAccessOn;
    private String myRoleName;

    private NamespaceType type;

    @Deprecated
    private boolean createdByConsole = true;

    private Boolean allowPrivilege;
    private Boolean allowHostpath;
    private Boolean allowCriticalPod;

    private long hostPathBytes;
    private long cephPoolBytes;
    private long ebsBytes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public NamespaceStatus getStatus() {
        return status;
    }

    public void setStatus(NamespaceStatus status) {
        this.status = status;
    }

    public List<String> getNsAdminList() {
        return nsAdminList;
    }

    public void setNsAdminList(List<String> nsAdminList) {
        this.nsAdminList = nsAdminList;
    }

    public Date getLastAccessOn() {
        return lastAccessOn;
    }

    public void setLastAccessOn(Date lastAccessOn) {
        this.lastAccessOn = lastAccessOn;
    }

    public String getMyRoleName() {
        return myRoleName;
    }

    public void setMyRoleName(String myRoleName) {
        this.myRoleName = myRoleName;
    }

    public NamespaceType getType() {
        return type;
    }

    public void setType(NamespaceType type) {
        this.type = type;
    }

    public Boolean getAllowPrivilege() {
        return allowPrivilege;
    }

    public void setAllowPrivilege(Boolean allowPrivilege) {
        this.allowPrivilege = allowPrivilege;
    }

    public Boolean getAllowHostpath() {
        return allowHostpath;
    }

    public void setAllowHostpath(Boolean allowHostpath) {
        this.allowHostpath = allowHostpath;
    }

    public Boolean getAllowCriticalPod() {
        return allowCriticalPod;
    }

    public void setAllowCriticalPod(Boolean allowCriticalPod) {
        this.allowCriticalPod = allowCriticalPod;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public boolean isCreatedByConsole() {
        return createdByConsole;
    }

    public void setCreatedByConsole(boolean createdByConsole) {
        this.createdByConsole = createdByConsole;
    }

    public long getHostPathBytes() {
        return hostPathBytes;
    }

    public void setHostPathBytes(long hostPathBytes) {
        this.hostPathBytes = hostPathBytes;
    }

    public long getCephPoolBytes() {
        return cephPoolBytes;
    }

    public void setCephPoolBytes(long cephPoolBytes) {
        this.cephPoolBytes = cephPoolBytes;
    }

    public long getEbsBytes() {
        return ebsBytes;
    }

    public void setEbsBytes(long ebsBytes) {
        this.ebsBytes = ebsBytes;
    }

    @Override
    public String toString() {
        return "NamespaceDetailDto{" +
                "name='" + name + '\'' +
                ", uid='" + uid + '\'' +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                ", status=" + status +
                ", nsAdminList=" + nsAdminList +
                ", lastAccessOn=" + lastAccessOn +
                ", myRoleName='" + myRoleName + '\'' +
                ", type=" + type +
                ", createdByConsole=" + createdByConsole +
                ", allowPrivilege=" + allowPrivilege +
                ", allowHostpath=" + allowHostpath +
                ", allowCriticalPod=" + allowCriticalPod +
                ", hostPathBytes=" + hostPathBytes +
                ", cephPoolBytes=" + cephPoolBytes +
                ", ebsBytes=" + ebsBytes +
                '}';
    }
}
