package cc.backend.kubernetes.namespaces.dto;

/**
 * Created by pumpkin on 2017/9/22.
 */
public class MetricsNodeDto {
    private String name;
    private MetricsDto cpu;
    private MetricsDto memory;
    private MetricsDto disk;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MetricsDto getCpu() {
        return cpu;
    }

    public void setCpu(MetricsDto cpu) {
        this.cpu = cpu;
    }

    public MetricsDto getMemory() {
        return memory;
    }

    public void setMemory(MetricsDto memory) {
        this.memory = memory;
    }

    public MetricsDto getDisk() {
        return disk;
    }

    public void setDisk(MetricsDto disk) {
        this.disk = disk;
    }

    @Override
    public String toString() {
        return "MetricsNodeDto{" +
                "name='" + name + '\'' +
                ", cpu=" + cpu +
                ", memory=" + memory +
                ", disk=" + disk +
                '}';
    }
}
