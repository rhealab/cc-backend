package cc.backend.kubernetes.namespaces.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/2/22.
 */
public class NamespaceUpdateRequest implements NamespaceNaming {
    private String name;

    @DecimalMin(value = "0.1", message = "cpu must larger than 0.1")
    private Double cpuRequest;
    @DecimalMin(value = "0.1", message = "cpu must larger than 0.1")
    private Double cpuLimit;

    @Min(value = 4 * 1024 * 1024, message = "memory at least 4M.")
    private Long memoryRequestBytes;
    @Min(value = 4 * 1024 * 1024, message = "memory at least 4M.")
    private Long memoryLimitBytes;

    @Min(value = 1024 * 1024, message = "storage at least 1M.")
    @JsonProperty("localStorageBytes")
    private Long hostPathStorageBytes;


    @Min(value = 1024 * 1024, message = "storage at least 1M.")
    @JsonProperty("remoteStorageBytes")
    @Deprecated
    private Long poolBytes;

    @Min(value = 1024 * 1024, message = "storage at least 1M.")
    private Long cephPoolBytes;

    @Min(value = 1024 * 1024, message = "storage at least 1M.")
    private Long ebsBytes;

    @JsonProperty("allowPrivilege")
    private Boolean allowPrivilege;

    @JsonProperty("allowHostpath")
    private Boolean allowHostpath;

    @JsonProperty("allowCriticalPod")
    private Boolean allowCriticalPod;

    public String getName() {
        return name;
    }

    @Override
    public String getNamespaceName() {
        return getName();
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getCpuRequest() {
        return cpuRequest;
    }

    public void setCpuRequest(Double cpuRequest) {
        this.cpuRequest = cpuRequest;
    }

    public Double getCpuLimit() {
        return cpuLimit;
    }

    public void setCpuLimit(Double cpuLimit) {
        this.cpuLimit = cpuLimit;
    }

    public Long getMemoryRequestBytes() {
        return memoryRequestBytes;
    }

    public void setMemoryRequestBytes(Long memoryRequestBytes) {
        this.memoryRequestBytes = memoryRequestBytes;
    }

    public Long getMemoryLimitBytes() {
        return memoryLimitBytes;
    }

    public void setMemoryLimitBytes(Long memoryLimitBytes) {
        this.memoryLimitBytes = memoryLimitBytes;
    }

    public Long getHostPathStorageBytes() {
        return hostPathStorageBytes;
    }

    public void setHostPathStorageBytes(Long hostPathStorageBytes) {
        this.hostPathStorageBytes = hostPathStorageBytes;
    }

    @Deprecated
    public Long getPoolBytes() {
        return poolBytes;
    }

    @Deprecated
    public void setPoolBytes(Long poolBytes) {
        this.poolBytes = poolBytes;
    }

    public Long getCephPoolBytes() {
        return cephPoolBytes;
    }

    public void setCephPoolBytes(Long cephPoolBytes) {
        this.cephPoolBytes = cephPoolBytes;
    }

    public Long getEbsBytes() {
        return ebsBytes;
    }

    public void setEbsBytes(Long ebsBytes) {
        this.ebsBytes = ebsBytes;
    }

    public Boolean getAllowPrivilege() {
        return allowPrivilege;
    }

    public void setAllowPrivilege(Boolean allowPrivilege) {
        this.allowPrivilege = allowPrivilege;
    }

    public Boolean getAllowHostpath() {
        return allowHostpath;
    }

    public void setAllowHostpath(Boolean allowHostpath) {
        this.allowHostpath = allowHostpath;
    }

    @JsonProperty("allowCriticalPod")
    public Boolean getAllowCriticalPod() {
        return allowCriticalPod;
    }

    @JsonProperty("allowCriticalPod")
    public void setAllowCriticalPod(Boolean allowCriticalPod) {
        this.allowCriticalPod = allowCriticalPod;
    }

    @Override
    public String toString() {
        return "NamespaceUpdateRequest{" +
                "name='" + name + '\'' +
                ", cpuRequest=" + cpuRequest +
                ", cpuLimit=" + cpuLimit +
                ", memoryRequestBytes=" + memoryRequestBytes +
                ", memoryLimitBytes=" + memoryLimitBytes +
                ", hostPathStorageBytes=" + hostPathStorageBytes +
                ", poolBytes=" + poolBytes +
                ", cephPoolBytes=" + cephPoolBytes +
                ", allowPrivilege=" + allowPrivilege +
                ", allowHostpath=" + allowHostpath +
                ", allowCriticalPod=" + allowCriticalPod +
                '}';
    }
}
