package cc.backend.kubernetes.namespaces;

import cc.keystone.client.domain.AssignInfo;
import cc.keystone.client.domain.RoleType;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/4.
 */
public class RoleAssignmentHelper {
    /**
     * clean assign info
     * remove user id is null info and set role type
     *
     * @param assignInfoList the assign info list
     * @param roleType       the role type for set
     * @return the cleaned list
     */
    public static List<AssignInfo> cleanAssignInfo(List<AssignInfo> assignInfoList, RoleType roleType) {
        return Optional.ofNullable(assignInfoList)
                .orElseGet(ArrayList::new)
                .stream()
                .filter(assignInfo -> assignInfo.getUserId() != null)
                .peek(assignInfo -> {
                    if (roleType != null) {
                        assignInfo.setRole(roleType);
                    }
                })
                .collect(Collectors.toList());
    }

    /**
     * clean assign info
     * remove user id is null info and set role type
     *
     * @param assignInfoList the assign info list
     * @return the cleaned list
     */
    public static List<AssignInfo> cleanAssignInfo(List<AssignInfo> assignInfoList) {
        return Optional.ofNullable(assignInfoList)
                .orElseGet(ArrayList::new)
                .stream()
                .filter(assignInfo -> assignInfo.getUserId() != null)
                .collect(Collectors.toList());
    }
}
