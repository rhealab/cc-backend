package cc.backend.kubernetes.namespaces.status;

import cc.backend.common.GsonFactory;
import cc.backend.event.AbstractRedisStatusChecker;
import cc.backend.event.EventErrorMessage;
import cc.backend.event.data.Event;
import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceType;
import cc.backend.kubernetes.namespaces.data.NamespaceExtras;
import cc.backend.kubernetes.namespaces.data.NamespaceExtrasRepository;
import cc.backend.kubernetes.namespaces.data.NamespaceStatus;
import cc.backend.kubernetes.namespaces.dto.NamespaceUpdateRequest;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

import static cc.backend.kubernetes.Constants.REDIS_NAMESPACE_UPDATE_STATUS_KEY_PREFIX;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/2.
 */
@Component
public class NamespaceUpdateStatusChecker extends AbstractRedisStatusChecker<String, NamespaceStatus> {

    @Inject
    private NamespaceExtrasRepository namespaceExtrasRepository;

    @Override
    protected void doFinalCheck(String clusterName, String namespaceName, String resource) {
        NamespaceStatus status = check(clusterName, namespaceName, resource);
        if (status == null || status == NamespaceStatus.UPDATE_PENDING) {
            // TODO support updated by
            namespaceExtrasRepository
                    .updateStatusByNamespace(namespaceName, NamespaceStatus.UPDATE_FAILED, "");
        }
    }

    /**
     * @param namespaceName namespace to be check
     * @return namespace status or null if namespace is not exists
     */
    @Override
    public NamespaceStatus check(String clusterName, String namespaceName, String resource) {
        NamespaceExtras namespaceExtras =
                namespaceExtrasRepository.findByNameAndDeletedIsFalse(namespaceName);
        if (namespaceExtras == null) {
            return null;
        }

        NamespaceStatus status = namespaceExtras.getStatus();
        if (status == NamespaceStatus.UPDATE_PENDING) {
            EventSource eventSource = findEventSource(namespaceName, getEventSourceType(), namespaceName);

            if (isUpdateFinished(eventSource)) {
                String namespaceUpdateRequestJson = eventSource.getPayloadJson();
                NamespaceUpdateRequest request = GsonFactory.getGson().
                        fromJson(namespaceUpdateRequestJson, NamespaceUpdateRequest.class);

                if (request.getAllowPrivilege() != null) {
                    namespaceExtras.setAllowPrivilege(request.getAllowPrivilege());
                }

                if (request.getAllowHostpath() != null) {
                    namespaceExtras.setAllowHostpath(request.getAllowHostpath());
                }

                if (request.getAllowCriticalPod() != null) {
                    namespaceExtras.setAllowCriticalPod(request.getAllowCriticalPod());
                }

                namespaceExtras.setStatus(NamespaceStatus.UPDATE_SUCCESS);
                namespaceExtrasRepository.save(namespaceExtras);
                cancel(clusterName, namespaceName, resource);
                return NamespaceStatus.UPDATE_SUCCESS;
            } else {
                return status;
            }
        } else {
            return status;
        }
    }

    @Override
    public String composeRedisKey(String clusterName, String namespaceName, String resource) {
        return REDIS_NAMESPACE_UPDATE_STATUS_KEY_PREFIX + clusterName + "." + namespaceName;
    }

    @Override
    public long getStatusCheckPeriodMillis() {
        return properties.getStatusCheck().getPeriodMillis().getNamespace();
    }

    @Override
    public void handleError(EventErrorMessage message) {
        namespaceExtrasRepository.updateStatusByNamespace(message.getNamespaceName(),
                NamespaceStatus.UPDATE_FAILED, message.getUserId());
        cancel(message.getClusterName(), message.getNamespaceName(), message.getNamespaceName());
    }

    @Override
    public boolean shouldHandleError(EventErrorMessage message) {
        return isRunning(message.getClusterName(), message.getNamespaceName(), message.getNamespaceName());
    }

    @Override
    public EventSourceType getEventSourceType() {
        return EventSourceType.NS_UPDATE;
    }

    private boolean isUpdateFinished(EventSource eventSource) {
        List<Event> events = findEvents(eventSource);
        String namespaceUpdateRequestJson = eventSource.getPayloadJson();
        NamespaceUpdateRequest request = GsonFactory.getGson().
                fromJson(namespaceUpdateRequestJson, NamespaceUpdateRequest.class);

        /*
        check if need change ns and if changed
         */
        boolean shouldChangeK8s = request.getAllowHostpath() != null
                || request.getAllowPrivilege() != null
                || request.getAllowCriticalPod() != null;

        if (shouldChangeK8s && !isK8sNsChanged(events)) {
            return false;
        }

        /*
        check if need change hostpath and if changed
         */
        if (request.getHostPathStorageBytes() != null
                && !isHostpathStorageChanged(events)) {
            return false;
        }

        //check if need change ceph pool and if changed
        if (request.getCephPoolBytes() != null
                && !isCephPoolChanged(events)) {
            return false;
        }

        //check if k8s quota(compute quota) changed and if changed
        return isK8sNsQuotaChanged(events);
    }

    private boolean isK8sNsChanged(List<Event> events) {
        return events.stream().anyMatch(event -> event.getEventName() == EventName.UPDATE_K8S_NS);
    }

    private boolean isK8sNsQuotaChanged(List<Event> events) {
        return events.stream().anyMatch(event -> event.getEventName() == EventName.UPDATE_K8S_NS_QUOTA);
    }

    private boolean isCephPoolChanged(List<Event> events) {
        return events.stream().anyMatch(event -> event.getEventName() == EventName.UPDATE_CEPH_POOL);
    }

    private boolean isHostpathStorageChanged(List<Event> events) {
        return events.stream().anyMatch(event -> event.getEventName() == EventName.UPDATE_NS_HOSTPATH_QUOTA);
    }
}
