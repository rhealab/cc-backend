package cc.backend.kubernetes.namespaces.status;

import cc.backend.event.AbstractRedisStatusChecker;
import cc.backend.event.EventErrorMessage;
import cc.backend.event.data.Event;
import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceType;
import cc.backend.kubernetes.namespaces.data.NamespaceExtras;
import cc.backend.kubernetes.namespaces.data.NamespaceExtrasRepository;
import cc.backend.kubernetes.namespaces.data.NamespaceStatus;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

import static cc.backend.kubernetes.Constants.REDIS_NAMESPACE_CREATE_STATUS_KEY_PREFIX;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/6/2.
 */
@Component
public class NamespaceCreationStatusChecker extends AbstractRedisStatusChecker<String, NamespaceStatus> {

    @Inject
    private NamespaceExtrasRepository namespaceExtrasRepository;

    @Override
    public NamespaceStatus check(String clusterName, String namespaceName, String resource) {
        NamespaceExtras namespaceExtras =
                namespaceExtrasRepository.findByNameAndDeletedIsFalse(namespaceName);

        if (namespaceExtras == null) {
            return null;
        }

        NamespaceStatus status = namespaceExtras.getStatus();
        if (status != NamespaceStatus.CREATE_PENDING ||
                !isCreateFinished(findEventSource(namespaceName, getEventSourceType(), namespaceName))) {
            return status;
        }

        namespaceExtras.setStatus(NamespaceStatus.CREATE_SUCCESS);
        namespaceExtrasRepository.save(namespaceExtras);
        cancel(clusterName, namespaceName, namespaceName);
        return NamespaceStatus.CREATE_SUCCESS;
    }

    @Override
    public String composeRedisKey(String clusterName, String namespaceName, String resource) {
        return REDIS_NAMESPACE_CREATE_STATUS_KEY_PREFIX + clusterName + "." + namespaceName;
    }

    @Override
    public long getStatusCheckPeriodMillis() {
        return properties.getStatusCheck().getPeriodMillis().getNamespace();
    }

    @Override
    protected void doFinalCheck(String clusterName, String namespaceName, String resource) {
        NamespaceStatus status = check(clusterName, namespaceName, resource);
        if (status == NamespaceStatus.CREATE_PENDING) {
            namespaceExtrasRepository.updateStatusByNamespace(namespaceName,
                    NamespaceStatus.CREATE_FAILED, "");
        }
    }

    @Override
    public void handleError(EventErrorMessage message) {
        namespaceExtrasRepository.updateStatusByNamespace(message.getNamespaceName(),
                NamespaceStatus.CREATE_FAILED, message.getUserId());
        cancel(message.getClusterName(), message.getNamespaceName(), message.getNamespaceName());
    }

    @Override
    public boolean shouldHandleError(EventErrorMessage message) {
        return isRunning(message.getClusterName(), message.getNamespaceName(), message.getNamespaceName());
    }

    @Override
    public EventSourceType getEventSourceType() {
        return EventSourceType.NS_CREATE;
    }

    private boolean isCreateFinished(EventSource eventSource) {
        List<Event> events = findEvents(eventSource);

        if (!isK8sNsCreated(events) ||
                !isK8sNsQuotaCreated(events) ||
                !isKeystoneProjectCreated(events)) {
            return false;
        }

        if (properties.getCurrentCeph().isEnabled()) {
            if (!isCephDirCreated(events) ||
                    !isCephPoolCreated(events) ||
                    !isCephUserCreated(events) ||
                    !isK8sSecretForCephCreated(events) ||
                    !isK8sStorageClassCreated(events)) {
                return false;
            }
        }

        if (properties.getCurrentNfs().isEnabled()) {
            return isNfsDirCreated(events);
        }

        return true;
    }

    private boolean isK8sNsCreated(List<Event> events) {
        return events.stream().anyMatch(event -> event.getEventName() == EventName.CREATE_K8S_NS);
    }

    private boolean isK8sNsQuotaCreated(List<Event> events) {
        return events.stream().anyMatch(event -> event.getEventName() == EventName.CREATE_K8S_NS_QUOTA);
    }

    private boolean isKeystoneProjectCreated(List<Event> events) {
        return events.stream().anyMatch(event -> event.getEventName() == EventName.CREATE_KEYSTONE_PROJECT);
    }

    private boolean isCephPoolCreated(List<Event> events) {
        return events.stream().anyMatch(event -> event.getEventName() == EventName.CREATE_CEPH_POOL);
    }

    private boolean isCephDirCreated(List<Event> events) {
        return events.stream().anyMatch(event -> event.getEventName() == EventName.CREATE_CEPH_FS_DIR);
    }

    private boolean isCephUserCreated(List<Event> events) {
        return events.stream().anyMatch(event -> event.getEventName() == EventName.CREATE_CEPH_USER);
    }

    private boolean isK8sSecretForCephCreated(List<Event> events) {
        return events.stream().anyMatch(event -> event.getEventName() == EventName.CREATE_K8S_SECRET);
    }

    private boolean isK8sStorageClassCreated(List<Event> events) {
        return events.stream().anyMatch(event -> event.getEventName() == EventName.CREATE_K8S_STORAGE_CLASS);
    }

    private boolean isNfsDirCreated(List<Event> events) {
        return events.stream().anyMatch(event -> event.getEventName() == EventName.CREATE_NFS_DIR);
    }
}
