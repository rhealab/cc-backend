package cc.backend.kubernetes.namespaces.status;

import cc.backend.audit.request.EnnContext;
import cc.backend.event.AbstractRedisStatusChecker;
import cc.backend.event.EventErrorMessage;
import cc.backend.event.data.Event;
import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceType;
import cc.backend.kubernetes.namespaces.data.NamespaceExtras;
import cc.backend.kubernetes.namespaces.data.NamespaceExtrasRepository;
import cc.backend.kubernetes.namespaces.data.NamespaceStatus;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

import static cc.backend.kubernetes.Constants.REDIS_NAMESPACE_DELETE_STATUS_KEY_PREFIX;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/6/2.
 */
@Component
public class NamespaceDeletionStatusChecker extends AbstractRedisStatusChecker<String, NamespaceStatus> {

    @Inject
    private NamespaceExtrasRepository namespaceExtrasRepository;

    /**
     * @param namespaceName namespace to be check
     * @return namespace status, when namespace is not exists will return null
     */
    @Override
    public NamespaceStatus check(String clusterName, String namespaceName, String resource) {
        NamespaceExtras namespaceExtras =
                namespaceExtrasRepository.findByNameAndDeletedIsFalse(namespaceName);

        if (namespaceExtras == null) {
            return null;
        }

        NamespaceStatus status = namespaceExtras.getStatus();

        if (status != NamespaceStatus.DELETE_PENDING) {
            return status;
        }

        EventSource eventSource = findEventSource(namespaceName, getEventSourceType(), namespaceName);
        EventSource createEventSource = findEventSource(namespaceName, EventSourceType.NS_CREATE, namespaceName);

        // when all namespace info was deleted should have the same number of event with create namespace
        if (isDeleteFinished(clusterName, createEventSource, eventSource)) {
            namespaceExtrasRepository.deleteByName(namespaceName, EnnContext.getUserId());
            cancel(clusterName, namespaceName, resource);
            return null;
        }

        return status;
    }

    @Override
    public String composeRedisKey(String clusterName, String namespaceName, String resource) {
        return REDIS_NAMESPACE_DELETE_STATUS_KEY_PREFIX + clusterName + "." + namespaceName;
    }

    @Override
    public long getStatusCheckPeriodMillis() {
        return properties.getStatusCheck().getPeriodMillis().getNamespace();
    }

    @Override
    protected void doFinalCheck(String clusterName, String namespaceName, String resource) {
        NamespaceStatus status = check(clusterName, namespaceName, resource);
        if (status == NamespaceStatus.DELETE_PENDING) {
            namespaceExtrasRepository.updateStatusByNamespace(namespaceName,
                    NamespaceStatus.DELETE_FAILED, "");
        }
    }

    @Override
    public void handleError(EventErrorMessage message) {
        namespaceExtrasRepository.updateStatusByNamespace(message.getNamespaceName(),
                NamespaceStatus.DELETE_FAILED, message.getUserId());
        cancel(message.getClusterName(), message.getNamespaceName(), message.getNamespaceName());
    }

    @Override
    public boolean shouldHandleError(EventErrorMessage message) {
        return isRunning(message.getClusterName(), message.getNamespaceName(), message.getNamespaceName());
    }

    @Override
    public EventSourceType getEventSourceType() {
        return EventSourceType.NS_DELETE;
    }

    private boolean isDeleteFinished(String clusterName, EventSource createEventSource, EventSource deleteEventSource) {
        List<Event> createEvents = findEvents(createEventSource);
        List<Event> deleteEvents = findEvents(deleteEventSource);

        if (!isK8sNsDeleted(createEvents, deleteEvents) ||
                !isK8sNsQuotaDeleted(createEvents, deleteEvents) ||
                !isKeystoneProjectDeleted(createEvents, deleteEvents)) {
            return false;
        }

        if (properties.getCluster(clusterName).getCeph().isEnabled()) {
            if (!isCephDirDeleted(createEvents, deleteEvents) ||
                    !isCephPoolDeleted(createEvents, deleteEvents) ||
                    !isCephUserDeleted(createEvents, deleteEvents) ||
                    !isK8sSecretForCephDeleted(createEvents, deleteEvents) ||
                    !isK8sStorageClassDeleted(createEvents, deleteEvents)) {
                return false;
            }
        }

        if (properties.getCluster(clusterName).getNfs().isEnabled()) {
            if (!isNfsDirDeleted(createEvents, deleteEvents)) {
                return false;
            }
        }

        return true;
    }

    private boolean isK8sNsDeleted(List<Event> createEvents, List<Event> deleteEvents) {
        boolean nsCreated = createEvents.stream().anyMatch(event -> event.getEventName() == EventName.CREATE_K8S_NS);
        return !nsCreated || deleteEvents.stream().anyMatch(event -> event.getEventName() == EventName.DELETE_K8S_NS);
    }

    private boolean isK8sNsQuotaDeleted(List<Event> createEvents, List<Event> deleteEvents) {
        boolean quotaCreated = createEvents.stream().anyMatch(event -> event.getEventName() == EventName.CREATE_K8S_NS_QUOTA);
        return !quotaCreated || deleteEvents.stream().anyMatch(event -> event.getEventName() == EventName.DELETE_K8S_NS_QUOTA);
    }

    private boolean isKeystoneProjectDeleted(List<Event> createEvents, List<Event> deleteEvents) {
        boolean keystoneProjectCreated = createEvents.stream().anyMatch(event -> event.getEventName() == EventName.CREATE_KEYSTONE_PROJECT);
        return !keystoneProjectCreated || deleteEvents.stream().anyMatch(event -> event.getEventName() == EventName.DELETE_KEYSTONE_PROJECT);
    }

    private boolean isCephPoolDeleted(List<Event> createEvents, List<Event> deleteEvents) {
        boolean cephPoolCreated = createEvents.stream().anyMatch(event -> event.getEventName() == EventName.CREATE_CEPH_POOL);
        return !cephPoolCreated || deleteEvents.stream().anyMatch(event -> event.getEventName() == EventName.DELETE_CEPH_POOL);
    }

    private boolean isCephDirDeleted(List<Event> createEvents, List<Event> deleteEvents) {
        boolean cephFsDirCreated = createEvents.stream().anyMatch(event -> event.getEventName() == EventName.CREATE_CEPH_FS_DIR);
        return !cephFsDirCreated || deleteEvents.stream().anyMatch(event -> event.getEventName() == EventName.DELETE_CEPH_FS_DIR);
    }

    private boolean isCephUserDeleted(List<Event> createEvents, List<Event> deleteEvents) {
        boolean cephUserCreated = createEvents.stream().anyMatch(event -> event.getEventName() == EventName.CREATE_CEPH_USER);
        return !cephUserCreated || deleteEvents.stream().anyMatch(event -> event.getEventName() == EventName.DELETE_CEPH_USER);
    }

    private boolean isK8sSecretForCephDeleted(List<Event> createEvents, List<Event> deleteEvents) {
        boolean secretCreated = createEvents.stream().anyMatch(event -> event.getEventName() == EventName.CREATE_K8S_SECRET);
        return !secretCreated || deleteEvents.stream().anyMatch(event -> event.getEventName() == EventName.DELETE_K8S_SECRET);
    }

    private boolean isK8sStorageClassDeleted(List<Event> createEvents, List<Event> deleteEvents) {
        boolean storageClassCreated = createEvents.stream().anyMatch(event -> event.getEventName() == EventName.CREATE_K8S_STORAGE_CLASS);
        return !storageClassCreated || deleteEvents.stream().anyMatch(event -> event.getEventName() == EventName.DELETE_K8S_STORAGE_CLASS);
    }

    private boolean isNfsDirDeleted(List<Event> createEvents, List<Event> deleteEvents) {
        boolean nfsDirCreated = createEvents.stream().anyMatch(event -> event.getEventName() == EventName.CREATE_NFS_DIR);
        return !nfsDirCreated || deleteEvents.stream().anyMatch(event -> event.getEventName() == EventName.DELETE_NFS_DIR);
    }
}