package cc.backend.kubernetes;

/**
 * @author wangchunyang@gmail.com, wangjinxin
 */
public interface Constants {
    String POD_PHASE_RUNNING = "Running";

    String STORAGE_CLASS_TYPE = "type";
    String STORAGE_CLASS_ZONE = "zone";

    String STORAGE_CLASS_ADMIN_ID = "adminId";
    String STORAGE_CLASS_ADMIN_SECRET_NAME = "adminSecretName";
    String STORAGE_CLASS_ADMIN_SECRET_NAMESPACE = "adminSecretNamespace";
    String STORAGE_CLASS_MONITORS = "monitors";
    String STORAGE_CLASS_POOL = "pool";
    String STORAGE_CLASS_USER_ID = "userId";
    String STORAGE_CLASS_USER_SECRET_NAME = "userSecretName";

    String EBS_TYPE_SC1 = "sc1";

    String K_QUOTA_CPU_LIMITS = "cpuLimits";
    String K_QUOTA_CPU_REQUESTS = "cpuRequests";
    String K_QUOTA_MEMORY_LIMITS = "memoryLimits";
    String K_QUOTA_MEMORY_REQUESTS = "memoryRequests";

    String FORMAT_JSON = "json";

    long DELAY_MILLIS = 5 * 60 * 1000;

    double NAMESPACE_MIN_CPU = 0.1;
    long NAMESPACE_MIN_MEMORY_BYTES = 4 * 1024 * 1024;

    double CONTAINER_MIN_CPU = 0.1;
    long CONTAINER_MIN_MEMORY_BYTES = 4 * 1024 * 1024;

    /**
     * Deprecated! the label's key of all resource for app.
     */
    String APP_LABEL = "X_APP";

    /**
     * the annotation's key of all resource for app.
     */
    String APP_LABEL_KEY = "io.enndata.console/app";
    String CREATED_BY_ANNOTATION_KEY = "io.enndata.console/created-by";
    String CREATED_ON_MILLIS_ANNOTATION_KEY = "io.enndata.console/created-on-millis";
    String LAST_UPDATED_BY_ANNOTATION_KEY = "io.enndata.console/last-updated-by";
    String LAST_UPDATED_ON_MILLIS_ANNOTATION_KEY = "io.enndata.console/last-updated-on-millis";
    long EBS_HDD_MIN_BYTES = 1024L * 1024 * 1024 * 500;

    String RESOURCE_NAME_PATTERN = "^[a-z0-9]([-a-z0-9]*[a-z0-9]){0,99}$";
    String PORT_NAME_PATTERN = "^[a-z0-9]([-a-z0-9]*[a-z0-9]){0,14}$";
    String DEPLOYMENT_NAME_PATTERN = "^([A-Za-z0-9][-A-Za-z0-9_.]*)?[A-Za-z0-9]$";
    String LABEL_NAME_PATTERN = "^([A-Za-z0-9][-A-Za-z0-9_.]*)?[A-Za-z0-9]$";
    String ANNOTATION_NAME_PATTERN = "^([A-Za-z0-9][-A-Za-z0-9_.]*)?[A-Za-z0-9]$";
    int RESOURCE_NAME_MAX = 100;
    int STATEFULSET_NAME_MAX = 63;
    int CONTAINER_PORT_NAME_MAX = 15;
    int SERVICE_NAME_MAX = 63;
    int CONTAINER_NAME_MAX = 63;

    String DEPLOYMENT_DESCRIPTION = "description";

    String CONSOLE_NAME = "cc";
    String NAMESPACE_CREATED_BY_LABEL = "X_NAMESPACE_CREATED_BY";

    String NAMESPACE_QUOTA_RBD_STORAGE_LABEL = "X_RBD_STORAGE";
    String NAMESPACE_QUOTA_CEPHFS_STORAGE_LABEL = "X_CEPHFS_STORAGE";
    String NAMESPACE_QUOTA_HOSTPATH_STORAGE_LABEL = "X_HOSTPATH_STORAGE";

    String K_NAMESPACE_CPU_REQUESTS = "requests.cpu";
    String K_NAMESPACE_CPU_LIMITS = "limits.cpu";
    String K_NAMESPACE_MEMORY_REQUESTS = "requests.memory";
    String K_NAMESPACE_MEMORY_LIMITS = "limits.memory";
    String K_NAMESPACE_STORAGE_REQUESTS = "requests.storage";


    String K_CPU = "cpu";
    String K_MEMORY = "memory";
    String K_STORAGE = "storage";

    String NODE_DISK_INFO_ANNOTATION = "io.enndata.kubelet/alpha-nodediskquotainfo";

    String NODE_DISK_STATE_ANNOTATION = "io.enndata.kubelet/alpha-nodediskquotastate";

    /**
     * 该值由用户定义定义PV所要限定的容量，如果不设则用PV Capacity所定义的大小
     */
    String HOSTPATH_PV_CAPACITY_ANNOTATION = "io.enndata.user/alpha-pvhostpathcapcity";
    String HOSTPATH_PV_STATE_ANNOTATION = "io.enndata.kubelet/alpha-pvhostpathcapcitystate";

    /**
     * (keep/none）被scheduler模块使用，
     * 当设为keep的时候,当该PV被某个Node所挂载之后,下次再有Pod要使用该PV时,只能被调度到这个Node。
     */
    String HOSTPATH_PV_PERSIST_ANNOTATION = "io.enndata.user/alpha-pvhostpathmountpolicy";
    /**
     * 为true表示:该PV为每个绑定的Pod在Node上单独创建一个私有目录
     */
    String HOSTPATH_PV_ONE_POD_ONE_STORAGE_ANNOTATION = "io.enndata.user/alpha-pvhostpathquotaforonepod";
    String HOSTPATH_PV_USAGE_ANNOTATION = "io.enndata.kubelet/alpha-pvchostpathnode";

    String PV_NAMESPACE_LABEL = "namespaceName";

    String STORAGE_CLASS_ANNOTATION = "volume.beta.kubernetes.io/storage-class";

    /**
     * namespace annotation for allow pod can run with privilege
     */
    String NAMESPACE_ALLOW_PRIVILEGE_ANNOTATION = "io.enndata.namespace/alpha-allowprivilege";

    /**
     * namespace annotation for allow pod can use host path directly instead of by pvc
     */
    String NAMESPACE_ALLOW_HOSTPATH_ANNOTATION = "io.enndata.namespace/alpha-allowhostpath";

    /**
     * namespace annotation for allow critical pod
     */
    String NAMESPACE_ALLOW_CRITICAL_POD_ANNOTATION = "scheduler.alpha.kubernetes.io/critical-pod";

    /**
     * pod annotation identify is a critical pod
     */
    String CRITICAL_POD_ANNOTATION = "scheduler.alpha.kubernetes.io/critical-pod";

    /**
     * ingress annotations
     */
    String ENN_INGRESS_ENABLE = "io.enndata.dns/export.enable";
    String ENN_INGRESS_PORT_NAME = "io.enndata.dns/rootpath";
    String ENN_INGRESS_SUB_DOMAIN = "io.enndata.dns/customurl.subdomain";
    String ENN_INGRESS_FULL_DOMAIN = "io.enndata.dns/customurl.full";
    String ENN_INGRESS_TLS_ENABLE = "io.enndata.dns/tls.enable";
    String ENN_INGRESS_BACKEND_TLS_ENABLE = "nginx.ingress.kubernetes.io/secure-backends";
    String ENN_INGRESS_TYPE = "io.enndata.dns/dns.type";

    /**
     * redis keys
     */
    String REDIS_ADMIN_TOKEN_KEY = "cc.keystone.admin.token";
    String REDIS_PROJECTS_LIST_KEY = "cc.keystone.projects.list";
    String REDIS_PROJECTS_MAP_KEY = "cc.keystone.projects.map";
    String REDIS_SYS_ADMIN_LIST_KEY = "cc.keystone.sysadmin.list";
    String REDIS_USER_ROLE_ASSIGNMENT_MAP_KEY = "cc.keystone.user.roleassignment.map";
    String REDIS_PROJECT_ROLE_ASSIGNMENT_MAP_KEY = "cc.keystone.project.roleassignment.map";
    String REDIS_ROLE_MAP_KEY = "cc.keystone.role.map";

    String REDIS_NAMESPACE_CREATE_STATUS_KEY_PREFIX = "cc.backend.namespace.creation.status.";
    String REDIS_NAMESPACE_UPDATE_STATUS_KEY_PREFIX = "cc.backend.namespace.update.status.";
    String REDIS_NAMESPACE_DELETE_STATUS_KEY_PREFIX = "cc.backend.namespace.deletion.status.";

    String REDIS_STORAGE_CREATE_STATUS_KEY_PREFIX = "cc.backend.storage.creation.status.";
    String REDIS_STORAGE_UPDATE_STATUS_KEY_PREFIX = "cc.backend.storage.update.status.";
    String REDIS_STORAGE_DELETE_STATUS_KEY_PREFIX = "cc.backend.storage.deletion.status.";

    String K8S_LIMITRANGE_CONTAINER_TYPE = "Container";
}
