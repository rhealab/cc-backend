package cc.backend.kubernetes.admin.services;

import cc.backend.audit.operation.OperationAuditMessage;
import cc.backend.audit.operation.OperationAuditMessageProducer;
import cc.backend.audit.operation.OperationType;
import cc.backend.audit.operation.ResourceType;
import cc.backend.audit.request.EnnContext;
import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.role.assignment.RoleAssignmentDto;
import cc.backend.role.assignment.data.AssignmentExtrasRepository;
import cc.backend.role.assignment.data.RoleAssignmentExtras;
import cc.keystone.client.KeystoneRoleAssignmentManagement;
import cc.keystone.client.domain.AssignResult;
import cc.keystone.client.domain.RoleAssignment;
import cc.keystone.client.utils.HttpStatus;
import cc.lib.retcode.ReturnCode;
import cc.lib.retcode.ReturnCodeManager;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static cc.backend.role.assignment.data.RoleAssignmentExtras.ActionType.ASSIGN;
import static cc.backend.role.assignment.data.RoleAssignmentExtras.ActionType.UNASSIGN;
import static cc.keystone.client.domain.AssignInfo.Operation.Add;
import static cc.keystone.client.domain.AssignInfo.Operation.Remove;
import static cc.keystone.client.domain.RoleType.SYS_ADMIN;


/**
 * Created by xzy on 2017/2/14.
 *
 * @author yanzhixiang modified on 2017-6-7
 */

@Component
public class SysAdminManagement {

    private final KeystoneRoleAssignmentManagement keystoneRoleAssignmentManagement;

    private final AssignmentExtrasRepository assignmentExtrasRepository;

    private final OperationAuditMessageProducer operationAuditMessageProducer;

    @Inject
    public SysAdminManagement(KeystoneRoleAssignmentManagement keystoneRoleAssignmentManagement,
                              AssignmentExtrasRepository assignmentExtrasRepository,
                              OperationAuditMessageProducer operationAuditMessageProducer) {
        this.keystoneRoleAssignmentManagement = keystoneRoleAssignmentManagement;
        this.assignmentExtrasRepository = assignmentExtrasRepository;
        this.operationAuditMessageProducer = operationAuditMessageProducer;
    }

    public List<RoleAssignmentDto> getSysAdmins() {
        List<RoleAssignment> sysAdminList = keystoneRoleAssignmentManagement
                .getSysAdminList(EnnContext.getClusterName());
        List<RoleAssignmentExtras> extrasList = assignmentExtrasRepository.findByRole(SYS_ADMIN);

        return composeRoleAssignmentsDto(sysAdminList, extrasList);
    }

    private List<RoleAssignmentDto> composeRoleAssignmentsDto(List<RoleAssignment> assignments,
                                                              List<RoleAssignmentExtras> extrasList) {
        Map<String, RoleAssignmentExtras> extrasMap = extrasList.stream()
                .collect(Collectors.toMap(RoleAssignmentExtras::getUserId, extras -> extras));


        return assignments.stream()
                .map(RoleAssignmentDto::from)
                .peek(dto -> {
                    RoleAssignmentExtras extras = extrasMap.remove(dto.getUserId());
                    if (extras == null) {
                        return;
                    }
                    dto.setAssignedBy(extras.getAssignedBy());
                    dto.setAssignedDate(extras.getAssignedOn());
                })
                .collect(Collectors.toList());
    }

    public List<AssignResult> batchGrantSysAdmin(List<String> userIdList) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        String clusterName = EnnContext.getClusterName();
        /*
        when a people already a sys admin and grant again keystone will not throw exception
        but we want to tell user the error. So we just do it self by check a user is a sys admin
         */
        List<AssignResult> assignResults = new ArrayList<>();
        List<String> grantList = new ArrayList<>();
        Map<String, String> sysAdminMap = keystoneRoleAssignmentManagement.getSysAdminList(clusterName)
                .stream()
                .collect(Collectors.toMap(RoleAssignment::getUserId, RoleAssignment::getUserId));
        userIdList.forEach(userId -> {
            if (sysAdminMap.containsKey(userId)) {
                String code = BackendReturnCodeNameConstants.ROLE_ASSIGNMENT_ALREADY_EXISTS;
                AssignResult result = new AssignResult();
                result.setUserId(userId);
                result.setRole(SYS_ADMIN);

                ReturnCode returnCode = ReturnCodeManager.getReturnCodeMap().get(code);
                result.setCode(returnCode.getCode());
                result.setMessage(returnCode.getMessage());
                result.setHttpStatus(returnCode.getStatusCode());
                assignResults.add(result);
            } else {
                grantList.add(userId);
            }
        });

        if (!grantList.isEmpty()) {
            assignResults.addAll(keystoneRoleAssignmentManagement
                    .batchGrantSysAdmin(clusterName, grantList, Add));
        }
        saveGrantExtrasInfo(assignResults);

        sendAssignmentAuditMessage(stopwatch.stop().elapsed(TimeUnit.MILLISECONDS),
                OperationType.CREATE,
                assignResults);

        return assignResults;
    }

    public List<AssignResult> batchRevokeSysAdmin(List<String> userIdList) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        List<String> sysAdminList = keystoneRoleAssignmentManagement
                .getSysAdminList(EnnContext.getClusterName())
                .stream()
                .map(RoleAssignment::getUserId)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        sysAdminList.removeAll(userIdList);

        List<AssignResult> assignResults = new ArrayList<>();
        if (sysAdminList.isEmpty()) {
            String seedSysAdmin = userIdList.remove(0);
            AssignResult assignResult = new AssignResult();
            assignResult.setCode(2);
            assignResult.setHttpStatus(400);
            assignResult.setRole(SYS_ADMIN);
            assignResult.setUserId(seedSysAdmin);
            assignResult.setMessage("last sys admin can not be unassigned");
            assignResults.add(assignResult);
        }

        assignResults.addAll(keystoneRoleAssignmentManagement
                .batchGrantSysAdmin(EnnContext.getClusterName(), userIdList, Remove));

        saveRevokeExtrasInfo(assignResults);

        sendAssignmentAuditMessage(stopwatch.stop().elapsed(TimeUnit.MILLISECONDS),
                OperationType.DELETE,
                assignResults);
        return assignResults;
    }

    private void saveGrantExtrasInfo(List<AssignResult> results) {
        results.stream()
                .filter(result -> result.getHttpStatus() >= HttpStatus.OK.getStatusCode())
                .filter(result -> result.getHttpStatus() < HttpStatus.BAD_REQUEST.getStatusCode())
                .forEach(result -> {
                    RoleAssignmentExtras extras = assignmentExtrasRepository
                            .findByNamespaceAndUserIdAndRole(null, result.getUserId(), SYS_ADMIN);
                    if (extras == null) {
                        extras = new RoleAssignmentExtras();
                        extras.setUserId(result.getUserId());
                        extras.setRole(SYS_ADMIN);
                    }
                    extras.setAssignedOn(new Date());
                    extras.setActionType(ASSIGN);
                    assignmentExtrasRepository.save(extras);
                });
    }

    private void saveRevokeExtrasInfo(List<AssignResult> results) {
        results.stream()
                .filter(result -> result.getHttpStatus() >= HttpStatus.OK.getStatusCode())
                .filter(result -> result.getHttpStatus() < HttpStatus.BAD_REQUEST.getStatusCode())
                .map(result -> assignmentExtrasRepository
                        .findByNamespaceAndUserIdAndRole(null, result.getUserId(), SYS_ADMIN))
                .filter(Objects::nonNull)
                .forEach(extras -> {
                    extras.setActionType(UNASSIGN);
                    assignmentExtrasRepository.save(extras);
                });
    }

    private void sendAssignmentAuditMessage(long elapsed,
                                            OperationType operationType,
                                            List<AssignResult> assignResults) {
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .elapsed(elapsed)
                .operationType(operationType)
                .resourceType(ResourceType.ROLE_ASSIGNMENT)
                .resourceName(SYS_ADMIN.name())
                .extras(ImmutableMap.of("details", assignResults.toString(),
                        "results", assignResults.toString()))
                .build();
        operationAuditMessageProducer.send(message);
    }
}
