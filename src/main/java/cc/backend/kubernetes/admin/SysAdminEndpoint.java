package cc.backend.kubernetes.admin;

import cc.backend.kubernetes.admin.services.SysAdminManagement;
import cc.backend.kubernetes.namespaces.dto.RoleAssignmentMaintainResponse;
import cc.backend.role.assignment.RoleAssignmentDto;
import cc.keystone.client.domain.AssignResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;


/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/2/8.
 * <p>
 * =================================
 * only sysadmin can use
 * =================================
 */

@Component
@Path("sys/sys_admins")
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "Role", description = "Deprecated. please use cc-account api. Operation about role.",
        produces = "application/json")
@Deprecated
public class SysAdminEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(SysAdminEndpoint.class);

    @Inject
    private SysAdminManagement management;

    @GET
    @ApiOperation(value = "Get sys admin list.", responseContainer = "List", response = RoleAssignmentDto.class)
    public Response getSysAdmins() {
        List<RoleAssignmentDto> dtos = management.getSysAdmins();
        return Response.ok(dtos).build();
    }

    /**
     * TODO use dto instead of map
     *
     * @param map the request
     * @return the response
     */
    @POST
    @ApiOperation(value = "Grant or Revoke sys admin.", response = RoleAssignmentMaintainResponse.class)
    public Response sysAdminMaintain(Map<String, List<String>> map) {
        RoleAssignmentMaintainResponse response = new RoleAssignmentMaintainResponse();

        List<String> addList = map.get("addList");
        if (addList != null) {
            List<AssignResult> assignResults = management.batchGrantSysAdmin(addList);
            response.setAddList(assignResults);
        }

        List<String> removeList = map.get("removeList");
        if (removeList != null) {
            List<AssignResult> unAssignResults = management.batchRevokeSysAdmin(removeList);
            response.setRemoveList(unAssignResults);
        }

        return Response.ok(response).build();
    }
}
