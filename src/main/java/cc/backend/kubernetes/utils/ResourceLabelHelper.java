package cc.backend.kubernetes.utils;

import cc.backend.kubernetes.batch.form.dto.FormLabel;
import io.fabric8.kubernetes.api.model.HasMetadata;
import io.fabric8.kubernetes.api.model.LabelSelector;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.PodTemplateSpec;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.DeploymentSpec;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import io.fabric8.kubernetes.api.model.extensions.StatefulSetSpec;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static cc.backend.kubernetes.Constants.APP_LABEL;

/**
 * @author wangchunyang@gmail.com
 */
public class ResourceLabelHelper {
    public static void addLabel(HasMetadata hasMetadata, String key, String value) {
        ObjectMeta metadata = hasMetadata.getMetadata();
        if (metadata == null) {
            metadata = new ObjectMeta();
        }
        hasMetadata.setMetadata(metadata);

        Map<String, String> labels = metadata.getLabels();
        if (labels == null) {
            labels = new HashMap<>(1);
            metadata.setLabels(labels);
        }
        labels.put(key, value);
    }

    public static String getLabel(HasMetadata hasMetadata, String key) {
        ObjectMeta metadata = hasMetadata.getMetadata();
        if (metadata == null) {
            return null;
        }

        Map<String, String> labels = metadata.getLabels();
        if (labels == null) {
            return null;
        }

        return labels.get(key);
    }

    public static void removeAppLabel(List<FormLabel> formLabelList) {
        for (int i = 0; i < formLabelList.size(); i++) {
            if (formLabelList.get(i).getKey().equals(APP_LABEL)) {
                formLabelList.remove(i);
                return;
            }
        }
    }

    public static void removeAppLabelOfSelector(Deployment deployment) {
        Optional<Deployment> deploymentOp = Optional.ofNullable(deployment);

        deploymentOp.map(Deployment::getSpec)
                .map(DeploymentSpec::getSelector)
                .map(LabelSelector::getMatchLabels)
                .ifPresent(labels -> labels.remove(APP_LABEL));

        deploymentOp.map(Deployment::getSpec)
                .map(DeploymentSpec::getTemplate)
                .map(PodTemplateSpec::getMetadata)
                .map(ObjectMeta::getLabels)
                .ifPresent(labels -> labels.remove(APP_LABEL));
    }

    public static void removeAppLabelOfSelector(StatefulSet statefulSet) {
        Optional<StatefulSet> statefulSetOp = Optional.ofNullable(statefulSet);

        statefulSetOp.map(StatefulSet::getSpec)
                .map(StatefulSetSpec::getSelector)
                .map(LabelSelector::getMatchLabels)
                .ifPresent(labels -> labels.remove(APP_LABEL));

        statefulSetOp.map(StatefulSet::getSpec)
                .map(StatefulSetSpec::getTemplate)
                .map(PodTemplateSpec::getMetadata)
                .map(ObjectMeta::getLabels)
                .ifPresent(labels -> labels.remove(APP_LABEL));
    }

}
