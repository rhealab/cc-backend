package cc.backend.kubernetes.utils;

import cc.backend.kubernetes.storage.StorageRepository;
import cc.backend.kubernetes.storage.domain.StorageType;
import io.fabric8.kubernetes.api.model.Volume;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/7/5. and yangzhixiang
 */
public class HostpathUtils {
    public static long getDeploymentHostpathBytes(String namespaceName,
                                                  Deployment deployment,
                                                  StorageRepository storageRepository) {
        if (deployment == null
                || deployment.getSpec().getTemplate() == null
                || deployment.getSpec().getTemplate().getSpec().getVolumes() == null
                || deployment.getSpec().getReplicas() == null) {
            return 0;
        }

        List<Volume> volumes = deployment.getSpec().getTemplate().getSpec().getVolumes();
        return getControllerHostPathBytes(namespaceName, deployment.getSpec().getReplicas(), volumes, storageRepository);
    }

    public static long getStatefulSetHostPathBytes(String namespaceName, StatefulSet statefulSet, StorageRepository storageRepository) {
        if (statefulSet == null
                || statefulSet.getSpec().getTemplate() == null
                || statefulSet.getSpec().getTemplate().getSpec().getVolumes() == null
                || statefulSet.getSpec().getReplicas() == null) {
            return 0;
        }

        List<Volume> volumes = statefulSet.getSpec().getTemplate().getSpec().getVolumes();
        return getControllerHostPathBytes(namespaceName, statefulSet.getSpec().getReplicas(), volumes, storageRepository);
    }

    public static long getControllerHostPathBytes(String namespaceName,
                                                  int replicas,
                                                  List<Volume> volumes,
                                                  StorageRepository storageRepository) {
        if (volumes == null) {
            return 0;
        }

        List<String> pvcNames = volumes.stream()
                .filter(Objects::nonNull)
                .filter(volume -> volume.getPersistentVolumeClaim() != null)
                .map(volume -> volume.getPersistentVolumeClaim().getClaimName())
                .collect(Collectors.toList());

        if (pvcNames.isEmpty()) {
            return 0;
        }

        return storageRepository.findByNamespaceNameAndPvcNameIn(namespaceName, pvcNames)
                .stream()
                .filter(storage -> storage.getStorageType() == StorageType.HostPath)
                .mapToLong(storage -> storage.isUnshared() ? storage.getAmountBytes() * replicas : storage.getAmountBytes())
                .sum();
    }
}
