package cc.backend.kubernetes.utils;

import cc.backend.kubernetes.workload.dto.PodInfo;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.PodTemplateSpec;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;

import java.util.*;

import static cc.backend.kubernetes.Constants.CRITICAL_POD_ANNOTATION;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.reducing;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/12.
 */
public class CommonUtils {
    /**
     * judge a PodTemplateSpec is critical or not
     *
     * @param podTemplateSpec the pod template for check
     * @return is critical or not
     */
    public static boolean isCriticalPod(PodTemplateSpec podTemplateSpec) {
        String criticalAnnotation = podTemplateSpec
                .getMetadata()
                .getAnnotations()
                .getOrDefault(CRITICAL_POD_ANNOTATION, "false");

        return "true".equals(criticalAnnotation);
    }

    public static List<Pod> filterPods(List<Pod> pods, Map<String, String> selector) {
        List<Pod> filteredPods = new ArrayList<>();
        for (Pod pod : pods) {
            Map<String, String> labels = pod.getMetadata().getLabels();

            boolean isContained = true;
            for (Map.Entry<String, String> entry : selector.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                if (value == null) {
                    if (!(labels.get(key) == null && labels.containsKey(key))) {
                        isContained = false;
                        break;
                    }
                } else {
                    if (!value.equals(labels.get(key))) {
                        isContained = false;
                        break;
                    }
                }
            }

            if (isContained) {
                filteredPods.add(pod);
            }
        }
        return filteredPods;
    }

    public static PodInfo composePodInfo(Deployment deployment, List<Pod> pods) {
        return composePodInfo(deployment.getSpec().getReplicas(), deployment.getStatus().getReplicas(), pods);
    }

    public static PodInfo composePodInfo(StatefulSet statefulSet, List<Pod> pods) {
        return composePodInfo(statefulSet.getSpec().getReplicas(), statefulSet.getStatus().getReplicas(), pods);
    }

    public static PodInfo composePodInfo(Integer desired, Integer current, List<Pod> pods) {
        PodInfo podInfo = new PodInfo();

        podInfo.setDesired(desired == null ? 0 : desired);
        podInfo.setCurrent(current == null ? 0 : current);

        Map<String, Integer> phaseCountMap = Optional.ofNullable(pods)
                .orElseGet(ArrayList::new)
                .stream()
                .map(pod -> pod.getStatus().getPhase())
                .collect(groupingBy(phase -> phase, reducing(0, e -> 1, Integer::sum)));

        podInfo.setPending(phaseCountMap.getOrDefault("Pending", 0));
        podInfo.setRunning(phaseCountMap.getOrDefault("Running", 0));
        podInfo.setFailed(phaseCountMap.getOrDefault("Failed", 0));
        podInfo.setSucceeded(phaseCountMap.getOrDefault("Succeeded", 0));

        return podInfo;
    }

    public static PodTemplateSpec getNewReplicaSetTemplate(Deployment deployment) {
        PodTemplateSpec templateSpec = new PodTemplateSpec();

        templateSpec.setMetadata(deployment.getSpec().getTemplate().getMetadata());
        templateSpec.setSpec(deployment.getSpec().getTemplate().getSpec());

        return templateSpec;
    }

    public static boolean equalIgnoreHash(PodTemplateSpec template1, PodTemplateSpec template2) {
        ObjectMeta metadata1 = template1.getMetadata();
        ObjectMeta metadata2 = template2.getMetadata();
        Map<String, String> labels1 = metadata1.getLabels();
        Map<String, String> labels2 = metadata2.getLabels();

        if (labels1 == null) {
            labels1 = new HashMap<>();
        }
        if (labels2 == null) {
            labels2 = new HashMap<>();
        }

        if (labels1.size() > labels2.size()) {
            Map<String, String> temp = labels1;
            labels1 = labels2;
            labels2 = temp;
        }

        for (Map.Entry<String, String> entry : labels2.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            if (value == null) {
                if (labels1.get(key) != null && !"pod-template-hash".equals(key)) {
                    return false;
                }
            } else if (!value.equals(labels1.get(key)) && !"pod-template-hash".equals(key)) {
                return false;
            }
        }

        labels1 = metadata1.getLabels();
        labels2 = metadata2.getLabels();

        metadata1.setLabels(null);
        metadata2.setLabels(null);

        boolean equals = template1.equals(template2);

        metadata1.setLabels(labels1);
        metadata2.setLabels(labels2);

        return equals;
    }
}
