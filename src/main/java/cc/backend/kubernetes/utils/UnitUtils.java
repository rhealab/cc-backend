package cc.backend.kubernetes.utils;

import cc.backend.kubernetes.storage.domain.BytesUnit;
import io.fabric8.kubernetes.api.model.Quantity;
import io.fabric8.kubernetes.api.model.QuantityBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/21.
 */
public class UnitUtils {

    public static Map<String, Long> cpuUnitMap = new HashMap<>();

    static {
        cpuUnitMap.put(BytesUnit.M.name(), BytesUnit.M.getBytes());
        cpuUnitMap.put(BytesUnit.G.name(), BytesUnit.G.getBytes());
        cpuUnitMap.put(BytesUnit.T.name(), BytesUnit.T.getBytes());
        cpuUnitMap.put(BytesUnit.P.name(), BytesUnit.P.getBytes());
        cpuUnitMap.put(BytesUnit.E.name(), BytesUnit.E.getBytes());
    }

    public static double parseK8sCpuQuantity(Quantity quantity) {
        if (quantity == null) {
            return 0;
        }

        String amount = quantity.getAmount();
        if (amount == null) {
            return 0;
        }

        try {
            return Double.valueOf(amount);
        } catch (NumberFormatException e) {
            if (amount.endsWith("m")) {
                int length = amount.length();
                if (length < 2) {
                    return 0;
                }
                Long value = Long.valueOf(amount.substring(0, length - 1));
                return value / 1000.0;
            } else if (amount.endsWith("k")) {
                int length = amount.length();
                if (length < 2) {
                    return 0;
                }
                Long value = Long.valueOf(amount.substring(0, length - 1));
                return value * 1000.0;
            } else {
                int length = amount.length();
                if (length < 2) {
                    return 0;
                }

                String unit = amount.substring(length - 1, length);
                Optional<Long> unitValue = Optional.ofNullable(cpuUnitMap.get(unit));
                if (unitValue.isPresent()) {
                    return unitValue.get() * Double.valueOf(amount.substring(0, length - 1));
                }
            }
        }

        return 0;
    }


    public static long parseK8sStorageQuantity(Quantity quantity) {
        return parseK8sMemoryQuantity(quantity);
    }

    public static long parseK8sMemoryQuantity(Quantity quantity) {
        if (quantity == null) {
            return 0;
        }

        String amount = quantity.getAmount();

        try {
            return Long.valueOf(amount);
        } catch (NumberFormatException e) {
            if (amount == null) {
                return 0;
            }

            if (amount.endsWith("m")) {
                int length = amount.length();
                if (length < 2) {
                    return 0;
                }
                return Long.valueOf(amount.substring(0, length - 1)) / 1000;
            }


            if (amount.endsWith("k")) {
                int length = amount.length();
                if (length < 3) {
                    return 0;
                }

                Long amountK = Long.valueOf(amount.substring(0, length - 1));
                return amountK * 1024;
            }

            if (amount.endsWith("K") || amount.endsWith("M")
                    || amount.endsWith("G") || amount.endsWith("T")
                    || amount.endsWith("P") || amount.endsWith("E")) {
                return parseDecimalUnit(amount);
            }

            if (amount.endsWith("i")) {
                return parseNormalUnit(amount);
            }
        }

        return 0;
    }

    public static long parseNamespaceStorageQuota(String amount) {
        if (amount == null) {
            return 0;
        }

        if (amount.endsWith("m")) {
            int length = amount.length();
            if (length < 2) {
                return 0;
            }
            return Long.valueOf(amount.substring(0, length - 1));
        }

        if (amount.endsWith("i")) {
            return parseNormalUnit(amount);
        }

        return Long.valueOf(amount);
    }

    public static Quantity toK8sMemoryQuantity(long bytes) {
        String amount = String.valueOf(bytes);
        return new QuantityBuilder().withAmount(amount).build();
    }

    public static Quantity toK8sCpuQuantity(double cores) {
        String amount = String.valueOf(cores);
        return new QuantityBuilder().withAmount(amount).build();
    }


    private static long parseNormalUnit(String amount) {
        int length = amount.length();
        if (length < 3) {
            return 0L;
        }

        BytesUnit unit = BytesUnit.valueOf(amount.substring(length - 2));
        double value = Double.valueOf(amount.substring(0, length - 2));

        return (long) (value * unit.getBytes());
    }

    private static long parseDecimalUnit(String amount) {
        int length = amount.length();
        if (length < 2) {
            return 0L;
        }

        BytesUnit unit = BytesUnit.valueOf(amount.substring(length - 1));
        double value = Double.valueOf(amount.substring(0, length - 1));

        return (long) (value * unit.getBytes());
    }
}
