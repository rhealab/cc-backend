package cc.backend.kubernetes.utils;

import io.fabric8.kubernetes.api.model.HasMetadata;

import static cc.backend.kubernetes.Constants.APP_LABEL;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/7/13.
 */
public class AppUtils {
    public static String getAppName(HasMetadata metadata) {
        return ResourceLabelHelper.getLabel(metadata, APP_LABEL);
    }
}
