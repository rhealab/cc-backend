package cc.backend.kubernetes.utils;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * @author dongsheng on 17-8-16.
 */
public interface Visitor<T> {
    /**
     * @param headers
     * @param inputStream
     * @return must return a concrete object, never a null
     */
    T succeed(Map<String, List<String>> headers, InputStream inputStream);

    /**
     * @param responseCode from http call
     * @param headers      http headers
     * @param inputStream  body
     * @return must return a concrete objects, never a null pointer
     */
    T fail(int responseCode, Map<String, List<String>> headers, InputStream inputStream);
}
