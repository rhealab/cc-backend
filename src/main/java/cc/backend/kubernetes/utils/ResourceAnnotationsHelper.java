package cc.backend.kubernetes.utils;

import cc.backend.audit.request.EnnContext;
import cc.backend.kubernetes.service.dto.ServiceDto;
import cc.backend.kubernetes.workload.deployment.dto.DeploymentDto;
import cc.backend.kubernetes.workload.statefulset.dto.StatefulSetDto;
import io.fabric8.kubernetes.api.model.HasMetadata;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static cc.backend.kubernetes.Constants.*;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/12/20.
 */
public class ResourceAnnotationsHelper {

    public static void addAnnotations(HasMetadata resource, Map<String, String> annotations) {
        getOrGenerateAnnotations(resource).putAll(annotations);
    }

    public static void addAnnotation(HasMetadata resource, String key, String value) {
        getOrGenerateAnnotations(resource).put(key, value);
    }

    private static String getAnnotation(HasMetadata resource, String key) {
        return Optional.ofNullable(getAnnotations(resource))
                .map(annotations -> annotations.get(key))
                .orElse(null);
    }

    public static Map<String, String> getAnnotations(HasMetadata resource) {
        return Optional.ofNullable(resource)
                .map(HasMetadata::getMetadata)
                .map(ObjectMeta::getAnnotations)
                .orElse(null);
    }

    public static void setExtrasInfoForCreate(HasMetadata resource) {
        Map<String, String> extrasInfo = new HashMap<>(2);
        extrasInfo.put(CREATED_BY_ANNOTATION_KEY, EnnContext.getUserId());
        extrasInfo.put(CREATED_ON_MILLIS_ANNOTATION_KEY, String.valueOf(System.currentTimeMillis()));
        addAnnotations(resource, extrasInfo);
    }

    public static void setExtrasInfoForUpdate(HasMetadata resource) {
        Map<String, String> extrasInfo = new HashMap<>(2);
        extrasInfo.put(LAST_UPDATED_BY_ANNOTATION_KEY, EnnContext.getUserId());
        extrasInfo.put(LAST_UPDATED_ON_MILLIS_ANNOTATION_KEY, String.valueOf(System.currentTimeMillis()));
        addAnnotations(resource, extrasInfo);
    }

    public static void addExtrasInfo(Service service, ServiceDto dto) {
        Map<String, String> annotations = getAnnotations(service);
        if (annotations != null) {
            dto.setCreatedBy(annotations.get(CREATED_BY_ANNOTATION_KEY));
            String createMillisStr = annotations.get(CREATED_ON_MILLIS_ANNOTATION_KEY);
            if (createMillisStr != null) {
                Long millis = Long.valueOf(createMillisStr);
                dto.setAge(System.currentTimeMillis() - millis);
                dto.setCreatedOn(new Date(millis));
            }
        }
    }

    public static void addExtrasInfo(Deployment deployment, DeploymentDto dto) {
        Map<String, String> annotations = getAnnotations(deployment);
        if (annotations != null) {
            dto.setCreatedBy(annotations.get(CREATED_BY_ANNOTATION_KEY));
            String createMillisStr = annotations.get(CREATED_ON_MILLIS_ANNOTATION_KEY);
            if (createMillisStr != null) {
                Long millis = Long.valueOf(createMillisStr);
                dto.setAge(System.currentTimeMillis() - millis);
                dto.setCreatedOn(new Date(millis));
            }
        }
    }

    public static void addExtrasInfo(StatefulSet statefulSet, StatefulSetDto dto) {
        Map<String, String> annotations = getAnnotations(statefulSet);
        if (annotations != null) {
            String createMillisStr = annotations.get(CREATED_ON_MILLIS_ANNOTATION_KEY);
            dto.setCreatedBy(annotations.get(CREATED_BY_ANNOTATION_KEY));
            if (createMillisStr != null) {
                Long millis = Long.valueOf(createMillisStr);
                dto.setAge(System.currentTimeMillis() - millis);
                dto.setCreatedOn(new Date(millis));
            }
        }
    }

    private static Map<String, String> getOrGenerateAnnotations(HasMetadata resource) {
        ObjectMeta metadata = resource.getMetadata();
        if (metadata == null) {
            metadata = new ObjectMeta();
            resource.setMetadata(metadata);
        }

        Map<String, String> annotations = metadata.getAnnotations();
        if (annotations == null) {
            annotations = new HashMap<>(1);
            metadata.setAnnotations(annotations);
        }

        return annotations;
    }
}
