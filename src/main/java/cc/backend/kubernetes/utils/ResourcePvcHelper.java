package cc.backend.kubernetes.utils;

import cc.backend.kubernetes.storage.StorageUtils;
import cc.backend.kubernetes.storage.domain.StorageNaming;
import io.fabric8.kubernetes.api.model.PersistentVolumeClaimVolumeSource;
import io.fabric8.kubernetes.api.model.Volume;
import io.fabric8.kubernetes.api.model.extensions.Deployment;

import java.util.List;

/**
 * when user create a deployment and specify a pvc, the pvc name is storage name.
 * we should convert the storage name to actual pvc name
 * <p>
 * when read deployment from k8s, the pvc name is actual pvc name, the user should see storage name.
 * we should convert the actual pvc name to storage name
 *
 * @author normanwang06@gmail.com (wangjinxin)
 */
public class ResourcePvcHelper {

    public static void composePvc(String namespaceName, List<Deployment> deployments) {
        if (deployments == null) {
            return;
        }

        for (Deployment deployment : deployments) {
            composePvc(namespaceName, deployment);
        }
    }

    public static void composePvc(String namespaceName, Deployment deployment) {
        if (deployment == null
                || deployment.getSpec().getTemplate() == null
                || deployment.getSpec().getTemplate().getSpec().getVolumes() == null) {
            return;
        }

        List<Volume> volumes = deployment.getSpec().getTemplate().getSpec().getVolumes();
        for (Volume volume : volumes) {
            PersistentVolumeClaimVolumeSource pvc = volume.getPersistentVolumeClaim();
            if (pvc != null && pvc.getClaimName() != null) {
                String storageName = pvc.getClaimName();
                pvc.setClaimName(StorageUtils.pvcName(namespaceName, storageName));
            }
        }
    }

    public static void parsePvc(String namespaceName, List<Deployment> deployments) {
        if (deployments == null) {
            return;
        }

        for (Deployment deployment : deployments) {
            parsePvc(namespaceName, deployment);
        }
    }

    public static void parsePvc(String namespaceName, Deployment deployment) {

        if (deployment == null
                || deployment.getSpec().getTemplate() == null
                || deployment.getSpec().getTemplate().getSpec().getVolumes() == null) {
            return;
        }

        List<Volume> volumes = deployment.getSpec().getTemplate().getSpec().getVolumes();
        for (Volume volume : volumes) {
            PersistentVolumeClaimVolumeSource pvc = volume.getPersistentVolumeClaim();
            if (pvc != null && pvc.getClaimName() != null) {
                String pvcName = pvc.getClaimName();
                pvc.setClaimName(StorageNaming.parseStorageName(namespaceName, pvcName));
            }
        }
    }
}
