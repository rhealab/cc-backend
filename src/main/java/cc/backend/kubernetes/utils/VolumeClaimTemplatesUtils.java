package cc.backend.kubernetes.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/21.
 */
public class VolumeClaimTemplatesUtils {

    public static String getStorageName(String templateName, String statefulSetName, int podOrder) {
        return templateName + "-" + statefulSetName + "-" + String.valueOf(podOrder);
    }

    public static List<String> getStorageNameList(String templateName, String statefulSetName, int replicas) {
        List<String> storageNameList = new ArrayList<>();
        for (int i = 0; i < replicas; i++) {
            storageNameList.add(getStorageName(templateName, statefulSetName, i));
        }
        return storageNameList;
    }

    public static List<String> getStorageNameList(String templateName, String statefulSetName, int from, int to) {
        List<String> storageNameList = new ArrayList<>();
        for (int i = from; i < to; i++) {
            storageNameList.add(getStorageName(templateName, statefulSetName, i));
        }
        return storageNameList;
    }
}
