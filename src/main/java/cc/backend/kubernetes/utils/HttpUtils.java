package cc.backend.kubernetes.utils;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * @author dongsheng on 17-8-16.
 */
@Component
public class HttpUtils {
    private static final Logger logger = LoggerFactory.getLogger(HttpUtils.class);
    private CloseableHttpClient httpClient;
    private HttpHost target;
    private HttpClientContext localContext;
    private String schedulerServer;
    private int port;

    /**
     * initializing the https client, use basic authentication(not safe)
     *
     * @param schedulerServer
     * @param port
     * @param username
     * @param password
     * @throws KeyStoreException
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     */
    public void init(String schedulerServer, int port, String username, String password) throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        this.schedulerServer = schedulerServer;
        this.port = port;

        SSLContextBuilder builder = new SSLContextBuilder();
        // use no certificate
        builder.loadTrustMaterial(null, new MyTrustStrategy());

        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        target = new HttpHost(schedulerServer, port, "https");
        // use basic
        credsProvider.setCredentials(
                AuthScope.ANY,
                new UsernamePasswordCredentials(username, password));

        httpClient = HttpClients.custom()
                .setDefaultCredentialsProvider(credsProvider)
                .setSSLContext(builder.build())
                .setSSLHostnameVerifier(new NoopHostnameVerifier())
                .build();

        localContext = HttpClientContext.create();
    }

    @PreDestroy
    public void close() {
        logger.info("closing scheduling service http client");
        try {
            httpClient.close();
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("error closing https client");
        }
    }

    public <T> T get(String url, Visitor<T> visitor) {
        try {
            HttpGet get = new HttpGet("https://" + schedulerServer + ":" + port + url);

            CloseableHttpResponse response = httpClient.execute(target, get, localContext);
            if (response.getStatusLine().getStatusCode() == 200) {
                return visitor.succeed(null, response.getEntity().getContent());
            } else {
                logger.error("http code is {}, reason is {}", response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase());
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] writeStreamAsByteArray(InputStream inputStream) {
        byte[] data = new byte[1024];
        int offset = 0;
        byte[] buffer = new byte[1024];
        try {
            int count = inputStream.read(buffer);
            while (count > 0) {
                if (data.length - offset >= count) {
                    System.arraycopy(buffer, 0, data, offset, count);
                    offset = offset + count;
                } else {
                    byte[] temp = new byte[data.length * 2 + count];
                    System.arraycopy(data, 0, temp, 0, offset);
                    data = temp;
                    System.arraycopy(buffer, 0, data, offset, count);
                    offset = offset + count;
                }
                count = inputStream.read(buffer);
            }
            //data has tailing '\0'
            byte[] out = new byte[offset];
            System.arraycopy(data, 0, out, 0, offset);
            return out;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String writeStreamToString(InputStream inputStream) {
        return new String(writeStreamAsByteArray(inputStream));
    }

    public static class MyTrustStrategy implements TrustStrategy {
        @Override
        public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            return true;
        }
    }
}
