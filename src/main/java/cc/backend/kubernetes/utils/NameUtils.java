package cc.backend.kubernetes.utils;

import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.Container;
import io.fabric8.kubernetes.api.model.ContainerPort;
import io.fabric8.kubernetes.api.model.PodSpec;
import io.fabric8.kubernetes.api.model.PodTemplateSpec;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.DeploymentSpec;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import io.fabric8.kubernetes.api.model.extensions.StatefulSetSpec;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

import static cc.backend.kubernetes.Constants.PORT_NAME_PATTERN;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/7/13.
 */
public class NameUtils {
    public static String getStorageClassName(String namespaceName) {
        return namespaceName;
    }

    public static void validatePortName(String namespaceName, String appName, StatefulSet statefulSet) {
        Optional.ofNullable(statefulSet)
                .map(StatefulSet::getSpec)
                .map(StatefulSetSpec::getTemplate)
                .map(PodTemplateSpec::getSpec)
                .map(PodSpec::getContainers)
                .orElseGet(ArrayList::new)
                .stream()
                .map(Container::getPorts)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(ContainerPort::getName)
                .filter(Objects::nonNull)
                .forEach(portName -> {
                    if (!portName.matches(PORT_NAME_PATTERN)) {
                        throw new CcException(BackendReturnCodeNameConstants.PROPERTY_INVALID,
                                ImmutableMap.of("namespace", namespaceName,
                                        "app", appName,
                                        "statefulSet", statefulSet.getMetadata().getName(),
                                        "portName", portName,
                                        "containers.ports.name", "port name formant not correct, must match" + PORT_NAME_PATTERN));
                    }
                });
    }

    public static void validatePortName(String namespaceName, String appName, Deployment deployment) {
        Optional.ofNullable(deployment)
                .map(Deployment::getSpec)
                .map(DeploymentSpec::getTemplate)
                .map(PodTemplateSpec::getSpec)
                .map(PodSpec::getContainers)
                .orElseGet(ArrayList::new)
                .stream()
                .map(Container::getPorts)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(ContainerPort::getName)
                .filter(Objects::nonNull)
                .forEach(portName -> {
                    if (!portName.matches(PORT_NAME_PATTERN)) {
                        throw new CcException(BackendReturnCodeNameConstants.PROPERTY_INVALID,
                                ImmutableMap.of("namespace", namespaceName,
                                        "app", appName,
                                        "deployment", deployment.getMetadata().getName(),
                                        "portName", portName,
                                        "containers.ports.name", "port name formant not correct, must match" + PORT_NAME_PATTERN));
                    }
                });
    }
}
