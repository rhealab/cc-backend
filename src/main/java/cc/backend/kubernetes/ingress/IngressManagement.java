package cc.backend.kubernetes.ingress;

import cc.backend.audit.operation.OperationAuditMessage;
import cc.backend.audit.operation.OperationAuditMessageProducer;
import cc.backend.audit.operation.OperationType;
import cc.backend.audit.operation.ResourceType;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.ingress.dto.CertificateData;
import cc.lib.retcode.CcException;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.Secret;
import io.fabric8.kubernetes.api.model.extensions.Ingress;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static cc.backend.retcode.BackendReturnCodeNameConstants.*;

/**
 * Created by xzy on 18-3-28.
 */
@Component
public class IngressManagement {
  public static final String INGRESS_NAME_LABEL = "X_INGRESS";
  public static final String INGRESS_CLASS = "kubernetes.io/ingress.class";
  @Inject
  private KubernetesClientManager clientManager;
  @Inject
  private OperationAuditMessageProducer operationAuditMessageProducer;

  private String hostnameRegex = "^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$";
  private Pattern pattern = Pattern.compile(hostnameRegex);

  public void createAndAudit(String namespace, Ingress ingress) {
    if (getIngress(namespace, ingress.getMetadata().getName()) != null) {
      throw new CcException(INGRESS_ALREADY_EXISTS);
    }
    operateAndAudit(namespace, ingress, OperationType.CREATE);
  }

  public void updateAndAudit(String namespace, Ingress ingress) {
    operateAndAudit(namespace, ingress, OperationType.UPDATE);
  }

  public void operateAndAudit(String namespace, Ingress ingress, OperationType operationType) {
    Stopwatch stopwatch = Stopwatch.createStarted();
    String ingressName = ingress.getMetadata().getName();
    deleteIngress(namespace, ingressName);
    create(namespace, ingress);
    sendAuditMessage(ingress.getMetadata().getNamespace(), ingressName,
            ResourceType.INGRESS, operationType, stopwatch);
  }

  public void create(String namespace, Ingress ingress) {
    KubernetesClient client = clientManager.getClient();
    String ingressName = ingress.getMetadata().getName();
    Map<String, String> annotations = ingress.getMetadata().getAnnotations();
    // step 1 validate hostname
    ingress.getSpec().getRules().forEach(rule -> {
      if (!validateHost(rule.getHost())) {
        throw new CcException(INVALID_HOST,
                ImmutableMap.of("invalid host", rule.getHost()));
      }
    });
    ingress.getSpec().getTls().stream().flatMap(ingressTLS -> Stream.of(ingressTLS.getHosts().toArray(new String[0]))).forEach(host->{
      if (!validateHost(host)) {
        throw new CcException(INVALID_HOST,
                ImmutableMap.of("invalid host", host));
      }
    });

    List<Secret> secretList = client.secrets().inNamespace(namespace).list().getItems();
    String defaultSecret = getDefaultSecretName(secretList);
    Set<String> secretNames = secretList.stream().map(secret -> secret.getMetadata().getName()).collect(Collectors.toSet());

    ingress.getSpec().getTls().stream()
            .filter(ingressTLS -> ingressTLS.getSecretName() == null || !secretNames.contains(ingressTLS.getSecretName()))
            .forEach(ingressTLS -> ingressTLS.setSecretName(defaultSecret));

    //add annotation
    Map<String, String> labels = ingress.getMetadata().getLabels();
    if (labels == null) {
      labels = new HashMap<>();
      labels.put(INGRESS_NAME_LABEL, ingressName);
      ingress.getMetadata().setLabels(labels);
    } else {
      labels.put(INGRESS_NAME_LABEL, ingressName);
    }

    String ingressClass = annotations.get(INGRESS_CLASS);
    if (ingressClass == null || ingressClass.equals("")) {
      //both inside and outside
      Ingress ingressInside = ingress;
      Ingress ingressOutside = new Ingress(ingress.getApiVersion(), ingress.getKind(),
              ingress.getMetadata(), ingress.getSpec(), ingress.getStatus());

      ingressInside.getMetadata().getAnnotations().put(INGRESS_CLASS, "inside");
      clientManager.getClient().extensions().ingresses().inNamespace(namespace).createOrReplace(ingressInside);

      ingressOutside.getMetadata().setName(ingressName + "-outside");
      ingressOutside.getMetadata().getAnnotations().put(INGRESS_CLASS, "outside");
      clientManager.getClient().extensions().ingresses().inNamespace(namespace).createOrReplace(ingressOutside);
    } else if (ingressClass.equals("inside") || ingressClass.equals("outside")) {
      clientManager.getClient().extensions().ingresses().inNamespace(namespace).createOrReplace(ingress);
    } else {
      throw new CcException(UNKNOWN_INGRESS_CLASS);
    }
  }

  public List<Ingress> getAllIngress(String namespace) {
    return clientManager.getClient().extensions().ingresses().inNamespace(namespace).list().getItems();
  }

  public Collection<Ingress> getConsoleIngresses(String namespace) {
    List<Ingress> allIngresses = clientManager.getClient().extensions().ingresses().inNamespace(namespace).list().getItems();
    Map<String, Ingress> ingressMap = new HashMap<>();
    for (Ingress ing : allIngresses) {
      Map<String, String> labels = ing.getMetadata().getLabels();
      if (labels != null && labels.get(INGRESS_NAME_LABEL) != null) {
        //ingress created by console
        String ingressName = labels.get(INGRESS_NAME_LABEL);
        Ingress half = ingressMap.get(ingressName);
        ing.getMetadata().setName(ingressName);
        if (half == null) {
          ingressMap.put(ingressName, ing);
        } else {
          Ingress half2 = ing;
          String ingressClass = half.getMetadata().getAnnotations().get(INGRESS_CLASS);
          String ingressClass2 = half2.getMetadata().getAnnotations().get(INGRESS_CLASS);
          if ("inside".equals(ingressClass) && "outside".equals(ingressClass2) || "inside".equals(ingressClass2) && "outside".equals(ingressClass)) {
            half.getMetadata().getAnnotations().put(INGRESS_CLASS, "inside&outside");
          } else {
            throw new CcException(UNKNOWN_INGRESS_CLASS,
                    ImmutableMap.of("ingress_class1", ingressClass, "ingress_class2", ingressClass2));
          }
        }
      } else {
        //ingress created by kubectl
        ingressMap.put(ing.getMetadata().getName(), ing);
      }
    }
    return ingressMap.values();
  }

  public Ingress getIngress(String namespace, String ingressName) {
    Ingress ingress = null;
    List<Ingress> allIngresses = clientManager.getClient().extensions().ingresses().inNamespace(namespace).list().getItems();
    for (Ingress ing : allIngresses) {
      Map<String, String> labels = ing.getMetadata().getLabels();
      if (labels != null && ingressName.equals(labels.get(INGRESS_NAME_LABEL))) {
        if (ingress == null) {
          ingress = ing;
        } else {
          ingress.getMetadata().getAnnotations().put(INGRESS_CLASS, "inside&outside");
        }
      } else if (ingressName.equals(ing.getMetadata().getName())) {
        return ing;
      }
    }
    if (ingress != null) {
      ingress.getMetadata().setName(ingressName);
    }
    return ingress;
  }

  public void deleteIngress(String namespace, String ingressName) {
    if (ingressName == null || ingressName.length() == 0) {
      throw new CcException(INGRESS_NAME_CAN_NOT_BE_EMPTY);
    }
    List<Ingress> allIngresses = clientManager.getClient().extensions().ingresses().inNamespace(namespace).list().getItems();
    for (Ingress ing : allIngresses) {
      Map<String, String> labels = ing.getMetadata().getLabels();
      if (ingressName.equals(ing.getMetadata().getName()) || (labels != null && ingressName.equals(labels.get(INGRESS_NAME_LABEL))) ) {
        clientManager.getClient().extensions().ingresses().inNamespace(namespace).delete(ing);
      }
    }
  }

  public void deleteAndAudit(String namespace, String ingressName) {
    Stopwatch stopwatch = Stopwatch.createStarted();
    deleteIngress(namespace, ingressName);
    sendAuditMessage(namespace, ingressName, ResourceType.INGRESS, OperationType.DELETE, stopwatch);
  }

  public List<Secret> getAllSecrets(String namespace) {
    return clientManager.getClient().secrets().inNamespace(namespace).list().getItems();
  }

  public Map<String, String> createSecret(String namespace, String ingressName, CertificateData certificateData) {
    Stopwatch stopwatch = Stopwatch.createStarted();
    String secretName = generateCertificateName(ingressName, certificateData.getHost());
    ObjectMeta objectMeta = new ObjectMeta();
    objectMeta.setName(secretName);
    objectMeta.setNamespace(namespace);
    objectMeta.setLabels(ImmutableMap.of(INGRESS_NAME_LABEL, ingressName));
    Secret secret = new Secret("v1", ImmutableMap.of("tls.crt", certificateData.getCertificate(), "tls.key", certificateData.getKey()),
            "Secret", objectMeta, null, "Opaque");
    clientManager.getClient().secrets().inNamespace(namespace).createOrReplace(secret);
    sendAuditMessage(namespace, secretName, ResourceType.SECRET, OperationType.CREATE, stopwatch);
    return ImmutableMap.of("secretName", secretName);
  }

  private void sendAuditMessage(String namespace, String name, ResourceType resourceType, OperationType operationType, Stopwatch stopwatch) {
    long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
    OperationAuditMessage message = new OperationAuditMessage.Builder()
            .elapsed(elapsed)
            .namespaceName(namespace)
            .operationType(operationType)
            .resourceType(resourceType)
            .resourceName(name).build();
    operationAuditMessageProducer.send(message);
  }

  public void deleteSecret(String namespace, String secretName) {
    Stopwatch stopwatch = Stopwatch.createStarted();
    clientManager.getClient().secrets().inNamespace(namespace).withName(secretName).delete();
    sendAuditMessage(namespace, secretName, ResourceType.SECRET, OperationType.DELETE, stopwatch);
  }

  public String generateCertificateName(String ingressName, String host) {
    if (!validateHost(host)) {
      throw new CcException(INVALID_HOST,
              ImmutableMap.of("invalid host", host));
    }
    if (ingressName == null || ingressName.equals("")) {
      throw new CcException(INGRESS_NAME_CAN_NOT_BE_EMPTY);
    }
    return ingressName + "-" + host.replace('.', '-');
  }

  public String randomLetters(int length) {
    Random rand = new Random();
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < length; i++) {
      rand.nextInt(26);
      char temp = (char) ('a' + rand.nextInt(26));
      sb.append(temp);
    }
    return sb.toString();
  }

  public boolean validateHost(String host) {
    if (host == null) {
      return false;
    }
    Matcher matcher = pattern.matcher(host);
    return matcher.matches();
  }

  /**
   * return the name of the secret with annotation kubernetes.io/service-account.name as default
   * @param secretList
   * @return null if not available
   */
  public String getDefaultSecretName(List<Secret> secretList) {
    for (Secret s : secretList) {
      Map<String, String> annotations = s.getMetadata().getAnnotations();
      if (annotations != null && "default".equals(annotations.get("kubernetes.io/service-account.name"))) {
        return s.getMetadata().getName();
      }
    }
    return null;
  }
}
