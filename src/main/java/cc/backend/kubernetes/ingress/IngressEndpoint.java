package cc.backend.kubernetes.ingress;

import cc.backend.kubernetes.ingress.dto.CertificateData;
import io.fabric8.kubernetes.api.model.extensions.Ingress;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by xzy on 18-3-28.
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes/namespaces/{namespace}/ingress")
@Api(value = "Ingress", description = "Operation about ingress.", produces = "application/json")
public class IngressEndpoint {
  @Inject
  IngressManagement ingressManagement;
  @POST
  @ApiOperation(value = "create ingress.")
  public Response createIngress(@PathParam("namespace") String namespace, Ingress ingress) {
    ingressManagement.createAndAudit(namespace, ingress);
    return Response.ok().build();
  }

  @PUT
  @ApiOperation(value = "update ingress.")
  public Response updateIngress(@PathParam("namespace") String namespace, Ingress ingress) {
    ingressManagement.updateAndAudit(namespace, ingress);
    return Response.ok().build();
  }

  @GET
  @Path("kubernetes")
  @ApiOperation(value = "get all kubernetes ingress.", response = List.class)
  public Response getIngresses(@PathParam("namespace") String namespace) {
    return Response.ok(ingressManagement.getAllIngress(namespace)).build();
  }

  @GET
  @ApiOperation(value = "get all console ingress.", response = List.class)
  public Response getConsoleIngresses(@PathParam("namespace") String namespace) {
    return Response.ok(ingressManagement.getConsoleIngresses(namespace)).build();
  }

  @GET
  @Path("{ingress}")
  @ApiOperation(value = "get one ingress.", response = Ingress.class)
  public Response getOneIngress(@PathParam("namespace") String namespace, @PathParam("ingress") String ingressName) {
    return Response.ok(ingressManagement.getIngress(namespace, ingressName)).build();
  }

  @DELETE
  @Path("{ingress}")
  @ApiOperation(value = "delete one ingress.", response = Ingress.class)
  public Response deleteOneIngress(@PathParam("namespace") String namespace, @PathParam("ingress") String ingressName) {
    ingressManagement.deleteAndAudit(namespace, ingressName);
    return Response.ok().build();
  }

  @POST
  @Path("{ingress}/secret")
  @ApiOperation(value = "create secret.", response = Map.class)
  public Response createSecret(@PathParam("namespace") String namespace, @PathParam("ingress") String ingress, CertificateData certificateData) {
    return Response.ok(ingressManagement.createSecret(namespace, ingress, certificateData)).build();
  }

  @GET
  @Path("secrets")
  @ApiOperation(value = "get all secrets.", response = List.class)
  public Response getSecrets(@PathParam("namespace") String namespace) {
    return Response.ok(ingressManagement.getAllSecrets(namespace)).build();
  }

  @DELETE
  @Path("secret/{secret}")
  @ApiOperation(value = "delete one secret.")
  public Response deleteSecret(@PathParam("namespace") String namespace, @PathParam("secret") String secretName) {
    ingressManagement.deleteSecret(namespace, secretName);
    return Response.ok().build();
  }
}
