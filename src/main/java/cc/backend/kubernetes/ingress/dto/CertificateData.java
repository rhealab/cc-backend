package cc.backend.kubernetes.ingress.dto;

/**
 * Created by xzy on 18-3-29.
 */
public class CertificateData {
  private String host;
  private String certificate;
  private String key;

  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public String getCertificate() {
    return certificate;
  }

  public void setCertificate(String certificate) {
    this.certificate = certificate;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }
}
