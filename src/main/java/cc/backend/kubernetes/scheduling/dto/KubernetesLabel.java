package cc.backend.kubernetes.scheduling.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author dongsheng on 17-9-30.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KubernetesLabel implements Comparable<KubernetesLabel> {
    private String key;
    private String value;

    public KubernetesLabel() {
    }

    public KubernetesLabel(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int compareTo(KubernetesLabel o) {
        int compareKey = key.compareTo(o.getKey());
        if (compareKey != 0) {
            return compareKey;
        } else {
            return value.compareTo(o.getValue());
        }
    }
}
