package cc.backend.kubernetes.scheduling.data;

import javax.persistence.*;

/**
 * @author dongsheng on 17-9-26.
 */
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"type", "labelKey"})})
public class NamespaceDefaultLabels {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(nullable = false)
    private String type;
    @Column(nullable = false)
    private String labelKey;
    @Column(nullable = false)
    private String value;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLabelKey() {
        return labelKey;
    }

    public void setLabelKey(String labelKey) {
        this.labelKey = labelKey;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
