package cc.backend.kubernetes.scheduling.data;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author dongsheng on 17-9-26.
 */
public interface NamespaceDefaultLabelsRepository extends JpaRepository<NamespaceDefaultLabels, Long> {
}
