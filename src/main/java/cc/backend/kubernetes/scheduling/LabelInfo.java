package cc.backend.kubernetes.scheduling;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

import static cc.backend.kubernetes.scheduling.NodeSelector.KeyValue;

/**
 * @author dongsheng xzy on 17-9-21.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LabelInfo {
    private List<NamespaceInfo> match;
    private List<NamespaceInfo> notMatch;
    private List<String> nodes;

    public LabelInfo() {
        match = new ArrayList<>();
        notMatch = new ArrayList<>();
        nodes = new ArrayList<>();
    }

    public List<NamespaceInfo> getMatch() {
        return match;
    }

    public void setMatch(List<NamespaceInfo> match) {
        this.match = match;
    }

    public List<NamespaceInfo> getNotMatch() {
        return notMatch;
    }

    public void setNotMatch(List<NamespaceInfo> notMatch) {
        this.notMatch = notMatch;
    }

    public List<String> getNodes() {
        return nodes;
    }

    public void setNodes(List<String> nodes) {
        this.nodes = nodes;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class NamespaceInfo {
        private String namespace;
        private List<KeyValue> match;
        private List<KeyValue> mustMatch;
        private List<KeyValue> notMatch;
        private List<KeyValue> mustNotMatch;

        public NamespaceInfo() {
            match = new ArrayList<>();
            notMatch = new ArrayList<>();
        }

        public String getNamespace() {
            return namespace;
        }

        public void setNamespace(String namespace) {
            this.namespace = namespace;
        }

        public List<KeyValue> getMatch() {
            return match;
        }

        public void setMatch(List<KeyValue> match) {
            this.match = match;
        }

        public List<KeyValue> getMustMatch() {
            return mustMatch;
        }

        public void setMustMatch(List<KeyValue> mustMatch) {
            this.mustMatch = mustMatch;
        }

        public List<KeyValue> getNotMatch() {
            return notMatch;
        }

        public void setNotMatch(List<KeyValue> notMatch) {
            this.notMatch = notMatch;
        }

        public List<KeyValue> getMustNotMatch() {
            return mustNotMatch;
        }

        public void setMustNotMatch(List<KeyValue> mustNotMatch) {
            this.mustNotMatch = mustNotMatch;
        }
    }
}
