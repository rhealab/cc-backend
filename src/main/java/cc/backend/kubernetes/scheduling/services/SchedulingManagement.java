package cc.backend.kubernetes.scheduling.services;

import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.scheduling.*;
import cc.backend.kubernetes.scheduling.data.NamespaceDefaultLabels;
import cc.backend.kubernetes.scheduling.data.NamespaceDefaultLabelsRepository;
import cc.backend.kubernetes.scheduling.dto.KubernetesLabel;
import io.fabric8.kubernetes.api.model.DoneableNode;
import io.fabric8.kubernetes.api.model.Node;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.client.dsl.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static cc.backend.kubernetes.scheduling.LabelInfo.NamespaceInfo;
import static cc.backend.kubernetes.scheduling.NodeSelector.KeyValue;
import static cc.backend.kubernetes.scheduling.SchedulingClient.Operation.*;
import static cc.backend.kubernetes.scheduling.SchedulingClient.SelectorType.MATCH;
import static cc.backend.kubernetes.scheduling.SchedulingClient.SelectorType.NOT_MATCH;

/**
 * @author dongsheng on 17-9-21.
 * @author modified by NormanWang06@gmail.com (wangjinxin) on 2018/1/2.
 */
@Component
public class SchedulingManagement {
    private static final Logger logger = LoggerFactory.getLogger(SchedulingManagement.class);

    @Inject
    SchedulingClient schedulingClient;

    @Inject
    protected KubernetesClientManager clientManager;

    @Inject
    NamespaceDefaultLabelsRepository namespaceDefaultLabelsRepository;

    public NodeSelector getNamespaceLabels(String namespace) {
        Map<String, NodeSelector> nodeSelector = schedulingClient.getNamespacesNodeSelector();
        return nodeSelector.get(namespace);
    }

    /**
     * @param label label could not be empty
     * @return label info
     */
    public LabelInfo getLabelInfo(KeyValue label) {
        Map<String, NodeSelector> namespaceNodeSelectorMap = schedulingClient.getNamespacesNodeSelector();

        LabelInfo labelInfo = new LabelInfo();

        Optional.ofNullable(namespaceNodeSelectorMap)
                .orElseGet(HashMap::new)
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue() != null)
                .forEach(entry -> {
                    List<KeyValue> match = entry.getValue().getMatch();
                    if (match != null) {
                        for (KeyValue kv : match) {
                            if (label.equals(kv)) {
                                labelInfo.getMatch().add(newNamespaceInfo(entry.getKey(), entry.getValue()));
                                break;
                            }
                        }
                    }

                    List<KeyValue> notMatch = entry.getValue().getNotMatch();
                    if (notMatch != null) {
                        for (KeyValue kv : notMatch) {
                            if (label.equals(kv)) {
                                labelInfo.getNotMatch().add(newNamespaceInfo(entry.getKey(), entry.getValue()));
                                break;
                            }
                        }
                    }
                });
        labelInfo.setNodes(nodesWithLabel(label));
        return labelInfo;
    }

    public Set<KeyValue> getAllLabels() {
        List<Node> nodeList = clientManager.getClient()
                .nodes()
                .list()
                .getItems();

        Set<KeyValue> labels = Optional.ofNullable(nodeList)
                .orElseGet(ArrayList::new)
                .stream()
                .map(node -> node.getMetadata().getLabels())
                .filter(Objects::nonNull)
                .map(Map::entrySet)
                .flatMap(Collection::stream)
                .map(entry -> new KeyValue(entry.getKey(), entry.getValue()))
                .collect(Collectors.toSet());

        Map<String, NodeSelector> nodeSelectorMap = schedulingClient.getNamespacesNodeSelector();

        Optional.ofNullable(nodeSelectorMap)
                .map(Map::values)
                .orElseGet(ArrayList::new)
                .forEach(nodeSelector -> {
                    if (nodeSelector.getMatch() != null) {
                        labels.addAll(nodeSelector.getMatch());
                    }

                    if (nodeSelector.getNotMatch() != null) {
                        labels.addAll(nodeSelector.getNotMatch());
                    }
                });

        return labels;
    }

    public List<KubernetesLabel> getNodeLabels(String nodeName) {
        Node node = clientManager.getClient()
                .nodes()
                .withName(nodeName)
                .get();

        return Optional.ofNullable(node)
                .map(Node::getMetadata)
                .map(ObjectMeta::getLabels)
                .map(Map::entrySet)
                .orElseGet(HashSet::new)
                .stream()
                .map(entry -> new KubernetesLabel(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }

    public SchedulingMessage addLabelToNamespaces(String key,
                                                  String value,
                                                  NamespaceConfig namespaceConfig) {
        Optional<SchedulingMessage> errorMsg = Optional.ofNullable(namespaceConfig)
                .map(NamespaceConfig::getMatch)
                .orElseGet(ArrayList::new)
                .stream()
                .map(namespaceName -> schedulingClient.operateSelector(ADD, namespaceName, MATCH, key, value))
                .filter(msg -> msg.getCode() != 0)
                .findAny();

        if (errorMsg.isPresent()) {
            return errorMsg.get();
        }

        errorMsg = Optional.ofNullable(namespaceConfig)
                .map(NamespaceConfig::getNotMatch)
                .orElseGet(ArrayList::new)
                .stream()
                .map(namespaceName -> schedulingClient.operateSelector(ADD, namespaceName, NOT_MATCH, key, value))
                .filter(msg -> msg.getCode() != 0)
                .findAny();

        return errorMsg.orElse(null);
    }

    public SchedulingMessage updateNamespacesLabel(String key,
                                                   String value,
                                                   NamespaceConfig namespaceConfig) {
        Map<String, NodeSelector> namespaceNodeSelectorMap = schedulingClient.getNamespacesNodeSelector();
        SchedulingMessage msg;
        //match
        for (String namespaceName : namespaceConfig.getMatch()) {
            NodeSelector nodeSelector = namespaceNodeSelectorMap.get(namespaceName);
            msg = updateNamespacesLabel(namespaceName, key, value, nodeSelector,
                    MATCH, NodeSelector::getMatch);
            if (msg.getCode() != 0) {
                return msg;
            }
        }

        Optional<SchedulingMessage> errorMsg = namespaceNodeSelectorMap.entrySet()
                .stream()
                .filter(entry -> !namespaceConfig.getMatch().contains(entry.getKey()))
                .filter(entry -> entry.getValue() != null && entry.getValue().getMatch() != null)
                .map(entry -> {
                    List<KeyValue> match = entry.getValue().getMatch();
                    return match.stream()
                            .filter(kv -> key.equals(kv.getKey()) && value.equals(kv.getValue()))
                            .map(kv -> schedulingClient.operateSelector(DELETE, entry.getKey(), MATCH, key, value))
                            .filter(response -> response.getCode() != 0)
                            .findAny()
                            .orElse(null);
                })
                .filter(Objects::nonNull)
                .findAny();

        if (errorMsg.isPresent()) {
            return errorMsg.get();
        }

        //not match
        for (String namespaceName : namespaceConfig.getNotMatch()) {
            NodeSelector nodeSelector = namespaceNodeSelectorMap.get(namespaceName);
            msg = updateNamespacesLabel(namespaceName, key, value, nodeSelector,
                    NOT_MATCH, NodeSelector::getNotMatch);
            if (msg.getCode() != 0) {
                return msg;
            }
        }

        errorMsg = namespaceNodeSelectorMap.entrySet()
                .stream()
                .filter(entry -> !namespaceConfig.getMatch().contains(entry.getKey()))
                .filter(entry -> entry.getValue() != null && entry.getValue().getNotMatch() != null)
                .map(entry -> {
                    List<KeyValue> notMatch = entry.getValue().getNotMatch();
                    return notMatch.stream()
                            .filter(kv -> key.equals(kv.getKey()) && value.equals(kv.getValue()))
                            .map(kv -> schedulingClient.operateSelector(DELETE, entry.getKey(), NOT_MATCH, key, value))
                            .filter(response -> response.getCode() != 0)
                            .findAny()
                            .orElse(null);
                })
                .filter(Objects::nonNull)
                .findAny();
        return errorMsg.orElse(null);
    }

    private SchedulingMessage updateNamespacesLabel(String namespaceName,
                                                    String key,
                                                    String value,
                                                    NodeSelector nodeSelector,
                                                    SchedulingClient.SelectorType selectorType,
                                                    Function<NodeSelector, List<KeyValue>> function) {
        SchedulingMessage msg;

        Optional<KeyValue> existsKeyValue = Optional.ofNullable(nodeSelector)
                .map(function)
                .orElseGet(ArrayList::new)
                .stream()
                .filter(keyValue -> key.equals(keyValue.getKey()))
                .findAny();

        if (existsKeyValue.isPresent()) {
            if (value.equals(existsKeyValue.get().getValue())) {
                msg = schedulingClient.operateSelector(UPDATE, namespaceName, selectorType, key, value);
                if (msg.getCode() != 0) {
                    return msg;
                }
            }
        } else {
            msg = schedulingClient.operateSelector(ADD, namespaceName, selectorType, key, value);
            if (msg.getCode() != 0) {
                return msg;
            }
        }

        return null;
    }

    public SchedulingMessage deleteNamespacesFromLabel(String key,
                                                       String value,
                                                       NamespaceConfig namespaceConfig) {
        if (namespaceConfig == null) {
            return null;
        }

        for (String ns : namespaceConfig.getMatch()) {
            SchedulingMessage msg = schedulingClient.operateSelector(DELETE, ns, MATCH, key, value);
            if (msg.getCode() != 0) {
                return msg;
            }
        }
        for (String ns : namespaceConfig.getNotMatch()) {
            SchedulingMessage msg = schedulingClient.operateSelector(DELETE, ns, NOT_MATCH, key, value);
            if (msg.getCode() != 0) {
                return msg;
            }
        }
        return null;
    }

    public SchedulingMessage addLabelsToNamespaces(String namespace, LabelsConfig labelsConfig) {
        if (labelsConfig == null) {
            return null;
        }
        for (KeyValue kv : labelsConfig.getMatch()) {
            SchedulingMessage msg = schedulingClient.operateSelector(ADD, namespace, MATCH, kv.getKey(), kv.getValue());
            if (msg.getCode() != 0) {
                return msg;
            }
        }

        for (KeyValue kv : labelsConfig.getNotMatch()) {
            SchedulingMessage msg = schedulingClient.operateSelector(ADD, namespace, NOT_MATCH, kv.getKey(), kv.getValue());
            if (msg.getCode() != 0) {
                return msg;
            }
        }
        return null;
    }

    public SchedulingMessage deleteAllLabelsOfNamespace(String namespace) {
        return updateLabelsOfNamespace(namespace, new LabelsConfig());
    }

    public SchedulingMessage updateLabelsOfNamespace(String namespace, LabelsConfig labelsConfig) {
        if (labelsConfig == null) {
            labelsConfig = new LabelsConfig();
        }

        SchedulingMessage msg;
        Map<String, NodeSelector> allNamespaces = schedulingClient.getNamespacesNodeSelector();
        NodeSelector nodeSelector = allNamespaces.get(namespace);
        if (nodeSelector == null) {
            for (KeyValue kv : labelsConfig.getMatch()) {
                msg = schedulingClient.operateSelector(ADD, namespace, MATCH, kv.getKey(), kv.getValue());
                if (msg.getCode() != 0) {
                    return msg;
                }
            }
            for (KeyValue kv : labelsConfig.getNotMatch()) {
                msg = schedulingClient.operateSelector(ADD, namespace, NOT_MATCH, kv.getKey(), kv.getValue());
                if (msg.getCode() != 0) {
                    return msg;
                }
            }
        } else {
            msg = updateNamespace(labelsConfig.getMatch(), nodeSelector.getMatch(), namespace, MATCH);
            if (msg != null && msg.getCode() != 0) {
                return msg;
            }
            msg = updateNamespace(labelsConfig.getNotMatch(), nodeSelector.getNotMatch(), namespace, NOT_MATCH);
            if (msg != null && msg.getCode() != 0) {
                return msg;
            }
        }
        return null;
    }

    public SchedulingMessage updateNamespace(List<KeyValue> whatWeWant,
                                             List<KeyValue> whatWeHaveNow,
                                             String namespace,
                                             SchedulingClient.SelectorType type) {
        SchedulingMessage msg;
        for (KeyValue kv : whatWeWant) {
            boolean notFound = true;
            for (KeyValue i : whatWeHaveNow) {
                if (kv.getKey().equals(i.getKey())) {
                    notFound = false;
                    if (!kv.getValue().equals(i.getValue())) {
                        msg = schedulingClient.operateSelector(UPDATE, namespace, type, kv.getKey(), kv.getValue());
                        if (msg.getCode() != 0) {
                            return msg;
                        }
                    }
                    break;
                }
            }
            if (notFound) {
                msg = schedulingClient.operateSelector(ADD, namespace, type, kv.getKey(), kv.getValue());
                if (msg.getCode() != 0) {
                    return msg;
                }
            }
        }
        //delete those not in toKeep
        for (KeyValue i : whatWeHaveNow) {
            boolean notFound = true;
            for (KeyValue j : whatWeWant) {
                if (i.getKey().equals(j.getKey())) {
                    notFound = false;
                    break;
                }
            }
            if (notFound) {
                msg = schedulingClient.operateSelector(DELETE, namespace, type, i.getKey(), i.getValue());
                if (msg.getCode() != 0) {
                    return msg;
                }
            }
        }
        return null;
    }

    public SchedulingMessage deleteLabelsFromNamesapces(String namespace, LabelsConfig labelsConfig) {
        if (labelsConfig == null) {
            return null;
        }
        for (KeyValue kv : labelsConfig.getMatch()) {
            SchedulingMessage msg = schedulingClient.operateSelector(DELETE, namespace, MATCH, kv.getKey(), kv.getValue());
            if (msg.getCode() != 0) {
                return msg;
            }
        }
        for (KeyValue kv : labelsConfig.getNotMatch()) {
            SchedulingMessage msg = schedulingClient.operateSelector(DELETE, namespace, NOT_MATCH, kv.getKey(), kv.getValue());
            if (msg.getCode() != 0) {
                return msg;
            }
        }
        return null;
    }

    /**
     * return nodes name with given label
     *
     * @param label key and value of label, could not be null
     * @return node name list
     */
    public List<String> nodesWithLabel(KeyValue label) {
        List<Node> nodeList = clientManager.getClient()
                .nodes()
                .list()
                .getItems();

        return Optional.ofNullable(nodeList)
                .orElseGet(ArrayList::new)
                .stream()
                .map(Node::getMetadata)
                .filter(metadata -> metadata.getLabels() != null)
                .filter(metadata -> label.getValue().equals(metadata.getLabels().get(label.getKey())))
                .map(ObjectMeta::getName)
                .collect(Collectors.toList());
    }

    public void addNodesToLabel(String key, String value, Set<String> nodes) {
        if (nodes == null) {
            return;
        }
        nodes.forEach(nodeName -> {
            Resource<Node, DoneableNode> node = clientManager.getClient().nodes().withName(nodeName);
            if (node != null) {
                node.edit().editMetadata().addToLabels(key, value).endMetadata().done();
            }
        });
    }

    public void updateNodesOfLabel(String key, String value, Set<String> nodes) {
        if (nodes == null) {
            nodes = new HashSet<>();
        }
        List<Node> allNodes = clientManager.getClient().nodes().list().getItems();
        if (allNodes != null) {
            for (Node n : allNodes) {
                if (nodes.contains(n.getMetadata().getName())) {
                    Resource<Node, DoneableNode> node = clientManager.getClient().nodes().withName(n.getMetadata().getName());
                    if (node != null) {
                        node.edit().editMetadata().addToLabels(key, value).endMetadata().done();
                    }
                } else {
                    Resource<Node, DoneableNode> node = clientManager.getClient().nodes().withName(n.getMetadata().getName());
                    if (node != null) {
                        node.edit().editMetadata().removeFromLabels(key).endMetadata().done();
                    }
                }
            }
        }
    }

    public void deleteNodesFromLabel(String key, Set<String> nodes) {
        if (nodes == null) {
            return;
        }
        nodes.forEach(nodeName -> {
            Resource<Node, DoneableNode> node = clientManager.getClient().nodes().withName(nodeName);
            if (node != null) {
                node.edit().editMetadata().removeFromLabels(key).endMetadata().done();
            }
        });
    }

    /**
     * throws exception if failed
     *
     * @param labels,  could be null(tested)
     * @param nodeName node name
     */
    public void addLabelsToNode(List<KubernetesLabel> labels, String nodeName) {
        if (labels == null) {
            return;
        }
        Map<String, String> map = new HashMap<>(labels.size());
        labels.forEach(l -> map.put(l.getKey(), l.getValue()));
        Resource<Node, DoneableNode> node = clientManager.getClient().nodes().withName(nodeName);
        if (node != null) {
            node.edit().editMetadata().addToLabels(map).endMetadata().done();
        }
    }

    public void updateLabelsOfNode(List<KubernetesLabel> labels, String nodeName) {
        if (labels == null) {
            return;
        }
        Map<String, String> map = new HashMap<>(labels.size());
        labels.forEach(l -> map.put(l.getKey(), l.getValue()));
        Resource<Node, DoneableNode> node = clientManager.getClient().nodes().withName(nodeName);
        if (node != null) {
            node.edit().editMetadata().withLabels(map).endMetadata().done();
        }
    }

    /**
     * @param labels   could have key value pairs which is not in node
     * @param nodeName node name
     */
    public void deleteLabelsFromNode(List<KubernetesLabel> labels, String nodeName) {
        if (labels == null) {
            return;
        }
        Map<String, String> map = new HashMap<>(labels.size());
        labels.forEach(l -> map.put(l.getKey(), l.getValue()));
        Resource<Node, DoneableNode> node = clientManager.getClient().nodes().withName(nodeName);
        if (node != null) {
            node.edit().editMetadata().removeFromLabels(map).endMetadata().done();
        }
    }

    public LabelsConfig getDefaultLabels() {
        List<NamespaceDefaultLabels> labels = namespaceDefaultLabelsRepository.findAll();
        LabelsConfig labelsConfig = new LabelsConfig();
        labels.forEach(i -> {
            KeyValue kv = new KeyValue(i.getLabelKey(), i.getValue());
            if (MATCH.getQueryStr().equals(i.getType())) {
                labelsConfig.getMatch().add(kv);
            } else if (NOT_MATCH.getQueryStr().equals(i.getType())) {
                labelsConfig.getNotMatch().add(kv);
            }
        });
        return labelsConfig;
    }

    /**
     * if the user want to delete some labels that are not in the namespace's default labels
     * this method would not give any warning, it simple ignore it
     */
    public void deleteDefaultLabelsOfNamespace(LabelsConfig labelsConfig) {
        if (labelsConfig == null) {
            return;
        }

        List<NamespaceDefaultLabels> allLabels = namespaceDefaultLabelsRepository.findAll();
        List<NamespaceDefaultLabels> toBeDeleted = new ArrayList<>();

        labelsConfig.getMatch().forEach(item -> allLabels.forEach(i -> {
            if (MATCH.getQueryStr().equals(i.getType()) && item.getKey().equals(i.getLabelKey()) && item.getValue().equals(i.getValue())) {
                toBeDeleted.add(i);
            }
        }));

        labelsConfig.getNotMatch().forEach(item -> allLabels.forEach(i -> {
            if (NOT_MATCH.getQueryStr().equals(i.getType()) && item.getKey().equals(i.getLabelKey()) && item.getValue().equals(i.getValue())) {
                toBeDeleted.add(i);
            }
        }));

        namespaceDefaultLabelsRepository.deleteInBatch(toBeDeleted);
    }

    /**
     * add or update default labels
     *
     * @param labelsConfig config
     */
    public void setDefaultLabelsOfNamespace(LabelsConfig labelsConfig) {
        if (labelsConfig == null) {
            return;
        }

        List<NamespaceDefaultLabels> allLabels = namespaceDefaultLabelsRepository.findAll();
        Set<NamespaceDefaultLabels> toBeUpdated = new HashSet<>();
        List<NamespaceDefaultLabels> toBeDeleted = new ArrayList<>();

        match:
        for (KeyValue kv : labelsConfig.getMatch()) {

            for (NamespaceDefaultLabels i : allLabels) {
                if (MATCH.getQueryStr().equals(i.getType()) && kv.getKey().equals(i.getLabelKey())) {
                    i.setValue(kv.getValue());
                    toBeUpdated.add(i);
                    continue match;
                }
            }
            //go to here means not found this label in db
            NamespaceDefaultLabels l = new NamespaceDefaultLabels();
            l.setType(MATCH.getQueryStr());
            l.setLabelKey(kv.getKey());
            l.setValue(kv.getValue());
            toBeUpdated.add(l);
        }

        //not match
        notmatch:
        for (KeyValue kv : labelsConfig.getNotMatch()) {

            for (NamespaceDefaultLabels i : allLabels) {
                if (NOT_MATCH.equals(i.getType()) && kv.getKey().equals(i.getLabelKey())) {
                    i.setValue(kv.getValue());
                    toBeUpdated.add(i);
                    continue notmatch;
                }
            }
            //go to here means not found this label in db
            NamespaceDefaultLabels l = new NamespaceDefaultLabels();
            l.setType(NOT_MATCH.getQueryStr());
            l.setLabelKey(kv.getKey());
            l.setValue(kv.getValue());
            toBeUpdated.add(l);
        }

        for (NamespaceDefaultLabels i : allLabels) {
            if (!toBeUpdated.contains(i)) {
                toBeDeleted.add(i);
            }
        }
        namespaceDefaultLabelsRepository.save(toBeUpdated);
        namespaceDefaultLabelsRepository.deleteInBatch(toBeDeleted);
    }

    /**
     * if apply failed for one namespace, abort and return the error message
     *
     * @param namespaces the namespaces
     * @return result message
     */
    public SchedulingMessage apply(List<String> namespaces) {
        if (namespaces == null) {
            return null;
        }
        for (String i : namespaces) {
            SchedulingMessage msg = schedulingClient.refreshNamespace(i);
            if (msg.getCode() != 0) {
                return msg;
            }
        }
        return null;
    }

    public NamespaceInfo newNamespaceInfo(String ns, NodeSelector nodeSelector) {
        NamespaceInfo namespaceInfo = new NamespaceInfo();
        namespaceInfo.setNamespace(ns);
        if (nodeSelector.getMatch() != null) {
            namespaceInfo.setMatch(nodeSelector.getMatch());
        }
        if (nodeSelector.getNotMatch() != null) {
            namespaceInfo.setNotMatch(nodeSelector.getNotMatch());
        }
        if (nodeSelector.getMustMatch() != null) {
            namespaceInfo.setMustMatch(nodeSelector.getMustMatch());
        }
        if (nodeSelector.getMustNotMatch() != null) {
            namespaceInfo.setMustNotMatch(nodeSelector.getMustNotMatch());
        }
        return namespaceInfo;
    }

    public String getSystemLabels() {
        return "{\n" +
                "   \"match\": [],\n" +
                "   \"notMatch\": [\n" +
                "    {\n" +
                "     \"key\": \"enndata.cn/systemnode\",\n" +
                "     \"value\": \"*\"\n" +
                "    }\n" +
                "   ]\n" +
                "}";
    }
}
