package cc.backend.kubernetes.scheduling;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dongsheng on 17-9-25.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NamespaceConfig {
    private List<String> match;
    private List<String> notMatch;
    private List<String> mustMatch;
    private List<String> mustNotMatch;

    public NamespaceConfig() {
        match = new ArrayList<>();
        notMatch = new ArrayList<>();
        mustMatch = new ArrayList<>();
        mustNotMatch = new ArrayList<>();
    }

    public List<String> getMatch() {
        return match;
    }

    public void setMatch(List<String> match) {
        this.match = match;
    }

    public List<String> getNotMatch() {
        return notMatch;
    }

    public void setNotMatch(List<String> notMatch) {
        this.notMatch = notMatch;
    }

    public List<String> getMustMatch() {
        return mustMatch;
    }

    public void setMustMatch(List<String> mustMatch) {
        this.mustMatch = mustMatch;
    }

    public List<String> getMustNotMatch() {
        return mustNotMatch;
    }

    public void setMustNotMatch(List<String> mustNotMatch) {
        this.mustNotMatch = mustNotMatch;
    }
}
