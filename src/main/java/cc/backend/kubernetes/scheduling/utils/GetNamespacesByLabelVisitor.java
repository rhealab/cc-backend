package cc.backend.kubernetes.scheduling.utils;

import cc.backend.kubernetes.scheduling.NodeSelector;
import cc.backend.kubernetes.utils.HttpUtils;
import cc.backend.kubernetes.utils.Visitor;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * @author dongsheng on 17-9-21.
 */
public class GetNamespacesByLabelVisitor implements Visitor<Map<String, NodeSelector>> {
    private static final Logger logger = LoggerFactory.getLogger(GetNamespacesByLabelVisitor.class);
    private ObjectMapper mapper = new ObjectMapper();

    @Override
    public Map<String, NodeSelector> succeed(Map<String, List<String>> headers, InputStream inputStream) {
        try {
            Map<String, NodeSelector> namespaces = mapper.readValue(inputStream, new TypeReference<Map<String, NodeSelector>>() {
            });
            return namespaces;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Map<String, NodeSelector> fail(int responseCode, Map<String, List<String>> headers, InputStream inputStream) {
        String err = HttpUtils.writeStreamToString(inputStream);
        logger.error("{}", err);
        return null;
    }
}
