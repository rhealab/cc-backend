package cc.backend.kubernetes.scheduling.utils;

import cc.backend.kubernetes.scheduling.SchedulingMessage;
import cc.backend.kubernetes.utils.Visitor;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * @author dongsheng on 17-9-25.
 */
public class GetMessageVisitor implements Visitor<SchedulingMessage> {
    private ObjectMapper mapper = new ObjectMapper();

    @Override
    public SchedulingMessage succeed(Map<String, List<String>> headers, InputStream inputStream) {
        try {
            SchedulingMessage msg = mapper.readValue(inputStream, SchedulingMessage.class);
            return msg;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public SchedulingMessage fail(int responseCode, Map<String, List<String>> headers, InputStream inputStream) {
        try {
            SchedulingMessage msg = mapper.readValue(inputStream, SchedulingMessage.class);
            return msg;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
