package cc.backend.kubernetes.scheduling;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

import static cc.backend.kubernetes.scheduling.NodeSelector.KeyValue;

/**
 * @author dongsheng on 17-9-25.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LabelsConfig {
    private List<KeyValue> match;
    private List<KeyValue> notMatch;
    private List<KeyValue> mustMatch;
    private List<KeyValue> mustNotMatch;

    public LabelsConfig() {
        match = new ArrayList<>();
        notMatch = new ArrayList<>();
        mustMatch = new ArrayList<>();
        mustNotMatch = new ArrayList<>();
    }

    public List<KeyValue> getMatch() {
        return match;
    }

    public void setMatch(List<KeyValue> match) {
        this.match = match;
    }

    public List<KeyValue> getNotMatch() {
        return notMatch;
    }

    public void setNotMatch(List<KeyValue> notMatch) {
        this.notMatch = notMatch;
    }

    public List<KeyValue> getMustMatch() {
        return mustMatch;
    }

    public void setMustMatch(List<KeyValue> mustMatch) {
        this.mustMatch = mustMatch;
    }

    public List<KeyValue> getMustNotMatch() {
        return mustNotMatch;
    }

    public void setMustNotMatch(List<KeyValue> mustNotMatch) {
        this.mustNotMatch = mustNotMatch;
    }
}
