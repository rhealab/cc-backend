package cc.backend.kubernetes.scheduling;

/**
 * @author dongsheng on 17-9-25.
 */
public class SchedulingMessage {
    private int code;
    private String msg;
    private String data;

    public SchedulingMessage() {
    }

    public SchedulingMessage(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
