package cc.backend.kubernetes.scheduling;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dongsheng on 17-9-21.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NodeSelector {
    private List<KeyValue> match;
    private List<KeyValue> mustMatch;
    private List<KeyValue> notMatch;
    private List<KeyValue> mustNotMatch;

    public NodeSelector() {
        match = new ArrayList<>();
        notMatch = new ArrayList<>();
    }

    public List<KeyValue> getMatch() {
        return match;
    }

    public void setMatch(List<KeyValue> match) {
        if (match != null) {
            this.match = match;
        }
    }

    public List<KeyValue> getMustMatch() {
        return mustMatch;
    }

    public void setMustMatch(List<KeyValue> mustMatch) {
        this.mustMatch = mustMatch;
    }

    public List<KeyValue> getNotMatch() {
        return notMatch;
    }

    public void setNotMatch(List<KeyValue> notMatch) {
        if (notMatch != null) {
            this.notMatch = notMatch;
        }
    }

    public List<KeyValue> getMustNotMatch() {
        return mustNotMatch;
    }

    public void setMustNotMatch(List<KeyValue> mustNotMatch) {
        this.mustNotMatch = mustNotMatch;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class KeyValue implements Comparable<KeyValue> {
        private static final Logger logger = LoggerFactory.getLogger(KeyValue.class);
        public static final String EMPTY_STRING = "";
        private String key;
        private String value;


        @Override
        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }

            if (o instanceof KeyValue) {
                boolean isKeyEqual;
                if (key != null) {
                    // null is not instanceof KeyValue, so o is not null
                    isKeyEqual = key.equals(((KeyValue) o).getKey());
                } else {
                    isKeyEqual = ((KeyValue) o).getKey() == null;
                }
                boolean isValueEqual;
                if (value != null) {
                    isValueEqual = value.equals(((KeyValue) o).getValue());
                } else {
                    isValueEqual = ((KeyValue) o).getValue() == null;
                }
                return isKeyEqual && isValueEqual;
            }
            return false;
        }

        @Override
        public int hashCode() {
            return (key != null ? key.hashCode() : 0) + (value != null ? value.hashCode() : 0);
        }

        public KeyValue() {
            this.key = EMPTY_STRING;
            this.value = EMPTY_STRING;
        }

        public KeyValue(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        @Override
        public int compareTo(KeyValue o) {
            int compareKey = key.compareTo(o.getKey());
            if (compareKey != 0) {
                return compareKey;
            } else {
                return value.compareTo(o.getValue());
            }
        }
    }
}
