package cc.backend.kubernetes.scheduling;

import cc.backend.kubernetes.scheduling.dto.KubernetesLabel;
import cc.backend.kubernetes.scheduling.services.SchedulingManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Set;

/**
 * @author dongsheng on 17-9-21.
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes/scheduling")
public class SchedulingEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(SchedulingEndpoint.class);

    @Inject
    private SchedulingManagement management;

    @Inject
    private SchedulingValidator validator;

    @GET
    @Path("/namespace/{namespace}")
    public Response getNamespaceLabels(@PathParam("namespace") String namespaceName) {
        return Response.ok(management.getNamespaceLabels(namespaceName)).build();
    }

    @GET
    @Path("/label")
    public Response getLabelInfo(@QueryParam("key") String key,
                                 @QueryParam("value") String value) {
        logger.info("{} : {}", key, value);
        NodeSelector.KeyValue kv = new NodeSelector.KeyValue(key, value);
        return Response.ok(management.getLabelInfo(kv)).build();
    }

    @GET
    @Path("/labels")
    public Response getAllLabels() {
        return Response.ok(management.getAllLabels()).build();
    }

    @GET
    @Path("/node/{node}/labels")
    public Response getNodeLabels(@PathParam("node") String node) {
        return Response.ok(management.getNodeLabels(node)).build();
    }

    @POST
    @Path("/label/add")
    public Response addLabelToNamespaces(@QueryParam("key") String key,
                                         @QueryParam("value") String value,
                                         NamespaceConfig namespaceConfig) {
        SchedulingMessage msg = management
                .addLabelToNamespaces(key, value, validator.validate(namespaceConfig));
        if (msg == null) {
            return Response.ok().build();
        } else {
            return Response.status(400).entity(msg).build();
        }
    }

    @POST
    @Path("/label/update")
    public Response updateNamespacesLabel(@QueryParam("key") String key,
                                          @QueryParam("value") String value,
                                          NamespaceConfig namespaceConfig) {
        SchedulingMessage msg = management
                .updateNamespacesLabel(key, value, validator.validate(namespaceConfig));
        if (msg == null) {
            return Response.ok().build();
        } else {
            return Response.status(400).entity(msg).build();
        }
    }

    @POST
    @Path("/label/delete")
    public Response deleteNamespacesFromLabel(@QueryParam("key") String key, @QueryParam("value") String value, NamespaceConfig namespaceConfig) {
        SchedulingMessage msg = management
                .deleteNamespacesFromLabel(key, value, validator.validate(namespaceConfig));
        if (msg == null) {
            return Response.ok().build();
        } else {
            return Response.status(400).entity(msg).build();
        }
    }

    @POST
    @Path("/namespace/{namespace}/add")
    public Response addLabelsToNamespace(@PathParam("namespace") String namespace, LabelsConfig labelsConfig) {
        SchedulingMessage msg = management.addLabelsToNamespaces(namespace, validator.validate(labelsConfig));
        if (msg == null) {
            return Response.ok().build();
        } else {
            return Response.status(400).entity(msg).build();
        }
    }

    @POST
    @Path("/namespace/{namespace}/update")
    public Response updateLabelsOfNamespace(@PathParam("namespace") String namespace, LabelsConfig labelsConfig) {
        SchedulingMessage msg = management.updateLabelsOfNamespace(namespace, validator.validate(labelsConfig));
        if (msg == null) {
            return Response.ok().build();
        } else {
            return Response.status(400).entity(msg).build();
        }
    }

    @POST
    @Path("/namespace/{namespace}/delete")
    public Response deleteLabelsFromNamespace(@PathParam("namespace") String namespace, LabelsConfig labelsConfig) {
        SchedulingMessage msg = management.deleteLabelsFromNamesapces(namespace, validator.validate(labelsConfig));
        if (msg == null) {
            return Response.ok().build();
        } else {
            return Response.status(400).entity(msg).build();
        }
    }

    @POST
    @Path("/node/{node}/add")
    public Response addLabelsToNode(@PathParam("node") String node, List<KubernetesLabel> labels) {
        management.addLabelsToNode(labels, node);
        return Response.ok().build();
    }

    @POST
    @Path("/node/{node}/update")
    public Response updateLabelsOfNode(@PathParam("node") String node, List<KubernetesLabel> labels) {
        management.updateLabelsOfNode(labels, node);
        return Response.ok().build();
    }

    @POST
    @Path("/node/{node}/delete")
    public Response deleteLabelsFromNode(@PathParam("node") String node, List<KubernetesLabel> labels) {
        management.deleteLabelsFromNode(labels, node);
        return Response.ok().build();
    }

    @POST
    @Path("/label/addNodes")
    public Response addNodesToLabel(@QueryParam("key") String key, @QueryParam("value") String value, Set<String> nodes) {
        management.addNodesToLabel(key, value, nodes);
        return Response.ok().build();
    }

    @POST
    @Path("/label/updateNodes")
    public Response updateNodesOfLabel(@QueryParam("key") String key,
                                       @QueryParam("value") String value,
                                       Set<String> nodes) {
        management.updateNodesOfLabel(key, value, nodes);
        return Response.ok().build();
    }

    @POST
    @Path("/label/deleteNodes")
    public Response deleteNodesFromLabel(@QueryParam("key") String key, @QueryParam("value") String value, Set<String> nodes) {
        management.deleteNodesFromLabel(key, nodes);
        return Response.ok().build();
    }

    @GET
    @Path("/namespace/defaultLabels")
    public Response getDefaultLabels() {
        LabelsConfig labelsConfig = management.getDefaultLabels();
        return Response.ok(labelsConfig).build();
    }

    @POST
    @Path("/namespace/set/defaultLabels")
    public Response addOrUpdateDefaultLabels(LabelsConfig labelsConfig) {
        management.setDefaultLabelsOfNamespace(validator.validate(labelsConfig));
        return Response.ok().build();
    }

    @POST
    @Path("/namespace/delete/defaultLabels")
    public Response deleteDefaultLabels(LabelsConfig labelsConfig) {
        management.deleteDefaultLabelsOfNamespace(validator.validate(labelsConfig));
        return Response.ok().build();
    }

    @POST
    @Path("/apply")
    public Response apply(List<String> namespaces) {
        SchedulingMessage msg = management.apply(namespaces);
        if (msg == null) {
            return Response.ok().build();
        } else {
            return Response.status(400).build();
        }
    }

    @GET
    @Path("/system/labels")
    public Response getSystemLabels() {
        return Response.ok(management.getSystemLabels()).build();
    }
}
