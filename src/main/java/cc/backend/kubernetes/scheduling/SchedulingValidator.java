package cc.backend.kubernetes.scheduling;

import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.lib.retcode.CcException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2018/1/3.
 */
@Component
public class SchedulingValidator {
    public NamespaceConfig validate(NamespaceConfig namespaceConfig) {
        if (namespaceConfig == null) {
            return new NamespaceConfig();
        }

        namespaceConfig.setMatch(removeDuplicate(namespaceConfig.getMatch()));
        namespaceConfig.setNotMatch(removeDuplicate(namespaceConfig.getNotMatch()));
        namespaceConfig.setMustMatch(removeDuplicate(namespaceConfig.getMustMatch()));
        namespaceConfig.setMustNotMatch(removeDuplicate(namespaceConfig.getMustNotMatch()));
        return namespaceConfig;
    }

    public List<String> removeDuplicate(List<String> list) {
        List<String> temp = new ArrayList<>();

        for (String current : list) {
            boolean alreadyThere = false;
            for (String aTemp : temp) {
                if (current.equals(aTemp)) {
                    alreadyThere = true;
                    break;
                }
            }
            if (!alreadyThere) {
                temp.add(current);
            }
        }
        return temp;
    }

    public LabelsConfig validate(LabelsConfig labels) {
        LabelsConfig temp = new LabelsConfig();
        if (labels != null) {
            List<NodeSelector.KeyValue> match = labels.getMatch();
            List<NodeSelector.KeyValue> notMatch = labels.getNotMatch();
            List<NodeSelector.KeyValue> mustMatch = labels.getMustMatch();
            List<NodeSelector.KeyValue> mustNotmatch = labels.getMustNotMatch();

            temp.setMatch(validateList(match));
            temp.setNotMatch(validateList(notMatch));
            temp.setMustMatch(validateList(mustMatch));
            temp.setMustNotMatch(validateList(mustNotmatch));
        }
        return temp;
    }

    public List<NodeSelector.KeyValue> validateList(List<NodeSelector.KeyValue> list) {
        List<NodeSelector.KeyValue> temp = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            NodeSelector.KeyValue kv = list.get(i);
            boolean notSame = true;
            for (int j = i + 1; j < list.size(); j++) {
                NodeSelector.KeyValue kv1 = list.get(j);
                if (kv.getKey().equals(kv1.getKey())) {
                    if (kv.getValue().equals(kv1.getValue())) {
                        notSame = false;
                        break;
                    } else {
                        throw new CcException(BackendReturnCodeNameConstants.SAME_KEY_DIFFERENT_VALUE_ERROR);
                    }
                }
            }
            if (notSame) {
                temp.add(kv);
            }
        }
        return temp;
    }
}
