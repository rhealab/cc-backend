package cc.backend.kubernetes.scheduling;

import cc.backend.EnnProperties;
import cc.backend.common.utils.OkHttpClientUtils;
import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.lib.retcode.CcException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Map;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2018/1/2.
 */
@Component
public class SchedulingClient {
    private static final Logger logger = LoggerFactory.getLogger(SchedulingClient.class);
    @Inject
    private EnnProperties properties;

    public enum SelectorType {
        MATCH("match"),
        NOT_MATCH("notmatch"),
        MUST_MATCH("mustmatch"),
        MUST_NOT_MATCH("mustnotmatch");

        private String queryStr;

        SelectorType(String queryStr) {
            this.queryStr = queryStr;
        }

        public String getQueryStr() {
            return queryStr;
        }
    }

    public enum Operation {
        ADD("add"),
        UPDATE("update"),
        DELETE("delete");

        private String operationStr;

        Operation(String operationStr) {
            this.operationStr = operationStr;
        }

        public String getOperationStr() {
            return operationStr;
        }
    }

    public SchedulingMessage refreshNamespace(String namespace) {
        return exec("/nsnodeselector/refresh/" + namespace, "GET", null,
                new TypeReference<SchedulingMessage>() {
                });
    }

    public Map<String, NodeSelector> getNamespacesNodeSelector() {
        return exec("/nsnodeselector", "GET", null,
                new TypeReference<Map<String, NodeSelector>>() {
                });
    }

    public SchedulingMessage operateSelector(Operation operation,
                                             String namespaceName,
                                             SelectorType type,
                                             String key,
                                             String value) {
        if (key != null && value != null) {
            String url = "/nsnodeselector/" +
                    operation.getOperationStr() +
                    "?namespace=" +
                    namespaceName +
                    "&type=" +
                    type.getQueryStr() +
                    "&key=" +
                    key +
                    "&value=" +
                    value;
            return exec(url, "GET", null,
                    new TypeReference<SchedulingMessage>() {
                    });
        } else {
            return new SchedulingMessage(0);
        }
    }

    public <T> T exec(String subUrl,
                      String requestMethod,
                      RequestBody requestBody,
                      TypeReference<T> type) {
        EnnProperties.Cluster.Scheduler scheduler = properties.getCurrentCluster().getScheduler();
        Request request = new Request.Builder()
                .method(requestMethod, requestBody)
                .url("https://" + scheduler.getHost() + ":" + scheduler.getPort() + subUrl)
                .header("Authorization", scheduler.getBasicToken())
                .build();
        try {
            Response response = OkHttpClientUtils
                    .getUnsafeOkHttpClient()
                    .newCall(request)
                    .execute();

            ResponseBody body = response.body();
            if (response.isSuccessful() && body != null) {
                ObjectMapper mapper = new ObjectMapper();
                mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                return mapper.readValue(body.bytes(), type);
            }

            if (body != null) {
                body.close();
            }

            throw new CcException(BackendReturnCodeNameConstants.SCHEDULING_SERVER_ERROR,
                    ImmutableMap.of("msg", response.message(),
                            "code", response.code()));
        } catch (IOException e) {
            throw new CcException(BackendReturnCodeNameConstants.SCHEDULING_SERVER_ERROR,
                    ImmutableMap.of("msg", e.getMessage(),
                            "cause", e.getCause()));
        }
    }
}
