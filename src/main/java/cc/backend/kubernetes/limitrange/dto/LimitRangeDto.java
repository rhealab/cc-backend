package cc.backend.kubernetes.limitrange.dto;

import cc.backend.kubernetes.Constants;
import cc.backend.kubernetes.limitrange.data.SysLimitRange;
import cc.backend.kubernetes.utils.UnitUtils;
import io.fabric8.kubernetes.api.model.LimitRange;
import io.fabric8.kubernetes.api.model.LimitRangeItem;
import org.springframework.util.CollectionUtils;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Comparator;
import java.util.List;

/**
 * @author yanzhixiang
 */
public class LimitRangeDto {
    @DecimalMin(value = "0.1", message = "limit range min cpu must larger than 0.1")
    @NotNull
    private double minCpu;

    @Min(value = 128 * 1024L * 1024L, message = "limit range min memory must larger than 128Mi.")
    @NotNull
    private long minMemory;

    @DecimalMin(value = "0.1", message = "limit range default request cpu must larger than 0.1")
    @NotNull
    private double defaultRequestCpu;

    @Min(value = 128 * 1024L * 1024L, message = "limit range default request memory must larger than 128Mi.")
    @NotNull
    private long defaultRequestMemory;

    @DecimalMin(value = "0.1", message = "limit range default limit cpu must larger than 0.1")
    @NotNull
    private double defaultLimitCpu;

    @Min(value = 128 * 1024L * 1024L, message = "limit range default limit memory must larger than 128Mi.")
    @NotNull
    private long defaultLimitMemory;

    public double getMinCpu() {
        return minCpu;
    }

    public void setMinCpu(double minCpu) {
        this.minCpu = minCpu;
    }

    public long getMinMemory() {
        return minMemory;
    }

    public void setMinMemory(long minMemory) {
        this.minMemory = minMemory;
    }

    public double getDefaultRequestCpu() {
        return defaultRequestCpu;
    }

    public void setDefaultRequestCpu(double defaultRequestCpu) {
        this.defaultRequestCpu = defaultRequestCpu;
    }

    public long getDefaultRequestMemory() {
        return defaultRequestMemory;
    }

    public void setDefaultRequestMemory(long defaultRequestMemory) {
        this.defaultRequestMemory = defaultRequestMemory;
    }

    public double getDefaultLimitCpu() {
        return defaultLimitCpu;
    }

    public void setDefaultLimitCpu(double defaultLimitCpu) {
        this.defaultLimitCpu = defaultLimitCpu;
    }

    public long getDefaultLimitMemory() {
        return defaultLimitMemory;
    }

    public void setDefaultLimitMemory(long defaultLimitMemory) {
        this.defaultLimitMemory = defaultLimitMemory;
    }


    public static LimitRangeDto from(SysLimitRange limitRange) {
        if (limitRange == null) {
            return new LimitRangeDto();
        }

        LimitRangeDto dto = new LimitRangeDto();
        dto.setMinCpu(limitRange.getMinCpu());
        dto.setMinMemory(limitRange.getMinMemory());

        dto.setDefaultRequestCpu(limitRange.getDefaultRequestCpu());
        dto.setDefaultRequestMemory(limitRange.getDefaultRequestMemory());

        dto.setDefaultLimitCpu(limitRange.getDefaultLimitCpu());
        dto.setDefaultLimitMemory(limitRange.getDefaultLimitMemory());

        return dto;
    }

    public static LimitRangeDto from(List<LimitRange> limitRangeList) {
        if (CollectionUtils.isEmpty(limitRangeList)) {
            return new LimitRangeDto();
        }

        limitRangeList.sort(Comparator.comparing(limitRange ->
                limitRange.getMetadata().getCreationTimestamp(),Comparator.reverseOrder()
        ));

        for (LimitRange limitRange : limitRangeList) {

            List<LimitRangeItem> limitRangeItemList = limitRange.getSpec().getLimits();

            for (LimitRangeItem limitRangeItem : limitRangeItemList) {
                if (limitRangeItem.getType().equals(Constants.K8S_LIMITRANGE_CONTAINER_TYPE)) {
                    LimitRangeDto dto = new LimitRangeDto();
                    dto.setMinCpu(UnitUtils.parseK8sCpuQuantity
                            (limitRangeItem.getMin().get(Constants.K_CPU)));
                    dto.setMinMemory(UnitUtils.parseK8sMemoryQuantity
                            (limitRangeItem.getMin().get(Constants.K_MEMORY)));

                    dto.setDefaultRequestCpu(UnitUtils.parseK8sCpuQuantity
                            (limitRangeItem.getDefaultRequest().get(Constants.K_CPU)));
                    dto.setDefaultRequestMemory(UnitUtils.parseK8sMemoryQuantity
                            (limitRangeItem.getDefaultRequest().get(Constants.K_MEMORY)));

                    dto.setDefaultLimitCpu(UnitUtils.parseK8sCpuQuantity
                            (limitRangeItem.getDefault().get(Constants.K_CPU)));
                    dto.setDefaultLimitMemory(UnitUtils.parseK8sMemoryQuantity
                            (limitRangeItem.getDefault().get(Constants.K_MEMORY)));
                    return dto;
                }
            }
        }
        return new LimitRangeDto();
    }
}
