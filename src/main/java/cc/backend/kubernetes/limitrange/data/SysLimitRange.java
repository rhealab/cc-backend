package cc.backend.kubernetes.limitrange.data;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author yanzhixiang
 */
@Entity
public class SysLimitRange {
    @Id
    private long id;

    private double minCpu;
    private long minMemory;

    private double defaultRequestCpu;
    private long defaultRequestMemory;

    private double defaultLimitCpu;
    private long defaultLimitMemory;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getMinCpu() {
        return minCpu;
    }

    public void setMinCpu(double minCpu) {
        this.minCpu = minCpu;
    }

    public long getMinMemory() {
        return minMemory;
    }

    public void setMinMemory(long minMemory) {
        this.minMemory = minMemory;
    }

    public double getDefaultRequestCpu() {
        return defaultRequestCpu;
    }

    public void setDefaultRequestCpu(double defaultRequestCpu) {
        this.defaultRequestCpu = defaultRequestCpu;
    }

    public long getDefaultRequestMemory() {
        return defaultRequestMemory;
    }

    public void setDefaultRequestMemory(long defaultRequestMemory) {
        this.defaultRequestMemory = defaultRequestMemory;
    }

    public double getDefaultLimitCpu() {
        return defaultLimitCpu;
    }

    public void setDefaultLimitCpu(double defaultLimitCpu) {
        this.defaultLimitCpu = defaultLimitCpu;
    }

    public long getDefaultLimitMemory() {
        return defaultLimitMemory;
    }

    public void setDefaultLimitMemory(long defaultLimitMemory) {
        this.defaultLimitMemory = defaultLimitMemory;
    }
}
