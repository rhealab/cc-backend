package cc.backend.kubernetes.limitrange.data;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author yanzhixiang
 */
public interface SysLimitRangeRepository extends JpaRepository<SysLimitRange, Long> {
}
