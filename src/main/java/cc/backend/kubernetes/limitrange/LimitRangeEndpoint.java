package cc.backend.kubernetes.limitrange;

import cc.backend.kubernetes.limitrange.dto.LimitRangeDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author yanzhixiang
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("sys/kubernetes/limitranges")
@Api(value = "LimitRange", description = "Operation about system limit range.", produces = "application/json")
public class LimitRangeEndpoint {
    @Inject
    private LimitRangeManagement limitRangeManagement;


    @GET
    @ApiOperation(value = "Get system limit range.", response = LimitRangeDto.class)
    public Response getSysLimitRange() {
        return Response.ok(limitRangeManagement.getSysLimitRange()).build();
    }

    @PUT
    @ApiOperation(value = "Update system limit range.", response = LimitRangeDto.class)
    public Response updateSysLimitRange(@Valid LimitRangeDto request) {
        LimitRangeDto res = limitRangeManagement.updateSysLimitRange(request);
        return Response.ok(res).build();
    }
}
