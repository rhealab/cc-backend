package cc.backend.kubernetes.limitrange;

import cc.backend.kubernetes.limitrange.dto.LimitRangeDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author xzy on 17-11-23.
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes/namespaces/{namespaceName}/limitranges")
@Api(value = "LimitRange", description = "Operation about system limit range.", produces = "application/json")
public class NsLimitRangeEndpoint {
    @Inject
    private LimitRangeManagement limitRangeManagement;

    @GET
    @ApiOperation(value = "Get system limit range.", response = LimitRangeDto.class)
    public Response getSysLimitRange(@PathParam("namespaceName") String namespaceName) {
        return Response.ok(limitRangeManagement.getNsLimitRange(namespaceName)).build();
    }
}
