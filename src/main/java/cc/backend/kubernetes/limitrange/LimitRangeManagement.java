package cc.backend.kubernetes.limitrange;

import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.limitrange.data.SysLimitRange;
import cc.backend.kubernetes.limitrange.data.SysLimitRangeRepository;
import cc.backend.kubernetes.limitrange.dto.LimitRangeDto;
import cc.lib.retcode.CcException;
import io.fabric8.kubernetes.api.model.LimitRange;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

/**
 * @author yanzhixiang
 */
@Component
public class LimitRangeManagement {
    @Inject
    private SysLimitRangeRepository limitRangeRepository;

    @Inject
    private KubernetesClientManager clientManager;

    public LimitRangeDto getSysLimitRange() {
        SysLimitRange limitRange = limitRangeRepository.findOne(1L);
        return LimitRangeDto.from(limitRange);
    }

    public LimitRangeDto getNsLimitRange(String namespaceName) {
        List<LimitRange> limitRangeList = clientManager.getClient()
                .limitRanges()
                .inNamespace(namespaceName)
                .list()
                .getItems();

        return LimitRangeDto.from(limitRangeList);
    }

    public LimitRangeDto updateSysLimitRange(LimitRangeDto request) {
        SysLimitRange limitRange = limitRangeRepository.findOne(1L);
        if (limitRange == null) {
            limitRange = new SysLimitRange();
            limitRange.setId(1);
        }

        if (request.getMinCpu() > request.getDefaultRequestCpu()) {
            throw new CcException(BackendReturnCodeNameConstants.MIN_CPU_LARGER_THAN_DEFAULT_REQUEST_CPU);
        }

        if (request.getDefaultRequestCpu() > request.getDefaultLimitCpu()) {
            throw new CcException(BackendReturnCodeNameConstants.DEFAULT_REQUEST_CPU_LARGER_THAN_DEFAULT_LIMIT_CPU);
        }

        if (request.getMinMemory() > request.getDefaultRequestMemory()) {
            throw new CcException(BackendReturnCodeNameConstants.MIN_MEMORY_LARGER_THAN_DEFAULT_REQUEST_MEMORY);
        }

        if (request.getDefaultRequestMemory() > request.getDefaultLimitMemory()) {
            throw new CcException(BackendReturnCodeNameConstants.DEFAULT_REQUEST_MEMORY_LARGER_THAN_DEFAULT_LIMIT_MEMORY);
        }

        limitRange.setMinCpu(request.getMinCpu());
        limitRange.setMinMemory(request.getMinMemory());

        limitRange.setDefaultRequestCpu(request.getDefaultRequestCpu());
        limitRange.setDefaultRequestMemory(request.getDefaultRequestMemory());

        limitRange.setDefaultLimitCpu(request.getDefaultLimitCpu());
        limitRange.setDefaultLimitMemory(request.getDefaultLimitMemory());

        SysLimitRange res = limitRangeRepository.save(limitRange);

        return LimitRangeDto.from(res);
    }
}
