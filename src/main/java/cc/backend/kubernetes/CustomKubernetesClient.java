package cc.backend.kubernetes;

import cc.backend.EnnProperties;
import cc.backend.audit.request.EnnContext;
import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.lib.retcode.CcException;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.Status;
import io.fabric8.kubernetes.api.model.StorageClass;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import io.fabric8.kubernetes.api.model.extensions.StatefulSetSpec;
import io.fabric8.kubernetes.client.KubernetesClientException;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import static cc.backend.common.utils.OkHttpClientUtils.JSON;
import static cc.backend.common.utils.OkHttpClientUtils.getUnsafeOkHttpClient;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/7/12.
 */
@Component
@RefreshScope
public class CustomKubernetesClient {
    private static final Logger logger = LoggerFactory.getLogger(CustomKubernetesClient.class);

    @Inject
    private EnnProperties properties;

    private Map<String, String> tokenMap = new HashMap<>();
    private Map<String, String> kubernetesMasterUrlMap = new HashMap<>();

    @Inject
    private KubernetesClientManager clientManager;

    @PostConstruct
    private void init() {
        properties.getClusters().forEach((name, cluster) -> {
            EnnProperties.Cluster.Kubernetes kubernetesProperties = cluster.getKubernetes();
            String token;
            if (!kubernetesProperties.isLoginWithToken()) {
                String str = kubernetesProperties.getAdmin().getUsername() + ":" + kubernetesProperties.getAdmin().getPassword();
                token = "Basic " + Base64.getEncoder().encodeToString(str.getBytes());
            } else {
                token = "Bearer " + kubernetesProperties.getToken();
            }

            tokenMap.put(name, token);
            kubernetesMasterUrlMap.put(name, kubernetesProperties.getMasterUrl());
        });

        /*
         * if null will return default cluster info
         */
        tokenMap.put(null, tokenMap.get(properties.getDefaultCluster()));
        kubernetesMasterUrlMap.put(null, kubernetesMasterUrlMap.get(properties.getDefaultCluster()));
    }

    public enum HttpMethod {
        POST, GET, DELETE, PUT, PATCH
    }

    public Response execute(String subUrl,
                            HttpMethod requestMethod,
                            RequestBody requestBody) {
        String clusterName = EnnContext.getClusterName();
        Request request = new Request.Builder()
                .header("Authorization", tokenMap.get(clusterName))
                .url(kubernetesMasterUrlMap.get(clusterName) + subUrl)
                .method(requestMethod.name(), requestBody)
                .build();
        OkHttpClient httpClient = getUnsafeOkHttpClient();

        try {
            Response response = httpClient.newCall(request).execute();
            logger.info("response code:" + response.code() + ",response msg:" + response.message());
            return response;
        } catch (IOException e) {
            e.printStackTrace();
            throw new CcException(BackendReturnCodeNameConstants.KUBERNETES_SERVER_ERROR,
                    ImmutableMap.of("msg", e.getMessage()));
        }
    }

    /**
     * in k8s 1.9, statefulset spec can only update 'replicas', 'template' and 'updateStrategy'
     */
    public StatefulSet updateStatefulSet(String namespaceName, String statefulSetName, StatefulSet statefulSet) {
        String subUrl = "/apis/apps/v1beta1/namespaces/" + namespaceName + "/statefulsets/" + statefulSetName;
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        mapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, true);
        mapper.configure(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS, false);
        String json;

        StatefulSet oldStatefulSet = clientManager.getClient()
                .apps()
                .statefulSets()
                .inNamespace(namespaceName)
                .withName(statefulSetName)
                .get();
        oldStatefulSet.setMetadata(statefulSet.getMetadata());
        StatefulSetSpec oldSpec = oldStatefulSet.getSpec();
        oldSpec.setTemplate(statefulSet.getSpec().getTemplate());
        oldSpec.setReplicas(statefulSet.getSpec().getReplicas());
        oldSpec.setUpdateStrategy(statefulSet.getSpec().getUpdateStrategy());

        try {
            json = mapper.writeValueAsString(oldStatefulSet);
        } catch (JsonProcessingException e) {
            throw new CcException(BackendReturnCodeNameConstants.CONVERT_STATEFULSET_TO_JSON_ERROR,
                    ImmutableMap.of("msg", e.getMessage()));
        }

        logger.debug(json);
        RequestBody body = RequestBody.create(JSON, json);

        Response response = execute(subUrl, HttpMethod.PUT, body);

        try {
            if (response.isSuccessful()) {
                ObjectMapper readMapper = new ObjectMapper();
                readMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

                statefulSet = readMapper.readValue(response.body().string(), StatefulSet.class);

            } else {
                throw new KubernetesClientException(response.message() + ":" + response.body().string(), response.code(), new Status());
            }
        } catch (IOException e) {
            throw new CcException(BackendReturnCodeNameConstants.KUBERNETES_SERVER_ERROR,
                    ImmutableMap.of("msg", e.getMessage()));
        }

        if (response.body() != null) {
            response.close();
        }
        return statefulSet;
    }

    public boolean deleteStatefulSet(String namespaceName, String statefulSetName) {
        String subUrl = "/apis/apps/v1beta1/namespaces/" + namespaceName + "/statefulsets/" + statefulSetName;
        String clusterName = EnnContext.getClusterName();
        Request request = new Request.Builder()
                .header("Authorization", tokenMap.get(clusterName))
                .url(kubernetesMasterUrlMap.get(clusterName) + subUrl)
                .delete()
                .build();

        OkHttpClient httpClient = getUnsafeOkHttpClient();

        try {
            Response response = httpClient.newCall(request).execute();
            ResponseBody responseBody = response.body();
            if (!response.isSuccessful()) {
                logger.error("delete statefulset failed - {}",
                        responseBody == null ? response.message() : responseBody.toString());
            }

            if (responseBody != null) {
                response.close();
            }

            return response.isSuccessful();
        } catch (IOException e) {
            e.printStackTrace();
            throw new CcException(BackendReturnCodeNameConstants.KUBERNETES_SERVER_ERROR,
                    ImmutableMap.of("msg", e.getMessage()));
        }
    }

    public boolean createStorageClass(StorageClass storageClass) {
        storageClass.setApiVersion("storage.k8s.io/v1beta1"); //TODO change version for different k8s version
        String subUrl = "/apis/storage.k8s.io/v1beta1/storageclasses";
        ObjectMapper mapper = new ObjectMapper();
        String json;
        try {
            json = mapper.writeValueAsString(storageClass);
        } catch (JsonProcessingException e) {
            throw new CcException(BackendReturnCodeNameConstants.CONVERT_STORAGECLASS_TO_JSON_ERROR,
                    ImmutableMap.of("msg", e.getMessage()));
        }

        RequestBody body = RequestBody.create(JSON, json);

        String clusterName = EnnContext.getClusterName();
        Request request = new Request.Builder()
                .header("Authorization", tokenMap.get(clusterName))
                .url(kubernetesMasterUrlMap.get(clusterName) + subUrl)
                .method("POST", body)
                .build();

        OkHttpClient httpClient = getUnsafeOkHttpClient();

        try {
            Response response = httpClient.newCall(request).execute();
            ResponseBody responseBody = response.body();
            if (!response.isSuccessful()) {
                logger.error("create storage class failed - {}",
                        responseBody == null ? response.message() : responseBody.string());
            }

            if (responseBody != null) {
                response.close();
            }

            return response.isSuccessful();
        } catch (IOException e) {
            e.printStackTrace();
            throw new CcException(BackendReturnCodeNameConstants.KUBERNETES_SERVER_ERROR,
                    ImmutableMap.of("msg", e.getMessage()));
        }
    }

    public boolean deleteStorageClass(String storageClassName) {
        String subUrl = "/apis/storage.k8s.io/v1beta1/storageclasses/" + storageClassName;
        String clusterName = EnnContext.getClusterName();
        Request request = new Request.Builder()
                .header("Authorization", tokenMap.get(clusterName))
                .url(kubernetesMasterUrlMap.get(clusterName) + subUrl)
                .delete()
                .build();

        OkHttpClient httpClient = getUnsafeOkHttpClient();

        try {
            Response response = httpClient.newCall(request).execute();
            ResponseBody responseBody = response.body();
            if (!response.isSuccessful()) {
                logger.error("delete storage class failed - {}",
                        responseBody == null ? response.message() : responseBody.string());
            }

            if (responseBody != null) {
                response.close();
            }

            return response.isSuccessful();
        } catch (IOException e) {
            e.printStackTrace();
            throw new CcException(BackendReturnCodeNameConstants.KUBERNETES_SERVER_ERROR,
                    ImmutableMap.of("msg", e.getMessage()));
        }
    }
}