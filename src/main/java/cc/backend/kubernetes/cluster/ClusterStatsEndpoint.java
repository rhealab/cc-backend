package cc.backend.kubernetes.cluster;

import cc.backend.common.Metrics;
import cc.backend.common.MetricsType;
import cc.backend.kubernetes.cluster.service.ClusterStatsManagement;
import cc.backend.kubernetes.namespaces.dto.MetricsDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author yanzhixiang on 17-7-28.
 */
@Component
@Path("kubernetes/cluster")
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "Cluster", description = "Info about cluster statistics.", produces = "application/json")
public class ClusterStatsEndpoint {

    private final ClusterStatsManagement management;

    @Inject
    public ClusterStatsEndpoint(ClusterStatsManagement management) {
        this.management = management;
    }

    @GET
    @Path("stats/cpu")
    @ApiOperation(value = "Get cluster cpu current usage info.", response = MetricsDto.class)
    public Response getCpuStats() {
        List<Metrics> metrics = management.getComputeMetrics(MetricsType.CPU);
        double cpuTotal = management.getClusterTotalCpu();
        MetricsDto dto = MetricsDto.from(metrics, cpuTotal, MetricsType.CPU);
        return Response.ok(dto).build();
    }

    @GET
    @Path("stats/memory")
    @ApiOperation(value = "Get cluster memory current usage info.", response = MetricsDto.class)
    public Response getMemoryStats() {
        List<Metrics> metrics = management.getComputeMetrics(MetricsType.MEMORY);
        long memoryTotal = management.getClusterTotalMemory();
        MetricsDto dto = MetricsDto.from(metrics, memoryTotal, MetricsType.MEMORY);
        return Response.ok(dto).build();
    }

    @GET
    @Path("stats/compute")
    @ApiOperation(value = "Get cluster compute resource current usage info.",
            responseContainer = "List", response = MetricsDto.class)
    public Response getComputeStats() {
        return Response.ok(management.getComputeStats()).build();
    }

    @GET
    @Path("stats/resource/compute")
    @ApiOperation(value = "Get cluster compute resource current usage info include disk usage.",
            responseContainer = "List", response = MetricsDto.class)
    public Response getResourceComputeStats() {
        return Response.ok(management.getResourceComputeStats()).build();
    }
}
