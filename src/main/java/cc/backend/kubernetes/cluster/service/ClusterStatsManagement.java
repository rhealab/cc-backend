package cc.backend.kubernetes.cluster.service;

import cc.backend.EnnProperties;
import cc.backend.common.Metrics;
import cc.backend.common.MetricsType;
import cc.backend.common.OpenTsdbClient;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.namespaces.dto.MetricsDto;
import cc.backend.kubernetes.namespaces.dto.MetricsNodeDto;
import cc.backend.kubernetes.utils.UnitUtils;
import com.google.common.collect.ImmutableList;
import io.fabric8.kubernetes.api.model.Node;
import io.fabric8.kubernetes.api.model.Quantity;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static cc.backend.kubernetes.Constants.K_CPU;
import static cc.backend.kubernetes.Constants.K_MEMORY;

/**
 * @author xzy on 17-7-28.
 */
@Component
public class ClusterStatsManagement {

    private final OpenTsdbClient openTsdbClient;

    private final KubernetesClientManager clientManager;

    private final EnnProperties properties;

    @Inject
    public ClusterStatsManagement(OpenTsdbClient openTsdbClient,
                                  KubernetesClientManager clientManager,
                                  EnnProperties properties) {
        this.openTsdbClient = openTsdbClient;
        this.clientManager = clientManager;
        this.properties = properties;
    }

    public List<Metrics> getComputeMetrics(MetricsType type) {
        return openTsdbClient.getClusterMetrics(type, properties.getCurrentOpentsdb().getClusterName());
    }

    public double getClusterTotalCpu() {
        List<Node> nodes = clientManager.getClient()
                .nodes()
                .list()
                .getItems();

        return Optional.ofNullable(nodes)
                .orElseGet(ArrayList::new)
                .stream()
                .map(node -> node.getStatus().getAllocatable())
                .mapToDouble(quantity -> UnitUtils.parseK8sCpuQuantity(quantity.get(K_CPU)))
                .sum();
    }

    public long getClusterTotalMemory() {
        List<Node> nodes = clientManager.getClient()
                .nodes()
                .list()
                .getItems();
        return Optional.ofNullable(nodes)
                .orElseGet(ArrayList::new)
                .stream()
                .map(node -> node.getStatus().getAllocatable())
                .mapToLong(quantity -> UnitUtils.parseK8sMemoryQuantity(quantity.get(K_MEMORY)))
                .sum();
    }

    public List<MetricsDto> getComputeStats() {
        MetricsDto cpuMetricsDto;
        MetricsDto memoryMetricsDto;

        List<Node> nodes = clientManager.getClient()
                .nodes()
                .list()
                .getItems();

        if (nodes != null) {
            double totalCpu = 0;
            long totalMemory = 0;
            for (Node node : nodes) {
                Map<String, Quantity> quantityMap = node.getStatus().getAllocatable();
                totalCpu += UnitUtils.parseK8sCpuQuantity(quantityMap.get(K_CPU));
                totalMemory += UnitUtils.parseK8sMemoryQuantity(quantityMap.get(K_MEMORY));
            }

            List<Metrics> cpuMetrics = openTsdbClient.getClusterCpuMetrics(properties.getCurrentOpentsdb().getClusterName());
            List<Metrics> memoryMetrics = openTsdbClient.getClusterMemoryMetrics(properties.getCurrentOpentsdb().getClusterName());

            cpuMetricsDto = MetricsDto.from(cpuMetrics, totalCpu, MetricsType.CPU);
            memoryMetricsDto = MetricsDto.from(memoryMetrics, totalMemory, MetricsType.MEMORY);
        } else {
            cpuMetricsDto = MetricsDto.newErrorInstance(MetricsType.CPU);
            memoryMetricsDto = MetricsDto.newErrorInstance(MetricsType.MEMORY);
        }

        return ImmutableList.of(cpuMetricsDto, memoryMetricsDto);
    }

    public List<MetricsNodeDto> getResourceComputeStats() {
        List<MetricsNodeDto> dtos = new ArrayList<>();

        List<Node> nodes = clientManager.getClient()
                .nodes()
                .list()
                .getItems();

        if (nodes != null) {
            String clusterName = properties.getCurrentOpentsdb().getClusterName();
            List<Metrics> cpuMetrics = openTsdbClient.getNodeMetrics(MetricsType.CPU,
                    OpenTsdbClient.Scope.Cluster, clusterName);
            List<Metrics> memoryMetrics = openTsdbClient.getNodeMetrics(MetricsType.MEMORY_WORKING_SET,
                    OpenTsdbClient.Scope.Cluster, clusterName);
            List<Metrics> diskUsage = openTsdbClient.getNodeMetrics(MetricsType.DISK_USAGE,
                    OpenTsdbClient.Scope.Cluster, clusterName);
            List<Metrics> diskCapacity = openTsdbClient.getNodeMetrics(MetricsType.DISK_CAPACITY,
                    OpenTsdbClient.Scope.Cluster, clusterName);

            dtos = nodes.stream()
                    .map(node -> getMetricsNodeDto(cpuMetrics, memoryMetrics, diskUsage, diskCapacity, node))
                    .collect(Collectors.toList());
        }

        return dtos;
    }

    private MetricsNodeDto getMetricsNodeDto(List<Metrics> cpuMetrics,
                                             List<Metrics> memoryMetrics,
                                             List<Metrics> diskUsage,
                                             List<Metrics> diskCapacity,
                                             Node node) {
        String nodeName = node.getMetadata().getName();
        MetricsNodeDto dto = new MetricsNodeDto();
        dto.setName(nodeName);

        Map<String, Quantity> quantityMap = node.getStatus().getAllocatable();
        double totalCpu = UnitUtils.parseK8sCpuQuantity(quantityMap.get(K_CPU));
        long totalMemory = UnitUtils.parseK8sMemoryQuantity(quantityMap.get(K_MEMORY));

        double totalDisk = Optional.ofNullable(diskCapacity)
                .orElseGet(ArrayList::new)
                .stream()
                .filter(dc -> dc.getTags().get("host").equals(nodeName))
                .filter(dc -> dc.getDps().size() != 0)
                .map(dc -> Double.parseDouble(dc.getDps().values().toArray()[dc.getDps().size() - 1].toString()))
                .findAny()
                .orElse(0D);

        MetricsDto cpu = MetricsDto.from(cpuMetrics, totalCpu, nodeName, MetricsType.CPU);
        MetricsDto memory = MetricsDto.from(memoryMetrics, totalMemory, nodeName, MetricsType.MEMORY_WORKING_SET);
        MetricsDto disk = MetricsDto.from(diskUsage, totalDisk, nodeName, MetricsType.DISK_USAGE);

        dto.setCpu(cpu);
        dto.setMemory(memory);
        dto.setDisk(disk);
        return dto;
    }
}
