package cc.backend.kubernetes.apps.services;

import cc.backend.EnnProperties;
import cc.backend.audit.operation.OperationAuditMessage;
import cc.backend.audit.operation.OperationAuditMessageProducer;
import cc.backend.audit.operation.OperationType;
import cc.backend.audit.operation.ResourceType;
import cc.backend.common.DependencyCheckerClient;
import cc.backend.common.utils.DateUtils;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.apps.data.*;
import cc.backend.kubernetes.apps.dto.*;
import cc.backend.kubernetes.apps.message.AppStatusMessage;
import cc.backend.kubernetes.apps.message.AppStatusMessageProducer;
import cc.backend.kubernetes.apps.validator.AppValidator;
import cc.backend.kubernetes.service.data.ServiceExtrasRepository;
import cc.backend.kubernetes.service.dto.ServiceDto;
import cc.backend.kubernetes.storage.HostPathStorageManager;
import cc.backend.kubernetes.storage.StorageManagement;
import cc.backend.kubernetes.storage.StorageValidator;
import cc.backend.kubernetes.storage.StorageWorkloadRepository;
import cc.backend.kubernetes.storage.domain.HostPathUsage.MountInfo;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageWorkload;
import cc.backend.kubernetes.storage.domain.WorkloadType;
import cc.backend.kubernetes.utils.CommonUtils;
import cc.backend.kubernetes.workload.deployment.data.DeploymentExtrasRepository;
import cc.backend.kubernetes.workload.deployment.dto.DeploymentDto;
import cc.backend.kubernetes.workload.pod.dto.VolumeDto;
import cc.backend.kubernetes.workload.pod.services.PodsManagement;
import cc.backend.kubernetes.workload.statefulset.StatefulSetManagement;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.*;
import io.fabric8.kubernetes.api.model.extensions.*;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static cc.backend.kubernetes.Constants.APP_LABEL;
import static cc.backend.kubernetes.storage.domain.StorageType.HostPath;
import static java.util.stream.Collectors.toList;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/2/22.
 * @author yanzhixiang modified on 2017-6-7
 */
@Component
public class AppManagement {
    private static final Logger logger = LoggerFactory.getLogger(AppManagement.class);

    @Inject
    private AppRepository appRepository;

    @Inject
    private KubernetesClientManager kubernetesClientManager;

    @Inject
    private StorageManagement storageManagement;

    @Inject
    private OperationAuditMessageProducer operationAuditMessageProducer;

    @Inject
    private PodsManagement podsManagement;

    @Inject
    private DeploymentExtrasRepository deploymentExtrasRepository;

    @Inject
    private ServiceExtrasRepository serviceExtrasRepository;

    @Inject
    private StorageWorkloadRepository storageWorkloadRepository;

    @Inject
    private StatefulSetManagement statefulSetManagement;


    @Inject
    private StorageValidator storageValidator;

    @Inject
    private HostPathStorageManager hostPathStorageManager;

    @Inject
    private AppValidator validator;

    @Inject
    private EnnProperties properties;

    @Inject
    private DependencyCheckerClient dependencyCheckerClient;

    @Inject
    private AppStatusMessageProducer appStatusMessageProducer;

    public List<App> getAllApps(String namespaceName) {
        return appRepository.findByNamespace(namespaceName);
    }

    public App getApp(String namespaceName, String appName) {
        return validator.validateAppShouldExists(namespaceName, appName);
    }

    public List<Storage> getAppStorageList(String namespaceName, String appName) {
        validator.validateAppShouldExists(namespaceName, appName);
        return storageManagement.getStorageListByApp(namespaceName, appName);
    }

    public App createApp(App app, String namespaceName) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        validator.validateAppShouldNotExists(namespaceName, app.getName());

        app.setNamespace(namespaceName);
        app.setType(StringUtils.isEmpty(app.getPredefineType()) ? AppType.CUSTOM : AppType.PREDEFINE);
        logger.debug("Create app: {}", app);
        app = appRepository.save(app);

        sendAppAuditMessage(app, OperationType.CREATE, stopwatch);

        AppStatusMessage appStatusMessage = new AppStatusMessage.Builder()
                .setNamespace(namespaceName)
                .setInstanceName(app.getName())
                .setStatus(app.getStatus())
                .build();
        appStatusMessageProducer.send(appStatusMessage);

        return app;
    }

    public void updateAppStatus(String namespace, String appName, AppStatus status, String details) {
        App app = validator.validateAppShouldExists(namespace, appName);
        app.setStatus(status);
        app.setDetails(details);
        appRepository.save(app);

        AppStatusMessage appStatusMessage = new AppStatusMessage.Builder()
                .setNamespace(namespace)
                .setInstanceName(app.getName())
                .setStatus(status)
                .build();
        appStatusMessageProducer.send(appStatusMessage);
    }

    public Map deletePredefineApp(String namespaceName, App app) {
        AppStatusMessage appStatusMessage = new AppStatusMessage.Builder()
                .setNamespace(namespaceName)
                .setInstanceName(app.getName())
                .setStatus(AppStatus.DELETING)
                .build();

        appStatusMessageProducer.send(appStatusMessage);

        List<Storage> storageList = storageManagement.getStorageListByApp(namespaceName, app.getName());
        Map message = deleteStandardApp(namespaceName, app.getName(), app);
        for (Storage storage : storageList) {
            storageManagement.deleteStorage(namespaceName, storage.getId());
        }
        AppStatusMessage statusMessageDeleted = new AppStatusMessage.Builder()
                .setNamespace(namespaceName)
                .setInstanceName(app.getName())
                .setStatus(AppStatus.DELETED)
                .build();
        appStatusMessageProducer.send(statusMessageDeleted);
        return message;
    }

    public Map deleteApp(String namespaceName, String appName) {
        App app = validator.validateAppShouldExists(namespaceName, appName);
        //if it is a predefined app, delete metadata from cc-template
        if (app.getType() == AppType.PREDEFINE) {
            return deletePredefineApp(namespaceName, app);
        } else {
            return deleteStandardApp(namespaceName, appName, app);
        }
    }

    public Map deleteStandardApp(String namespaceName, String appName, App app) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        boolean serviceDeletedSucceed = deleteService(namespaceName, appName);

        List<Deployment> deployments = kubernetesClientManager.getClient()
                .extensions()
                .deployments()
                .inNamespace(namespaceName)
                .withLabel(APP_LABEL, appName)
                .list()
                .getItems();

        boolean deploymentDeletedSucceed = deleteDeployment(namespaceName, appName);

        boolean statefulSetDeletedSucceed = false;
        if (properties.getCurrentCeph().isEnabled()) {
           statefulSetDeletedSucceed = deleteStatefulSet(namespaceName, appName);
        }

        boolean rsDeletedSucceed = deleteReplicaSet(namespaceName, deployments);

        storageWorkloadRepository.deleteByNamespaceNameAndAppName(namespaceName, appName);
        appRepository.delete(app);
        logger.info("Delete app: name={}", appName);

        sendAppAuditMessage(app, OperationType.DELETE, stopwatch);

        return ImmutableMap.builder()
                .put("status", "ok")
                .put("serviceDeleteStatus", serviceDeletedSucceed ? "ok" : "error")
                .put("deploymentDeleteStatus", deploymentDeletedSucceed ? "ok" : "error")
                .put("statefulSetDeleteStatus", statefulSetDeletedSucceed ? "ok" : "error")
                .put("replicaSetDeleteStatus", rsDeletedSucceed ? "ok" : "error")
                .put("storageDeleteList", new HashMap<>(0))
                .build();
    }

    private void sendAppAuditMessage(App app, OperationType operationType, Stopwatch stopwatch) {
        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .elapsed(elapsed)
                .namespaceName(app.getNamespace())
                .operationType(operationType)
                .resourceType(ResourceType.APPLICATION)
                .resourceName(app.getName()).build();
        operationAuditMessageProducer.send(message);
    }

    public void deleteAppByNamespace(String namespaceName) {
        appRepository.deleteByNamespace(namespaceName);
    }

    private boolean deleteService(String namespace, String appName) {
        KubernetesClient client = kubernetesClientManager.getClient();
        List<Service> services = client
                .services()
                .inNamespace(namespace)
                .withLabel(APP_LABEL, appName)
                .list()
                .getItems();
        if (services != null) {
            boolean succeed = client
                    .services()
                    .inNamespace(namespace)
                    .withLabel(APP_LABEL, appName)
                    .delete();

            //delete serviceInfo
            if (succeed) {
                serviceExtrasRepository.deleteByNamespaceAndAppName(namespace, appName);
            }
            return succeed;
        }
        return true;
    }

    private boolean deleteDeployment(String namespaceName, String appName) {
        KubernetesClient client = kubernetesClientManager.getClient();
        List<Deployment> deployments = client
                .extensions()
                .deployments()
                .inNamespace(namespaceName)
                .withLabel(APP_LABEL, appName)
                .list()
                .getItems();
        if (deployments != null) {
            boolean succeed = client
                    .extensions()
                    .deployments()
                    .inNamespace(namespaceName)
                    .withLabel(APP_LABEL, appName)
                    .delete();

            //delete deploymentInfo
            if (succeed) {
                deploymentExtrasRepository.deleteByNamespaceAndAppName(namespaceName, appName);
            }
            return succeed;
        }
        return true;
    }

    private boolean deleteStatefulSet(String namespaceName, String appName) {
        List<StatefulSet> statefulSets = statefulSetManagement.getStatefulSets(namespaceName, appName);
        for (StatefulSet statefulSet : statefulSets) {
            statefulSetManagement.deleteWithAudit(namespaceName, appName, statefulSet.getMetadata().getName());
        }
        return true;
    }

    private boolean deleteReplicaSet(String namespaceName, List<Deployment> deployments) {
        List<ReplicaSet> replicaSets = kubernetesClientManager.getClient()
                .extensions()
                .replicaSets()
                .inNamespace(namespaceName)
                .list()
                .getItems();
        if (replicaSets == null || deployments == null) {
            return true;
        }
        for (ReplicaSet replicaSet : replicaSets) {
            PodTemplateSpec rsTemplate = replicaSet.getSpec().getTemplate();
            String rsName = replicaSet.getMetadata().getName();
            deployments.stream()
                    .map(deployment -> deployment.getSpec().getTemplate())
                    .filter(templateSpec -> CommonUtils.equalIgnoreHash(rsTemplate, templateSpec))
                    .forEach(templateSpec -> kubernetesClientManager.getClient()
                            .extensions()
                            .replicaSets()
                            .inNamespace(namespaceName)
                            .withName(rsName)
                            .delete());
        }
        return true;
    }

    public AppResourcesDetails getResourcesDetails(String namespaceName, String appName) {
        App app = validator.validateAppShouldExists(namespaceName, appName);
        logger.debug("app: {}", app);

        AppResourcesDetails resources = new AppResourcesDetails();
        resources.setApp(app);

        KubernetesClient client = kubernetesClientManager.getClient();
        List<Service> services = client.services().
                inNamespace(namespaceName).withLabel(APP_LABEL, appName).list().getItems();
        if (services == null) {
            services = new ArrayList<>();
        }
        logger.debug("serviceList: {}", services);

        //add createdBy field to serviceDto
        resources.setServices(ServiceDto.from(services, properties.getCurrentKubernetes().getVipHost()));

        List<Deployment> deployments = client.extensions().deployments().inNamespace(namespaceName)
                .withLabel(APP_LABEL, appName).list().getItems();
        if (deployments == null) {
            deployments = new ArrayList<>();
        }
        logger.debug("deploymentList: {}", deployments);

        Map<String, List<Pod>> podsMap = podsManagement.getDeploymentPodsMap(namespaceName, deployments);
        resources.setDeployments(DeploymentDto.from(deployments, podsMap));

        return resources;
    }

    public AppResourcesTopologicalDeprecated getResourcesTopologicalDeprecated(String namespaceName, String appName) {
        App app = validator.validateAppShouldExists(namespaceName, appName);
        AppResourcesTopologicalDeprecated topological = new AppResourcesTopologicalDeprecated();
        topological.setApp(app);

        KubernetesClient client = kubernetesClientManager.getClient();

        ServiceList serviceList = client.services().
                inNamespace(namespaceName).withLabel(APP_LABEL, appName).list();
        topological.setServices(ServiceTopologicalDeprecated.from(serviceList.getItems()));

        DeploymentList deploymentList = client.extensions().
                deployments().inNamespace(namespaceName).withLabel(APP_LABEL, appName).list();

        StatefulSetList statefulSetList = client
                .apps()
                .statefulSets()
                .inNamespace(namespaceName)
                .withLabel(APP_LABEL, appName)
                .list();

        List<DeploymentTopologicalDeprecated> deploymentTopologicalList = composeDeploymentTopologicalList(deploymentList.getItems());
        List<DeploymentTopologicalDeprecated> statefulSetTopologicalList = composeStatefulSetTopologicalList(statefulSetList.getItems());

        deploymentTopologicalList.addAll(statefulSetTopologicalList);
        topological.setDeployments(deploymentTopologicalList);

        List<Storage> storages = storageManagement.getStorageListByApp(namespaceName, appName);
        topological.setStorages(composeStorageTopologicalList(storages, getPodDeploymentMap(topological)));

        addStorageToPod(topological);

        List<PodTopologicalDeprecated> pods = new ArrayList<>();
        for (DeploymentTopologicalDeprecated deployment : topological.getDeployments()) {
            pods.addAll(deployment.getPods());
        }
        topological.setPods(pods);

        return topological;
    }

    private void addStorageToPod(AppResourcesTopologicalDeprecated topological) {
        List<StorageTopologicalDeprecated> storages = topological.getStorages();
        List<DeploymentTopologicalDeprecated> deployments = topological.getDeployments();
        if (storages == null || deployments == null) {
            return;
        }

        Map<String, PodTopologicalDeprecated> podMap = new HashMap<>(100);
        Map<String, DeploymentTopologicalDeprecated> deploymentMap = new HashMap<>(100);
        for (DeploymentTopologicalDeprecated deployment : deployments) {
            List<PodTopologicalDeprecated> pods = deployment.getPods();
            if (pods == null) {
                continue;
            }

            for (PodTopologicalDeprecated pod : pods) {
                podMap.put(pod.getName(), pod);
            }
            deploymentMap.put(deployment.getName(), deployment);
        }

        for (StorageTopologicalDeprecated storage : storages) {
            List<HostpathTopological> hostpathInfo = storage.getHostpathInfo();
            if (hostpathInfo != null) {
                for (HostpathTopological hostpath : hostpathInfo) {
                    PodTopologicalDeprecated pod = podMap.get(hostpath.getPodName());
                    if (pod == null) {
                        continue;
                    }
                    List<String> storageNames = pod.getStorageNames();
                    if (storageNames == null) {
                        storageNames = new ArrayList<>();
                        pod.setStorageNames(storageNames);
                    }
                    storageNames.add(storage.getStorageName());
                }
            }

            List<StorageWorkload> deploymentList = storage.getRelatedDeploymentList();
            if (deploymentList != null) {
                for (StorageWorkload workload : deploymentList) {
                    DeploymentTopologicalDeprecated deployment = deploymentMap.get(workload.getWorkloadName());
                    if (deployment == null) {
                        continue;
                    }

                    List<PodTopologicalDeprecated> pods = deployment.getPods();
                    if (pods == null) {
                        continue;
                    }

                    for (PodTopologicalDeprecated pod : pods) {
                        List<String> storageNames = pod.getStorageNames();
                        if (storageNames == null) {
                            storageNames = new ArrayList<>();
                            pod.setStorageNames(storageNames);
                        }
                        storageNames.add(storage.getStorageName());
                    }
                }
            }

            if (storage.getRelatedPodName() != null) {
                PodTopologicalDeprecated pod = podMap.get(storage.getRelatedPodName());
                if (pod != null) {
                    List<String> storageNames = pod.getStorageNames();
                    if (storageNames == null) {
                        storageNames = new ArrayList<>();
                        pod.setStorageNames(storageNames);
                    }
                    storageNames.add(storage.getStorageName());
                }
            }
        }
    }

    private Map<String, String> getPodDeploymentMap(AppResourcesTopologicalDeprecated topological) {
        Map<String, String> map = new HashMap<>(100);
        if (topological != null && topological.getDeployments() != null) {
            List<DeploymentTopologicalDeprecated> deployments = topological.getDeployments();
            for (DeploymentTopologicalDeprecated deployment : deployments) {
                List<PodTopologicalDeprecated> pods = deployment.getPods();
                if (pods != null) {
                    for (PodTopologicalDeprecated pod : pods) {
                        map.put(pod.getName(), deployment.getName());
                    }
                }
            }
        }

        return map;
    }

    private List<DeploymentTopologicalDeprecated> composeDeploymentTopologicalList(List<Deployment> deployments) {
        List<DeploymentTopologicalDeprecated> summaryList = new ArrayList<>();

        if (deployments != null) {
            for (Deployment deployment : deployments) {
                summaryList.add(composeDeploymentTopological(deployment));
            }
        }

        return summaryList;
    }

    private List<DeploymentTopologicalDeprecated> composeStatefulSetTopologicalList(List<StatefulSet> statefulSetList) {
        List<DeploymentTopologicalDeprecated> summaryList = new ArrayList<>();

        if (statefulSetList != null) {
            for (StatefulSet statefulSet : statefulSetList) {
                summaryList.add(composeStatefulSetTopological(statefulSet));
            }
        }

        return summaryList;
    }

    private DeploymentTopologicalDeprecated composeStatefulSetTopological(StatefulSet statefulSet) {
        DeploymentTopologicalDeprecated dto = new DeploymentTopologicalDeprecated();
        dto.setType(DeploymentTopologicalDeprecated.Type.StatefulSet);

        ObjectMeta metadata = statefulSet.getMetadata();
        StatefulSetSpec spec = statefulSet.getSpec();

        String statefulSetName = metadata.getName();
        String namespaceName = metadata.getNamespace();

        dto.setName(statefulSetName);
        dto.setUid(metadata.getUid());
        dto.setNamespace(namespaceName);

        dto.setReplicas(spec.getReplicas() == null ? 0 : spec.getReplicas());
        dto.setSelector(spec.getSelector());
        dto.setLabels(metadata.getLabels());

        List<String> images = new ArrayList<>();
        List<Container> containers = spec.getTemplate().getSpec().getContainers();
        for (Container container : containers) {
            images.add(container.getImage());
        }
        dto.setImages(images);

        dto.setCreatedOn(DateUtils.parseK8sDate(metadata.getCreationTimestamp()));
        dto.setAge(DateUtils.getAge(dto.getCreatedOn()));

        List<StorageWorkload> storageWorkloadList = storageWorkloadRepository.
                findByNamespaceNameAndWorkloadTypeAndWorkloadName(namespaceName, WorkloadType.StatefulSet, statefulSetName);
        List<String> storageNames = new ArrayList<>();
        for (StorageWorkload storageWorkload : storageWorkloadList) {
            storageNames.add(storageWorkload.getStorageName());
        }
        dto.setStorageNames(storageNames);

        List<PodTopologicalDeprecated> podSummaryList = composePodTopological(namespaceName, spec.getSelector().getMatchLabels());
        dto.setPods(podSummaryList);

        return dto;
    }

    private DeploymentTopologicalDeprecated composeDeploymentTopological(Deployment deployment) {
        DeploymentTopologicalDeprecated dto = new DeploymentTopologicalDeprecated();
        dto.setType(DeploymentTopologicalDeprecated.Type.Deployment);
        ObjectMeta metadata = deployment.getMetadata();
        DeploymentSpec spec = deployment.getSpec();

        String deploymentName = metadata.getName();
        String namespaceName = metadata.getNamespace();

        dto.setName(deploymentName);
        dto.setUid(metadata.getUid());
        dto.setNamespace(namespaceName);

        dto.setReplicas(spec.getReplicas() == null ? 0 : spec.getReplicas());
        dto.setSelector(spec.getSelector());
        dto.setLabels(metadata.getLabels());

        if (spec.getStrategy() != null) {
            dto.setStrategyType(spec.getStrategy().getType());
            dto.setRollingUpdateStrategy(spec.getStrategy().getRollingUpdate());
        }

        List<String> images = new ArrayList<>();
        List<Container> containers = spec.getTemplate().getSpec().getContainers();
        for (Container container : containers) {
            images.add(container.getImage());
        }
        dto.setImages(images);

        dto.setCreatedOn(DateUtils.parseK8sDate(metadata.getCreationTimestamp()));
        dto.setAge(DateUtils.getAge(dto.getCreatedOn()));

        List<StorageWorkload> storageWorkloadList = storageWorkloadRepository.
                findByNamespaceNameAndWorkloadTypeAndWorkloadName(namespaceName, WorkloadType.Deployment, deploymentName);
        List<String> storageNames = new ArrayList<>();
        if (storageWorkloadList != null) {
            for (StorageWorkload storageWorkload : storageWorkloadList) {
                storageNames.add(storageWorkload.getStorageName());
            }
        }
        dto.setStorageNames(storageNames);

        List<PodTopologicalDeprecated> podSummaryList = composePodTopological(namespaceName, spec.getSelector().getMatchLabels());
        dto.setPods(podSummaryList);

        return dto;
    }

    private List<PodTopologicalDeprecated> composePodTopological(String namespaceName, Map<String, String> labels) {
        if (labels == null) {
            return new ArrayList<>();
        }
        KubernetesClient client = kubernetesClientManager.getClient();
        List<Pod> items = client.pods().inNamespace(namespaceName).withLabels(labels).list().getItems();
        return PodTopologicalDeprecated.from(items);
    }

    public List<StorageTopologicalDeprecated> composeStorageTopologicalList(List<Storage> storageList, Map<String, String> podDeploymentMap) {
        if (storageList != null) {
            return storageList.stream().map(storage -> composeStorageTopological(storage, podDeploymentMap)).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    public StorageTopologicalDeprecated composeStorageTopological(Storage storage, Map<String, String> podDeploymentMap) {
        List<StorageWorkload> storageWorkloadList = storageWorkloadRepository.findByStorageId(storage.getId());

        StorageTopologicalDeprecated.Builder builder = new StorageTopologicalDeprecated.Builder()
                .id(storage.getId())
                .namespaceName(storage.getNamespaceName())
                .storageName(storage.getStorageName())
                .storageType(storage.getStorageType())
                .accessMode(storage.getAccessMode())
                .amountBytes(storage.getAmountBytes())
                .unshared(storage.isUnshared())
                .persisted(storage.isPersisted())
                .readOnly(storage.isReadOnly())
                .relatedWorkloadList(storageWorkloadList)
                .status(storage.getStatus())
                .createdBy(storage.getCreatedBy())
                .createdOn(storage.getCreatedOn())
                .modifiedBy(storage.getLastUpdatedBy())
                .modifiedOn(storage.getLastUpdatedOn())
                .pvcName(storage.getPvcName());
        if (storage.getStorageType() == HostPath) {
            List<MountInfo> mountInfoList = hostPathStorageManager.getHostPathMountInfo(storage.getPvName());
            builder.hostpathInfo(HostpathTopological.from(mountInfoList, podDeploymentMap));
        } else {
            builder.usedAmountBytes(storageValidator.getStorageUsedBytes(storage));
        }

        return builder.build();
    }

    public AppResourcesSummary getResourcesSummary(String namespace, String appName) {
        App app = validator.validateAppShouldExists(namespace, appName);
        logger.debug("app: {}", app);

        AppResourcesSummary resources = new AppResourcesSummary();
        resources.setApp(app);

        KubernetesClient client = kubernetesClientManager.getClient();

        ServiceList serviceList = client
                .services()
                .inNamespace(namespace)
                .withLabel(APP_LABEL, appName)
                .list();
        logger.debug("serviceList: {}", serviceList);
        resources.setServices(mapServiceList(serviceList));

        DeploymentList deploymentList = client
                .extensions()
                .deployments()
                .inNamespace(namespace)
                .withLabel(APP_LABEL, appName)
                .list();
        logger.debug("deploymentList: {}", deploymentList);
        resources.setDeployments(mapDeploymentListWithPods(deploymentList, client, namespace));

        return resources;
    }

    private List<Map> mapServiceList(ServiceList serviceList) {
        List<Map> services = new ArrayList<>();

        if (serviceList.getItems() != null) {
            for (Service service : serviceList.getItems()) {
                Map<String, Object> props = new HashMap<>(3);
                props.put("name", service.getMetadata().getName());
                props.put("selector", service.getSpec().getSelector());
                props.put("labels", service.getMetadata().getLabels());
                services.add(props);
            }
        }
        return services;
    }

    private List<Map> mapDeploymentListWithPods(DeploymentList deploymentList, KubernetesClient client, String namespace) {
        List<Map> deployments = new ArrayList<>();

        if (deploymentList.getItems() != null) {
            for (Deployment deployment : deploymentList.getItems()) {
                Map<String, Object> props = new HashMap<>(3);
                props.put("name", deployment.getMetadata().getName());
                props.put("labels", deployment.getMetadata().getLabels());

                Map<String, String> labels = deployment.getSpec().getTemplate().getMetadata().getLabels();
                PodList podList = getPodList(client, namespace, labels);
                props.put("pods", mapPodList(podList));
                deployments.add(props);
            }
        }
        return deployments;
    }

    private PodList getPodList(KubernetesClient client, String namespace, Map<String, String> labels) {
        return client.pods().inNamespace(namespace).withLabels(labels).list();
    }

    private List<Map> mapPodList(PodList podList) {
        List<Map> pods = new ArrayList<>();

        if (podList.getItems() != null) {
            for (Pod pod : podList.getItems()) {
                ObjectMeta metadata = pod.getMetadata();
                PodSpec spec = pod.getSpec();

                Map<String, Object> props = new HashMap<>(5);
                props.put("name", metadata.getName());
                props.put("labels", metadata.getLabels());
                props.put("containers", mapContainerList(spec.getContainers()));
                props.put("volumes", VolumeDto.from(spec.getVolumes()));
                props.put("status", pod.getStatus().getPhase());
                pods.add(props);
            }
        }
        return pods;
    }

    private List<Map> mapContainerList(List<Container> containerList) {
        List<Map> containers = new ArrayList<>();

        if (containerList != null) {
            for (Container container : containerList) {
                Map<String, Object> props = new HashMap<>(2);
                props.put("name", container.getName());
                props.put("image", container.getImage());

                containers.add(props);
            }
        }
        return containers;
    }

    public AppResourcesTopological getResourcesTopological(String namespaceName, String appName) {
        App app = validator.validateAppShouldExists(namespaceName, appName);

        List<Service> services = kubernetesClientManager.getClient()
                .services()
                .inNamespace(namespaceName)
                .withLabel(APP_LABEL, appName)
                .list()
                .getItems();

        List<Deployment> deployments = kubernetesClientManager.getClient()
                .extensions()
                .deployments()
                .inNamespace(namespaceName)
                .withLabel(APP_LABEL, appName)
                .list()
                .getItems();

        List<StatefulSet> statefulSets = kubernetesClientManager.getClient()
                .apps()
                .statefulSets()
                .inNamespace(namespaceName)
                .withLabel(APP_LABEL, appName)
                .list()
                .getItems();

        List<Pod> pods = getDeploymentPods(namespaceName, deployments);
        List<Pod> statefulSetPods = getStatefulSetPods(namespaceName, statefulSets);
        pods.addAll(statefulSetPods);

        List<Storage> storages = storageManagement.getStorageListByApp(namespaceName, appName);

        AppResourcesTopological.Builder builder = new AppResourcesTopological.Builder();
        builder.app(app);
        builder.services(services);
        builder.deployments(deployments);
        builder.statefulSets(statefulSets);
        builder.pods(pods);
        builder.storages(storages);
        builder.hostPathStorageManager(hostPathStorageManager);
        builder.storageValidator(storageValidator);

        return builder.build();
    }

    private List<Pod> getDeploymentPods(String namespaceName, List<Deployment> deployments) {
        if (deployments == null) {
            return new ArrayList<>();
        }

        return deployments.stream()
                .map(deployment -> deployment.getSpec().getSelector())
                .map(selector -> kubernetesClientManager.getClient()
                        .pods()
                        .inNamespace(namespaceName)
                        .withLabelSelector(selector)
                        .list()
                        .getItems())
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private List<Pod> getStatefulSetPods(String namespaceName, List<StatefulSet> statefulSets) {
        if (statefulSets == null) {
            return new ArrayList<>();
        }

        return statefulSets.stream()
                .flatMap(statefulSet -> getStatefulSetPodNames(statefulSet).stream())
                .map(podName -> kubernetesClientManager.getClient()
                        .pods()
                        .inNamespace(namespaceName)
                        .withName(podName)
                        .get())
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private List<String> getStatefulSetPodNames(StatefulSet statefulSet) {
        String name = statefulSet.getMetadata().getName();
        int replicas = Optional.ofNullable(statefulSet.getSpec().getReplicas()).orElse(0);

        return IntStream.range(0, replicas)
                .mapToObj(num -> name + "-" + num)
                .collect(toList());
    }
}
