package cc.backend.kubernetes.apps.services;

import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.apps.dto.AppStatsDto;
import cc.backend.kubernetes.apps.validator.AppValidator;
import cc.backend.kubernetes.storage.HostPathStorageManager;
import cc.backend.kubernetes.storage.StorageManagement;
import cc.backend.kubernetes.storage.StorageUtils;
import cc.backend.kubernetes.storage.StorageWorkloadRepository;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageType;
import cc.backend.kubernetes.storage.domain.StorageWorkload;
import cc.backend.kubernetes.storage.usage.entity.CephImageUsage;
import cc.backend.kubernetes.storage.usage.entity.CephImageUsageRepository;
import cc.backend.kubernetes.storage.usage.entity.CephfsDirectoryUsage;
import cc.backend.kubernetes.storage.usage.entity.CephfsDirectoryUsageRepository;
import cc.backend.kubernetes.utils.UnitUtils;
import io.fabric8.kubernetes.api.model.Container;
import io.fabric8.kubernetes.api.model.Quantity;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.DeploymentSpec;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cc.backend.kubernetes.Constants.*;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/2/24.
 */
@Component
public class AppStatsManagement {
    private static final Logger logger = LoggerFactory.getLogger(AppStatsManagement.class);

    @Inject
    private KubernetesClientManager kubernetesClientManager;

    @Inject
    private CephImageUsageRepository imageUsageRepository;

    @Inject
    private CephfsDirectoryUsageRepository cephfsDirectoryUsageRepository;

    @Inject
    private AppValidator appValidator;

    @Inject
    private HostPathStorageManager hostPathStorageManager;

    @Inject
    private StorageWorkloadRepository storageWorkloadRepository;

    @Inject
    private StorageManagement storageManagement;

    public AppStatsDto getAppStats(String namespaceName, String appName) {
        appValidator.validateAppShouldExists(namespaceName, appName);

        AppStatsDto dto = new AppStatsDto();
        dto.setAppName(appName);

        KubernetesClient client = kubernetesClientManager.getClient();
        List<Deployment> deployments = client.extensions().deployments()
                .inNamespace(namespaceName).withLabel(APP_LABEL, appName).list().getItems();
        if (deployments != null) {

            List<StorageWorkload> storageWorkloadList = storageWorkloadRepository.
                    findByNamespaceNameAndAppName(namespaceName, appName);
            List<Storage> storageList = storageManagement.getStorageListByStorageWorkload(storageWorkloadList);

            Map<String, List<Storage>> storageMap = composeStorageMap(storageWorkloadList, storageList);

            long hostpathUsedBytes = getHostpathUsedBytes(storageList);
            long hostpathTotalBytes = 0;

            for (Deployment deployment : deployments) {
                String deploymentName = deployment.getMetadata().getName();
                DeploymentSpec spec = deployment.getSpec();

                Integer availableReplicas = deployment.getStatus().getAvailableReplicas();
                if (availableReplicas == null) {
                    availableReplicas = 0;
                }

                addComputeResource(dto, spec, availableReplicas);
                hostpathTotalBytes += getHostpathBytes(availableReplicas, storageMap.get(deploymentName));
            }

            dto.setLocalStorageUsedBytes(hostpathUsedBytes);
            dto.setLocalStorageBytes(hostpathTotalBytes);

            composeCephRbdStorage(dto, namespaceName, storageList);
            composeCephFsStorage(dto, namespaceName, storageList);

            return dto;
        }

        return null;
    }

    private long getHostpathUsedBytes(List<Storage> storageList) {
        long hostpathUsedBytes = 0;
        for (Storage storage : storageList) {
            if (storage.getStorageType() == StorageType.HostPath) {
                hostpathUsedBytes += hostPathStorageManager.getHostpathPvUsedBytes(storage.getPvName());
            }
        }
        return hostpathUsedBytes;
    }

    private Map<String, List<Storage>> composeStorageMap(List<StorageWorkload> storageWorkloadList,
                                                         List<Storage> storageList) {
        Map<String, List<Storage>> map = new HashMap<>();

        Map<Long, Storage> storageIdMap = new HashMap<>();
        for (Storage storage : storageList) {
            storageIdMap.put(storage.getId(), storage);
        }

        for (StorageWorkload storageWorkload : storageWorkloadList) {
            List<Storage> list = map.computeIfAbsent(storageWorkload.getWorkloadName(), k -> new ArrayList<>());
            Storage storage = storageIdMap.get(storageWorkload.getStorageId());
            if (storage != null) {
                list.add(storage);
            }
        }

        return map;
    }

    private void addComputeResource(AppStatsDto dto, DeploymentSpec spec, Integer availableReplicas) {
        if (availableReplicas == 0) {
            return;
        }

        List<Container> containers = spec.getTemplate().getSpec().getContainers();

        double tempCpuRequestsSum = 0;
        double tempCpuLimitsSum = 0;
        long tempMemoryRequestsSum = 0;
        long tempMemoryLimitsSum = 0;

        for (Container container : containers) {
            Map<String, Quantity> requests = container.getResources().getRequests();
            Map<String, Quantity> limits = container.getResources().getLimits();

            if (requests != null) {
                tempCpuRequestsSum += UnitUtils.parseK8sCpuQuantity(requests.get(K_CPU));
                tempMemoryRequestsSum += UnitUtils.parseK8sMemoryQuantity(requests.get(K_MEMORY));
            }

            if (limits != null) {
                tempCpuLimitsSum += UnitUtils.parseK8sCpuQuantity(limits.get(K_CPU));
                tempMemoryLimitsSum += UnitUtils.parseK8sMemoryQuantity(limits.get(K_MEMORY));
            }
        }

        tempCpuRequestsSum *= availableReplicas;
        tempCpuLimitsSum *= availableReplicas;
        tempMemoryRequestsSum *= availableReplicas;
        tempMemoryLimitsSum *= availableReplicas;

        dto.addCpuRequest(tempCpuRequestsSum);
        dto.addCpuLimit(tempCpuLimitsSum);
        dto.addMemoryRequest(tempMemoryRequestsSum);
        dto.addMemoryLimit(tempMemoryLimitsSum);
    }

    private long getHostpathBytes(int replicas, List<Storage> storageList) {
        if (replicas < 1) {
            replicas = 1;
        }

        long hostpathTotalBytes = 0;
        if (storageList != null) {
            for (Storage storage : storageList) {
                if (storage.getStorageType() == StorageType.HostPath) {
                    hostpathTotalBytes += replicas * storage.getAmountBytes();
                }
            }
        }
        return hostpathTotalBytes;
    }

    private void composeCephRbdStorage(AppStatsDto dto,
                                       String namespaceName,
                                       List<Storage> storageList) {
        List<String> imageNameList = new ArrayList<>();
        for (Storage storage : storageList) {
            if (storage.getStorageType() == StorageType.RBD) {
                imageNameList.add(storage.getImageName());
            }
        }

        List<CephImageUsage> imageUsages = new ArrayList<>();
        if (!imageNameList.isEmpty()) {
            imageUsages = imageUsageRepository.findByNamespaceNameAndImageNameIn(namespaceName, imageNameList);
        }
        long rbdReservedBytes = 0;
        long rbdUsedBytes = 0;
        for (CephImageUsage usage : imageUsages) {
            rbdReservedBytes += usage.getBytesProvisioned();
            rbdUsedBytes += usage.getBytesUsed();
        }

        dto.setCephRbdStorageBytes(rbdReservedBytes);
        dto.setCephRbdStorageUsedBytes(rbdUsedBytes);
    }

    private void composeCephFsStorage(AppStatsDto dto,
                                      String namespaceName,
                                      List<Storage> storageList) {
        List<String> dirList = new ArrayList<>();
        for (Storage storage : storageList) {
            if (storage.getStorageType() == StorageType.CephFS) {
                dirList.add(StorageUtils.cephFsDir(namespaceName, storage.getStorageName()));
            }
        }

        List<CephfsDirectoryUsage> cephfsDirectoryUsages = new ArrayList<>();
        if (!dirList.isEmpty()) {
            cephfsDirectoryUsages = cephfsDirectoryUsageRepository.
                    findByNamespaceNameAndDirectoryNameIn(namespaceName, dirList);
        }

        long usedBytes = 0;
        for (CephfsDirectoryUsage usage : cephfsDirectoryUsages) {
            usedBytes += usage.getBytesUsed();
        }

        dto.setCephFsUsedBytes(usedBytes);
    }
}
