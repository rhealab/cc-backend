package cc.backend.kubernetes.apps.services;

import cc.backend.kubernetes.apps.data.AppRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/2/22.
 */
@Component
public class AppNamesManagement {
    private static final Logger logger = LoggerFactory.getLogger(AppNamesManagement.class);

    @Inject
    private AppRepository appRepository;

    public List<String> getAppsName(String namespaceName) {
        return appRepository.getAppsName(namespaceName);
    }
}
