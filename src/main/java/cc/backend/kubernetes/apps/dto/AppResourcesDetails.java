package cc.backend.kubernetes.apps.dto;

import cc.backend.kubernetes.apps.data.App;
import cc.backend.kubernetes.workload.deployment.dto.DeploymentDto;
import cc.backend.kubernetes.service.dto.ServiceDto;

import java.util.List;

/**
 * @author wangchunyang@gmail.com
 */
public class AppResourcesDetails {
    private App app;
    private List<ServiceDto> services;
    private List<DeploymentDto> deployments;

    public App getApp() {
        return app;
    }

    public void setApp(App app) {
        this.app = app;
    }

    public List<ServiceDto> getServices() {
        return services;
    }

    public void setServices(List<ServiceDto> services) {
        this.services = services;
    }

    public List<DeploymentDto> getDeployments() {
        return deployments;
    }

    public void setDeployments(List<DeploymentDto> deployments) {
        this.deployments = deployments;
    }

    @Override
    public String toString() {
        return "AppResourcesDetails{" +
                "app=" + app +
                ", services=" + services +
                ", deployments=" + deployments +
                '}';
    }
}
