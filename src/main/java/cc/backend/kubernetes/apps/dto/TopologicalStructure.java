package cc.backend.kubernetes.apps.dto;

import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/11/09.
 */
public class TopologicalStructure {
    private String name;
    private List<TopologicalStructure> children;

    public TopologicalStructure(String name, List<TopologicalStructure> children) {
        this.name = name;
        this.children = children;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TopologicalStructure> getChildren() {
        return children;
    }

    public void setChildren(List<TopologicalStructure> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "TopologicalStructure{" +
                "name='" + name + '\'' +
                ", children=" + children +
                '}';
    }

    public static TopologicalStructure from(AppResourcesTopological topological) {
        TopologicalStructure structure = new TopologicalStructure(topological.getName(), fromServices(topological.getServices()));

        TopologicalStructure independentDeployment = new TopologicalStructure("(independentDeployment)",
                fromDeployments(topological.getIndependentDeployments()));
        TopologicalStructure independentStatefulSet = new TopologicalStructure("(independentStatefulSet)",
                fromStatefulSets(topological.getIndependentStatefulSets()));

        if (!CollectionUtils.isEmpty(independentDeployment.getChildren())) {
            structure.getChildren().add(independentDeployment);
        }
        if (!CollectionUtils.isEmpty(independentStatefulSet.getChildren())) {
            structure.getChildren().add(independentStatefulSet);
        }
        return structure;
    }

    public static List<TopologicalStructure> fromServices(List<ServiceTopological> services) {
        if (services == null) {
            return new ArrayList<>();
        }

        return services.stream()
                .map(service -> {
                    List<TopologicalStructure> structures = fromDeployments(service.getDeployments());
                    structures.addAll(fromStatefulSets(service.getStatefulSets()));
                    return new TopologicalStructure(service.getName(), structures);
                })
                .collect(Collectors.toList());
    }

    public static List<TopologicalStructure> fromStatefulSets(List<StatefulSetTopological> statefulSets) {
        if (statefulSets == null) {
            return new ArrayList<>();
        }

        return statefulSets.stream()
                .map(statefulSet -> new TopologicalStructure(statefulSet.getName() + "(statefulset)", fromPods(statefulSet.getPods())))
                .collect(Collectors.toList());
    }

    public static List<TopologicalStructure> fromDeployments(List<DeploymentTopological> deployments) {
        if (deployments == null) {
            return new ArrayList<>();
        }

        return deployments.stream()
                .map(deployment -> new TopologicalStructure(deployment.getName(), fromPods(deployment.getPods())))
                .collect(Collectors.toList());
    }

    public static List<TopologicalStructure> fromPods(List<PodTopological> pods) {
        if (pods == null) {
            return new ArrayList<>();
        }

        return pods.stream()
                .map(pod -> new TopologicalStructure(pod.getName(), fromStorage(pod.getStorages())))
                .collect(Collectors.toList());
    }

    public static List<TopologicalStructure> fromStorage(List<StorageTopological> storages) {
        if (storages == null) {
            return new ArrayList<>();
        }

        return storages.stream()
                .map(storage -> new TopologicalStructure(storage.getStorageName(), new ArrayList<>()))
                .collect(Collectors.toList());
    }
}
