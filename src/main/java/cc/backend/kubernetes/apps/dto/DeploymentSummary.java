package cc.backend.kubernetes.apps.dto;

import io.fabric8.kubernetes.api.model.LabelSelector;
import io.fabric8.kubernetes.api.model.extensions.RollingUpdateDeployment;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/12.
 */
public class DeploymentSummary {
    private String name;
    private String uid;
    private String namespace;
    private long age;
    private int replicas;
    private Map<String, String> labels;
    private LabelSelector selector;
    private String strategyType;
    private RollingUpdateDeployment rollingUpdateStrategy;
    private List<String> images;
    private Date createdOn;
    private List<String> storageNames;
    private List<PodSummary> pods;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public int getReplicas() {
        return replicas;
    }

    public void setReplicas(int replicas) {
        this.replicas = replicas;
    }

    public LabelSelector getSelector() {
        return selector;
    }

    public void setSelector(LabelSelector selector) {
        this.selector = selector;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    public void setLabels(Map<String, String> labels) {
        this.labels = labels;
    }

    public String getStrategyType() {
        return strategyType;
    }

    public void setStrategyType(String strategyType) {
        this.strategyType = strategyType;
    }

    public RollingUpdateDeployment getRollingUpdateStrategy() {
        return rollingUpdateStrategy;
    }

    public void setRollingUpdateStrategy(RollingUpdateDeployment rollingUpdateStrategy) {
        this.rollingUpdateStrategy = rollingUpdateStrategy;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public List<String> getStorageNames() {
        return storageNames;
    }

    public void setStorageNames(List<String> storageNames) {
        this.storageNames = storageNames;
    }

    public List<PodSummary> getPods() {
        return pods;
    }

    public void setPods(List<PodSummary> pods) {
        this.pods = pods;
    }

    @Override
    public String toString() {
        return "DeploymentSummary{" +
                "name='" + name + '\'' +
                ", uid='" + uid + '\'' +
                ", namespace='" + namespace + '\'' +
                ", age=" + age +
                ", replicas=" + replicas +
                ", labels=" + labels +
                ", selector=" + selector +
                ", strategyType='" + strategyType + '\'' +
                ", rollingUpdateStrategy=" + rollingUpdateStrategy +
                ", images=" + images +
                ", createdOn=" + createdOn +
                ", storageNames=" + storageNames +
                ", pods=" + pods +
                '}';
    }
}
