package cc.backend.kubernetes.apps.dto;

import cc.backend.common.utils.DateUtils;
import io.fabric8.kubernetes.api.model.Container;
import io.fabric8.kubernetes.api.model.LabelSelector;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.PodSpec;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import io.fabric8.kubernetes.api.model.extensions.StatefulSetSpec;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/12.
 */
public class StatefulSetTopological {
    private String name;
    private String namespace;
    private long age;
    private int replicas;
    private Map<String, String> labels;
    private LabelSelector selector;
    private List<String> images;
    private Date createdOn;
    private List<PodTopological> pods;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public int getReplicas() {
        return replicas;
    }

    public void setReplicas(int replicas) {
        this.replicas = replicas;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    public void setLabels(Map<String, String> labels) {
        this.labels = labels;
    }

    public LabelSelector getSelector() {
        return selector;
    }

    public void setSelector(LabelSelector selector) {
        this.selector = selector;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public List<PodTopological> getPods() {
        return pods;
    }

    public void setPods(List<PodTopological> pods) {
        this.pods = pods;
    }

    @Override
    public String toString() {
        return "StatefulSetTopological{" +
                "name='" + name + '\'' +
                ", namespace='" + namespace + '\'' +
                ", age=" + age +
                ", replicas=" + replicas +
                ", labels=" + labels +
                ", selector=" + selector +
                ", images=" + images +
                ", createdOn=" + createdOn +
                ", pods=" + pods +
                '}';
    }

    public static List<StatefulSetTopological> from(List<StatefulSet> statefulSets,
                                                    Map<StatefulSet, List<PodTopological>> statefulSetPodsMap) {
        if (statefulSets == null) {
            return new ArrayList<>();
        }

        return statefulSets.stream()
                .map(statefulSet -> from(statefulSet, statefulSetPodsMap.get(statefulSet)))
                .collect(Collectors.toList());
    }

    public static StatefulSetTopological from(StatefulSet statefulSets, List<PodTopological> podTopoList) {
        StatefulSetTopological statefulSetTopo = new StatefulSetTopological();

        ObjectMeta metadata = statefulSets.getMetadata();
        StatefulSetSpec spec = statefulSets.getSpec();
        PodSpec podSpec = spec.getTemplate().getSpec();

        statefulSetTopo.setName(metadata.getName());
        statefulSetTopo.setNamespace(metadata.getNamespace());
        statefulSetTopo.setCreatedOn(DateUtils.parseK8sDate(metadata.getCreationTimestamp()));
        statefulSetTopo.setAge(DateUtils.getAge(statefulSetTopo.getCreatedOn()));
        statefulSetTopo.setReplicas(spec.getReplicas());
        statefulSetTopo.setLabels(metadata.getLabels());
        statefulSetTopo.setSelector(spec.getSelector());
        statefulSetTopo.setImages(podSpec.getContainers().stream().map(Container::getImage).collect(Collectors.toList()));
        statefulSetTopo.setPods(podTopoList);

        return statefulSetTopo;
    }
}
