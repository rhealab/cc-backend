package cc.backend.kubernetes.apps.dto;

import cc.backend.common.utils.DateUtils;
import io.fabric8.kubernetes.api.model.*;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/11/09.
 */
public class ServiceTopological {
    private String name;
    private String namespace;
    private String type;
    private Map<String, String> labels;
    private Map<String, String> selector;
    private String clusterIP;
    private List<String> externalIPs;

    private List<ServicePort> ports;
    private List<LoadBalancerIngress> ingress;
    private Date createdOn;

    private List<SvcEndpointDto> internalEndpoint;
    private List<SvcEndpointDto> externalEndpoints;

    private List<DeploymentTopological> deployments;
    private List<StatefulSetTopological> statefulSets;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public Map<String, String> getSelector() {
        return selector;
    }

    public void setSelector(Map<String, String> selector) {
        this.selector = selector;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    public void setLabels(Map<String, String> labels) {
        this.labels = labels;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getClusterIP() {
        return clusterIP;
    }

    public void setClusterIP(String clusterIP) {
        this.clusterIP = clusterIP;
    }

    public List<String> getExternalIPs() {
        return externalIPs;
    }

    public void setExternalIPs(List<String> externalIPs) {
        this.externalIPs = externalIPs;
    }

    public List<ServicePort> getPorts() {
        return ports;
    }

    public void setPorts(List<ServicePort> ports) {
        this.ports = ports;
    }

    public List<LoadBalancerIngress> getIngress() {
        return ingress;
    }

    public void setIngress(List<LoadBalancerIngress> ingress) {
        this.ingress = ingress;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public List<SvcEndpointDto> getInternalEndpoint() {
        return internalEndpoint;
    }

    public void setInternalEndpoint(List<SvcEndpointDto> internalEndpoint) {
        this.internalEndpoint = internalEndpoint;
    }

    public List<SvcEndpointDto> getExternalEndpoints() {
        return externalEndpoints;
    }

    public void setExternalEndpoints(List<SvcEndpointDto> externalEndpoints) {
        this.externalEndpoints = externalEndpoints;
    }

    public List<DeploymentTopological> getDeployments() {
        return deployments;
    }

    public void setDeployments(List<DeploymentTopological> deployments) {
        this.deployments = deployments;
    }

    public List<StatefulSetTopological> getStatefulSets() {
        return statefulSets;
    }

    public void setStatefulSets(List<StatefulSetTopological> statefulSets) {
        this.statefulSets = statefulSets;
    }

    @Override
    public String toString() {
        return "ServiceTopological{" +
                "name='" + name + '\'' +
                ", namespace='" + namespace + '\'' +
                ", type='" + type + '\'' +
                ", labels=" + labels +
                ", selector=" + selector +
                ", clusterIP='" + clusterIP + '\'' +
                ", externalIPs=" + externalIPs +
                ", ports=" + ports +
                ", ingress=" + ingress +
                ", createdOn=" + createdOn +
                ", internalEndpoint=" + internalEndpoint +
                ", externalEndpoints=" + externalEndpoints +
                ", deployments=" + deployments +
                ", statefulSets=" + statefulSets +
                '}';
    }

    public static class SvcEndpointDto {
        private String host;
        private List<ServicePort> ports;

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public List<ServicePort> getPorts() {
            return ports;
        }

        public void setPorts(List<ServicePort> ports) {
            this.ports = ports;
        }

        @Override
        public String toString() {
            return "SvcEndpointDto{" +
                    "host='" + host + '\'' +
                    ", ports=" + ports +
                    '}';
        }
    }

    public static List<ServiceTopological> from(List<Service> services,
                                                Map<Service, List<DeploymentTopological>> serviceDeploymentListMap,
                                                Map<Service, List<StatefulSetTopological>> serviceStatefulListMap) {
        if (services == null) {
            return new ArrayList<>();
        }

        return services.stream()
                .map(service -> from(service, serviceDeploymentListMap.get(service), serviceStatefulListMap.get(service)))
                .collect(toList());
    }

    public static ServiceTopological from(Service service,
                                          List<DeploymentTopological> deployments,
                                          List<StatefulSetTopological> statefulSets) {
        ObjectMeta metadata = service.getMetadata();
        ServiceSpec spec = service.getSpec();
        ServiceStatus status = service.getStatus();
        List<String> externalIPs = spec.getExternalIPs();

        ServiceTopological serviceTopo = new ServiceTopological();

        serviceTopo.setName(metadata.getName());
        serviceTopo.setNamespace(metadata.getNamespace());
        serviceTopo.setType(spec.getType());
        serviceTopo.setLabels(metadata.getLabels());
        serviceTopo.setSelector(spec.getSelector());
        serviceTopo.setClusterIP(spec.getClusterIP());
        serviceTopo.setExternalIPs(externalIPs);
        serviceTopo.setPorts(spec.getPorts());
        if (status != null && status.getLoadBalancer() != null) {
            serviceTopo.setIngress(status.getLoadBalancer().getIngress());
        }

        serviceTopo.setCreatedOn(DateUtils.parseK8sDate(metadata.getCreationTimestamp()));
        composeSvcEndpointDto(serviceTopo, externalIPs);
        serviceTopo.setDeployments(deployments);
        serviceTopo.setStatefulSets(statefulSets);

        return serviceTopo;
    }

    private static void composeSvcEndpointDto(ServiceTopological serviceTopo, List<String> externalIPs) {
        List<ServiceTopological.SvcEndpointDto> internalList = new ArrayList<>();
        ServiceTopological.SvcEndpointDto internal = new ServiceTopological.SvcEndpointDto();
        internal.setHost(serviceTopo.getName() + "." + serviceTopo.getNamespace());
        internal.setPorts(serviceTopo.getPorts());
        internalList.add(internal);
        serviceTopo.setInternalEndpoint(internalList);

        if (!CollectionUtils.isEmpty(externalIPs)) {
            List<ServiceTopological.SvcEndpointDto> externalList = new ArrayList<>();
            for (String ip : externalIPs) {
                ServiceTopological.SvcEndpointDto external = new ServiceTopological.SvcEndpointDto();
                external.setHost(ip);
                external.setPorts(serviceTopo.getPorts());
                externalList.add(external);
            }
            serviceTopo.setExternalEndpoints(externalList);
        }
    }
}