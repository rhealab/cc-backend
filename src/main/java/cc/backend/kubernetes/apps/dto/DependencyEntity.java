package cc.backend.kubernetes.apps.dto;

/**
 * Created by mye(Xizhe Ye) on 17-9-18.
 */
public class DependencyEntity {
  private String appName;
  private String instanceName;

  public DependencyEntity() {
  }

  public DependencyEntity(String appName, String instanceName) {
    this.appName = appName;
    this.instanceName = instanceName;
  }

  public String getAppName() {
    return appName;
  }

  public void setAppName(String appName) {
    this.appName = appName;
  }

  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof DependencyEntity) {
      DependencyEntity e = (DependencyEntity) o;
      return appName.equals(e.appName) && instanceName.equals(e.instanceName);
    }
    return false;
  }
}
