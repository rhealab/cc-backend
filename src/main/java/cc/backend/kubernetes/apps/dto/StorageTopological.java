package cc.backend.kubernetes.apps.dto;

import cc.backend.kubernetes.storage.domain.AccessModeType;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/11/09.
 */
public class StorageTopological {
    private long id;
    private String namespaceName;
    private String storageName;
    private String pvcName;
    private String pvName;
    private StorageType storageType;
    private AccessModeType accessMode;
    private long amountBytes;
    private long usedAmountBytes;

    /**
     * only for hostpath
     * true: storage keep in the chosen node and info will not loss when pod was deleted
     * false: when pod was deleted the info will loss
     */
    private boolean persisted;

    /**
     * only for hostpath
     * true: each pod has there own storage (quota will be replicas * amountBytes)
     * false: all pod use the only one storage
     */
    private boolean unshared;

    /**
     * for nfs cephfs rbd pv read only option
     */
    private boolean readOnly;

    private Storage.Status status;

    private Date createdOn;
    private String createdBy;
    private Date lastUpdatedOn;
    private String lastUpdatedBy;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public String getPvcName() {
        return pvcName;
    }

    public void setPvcName(String pvcName) {
        this.pvcName = pvcName;
    }

    public String getPvName() {
        return pvName;
    }

    public void setPvName(String pvName) {
        this.pvName = pvName;
    }

    public StorageType getStorageType() {
        return storageType;
    }

    public void setStorageType(StorageType storageType) {
        this.storageType = storageType;
    }

    public AccessModeType getAccessMode() {
        return accessMode;
    }

    public void setAccessMode(AccessModeType accessMode) {
        this.accessMode = accessMode;
    }

    public long getAmountBytes() {
        return amountBytes;
    }

    public void setAmountBytes(long amountBytes) {
        this.amountBytes = amountBytes;
    }

    public long getUsedAmountBytes() {
        return usedAmountBytes;
    }

    public void setUsedAmountBytes(long usedAmountBytes) {
        this.usedAmountBytes = usedAmountBytes;
    }

    public boolean isPersisted() {
        return persisted;
    }

    public void setPersisted(boolean persisted) {
        this.persisted = persisted;
    }

    public boolean isUnshared() {
        return unshared;
    }

    public void setUnshared(boolean unshared) {
        this.unshared = unshared;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public Storage.Status getStatus() {
        return status;
    }

    public void setStatus(Storage.Status status) {
        this.status = status;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(Date lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    @Override
    public String toString() {
        return "StorageTopological{" +
                "id=" + id +
                ", namespaceName='" + namespaceName + '\'' +
                ", storageName='" + storageName + '\'' +
                ", pvcName='" + pvcName + '\'' +
                ", pvName='" + pvName + '\'' +
                ", storageType=" + storageType +
                ", accessMode=" + accessMode +
                ", amountBytes=" + amountBytes +
                ", usedAmountBytes=" + usedAmountBytes +
                ", persisted=" + persisted +
                ", unshared=" + unshared +
                ", readOnly=" + readOnly +
                ", status=" + status +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                ", lastUpdatedOn=" + lastUpdatedOn +
                ", lastUpdatedBy='" + lastUpdatedBy + '\'' +
                '}';
    }

    /**
     * convert from storage list
     * used bytes will be set when set storage to pod
     *
     * @param storages the storage list
     * @return the storage topological list
     */
    public static List<StorageTopological> from(List<Storage> storages) {
        if (storages == null) {
            return new ArrayList<>();
        }

        return storages.stream()
                .map(storage -> {
                    StorageTopological topological = new StorageTopological();
                    topological.setId(storage.getId());
                    topological.setNamespaceName(storage.getNamespaceName());
                    topological.setStorageName(storage.getStorageName());
                    topological.setStorageType(storage.getStorageType());
                    topological.setPvcName(storage.getPvcName());
                    topological.setPvName(storage.getPvName());
                    topological.setAccessMode(storage.getAccessMode());
                    topological.setAmountBytes(storage.getAmountBytes());

                    topological.setPersisted(storage.isPersisted());
                    topological.setUnshared(storage.isUnshared());
                    topological.setReadOnly(storage.isReadOnly());
                    topological.setStatus(storage.getStatus());
                    topological.setCreatedOn(storage.getCreatedOn());
                    topological.setCreatedBy(storage.getCreatedBy());
                    topological.setLastUpdatedOn(storage.getLastUpdatedOn());
                    topological.setLastUpdatedBy(storage.getLastUpdatedBy());
                    return topological;
                })
                .collect(toList());
    }
}
