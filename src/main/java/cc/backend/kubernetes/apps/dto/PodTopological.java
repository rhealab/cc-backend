package cc.backend.kubernetes.apps.dto;

import cc.backend.common.utils.DateUtils;
import cc.backend.kubernetes.workload.pod.PodHelper;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.PodSpec;
import io.fabric8.kubernetes.api.model.PodStatus;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/11/09.
 */
public class PodTopological {
    private String name;
    private String namespace;
    private String status;
    private long age;
    private Map<String, String> labels;
    private Date startTime;
    private Date createdOn;

    private String podIP;
    private String nodeName;

    private List<ContainerTopological> containers;
    private List<StorageTopological> storages;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    public void setLabels(Map<String, String> labels) {
        this.labels = labels;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getPodIP() {
        return podIP;
    }

    public void setPodIP(String podIP) {
        this.podIP = podIP;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public List<ContainerTopological> getContainers() {
        return containers;
    }

    public void setContainers(List<ContainerTopological> containers) {
        this.containers = containers;
    }

    public List<StorageTopological> getStorages() {
        return storages;
    }

    public void setStorages(List<StorageTopological> storages) {
        this.storages = storages;
    }

    @Override
    public String toString() {
        return "PodTopological{" +
                "name='" + name + '\'' +
                ", namespace='" + namespace + '\'' +
                ", status='" + status + '\'' +
                ", age=" + age +
                ", labels=" + labels +
                ", startTime=" + startTime +
                ", createdOn=" + createdOn +
                ", podIP='" + podIP + '\'' +
                ", nodeName='" + nodeName + '\'' +
                ", containers=" + containers +
                ", storages=" + storages +
                '}';
    }

    public static List<PodTopological> from(Map<Pod, List<StorageTopological>> podStoragesMap) {
        return podStoragesMap.entrySet().stream()
                .map(entry -> from(entry.getKey(), podStoragesMap.get(entry.getKey())))
                .collect(Collectors.toList());
    }

    public static List<PodTopological> from(Map<String, List<Pod>> deploymentPodsMap,
                                            Map<String, List<StorageTopological>> deploymentStoragesMap) {
        return deploymentPodsMap.entrySet().stream()
                .flatMap(entry -> entry.getValue().stream()
                        .map(pod -> from(pod, deploymentStoragesMap.get(entry.getKey()))))
                .collect(Collectors.toList());
    }

    public static PodTopological from(Pod pod, List<StorageTopological> storages) {
        PodTopological podTopological = new PodTopological();

        ObjectMeta metadata = pod.getMetadata();
        PodSpec spec = pod.getSpec();
        PodStatus status = pod.getStatus();

        podTopological.setName(metadata.getName());
        podTopological.setNamespace(metadata.getNamespace());
        podTopological.setStatus(PodHelper.getDisplayStatus(pod));
        podTopological.setAge(DateUtils.getAge(podTopological.getStartTime()));
        podTopological.setLabels(metadata.getLabels());
        podTopological.setStartTime(DateUtils.parseK8sDate(status.getStartTime()));
        podTopological.setCreatedOn(DateUtils.parseK8sDate(metadata.getCreationTimestamp()));

        podTopological.setPodIP(status.getPodIP());
        podTopological.setNodeName(spec.getNodeName());

        podTopological.setContainers(ContainerTopological.from(status.getContainerStatuses(), spec.getContainers()));

        podTopological.setStorages(storages);
        return podTopological;
    }
}
