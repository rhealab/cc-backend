package cc.backend.kubernetes.apps.dto;

import cc.backend.kubernetes.apps.data.App;

import java.util.List;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/11/09.
 * <p>
 * this shit code is not my intention, my intention is AppResourcesTopological
 */
public class AppTopologicalFrontend {
    private App app;
    private List<ServiceTopologicalDeprecated> services;
    private List<DeploymentTopologicalDeprecated> deployments;
    private List<StorageTopologicalDeprecated> storages;
    private List<PodTopologicalDeprecated> pods;

    private TopologicalStructure structure;

    public App getApp() {
        return app;
    }

    public void setApp(App app) {
        this.app = app;
    }

    public List<ServiceTopologicalDeprecated> getServices() {
        return services;
    }

    public void setServices(List<ServiceTopologicalDeprecated> services) {
        this.services = services;
    }

    public List<DeploymentTopologicalDeprecated> getDeployments() {
        return deployments;
    }

    public void setDeployments(List<DeploymentTopologicalDeprecated> deployments) {
        this.deployments = deployments;
    }

    public List<StorageTopologicalDeprecated> getStorages() {
        return storages;
    }

    public void setStorages(List<StorageTopologicalDeprecated> storages) {
        this.storages = storages;
    }

    public List<PodTopologicalDeprecated> getPods() {
        return pods;
    }

    public void setPods(List<PodTopologicalDeprecated> pods) {
        this.pods = pods;
    }

    public TopologicalStructure getStructure() {
        return structure;
    }

    public void setStructure(TopologicalStructure structure) {
        this.structure = structure;
    }

    @Override
    public String toString() {
        return "AppTopologicalFrontend{" +
                "app=" + app +
                ", services=" + services +
                ", deployments=" + deployments +
                ", storages=" + storages +
                ", pods=" + pods +
                ", structure=" + structure +
                '}';
    }


    public static AppTopologicalFrontend from(AppResourcesTopological topological,
                                              AppResourcesTopologicalDeprecated deprecatedTopological) {
        AppTopologicalFrontend frontend = new AppTopologicalFrontend();
        frontend.setApp(deprecatedTopological.getApp());
        frontend.setDeployments(deprecatedTopological.getDeployments());
        frontend.setServices(deprecatedTopological.getServices());
        frontend.setPods(deprecatedTopological.getPods());
        frontend.setStorages(deprecatedTopological.getStorages());
        frontend.setStructure(TopologicalStructure.from(topological));

        return frontend;
    }
}
