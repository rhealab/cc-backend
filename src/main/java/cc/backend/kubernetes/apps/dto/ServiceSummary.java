package cc.backend.kubernetes.apps.dto;

import cc.backend.common.utils.DateUtils;
import io.fabric8.kubernetes.api.model.*;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by xzy on 2017/2/22.
 */
public class ServiceSummary {
    private String name;
    private String uid;
    private String namespace;
    private String type;
    private Map<String, String> labels;
    private Map<String, String> selector;
    private String clusterIP;
    private List<String> externalIPs;

    private List<SvcEndpointDto> internalEndpoint;
    private List<SvcEndpointDto> externalEndpoints;

    private List<ServicePort> ports;
    private List<LoadBalancerIngress> ingress;
    private Date createdOn;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public Map<String, String> getSelector() {
        return selector;
    }

    public void setSelector(Map<String, String> selector) {
        this.selector = selector;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    public void setLabels(Map<String, String> labels) {
        this.labels = labels;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getClusterIP() {
        return clusterIP;
    }

    public void setClusterIP(String clusterIP) {
        this.clusterIP = clusterIP;
    }

    public List<String> getExternalIPs() {
        return externalIPs;
    }

    public void setExternalIPs(List<String> externalIPs) {
        this.externalIPs = externalIPs;
    }

    public List<SvcEndpointDto> getInternalEndpoint() {
        return internalEndpoint;
    }

    public void setInternalEndpoint(List<SvcEndpointDto> internalEndpoint) {
        this.internalEndpoint = internalEndpoint;
    }

    public List<SvcEndpointDto> getExternalEndpoints() {
        return externalEndpoints;
    }

    public void setExternalEndpoints(List<SvcEndpointDto> externalEndpoints) {
        this.externalEndpoints = externalEndpoints;
    }

    public List<ServicePort> getPorts() {
        return ports;
    }

    public void setPorts(List<ServicePort> ports) {
        this.ports = ports;
    }

    public List<LoadBalancerIngress> getIngress() {
        return ingress;
    }

    public void setIngress(List<LoadBalancerIngress> ingress) {
        this.ingress = ingress;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public static class SvcEndpointDto {
        private String host;
        private List<ServicePort> ports;

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public List<ServicePort> getPorts() {
            return ports;
        }

        public void setPorts(List<ServicePort> ports) {
            this.ports = ports;
        }
    }

    /**
     * @param services not null
     * @return composed service list
     */
    public static List<ServiceSummary> from(List<Service> services) {
        List<ServiceSummary> dtos = new ArrayList<>();
        if (services != null) {
            for (Service service : services) {
                dtos.add(from(service));
            }
        }
        return dtos;
    }

    /**
     * @param service not null
     * @return composed service
     */
    public static ServiceSummary from(Service service) {
        ObjectMeta metadata = service.getMetadata();
        ServiceSpec spec = service.getSpec();
        ServiceStatus status = service.getStatus();
        ServiceSummary dto = new ServiceSummary();

        dto.setName(metadata.getName());
        dto.setUid(metadata.getUid());
        dto.setNamespace(metadata.getNamespace());
        dto.setSelector(spec.getSelector());
        dto.setLabels(metadata.getLabels());
        dto.setType(spec.getType());

        dto.setClusterIP(spec.getClusterIP());
        List<String> externalIPs = spec.getExternalIPs();
        dto.setExternalIPs(externalIPs);

        dto.setPorts(spec.getPorts());
        if (status != null && status.getLoadBalancer() != null) {
            dto.setIngress(status.getLoadBalancer().getIngress());
        }

        dto.setCreatedOn(DateUtils.parseK8sDate(metadata.getCreationTimestamp()));

        composeSvcEndpointDto(dto, externalIPs);

        return dto;
    }

    private static void composeSvcEndpointDto(ServiceSummary dto, List<String> externalIPs) {
        List<SvcEndpointDto> internalList = new ArrayList<>();
        SvcEndpointDto internal = new SvcEndpointDto();
        internal.setHost(dto.getName() + "." + dto.getNamespace());
        internal.setPorts(dto.getPorts());
        internalList.add(internal);
        dto.setInternalEndpoint(internalList);

        if (!CollectionUtils.isEmpty(externalIPs)) {
            List<SvcEndpointDto> externalList = new ArrayList<>();
            for (String ip : externalIPs) {
                SvcEndpointDto external = new SvcEndpointDto();
                external.setHost(ip);
                external.setPorts(dto.getPorts());
                externalList.add(external);
            }
            dto.setInternalEndpoint(externalList);
        }
    }
}