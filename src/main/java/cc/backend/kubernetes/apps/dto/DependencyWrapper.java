package cc.backend.kubernetes.apps.dto;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * Created by mye(Xizhe Ye) on 17-9-18.
 */
public class DependencyWrapper {
  private String name = new String();
  private List<DependencyEntity> entityList = Lists.newLinkedList();

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<DependencyEntity> getEntityList() {
    return entityList;
  }

  public void setEntityList(List<DependencyEntity> entityList) {
    this.entityList = entityList;
  }
}
