package cc.backend.kubernetes.apps.dto;

import cc.backend.kubernetes.apps.data.App;
import cc.backend.kubernetes.apps.data.AppStatus;
import cc.backend.kubernetes.apps.data.AppType;
import cc.backend.kubernetes.storage.HostPathStorageManager;
import cc.backend.kubernetes.storage.StorageValidator;
import cc.backend.kubernetes.storage.domain.HostPathUsage;
import cc.backend.kubernetes.storage.domain.Storage;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.IntStream;

import static cc.backend.kubernetes.storage.domain.StorageType.HostPath;
import static java.util.stream.Collectors.*;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/11/09.
 */
public class AppResourcesTopological {
    private long id;
    private String name;
    private String namespace;
    private AppStatus status;
    private AppType type;
    private String predefineType;
    private Date lastUpdatedOn;
    private String lastUpdatedBy;
    private Date createdOn;
    private String createdBy;

    private List<DeploymentTopological> independentDeployments;
    private List<StatefulSetTopological> independentStatefulSets;

    private List<ServiceTopological> services;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public AppStatus getStatus() {
        return status;
    }

    public void setStatus(AppStatus status) {
        this.status = status;
    }

    public AppType getType() {
        return type;
    }

    public void setType(AppType type) {
        this.type = type;
    }

    public String getPredefineType() {
        return predefineType;
    }

    public void setPredefineType(String predefineType) {
        this.predefineType = predefineType;
    }

    public Date getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(Date lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public List<DeploymentTopological> getIndependentDeployments() {
        return independentDeployments;
    }

    public void setIndependentDeployments(List<DeploymentTopological> independentDeployments) {
        this.independentDeployments = independentDeployments;
    }

    public List<StatefulSetTopological> getIndependentStatefulSets() {
        return independentStatefulSets;
    }

    public void setIndependentStatefulSets(List<StatefulSetTopological> independentStatefulSets) {
        this.independentStatefulSets = independentStatefulSets;
    }

    public List<ServiceTopological> getServices() {
        return services;
    }

    public void setServices(List<ServiceTopological> services) {
        this.services = services;
    }

    @Override
    public String toString() {
        return "AppResourcesTopological{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", namespace='" + namespace + '\'' +
                ", status=" + status +
                ", type=" + type +
                ", predefineType='" + predefineType + '\'' +
                ", lastUpdatedOn=" + lastUpdatedOn +
                ", lastUpdatedBy='" + lastUpdatedBy + '\'' +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                ", independentDeployments=" + independentDeployments +
                ", independentStatefulSets=" + independentStatefulSets +
                ", services=" + services +
                '}';
    }

    public static class Builder {
        private App app;

        private List<Service> services;
        private List<Deployment> deployments;
        private List<StatefulSet> statefulSets;
        private List<Pod> pods;
        private List<Storage> storages;
        private HostPathStorageManager hostPathStorageManager;
        private StorageValidator storageValidator;

        public Builder app(App app) {
            this.app = app;
            return this;
        }

        public Builder services(List<Service> services) {
            this.services = services;
            return this;
        }

        public Builder deployments(List<Deployment> deployments) {
            this.deployments = deployments;
            return this;
        }

        public Builder statefulSets(List<StatefulSet> statefulSets) {
            this.statefulSets = statefulSets;
            return this;
        }

        public Builder pods(List<Pod> pods) {
            this.pods = pods;
            return this;
        }

        public Builder storages(List<Storage> storages) {
            this.storages = storages;
            return this;
        }

        public Builder hostPathStorageManager(HostPathStorageManager hostPathStorageManager) {
            this.hostPathStorageManager = hostPathStorageManager;
            return this;
        }

        public Builder storageValidator(StorageValidator storageValidator) {
            this.storageValidator = storageValidator;
            return this;
        }

        public AppResourcesTopological build() {
            AppResourcesTopological topological = new AppResourcesTopological();
            if (app != null) {
                topological.setId(app.getId());
                topological.setName(app.getName());
                topological.setNamespace(app.getNamespace());
                topological.setStatus(app.getStatus());
                topological.setType(app.getType());
                topological.setPredefineType(app.getPredefineType());
                topological.setLastUpdatedOn(app.getLastUpdatedOn());
                topological.setLastUpdatedBy(app.getLastUpdatedBy());
                topological.setCreatedBy(app.getCreatedBy());
                topological.setCreatedOn(app.getCreatedOn());
            }

            List<StorageTopological> storageTopoList = StorageTopological.from(storages);

            Map<Pod, List<StorageTopological>> podStorageTopoListMap = composePodStorageListMap(pods, storageTopoList);
            List<PodTopological> podTopoList = PodTopological.from(podStorageTopoListMap);
            setStorageUsedBytes(podTopoList, hostPathStorageManager, storageValidator);

            Map<Deployment, List<PodTopological>> deploymentPodTopoListMap = composeDeploymentPodsMap(deployments, podTopoList);
            List<DeploymentTopological> deploymentTopoList = DeploymentTopological.from(deployments, deploymentPodTopoListMap);

            Map<StatefulSet, List<PodTopological>> statefulSetPodTopoListMap = composeStatefulSetPodsMap(statefulSets, podTopoList);
            List<StatefulSetTopological> statefulSetTopoList = StatefulSetTopological.from(statefulSets, statefulSetPodTopoListMap);

            Map<Service, List<StatefulSetTopological>> serviceStatefulSetListMapListMap = composeServiceStatefulSetListMap(services, statefulSetTopoList);
            Map<Service, List<DeploymentTopological>> serviceDeploymentListMap = composeServiceDeploymentListMap(services, deploymentTopoList);
            List<ServiceTopological> serviceTopoList = ServiceTopological.from(services, serviceDeploymentListMap, serviceStatefulSetListMapListMap);

            List<DeploymentTopological> independentDeploymentTopoList = getIndependentDeploymentTopoList(serviceDeploymentListMap, deploymentTopoList);
            List<StatefulSetTopological> independentStatefulSetTopoList = getIndependentStatefulSetTopoList(serviceStatefulSetListMapListMap, statefulSetTopoList);

            topological.setServices(serviceTopoList);
            topological.setIndependentDeployments(independentDeploymentTopoList);
            topological.setIndependentStatefulSets(independentStatefulSetTopoList);

            return topological;
        }


        private void setStorageUsedBytes(List<PodTopological> podTopoList,
                                         HostPathStorageManager hostPathStorageManager,
                                         StorageValidator storageValidator) {
            /*
            consider get all storage used bytes at one time
             */

            podTopoList.forEach(pod -> pod.getStorages().forEach(storage -> {
                if (storage.getStorageType() == HostPath) {
                    List<HostPathUsage.MountInfo> mountInfoList = hostPathStorageManager.getHostPathMountInfo(storage.getPvName());
                    OptionalLong bytes = mountInfoList.stream()
                            .filter(mountInfo -> mountInfo.getPodInfo() != null)
                            .filter(mountInfo -> pod.getName().equals(mountInfo.getPodInfo().getInfo()))
                            .mapToLong(HostPathUsage.MountInfo::getVolumeCurrentSize)
                            .findAny();
                    storage.setUsedAmountBytes(bytes.orElse(0));

                } else {
                    storage.setUsedAmountBytes(storageValidator.getStorageUsedBytes(storage));
                }
            }));
        }

        private List<DeploymentTopological> getIndependentDeploymentTopoList(Map<Service, List<DeploymentTopological>> serviceDeploymentListMap,
                                                                             List<DeploymentTopological> deploymentTopoList) {

            List<DeploymentTopological> dependentDeployments = serviceDeploymentListMap.values().stream()
                    .flatMap(Collection::stream)
                    .collect(toList());

            return deploymentTopoList.stream()
                    .filter(deployment -> !dependentDeployments.contains(deployment))
                    .collect(toList());
        }

        private List<StatefulSetTopological> getIndependentStatefulSetTopoList(Map<Service, List<StatefulSetTopological>> serviceStatefulSetListMap,
                                                                               List<StatefulSetTopological> statefulSetTopoList) {

            List<StatefulSetTopological> dependentStatefulSets = serviceStatefulSetListMap.values().stream()
                    .flatMap(Collection::stream)
                    .collect(toList());

            return statefulSetTopoList.stream()
                    .filter(statefulSet -> !dependentStatefulSets.contains(statefulSet))
                    .collect(toList());
        }

        private Map<Service, List<StatefulSetTopological>> composeServiceStatefulSetListMap(List<Service> services,
                                                                                            List<StatefulSetTopological> statefulSetTopoList) {
            if (CollectionUtils.isEmpty(services)) {
                return new HashMap<>(0);
            }

            Map<Map<String, String>, List<StatefulSetTopological>> selectorStatefulSetMap = statefulSetTopoList.stream()
                    .filter(statefulSet -> statefulSet.getSelector() != null
                            && !CollectionUtils.isEmpty(statefulSet.getSelector().getMatchLabels()))
                    .collect(groupingBy(statefulSet -> statefulSet.getSelector().getMatchLabels()));

            return services.stream()
                    .filter(service -> !CollectionUtils.isEmpty(service.getSpec().getSelector()))
                    .collect(toMap(service -> service,
                            service -> selectorStatefulSetMap.getOrDefault(service.getSpec().getSelector(), new ArrayList<>())));
        }

        private Map<Service, List<DeploymentTopological>> composeServiceDeploymentListMap(List<Service> services,
                                                                                          List<DeploymentTopological> deploymentTopoList) {
            if (CollectionUtils.isEmpty(services)) {
                return new HashMap<>(0);
            }

            Map<Map<String, String>, List<DeploymentTopological>> selectorDeploymentMap = deploymentTopoList.stream()
                    .filter(deployment -> deployment.getSelector() != null
                            && !CollectionUtils.isEmpty(deployment.getSelector().getMatchLabels()))
                    .collect(groupingBy(deployment -> deployment.getSelector().getMatchLabels()));

            return services.stream()
                    .filter(service -> !CollectionUtils.isEmpty(service.getSpec().getSelector()))
                    .collect(toMap(service -> service,
                            service -> selectorDeploymentMap.getOrDefault(service.getSpec().getSelector(), new ArrayList<>())));
        }

        private Map<Pod, List<StorageTopological>> composePodStorageListMap(List<Pod> pods, List<StorageTopological> storageList) {
            if (CollectionUtils.isEmpty(pods)) {
                return new HashMap<>(0);
            }

            Map<String, StorageTopological> pvcStorageMap = storageList.stream()
                    .collect(toMap(StorageTopological::getPvcName, storage -> storage));
            return pods.stream()
                    .distinct()
                    .collect(toMap(pod -> pod, pod -> getStorageListByPod(pvcStorageMap, pod)));
        }

        private List<StorageTopological> getStorageListByPod(Map<String, StorageTopological> pvcStorageMap, Pod pod) {
            return Optional.ofNullable(pod.getSpec().getVolumes())
                    .orElseGet(ArrayList::new)
                    .stream()
                    .filter(volume -> volume.getPersistentVolumeClaim() != null)
                    .map(volume -> pvcStorageMap.get(volume.getPersistentVolumeClaim().getClaimName()))
                    .filter(Objects::nonNull)
                    .collect(toList());
        }

        private Map<Deployment, List<PodTopological>> composeDeploymentPodsMap(List<Deployment> deployments,
                                                                               List<PodTopological> pods) {

            if (StringUtils.isEmpty(deployments)) {
                return new HashMap<>(0);
            }

            if (StringUtils.isEmpty(pods)) {
                return deployments.stream()
                        .collect(toMap(deployment -> deployment,
                                deployment -> new ArrayList<>()));
            }

            /*
            group by pod labels
             */
            Map<Map<String, String>, List<PodTopological>> labelPodMap = pods.stream()
                    .collect(groupingBy(PodTopological::getLabels));
            return deployments
                    .stream()
                    .collect(toMap(deployment -> deployment,
                            deployment -> getPodsByDeployment(deployment, labelPodMap)));
        }

        private List<PodTopological> getPodsByDeployment(Deployment deployment,
                                                         Map<Map<String, String>, List<PodTopological>> labelsPodsMap) {
            Map<String, String> deploymentSelectors = deployment.getSpec()
                    .getSelector().getMatchLabels();
            return labelsPodsMap.entrySet()
                    .stream()
                    .filter(entry -> entry.getKey().entrySet().
                            containsAll(deploymentSelectors.entrySet()))
                    .map(Map.Entry::getValue)
                    .flatMap(List::stream)
                    .collect(toList());
        }

        private Map<StatefulSet, List<PodTopological>> composeStatefulSetPodsMap(List<StatefulSet> statefulSets,
                                                                                 List<PodTopological> pods) {

            if (StringUtils.isEmpty(statefulSets)) {
                return new HashMap<>(0);
            }

            if (StringUtils.isEmpty(pods)) {
                return statefulSets.stream()
                        .collect(toMap(statefulSet -> statefulSet,
                                statefulSet -> new ArrayList<>()));
            }

            Map<String, PodTopological> podMap = pods.stream()
                    .collect(toMap(PodTopological::getName, pod -> pod));

            return statefulSets
                    .stream()
                    .collect(toMap(statefulSet -> statefulSet,
                            statefulSet -> getPodsByStatefulSet(statefulSet, podMap)));
        }

        private List<PodTopological> getPodsByStatefulSet(StatefulSet statefulSet,
                                                          Map<String, PodTopological> podMap) {
            String name = statefulSet.getMetadata().getName();
            int replicas = Optional.ofNullable(statefulSet.getSpec().getReplicas()).orElse(0);

            return IntStream.range(0, replicas)
                    .mapToObj(num -> podMap.get(name + "-" + num))
                    .filter(Objects::nonNull)
                    .collect(toList());
        }
    }
}
