package cc.backend.kubernetes.apps.dto;

import cc.backend.kubernetes.apps.data.App;

import java.util.List;

/**
 * @author wangchunyang@gmail.com
 */
public class AppResourcesTopologicalDeprecated {
    private App app;
    private List<ServiceTopologicalDeprecated> services;
    private List<DeploymentTopologicalDeprecated> deployments;
    private List<StorageTopologicalDeprecated> storages;

    private List<PodTopologicalDeprecated> pods;

    public App getApp() {
        return app;
    }

    public void setApp(App app) {
        this.app = app;
    }

    public List<ServiceTopologicalDeprecated> getServices() {
        return services;
    }

    public void setServices(List<ServiceTopologicalDeprecated> services) {
        this.services = services;
    }

    public List<DeploymentTopologicalDeprecated> getDeployments() {
        return deployments;
    }

    public void setDeployments(List<DeploymentTopologicalDeprecated> deployments) {
        this.deployments = deployments;
    }

    public List<StorageTopologicalDeprecated> getStorages() {
        return storages;
    }

    public void setStorages(List<StorageTopologicalDeprecated> storages) {
        this.storages = storages;
    }

    public List<PodTopologicalDeprecated> getPods() {
        return pods;
    }

    public void setPods(List<PodTopologicalDeprecated> pods) {
        this.pods = pods;
    }

    @Override
    public String toString() {
        return "AppResourcesTopologicalDeprecated{" +
                "app=" + app +
                ", services=" + services +
                ", deployments=" + deployments +
                ", storages=" + storages +
                ", pods=" + pods +
                '}';
    }
}
