package cc.backend.kubernetes.apps.dto;

import cc.backend.kubernetes.storage.domain.*;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static cc.backend.kubernetes.storage.domain.StorageType.HostPath;
import static cc.backend.kubernetes.storage.domain.StorageType.StorageClass;

/**
 * @author wangchunyang@gmail.com
 */
public class StorageTopologicalDeprecated {
    private long id;
    private String namespaceName;
    private String storageName;
    private StorageType storageType;
    private AccessModeType accessMode;
    private long amountBytes;
    private long usedAmountBytes;

    /**
     * only for hostpath
     * true: storage keep in the chosen node and info will not loss when pod was deleted
     * false: when pod was deleted the info will loss
     */
    private boolean persisted;

    /**
     * only for hostpath
     * true: each pod has there own storage (quota will be replicas * amountBytes)
     * false: all pod use the only one storage
     */
    private boolean unshared;

    /**
     * for nfs cephfs rbd pv read only option
     */
    private boolean readOnly;

    private Storage.Status status;

    private String relatedPodName;

    private List<HostpathTopological> hostpathInfo;
    private List<StorageWorkload> relatedWorkloadList;
    private List<StorageWorkload> relatedDeploymentList;

    private Date createdOn;
    private String createdBy;
    private Date modifiedOn;
    private String modifiedBy;

    private boolean mounted;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public StorageType getStorageType() {
        return storageType;
    }

    public void setStorageType(StorageType storageType) {
        this.storageType = storageType;
    }

    public AccessModeType getAccessMode() {
        return accessMode;
    }

    public void setAccessMode(AccessModeType accessMode) {
        this.accessMode = accessMode;
    }

    public long getAmountBytes() {
        return amountBytes;
    }

    public void setAmountBytes(long amountBytes) {
        this.amountBytes = amountBytes;
    }

    public long getUsedAmountBytes() {
        return usedAmountBytes;
    }

    public void setUsedAmountBytes(long usedAmountBytes) {
        this.usedAmountBytes = usedAmountBytes;
    }

    public boolean isPersisted() {
        return persisted;
    }

    public void setPersisted(boolean persisted) {
        this.persisted = persisted;
    }

    public boolean isUnshared() {
        return unshared;
    }

    public void setUnshared(boolean unshared) {
        this.unshared = unshared;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public Storage.Status getStatus() {
        return status;
    }

    public void setStatus(Storage.Status status) {
        this.status = status;
    }

    public String getRelatedPodName() {
        return relatedPodName;
    }

    public void setRelatedPodName(String relatedPodName) {
        this.relatedPodName = relatedPodName;
    }

    public List<HostpathTopological> getHostpathInfo() {
        return hostpathInfo;
    }

    public void setHostpathInfo(List<HostpathTopological> hostpathInfo) {
        this.hostpathInfo = hostpathInfo;
    }

    public List<StorageWorkload> getRelatedWorkloadList() {
        return relatedWorkloadList;
    }

    public void setRelatedWorkloadList(List<StorageWorkload> relatedWorkloadList) {
        this.relatedWorkloadList = relatedWorkloadList;
    }

    public List<StorageWorkload> getRelatedDeploymentList() {
        return relatedDeploymentList;
    }

    public void setRelatedDeploymentList(List<StorageWorkload> relatedDeploymentList) {
        this.relatedDeploymentList = relatedDeploymentList;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public boolean isMounted() {
        return mounted;
    }

    public void setMounted(boolean mounted) {
        this.mounted = mounted;
    }

    @Override
    public String toString() {
        return "StorageTopologicalDeprecated{" +
                "id=" + id +
                ", namespaceName='" + namespaceName + '\'' +
                ", storageName='" + storageName + '\'' +
                ", storageType=" + storageType +
                ", accessMode=" + accessMode +
                ", amountBytes=" + amountBytes +
                ", usedAmountBytes=" + usedAmountBytes +
                ", persisted=" + persisted +
                ", unshared=" + unshared +
                ", readOnly=" + readOnly +
                ", status=" + status +
                ", relatedPodName='" + relatedPodName + '\'' +
                ", hostpathInfo=" + hostpathInfo +
                ", relatedWorkloadList=" + relatedWorkloadList +
                ", relatedDeploymentList=" + relatedDeploymentList +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                ", modifiedOn=" + modifiedOn +
                ", modifiedBy='" + modifiedBy + '\'' +
                ", mounted=" + mounted +
                '}';
    }


    public static class Builder {
        private long id;

        private String namespaceName;
        private String storageName;
        private StorageType storageType;
        private AccessModeType accessMode;
        private long amountBytes;
        private long usedAmountBytes;

        private boolean persisted;
        private boolean unshared;
        private boolean readOnly;

        private String pvcName;

        private List<HostpathTopological> hostpathInfo;
        private List<StorageWorkload> relatedWorkloadList;

        private Date createdOn;
        private String createdBy;
        private Date modifiedOn;
        private String modifiedBy;
        private Storage.Status status;

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder namespaceName(String namespaceName) {
            this.namespaceName = namespaceName;
            return this;
        }

        public Builder storageName(String storageName) {
            this.storageName = storageName;
            return this;
        }

        public Builder storageType(StorageType storageType) {
            this.storageType = storageType;
            return this;
        }

        public Builder accessMode(AccessModeType accessMode) {
            this.accessMode = accessMode;
            return this;
        }

        public Builder amountBytes(long amountBytes) {
            this.amountBytes = amountBytes;
            return this;
        }

        public Builder persisted(boolean persisted) {
            this.persisted = persisted;
            return this;
        }

        public Builder unshared(boolean unshared) {
            this.unshared = unshared;
            return this;
        }

        public Builder readOnly(boolean readOnly) {
            this.readOnly = readOnly;
            return this;
        }

        public Builder usedAmountBytes(long usedAmountBytes) {
            this.usedAmountBytes = usedAmountBytes;
            return this;
        }

        public Builder hostpathInfo(List<HostpathTopological> hostpathInfo) {
            this.hostpathInfo = hostpathInfo;
            return this;
        }

        public Builder createdBy(String createdBy) {
            this.createdBy = createdBy;
            return this;
        }

        public Builder createdOn(Date createdOn) {
            this.createdOn = createdOn;
            return this;
        }

        public Builder modifiedBy(String modifiedBy) {
            this.modifiedBy = modifiedBy;
            return this;
        }

        public Builder modifiedOn(Date modifiedOn) {
            this.modifiedOn = modifiedOn;
            return this;
        }

        public Builder relatedWorkloadList(List<StorageWorkload> relatedWorkloadList) {
            this.relatedWorkloadList = relatedWorkloadList;
            return this;
        }

        public Builder pvcName(String pvcName) {
            this.pvcName = pvcName;
            return this;
        }

        public Builder status(Storage.Status status) {
            this.status = status;
            return this;
        }

        public StorageTopologicalDeprecated build() {
            StorageTopologicalDeprecated dto = new StorageTopologicalDeprecated();

            dto.setId(id);
            dto.setNamespaceName(namespaceName);
            dto.setStorageName(storageName);
            dto.setStorageType(storageType);
            dto.setAccessMode(accessMode);
            dto.setAmountBytes(amountBytes);
            dto.setUsedAmountBytes(usedAmountBytes);

            dto.setPersisted(persisted);
            dto.setUnshared(unshared);
            dto.setReadOnly(readOnly);
            dto.setMounted(CollectionUtils.isEmpty(relatedWorkloadList));
            dto.setStatus(status);
            if (pvcName != null && storageType == StorageClass && relatedWorkloadList != null) {
                for (StorageWorkload workload : relatedWorkloadList) {
                    if (workload.getWorkloadType() == WorkloadType.StatefulSet) {
                        String workloadName = workload.getWorkloadName();
                        int i = pvcName.lastIndexOf('-');
                        if (i > 0) {
                            String podName = workloadName + pvcName.substring(i);
                            dto.setRelatedPodName(podName);
                        }
                    }
                }
            }

            if (hostpathInfo != null) {
                dto.setHostpathInfo(hostpathInfo);
                dto.setUsedAmountBytes(sum(hostpathInfo));
            } else {
                dto.setHostpathInfo(new ArrayList<>());
            }

            dto.setRelatedWorkloadList(relatedWorkloadList);

            if (relatedWorkloadList != null && (storageType != HostPath || !unshared)) {
                List<StorageWorkload> list = new ArrayList<>();
                for (StorageWorkload storageWorkload : relatedWorkloadList) {
                    if (storageWorkload.getWorkloadType() == WorkloadType.Deployment
                            // this is stupid, but the frontend need set statefulset to deployment list for his lazy
                            ||storageWorkload.getWorkloadType() == WorkloadType.StatefulSet) {
                        list.add(storageWorkload);
                    }
                }

                dto.setRelatedDeploymentList(list);
            }

            dto.setCreatedBy(createdBy);
            dto.setCreatedOn(createdOn);
            dto.setModifiedBy(modifiedBy);
            dto.setModifiedOn(modifiedOn);

            return dto;
        }

        private long sum(List<HostpathTopological> list) {
            long sum = 0;
            for (HostpathTopological dto : list) {
                sum += dto.getUsedBytes();
            }

            return sum;
        }
    }
}
