package cc.backend.kubernetes.apps.dto;

import cc.backend.common.utils.DateUtils;
import io.fabric8.kubernetes.api.model.Container;
import io.fabric8.kubernetes.api.model.LabelSelector;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.PodSpec;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.DeploymentSpec;
import io.fabric8.kubernetes.api.model.extensions.RollingUpdateDeployment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/11/09.
 */
public class DeploymentTopological {
    private String name;
    private String namespace;
    private long age;
    private int replicas;
    private Map<String, String> labels;
    private LabelSelector selector;
    private String strategyType;
    private RollingUpdateDeployment rollingUpdateStrategy;
    private List<String> images;
    private Date createdOn;
    private List<PodTopological> pods;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public int getReplicas() {
        return replicas;
    }

    public void setReplicas(int replicas) {
        this.replicas = replicas;
    }

    public LabelSelector getSelector() {
        return selector;
    }

    public void setSelector(LabelSelector selector) {
        this.selector = selector;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    public void setLabels(Map<String, String> labels) {
        this.labels = labels;
    }

    public String getStrategyType() {
        return strategyType;
    }

    public void setStrategyType(String strategyType) {
        this.strategyType = strategyType;
    }

    public RollingUpdateDeployment getRollingUpdateStrategy() {
        return rollingUpdateStrategy;
    }

    public void setRollingUpdateStrategy(RollingUpdateDeployment rollingUpdateStrategy) {
        this.rollingUpdateStrategy = rollingUpdateStrategy;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public List<PodTopological> getPods() {
        return pods;
    }

    public void setPods(List<PodTopological> pods) {
        this.pods = pods;
    }

    @Override
    public String toString() {
        return "DeploymentTopological{" +
                "name='" + name + '\'' +
                ", namespace='" + namespace + '\'' +
                ", age=" + age +
                ", replicas=" + replicas +
                ", labels=" + labels +
                ", selector=" + selector +
                ", strategyType='" + strategyType + '\'' +
                ", rollingUpdateStrategy=" + rollingUpdateStrategy +
                ", images=" + images +
                ", createdOn=" + createdOn +
                ", pods=" + pods +
                '}';
    }

    public static List<DeploymentTopological> from(List<Deployment> deployments,
                                                   Map<Deployment, List<PodTopological>> deploymentPodsMap) {
        if (deployments == null) {
            return new ArrayList<>();
        }

        return deployments.stream()
                .map(deployment -> from(deployment, deploymentPodsMap.get(deployment)))
                .collect(Collectors.toList());
    }

    public static DeploymentTopological from(Deployment deployment, List<PodTopological> podTopoList) {
        DeploymentTopological deploymentTopological = new DeploymentTopological();

        ObjectMeta metadata = deployment.getMetadata();
        DeploymentSpec spec = deployment.getSpec();
        PodSpec podSpec = spec.getTemplate().getSpec();

        deploymentTopological.setName(metadata.getName());
        deploymentTopological.setNamespace(metadata.getNamespace());
        deploymentTopological.setCreatedOn(DateUtils.parseK8sDate(metadata.getCreationTimestamp()));
        deploymentTopological.setAge(DateUtils.getAge(deploymentTopological.getCreatedOn()));
        deploymentTopological.setReplicas(spec.getReplicas());
        deploymentTopological.setLabels(metadata.getLabels());
        deploymentTopological.setSelector(spec.getSelector());
        deploymentTopological.setStrategyType(spec.getStrategy().getType());
        deploymentTopological.setRollingUpdateStrategy(spec.getStrategy().getRollingUpdate());
        deploymentTopological.setImages(podSpec.getContainers().stream().map(Container::getImage).collect(Collectors.toList()));
        deploymentTopological.setPods(podTopoList);

        return deploymentTopological;
    }
}
