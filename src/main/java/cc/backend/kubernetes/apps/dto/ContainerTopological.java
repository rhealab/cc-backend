package cc.backend.kubernetes.apps.dto;

import cc.backend.common.utils.DateUtils;
import cc.backend.kubernetes.Constants;
import cc.backend.kubernetes.utils.UnitUtils;
import io.fabric8.kubernetes.api.model.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/**
 * Created by xzy on 2017/2/22.
 */
public class ContainerTopological {
    private String name;
    private String image;
    private String status;
    private Date startTime;
    private List<EnvVar> env;

    private double cpuLimits;
    private double cpuRequests;
    private long memoryLimitsBytes;
    private long memoryRequestsBytes;

    private List<ContainerPort> ports;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public List<EnvVar> getEnv() {
        return env;
    }

    public void setEnv(List<EnvVar> env) {
        this.env = env;
    }

    public double getCpuLimits() {
        return cpuLimits;
    }

    public void setCpuLimits(double cpuLimits) {
        this.cpuLimits = cpuLimits;
    }

    public double getCpuRequests() {
        return cpuRequests;
    }

    public void setCpuRequests(double cpuRequests) {
        this.cpuRequests = cpuRequests;
    }

    public long getMemoryLimitsBytes() {
        return memoryLimitsBytes;
    }

    public void setMemoryLimitsBytes(long memoryLimitsBytes) {
        this.memoryLimitsBytes = memoryLimitsBytes;
    }

    public long getMemoryRequestsBytes() {
        return memoryRequestsBytes;
    }

    public void setMemoryRequestsBytes(long memoryRequestsBytes) {
        this.memoryRequestsBytes = memoryRequestsBytes;
    }

    public List<ContainerPort> getPorts() {
        return ports;
    }

    public void setPorts(List<ContainerPort> ports) {
        this.ports = ports;
    }

    @Override
    public String toString() {
        return "ContainerTopological{" +
                "name='" + name + '\'' +
                ", image='" + image + '\'' +
                ", status='" + status + '\'' +
                ", startTime=" + startTime +
                ", env=" + env +
                ", cpuLimits=" + cpuLimits +
                ", cpuRequests=" + cpuRequests +
                ", memoryLimitsBytes=" + memoryLimitsBytes +
                ", memoryRequestsBytes=" + memoryRequestsBytes +
                ", ports=" + ports +
                '}';
    }

    public static List<ContainerTopological> from(List<ContainerStatus> statusList, List<Container> containers) {
        if (containers == null) {
            return new ArrayList<>();
        }

        if (statusList == null) {
            statusList = new ArrayList<>();
        }

        Map<String, ContainerStatus> statusMap = statusList.stream()
                .filter(status -> status.getState() != null)
                .collect(Collectors.toMap(ContainerStatus::getName, status -> status));

        return containers.stream()
                .map(container -> from(statusMap.get(container.getName()), container))
                .collect(toList());
    }

    public static ContainerTopological from(ContainerStatus status, Container container) {
        ContainerTopological containerTopological = new ContainerTopological();
        containerTopological.setName(container.getName());
        containerTopological.setImage(container.getImage());
        if (status != null) {
            if (status.getState().getRunning() != null) {
                containerTopological.setStatus("running");
                containerTopological.setStartTime(DateUtils.parseK8sDate(status.getState().getRunning().getStartedAt()));
            }
            if (status.getState().getWaiting() != null) {
                containerTopological.setStatus("waiting");
            }

            if (status.getState().getTerminated() != null) {
                containerTopological.setStatus("terminated");
            }
        }
        containerTopological.setEnv(container.getEnv());
        Map<String, Quantity> limits = container.getResources().getLimits();
        if (limits != null) {
            containerTopological.setCpuLimits(UnitUtils.parseK8sCpuQuantity(limits.get(Constants.K_CPU)));
            containerTopological.setMemoryLimitsBytes(UnitUtils.parseK8sMemoryQuantity(limits.get(Constants.K_MEMORY)));
        }

        Map<String, Quantity> requests = container.getResources().getRequests();
        if (requests != null) {
            containerTopological.setCpuRequests(UnitUtils.parseK8sCpuQuantity(requests.get(Constants.K_CPU)));
            containerTopological.setMemoryRequestsBytes(UnitUtils.parseK8sMemoryQuantity(requests.get(Constants.K_MEMORY)));
        }

        containerTopological.setPorts(container.getPorts());

        return containerTopological;
    }
}
