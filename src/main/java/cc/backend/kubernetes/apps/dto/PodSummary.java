package cc.backend.kubernetes.apps.dto;

import cc.backend.common.utils.DateUtils;
import cc.backend.kubernetes.utils.UnitUtils;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.fabric8.kubernetes.api.model.*;
import org.springframework.util.CollectionUtils;

import java.util.*;

import static cc.backend.kubernetes.Constants.K_CPU;
import static cc.backend.kubernetes.Constants.K_MEMORY;

/**
 * Created by xzy on 2017/2/22.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PodSummary {
    //common
    private String name;
    private String uid;
    private String namespace;
    private String status;
    private long age;
    private Map<String, String> labels;
    private Date startTime;
    private Date createdOn;

    //network
    private String podIP;
    private String nodeName;
    private List<Integer> targetPorts;
    private List<String> endpoints;

    //resources
    private double cpuLimits;
    private double cpuRequests;
    private long memoryLimitsBytes;
    private long memoryRequestsBytes;
    //TODO do we need storage bytes? Because the storage list already exists!!!
    private long localStorageRequestsBytes;
    private long remoteStorageRequestsBytes;

    private List<PodContainerSummary> containers;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    public void setLabels(Map<String, String> labels) {
        this.labels = labels;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getPodIP() {
        return podIP;
    }

    public void setPodIP(String podIP) {
        this.podIP = podIP;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public List<Integer> getTargetPorts() {
        return targetPorts;
    }

    public void setTargetPorts(List<Integer> targetPorts) {
        this.targetPorts = targetPorts;
    }

    public List<String> getEndpoints() {
        return endpoints;
    }

    public void setEndpoints(List<String> endpoints) {
        this.endpoints = endpoints;
    }

    public double getCpuLimits() {
        return cpuLimits;
    }

    public void setCpuLimits(double cpuLimits) {
        this.cpuLimits = cpuLimits;
    }

    public double getCpuRequests() {
        return cpuRequests;
    }

    public void setCpuRequests(double cpuRequests) {
        this.cpuRequests = cpuRequests;
    }

    public long getMemoryLimitsBytes() {
        return memoryLimitsBytes;
    }

    public void setMemoryLimitsBytes(long memoryLimitsBytes) {
        this.memoryLimitsBytes = memoryLimitsBytes;
    }

    public long getMemoryRequestsBytes() {
        return memoryRequestsBytes;
    }

    public void setMemoryRequestsBytes(long memoryRequestsBytes) {
        this.memoryRequestsBytes = memoryRequestsBytes;
    }

    public long getLocalStorageRequestsBytes() {
        return localStorageRequestsBytes;
    }

    public void setLocalStorageRequestsBytes(long localStorageRequestsBytes) {
        this.localStorageRequestsBytes = localStorageRequestsBytes;
    }

    public long getRemoteStorageRequestsBytes() {
        return remoteStorageRequestsBytes;
    }

    public void setRemoteStorageRequestsBytes(long remoteStorageRequestsBytes) {
        this.remoteStorageRequestsBytes = remoteStorageRequestsBytes;
    }

    public List<PodContainerSummary> getContainers() {
        return containers;
    }

    public void setContainers(List<PodContainerSummary> containers) {
        this.containers = containers;
    }

    @Override
    public String toString() {
        return "PodSummary{" +
                "name='" + name + '\'' +
                ", uid='" + uid + '\'' +
                ", namespace='" + namespace + '\'' +
                ", status='" + status + '\'' +
                ", age=" + age +
                ", labels=" + labels +
                ", startTime=" + startTime +
                ", createdOn=" + createdOn +
                ", podIP='" + podIP + '\'' +
                ", nodeName='" + nodeName + '\'' +
                ", targetPorts=" + targetPorts +
                ", endpoints=" + endpoints +
                ", cpuLimits=" + cpuLimits +
                ", cpuRequests=" + cpuRequests +
                ", memoryLimitsBytes=" + memoryLimitsBytes +
                ", memoryRequestsBytes=" + memoryRequestsBytes +
                ", localStorageRequestsBytes=" + localStorageRequestsBytes +
                ", remoteStorageRequestsBytes=" + remoteStorageRequestsBytes +
                ", containers=" + containers +
                '}';
    }

    public static List<PodSummary> from(List<Pod> pods) {
        List<PodSummary> dtos = new ArrayList<>();

        if (pods != null) {
            for (Pod pod : pods) {
                dtos.add(from(pod));
            }
        }

        return dtos;
    }

    public static PodSummary from(Pod pod) {
        ObjectMeta metadata = pod.getMetadata();
        PodSpec spec = pod.getSpec();
        PodStatus status = pod.getStatus();

        String namespaceName = metadata.getNamespace();

        PodSummary dto = new PodSummary();

        dto.setName(metadata.getName());
        dto.setUid(metadata.getUid());
        dto.setNamespace(namespaceName);
        dto.setStatus(status.getPhase());
        dto.setStartTime(DateUtils.parseK8sDate(status.getStartTime()));
        dto.setAge(DateUtils.getAge(dto.getStartTime()));
        dto.setLabels(metadata.getLabels());
        dto.setCreatedOn(DateUtils.parseK8sDate(metadata.getCreationTimestamp()));

        dto.setPodIP(status.getPodIP());
        dto.setNodeName(spec.getNodeName());
        List<Container> containers = spec.getContainers();
        List<Integer> targetPorts = new ArrayList<>();
        for (Container container : containers) {
            List<ContainerPort> ports = container.getPorts();
            if (ports != null) {
                for (ContainerPort port : ports) {
                    Integer containerPort = port.getContainerPort();
                    if (containerPort != null) {
                        targetPorts.add(containerPort);
                    }
                }
            }
        }
        dto.setTargetPorts(targetPorts);

        composeComputeResources(dto, containers);

        List<ContainerStatus> containerStatuses = status.getContainerStatuses();
        dto.setContainers(PodContainerSummary.from(containerStatuses, containers));

        return dto;
    }

    private static void composeComputeResources(PodSummary dto, List<Container> containers) {
        double cpuLimits = 0;
        double cpuRequests = 0;

        long memoryLimitsBytes = 0;
        long memoryRequestsBytes = 0;

        for (Container container : containers) {
            ResourceRequirements resources = container.getResources();
            Map<String, Quantity> limits = resources.getLimits();
            Map<String, Quantity> requests = resources.getRequests();

            cpuLimits += UnitUtils.parseK8sCpuQuantity(limits.get(K_CPU));
            cpuRequests += UnitUtils.parseK8sCpuQuantity(requests.get(K_CPU));
            memoryLimitsBytes += UnitUtils.parseK8sMemoryQuantity(limits.get(K_MEMORY));
            memoryRequestsBytes += UnitUtils.parseK8sMemoryQuantity(requests.get(K_MEMORY));
        }

        dto.setCpuLimits(cpuLimits);
        dto.setCpuRequests(cpuRequests);
        dto.setMemoryLimitsBytes(memoryLimitsBytes);
        dto.setMemoryRequestsBytes(memoryRequestsBytes);
    }


    /**
     * @param pods not null
     * @return composed pod list
     */
    public static List<PodSummary> from(List<Pod> pods, List<Endpoints> endpointsList) {
        List<PodSummary> dtos = new ArrayList<>();

        Map<String, List<String>> map = new HashMap<>();

        for (Endpoints endpoints : endpointsList) {
            String name = endpoints.getMetadata().getName();
            List<EndpointSubset> subsets = endpoints.getSubsets();
            if (!CollectionUtils.isEmpty(subsets)) {
                for (EndpointSubset subset : subsets) {
                    List<EndpointAddress> addresses = subset.getAddresses();
                    List<EndpointAddress> notReadyAddresses = subset.getNotReadyAddresses();

                    if (!CollectionUtils.isEmpty(addresses)) {
                        for (EndpointAddress address : addresses) {
                            ObjectReference targetRef = address.getTargetRef();
                            if (targetRef != null && targetRef.getUid() != null) {
                                List<String> names = map.computeIfAbsent(targetRef.getUid(), k -> new ArrayList<>());
                                names.add(name);
                            }
                        }
                    }

                    if (!CollectionUtils.isEmpty(notReadyAddresses)) {
                        for (EndpointAddress address : notReadyAddresses) {
                            ObjectReference targetRef = address.getTargetRef();
                            if (targetRef != null && targetRef.getUid() != null) {
                                List<String> names = map.computeIfAbsent(targetRef.getUid(), k -> new ArrayList<>());
                                names.add(name);
                            }
                        }
                    }
                }
            }
        }
        if (pods != null) {
            for (Pod pod : pods) {
                dtos.add(from(pod, map.get(pod.getMetadata().getUid())));
            }
        }

        return dtos;
    }

    /**
     * @param pod not null
     * @return composed pod list
     */
    public static PodSummary from(Pod pod, List<String> endpoints) {
        ObjectMeta metadata = pod.getMetadata();
        PodSpec spec = pod.getSpec();
        PodStatus status = pod.getStatus();

        String namespaceName = metadata.getNamespace();

        PodSummary dto = new PodSummary();

        dto.setName(metadata.getName());
        dto.setUid(metadata.getUid());
        dto.setNamespace(namespaceName);
        dto.setStatus(status.getPhase());
        dto.setStartTime(DateUtils.parseK8sDate(status.getStartTime()));
        dto.setAge(DateUtils.getAge(dto.getCreatedOn()));
        dto.setLabels(metadata.getLabels());
        dto.setCreatedOn(DateUtils.parseK8sDate(metadata.getCreationTimestamp()));

        dto.setPodIP(status.getPodIP());
        dto.setNodeName(spec.getNodeName());
        List<Container> containers = spec.getContainers();
        List<Integer> targetPorts = new ArrayList<>();
        for (Container container : containers) {
            List<ContainerPort> ports = container.getPorts();
            if (ports != null) {
                for (ContainerPort port : ports) {
                    Integer containerPort = port.getContainerPort();
                    if (containerPort != null) {
                        targetPorts.add(containerPort);
                    }
                }
            }
        }
        dto.setTargetPorts(targetPorts);
        dto.setEndpoints(endpoints);

        composeComputeResources(dto, containers);

        List<ContainerStatus> containerStatuses = status.getContainerStatuses();
        dto.setContainers(PodContainerSummary.from(containerStatuses, containers));

        return dto;
    }
}
