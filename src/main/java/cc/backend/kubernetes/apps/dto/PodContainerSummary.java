package cc.backend.kubernetes.apps.dto;

import cc.backend.common.utils.DateUtils;
import io.fabric8.kubernetes.api.model.Container;
import io.fabric8.kubernetes.api.model.ContainerStateRunning;
import io.fabric8.kubernetes.api.model.ContainerStatus;
import io.fabric8.kubernetes.api.model.EnvVar;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * Created by xzy on 2017/2/22.
 */
public class PodContainerSummary {
    private String name;
    private String image;
    private String status;
    private Date startTime;
    private List<EnvVar> env;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public List<EnvVar> getEnv() {
        return env;
    }

    public void setEnv(List<EnvVar> env) {
        this.env = env;
    }

    @Override
    public String toString() {
        return "ContainerDto{" +
                "name='" + name + '\'' +
                ", image='" + image + '\'' +
                ", status='" + status + '\'' +
                ", startTime=" + startTime +
                ", env=" + env +
                '}';
    }

    public static List<PodContainerSummary> from(List<ContainerStatus> statusList, List<Container> containers) {
        Map<String, ContainerStatus> statusMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(statusList)) {
            for (ContainerStatus status : statusList) {
                statusMap.put(status.getName(), status);
            }
        }

        List<PodContainerSummary> dtos = new ArrayList<>();
        if (containers != null) {
            for (Container container : containers) {
                PodContainerSummary dto = new PodContainerSummary();
                dto.setName(container.getName());
                dto.setImage(container.getImage());
                ContainerStatus status = statusMap.get(container.getName());
                if (status != null && status.getState() != null) {
                    ContainerStateRunning running = status.getState().getRunning();
                    if (running != null) {
                        dto.setStatus("running");
                        dto.setStartTime(DateUtils.parseK8sDate(running.getStartedAt()));
                    }
                    if (status.getState().getWaiting() != null) {
                        dto.setStatus("waiting");
                    }

                    if (status.getState().getTerminated() != null) {
                        dto.setStatus("terminated");
                    }
                }
                dto.setEnv(container.getEnv());
                dtos.add(dto);
            }
        }

        return dtos;
    }
}
