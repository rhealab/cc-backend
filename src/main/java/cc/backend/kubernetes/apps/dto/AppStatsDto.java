package cc.backend.kubernetes.apps.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by xzy on 2017/2/27.
 */
public class AppStatsDto {
    private String appName;
    private double cpuRequest;
    private double cpuLimit;
    private long memoryRequestBytes;
    private long memoryLimitBytes;
    private long localStorageBytes;
    private long localStorageUsedBytes;

    @JsonProperty("remoteStorageBytes")
    private long cephRbdStorageBytes;
    @JsonProperty("remoteStorageUsedBytes")
    private long cephRbdStorageUsedBytes;
    private long cephFsUsedBytes;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public double getCpuRequest() {
        return cpuRequest;
    }

    public void setCpuRequest(double cpuRequest) {
        this.cpuRequest = cpuRequest;
    }

    public double getCpuLimit() {
        return cpuLimit;
    }

    public void setCpuLimit(double cpuLimit) {
        this.cpuLimit = cpuLimit;
    }

    public long getMemoryRequestBytes() {
        return memoryRequestBytes;
    }

    public void setMemoryRequestBytes(long memoryRequestBytes) {
        this.memoryRequestBytes = memoryRequestBytes;
    }

    public long getMemoryLimitBytes() {
        return memoryLimitBytes;
    }

    public void setMemoryLimitBytes(long memoryLimitBytes) {
        this.memoryLimitBytes = memoryLimitBytes;
    }

    public long getLocalStorageBytes() {
        return localStorageBytes;
    }

    public void setLocalStorageBytes(long localStorageBytes) {
        this.localStorageBytes = localStorageBytes;
    }

    public long getLocalStorageUsedBytes() {
        return localStorageUsedBytes;
    }

    public void setLocalStorageUsedBytes(long localStorageUsedBytes) {
        this.localStorageUsedBytes = localStorageUsedBytes;
    }

    public long getCephRbdStorageBytes() {
        return cephRbdStorageBytes;
    }

    public void setCephRbdStorageBytes(long cephRbdStorageBytes) {
        this.cephRbdStorageBytes = cephRbdStorageBytes;
    }

    public long getCephRbdStorageUsedBytes() {
        return cephRbdStorageUsedBytes;
    }

    public void setCephRbdStorageUsedBytes(long cephRbdStorageUsedBytes) {
        this.cephRbdStorageUsedBytes = cephRbdStorageUsedBytes;
    }

    public long getCephFsUsedBytes() {
        return cephFsUsedBytes;
    }

    public void setCephFsUsedBytes(long cephFsUsedBytes) {
        this.cephFsUsedBytes = cephFsUsedBytes;
    }

    @Override
    public String toString() {
        return "AppStatsDto{" +
                "appName='" + appName + '\'' +
                ", cpuRequest=" + cpuRequest +
                ", cpuLimit=" + cpuLimit +
                ", memoryRequestBytes=" + memoryRequestBytes +
                ", memoryLimitBytes=" + memoryLimitBytes +
                ", localStorageBytes=" + localStorageBytes +
                ", localStorageUsedBytes=" + localStorageUsedBytes +
                ", cephRbdStorageBytes=" + cephRbdStorageBytes +
                ", cephRbdStorageUsedBytes=" + cephRbdStorageUsedBytes +
                ", cephFsUsedBytes=" + cephFsUsedBytes +
                '}';
    }

    public void addCpuRequest(double addedBytes) {
        cpuRequest += addedBytes;
    }

    public void addCpuLimit(double addedBytes) {
        cpuLimit += addedBytes;
    }

    public void addMemoryRequest(long addedBytes) {
        memoryRequestBytes += addedBytes;
    }

    public void addMemoryLimit(long addedBytes) {
        memoryLimitBytes += addedBytes;
    }
}
