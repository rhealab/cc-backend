package cc.backend.kubernetes.apps.dto;

import cc.backend.kubernetes.storage.domain.HostPathUsage;
import cc.backend.kubernetes.storage.domain.WorkloadType;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author wangchunyang@gmail.com
 */
public class HostpathTopological {
    private long amountBytes;
    private long usedBytes;
    private String podName;
    private WorkloadType workloadType;
    private String workloadName;

    public long getAmountBytes() {
        return amountBytes;
    }

    public void setAmountBytes(long amountBytes) {
        this.amountBytes = amountBytes;
    }

    public long getUsedBytes() {
        return usedBytes;
    }

    public void setUsedBytes(long usedBytes) {
        this.usedBytes = usedBytes;
    }

    public String getPodName() {
        return podName;
    }

    public void setPodName(String podName) {
        this.podName = podName;
    }

    public WorkloadType getWorkloadType() {
        return workloadType;
    }

    public void setWorkloadType(WorkloadType workloadType) {
        this.workloadType = workloadType;
    }

    public String getWorkloadName() {
        return workloadName;
    }

    public void setWorkloadName(String workloadName) {
        this.workloadName = workloadName;
    }

    @Override
    public String toString() {
        return "HostpathTopological{" +
                "amountBytes=" + amountBytes +
                ", usedBytes=" + usedBytes +
                ", podName='" + podName + '\'' +
                ", workloadType='" + workloadType + '\'' +
                ", workloadName='" + workloadName + '\'' +
                '}';
    }

    public static List<HostpathTopological> from(List<HostPathUsage.MountInfo> mountInfoList, Map<String, String> podDeploymentMap) {
        List<HostpathTopological> hostpathTopologicalList = new ArrayList<>();
        if (mountInfoList != null) {
            for (HostPathUsage.MountInfo mountInfo : mountInfoList) {
                hostpathTopologicalList.add(from(mountInfo, podDeploymentMap));
            }
        }

        return hostpathTopologicalList;
    }

    public static HostpathTopological from(HostPathUsage.MountInfo mountInfo, Map<String, String> podDeploymentMap) {
        if (mountInfo == null) {
            return null;
        }
        HostpathTopological dto = new HostpathTopological();

        if (mountInfo.getPodInfo() != null && !StringUtils.isEmpty(mountInfo.getPodInfo().getInfo())) {
            String info = mountInfo.getPodInfo().getInfo();
            String[] infos = info.split(":");
            if (infos.length >= 3) {
                dto.setPodName(infos[1]);
            }
        }

        dto.setUsedBytes(mountInfo.getVolumeCurrentSize());
        dto.setAmountBytes(mountInfo.getVolumeQuotaSize());
        if (dto.getPodName() != null) {
            dto.setWorkloadType(WorkloadType.Deployment);
            dto.setWorkloadName(podDeploymentMap.getOrDefault(dto.getPodName(), ""));
        }

        return dto;
    }
}
