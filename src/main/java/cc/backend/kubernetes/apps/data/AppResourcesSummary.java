package cc.backend.kubernetes.apps.data;

import java.util.List;
import java.util.Map;

/**
 * @author wangchunyang@gmail.com
 */
public class AppResourcesSummary {
    private App app;
    private List<Map> services;
    private List<Map> deployments;

    public App getApp() {
        return app;
    }

    public void setApp(App app) {
        this.app = app;
    }

    public void setServices(List<Map> services) {
        this.services = services;
    }

    public List<Map> getServices() {
        return services;
    }

    public void setDeployments(List<Map> deployments) {
        this.deployments = deployments;
    }

    public List<Map> getDeployments() {
        return deployments;
    }
}
