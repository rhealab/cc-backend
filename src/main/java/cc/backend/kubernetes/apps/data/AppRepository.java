package cc.backend.kubernetes.apps.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author wangchunyang@gmail.com
 */
public interface AppRepository extends JpaRepository<App, Long> {

    /**
     * find a namespace's all app
     *
     * @param namespaceName the namespace for find
     * @return app list
     */
    List<App> findByNamespace(String namespaceName);

    /**
     * find a app in namespace
     *
     * @param namespaceName the namespace name
     * @param name          the app name
     * @return the app
     */
    App findByNamespaceAndName(String namespaceName, String name);

    /**
     * list a namespace's all app's name
     *
     * @param namespaceName the namespace name
     * @return the app name list
     */
    @Query(value = "SELECT a.name FROM App a WHERE a.namespace=:namespaceName")
    List<String> getAppsName(@Param("namespaceName") String namespaceName);

    /**
     * delete a app
     *
     * @param namespaceName the namespace name
     * @param name          the app name
     * @return the deleted app's id
     */
    @Transactional(rollbackFor = Exception.class)
    Long deleteByNamespaceAndName(String namespaceName, String name);

    /**
     * delete a namespace's all apps
     *
     * @param namespaceName the namespace for delete
     */
    @Transactional(rollbackFor = Exception.class)
    void deleteByNamespace(String namespaceName);

    /**
     * count a namespace's app number
     *
     * @param namespaceName the namespace name
     * @return the number of app
     */
    long countByNamespace(String namespaceName);
}
