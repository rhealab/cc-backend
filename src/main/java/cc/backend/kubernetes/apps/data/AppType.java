package cc.backend.kubernetes.apps.data;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/6/20.
 */
public enum AppType {
    PREDEFINE, CUSTOM
}
