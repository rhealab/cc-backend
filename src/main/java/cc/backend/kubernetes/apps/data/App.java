package cc.backend.kubernetes.apps.data;

import cc.backend.audit.request.EnnContext;
import cc.backend.kubernetes.Constants;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author wangchunyang@gmail.com
 */

@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"namespace", "name"})
})
public class App {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Pattern(regexp = Constants.RESOURCE_NAME_PATTERN)
    @Size(max = 63)
    private String name;

    private String namespace;

    @Enumerated(EnumType.STRING)
    private AppStatus status = AppStatus.NORMAL;

    @Enumerated(EnumType.STRING)
    private AppType type;

    /**
     * predefine app type, like hadoop, kafka.
     * TODO talk with mye
     */
    private String predefineType;
    private String version;
    @Column(length = 8192)
    private String details;

    private Date lastUpdatedOn;
    private String lastUpdatedBy;
    private Date createdOn;
    private String createdBy;

    @PreUpdate
    public void onUpdate() {
        lastUpdatedBy = EnnContext.getUserId();
        lastUpdatedOn = new Date();
    }

    @PrePersist
    public void onCreate() {
        createdBy = EnnContext.getUserId();
        createdOn = new Date();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public AppStatus getStatus() {
        return status;
    }

    public void setStatus(AppStatus status) {
        this.status = status;
    }

    public AppType getType() {
        return type;
    }

    public void setType(AppType type) {
        this.type = type;
    }

    public String getPredefineType() {
        return predefineType;
    }

    public void setPredefineType(String predefineType) {
        this.predefineType = predefineType;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Date getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(Date lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public String toString() {
        return "App{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", namespace='" + namespace + '\'' +
                ", status=" + status +
                ", type=" + type +
                ", predefineType='" + predefineType + '\'' +
                ", lastUpdatedOn=" + lastUpdatedOn +
                ", lastUpdatedBy='" + lastUpdatedBy + '\'' +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                '}';
    }
}
