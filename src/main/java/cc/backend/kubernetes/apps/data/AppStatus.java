package cc.backend.kubernetes.apps.data;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/20.
 */
public enum AppStatus {
    NORMAL, CREATING, CREATE_FAILED, DELETE_FAILED, CREATE_SUCCESS, NEW, ABNORMAL, DELETING, DELETED, WARNING, TIMEOUT
}
