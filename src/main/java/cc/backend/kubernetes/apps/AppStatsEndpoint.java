package cc.backend.kubernetes.apps;

import cc.backend.kubernetes.apps.dto.AppStatsDto;
import cc.backend.kubernetes.apps.services.AppStatsManagement;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author wangchunyang@gmail.com
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes/namespaces/{namespaceName}/app_stats")
@Api(value = "App", description = "Info about apps.", produces = "application/json")
public class AppStatsEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(AppStatsEndpoint.class);

    @Inject
    private AppStatsManagement appStatsManagement;

    @GET
    @Path("{appName}")
    @ApiOperation(value = "Get app stats.", response = AppStatsDto.class)
    public Response getAppStats(@PathParam("namespaceName") String nsName, @PathParam("appName") String appName) {
        AppStatsDto appStats = appStatsManagement.getAppStats(nsName, appName);
        return Response.ok(appStats).build();
    }
}
