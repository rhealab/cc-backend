package cc.backend.kubernetes.apps;

import cc.backend.kubernetes.apps.data.App;
import cc.backend.kubernetes.apps.data.AppResourcesSummary;
import cc.backend.kubernetes.apps.data.AppStatus;
import cc.backend.kubernetes.apps.dto.AppResourcesDetails;
import cc.backend.kubernetes.apps.dto.AppResourcesTopological;
import cc.backend.kubernetes.apps.dto.AppResourcesTopologicalDeprecated;
import cc.backend.kubernetes.apps.dto.AppTopologicalFrontend;
import cc.backend.kubernetes.apps.services.AppManagement;
import cc.backend.kubernetes.storage.domain.Storage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * @author wangchunyang@gmail.com
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes/namespaces/{namespaceName}/apps")
@Api(value = "App", description = "Info about apps.", produces = "application/json")
public class AppsEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(AppsEndpoint.class);

    @Inject
    private AppManagement management;

    @GET
    @ApiOperation(value = "List apps in namespace.", responseContainer = "List", response = App.class)
    public Response getApps(@PathParam("namespaceName") String namespaceName) {
        List<App> apps = management.getAllApps(namespaceName);
        return Response.ok(apps).build();
    }

    @GET
    @Path("{appName}")
    @ApiOperation(value = "Get app info.", response = App.class)
    public Response getApp(@PathParam("namespaceName") String namespaceName,
                           @PathParam("appName") String appName) {
        App app = management.getApp(namespaceName, appName);
        return Response.ok(app).build();
    }

    @GET
    @Path("{appName}/storage")
    @ApiOperation(value = "List storage in app.", responseContainer = "List", response = Storage.class)
    public Response getAppStorageList(@PathParam("namespaceName") String namespaceName,
                                      @PathParam("appName") String appName) {
        List<Storage> storageList = management.getAppStorageList(namespaceName, appName);
        return Response.ok(storageList).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Create app.", response = App.class)
    public Response createApp(@PathParam("namespaceName") String namespaceName,
                              @Valid App app) {
        App result = management.createApp(app, namespaceName);
        return Response.ok(result).build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Update app status.")
    @Path("{appName}/status/{status}")
    public Response updateAppStatus(@PathParam("namespaceName") String namespaceName,
                                    @PathParam("appName") String app,
                                    @PathParam("status") AppStatus status,
                                    String details) {
        management.updateAppStatus(namespaceName, app, status, details);
        return Response.ok().build();
    }

    @DELETE
    @Path("{appName}")
    @ApiOperation(value = "Delete app.", responseContainer = "Map")
    public Response deleteApp(@PathParam("appName") String appName,
                              @PathParam("namespaceName") String namespaceName) {
        Map resultMap = management.deleteApp(namespaceName, appName);
        logger.info("Delete app: name={}", appName);
        return Response.ok(resultMap).build();
    }

    @GET
    @Path("{appName}/resources/details")
    @ApiOperation(value = "Get app resources detail.", response = AppResourcesDetails.class)
    public Response getResourcesDetails(@PathParam("namespaceName") String namespaceName,
                                        @PathParam("appName") String appName) {
        AppResourcesDetails resources = management.getResourcesDetails(namespaceName, appName);
        return Response.ok(resources).build();
    }

    @GET
    @Path("{appName}/resources/summary")
    @ApiOperation(value = "Get app resources summary.", response = AppResourcesSummary.class)
    public Response getResourcesSummary(@PathParam("namespaceName") String namespaceName,
                                        @PathParam("appName") String appName) {
        AppResourcesSummary resources = management.getResourcesSummary(namespaceName, appName);
        return Response.ok(resources).build();
    }

    @GET
    @Path("{appName}/resources/topological")
    @ApiOperation(value = "Get app resources topological.", response = AppResourcesTopological.class)
    public Response getResourcesTopological(@PathParam("namespaceName") String namespaceName,
                                            @PathParam("appName") String appName) {
        AppResourcesTopological topological = management.getResourcesTopological(namespaceName, appName);
        return Response.ok(topological).build();
    }

    @GET
    @Path("{appName}/resources/topological2")
    @Deprecated
    @ApiOperation(value = "Deprecated.", response = AppResourcesTopologicalDeprecated.class)
    public Response getResourcesTopologicalDeprecated2(@PathParam("namespaceName") String namespaceName,
                                                       @PathParam("appName") String appName) {
        AppResourcesTopologicalDeprecated topological = management.getResourcesTopologicalDeprecated(namespaceName, appName);
        return Response.ok(topological).build();
    }

    @GET
    @Path("{appName}/resources/topological3")
    @Deprecated
    @ApiOperation(value = "Deprecated.", response = AppTopologicalFrontend.class)
    public Response getResourcesTopologicalDeprecated3(@PathParam("namespaceName") String namespaceName,
                                                       @PathParam("appName") String appName) {
        AppResourcesTopologicalDeprecated topologicalDeprecated = management.getResourcesTopologicalDeprecated(namespaceName, appName);
        AppResourcesTopological topological = management.getResourcesTopological(namespaceName, appName);
        return Response.ok(AppTopologicalFrontend.from(topological, topologicalDeprecated)).build();
    }
}
