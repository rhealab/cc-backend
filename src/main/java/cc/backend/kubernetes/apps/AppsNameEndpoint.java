package cc.backend.kubernetes.apps;

import cc.backend.kubernetes.apps.services.AppNamesManagement;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author yanzhixiang on 17-6-22.
 */
@Component
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes/namespaces/{namespaceName}/apps_name")
@Api(value = "App", description = "Info about apps.", produces = "application/json")
public class AppsNameEndpoint {
    @Inject
    private AppNamesManagement management;

    @GET
    @ApiOperation(value = "List apps name in namespace.", responseContainer = "List", response = String.class)
    public Response getAppsName(@PathParam("namespaceName") String namespaceName) {
        List<String> appNames = management.getAppsName(namespaceName);
        return Response.ok(appNames).build();
    }
}
