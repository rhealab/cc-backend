package cc.backend.kubernetes.apps.validator;

import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.kubernetes.apps.data.App;
import cc.backend.kubernetes.apps.data.AppRepository;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * Created by xzy on 2017/2/22.
 *
 * @author yanzhixiang modified on 2017-6-7
 */
@Component
public class AppValidator {
    private static final Logger logger = LoggerFactory.getLogger(AppValidator.class);

    @Inject
    private AppRepository appRepository;

    public App validateAppShouldExists(String namespaceName, String appName) {
        App app = appRepository.findByNamespaceAndName(namespaceName, appName);

        if (app == null) {
            throw new CcException(BackendReturnCodeNameConstants.APP_NOT_EXISTS,
                    ImmutableMap.of("namespace", namespaceName,
                            "app", appName));
        }

        return app;
    }

    public void validateAppShouldNotExists(String namespaceName,
                                           String appName) {
        App app = appRepository.findByNamespaceAndName(namespaceName, appName);
        if (app != null) {
            throw new CcException(BackendReturnCodeNameConstants.APP_ALREADY_EXISTS,
                    ImmutableMap.of("namespace", namespaceName,
                            "app", appName));
        }
    }
}
