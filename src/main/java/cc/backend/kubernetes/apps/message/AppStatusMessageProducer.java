package cc.backend.kubernetes.apps.message;

import cc.backend.RabbitConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

/**
 * Created by mye(Xizhe Ye) on 18-2-1.
 */
@Component
public class AppStatusMessageProducer {
  private static final Logger logger = LoggerFactory.getLogger(AppStatusMessageProducer.class);

  @Inject
  private RabbitMessagingTemplate messagingTemplate;

  public void send(AppStatusMessage message) {
    CompletableFuture.runAsync(() -> {
      logger.info("AppStatusMessage={}", message);
      messagingTemplate.convertAndSend(RabbitConfig.APP_STATUS_Q, message);
    });
  }
}
