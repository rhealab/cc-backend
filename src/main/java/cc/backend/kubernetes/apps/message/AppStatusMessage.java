package cc.backend.kubernetes.apps.message;

import cc.backend.audit.request.EnnContext;
import cc.backend.kubernetes.apps.data.AppStatus;

/**
 * @author mye(Xizhe Ye) on 18-2-1.
 */
public class AppStatusMessage {
    private String userId;
    private String requestId;
    private String namespace;
    private String clusterName;
    private String dataflowName;
    private String instanceName;
    private AppStatus status;
    private String details;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getDataflowName() {
        return dataflowName;
    }

    public void setDataflowName(String dataflowName) {
        this.dataflowName = dataflowName;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    public AppStatus getStatus() {
        return status;
    }

    public void setStatus(AppStatus status) {
        this.status = status;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public static class Builder {
        private String userId;
        private String requestId;
        private String namespace;
        private String dataflowName;
        private String instanceName;
        private AppStatus status;
        private String details;

        public Builder setUserId(String userId) {
            this.userId = userId;
            return this;
        }

        public Builder setRequestId(String requestId) {
            this.requestId = requestId;
            return this;
        }

        public Builder setNamespace(String namespace) {
            this.namespace = namespace;
            return this;
        }

        public Builder setDataflowName(String dataflowName) {
            this.dataflowName = dataflowName;
            return this;
        }

        public Builder setInstanceName(String instanceName) {
            this.instanceName = instanceName;
            return this;
        }

        public Builder setStatus(AppStatus appStatus) {
            this.status = appStatus;
            return this;
        }

        public Builder setDetails(String details) {
            this.details = details;
            return this;
        }

        public AppStatusMessage build() {
            AppStatusMessage m = new AppStatusMessage();
            m.setUserId(userId != null ? userId : EnnContext.getUserId());
            m.setRequestId(requestId != null ? requestId : EnnContext.getRequestId());
            m.setNamespace(namespace != null ? namespace : EnnContext.getNamespaceName());
            m.setClusterName(EnnContext.getClusterName());
            m.setDataflowName(dataflowName);
            m.setInstanceName(instanceName);
            m.setStatus(status);
            m.setDetails(details);
            return m;
        }
    }
}
