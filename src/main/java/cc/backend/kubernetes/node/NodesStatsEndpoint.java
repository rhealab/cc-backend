package cc.backend.kubernetes.node;

import cc.backend.common.Metrics;
import cc.backend.common.MetricsType;
import cc.backend.kubernetes.namespaces.dto.MetricsDto;
import cc.backend.kubernetes.node.services.NodesStatsManagement;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


/**
 * @author wangchunyang@gmail.com
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes/node_stats")
@Api(value = "Node", description = "Info about nodes.", produces = "application/json")
public class NodesStatsEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(NodesStatsEndpoint.class);

    @Inject
    private NodesStatsManagement management;

    @GET
    @Path("{nodeName}/cpu")
    @ApiOperation(value = "Get node cpu current usage info.", response = MetricsDto.class)
    public Response getCpuStats(@PathParam("nodeName") String nodeName) {
        List<Metrics> metrics = management.getComputeMetrics(MetricsType.CPU, nodeName);
        double cpuTotal = management.getNodeTotalCpu(nodeName);
        MetricsDto dto = MetricsDto.from(metrics, cpuTotal, MetricsType.CPU);
        return Response.ok(dto).build();
    }

    @GET
    @Path("{nodeName}/memory")
    @ApiOperation(value = "Get node memory current usage info.", response = MetricsDto.class)
    public Response getMemoryStats(@PathParam("nodeName") String nodeName) {
        List<Metrics> metrics = management.getComputeMetrics(MetricsType.MEMORY, nodeName);
        long memoryTotal = management.getNodeTotalMemory(nodeName);
        MetricsDto dto = MetricsDto.from(metrics, memoryTotal, MetricsType.MEMORY);
        return Response.ok(dto).build();
    }

    @GET
    @Path("{nodeName}/compute")
    @ApiOperation(value = "Get node compute resource current usage info.",
            responseContainer = "List", response = MetricsDto.class)
    public Response getComputeStats(@PathParam("nodeName") String nodeName) {
        return Response.ok(management.getComputeStats(nodeName)).build();
    }
}
