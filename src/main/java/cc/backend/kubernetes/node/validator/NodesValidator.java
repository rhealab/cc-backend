package cc.backend.kubernetes.node.validator;

import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.Node;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/2/23.
 */
@Component
public class NodesValidator {
    @Inject
    private KubernetesClientManager clientManager;

    public Node validateNodeShouldExists(String nodeName) {
        Node node = clientManager.getClient().nodes().withName(nodeName).get();
        if (node == null) {
            throw new CcException(BackendReturnCodeNameConstants.NODE_NOT_EXISTS,
                    ImmutableMap.of("resourceName", nodeName));
        }

        return node;
    }
}