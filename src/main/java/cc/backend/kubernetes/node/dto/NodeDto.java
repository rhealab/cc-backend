package cc.backend.kubernetes.node.dto;

import cc.backend.common.utils.DateUtils;
import cc.backend.kubernetes.node.NodeHelper;
import cc.backend.kubernetes.utils.UnitUtils;
import io.fabric8.kubernetes.api.model.*;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

import static cc.backend.kubernetes.Constants.*;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/2/23.
 */
public class NodeDto {

    private String name;
    private String uid;
    private String status;
    private String ip;
    private int podNum;
    private Date createdOn;
    private long age;
    private double cpuSum;
    private double cpuLimits;
    private double cpuRequests;
    private long memorySumBytes;
    private long memoryLimitsBytes;
    private long memoryRequestsBytes;
    private long localStorageSumBytes;
    private long localStorageRequestsBytes;
    private long localStorageUsageBytes;
    private String role;
    private String os;
    private String kernelVersion;
    private String kubeProxyVersion;
    private String kubeletVersion;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPodNum() {
        return podNum;
    }

    public void setPodNum(int podNum) {
        this.podNum = podNum;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public double getCpuSum() {
        return cpuSum;
    }

    public void setCpuSum(double cpuSum) {
        this.cpuSum = cpuSum;
    }

    public double getCpuLimits() {
        return cpuLimits;
    }

    public void setCpuLimits(double cpuLimits) {
        this.cpuLimits = cpuLimits;
    }

    public double getCpuRequests() {
        return cpuRequests;
    }

    public void setCpuRequests(double cpuRequests) {
        this.cpuRequests = cpuRequests;
    }

    public long getMemorySumBytes() {
        return memorySumBytes;
    }

    public void setMemorySumBytes(long memorySumBytes) {
        this.memorySumBytes = memorySumBytes;
    }

    public long getMemoryLimitsBytes() {
        return memoryLimitsBytes;
    }

    public void setMemoryLimitsBytes(long memoryLimitsBytes) {
        this.memoryLimitsBytes = memoryLimitsBytes;
    }

    public long getMemoryRequestsBytes() {
        return memoryRequestsBytes;
    }

    public void setMemoryRequestsBytes(long memoryRequestsBytes) {
        this.memoryRequestsBytes = memoryRequestsBytes;
    }

    public long getLocalStorageSumBytes() {
        return localStorageSumBytes;
    }

    public void setLocalStorageSumBytes(long localStorageSumBytes) {
        this.localStorageSumBytes = localStorageSumBytes;
    }

    public long getLocalStorageRequestsBytes() {
        return localStorageRequestsBytes;
    }

    public void setLocalStorageRequestsBytes(long localStorageRequestsBytes) {
        this.localStorageRequestsBytes = localStorageRequestsBytes;
    }

    public long getLocalStorageUsageBytes() {
        return localStorageUsageBytes;
    }

    public void setLocalStorageUsageBytes(long localStorageUsageBytes) {
        this.localStorageUsageBytes = localStorageUsageBytes;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getKernelVersion() {
        return kernelVersion;
    }

    public void setKernelVersion(String kernelVersion) {
        this.kernelVersion = kernelVersion;
    }

    public String getKubeProxyVersion() {
        return kubeProxyVersion;
    }

    public void setKubeProxyVersion(String kubeProxyVersion) {
        this.kubeProxyVersion = kubeProxyVersion;
    }

    public String getKubeletVersion() {
        return kubeletVersion;
    }

    public void setKubeletVersion(String kubeletVersion) {
        this.kubeletVersion = kubeletVersion;
    }

    @Override
    public String toString() {
        return "NodeDto{" +
                "name='" + name + '\'' +
                ", uid='" + uid + '\'' +
                ", status='" + status + '\'' +
                ", ip='" + ip + '\'' +
                ", podNum=" + podNum +
                ", createdOn=" + createdOn +
                ", age=" + age +
                ", cpuSum=" + cpuSum +
                ", cpuLimits=" + cpuLimits +
                ", cpuRequests=" + cpuRequests +
                ", memorySumBytes=" + memorySumBytes +
                ", memoryLimitsBytes=" + memoryLimitsBytes +
                ", memoryRequestsBytes=" + memoryRequestsBytes +
                ", localStorageSumBytes=" + localStorageSumBytes +
                ", localStorageRequestsBytes=" + localStorageRequestsBytes +
                ", localStorageUsageBytes=" + localStorageUsageBytes +
                ", role='" + role + '\'' +
                ", os='" + os + '\'' +
                '}';
    }

    public static List<NodeDto> from(List<Node> nodeList,
                                     Map<String, List<Pod>> nodePodsMap,
                                     Map<String, Map<String, PersistentVolumeClaim>> namespacePvcMap,
                                     Map<String, PersistentVolume> pvMap) {
        Map<String, List<PersistentVolumeClaim>> nodePvcMap = nodeList.stream()
                .map(node -> node.getMetadata().getName())
                .collect(Collectors.toMap(name -> name,
                        name -> getPodsPvcList(nodePodsMap.get(name), namespacePvcMap)));

        Map<String, List<PersistentVolume>> nodePvMap = nodeList.stream()
                .map(node -> node.getMetadata().getName())
                .collect(Collectors.toMap(name -> name,
                        name -> getPvList(nodePvcMap.get(name), pvMap)));

        return nodeList.stream()
                .map(node -> {
                    String name = node.getMetadata().getName();
                    return NodeDto.from(node, nodePvMap.get(name), nodePodsMap.get(name));
                })
                .collect(Collectors.toList());
    }

    public static List<PersistentVolumeClaim> getPodsPvcList(List<Pod> podList,
                                                             Map<String, Map<String, PersistentVolumeClaim>> namespacePvcMap) {
        return podList.stream()
                .filter(pod -> !CollectionUtils.isEmpty(pod.getSpec().getVolumes()))
                .map(pod -> getPodPvcList(pod, namespacePvcMap))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private static List<PersistentVolumeClaim> getPodPvcList(Pod pod,
                                                             Map<String, Map<String, PersistentVolumeClaim>> namespacePvcMap) {
        Map<String, PersistentVolumeClaim> pvcMap = namespacePvcMap
                .computeIfAbsent(pod.getMetadata().getNamespace(), s -> new HashMap<>(0));
        return pod.getSpec().getVolumes()
                .stream()
                .filter(volume -> volume.getPersistentVolumeClaim() != null)
                .filter(volume -> volume.getPersistentVolumeClaim().getClaimName() != null)
                .map(volume -> volume.getPersistentVolumeClaim().getClaimName())
                .map(pvcMap::get)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public static List<PersistentVolume> getPvList(List<PersistentVolumeClaim> pvcList,
                                                   Map<String, PersistentVolume> pvMap) {
        return Optional.ofNullable(pvcList)
                .orElseGet(ArrayList::new)
                .stream()
                .map(pvc -> pvc.getSpec().getVolumeName())
                .filter(Objects::nonNull)
                .map(pvMap::get)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }


    public static NodeDto from(Node node, List<PersistentVolume> pvList, List<Pod> podList) {
        NodeDto dto = new NodeDto();

        ObjectMeta metadata = node.getMetadata();
        NodeSpec spec = node.getSpec();
        NodeStatus status = node.getStatus();
        Map<String, String> labels = metadata.getLabels();
        Map<String, Quantity> allocatable = status.getAllocatable();

        dto.setName(metadata.getName());
        dto.setUid(metadata.getUid());
        dto.setStatus(NodeHelper.composeNodeDtoStatus(status));
        dto.setIp(spec.getExternalID());
        dto.setPodNum(podList.size());

        dto.setCreatedOn(DateUtils.parseK8sDate(metadata.getCreationTimestamp()));
        dto.setAge(DateUtils.getAge(dto.getCreatedOn()));
        dto.setRole(NodeHelper.composeNodeDtoRole(labels));

        dto.setOs(status.getNodeInfo().getOsImage());
        dto.setKernelVersion(status.getNodeInfo().getKernelVersion());
        dto.setKubeletVersion(status.getNodeInfo().getKubeletVersion());
        dto.setKubeProxyVersion(status.getNodeInfo().getKubeProxyVersion());

        dto.setCpuSum(UnitUtils.parseK8sCpuQuantity(allocatable.get(K_CPU)));
        dto.setMemorySumBytes(UnitUtils.parseK8sMemoryQuantity(allocatable.get(K_MEMORY)));

        NodeDiskQuotaState diskQuotaState = NodeHelper.getDiskQuotaState(node);
        dto.setLocalStorageSumBytes(diskQuotaState == null ? 0 : diskQuotaState.getCapacity());
        dto.setLocalStorageUsageBytes(diskQuotaState == null ? 0 : diskQuotaState.getCurUseSize());

        long requestBytes = Optional.ofNullable(pvList)
                .orElseGet(ArrayList::new)
                .stream()
                .map(pv -> pv.getSpec().getCapacity())
                .filter(Objects::nonNull)
                .map(capacity -> capacity.get(K_STORAGE))
                .mapToLong(UnitUtils::parseK8sStorageQuantity)
                .sum();
        dto.setLocalStorageRequestsBytes(requestBytes);

        composeComputeResource(dto, podList);

        return dto;
    }

    public static void composeComputeResource(NodeDto dto,
                                              List<Pod> podList) {
        double cpuRequests = 0;
        double cpuLimits = 0;
        long memoryRequestsBytes = 0;
        long memoryLimitsBytes = 0;

        for (Pod pod : podList) {
            String podPhase = pod.getStatus().getPhase();
            if (!POD_PHASE_RUNNING.equals(podPhase)) {
                continue;
            }
            List<Container> containers = Optional.ofNullable(pod.getSpec().getContainers()).orElseGet(ArrayList::new);
            for (Container container : containers) {
                ResourceRequirements resources = container.getResources();
                Map<String, Quantity> requests = resources.getRequests();
                Map<String, Quantity> limits = resources.getLimits();
                if (requests != null) {
                    cpuRequests += UnitUtils.parseK8sCpuQuantity(requests.get(K_CPU));
                    memoryRequestsBytes += UnitUtils.parseK8sMemoryQuantity(requests.get(K_MEMORY));
                }

                if (limits != null) {
                    cpuLimits += UnitUtils.parseK8sCpuQuantity(limits.get(K_CPU));
                    memoryLimitsBytes += UnitUtils.parseK8sMemoryQuantity(limits.get(K_MEMORY));
                }
            }
        }

        dto.setCpuRequests(cpuRequests);
        dto.setCpuLimits(cpuLimits);
        dto.setMemoryRequestsBytes(memoryRequestsBytes);
        dto.setMemoryLimitsBytes(memoryLimitsBytes);
    }
}
