package cc.backend.kubernetes.node.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/26.
 */
public class NodeDiskQuotaState {
    @JsonProperty("Capacity")
    private long capacity;

    @JsonProperty("CurUseSize")
    private long curUseSize;

    @JsonProperty("CurQuotaSize")
    private long curQuotaSize;

    @JsonProperty("AvaliableSize")
    private String availableSize;

    @JsonProperty("Disabled")
    private boolean disabled;

    @JsonProperty("DiskStatus")
    private List<DiskStatus> diskStatus;

    public long getCapacity() {
        return capacity;
    }

    public void setCapacity(long capacity) {
        this.capacity = capacity;
    }

    public long getCurUseSize() {
        return curUseSize;
    }

    public void setCurUseSize(long curUseSize) {
        this.curUseSize = curUseSize;
    }

    public long getCurQuotaSize() {
        return curQuotaSize;
    }

    public void setCurQuotaSize(long curQuotaSize) {
        this.curQuotaSize = curQuotaSize;
    }

    public String getAvailableSize() {
        return availableSize;
    }

    public void setAvailableSize(String availableSize) {
        this.availableSize = availableSize;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public List<DiskStatus> getDiskStatus() {
        return diskStatus;
    }

    public void setDiskStatus(List<DiskStatus> diskStatus) {
        this.diskStatus = diskStatus;
    }

    @Override
    public String toString() {
        return "NodeDiskQuotaState{" +
                "capacity=" + capacity +
                ", curUseSize=" + curUseSize +
                ", curQuotaSize=" + curQuotaSize +
                ", availableSize='" + availableSize + '\'' +
                ", disabled=" + disabled +
                ", diskStatus=" + diskStatus +
                '}';
    }

    public static class DiskStatus {
        @JsonProperty("Capacity")
        private long capacity;

        @JsonProperty("CurUseSize")
        private long curUseSize;

        @JsonProperty("CurQuotaSize")
        private long curQuotaSize;

        @JsonProperty("AvaliableSize")
        private String availableSize;

        @JsonProperty("MountPath")
        private String mountPath;

        public long getCapacity() {
            return capacity;
        }

        public void setCapacity(long capacity) {
            this.capacity = capacity;
        }

        public long getCurUseSize() {
            return curUseSize;
        }

        public void setCurUseSize(long curUseSize) {
            this.curUseSize = curUseSize;
        }

        public long getCurQuotaSize() {
            return curQuotaSize;
        }

        public void setCurQuotaSize(long curQuotaSize) {
            this.curQuotaSize = curQuotaSize;
        }

        public String getAvailableSize() {
            return availableSize;
        }

        public void setAvailableSize(String availableSize) {
            this.availableSize = availableSize;
        }

        public String getMountPath() {
            return mountPath;
        }

        public void setMountPath(String mountPath) {
            this.mountPath = mountPath;
        }

        @Override
        public String toString() {
            return "DiskStatus{" +
                    "capacity=" + capacity +
                    ", curUseSize=" + curUseSize +
                    ", curQuotaSize=" + curQuotaSize +
                    ", availableSize='" + availableSize + '\'' +
                    ", mountPath='" + mountPath + '\'' +
                    '}';
        }
    }
}
