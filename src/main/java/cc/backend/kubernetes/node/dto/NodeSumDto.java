package cc.backend.kubernetes.node.dto;

import cc.backend.kubernetes.storage.usage.entity.CephGlobalUsage;
import cc.backend.kubernetes.storage.usage.entity.NfsGlobalUsage;

import java.util.List;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/2/23.
 */
public class NodeSumDto {

    private double cpuSum;
    private double cpuLimits;
    private double cpuRequests;
    private long memorySumBytes;
    private long memoryLimitsBytes;
    private long memoryRequestsBytes;

    @Deprecated
    private long localStorageSumBytes;
    @Deprecated
    private long localStorageRequestsBytes;
    @Deprecated
    private long localStorageUsageBytes;
    @Deprecated
    private long remoteStorageSumBytes;
    @Deprecated
    private long remoteStorageRequestsBytes;
    @Deprecated
    private long remoteStorageUsageBytes;

    private long hostPathSumBytes;
    private long hostPathRequestsBytes;
    private long hostPathUsageBytes;

    private long rbdSumBytes;
    private long rbdRequestsBytes;
    private long rbdUsageBytes;

    private long cephFsSumBytes;
    private long cephFsRequestsBytes;
    private long cephFsUsageBytes;

    private long nfsSumBytes;
    private long nfsUsageBytes;

    private long efsSumBytes;
    private long efsRequestsBytes;
    private long efsUsageBytes;

    private long ebsSumBytes;
    private long ebsRequestsBytes;
    private long ebsUsageBytes;

    public double getCpuSum() {
        return cpuSum;
    }

    public void setCpuSum(double cpuSum) {
        this.cpuSum = cpuSum;
    }

    public double getCpuLimits() {
        return cpuLimits;
    }

    public void setCpuLimits(double cpuLimits) {
        this.cpuLimits = cpuLimits;
    }

    public double getCpuRequests() {
        return cpuRequests;
    }

    public void setCpuRequests(double cpuRequests) {
        this.cpuRequests = cpuRequests;
    }

    public long getMemorySumBytes() {
        return memorySumBytes;
    }

    public void setMemorySumBytes(long memorySumBytes) {
        this.memorySumBytes = memorySumBytes;
    }

    public long getMemoryLimitsBytes() {
        return memoryLimitsBytes;
    }

    public void setMemoryLimitsBytes(long memoryLimitsBytes) {
        this.memoryLimitsBytes = memoryLimitsBytes;
    }

    public long getMemoryRequestsBytes() {
        return memoryRequestsBytes;
    }

    public void setMemoryRequestsBytes(long memoryRequestsBytes) {
        this.memoryRequestsBytes = memoryRequestsBytes;
    }

    public long getLocalStorageSumBytes() {
        return localStorageSumBytes;
    }

    public void setLocalStorageSumBytes(long localStorageSumBytes) {
        this.localStorageSumBytes = localStorageSumBytes;
    }

    public long getLocalStorageRequestsBytes() {
        return localStorageRequestsBytes;
    }

    public void setLocalStorageRequestsBytes(long localStorageRequestsBytes) {
        this.localStorageRequestsBytes = localStorageRequestsBytes;
    }

    public long getLocalStorageUsageBytes() {
        return localStorageUsageBytes;
    }

    public void setLocalStorageUsageBytes(long localStorageUsageBytes) {
        this.localStorageUsageBytes = localStorageUsageBytes;
    }

    public long getRemoteStorageSumBytes() {
        return remoteStorageSumBytes;
    }

    public void setRemoteStorageSumBytes(long remoteStorageSumBytes) {
        this.remoteStorageSumBytes = remoteStorageSumBytes;
    }

    public long getRemoteStorageRequestsBytes() {
        return remoteStorageRequestsBytes;
    }

    public void setRemoteStorageRequestsBytes(long remoteStorageRequestsBytes) {
        this.remoteStorageRequestsBytes = remoteStorageRequestsBytes;
    }

    public long getRemoteStorageUsageBytes() {
        return remoteStorageUsageBytes;
    }

    public void setRemoteStorageUsageBytes(long remoteStorageUsageBytes) {
        this.remoteStorageUsageBytes = remoteStorageUsageBytes;
    }

    public long getHostPathSumBytes() {
        return hostPathSumBytes;
    }

    public void setHostPathSumBytes(long hostPathSumBytes) {
        this.hostPathSumBytes = hostPathSumBytes;
    }

    public long getHostPathRequestsBytes() {
        return hostPathRequestsBytes;
    }

    public void setHostPathRequestsBytes(long hostPathRequestsBytes) {
        this.hostPathRequestsBytes = hostPathRequestsBytes;
    }

    public long getHostPathUsageBytes() {
        return hostPathUsageBytes;
    }

    public void setHostPathUsageBytes(long hostPathUsageBytes) {
        this.hostPathUsageBytes = hostPathUsageBytes;
    }

    public long getRbdSumBytes() {
        return rbdSumBytes;
    }

    public void setRbdSumBytes(long rbdSumBytes) {
        this.rbdSumBytes = rbdSumBytes;
    }

    public long getRbdRequestsBytes() {
        return rbdRequestsBytes;
    }

    public void setRbdRequestsBytes(long rbdRequestsBytes) {
        this.rbdRequestsBytes = rbdRequestsBytes;
    }

    public long getRbdUsageBytes() {
        return rbdUsageBytes;
    }

    public void setRbdUsageBytes(long rbdUsageBytes) {
        this.rbdUsageBytes = rbdUsageBytes;
    }

    public long getCephFsSumBytes() {
        return cephFsSumBytes;
    }

    public void setCephFsSumBytes(long cephFsSumBytes) {
        this.cephFsSumBytes = cephFsSumBytes;
    }

    public long getCephFsRequestsBytes() {
        return cephFsRequestsBytes;
    }

    public void setCephFsRequestsBytes(long cephFsRequestsBytes) {
        this.cephFsRequestsBytes = cephFsRequestsBytes;
    }

    public long getCephFsUsageBytes() {
        return cephFsUsageBytes;
    }

    public void setCephFsUsageBytes(long cephFsUsageBytes) {
        this.cephFsUsageBytes = cephFsUsageBytes;
    }

    public long getNfsSumBytes() {
        return nfsSumBytes;
    }

    public void setNfsSumBytes(long nfsSumBytes) {
        this.nfsSumBytes = nfsSumBytes;
    }

    public long getNfsUsageBytes() {
        return nfsUsageBytes;
    }

    public void setNfsUsageBytes(long nfsUsageBytes) {
        this.nfsUsageBytes = nfsUsageBytes;
    }

    public long getEfsSumBytes() {
        return efsSumBytes;
    }

    public void setEfsSumBytes(long efsSumBytes) {
        this.efsSumBytes = efsSumBytes;
    }

    public long getEfsRequestsBytes() {
        return efsRequestsBytes;
    }

    public void setEfsRequestsBytes(long efsRequestsBytes) {
        this.efsRequestsBytes = efsRequestsBytes;
    }

    public long getEfsUsageBytes() {
        return efsUsageBytes;
    }

    public void setEfsUsageBytes(long efsUsageBytes) {
        this.efsUsageBytes = efsUsageBytes;
    }

    public long getEbsSumBytes() {
        return ebsSumBytes;
    }

    public void setEbsSumBytes(long ebsSumBytes) {
        this.ebsSumBytes = ebsSumBytes;
    }

    public long getEbsRequestsBytes() {
        return ebsRequestsBytes;
    }

    public void setEbsRequestsBytes(long ebsRequestsBytes) {
        this.ebsRequestsBytes = ebsRequestsBytes;
    }

    public long getEbsUsageBytes() {
        return ebsUsageBytes;
    }

    public void setEbsUsageBytes(long ebsUsageBytes) {
        this.ebsUsageBytes = ebsUsageBytes;
    }

    @Override
    public String toString() {
        return "NodeSumDto{" +
                "cpuSum=" + cpuSum +
                ", cpuLimits=" + cpuLimits +
                ", cpuRequests=" + cpuRequests +
                ", memorySumBytes=" + memorySumBytes +
                ", memoryLimitsBytes=" + memoryLimitsBytes +
                ", memoryRequestsBytes=" + memoryRequestsBytes +
                ", localStorageSumBytes=" + localStorageSumBytes +
                ", localStorageRequestsBytes=" + localStorageRequestsBytes +
                ", localStorageUsageBytes=" + localStorageUsageBytes +
                ", remoteStorageSumBytes=" + remoteStorageSumBytes +
                ", remoteStorageRequestsBytes=" + remoteStorageRequestsBytes +
                ", remoteStorageUsageBytes=" + remoteStorageUsageBytes +
                ", hostPathSumBytes=" + hostPathSumBytes +
                ", hostPathRequestsBytes=" + hostPathRequestsBytes +
                ", hostPathUsageBytes=" + hostPathUsageBytes +
                ", rbdSumBytes=" + rbdSumBytes +
                ", rbdRequestsBytes=" + rbdRequestsBytes +
                ", rbdUsageBytes=" + rbdUsageBytes +
                ", cephFsSumBytes=" + cephFsSumBytes +
                ", cephFsRequestsBytes=" + cephFsRequestsBytes +
                ", cephFsUsageBytes=" + cephFsUsageBytes +
                ", nfsSumBytes=" + nfsSumBytes +
                ", nfsUsageBytes=" + nfsUsageBytes +
                ", efsSumBytes=" + efsSumBytes +
                ", efsRequestsBytes=" + efsRequestsBytes +
                ", efsUsageBytes=" + efsUsageBytes +
                ", ebsSumBytes=" + ebsSumBytes +
                ", ebsRequestsBytes=" + ebsRequestsBytes +
                ", ebsUsageBytes=" + ebsUsageBytes +
                '}';
    }

    public static NodeSumDto from(List<NodeDto> nodeDtos,
                                  CephGlobalUsage cephGlobalUsage,
                                  NfsGlobalUsage nfsGlobalUsage,
                                  Long rbdBytesProvisioned,
                                  Long rbdBytesUsed,
                                  long ebsSumBytes,
                                  long ebsReservedBytes) {
        NodeSumDto nodeSumDto = new NodeSumDto();

        long cpuSumBytes = 0;
        long cpuLimitsBytes = 0;
        long cpuRequestsBytes = 0;
        long memorySumBytes = 0;
        long memoryLimitsBytes = 0;
        long memoryRequestsBytes = 0;

        long hostPathSumBytes = 0;
        long hostPathRequestsBytes = 0;
        long hostPathUsageBytes = 0;

        for (NodeDto dto : nodeDtos) {
            cpuSumBytes += dto.getCpuSum();
            cpuLimitsBytes += dto.getCpuLimits();
            cpuRequestsBytes += dto.getCpuRequests();

            memorySumBytes += dto.getMemorySumBytes();
            memoryLimitsBytes += dto.getMemoryLimitsBytes();
            memoryRequestsBytes += dto.getMemoryRequestsBytes();

            hostPathSumBytes += dto.getLocalStorageSumBytes();
            hostPathRequestsBytes += dto.getLocalStorageRequestsBytes();
            hostPathUsageBytes += dto.getLocalStorageUsageBytes();
        }

        nodeSumDto.setCpuSum(cpuSumBytes);
        nodeSumDto.setCpuLimits(cpuLimitsBytes);
        nodeSumDto.setCpuRequests(cpuRequestsBytes);

        nodeSumDto.setMemorySumBytes(memorySumBytes);
        nodeSumDto.setMemoryLimitsBytes(memoryLimitsBytes);
        nodeSumDto.setMemoryRequestsBytes(memoryRequestsBytes);

        nodeSumDto.setLocalStorageSumBytes(hostPathSumBytes);
        nodeSumDto.setLocalStorageRequestsBytes(hostPathRequestsBytes);
        nodeSumDto.setLocalStorageUsageBytes(hostPathUsageBytes);
        nodeSumDto.setHostPathSumBytes(hostPathSumBytes);
        nodeSumDto.setHostPathRequestsBytes(hostPathRequestsBytes);
        nodeSumDto.setHostPathUsageBytes(hostPathUsageBytes);

        if (cephGlobalUsage != null) {
            nodeSumDto.setRemoteStorageSumBytes(cephGlobalUsage.getTotalBytes());
            nodeSumDto.setRbdSumBytes(cephGlobalUsage.getTotalBytes());

            nodeSumDto.setCephFsSumBytes(cephGlobalUsage.getCephfsTotalBytes());
            nodeSumDto.setCephFsUsageBytes(cephGlobalUsage.getTotalUsedBytes());
        }

        if (nfsGlobalUsage != null) {
            nodeSumDto.setNfsSumBytes(nfsGlobalUsage.getTotalBytes());
            nodeSumDto.setNfsUsageBytes(nfsGlobalUsage.getTotalUsedBytes());
            nodeSumDto.setEfsSumBytes(nfsGlobalUsage.getTotalBytes());
            nodeSumDto.setEfsUsageBytes(nfsGlobalUsage.getTotalUsedBytes());
        }

        nodeSumDto.setRemoteStorageRequestsBytes(rbdBytesProvisioned == null ? 0 : rbdBytesProvisioned);
        nodeSumDto.setRbdRequestsBytes(rbdBytesProvisioned == null ? 0 : rbdBytesProvisioned);

        nodeSumDto.setRemoteStorageUsageBytes(rbdBytesUsed == null ? 0 : rbdBytesUsed);
        nodeSumDto.setRbdUsageBytes(rbdBytesUsed == null ? 0 : rbdBytesUsed);

        nodeSumDto.setEbsSumBytes(ebsSumBytes);
        nodeSumDto.setEbsRequestsBytes(ebsReservedBytes);

        return nodeSumDto;
    }
}
