package cc.backend.kubernetes.node.dto;

import cc.backend.common.utils.DateUtils;
import cc.backend.kubernetes.workload.pod.PodHelper;
import cc.backend.kubernetes.workload.utils.WorkloadUtils;
import io.fabric8.kubernetes.api.model.*;

import java.util.*;
import java.util.stream.Collectors;

import static cc.backend.kubernetes.Constants.*;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/6.
 */
public class NodePodDto {
    private String name;
    private String uid;
    private String namespaceName;
    private String status;
    private int restartCount;
    private Date createdOn;
    private long age;
    private String clusterIp;
    private double cpuLimits;
    private double cpuRequests;
    private long memoryLimitsBytes;
    private long memoryRequestsBytes;
    private String deploymentName;
    private List<ContainerStatusDto> containerStatus;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getRestartCount() {
        return restartCount;
    }

    public void setRestartCount(int restartCount) {
        this.restartCount = restartCount;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public String getClusterIp() {
        return clusterIp;
    }

    public void setClusterIp(String clusterIp) {
        this.clusterIp = clusterIp;
    }

    public double getCpuLimits() {
        return cpuLimits;
    }

    public void setCpuLimits(double cpuLimits) {
        this.cpuLimits = cpuLimits;
    }

    public double getCpuRequests() {
        return cpuRequests;
    }

    public void setCpuRequests(double cpuRequests) {
        this.cpuRequests = cpuRequests;
    }

    public long getMemoryLimitsBytes() {
        return memoryLimitsBytes;
    }

    public void setMemoryLimitsBytes(long memoryLimitsBytes) {
        this.memoryLimitsBytes = memoryLimitsBytes;
    }

    public long getMemoryRequestsBytes() {
        return memoryRequestsBytes;
    }

    public void setMemoryRequestsBytes(long memoryRequestsBytes) {
        this.memoryRequestsBytes = memoryRequestsBytes;
    }

    public String getDeploymentName() {
        return deploymentName;
    }

    public void setDeploymentName(String deploymentName) {
        this.deploymentName = deploymentName;
    }

    public List<ContainerStatusDto> getContainerStatus() {
        return containerStatus;
    }

    public void setContainerStatus(List<ContainerStatusDto> containerStatus) {
        this.containerStatus = containerStatus;
    }

    @Override
    public String toString() {
        return "NodePodDto{" +
                "name='" + name + '\'' +
                ", uid='" + uid + '\'' +
                ", namespaceName='" + namespaceName + '\'' +
                ", status='" + status + '\'' +
                ", restartCount=" + restartCount +
                ", createdOn=" + createdOn +
                ", age=" + age +
                ", clusterIp='" + clusterIp + '\'' +
                ", cpuLimits=" + cpuLimits +
                ", cpuRequests=" + cpuRequests +
                ", memoryLimitsBytes=" + memoryLimitsBytes +
                ", memoryRequestsBytes=" + memoryRequestsBytes +
                ", deploymentName='" + deploymentName + '\'' +
                ", containerStatus=" + containerStatus +
                '}';
    }

    public static List<NodePodDto> from(List<Pod> pods) {
        return Optional.ofNullable(pods)
                .orElseGet(ArrayList::new)
                .stream()
                .map(NodePodDto::from)
                .collect(Collectors.toList());
    }

    public static NodePodDto from(Pod pod) {
        NodePodDto dto = new NodePodDto();

        ObjectMeta metadata = pod.getMetadata();
        PodStatus status = pod.getStatus();
        PodSpec spec = pod.getSpec();

        dto.setName(metadata.getName());
        dto.setUid(metadata.getUid());
        dto.setNamespaceName(metadata.getNamespace());
        dto.setStatus(PodHelper.getDisplayStatus(pod));

        List<ContainerStatus> containerStatusList = Optional.ofNullable(status.getContainerStatuses())
                .orElseGet(ArrayList::new);

        int totalRestartCount = containerStatusList.stream()
                .mapToInt(ContainerStatus::getRestartCount)
                .sum();
        dto.setRestartCount(totalRestartCount);
        dto.setContainerStatus(ContainerStatusDto.from(containerStatusList));

        dto.setCreatedOn(DateUtils.parseK8sDate(metadata.getCreationTimestamp()));
        dto.setAge(DateUtils.getAge(dto.getCreatedOn()));
        dto.setClusterIp(status.getPodIP());

        List<Container> containers = Optional.ofNullable(spec.getContainers()).orElseGet(ArrayList::new);
        Map<String, Number> computeQuotaMap = WorkloadUtils.getComputeQuotaMap(containers, 1);
        dto.setCpuLimits((Double) computeQuotaMap.get(K_QUOTA_CPU_LIMITS));
        dto.setCpuRequests((Double) computeQuotaMap.get(K_QUOTA_CPU_REQUESTS));
        dto.setMemoryLimitsBytes((Long) computeQuotaMap.get(K_QUOTA_MEMORY_LIMITS));
        dto.setMemoryRequestsBytes((Long) computeQuotaMap.get(K_QUOTA_MEMORY_REQUESTS));

        // Get deployment name by RS name
        // refer: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/
        // You may notice that the name of the ReplicaSet
        // is always <the name of the Deployment>-<hash value of the pod template>
        Optional<String> deploymentName = Optional.ofNullable(metadata.getOwnerReferences())
                .orElseGet(ArrayList::new)
                .stream()
                .filter(reference -> "ReplicaSet".equals(reference.getKind()))
                .map(OwnerReference::getName)
                .map(replicaSetName -> {
                    int index = replicaSetName.lastIndexOf("-");
                    if (index > 0) {
                        return replicaSetName.substring(0, index);
                    }
                    return null;
                })
                .filter(Objects::nonNull)
                .findAny();
        dto.setDeploymentName(deploymentName.orElse(null));
        return dto;
    }
}
