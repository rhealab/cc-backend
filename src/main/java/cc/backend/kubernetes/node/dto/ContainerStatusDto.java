package cc.backend.kubernetes.node.dto;

import io.fabric8.kubernetes.api.model.ContainerStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/22.
 */
public class ContainerStatusDto {
    private String name;
    private int restartCount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRestartCount() {
        return restartCount;
    }

    public void setRestartCount(int restartCount) {
        this.restartCount = restartCount;
    }

    @Override
    public String toString() {
        return "ContainerStatusDto{" +
                "name='" + name + '\'' +
                ", restartCount=" + restartCount +
                '}';
    }

    public static List<ContainerStatusDto> from(List<ContainerStatus> containerStatus) {
        return Optional.ofNullable(containerStatus)
                .orElseGet(ArrayList::new)
                .stream()
                .map(ContainerStatusDto::from)
                .collect(Collectors.toList());
    }

    public static ContainerStatusDto from(ContainerStatus containerStatus) {
        ContainerStatusDto dto = new ContainerStatusDto();

        dto.setName(containerStatus.getName());
        dto.setRestartCount(containerStatus.getRestartCount());

        return dto;
    }
}
