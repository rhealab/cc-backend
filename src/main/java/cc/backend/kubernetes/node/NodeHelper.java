package cc.backend.kubernetes.node;

import cc.backend.kubernetes.node.dto.NodeDiskQuotaState;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.fabric8.kubernetes.api.model.Node;
import io.fabric8.kubernetes.api.model.NodeCondition;
import io.fabric8.kubernetes.api.model.NodeStatus;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static cc.backend.kubernetes.Constants.NODE_DISK_STATE_ANNOTATION;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/26.
 */
public class NodeHelper {
    public static NodeDiskQuotaState getDiskQuotaState(Node node) {
        Map<String, String> annotations = node.getMetadata().getAnnotations();
        return Optional.ofNullable(annotations)
                .map(a -> a.get(NODE_DISK_STATE_ANNOTATION))
                .map(state -> {
                    ObjectMapper mapper = new ObjectMapper();
                    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                    try {
                        return mapper.readValue(state, NodeDiskQuotaState.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return null;
                }).orElse(null);
    }

    public static String composeNodeDtoRole(Map<String, String> labels) {
        String role = null;
        if (labels != null) {
            role = labels.get("role");
        }
        if (role == null) {
            role = "node";
        }
        return role;
    }

    public static String composeNodeDtoStatus(NodeStatus status) {
        StringBuilder dtoStatus = new StringBuilder();
        List<NodeCondition> conditions = status.getConditions();
        for (NodeCondition condition : conditions) {
            if ("Ready".equals(condition.getType()) && !"true".equalsIgnoreCase(condition.getStatus())) {
                return "notReady";
            }
            if ("true".equalsIgnoreCase(condition.getStatus())) {
                dtoStatus.append("/").append(condition.getType());
            }
        }

        return dtoStatus.deleteCharAt(0).toString();
    }
}
