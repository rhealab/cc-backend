package cc.backend.kubernetes.node;

import cc.backend.kubernetes.event.EventDto;
import cc.backend.kubernetes.node.dto.NodeDto;
import cc.backend.kubernetes.node.dto.NodePodDto;
import cc.backend.kubernetes.node.dto.NodeSumDto;
import cc.backend.kubernetes.node.services.NodesManagement;
import cc.backend.kubernetes.node.validator.NodesValidator;
import io.fabric8.kubernetes.api.model.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;


/**
 * @author wangchunyang@gmail.com
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes")
@Api(value = "Node", description = "Info about nodes.", produces = "application/json")
public class NodesEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(NodesEndpoint.class);

    @Inject
    private NodesManagement management;

    @Inject
    private NodesValidator validator;

    @GET
    @Path("nodes")
    @ApiOperation(value = "List all nodes info.", responseContainer = "List", response = NodeDto.class)
    public Response getNodeList() {
        List<Node> nodes = management.getNodes();

        Map<String, List<Pod>> nodePodsMap = management.getNodePodsMap();
        Map<String, Map<String, PersistentVolumeClaim>> namespacePvcMap = management.getNamespacePvcMap();
        Map<String, PersistentVolume> pvMap = management.getPvMap();

        return Response.ok(NodeDto.from(nodes, nodePodsMap, namespacePvcMap, pvMap)).build();
    }

    @GET
    @Path("nodes/{nodeName}")
    @ApiOperation(value = "Get node info.", response = NodeDto.class)
    public Response getNode(@PathParam("nodeName") String nodeName) {
        Node node = validator.validateNodeShouldExists(nodeName);

        List<Pod> podList = management.getNodePods(nodeName);
        Map<String, Map<String, PersistentVolumeClaim>> namespacePvcMap = management.getNamespacePvcMap();
        Map<String, PersistentVolume> pvMap = management.getPvMap();

        List<PersistentVolumeClaim> pvcList = NodeDto.getPodsPvcList(podList, namespacePvcMap);
        List<PersistentVolume> nodePvList = NodeDto.getPvList(pvcList, pvMap);

        return Response.ok(NodeDto.from(node, nodePvList, podList)).build();
    }

    @GET
    @Path("nodes/{nodeName}/pods")
    @ApiOperation(value = "List the node's pod list info.", responseContainer = "List", response = NodePodDto.class)
    public Response getNodePods(@PathParam("nodeName") String name) {
        List<Pod> podList = management.getNodePods(name);
        return Response.ok(NodePodDto.from(podList)).build();
    }

    @GET
    @Path("nodes/{nodeName}/events")
    @ApiOperation(value = "List the node's event list info.", responseContainer = "List", response = EventDto.class)
    public Response getNodeEvents(@PathParam("nodeName") String nodeName) {
        List<Event> eventList = management.getNodeEvents(nodeName);
        return Response.ok(EventDto.from(eventList)).build();
    }

    @GET
    @Path("nodes/{nodeName}/events/json")
    @ApiOperation(value = "List the node's event list info of k8s original format.",
            responseContainer = "List", response = Event.class)
    public Response getNodeEventsJson(@PathParam("nodeName") String nodeName) {
        List<Event> eventList = management.getNodeEvents(nodeName);
        return Response.ok(eventList).build();
    }

    @GET
    @Path("nodes_sum")
    @ApiOperation(value = "Get the whole system resource info.", response = NodeSumDto.class)
    public Response getNodesSum() {
        NodeSumDto nodeSum = management.getNodeSum();
        return Response.ok(nodeSum).build();
    }
}
