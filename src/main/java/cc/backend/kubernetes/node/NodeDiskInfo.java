package cc.backend.kubernetes.node;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by xzy on 2017/3/29.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NodeDiskInfo {

    private String mountPath;

    private long allocable;

    public String getMountPath() {
        return mountPath;
    }

    @JsonProperty("MountPath")
    public void setMountPath(String mountPath) {
        this.mountPath = mountPath;
    }

    public long getAllocable() {
        return allocable;
    }

    @JsonProperty("Allocable")
    public void setAllocable(long allocable) {
        this.allocable = allocable;
    }

    @Override
    public String toString() {
        return "NodeDiskInfo{" +
                "mountPath='" + mountPath + '\'' +
                ", allocable=" + allocable +
                '}';
    }
}
