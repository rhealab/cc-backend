package cc.backend.kubernetes.node.services;

import cc.backend.common.Metrics;
import cc.backend.common.MetricsType;
import cc.backend.common.OpenTsdbClient;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.namespaces.dto.MetricsDto;
import cc.backend.kubernetes.utils.UnitUtils;
import com.google.common.collect.ImmutableList;
import io.fabric8.kubernetes.api.model.Node;
import io.fabric8.kubernetes.api.model.NodeStatus;
import io.fabric8.kubernetes.api.model.Quantity;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static cc.backend.kubernetes.Constants.K_CPU;
import static cc.backend.kubernetes.Constants.K_MEMORY;

/**
 * @author yanzhixiang on 17-7-31.
 */
@Named
public class NodesStatsManagement {
    @Inject
    private OpenTsdbClient openTsdbClient;

    @Inject
    private KubernetesClientManager clientManager;

    public List<MetricsDto> getComputeStats(String nodeName) {
        Node node = clientManager.getClient()
                .nodes()
                .withName(nodeName)
                .get();

        MetricsDto cpuMetricsDto;
        MetricsDto memoryMetricsDto;
        if (node != null) {
            Map<String, Quantity> quantityMap = node.getStatus().getAllocatable();
            double totalCpu = UnitUtils.parseK8sCpuQuantity(quantityMap.get(K_CPU));
            long totalMemory = UnitUtils.parseK8sMemoryQuantity(quantityMap.get(K_MEMORY));

            List<Metrics> cpuMetrics = openTsdbClient.getNodeCpuMetrics(nodeName);
            List<Metrics> memoryMetrics = openTsdbClient.getNodeMemoryMetrics(nodeName);

            cpuMetricsDto = MetricsDto.from(cpuMetrics, totalCpu, MetricsType.CPU);
            memoryMetricsDto = MetricsDto.from(memoryMetrics, totalMemory, MetricsType.MEMORY);
        } else {
            cpuMetricsDto = MetricsDto.newErrorInstance(MetricsType.CPU);
            memoryMetricsDto = MetricsDto.newErrorInstance(MetricsType.MEMORY);
        }

        return ImmutableList.of(cpuMetricsDto, memoryMetricsDto);
    }

    public List<Metrics> getComputeMetrics(MetricsType type, String nodeName) {
        return openTsdbClient.getNodeMetrics(type, nodeName);
    }

    public long getNodeTotalMemory(String nodeName) {
        Node node = clientManager.getClient()
                .nodes()
                .withName(nodeName)
                .get();

        return getNodeTotalMemory(node);
    }

    public double getNodeTotalCpu(String nodeName) {
        Node node = clientManager.getClient()
                .nodes()
                .withName(nodeName)
                .get();

        return getNodeTotalCpu(node);
    }

    private long getNodeTotalMemory(Node node) {
        return Optional.ofNullable(node)
                .map(Node::getStatus)
                .map(NodeStatus::getAllocatable)
                .map(a -> a.get(K_MEMORY))
                .map(UnitUtils::parseK8sMemoryQuantity)
                .orElse(0L);
    }

    private double getNodeTotalCpu(Node node) {
        return Optional.ofNullable(node)
                .map(Node::getStatus)
                .map(NodeStatus::getAllocatable)
                .map(a -> a.get(K_CPU))
                .map(UnitUtils::parseK8sCpuQuantity)
                .orElse(0.0);
    }
}
