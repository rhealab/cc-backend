package cc.backend.kubernetes.node.services;

import cc.backend.EnnProperties;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.node.dto.NodeDto;
import cc.backend.kubernetes.node.dto.NodeSumDto;
import cc.backend.kubernetes.storage.StorageRepository;
import cc.backend.kubernetes.storage.domain.StorageType;
import cc.backend.kubernetes.storage.usage.entity.*;
import io.fabric8.kubernetes.api.model.*;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/2/23.
 */
@Named
public class NodesManagement {
    @Inject
    private KubernetesClientManager clientManager;

    @Inject
    private CephGlobalUsageRepository cephGlobalUsageRepository;

    @Inject
    private NfsGlobalUsageRepository nfsGlobalUsageRepository;

    @Inject
    private CephImageUsageRepository cephImageUsageRepository;

    @Inject
    private EnnProperties properties;

    @Inject
    private StorageRepository storageRepository;

    public List<Node> getNodes() {
        List<Node> nodes = clientManager.getClient()
                .nodes()
                .list()
                .getItems();
        return Optional.ofNullable(nodes).orElseGet(ArrayList::new);
    }

    public List<Pod> getNodePods(String nodeName) {
        List<Pod> podList = clientManager.getClient()
                .pods()
                .inAnyNamespace()
                .withField("spec.nodeName", nodeName)
                .list()
                .getItems();
        return Optional.ofNullable(podList).orElseGet(ArrayList::new);
    }

    public List<Event> getNodeEvents(String nodeName) {
        List<Event> eventList = clientManager.getClient()
                .events()
                .withField("involvedObject.uid", nodeName)
                .list()
                .getItems();
        return Optional.ofNullable(eventList).orElseGet(ArrayList::new);
    }

    public Map<String, List<Pod>> getNodePodsMap() {
        List<Pod> allPodList = clientManager.getClient()
                .pods()
                .inAnyNamespace()
                .list()
                .getItems();

        return Optional.ofNullable(allPodList)
                .orElseGet(ArrayList::new)
                .stream()
                .filter(pod -> pod.getSpec().getNodeName() != null)
                .collect(groupingBy(pod -> pod.getSpec().getNodeName()));
    }

    public Map<String, Map<String, PersistentVolumeClaim>> getNamespacePvcMap() {
        List<PersistentVolumeClaim> allPvcList = clientManager.getClient()
                .persistentVolumeClaims()
                .inAnyNamespace()
                .list()
                .getItems();

        return Optional.ofNullable(allPvcList)
                .orElseGet(ArrayList::new)
                .stream()
                .collect(groupingBy(pvc -> pvc.getMetadata().getNamespace(),
                        toMap(pvc -> pvc.getMetadata().getName(), pvc -> pvc)));
    }

    public Map<String, PersistentVolume> getPvMap() {
        List<PersistentVolume> allPv = clientManager.getClient()
                .persistentVolumes()
                .list()
                .getItems();

        return Optional.ofNullable(allPv)
                .orElseGet(ArrayList::new)
                .stream()
                .collect(Collectors.toMap(pv -> pv.getMetadata().getName(), pv -> pv));
    }

    public NodeSumDto getNodeSum() {
        List<Node> nodeList = getNodes();
        Map<String, List<Pod>> nodePodsMap = getNodePodsMap();
        Map<String, Map<String, PersistentVolumeClaim>> namespacePvcMap = getNamespacePvcMap();
        Map<String, PersistentVolume> pvMap = getPvMap();

        List<NodeDto> nodeDtoList = NodeDto.from(nodeList, nodePodsMap, namespacePvcMap, pvMap);

        CephGlobalUsage cephGlobalUsage = cephGlobalUsageRepository.findLatest();
        NfsGlobalUsage nfsGlobalUsage = nfsGlobalUsageRepository.findLatest();
        Long bytesProvisioned = cephImageUsageRepository.findSumOfBytesProvisioned();
        Long bytesUsed = cephImageUsageRepository.findSumOfBytesUsed();

        long ebsReservedBytes = Optional.ofNullable(storageRepository.sumByStorageType(StorageType.EBS.name())).orElse(0L);
        return NodeSumDto.from(nodeDtoList, cephGlobalUsage, nfsGlobalUsage,
                bytesProvisioned, bytesUsed, properties.getCurrentEbs().getTotalBytes(), ebsReservedBytes);
    }
}