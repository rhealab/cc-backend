package cc.backend.kubernetes.check.data;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by xzy on 18-3-20.
 */
@Entity
public class NodePortRange {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Enumerated(EnumType.STRING)
  private NodePortRangeType type;

  //inclusive
  private int min;
  private int max;

  public NodePortRangeType getType() {
    return type;
  }

  public void setType(NodePortRangeType type) {
    this.type = type;
  }

  public int getMin() {
    return min;
  }

  public void setMin(int min) {
    this.min = min;
  }

  public int getMax() {
    return max;
  }

  public void setMax(int max) {
    this.max = max;
  }
}
