package cc.backend.kubernetes.check.data;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by xzy on 18-3-21.
 */
public interface NodePortRangeRepository extends JpaRepository<NodePortRange, Long>{
  List<NodePortRange> findByType(NodePortRangeType type);
}
