package cc.backend.kubernetes.check.data;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Set;

/**
 * Created by xzy on 18-3-21.
 */
public interface NamespaceNodePortRangeRepository extends JpaRepository<NamespaceNodePortRange, Long>{
  @Query("SELECT n.namespace FROM NamespaceNodePortRange n WHERE n.type = :nodePortRangeType")
  Set<String> findNamespacesByType(@Param("nodePortRangeType") NodePortRangeType type);
}
