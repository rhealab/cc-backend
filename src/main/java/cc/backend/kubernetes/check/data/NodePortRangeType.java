package cc.backend.kubernetes.check.data;

/**
 * Created by xzy on 18-3-21.
 */
public enum NodePortRangeType {
  PLATFORM, USER
}
