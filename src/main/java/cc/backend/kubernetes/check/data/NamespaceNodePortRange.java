package cc.backend.kubernetes.check.data;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Created by xzy on 18-3-21.
 */
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"namespace", "type"})
})
public class NamespaceNodePortRange {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  private String namespace;

  @Enumerated(EnumType.STRING)
  private NodePortRangeType type;

  public String getNamespace() {
    return namespace;
  }

  public void setNamespace(String namespace) {
    this.namespace = namespace;
  }

  public NodePortRangeType getType() {
    return type;
  }

  public void setType(NodePortRangeType type) {
    this.type = type;
  }
}
