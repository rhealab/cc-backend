package cc.backend.kubernetes.check;

import cc.backend.kubernetes.check.domain.ExternalIPCheckRequest;
import cc.backend.kubernetes.check.domain.ExternalIPCheckResult;
import cc.backend.kubernetes.check.domain.NodePortCheckResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Set;

/**
 * @author yanzhixiang on 17-7-10.
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes/check")
@Api(value = "Service", description = "Operation about service.", produces = "application/json")
public class ResourceCheckEndpoint {

    @Inject
    private ResourceCheckManagement management;

    @GET
    @Path("node_port/{nodePort}")
    @ApiOperation(value = "checks if node port is available for allocation.", response = NodePortCheckResult.class)
    public Response checkNodePort(@PathParam("nodePort") int nodePort) {
        return Response.ok(management.isNodePortAssignable(nodePort)).build();
    }

    @GET
    @Path("/namespace/{namespace}/node_port/{nodePort}")
    @ApiOperation(value = "checks if node port in this namespace could be used.", response = NodePortCheckResult.class)
    public Response checkNodePortInNamespace(@PathParam("namespace") String namespace, @PathParam("nodePort") int nodePort) {
        return Response.ok(management.isNodePortAssignable(namespace, nodePort)).build();
    }

    @POST
    @Path("external_ip")
    @ApiOperation(value = "checks if external ip is available for allocation, " +
            "external ip is available if the result is empty list",
            responseContainer = "List", response = ExternalIPCheckResult.class)
    public Response checkExternalIPAndPort(@Valid ExternalIPCheckRequest request) {
        return Response.ok(management.isExternalIPsAndPortAssignable(request.getExternalIPs(), request.getPortList())).build();
    }

    @GET
    @Path("/nodePort/random/num/{num}")
    @ApiOperation(value = "get a random NodePort.", response = Set.class)
    public Response randomNodePort(@PathParam("num") int num) {
        return Response.ok(management.randomNodePorts(num)).build();
    }

    @GET
    @Path("/platformNamespaces")
    @ApiOperation(value = "get platform namespaces.", response = Set.class)
    public Response getPlatformNamespaces() {
        return Response.ok(management.getPlatformNamespaces()).build();
    }
}
