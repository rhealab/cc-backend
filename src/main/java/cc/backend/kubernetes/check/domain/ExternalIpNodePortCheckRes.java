package cc.backend.kubernetes.check.domain;

/**
 * @author xzy on 17-11-14.
 */
public class ExternalIpNodePortCheckRes {
    private String externalIP;
    private Integer port;
    private Integer nodePort;
    private boolean isAssignable;
    private boolean isNodePortConflict;
    private String conflictSvc;
    private String conflictSvcNs;
    private boolean conflictWithNodePort;
    private boolean notInRange;

    public String getExternalIP() {
        return externalIP;
    }

    public void setExternalIP(String externalIP) {
        this.externalIP = externalIP;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Integer getNodePort() {
        return nodePort;
    }

    public void setNodePort(Integer nodePort) {
        this.nodePort = nodePort;
    }

    public boolean isAssignable() {
        return isAssignable;
    }

    public void setAssignable(boolean assignable) {
        isAssignable = assignable;
    }

    public boolean isNodePortConflict() {
        return isNodePortConflict;
    }

    public void setNodePortConflict(boolean nodePortConflict) {
        isNodePortConflict = nodePortConflict;
    }

    public String getConflictSvc() {
        return conflictSvc;
    }

    public void setConflictSvc(String conflictSvc) {
        this.conflictSvc = conflictSvc;
    }

    public String getConflictSvcNs() {
        return conflictSvcNs;
    }

    public void setConflictSvcNs(String conflictSvcNs) {
        this.conflictSvcNs = conflictSvcNs;
    }

    public boolean isConflictWithNodePort() {
        return conflictWithNodePort;
    }

    public void setConflictWithNodePort(boolean conflictWithNodePort) {
        this.conflictWithNodePort = conflictWithNodePort;
    }

    public boolean isNotInRange() {
        return notInRange;
    }

    public void setNotInRange(boolean notInRange) {
        this.notInRange = notInRange;
    }

    @Override
    public String toString() {
        return "ExternalIpNodePortCheckRes{" +
                "externalIP='" + externalIP + '\'' +
                ", port=" + port +
                ", nodePort=" + nodePort +
                ", isAssignable=" + isAssignable +
                ", isNodePortConflict=" + isNodePortConflict +
                ", conflictSvc='" + conflictSvc + '\'' +
                ", conflictSvcNs='" + conflictSvcNs + '\'' +
                ", conflictWithNodePort=" + conflictWithNodePort +
                '}';
    }

    public static class Builder {
        private String conflictSvc;
        private String conflictSvcNs;
        private String externalIP;
        private Integer port;
        private Integer nodePort;
        private boolean conflictWithNodePort;
        private boolean isNodePortConflict;
        private boolean isAssignable;
        private boolean notInRange;

        public Builder conflictSvc(String conflictSvc) {
            this.conflictSvc = conflictSvc;
            return this;
        }

        public Builder conflictSvcNs(String conflictSvcNs) {
            this.conflictSvcNs = conflictSvcNs;
            return this;
        }

        public Builder externalIP(String externalIP) {
            this.externalIP = externalIP;
            return this;
        }

        public Builder port(Integer port) {
            this.port = port;
            return this;
        }

        public Builder nodePort(Integer nodePort) {
            this.nodePort = nodePort;
            return this;
        }

        public Builder conflictWithNodePort(boolean conflictWithNodePort) {
            this.conflictWithNodePort = conflictWithNodePort;
            return this;
        }

        public Builder isNodePortConflict(boolean isNodePortConflict) {
            this.isNodePortConflict = isNodePortConflict;
            return this;
        }

        public Builder isAssignable(boolean isAssignable) {
            this.isAssignable = isAssignable;
            return this;
        }

        public Builder notInRange(boolean notInRange) {
            this.notInRange = notInRange;
            return this;
        }

        public ExternalIpNodePortCheckRes build() {
            ExternalIpNodePortCheckRes res = new ExternalIpNodePortCheckRes();
            res.setConflictSvc(conflictSvc);
            res.setConflictSvcNs(conflictSvcNs);
            res.setNodePort(nodePort);
            res.setPort(port);
            res.setExternalIP(externalIP);
            res.setConflictWithNodePort(conflictWithNodePort);
            res.setNotInRange(notInRange);
            return res;
        }
    }
}
