package cc.backend.kubernetes.check.domain;

/**
 * @author xzy on 17-7-24.
 */
public class NodePortCheckResult {
    private int nodePort;
    private boolean isAssignable;
    private String detail;

    public int getNodePort() {
        return nodePort;
    }

    public void setNodePort(int nodePort) {
        this.nodePort = nodePort;
    }

    public boolean isAssignable() {
        return isAssignable;
    }

    public void setAssignable(boolean assignable) {
        isAssignable = assignable;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return "NodePortCheckResult{" +
                "nodePort='" + nodePort + '\'' +
                ", isAssignable='" + isAssignable + '\'' +
                ", detail='" + detail + '\'' +
                '}';
    }
}
