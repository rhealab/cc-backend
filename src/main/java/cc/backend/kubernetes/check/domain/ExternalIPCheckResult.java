package cc.backend.kubernetes.check.domain;

/**
 * @author yanzhixiang on 17-7-24.
 */
public class ExternalIPCheckResult {
    private String externalIP;
    private int port;
    private boolean isAssignable;
    private String detail;

    public String getExternalIP() {
        return externalIP;
    }

    public void setExternalIP(String externalIP) {
        this.externalIP = externalIP;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isAssignable() {
        return isAssignable;
    }

    public void setAssignable(boolean assignable) {
        isAssignable = assignable;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return "ExternalIPCheckResult{" +
                "externalIP='" + externalIP + '\'' +
                "port='" + port + '\'' +
                ", isAssignable='" + isAssignable + '\'' +
                ", detail='" + detail + '\'' +
                '}';
    }

}
