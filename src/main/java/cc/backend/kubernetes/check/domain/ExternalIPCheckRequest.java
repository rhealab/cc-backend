package cc.backend.kubernetes.check.domain;

import java.util.List;

/**
 * @author xzy on 17-7-24.
 */
public class ExternalIPCheckRequest {
    private List<String> externalIPs;
    private List<Integer> portList;

    public List<String> getExternalIPs() {
        return externalIPs;
    }

    public void setExternalIPs(List<String> externalIPs) {
        this.externalIPs = externalIPs;
    }

    public List<Integer> getPortList() {
        return portList;
    }

    public void setPortList(List<Integer> portList) {
        this.portList = portList;
    }
}
