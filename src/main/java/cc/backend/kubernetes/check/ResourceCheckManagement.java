package cc.backend.kubernetes.check;

import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.check.data.NamespaceNodePortRange;
import cc.backend.kubernetes.check.data.NamespaceNodePortRangeRepository;
import cc.backend.kubernetes.check.data.NodePortRange;
import cc.backend.kubernetes.check.data.NodePortRangeRepository;
import cc.backend.kubernetes.check.data.NodePortRangeType;
import cc.backend.kubernetes.check.domain.ExternalIPCheckResult;
import cc.backend.kubernetes.check.domain.ExternalIpNodePortCheckRes;
import cc.backend.kubernetes.check.domain.NodePortCheckResult;
import cc.backend.kubernetes.service.dto.ServiceType;
import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.lib.retcode.CcException;
import io.fabric8.kubernetes.api.model.*;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author xzy on 17-7-26.
 */
@Component
public class ResourceCheckManagement {
  public static final int PLATFORM_NODEPORTS_MIN = 29000;
  public static final int PLATFORM_NODEPORTS_MAX = 29999;
  public static final int USER_NAMESPACE_NODEPORTS_MIN = 30000;
  public static final int USER_NAMESPACE_NODEPORTS_MAX = 32767;

  @Inject
  private KubernetesClientManager clientManager;
  @Inject
  private NamespaceNodePortRangeRepository namespaceNodePortRangeRepository;
  @Inject
  private NodePortRangeRepository nodePortRangeRepository;

  private Set<String> platformNamespaces;

  public void platformNamespace(String namespace) {
    NamespaceNodePortRange range = new NamespaceNodePortRange();
    range.setNamespace(namespace);
    range.setType(NodePortRangeType.PLATFORM);
    namespaceNodePortRangeRepository.save(range);
  }

  @PostConstruct
  public void init() {
    List<NodePortRange> nodePortRanges = nodePortRangeRepository.findAll();
    if (nodePortRanges == null || nodePortRanges.size() == 0 ) {
      //initialize
      NodePortRange platform = new NodePortRange();
      platform.setType(NodePortRangeType.PLATFORM);
      platform.setMin(PLATFORM_NODEPORTS_MIN);
      platform.setMax(PLATFORM_NODEPORTS_MAX);
      NodePortRange user = new NodePortRange();
      user.setType(NodePortRangeType.USER);
      user.setMin(USER_NAMESPACE_NODEPORTS_MIN);
      user.setMax(USER_NAMESPACE_NODEPORTS_MAX);
      nodePortRangeRepository.save(platform);
      nodePortRangeRepository.save(user);
      platformNamespace("console");
      platformNamespace("system-tools");
      platformNamespace("ceph");
      platformNamespace("kube-system");
      platformNamespace("tube-public");
      platformNamespace("default");
      platformNamespace("monitor-system-graph");
      platformNamespace("monitor-essential-service");
      platformNamespace("monitor-system-alert");
      platformNamespace("monitor-system-security");
      platformNamespace("monitor-streaming-jobs");
      platformNamespace("monitor-system-metrics");
      platformNamespace("monitor-system-log");
    }

    platformNamespaces = namespaceNodePortRangeRepository.findNamespacesByType(NodePortRangeType.PLATFORM);
    if (platformNamespaces == null) {
      platformNamespaces = new TreeSet<>();
    }
  }

  public Set<String> getPlatformNamespaces() {
    return platformNamespaces;
  }

  public NodePortCheckResult isNodePortAssignable(String namespace, int nodePort) {
    if (platformNamespaces.contains(namespace)) {
      return checkNodePort(nodePort, PLATFORM_NODEPORTS_MIN, PLATFORM_NODEPORTS_MAX);
    } else {
      return checkNodePort(nodePort, USER_NAMESPACE_NODEPORTS_MIN, USER_NAMESPACE_NODEPORTS_MAX);
    }
  }

  public NodePortCheckResult checkNodePort(int nodePort, int min, int max) {
    if (nodePort >= min && nodePort <= max) {
      return isNodePortAssignable(nodePort);
    } else {
      NodePortCheckResult result = new NodePortCheckResult();
      result.setAssignable(false);
      result.setNodePort(nodePort);
      result.setDetail(String.format("nodePort range is %d-%d", min, max));
      return result;
    }
  }

  public int randomNodePort() {
    int increment = USER_NAMESPACE_NODEPORTS_MAX - USER_NAMESPACE_NODEPORTS_MIN + 1;
    Random rand = new Random();
    int nodeport;
    NodePortCheckResult result;
    int maxAttempts = 100;
    for (int i = 0; i < maxAttempts; i++) {
      nodeport = USER_NAMESPACE_NODEPORTS_MIN + rand.nextInt(increment);
      result = isNodePortAssignable(nodeport);
      if (result.isAssignable()) {
        return nodeport;
      }
    }
    throw new CcException(BackendReturnCodeNameConstants.RANDOM_NODEPORT_MAX_ATTEMPTS);
  }

  public Set<Integer> randomNodePorts(int num) {
    Set<Integer> set = new TreeSet<>();
    while (set.size() < num) {
      set.add(randomNodePort());
    }
    return set;
  }

  public NodePortCheckResult isNodePortAssignable(int nodePort) {
    List<Service> serviceList = clientManager.getClient()
            .services()
            .inAnyNamespace()
            .list()
            .getItems();

    NodePortCheckResult result = new NodePortCheckResult();
    result.setNodePort(nodePort);
    result.setAssignable(true);

    Set<String> nodeIpSet = getNodeIpSet();

    Optional<Service> conflictService = Optional.ofNullable(serviceList)
            .orElseGet(ArrayList::new)
            .stream()
            .filter(service -> !checkServiceForNodePort(nodeIpSet, service, nodePort, result))
            .findAny();

    if (!conflictService.isPresent()) {
      result.setDetail(String.format("nodePort:%d is assignable", nodePort));
    }

    return result;
  }

  private boolean checkServiceForNodePort(Set<String> nodeIpSet,
                                          Service service,
                                          int nodePort,
                                          NodePortCheckResult result) {
    List<ServicePort> portList = service.getSpec().getPorts();

    Optional<ServicePort> conflictPort = portList.stream()
            .filter(port -> port.getNodePort() != null && nodePort == port.getNodePort())
            .findAny();
    if (conflictPort.isPresent()) {
      String detail = String.format("nodePort:%d is already assign to service:%s in namespace:%s"
              , nodePort, service.getMetadata().getName(), service.getMetadata().getNamespace());
      result.setAssignable(false);
      result.setDetail(detail);
      return false;
    }

    List<String> externalIPs = service.getSpec().getExternalIPs();
    if (!CollectionUtils.isEmpty(externalIPs)) {
      conflictPort = portList.stream()
              .filter(port -> port.getPort() != null && nodePort == port.getPort())
              .findAny();
      if (conflictPort.isPresent()) {
        Optional<String> conflictIp = externalIPs.stream()
                .filter(nodeIpSet::contains)
                .findAny();
        if (conflictIp.isPresent()) {
          String detail = String.format("nodePort:%d is conflict with externalIP:port %s:%d in service:%s in namespace:%s"
                  , nodePort, conflictIp.get(), nodePort, service.getMetadata().getName(), service.getMetadata().getNamespace());
          result.setAssignable(false);
          result.setDetail(detail);
          return false;
        }
      }
    }

    return true;
  }

  public List<ExternalIPCheckResult> isExternalIPsAndPortAssignable(List<String> externalIPs,
                                                                    List<Integer> portList) {
    List<Service> serviceList = clientManager.getClient()
            .services()
            .inAnyNamespace()
            .list()
            .getItems();

        /*
        find all the external ip which are node ip
         */
    Set<String> nodeIpSet = getNodeIpSet();

    List<ExternalIPCheckResult> resultList = new ArrayList<>();

    for (Service service : serviceList) {
      checkService(externalIPs, portList, nodeIpSet, resultList, service);
    }
    return resultList;
  }

  private void checkService(List<String> externalIPsForCheck,
                            List<Integer> portList,
                            Set<String> nodeIpSet,
                            List<ExternalIPCheckResult> resultList,
                            Service service) {
    List<String> svcExIPs = service.getSpec().getExternalIPs();
    List<ServicePort> svcPorts = service.getSpec().getPorts();

    for (String externalIP : externalIPsForCheck) {

      boolean isNodeIP = nodeIpSet.contains(externalIP);

      for (ServicePort svcPort : svcPorts) {
        if (svcExIPs.contains(externalIP) && portList.contains(svcPort.getPort())) {
          ExternalIPCheckResult tmp = new ExternalIPCheckResult();
          tmp.setAssignable(false);
          tmp.setExternalIP(externalIP);
          tmp.setPort(svcPort.getPort());
          String detail = String.format("externalIP:port %s:%d is already assigned to service:%s in namespace:%s"
                  , externalIP, svcPort.getPort(), service.getMetadata().getName(), service.getMetadata().getNamespace());
          tmp.setDetail(detail);
          portList.remove(svcPort.getPort());
          resultList.add(tmp);
        }

        if (isNodeIP && portList.contains(svcPort.getNodePort())) {
          ExternalIPCheckResult tmp = new ExternalIPCheckResult();
          tmp.setAssignable(false);
          tmp.setExternalIP(externalIP);
          tmp.setPort(svcPort.getNodePort());
          String detail = String.format("externalIP:%s is node ip and port:%d is already assigned to nodePort in service:%s in namespace:%s"
                  , externalIP, svcPort.getNodePort(), service.getMetadata().getName(), service.getMetadata().getNamespace());
          tmp.setDetail(detail);
          portList.remove(svcPort.getNodePort());
          resultList.add(tmp);
        }
      }
    }
  }

  public ExternalIpNodePortCheckRes validateSvcExternalIpAndNodePort(Service service) {
    ExternalIpNodePortCheckRes result = new ExternalIpNodePortCheckRes();
    result.setAssignable(true);

    String serviceName = service.getMetadata().getName();
    String namespaceName = service.getMetadata().getNamespace();

    ServiceSpec spec = service.getSpec();

    List<String> externalIpList = Optional.ofNullable(spec.getExternalIPs()).orElseGet(ArrayList::new);

    List<Integer> portList = Optional
            .ofNullable(spec.getPorts())
            .orElseGet(ArrayList::new)
            .stream()
            .map(ServicePort::getPort)
            .filter(Objects::nonNull)
            .collect(Collectors.toList());

    List<Integer> nodePortList;
    if (ServiceType.NodePort.name().equals(service.getSpec().getType())) {
      nodePortList = Optional.ofNullable(spec.getPorts())
              .orElseGet(ArrayList::new)
              .stream()
              .map(ServicePort::getNodePort)
              .filter(Objects::nonNull)
              .collect(Collectors.toList());
    } else {
      nodePortList = new ArrayList<>();
    }

    if (nodePortList.isEmpty()) {
      if (externalIpList.isEmpty() || portList.isEmpty()) {
        return result;
      }
    } else {
      //verify nodeport in the right range
      List<Integer> notInRangeNodePorts = Optional.ofNullable(nodePortList)
              .orElseGet(ArrayList::new)
              .stream()
              .filter(port->{
                if (platformNamespaces.contains(namespaceName)) {
                  return port < PLATFORM_NODEPORTS_MIN || port > PLATFORM_NODEPORTS_MAX;
                } else {
                  return port < USER_NAMESPACE_NODEPORTS_MIN || port > USER_NAMESPACE_NODEPORTS_MAX;
                }
              })
              .collect(Collectors.toList());
      if (notInRangeNodePorts.size() > 0) {
        result.setAssignable(false);
        result.setNotInRange(true);
        return result;
      }
    }

    List<Service> serviceList = clientManager.getClient()
            .services()
            .inAnyNamespace()
            .list()
            .getItems();


    serviceList = Optional.ofNullable(serviceList)
            .orElseGet(ArrayList::new);

    Set<String> nodeIpSet = getNodeIpSet();

    ExternalIpNodePortCheckRes res = checkServiceList(serviceList, serviceName, namespaceName, nodePortList, externalIpList, portList, nodeIpSet);
    if (res != null) {
      return res;
    }

    return result;
  }

  private ExternalIpNodePortCheckRes checkServiceList(List<Service> serviceList,
                                                      String serviceName,
                                                      String namespaceName,
                                                      List<Integer> nodePortList,
                                                      List<String> externalIpList,
                                                      List<Integer> portList,
                                                      Set<String> nodeIpSet) {
    for (Service svc : serviceList) {

            /*
            skip the checked service self
             */
      ObjectMeta metadata = svc.getMetadata();
      if (metadata.getName().equals(serviceName) &&
              metadata.getNamespace().equals(namespaceName)) {
        continue;
      }

      List<ServicePort> svcPorts = Optional
              .ofNullable(svc.getSpec().getPorts())
              .orElseGet(ArrayList::new);
      List<String> svcExternalIps = Optional
              .ofNullable(svc.getSpec().getExternalIPs())
              .orElseGet(ArrayList::new);

      Set<Integer> svcNodePortSet = svcPorts.stream()
              .map(ServicePort::getNodePort)
              .filter(Objects::nonNull)
              .collect(Collectors.toSet());

      Set<Integer> svcPortSet = svcPorts.stream()
              .map(ServicePort::getPort)
              .filter(Objects::nonNull)
              .collect(Collectors.toSet());

            /*
            check node port
            */
      if (!nodePortList.isEmpty()) {
        ExternalIpNodePortCheckRes res = checkNodePort(svc, nodeIpSet, nodePortList, svcExternalIps, svcPortSet, svcNodePortSet);
        if (res != null) {
          return res;
        }
      }

      if (!externalIpList.isEmpty() && !portList.isEmpty()) {
        ExternalIpNodePortCheckRes res = checkExternalIp(svc, nodeIpSet, externalIpList, portList, svcExternalIps, svcPortSet, svcNodePortSet);
        if (res != null) {
          return res;
        }
      }
    }
    return null;
  }

  private Set<String> getNodeIpSet() {
    List<Node> nodeList = clientManager.getClient()
            .nodes()
            .list()
            .getItems();
    return Optional.ofNullable(nodeList)
            .orElseGet(ArrayList::new)
            .stream()
            .map(node -> node.getSpec().getExternalID())
            .filter(Objects::nonNull)
            .collect(Collectors.toSet());
  }

  private ExternalIpNodePortCheckRes checkNodePort(Service svc,
                                                   Set<String> nodeIpSet,
                                                   List<Integer> nodePortList,
                                                   List<String> svcExternalIps,
                                                   Set<Integer> svcPortSet,
                                                   Set<Integer> svcNodePortSet) {
    for (Integer nodePort : nodePortList) {
      if (svcNodePortSet.contains(nodePort)) {
        return new ExternalIpNodePortCheckRes.Builder()
                .conflictSvc(svc.getMetadata().getName())
                .conflictSvcNs(svc.getMetadata().getNamespace())
                .nodePort(nodePort)
                .isNodePortConflict(true)
                .conflictWithNodePort(true)
                .build();
      }

      if (svcPortSet.contains(nodePort)) {
        Optional<String> conflictExternalIp = svcExternalIps.stream()
                .filter(nodeIpSet::contains)
                .findAny();
        if (conflictExternalIp.isPresent()) {
          return new ExternalIpNodePortCheckRes.Builder()
                  .conflictSvc(svc.getMetadata().getName())
                  .conflictSvcNs(svc.getMetadata().getNamespace())
                  .externalIP(conflictExternalIp.get())
                  .nodePort(nodePort)
                  .port(nodePort)
                  .isNodePortConflict(true)
                  .build();
        }
      }
    }

    return null;
  }

  private ExternalIpNodePortCheckRes checkExternalIp(Service svc,
                                                     Set<String> nodeIpSet,
                                                     List<String> externalIpList,
                                                     List<Integer> portList,
                                                     List<String> svcExternalIps,
                                                     Set<Integer> svcPortSet,
                                                     Set<Integer> svcNodePortSet) {
    for (String externalIp : externalIpList) {
      if (svcExternalIps.contains(externalIp)) {
        Optional<Integer> conflictPort = portList.stream()
                .filter(svcPortSet::contains)
                .findAny();

        if (conflictPort.isPresent()) {
          return new ExternalIpNodePortCheckRes.Builder()
                  .conflictSvc(svc.getMetadata().getName())
                  .conflictSvcNs(svc.getMetadata().getNamespace())
                  .externalIP(externalIp)
                  .port(conflictPort.get())
                  .isNodePortConflict(true)
                  .build();
        }
      }

      if (nodeIpSet.contains(externalIp)) {
        Optional<Integer> conflictNodePort = portList.stream()
                .filter(svcNodePortSet::contains)
                .findAny();
        if (conflictNodePort.isPresent()) {

          return new ExternalIpNodePortCheckRes.Builder()
                  .conflictSvc(svc.getMetadata().getName())
                  .conflictSvcNs(svc.getMetadata().getNamespace())
                  .externalIP(externalIp)
                  .nodePort(conflictNodePort.get())
                  .port(conflictNodePort.get())
                  .conflictWithNodePort(true)
                  .build();
        }
      }
    }

    return null;
  }
}
