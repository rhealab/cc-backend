package cc.backend.kubernetes;

import cc.backend.EnnProperties;
import cc.backend.audit.request.EnnContext;
import cc.backend.common.utils.OkHttpClientUtils;
import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.ConfigBuilder;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author wangchunyang@gmail.com
 */

@Named
@RefreshScope
public class KubernetesClientManager {
    private static final Logger logger = LoggerFactory.getLogger(KubernetesClientManager.class);

    @Inject
    private EnnProperties ennProperties;

    private Map<String, KubernetesClient> clientMap;

    @PostConstruct
    private void init() {
        clientMap = ennProperties.getClusters().entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> createClient(entry.getValue().getKubernetes())));
    }

    private KubernetesClient createClient(EnnProperties.Cluster.Kubernetes k8sProperties) {
        ConfigBuilder configBuilder = new ConfigBuilder()
                .withMasterUrl(k8sProperties.getMasterUrl())
                .withConnectionTimeout(Math.toIntExact(ennProperties.getHttp().getConnectTimeoutMillis()))
                .withRequestTimeout(Math.toIntExact(ennProperties.getHttp().getReadTimeoutMillis()));
        if (!k8sProperties.isLoginWithToken()) {
            configBuilder
                    .withUsername(k8sProperties.getAdmin().getUsername())
                    .withPassword(k8sProperties.getAdmin().getPassword())
                    .withTrustCerts(true);
        } else {
            configBuilder
                    .withOauthToken(k8sProperties.getToken())
                    .withTrustCerts(true);
        }

        Config config = configBuilder.build();

        logger.info("kubernetes.masterUrl={}", k8sProperties.getMasterUrl());

        return new DefaultKubernetesClient(OkHttpClientUtils.createK8sHttpClient(config), config);
    }

    public KubernetesClient getClient() {
        return getClient(EnnContext.getClusterName());
    }

    public KubernetesClient getClient(String clusterName) {
        return clientMap.get(clusterName);
    }
}
