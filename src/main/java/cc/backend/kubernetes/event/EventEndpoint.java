package cc.backend.kubernetes.event;

import io.fabric8.kubernetes.api.model.Event;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/11.
 * @author yanzhixiang on 2017/8/23
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes/namespaces/{namespaceName}/apps/{appName}/events")
@Api(value = "Event", description = "Info about events.", produces = "application/json")
public class EventEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(EventEndpoint.class);

    @Inject
    private EventsManagement management;

    @GET
    @Path("{eventName}")
    @ApiOperation(value = "Get event info.", response = EventDto.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "namespace,app or event not exists")})
    public Response getEvents(@PathParam("namespaceName") String namespaceName,
                              @PathParam("appName") String appName,
                              @PathParam("eventName") String eventName) {
        Event event = management.getEventByName(namespaceName, eventName);
        return Response.ok(EventDto.from(event)).build();
    }

    @GET
    @Path("{eventName}/json")
    @ApiOperation(value = "Get event info of k8s original format.", response = Event.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "namespace,app or event not exists")})
    public Response getEventsJson(@PathParam("namespaceName") String namespaceName,
                                  @PathParam("eventName") String eventName) {
        Event event = management.getEventByName(namespaceName, eventName);
        return Response.ok(event).build();
    }

    @GET
    @Path("by_deployment/{deploymentName}")
    @ApiOperation(value = "Get events in deployment.", response = EventDto.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "namespace,app or deployment not exists")})
    public Response getEventsByDeployment(@PathParam("namespaceName") String namespaceName,
                                          @PathParam("appName") String appName,
                                          @PathParam("deploymentName") String deploymentName) {
        List<Event> events = management.getEventsByDeployment(namespaceName, appName, deploymentName);
        return Response.ok(EventDto.from(events)).build();
    }

    @GET
    @Path("by_deployment/{deploymentName}/json")
    @ApiOperation(value = "Get events in deployment of k8s original format.", response = Event.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "namespace,app or deployment not exists")})
    public Response getEventsJsonByDeployment(@PathParam("namespaceName") String namespaceName,
                                              @PathParam("appName") String appName,
                                              @PathParam("deploymentName") String deploymentName) {
        List<Event> events = management.getEventsByDeployment(namespaceName, appName, deploymentName);
        return Response.ok(events).build();
    }

    @GET
    @Path("by_service/{serviceName}")
    @ApiOperation(value = "Get events in service.", response = EventDto.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "namespace,app or service not exists")})
    public Response getEventsByService(@PathParam("namespaceName") String namespaceName,
                                       @PathParam("appName") String appName,
                                       @PathParam("serviceName") String serviceName) {
        List<Event> events = management.getEventsByService(namespaceName, appName, serviceName);
        return Response.ok(EventDto.from(events)).build();
    }

    @GET
    @Path("by_service/{serviceName}/json")
    @ApiOperation(value = "Get events in service of k8s original format.", response = Event.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "namespace,app or service not exists")})
    public Response getEventsJsonByService(@PathParam("namespaceName") String namespaceName,
                                           @PathParam("appName") String appName,
                                           @PathParam("serviceName") String serviceName) {
        List<Event> events = management.getEventsByService(namespaceName, appName, serviceName);
        return Response.ok(events).build();
    }

    @GET
    @Path("by_statefulset/{statefulSetName}")
    @ApiOperation(value = "Get events in statefulset.", response = EventDto.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "namespace,app or statefulset not exists")})
    public Response getEventsByStatefulSet(@PathParam("namespaceName") String namespaceName,
                                           @PathParam("appName") String appName,
                                           @PathParam("statefulSetName") String statefulSetName) {
        List<Event> events = management.getEventsByStatefulSet(namespaceName, appName, statefulSetName);
        return Response.ok(EventDto.from(events)).build();
    }

    @GET
    @Path("by_statefulset/{statefulSetName}/json")
    @ApiOperation(value = "Get events in statefulset of k8s original format.", response = Event.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "namespace,app or statefulset not exists")})
    public Response getEventsJsonByStatefulSet(@PathParam("namespaceName") String namespaceName,
                                               @PathParam("appName") String appName,
                                               @PathParam("statefulSetName") String statefulSetName) {
        List<Event> events = management.getEventsByStatefulSet(namespaceName, appName, statefulSetName);
        return Response.ok(events).build();
    }


    @GET
    @Path("by_pod/{podName}")
    @ApiOperation(value = "Get events in pod.", response = EventDto.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "namespace,app or pod not exists")})
    public Response getEventsByPod(@PathParam("namespaceName") String namespaceName,
                                   @PathParam("appName") String appName,
                                   @PathParam("podName") String podName) {
        List<Event> events = management.getEventsByPod(namespaceName, appName, podName);
        return Response.ok(EventDto.from(events)).build();
    }

    @GET
    @Path("by_pod/{podName}/json")
    @ApiOperation(value = "Get events in pod of k8s original format.", response = Event.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "namespace,app or pod not exists")})
    public Response getEventsJsonByPod(@PathParam("namespaceName") String namespaceName,
                                       @PathParam("appName") String appName,
                                       @PathParam("podName") String podName) {
        List<Event> events = management.getEventsByPod(namespaceName, appName, podName);
        return Response.ok(events).build();
    }

}
