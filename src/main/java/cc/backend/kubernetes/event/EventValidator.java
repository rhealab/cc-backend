package cc.backend.kubernetes.event;

import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.Event;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2018/1/2.
 */
@Component
public class EventValidator {
    @Inject
    private KubernetesClientManager clientManager;

    public Event validateEventShouldExists(String namespaceName, String eventName) {
        Event event = clientManager.getClient()
                .events()
                .inNamespace(namespaceName)
                .withName(eventName)
                .get();
        if (event == null) {
            throw new CcException(BackendReturnCodeNameConstants.EVENT_NOT_EXISTS, ImmutableMap.of(
                    "namespace", namespaceName,
                    "event", eventName));
        }

        return event;
    }
}
