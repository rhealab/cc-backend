package cc.backend.kubernetes.event;

import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.service.ServicesManagement;
import cc.backend.kubernetes.workload.deployment.service.DeploymentsManagement;
import cc.backend.kubernetes.workload.statefulset.StatefulSetManagement;
import com.google.common.base.Strings;
import io.fabric8.kubernetes.api.model.Event;
import io.fabric8.kubernetes.api.model.HasMetadata;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/11.
 */
@Component
public class EventsManagement {
    @Inject
    private KubernetesClientManager clientManager;

    @Inject
    private EventValidator validator;

    @Inject
    private DeploymentsManagement deploymentsManagement;

    @Inject
    private ServicesManagement servicesManagement;

    @Inject
    private StatefulSetManagement statefulSetManagement;

    public Event getEventByName(String namespaceName, String eventName) {
        return validator.validateEventShouldExists(namespaceName, eventName);
    }

    public List<Event> getEventsByDeployment(String namespaceName,
                                             String appName,
                                             String deploymentName) {
        Deployment deployment = deploymentsManagement.getDeployment(namespaceName, appName, deploymentName);

        return getEvents(namespaceName, deployment);
    }

    public List<Event> getEventsByService(String namespaceName, String appName, String serviceName) {
        Service service = servicesManagement.getService(namespaceName, appName, serviceName);

        return getEvents(namespaceName, service);
    }

    public List<Event> getEventsByStatefulSet(String namespaceName, String appName, String statefulSetName) {
        StatefulSet statefulSet = statefulSetManagement.getStatefulSet(namespaceName, appName, statefulSetName);

        return getEvents(namespaceName, statefulSet);
    }

    public List<Event> getEventsByPod(String namespaceName, String appName, String podName) {
        Pod pod = clientManager.getClient()
                .pods()
                .inNamespace(namespaceName)
                .withName(podName)
                .get();

        if (pod != null) {
            return getEvents(namespaceName, pod);
        }

        return new ArrayList<>();
    }

    private List<Event> getEvents(String namespaceName, HasMetadata resource) {
        String uid = resource.getMetadata().getUid();

        List<Event> eventList = null;

        if (!Strings.isNullOrEmpty(uid)) {
            eventList = clientManager.getClient()
                    .events()
                    .inNamespace(namespaceName)
                    .withField("involvedObject.uid", uid)
                    .list()
                    .getItems();
        }

        return Optional.ofNullable(eventList).orElseGet(ArrayList::new);
    }

}
