package cc.backend.kubernetes.event;

import cc.backend.common.utils.DateUtils;
import io.fabric8.kubernetes.api.model.Event;
import io.fabric8.kubernetes.api.model.EventSource;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.ObjectReference;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/11.
 */
public class EventDto {
    private String name;
    private String uid;
    private String namespace;
    private int count;
    private String message;
    private String sourceComponent;
    private String sourceHost;
    private String involvedObjectName;
    private String involvedObjectKind;
    private String subObject;
    private String type;
    private String reason;
    private Date firstTime;
    private Date lastTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSourceComponent() {
        return sourceComponent;
    }

    public void setSourceComponent(String sourceComponent) {
        this.sourceComponent = sourceComponent;
    }

    public String getSourceHost() {
        return sourceHost;
    }

    public void setSourceHost(String sourceHost) {
        this.sourceHost = sourceHost;
    }

    public String getInvolvedObjectName() {
        return involvedObjectName;
    }

    public void setInvolvedObjectName(String involvedObjectName) {
        this.involvedObjectName = involvedObjectName;
    }

    public String getInvolvedObjectKind() {
        return involvedObjectKind;
    }

    public void setInvolvedObjectKind(String involvedObjectKind) {
        this.involvedObjectKind = involvedObjectKind;
    }

    public String getSubObject() {
        return subObject;
    }

    public void setSubObject(String subObject) {
        this.subObject = subObject;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getFirstTime() {
        return firstTime;
    }

    public void setFirstTime(Date firstTime) {
        this.firstTime = firstTime;
    }

    public Date getLastTime() {
        return lastTime;
    }

    public void setLastTime(Date lastTime) {
        this.lastTime = lastTime;
    }

    @Override
    public String toString() {
        return "EventDto{" +
                "name='" + name + '\'' +
                ", uid='" + uid + '\'' +
                ", namespace='" + namespace + '\'' +
                ", count=" + count +
                ", message='" + message + '\'' +
                ", sourceComponent='" + sourceComponent + '\'' +
                ", sourceHost='" + sourceHost + '\'' +
                ", involvedObjectName='" + involvedObjectName + '\'' +
                ", involvedObjectKind='" + involvedObjectKind + '\'' +
                ", subObject='" + subObject + '\'' +
                ", type='" + type + '\'' +
                ", reason='" + reason + '\'' +
                ", firstTime=" + firstTime +
                ", lastTime=" + lastTime +
                '}';
    }

    /**
     * @param events not null
     * @return composed event list
     */
    public static List<EventDto> from(List<Event> events) {
        List<EventDto> dtos = new ArrayList<>();

        for (Event event : events) {
            dtos.add(from(event));
        }

        return dtos;
    }

    /**
     * @param event not null
     * @return composed event
     */
    public static EventDto from(Event event) {
        EventDto dto = new EventDto();
        ObjectMeta metadata = event.getMetadata();
        EventSource source = event.getSource();
        ObjectReference involvedObject = event.getInvolvedObject();

        dto.setName(metadata.getName());
        dto.setUid(metadata.getUid());
        dto.setNamespace(metadata.getNamespace());
        dto.setCount(event.getCount());
        dto.setMessage(event.getMessage());
        dto.setSourceComponent(source.getComponent());
        dto.setSourceHost(source.getHost());
        dto.setInvolvedObjectName(involvedObject.getName());
        dto.setInvolvedObjectKind(involvedObject.getKind());
        dto.setSubObject(involvedObject.getFieldPath());
        dto.setType(event.getType());
        dto.setReason(event.getReason());
        dto.setFirstTime(DateUtils.parseK8sDate(event.getFirstTimestamp()));
        dto.setLastTime(DateUtils.parseK8sDate(event.getLastTimestamp()));
        return dto;
    }
}
