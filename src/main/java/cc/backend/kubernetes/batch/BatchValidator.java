package cc.backend.kubernetes.batch;

import cc.backend.kubernetes.namespaces.data.NamespaceExtras;
import cc.backend.kubernetes.namespaces.data.NamespaceExtrasRepository;
import cc.backend.kubernetes.workload.deployment.validator.DeploymentsValidator;
import cc.backend.kubernetes.workload.statefulset.StatefulSetValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.inject.Inject;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/11/9.
 */
@Component
public class BatchValidator {
    private static final Logger logger = LoggerFactory.getLogger(BatchValidator.class);

    @Inject
    private NamespaceExtrasRepository namespaceExtrasRepository;

    @Inject
    private DeploymentsValidator deploymentsValidator;

    @Inject
    private StatefulSetValidator statefulSetValidator;

    public void validateCriticalPod(String namespaceName, BatchItems batchItems) {
        if (CollectionUtils.isEmpty(batchItems.getDeployments())
                && CollectionUtils.isEmpty(batchItems.getStatefulSets())) {
            return;
        }

        NamespaceExtras namespaceExtras = namespaceExtrasRepository.findByNameAndDeletedIsFalse(namespaceName);
        if (!CollectionUtils.isEmpty(batchItems.getDeployments())) {
            deploymentsValidator.validateCriticalPod(namespaceName, namespaceExtras, batchItems.getDeployments());
        }

        if (!CollectionUtils.isEmpty(batchItems.getStatefulSets())) {
            statefulSetValidator.validateCriticalPod(namespaceName, namespaceExtras, batchItems.getStatefulSets());
        }
    }
}
