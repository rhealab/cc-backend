package cc.backend.kubernetes.batch.form.dto;

import cc.backend.kubernetes.service.dto.EnnIngress;
import cc.backend.kubernetes.utils.ResourceLabelHelper;
import cc.backend.kubernetes.service.ServiceHelper;
import cc.backend.kubernetes.storage.domain.Storage;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.fabric8.kubernetes.api.model.*;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import io.fabric8.kubernetes.api.model.extensions.StatefulSetSpec;
import org.springframework.util.StringUtils;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static cc.backend.kubernetes.Constants.APP_LABEL;
import static cc.backend.kubernetes.Constants.CRITICAL_POD_ANNOTATION;

/**
 * a mixture of deployment service and storage
 *
 * @author NormanWang06@gmail.com (jinxin) on 2017/7/6.
 */
public class StatefulSetFormMixture extends BaseFormMixture implements ConsoleMetadata {

    private String statefulSetServiceName;


    /**
     * false will not update a service
     */
    @JsonProperty("createService")
    @Deprecated
    protected Boolean serviceExists;

    @Valid
    private List<VolumeClaimTemplateForm> volumeClaimTemplates;

    public String getStatefulSetServiceName() {
        return statefulSetServiceName;
    }

    public void setStatefulSetServiceName(String statefulSetServiceName) {
        this.statefulSetServiceName = statefulSetServiceName;
    }

    public Boolean getServiceExists() {
        return serviceExists;
    }

    public void setServiceExists(Boolean serviceExists) {
        this.serviceExists = serviceExists;
    }

    public List<VolumeClaimTemplateForm> getVolumeClaimTemplates() {
        return volumeClaimTemplates;
    }

    public void setVolumeClaimTemplates(List<VolumeClaimTemplateForm> volumeClaimTemplates) {
        this.volumeClaimTemplates = volumeClaimTemplates;
    }

    @Override
    public String toString() {
        return "StatefulSetFormMixture{" +
                "statefulSetServiceName='" + statefulSetServiceName + '\'' +
                ", serviceExists=" + serviceExists +
                ", volumeClaimTemplates=" + volumeClaimTemplates +
                ", name='" + name + '\'' +
                ", namespace='" + namespace + '\'' +
                ", labels=" + labels +
                ", annotations=" + annotations +
                ", replicas=" + replicas +
                ", revisionHistoryLimit=" + revisionHistoryLimit +
                ", criticalPod=" + criticalPod +
                ", imagePullSecrets=" + imagePullSecrets +
                ", podTemplateLabels=" + podTemplateLabels +
                ", podTemplateAnnotations=" + podTemplateAnnotations +
                ", containers=" + containers +
                ", newStorageList=" + newStorageList +
                ", existsStorageList=" + existsStorageList +
                ", serviceLabels=" + serviceLabels +
                ", serviceAnnotations=" + serviceAnnotations +
                ", serviceType=" + serviceType +
                ", serviceExternalIPs=" + serviceExternalIPs +
                ", servicePorts=" + servicePorts +
                ", serviceSelectors=" + serviceSelectors +
                '}';
    }

    /**
     * convert a stateful set to Mixture
     *
     * @param statefulSet the stateful set for converting
     * @param service     the service for converting
     * @return Mixture or null if stateful set is null
     */
    public static StatefulSetFormMixture from(StatefulSet statefulSet,
                                              Service service,
                                              List<Storage> storageList,
                                              String vipHost) {
        if (statefulSet == null) {
            return null;
        }

        ObjectMeta metadata = statefulSet.getMetadata();
        StatefulSetSpec spec = statefulSet.getSpec();
        ObjectMeta podMetadata = spec.getTemplate().getMetadata();
        PodSpec podSpec = spec.getTemplate().getSpec();

        StatefulSetFormMixture mixture = new StatefulSetFormMixture();
        mixture.setName(metadata.getName());
        mixture.setNamespace(metadata.getNamespace());
        mixture.setLabels(FormLabel.from(metadata.getLabels()));
        mixture.setAnnotations(FormAnnotation.from(metadata.getAnnotations()));
        mixture.setReplicas(spec.getReplicas());
        mixture.setRevisionHistoryLimit(spec.getRevisionHistoryLimit());

        mixture.setStatefulSetServiceName(spec.getServiceName());

        List<LocalObjectReference> imagePullSecrets = podSpec.getImagePullSecrets();
        if (imagePullSecrets != null) {
            mixture.setImagePullSecrets(imagePullSecrets
                    .stream()
                    .map(LocalObjectReference::getName)
                    .collect(Collectors.toList()));
        }
        mixture.setPodTemplateLabels(FormLabel.from(podMetadata.getLabels()));
        Map<String, String> podAnnotations = podMetadata.getAnnotations();
        if (podAnnotations != null) {
            mixture.setCriticalPod("true".equalsIgnoreCase(podAnnotations.get(CRITICAL_POD_ANNOTATION)));
            mixture.setPodTemplateAnnotations(FormAnnotation.from(podAnnotations));
        }
        mixture.setContainers(FormContainer.from(podSpec.getContainers()));

        mixture.setExistsStorageList(FormStorage.from(storageList));
        mixture.setVolumeClaimTemplates(VolumeClaimTemplateForm.from(statefulSet.getSpec().getVolumeClaimTemplates()));

        mixture.setServiceExists(Boolean.TRUE);
        fillService(service, mixture, vipHost);

        return mixture;
    }

    public static StatefulSet toStatefulSet(String appName,
                                            StatefulSetFormMixture mixture,
                                            List<Storage> storageList) {
        StatefulSet statefulSet = new StatefulSet();

        ObjectMeta metadata = composeMetadata(appName, mixture);
        statefulSet.setMetadata(metadata);

        StatefulSetSpec spec = composeStatefulSetSpec(mixture, storageList);
        statefulSet.setSpec(spec);

        return statefulSet;
    }

    private static StatefulSetSpec composeStatefulSetSpec(StatefulSetFormMixture mixture,
                                                          List<Storage> storageList) {
        StatefulSetSpec spec = new StatefulSetSpec();
        spec.setRevisionHistoryLimit(mixture.getRevisionHistoryLimit());
        spec.setReplicas(mixture.getReplicas());
        spec.setSelector(FormLabel.toSelector(mixture.getPodTemplateLabels()));

        String serviceName = mixture.getStatefulSetServiceName();
        if (StringUtils.isEmpty(serviceName)) {
            serviceName = mixture.getName();
        }
        spec.setServiceName(serviceName);

        //StatefulSet spec pod template
        PodTemplateSpec template = composePodTemplateSpec(mixture, storageList);
        spec.setTemplate(template);
        spec.setVolumeClaimTemplates(VolumeClaimTemplateForm.to(mixture.getNamespace(), mixture.getVolumeClaimTemplates()));
        return spec;
    }

    /**
     * compose a service from Mixture
     *
     * @param appName the app name
     * @param mixture the Mixture
     * @return composed service or null
     */
    public static Service toService(String appName, StatefulSetFormMixture mixture) {
        Service service = new Service();

        ObjectMeta metadata = new ObjectMeta();
        if (!StringUtils.isEmpty(mixture.getStatefulSetServiceName())) {
            metadata.setName(mixture.getStatefulSetServiceName());
        } else {
            metadata.setName(mixture.getName());
        }

        metadata.setNamespace(mixture.getNamespace());
        metadata.setLabels(FormLabel.toMap(mixture.getServiceLabels()));

        Map<String, String> annotations = FormAnnotation.toMap(mixture.getServiceAnnotations());
        if (mixture.getEnnIngress() != null) {
            annotations.putAll(EnnIngress.toAnnotations(mixture.getEnnIngress()));
        }
        metadata.setAnnotations(annotations);

        ResourceLabelHelper.addLabel(service, APP_LABEL, appName);
        service.setMetadata(metadata);

        service.setSpec(ServiceHelper.composeServiceSpec(mixture));

        return service;
    }
}
