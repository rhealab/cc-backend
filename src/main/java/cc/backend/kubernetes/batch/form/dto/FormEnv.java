package cc.backend.kubernetes.batch.form.dto;

import cc.backend.kubernetes.Constants;
import io.fabric8.kubernetes.api.model.EnvVar;

import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/20.
 */
public class FormEnv {
    @Pattern(regexp = Constants.LABEL_NAME_PATTERN, message = "env name should match reg exp:" + Constants.LABEL_NAME_PATTERN)
    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static List<FormEnv> from(List<EnvVar> envVars) {
        List<FormEnv> formEnvs = new ArrayList<>();

        if (envVars != null) {
            for (EnvVar envVar : envVars) {
                FormEnv formEnv = new FormEnv();
                formEnv.setName(envVar.getName());
                formEnv.setValue(envVar.getValue());
                formEnvs.add(formEnv);
            }
        }

        return formEnvs;
    }

    public static List<EnvVar> toEnvVar(List<FormEnv> formEnvs) {
        List<EnvVar> envVars = new ArrayList<>();

        if (formEnvs != null) {
            for (FormEnv formEnv : formEnvs) {
                EnvVar envVar = new EnvVar();
                envVar.setName(formEnv.getName());
                envVar.setValue(formEnv.getValue());
                envVars.add(envVar);
            }
        }
        return envVars;
    }
}
