package cc.backend.kubernetes.batch.form.deprecated;

import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.kubernetes.batch.form.MixtureManagement;
import cc.backend.kubernetes.batch.form.dto.FormDeploymentServiceCreateRequest;
import cc.backend.kubernetes.batch.form.dto.FormLabel;
import cc.backend.kubernetes.batch.form.dto.ParsedForm;
import cc.backend.kubernetes.service.ServiceHelper;
import cc.backend.kubernetes.service.ServiceValidator;
import cc.backend.kubernetes.service.ServicesManagement;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.workload.deployment.service.DeploymentsManagement;
import cc.backend.kubernetes.workload.deployment.validator.DeploymentsValidator;
import cc.lib.retcode.CcException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.yaml.snakeyaml.Yaml;

import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/20.
 */
@Component
@Deprecated
public class FormManagement {
    private static final Logger logger = LoggerFactory.getLogger(DeploymentsManagement.class);

    @Inject
    private DeploymentsManagement deploymentsManagement;

    @Inject
    private FormValidator validator;

    @Inject
    private DeploymentsValidator deploymentsValidator;

    @Inject
    private ServicesManagement servicesManagement;

    @Inject
    private MixtureManagement mixtureManagement;

    @Inject
    private FormDeploymentValidator formDeploymentValidator;

    @Inject
    private WorkloadComputeQuotaValidator workloadComputeQuotaValidator;

    @Inject
    private ServiceValidator serviceValidator;

    public void create(String namespaceName,
                       String appName,
                       FormDeploymentServiceCreateRequest request) {
        request.setNamespace(namespaceName);

        formDeploymentValidator.validateCriticalPod(namespaceName, request);
        deploymentsValidator.validateDeploymentShouldNotExists(namespaceName, request.getName());
        List<Storage> storageList = formDeploymentValidator.validateStorageExists(namespaceName, request);
        formDeploymentValidator.validateStorageType(namespaceName, storageList);
        formDeploymentValidator.validateStorageQuotaForCreate(namespaceName, storageList, request);

        Deployment deployment = FormDeployment.to(namespaceName, appName, request, storageList);

        Service service = composeServiceByForm(namespaceName, request);

        serviceValidator.validateClusterIP(service);
        serviceValidator.validatePortProtocol(namespaceName, appName, service);
        serviceValidator.validateSvcNodePortAndExternalIp(service);

        workloadComputeQuotaValidator.validateDeploymentComputeQuota(
                namespaceName, appName, deployment, WorkloadComputeQuotaValidator.ActionType.CREATE);

        mixtureManagement.createStorage(namespaceName, storageList);

        deploymentsManagement.create(namespaceName, appName, deployment);
        if (service != null) {
            servicesManagement.createWithAudit(namespaceName, appName, service);
        }
    }

    public void createFromParsedForm(String namespaceName, String appName, ParsedForm parsedForm) {
        List<Deployment> deployments = parsedForm.getDeployments();
        List<Service> services = parsedForm.getServices();

        //TODO validate all at first
        for (Deployment deployment : deployments) {
            validator.validateDeploymentForCreate(namespaceName, appName, deployment);
            deploymentsManagement.create(namespaceName, appName, deployment);
        }

        for (Service service : services) {
            servicesManagement.createWithAudit(namespaceName, appName, service);
        }
    }

    public void createFromYml(String namespaceName, String appName, String ymlStr) {
        Yaml parser = new Yaml();
        Iterable<Object> objects = parser.loadAll(ymlStr);
        ParsedForm parsedForm = composeParsedFormByObject(objects);
        logger.info("{}", parsedForm);
        createFromParsedForm(namespaceName, appName, parsedForm);
    }

    public void createFromJson(String namespaceName, String appName, String json) {
        ObjectMapper mapper = new ObjectMapper();
        List<Object> myObjects = new ArrayList<>();
        try {
            myObjects = mapper.readValue(json, new TypeReference<List<Object>>() {
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        ParsedForm parsedForm = composeParsedFormByObject(myObjects);
        logger.info("{}", parsedForm);
        createFromParsedForm(namespaceName, appName, parsedForm);
    }


    private ParsedForm composeParsedFormByObject(Iterable<Object> objects) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        List<Service> services = new ArrayList<>();
        List<Deployment> deployments = new ArrayList<>();

        try {
            for (Object o : objects) {
                if (o == null) {
                    continue;
                }

                if (o instanceof Map) {
                    Object kind = ((Map) o).get("kind");
                    if ("Service".equals(kind)) {
                        byte[] serviceBytes = objectMapper.writeValueAsBytes(o);
                        Service service = objectMapper.readValue(serviceBytes, Service.class);
                        services.add(service);
                    } else if ("Deployment".equals(kind)) {
                        byte[] deploymentBytes = objectMapper.writeValueAsBytes(o);
                        Deployment deployment = objectMapper.readValue(deploymentBytes, Deployment.class);
                        deployments.add(deployment);
                    } else {
                        throw new CcException(BackendReturnCodeNameConstants.PROPERTY_INVALID,
                                ImmutableMap.of("msg", "not supported kind:" + kind,
                                        "param", o));
                    }
                } else {
                    throw new CcException(BackendReturnCodeNameConstants.PROPERTY_INVALID,
                            ImmutableMap.of("msg", "not supported object",
                                    "param", o));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        ParsedForm parsedForm = new ParsedForm();
        parsedForm.setServices(services);
        parsedForm.setDeployments(deployments);
        return parsedForm;
    }

    private Service composeServiceByForm(String namespaceName, FormDeploymentServiceCreateRequest request) {
        if (ServiceHelper.getK8sType(request) == null) {
            return null;
        }

        Service service = new Service();

        ObjectMeta metadata = new ObjectMeta();
        metadata.setName(request.getName());
        metadata.setNamespace(namespaceName);
        metadata.setLabels(FormLabel.toMap(request.getLabels()));
        if (!StringUtils.isEmpty(request.getDescription())) {
            metadata.setAnnotations(ImmutableMap.of("description", request.getDescription()));
        }
        service.setMetadata(metadata);

        service.setSpec(ServiceHelper.composeServiceSpec(request));

        return service;
    }
}
