package cc.backend.kubernetes.batch.form.deprecated;

import cc.backend.kubernetes.apps.validator.AppValidator;
import cc.backend.kubernetes.batch.BatchManagement;
import cc.backend.kubernetes.batch.form.MixtureManagement;
import cc.backend.kubernetes.batch.form.dto.DeploymentFormMixture;
import cc.backend.kubernetes.batch.form.dto.FormDeploymentServiceCreateRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * TODO validate deployment support privilege/hostpath/vip in yaml
 *
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/20.
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes/namespaces/{namespaceName}/apps/{appName}/form")
@Api(value = "Form", description = "The form to create deployment.", produces = "application/json")
@Deprecated
public class FormEndpoint {

    @Inject
    private FormManagement formManagement;

    @Inject
    private AppValidator appValidator;

    @Inject
    private MixtureManagement mixtureManagement;

    @Inject
    private BatchManagement batchManagement;

    /**
     * create deployment, service, storage by a form
     *
     * @param namespaceName namespace name
     * @return response
     */
    @POST
    @ApiOperation(value = "Create deployment storage and service by form. " +
            "When use exists storage, you can just specify id or name. " +
            "Deprecated, use POST /kubernetes/namespaces/{namespaceName}/apps/{appName}/form/mixture/deployments")
    @Path("mixture")
    @Deprecated
    public Response createMixture(@PathParam("namespaceName") String namespaceName,
                                  @PathParam("appName") String appName,
                                  @Valid DeploymentFormMixture request) {
        request.setNamespace(namespaceName);
        appValidator.validateAppShouldExists(namespaceName, appName);
        mixtureManagement.createDeploymentMixture(namespaceName, appName, request);
        return Response.ok().build();
    }

    /**
     * get deployment, service, storage in a form
     *
     * @param namespaceName  the namespace
     * @param appName        the app
     * @param deploymentName the deployment
     * @return the FormMixture
     */
    @GET
    @ApiOperation(value = "get deployment storage and service in a form. " +
            "Deprecated, use GET /kubernetes/namespaces/{namespaceName}/apps/{appName}/form/mixture/deployments/{deploymentName}"
            , response = DeploymentFormMixture.class)
    @Path("mixture/{deploymentName}")
    @ApiResponses(value = {@ApiResponse(code = 404, message = "namespace, app or deployment not exists")})
    @Deprecated
    public Response getMixture(@PathParam("namespaceName") String namespaceName,
                               @PathParam("appName") String appName,
                               @PathParam("deploymentName") String deploymentName) {
        appValidator.validateAppShouldExists(namespaceName, appName);
        return Response.ok(mixtureManagement.getDeploymentMixture(namespaceName, appName, deploymentName)).build();
    }

    /**
     * update deployment, service in a form
     *
     * @param namespaceName  the namespace
     * @param appName        the app
     * @param deploymentName the deployment
     * @param request        the request
     * @return void
     */
    @PUT
    @ApiOperation(value = "update deployment and service in a form. " +
            "Deprecated, use PUT /kubernetes/namespaces/{namespaceName}/apps/{appName}/form/mixture/deployments/{deploymentName}")
    @Path("mixture/{deploymentName}")
    @Deprecated
    public Response updateMixture(@PathParam("namespaceName") String namespaceName,
                                  @PathParam("appName") String appName,
                                  @PathParam("deploymentName") String deploymentName,
                                  @Valid DeploymentFormMixture request) {
        request.setName(deploymentName);
        request.setNamespace(namespaceName);
        appValidator.validateAppShouldExists(namespaceName, appName);
        mixtureManagement.updateDeploymentMixture(namespaceName, appName, request);
        return Response.ok().build();
    }

    /**
     * create deployment, service, storage by a form
     *
     * @param namespaceName namespace name
     * @return response
     */
    @POST
    @ApiOperation(value = "Create deployment by form. " +
            "Deprecated, use POST /kubernetes/namespaces/{namespaceName}/apps/{appName}/form/mixture/deployments")
    @Deprecated
    public Response createFormDeploymentService(@PathParam("namespaceName") String namespaceName,
                                                @PathParam("appName") String appName,
                                                @Valid FormDeploymentServiceCreateRequest request) {
        appValidator.validateAppShouldExists(namespaceName, appName);
        formManagement.create(namespaceName, appName, request);
        return Response.ok().build();
    }

    /**
     * @deprecated use batch api
     * kubernetes/namespaces/{namespaceName}/apps/{appName}/batch/k8s_yaml
     */
    @POST
    @Path("yml")
    @ApiOperation(value = "Create deployment service by yml. " +
            "Deprecated, use POST kubernetes/namespaces/{namespaceName}/apps/{appName}/batch/k8s_yaml")
    @Deprecated
    public Response createFromYml(@PathParam("namespaceName") String namespaceName,
                                  @PathParam("appName") String appName,
                                  Object ymlStr) {
        appValidator.validateAppShouldExists(namespaceName, appName);
        batchManagement.createFromK8sYaml(namespaceName, appName, ymlStr.toString());
        return Response.ok().build();
    }

    /**
     * @deprecated use batch api
     * kubernetes/namespaces/{namespaceName}/apps/{appName}/batch/k8s_json
     */
    @POST
    @Path("json")
    @ApiOperation(value = "Create deployment service by json. " +
            "Deprecated, use POST kubernetes/namespaces/{namespaceName}/apps/{appName}/batch/k8s_json")
    @Deprecated
    public Response createFromJson(@PathParam("namespaceName") String namespaceName,
                                   @PathParam("appName") String appName,
                                   Object json) {
        appValidator.validateAppShouldExists(namespaceName, appName);
        batchManagement.createFromK8sJson(namespaceName, appName, json.toString());
        return Response.ok().build();
    }
}
