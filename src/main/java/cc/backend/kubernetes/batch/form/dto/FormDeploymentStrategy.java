package cc.backend.kubernetes.batch.form.dto;

import io.fabric8.kubernetes.api.model.IntOrString;
import io.fabric8.kubernetes.api.model.extensions.DeploymentStrategy;
import io.fabric8.kubernetes.api.model.extensions.RollingUpdateDeployment;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/20.
 * TODO validate maxUnavailable and maxSurge
 */
public class FormDeploymentStrategy {
    @NotNull
    private StrategyType type;

    @Min(0)
    private int maxUnavailable;

    @Min(0)
    private int maxSurge;

    public StrategyType getType() {
        return type;
    }

    public void setType(StrategyType type) {
        this.type = type;
    }

    public int getMaxUnavailable() {
        return maxUnavailable;
    }

    public void setMaxUnavailable(int maxUnavailable) {
        this.maxUnavailable = maxUnavailable;
    }

    public int getMaxSurge() {
        return maxSurge;
    }

    public void setMaxSurge(int maxSurge) {
        this.maxSurge = maxSurge;
    }

    public static DeploymentStrategy toStrategy(FormDeploymentStrategy formDeploymentStrategy) {
        if (formDeploymentStrategy == null) {
            return null;
        }

        DeploymentStrategy strategy = new DeploymentStrategy();

        strategy.setType(formDeploymentStrategy.getType().name());
        if (formDeploymentStrategy.getType() == StrategyType.RollingUpdate) {
            RollingUpdateDeployment rollingUpdate = new RollingUpdateDeployment();
            rollingUpdate.setMaxSurge(new IntOrString(formDeploymentStrategy.getMaxSurge()));
            rollingUpdate.setMaxUnavailable(new IntOrString(formDeploymentStrategy.getMaxUnavailable()));
            strategy.setRollingUpdate(rollingUpdate);
        }

        return strategy;
    }

    public static FormDeploymentStrategy from(DeploymentStrategy strategy) {
        if (strategy == null || strategy.getType() == null) {
            return null;
        }

        FormDeploymentStrategy formDeploymentStrategy = new FormDeploymentStrategy();

        if (strategy.getType().equals(StrategyType.RollingUpdate.name())) {
            formDeploymentStrategy.setType(StrategyType.RollingUpdate);
            RollingUpdateDeployment rollingUpdate = strategy.getRollingUpdate();
            if (rollingUpdate != null) {
                formDeploymentStrategy.setMaxSurge(rollingUpdate.getMaxSurge().getIntVal());
                formDeploymentStrategy.setMaxUnavailable(rollingUpdate.getMaxUnavailable().getIntVal());
            }
        } else if (strategy.getType().equals(StrategyType.Recreate.name())) {
            formDeploymentStrategy.setType(StrategyType.Recreate);
        }

        return formDeploymentStrategy;
    }
}
