package cc.backend.kubernetes.batch.form.deprecated;

import cc.backend.kubernetes.Constants;
import cc.backend.kubernetes.batch.form.dto.FormContainerDeprecated;
import cc.backend.kubernetes.batch.form.dto.FormLabel;
import cc.backend.kubernetes.batch.form.dto.FormServicePort;
import cc.backend.kubernetes.batch.form.dto.FormStorage;
import cc.backend.kubernetes.service.dto.ServiceType;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.PodSpec;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import io.fabric8.kubernetes.api.model.extensions.StatefulSetSpec;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Map;

import static cc.backend.kubernetes.Constants.*;

/**
 * Created by yanzhixiang on 17-7-12.
 */
@Deprecated
public class FormStatefulSet {
    private String imagePullSecret;
    private List<String> imagePullSecrets;
    @NotNull
    @Size(max = STATEFULSET_NAME_MAX, message = "statefulset name must no more than " + STATEFULSET_NAME_MAX + " characters.")
    @Pattern(regexp = Constants.RESOURCE_NAME_PATTERN)
    private String name;
    private String description;
    private String namespace;

    @Size(max = SERVICE_NAME_MAX, message = "service name must no more than " + SERVICE_NAME_MAX + " characters.")
    @Pattern(regexp = Constants.RESOURCE_NAME_PATTERN)
    private String serviceName;

    @Min(0)
    private int replicas;
    @Valid
    @NotNull
    private List<FormLabel> labels;

    //storage for create
    @Valid
    private List<FormStorage> storages;

    //storage already exists
    private List<Long> storageIdList;

    @Valid
    private List<FormStorage> newStorageList;

    @Valid
    private List<FormStorage> existsStorageList;

    @Valid
    private List<VolumeClaimTemplate> volumeClaimTemplates;

    @Valid
    private List<FormContainerDeprecated> containers;

    @Valid
    private ServiceType serviceType;
    private List<String> externalIPs;

    private List<FormServicePort> portMappings;

    private Boolean criticalPod;

    /**
     * TODO add annotations key and value format check
     */
    private Map<String, String> annotations;

    private Map<String, String> serviceAnnotations;

    private String clusterIP;

    private Map<String, String> podTemplateAnnotations;

    public String getImagePullSecret() {
        return imagePullSecret;
    }

    public void setImagePullSecret(String imagePullSecret) {
        this.imagePullSecret = imagePullSecret;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getReplicas() {
        return replicas;
    }

    public void setReplicas(int replicas) {
        this.replicas = replicas;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public List<FormLabel> getLabels() {
        return labels;
    }

    public void setLabels(List<FormLabel> labels) {
        this.labels = labels;
    }

    public List<FormStorage> getStorages() {
        return storages;
    }

    public void setStorages(List<FormStorage> storages) {
        this.storages = storages;
    }

    public List<Long> getStorageIdList() {
        return storageIdList;
    }

    public void setStorageIdList(List<Long> storageIdList) {
        this.storageIdList = storageIdList;
    }

    public List<String> getImagePullSecrets() {
        return imagePullSecrets;
    }

    public void setImagePullSecrets(List<String> imagePullSecrets) {
        this.imagePullSecrets = imagePullSecrets;
    }


    public List<FormContainerDeprecated> getContainers() {
        return containers;
    }

    public void setContainers(List<FormContainerDeprecated> containers) {
        this.containers = containers;
    }

    public List<FormStorage> getNewStorageList() {
        return newStorageList;
    }

    public void setNewStorageList(List<FormStorage> newStorageList) {
        this.newStorageList = newStorageList;
    }

    public List<FormStorage> getExistsStorageList() {
        return existsStorageList;
    }

    public void setExistsStorageList(List<FormStorage> existsStorageList) {
        this.existsStorageList = existsStorageList;
    }

    public List<VolumeClaimTemplate> getVolumeClaimTemplates() {
        return volumeClaimTemplates;
    }

    public void setVolumeClaimTemplates(List<VolumeClaimTemplate> volumeClaimTemplates) {
        this.volumeClaimTemplates = volumeClaimTemplates;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public List<String> getExternalIPs() {
        return externalIPs;
    }

    public void setExternalIPs(List<String> externalIPs) {
        this.externalIPs = externalIPs;
    }

    public List<FormServicePort> getPortMappings() {
        return portMappings;
    }

    public void setPortMappings(List<FormServicePort> portMappings) {
        this.portMappings = portMappings;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Boolean getCriticalPod() {
        return criticalPod;
    }

    public void setCriticalPod(Boolean criticalPod) {
        this.criticalPod = criticalPod;
    }

    public Map<String, String> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(Map<String, String> annotations) {
        this.annotations = annotations;
    }

    public String getClusterIP() {
        return clusterIP;
    }

    public void setClusterIP(String clusterIP) {
        this.clusterIP = clusterIP;
    }

    public Map<String, String> getServiceAnnotations() {
        return serviceAnnotations;
    }

    public void setServiceAnnotations(Map<String, String> serviceAnnotations) {
        this.serviceAnnotations = serviceAnnotations;
    }

    public Map<String, String> getPodTemplateAnnotations() {
        return podTemplateAnnotations;
    }

    public void setPodTemplateAnnotations(Map<String, String> podTemplateAnnotations) {
        this.podTemplateAnnotations = podTemplateAnnotations;
    }

    @Override
    public String toString() {
        return "FormStatefulSet{" +
                "imagePullSecret='" + imagePullSecret + '\'' +
                ", imagePullSecrets=" + imagePullSecrets +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", namespace='" + namespace + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", replicas=" + replicas +
                ", labels=" + labels +
                ", storages=" + storages +
                ", storageIdList=" + storageIdList +
                ", newStorageList=" + newStorageList +
                ", existsStorageList=" + existsStorageList +
                ", volumeClaimTemplates=" + volumeClaimTemplates +
                ", containers=" + containers +
                ", serviceType=" + serviceType +
                ", externalIPs=" + externalIPs +
                ", portMappings=" + portMappings +
                ", criticalPod=" + criticalPod +
                ", annotations=" + annotations +
                ", serviceAnnotations=" + serviceAnnotations +
                ", podTemplateAnnotations=" + podTemplateAnnotations +
                '}';
    }

    public static FormStatefulSet from(StatefulSet statefulSet) {
        if (statefulSet == null) {
            return null;
        }

        ObjectMeta statefulSetMetadata = statefulSet.getMetadata();
        StatefulSetSpec statefulSetSpec = statefulSet.getSpec();

        Map<String, String> annotations = statefulSetMetadata.getAnnotations();

        PodSpec podSpec = statefulSetSpec.getTemplate().getSpec();

        FormStatefulSet formStatefulSet = new FormStatefulSet();
        formStatefulSet.setName(statefulSetMetadata.getName());

        if (annotations != null) {
            formStatefulSet.setAnnotations(annotations);
            formStatefulSet.setDescription(annotations.get("description"));
        }

        Map<String, String> podAnnotations = statefulSet.getSpec().getTemplate().getMetadata().getAnnotations();
        if (podAnnotations != null) {
            formStatefulSet.setPodTemplateAnnotations(annotations);
            formStatefulSet.setCriticalPod("true".equalsIgnoreCase(podAnnotations.get(CRITICAL_POD_ANNOTATION)));
        }

        formStatefulSet.setReplicas(statefulSetSpec.getReplicas() == null ? 0 : statefulSetSpec.getReplicas());
        formStatefulSet.setNamespace(statefulSetMetadata.getNamespace());
        formStatefulSet.setLabels(FormLabel.from(statefulSetMetadata.getLabels()));
        formStatefulSet.setServiceName(statefulSet.getSpec().getServiceName());

        formStatefulSet.setContainers(FormContainerDeprecated.from(podSpec.getContainers()));

        return formStatefulSet;
    }

}
