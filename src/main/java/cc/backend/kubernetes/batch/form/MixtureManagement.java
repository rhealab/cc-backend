package cc.backend.kubernetes.batch.form;

import cc.backend.EnnProperties;
import cc.backend.audit.operation.OperationAuditMessage;
import cc.backend.audit.operation.OperationAuditMessageProducer;
import cc.backend.audit.operation.OperationType;
import cc.backend.audit.operation.ResourceType;
import cc.backend.event.data.EventSource;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.batch.form.dto.DeploymentFormMixture;
import cc.backend.kubernetes.batch.form.dto.StatefulSetFormMixture;
import cc.backend.kubernetes.batch.form.validator.DeploymentMixtureValidator;
import cc.backend.kubernetes.batch.form.validator.StatefulSetMixtureValidator;
import cc.backend.kubernetes.service.ServiceValidator;
import cc.backend.kubernetes.service.ServicesManagement;
import cc.backend.kubernetes.storage.StorageManagement;
import cc.backend.kubernetes.storage.StorageRepository;
import cc.backend.kubernetes.storage.StorageWorkloadRepository;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageWorkload;
import cc.backend.kubernetes.storage.domain.WorkloadType;
import cc.backend.kubernetes.workload.WorkloadValidator;
import cc.backend.kubernetes.workload.deployment.DeploymentHelper;
import cc.backend.kubernetes.workload.deployment.service.DeploymentsManagement;
import cc.backend.kubernetes.workload.statefulset.StatefulSetHelper;
import cc.backend.kubernetes.workload.statefulset.StatefulSetManagement;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.PersistentVolumeClaimVolumeSource;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.Volume;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/20.
 */
@Component
public class MixtureManagement {
    private static final Logger logger = LoggerFactory.getLogger(DeploymentsManagement.class);

    @Inject
    private DeploymentsManagement deploymentsManagement;

    @Inject
    private StatefulSetManagement statefulSetManagement;

    @Inject
    private ServicesManagement servicesManagement;

    @Inject
    private StorageManagement storageManagement;

    @Inject
    private WorkloadValidator workloadValidator;

    @Inject
    private DeploymentMixtureValidator deploymentValidator;

    @Inject
    private StatefulSetMixtureValidator statefulSetValidator;

    @Inject
    private KubernetesClientManager clientManager;

    @Inject
    private StorageRepository storageRepository;

    @Inject
    private StorageWorkloadRepository storageWorkloadRepository;

    @Inject
    private OperationAuditMessageProducer operationAuditMessageProducer;

    @Inject
    private ServiceValidator serviceValidator;

    @Inject
    private EnnProperties properties;

    public void createDeploymentMixture(String namespaceName, String appName, DeploymentFormMixture request) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        deploymentValidator.validateDeploymentShouldNotExists(namespaceName, appName, request.getName());
        if (request.getCriticalPod() != null && request.getCriticalPod()) {
            deploymentValidator.validateCanCriticalPod(namespaceName);
        }
        deploymentValidator.validateService(namespaceName, request);

        List<Storage> storageList = deploymentValidator.validateStoragesForCreate(namespaceName, request);

        Deployment deployment = DeploymentFormMixture.toDeployment(appName, request, storageList);
        Service service = null;
        if (request.getServiceExists() != null && request.getServiceExists()) {
            service = DeploymentFormMixture.toService(namespaceName, request);
            serviceValidator.validateClusterIP(service);
            serviceValidator.validatePortProtocol(namespaceName, appName, service);
            serviceValidator.validateSvcNodePortAndExternalIp(service);
        }

        workloadValidator.validateComputeQuotaForCreate(namespaceName, appName, deployment);

        createStorage(namespaceName, storageList);

        deploymentsManagement.createWithAudit(namespaceName, appName, deployment);
        if (service != null) {
            servicesManagement.createWithAudit(namespaceName, appName, service);
        }

        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .elapsed(stopwatch.stop().elapsed(TimeUnit.MILLISECONDS))
                .namespaceName(namespaceName)
                .operationType(OperationType.CREATE)
                .resourceType(ResourceType.DEPLOYMENT)
                .resourceName(request.getName())
                .extras(ImmutableMap.of("request", request))
                .build();
        operationAuditMessageProducer.send(message);
    }

    public DeploymentFormMixture getDeploymentMixture(String namespaceName, String appName, String deploymentName) {
        Deployment deployment = deploymentValidator.validateDeploymentShouldExists(namespaceName, appName, deploymentName);
        Service service = clientManager.getClient()
                .services()
                .inNamespace(namespaceName)
                .withName(deploymentName)
                .get();

        List<StorageWorkload> relationShipList = storageWorkloadRepository
                .findByNamespaceNameAndWorkloadTypeAndWorkloadName(namespaceName, WorkloadType.Deployment, deploymentName);

        List<Long> storageIdList = relationShipList.stream()
                .map(StorageWorkload::getStorageId)
                .collect(Collectors.toList());

        List<Storage> storageList = storageRepository.findByNamespaceNameAndIdIn(namespaceName, storageIdList);

        return DeploymentFormMixture.from(deployment, service, storageList, properties.getCurrentKubernetes().getVipHost());
    }

    public void updateDeploymentMixture(String namespaceName, String appName, DeploymentFormMixture request) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        Deployment oldDeployment = deploymentValidator.validateDeploymentShouldExists(namespaceName, appName, request.getName());
        if (request.getCriticalPod() != null && request.getCriticalPod() && !DeploymentHelper.isCriticalPod(oldDeployment)) {
            deploymentValidator.validateCanCriticalPod(namespaceName);
        }

        List<Storage> storageList = deploymentValidator.validateStoragesForUpdate(namespaceName, request, oldDeployment);

        Deployment newDeployment = DeploymentFormMixture.toDeployment(appName, request, storageList);
        Service service = null;
        if (request.getServiceExists() != null && request.getServiceExists()) {
            service = DeploymentFormMixture.toService(namespaceName, request);
            serviceValidator.validateClusterIP(service);
            serviceValidator.validatePortProtocol(namespaceName, appName, service);
            serviceValidator.validateSvcNodePortAndExternalIp(service);
        }

        workloadValidator.validateComputeQuotaForUpdate(namespaceName, appName, newDeployment, oldDeployment);

        createStorage(namespaceName, storageList);

        deploymentsManagement.updateInternal(namespaceName, appName, newDeployment, storageList);
        if (service != null) {
            servicesManagement.update(namespaceName, appName, service);
        }

        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .elapsed(stopwatch.stop().elapsed(TimeUnit.MILLISECONDS))
                .namespaceName(namespaceName)
                .operationType(OperationType.UPDATE)
                .resourceType(ResourceType.DEPLOYMENT)
                .resourceName(request.getName())
                .extras(ImmutableMap.of("request", request))
                .build();
        operationAuditMessageProducer.send(message);
    }

    public void createStatefulSetMixture(String namespaceName,
                                         String appName,
                                         StatefulSetFormMixture request) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        statefulSetValidator.validateStatefulSetShouldNotExists(namespaceName, appName, request.getName());
        if (request.getCriticalPod() != null && request.getCriticalPod()) {
            statefulSetValidator.validateCanCriticalPod(namespaceName);
        }
        statefulSetValidator.validateService(namespaceName, request);

        List<Storage> storageList = statefulSetValidator.validateStoragesForCreate(namespaceName, request);

        StatefulSet statefulSet = StatefulSetFormMixture.toStatefulSet(appName, request, storageList);
        Service service = StatefulSetFormMixture.toService(namespaceName, request);

        serviceValidator.validateClusterIP(service);
        serviceValidator.validatePortProtocol(namespaceName, appName, service);
        serviceValidator.validateSvcNodePortAndExternalIp(service);

        workloadValidator.validateComputeQuotaForCreate(namespaceName, appName, statefulSet);

        createStorage(namespaceName, storageList);

        servicesManagement.createWithAudit(namespaceName, appName, service);
        statefulSetManagement.createWithAudit(namespaceName, appName, statefulSet);

        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .elapsed(stopwatch.stop().elapsed(TimeUnit.MILLISECONDS))
                .namespaceName(namespaceName)
                .operationType(OperationType.CREATE)
                .resourceType(ResourceType.STATEFUL_SET_MIXTURE)
                .resourceName(request.getName())
                .extras(ImmutableMap.of("request", request))
                .build();
        operationAuditMessageProducer.send(message);
    }

    public void updateStatefulSetMixture(String namespaceName,
                                         String appName,
                                         StatefulSetFormMixture request) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        StatefulSet oldStatefulSet = statefulSetValidator.validateStatefulSetShouldExists(namespaceName, appName, request.getName());
        if (request.getCriticalPod() != null && request.getCriticalPod() && !StatefulSetHelper.isCriticalPod(oldStatefulSet)) {
            statefulSetValidator.validateCanCriticalPod(namespaceName);
        }

        List<Storage> storageList = statefulSetValidator.validateStoragesForUpdate(namespaceName, request, oldStatefulSet);

        StatefulSet statefulSet = StatefulSetFormMixture.toStatefulSet(appName, request, storageList);

        Service service = null;
        if (request.getServiceExists() == null || request.getServiceExists()) {
            service = StatefulSetFormMixture.toService(appName, request);
            serviceValidator.validateClusterIP(service);
            serviceValidator.validatePortProtocol(namespaceName, appName, service);
            serviceValidator.validateSvcNodePortAndExternalIp(service);
        }

        workloadValidator.validateComputeQuotaForUpdate(namespaceName, appName, statefulSet, oldStatefulSet);

        createStorage(namespaceName, storageList);

        statefulSetManagement.updateInternal(namespaceName, appName, statefulSet, storageList);

        if (service != null) {
            servicesManagement.update(namespaceName, appName, service);
        }

        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .elapsed(stopwatch.stop().elapsed(TimeUnit.MILLISECONDS))
                .namespaceName(namespaceName)
                .operationType(OperationType.UPDATE)
                .resourceType(ResourceType.STATEFUL_SET_MIXTURE)
                .resourceName(request.getName())
                .extras(ImmutableMap.of("request", request))
                .build();
        operationAuditMessageProducer.send(message);
    }

    public void createStorage(String namespaceName, List<Storage> storageList) {
        Optional.ofNullable(storageList)
                .orElseGet(ArrayList::new)
                .stream()
                .filter(storage -> storage.getId() == 0)
                .forEach(storage -> {
                    storage.setNamespaceName(namespaceName);
                    // storage has been validated, should not validate again
                    EventSource<Storage> eventSource = storageManagement.initStorageForCreate(storage);
                    storageManagement.createStorage(eventSource);
                });
    }

    public StatefulSetFormMixture getStatefulSetMixture(String namespaceName,
                                                        String appName,
                                                        String statefulSetName) {
        StatefulSet statefulSet = statefulSetValidator.validateStatefulSetShouldExists(namespaceName, appName, statefulSetName);
        Service service = null;
        if (!StringUtils.isEmpty(statefulSet.getSpec().getServiceName())) {
            service = clientManager.getClient()
                    .services()
                    .inNamespace(namespaceName)
                    .withName(statefulSet.getSpec().getServiceName())
                    .get();
        }

        List<Storage> storageList = new ArrayList<>();
        List<Volume> volumes = statefulSet.getSpec().getTemplate().getSpec().getVolumes();
        if (volumes != null) {
            volumes.forEach(volume -> {
                PersistentVolumeClaimVolumeSource pvc = volume.getPersistentVolumeClaim();
                if (pvc == null) {
                    return;
                }
                Storage storage = storageRepository.findByNamespaceNameAndPvcName(namespaceName, pvc.getClaimName());
                if (storage != null) {
                    storageList.add(storage);
                }
            });
        }

        return StatefulSetFormMixture.from(statefulSet, service, storageList, properties.getCurrentKubernetes().getVipHost());
    }
}
