package cc.backend.kubernetes.batch.form.deprecated;

import cc.backend.kubernetes.storage.domain.AccessModeType;
import cc.backend.kubernetes.storage.domain.StorageCapacity;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static cc.backend.kubernetes.Constants.RESOURCE_NAME_MAX;
import static cc.backend.kubernetes.Constants.RESOURCE_NAME_PATTERN;

/**
 * Created by xzy on 17-7-12.
 */
@Deprecated
public class VolumeClaimTemplate implements StorageCapacity{

    @Pattern(regexp = RESOURCE_NAME_PATTERN)
    @Size(max = RESOURCE_NAME_MAX)
    private String storageName;
    @NotNull
    private AccessModeType accessMode;
    @Min(1024 * 1024)
    private long amountBytes;

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public AccessModeType getAccessMode() {
        return accessMode;
    }

    public void setAccessMode(AccessModeType accessMode) {
        this.accessMode = accessMode;
    }

    public long getAmountBytes() {
        return amountBytes;
    }

    public void setAmountBytes(long amountBytes) {
        this.amountBytes = amountBytes;
    }
}
