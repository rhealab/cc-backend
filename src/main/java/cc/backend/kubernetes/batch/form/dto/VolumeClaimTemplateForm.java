package cc.backend.kubernetes.batch.form.dto;

import cc.backend.kubernetes.storage.domain.AccessModeType;
import cc.backend.kubernetes.storage.domain.StorageCapacity;
import cc.backend.kubernetes.utils.NameUtils;
import cc.backend.kubernetes.utils.UnitUtils;
import com.google.common.collect.ImmutableList;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.PersistentVolumeClaim;
import io.fabric8.kubernetes.api.model.PersistentVolumeClaimSpec;
import io.fabric8.kubernetes.api.model.ResourceRequirements;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static cc.backend.kubernetes.Constants.*;

/**
 * @author by xzy on 17-7-12.
 */
public class VolumeClaimTemplateForm implements StorageCapacity {

    @Pattern(regexp = RESOURCE_NAME_PATTERN)
    @Size(max = RESOURCE_NAME_MAX)
    private String name;

    @Valid
    private List<FormAnnotation> annotations;

    @Valid
    private List<FormLabel> labels;

    @NotNull
    private AccessModeType accessMode;
    @Min(1024 * 1024)
    private long amountBytes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<FormAnnotation> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(List<FormAnnotation> annotations) {
        this.annotations = annotations;
    }

    public List<FormLabel> getLabels() {
        return labels;
    }

    public void setLabels(List<FormLabel> labels) {
        this.labels = labels;
    }

    public AccessModeType getAccessMode() {
        return accessMode;
    }

    public void setAccessMode(AccessModeType accessMode) {
        this.accessMode = accessMode;
    }

    @Override
    public long getAmountBytes() {
        return amountBytes;
    }

    public void setAmountBytes(long amountBytes) {
        this.amountBytes = amountBytes;
    }

    @Override
    public String toString() {
        return "VolumeClaimTemplateForm{" +
                "name='" + name + '\'' +
                ", annotations=" + annotations +
                ", labels=" + labels +
                ", accessMode=" + accessMode +
                ", amountBytes=" + amountBytes +
                '}';
    }

    public static List<VolumeClaimTemplateForm> from(List<PersistentVolumeClaim> pvcList) {
        if (pvcList == null) {
            return new ArrayList<>();
        }

        return pvcList.stream()
                .map(VolumeClaimTemplateForm::from)
                .collect(Collectors.toList());
    }

    public static VolumeClaimTemplateForm from(PersistentVolumeClaim pvc) {
        VolumeClaimTemplateForm form = new VolumeClaimTemplateForm();

        form.setName(pvc.getMetadata().getName());
        form.setAnnotations(FormAnnotation.from(pvc.getMetadata().getAnnotations()));
        form.setLabels(FormLabel.from(pvc.getMetadata().getLabels()));
        form.setAccessMode(AccessModeType.valueOf(pvc.getSpec().getAccessModes().get(0)));
        form.setAmountBytes(UnitUtils.parseK8sStorageQuantity(pvc.getSpec().getResources().getRequests().get(K_STORAGE)));

        return form;
    }

    public static List<PersistentVolumeClaim> to(String namespaceName, List<VolumeClaimTemplateForm> formList) {
        if (formList == null) {
            return new ArrayList<>();
        }

        return formList.stream()
                .map(form -> to(namespaceName, form))
                .collect(Collectors.toList());
    }

    public static PersistentVolumeClaim to(String namespaceName, VolumeClaimTemplateForm form) {
        PersistentVolumeClaim pvc = new PersistentVolumeClaim();

        ObjectMeta metadata = composeMetadata(namespaceName, form);
        pvc.setMetadata(metadata);

        PersistentVolumeClaimSpec pvcSpec = composeSpec(namespaceName, form);
        pvc.setSpec(pvcSpec);

        return pvc;
    }

    private static PersistentVolumeClaimSpec composeSpec(String namespaceName, VolumeClaimTemplateForm form) {
        PersistentVolumeClaimSpec pvcSpec = new PersistentVolumeClaimSpec();
        pvcSpec.setAccessModes(ImmutableList.of(form.getAccessMode().name()));
        ResourceRequirements resourceRequirements = new ResourceRequirements();
        resourceRequirements.setRequests(form.capacity());
        pvcSpec.setResources(resourceRequirements);
        pvcSpec.setStorageClassName(NameUtils.getStorageClassName(namespaceName));
        return pvcSpec;
    }

    private static ObjectMeta composeMetadata(String namespaceName, VolumeClaimTemplateForm form) {
        ObjectMeta metadata = new ObjectMeta();
        metadata.setName(form.getName());
        metadata.setLabels(FormLabel.toMap(form.getLabels()));
        Map<String, String> annotations = FormAnnotation.toMap(form.getAnnotations());
        annotations.put(STORAGE_CLASS_ANNOTATION, NameUtils.getStorageClassName(namespaceName));
        metadata.setAnnotations(annotations);
        return metadata;
    }
}
