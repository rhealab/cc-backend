package cc.backend.kubernetes.batch.form.validator;

import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.batch.form.dto.DeploymentFormMixture;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageType;
import cc.backend.kubernetes.utils.ResourceLabelHelper;
import cc.backend.kubernetes.workload.deployment.DeploymentHelper;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

import static cc.backend.kubernetes.Constants.APP_LABEL;
import static cc.backend.kubernetes.storage.domain.StorageType.RBD;

/**
 * validator for mixture
 * validate service storage and deployment from the mixture
 *
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/11/7.
 */
@Component
public class DeploymentMixtureValidator extends BaseMixtureValidator<DeploymentFormMixture, Deployment> {

    @Inject
    private KubernetesClientManager clientManager;

    /**
     * validate the service for create should not exists
     * <p>
     * createService is null or false and serviceType is null will not create service
     *
     * @param namespaceName the namespace
     * @param request       the request
     */
    public void validateService(String namespaceName,
                                DeploymentFormMixture request) {
        if (request.getServiceExists() == null
                || !request.getServiceExists()
                || request.getServiceType() == null) {
            return;
        }

        Service service = clientManager.getClient()
                .services()
                .inNamespace(namespaceName)
                .withName(request.getName())
                .get();

        if (service != null) {
            throw new CcException(BackendReturnCodeNameConstants.SERVICE_ALREADY_EXISTS,
                    ImmutableMap.of("namespaceName", namespaceName,
                            "serviceName", request.getName()));
        }
    }

    /**
     * validate the deployment for create should not exists
     *
     * @param namespaceName  the namespace
     * @param appName        the app
     * @param deploymentName the deployment name
     */
    public void validateDeploymentShouldNotExists(String namespaceName,
                                                  String appName,
                                                  String deploymentName) {
        Deployment deployment = clientManager.getClient()
                .extensions()
                .deployments()
                .inNamespace(namespaceName)
                .withName(deploymentName)
                .get();

        if (deployment != null) {
            throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_ALREADY_EXISTS,
                    ImmutableMap.of("namespaceName", namespaceName,
                            "appName", appName,
                            "deploymentName", deploymentName));
        }
    }

    /**
     * validate the deployment should exists
     *
     * @param namespaceName  the namespace name
     * @param appName        the app name
     * @param deploymentName the deployment for check
     * @return the k8s deployment
     */
    public Deployment validateDeploymentShouldExists(String namespaceName,
                                                     String appName,
                                                     String deploymentName) {
        Deployment deployment = clientManager.getClient()
                .extensions()
                .deployments()
                .inNamespace(namespaceName)
                .withName(deploymentName)
                .get();

        if (deployment != null) {
            String label = ResourceLabelHelper.getLabel(deployment, APP_LABEL);
            if (label != null && appName.equals(label)) {
                return deployment;
            }
        }

        throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_NOT_EXISTS_IN_APP,
                ImmutableMap.of("namespaceName", namespaceName,
                        "appName", appName,
                        "deploymentName", deploymentName));
    }

    @Override
    public void validateRbdStorageQuotaForCreate(String namespaceName,
                                                 DeploymentFormMixture request,
                                                 List<Storage> newStorageList) {
        validateRbdStorageQuota(namespaceName, request, newStorageList);
    }

    /**
     * validate hostpath and rbd storage for deployment update
     *
     * @param namespaceName     the namespace name
     * @param request           the request
     * @param newStorageList    the storage list for create
     * @param existsStorageList the storage list already exists
     */
    @Override
    public void validateStorageQuotaForUpdate(String namespaceName,
                                              DeploymentFormMixture request,
                                              List<Storage> newStorageList,
                                              List<Storage> existsStorageList,
                                              Deployment oldDeployment) {

        validateRbdStorageQuota(namespaceName, request, newStorageList);

        long newHostpathBytes = getHostpathBytes(request, newStorageList, existsStorageList);
        if (newHostpathBytes == 0) {
            return;
        }

        long oldHostpathBytes = DeploymentHelper.getHostpathBytes(namespaceName, oldDeployment, storageRepository);
        if (oldHostpathBytes > newHostpathBytes) {
            return;
        }

        long availableBytes = namespaceStatsManagement.getNamespaceStorageAvailableBytes(namespaceName, StorageType.HostPath);
        if (availableBytes + oldHostpathBytes < newHostpathBytes) {
            throw new CcException(BackendReturnCodeNameConstants.FORM_DEPLOYMENT_HOSTPATH_STORAGE_OVER_QUOTA,
                    ImmutableMap.of("namespaceName", namespaceName,
                            "deploymentName", request.getName(),
                            "hostpathBytes", newHostpathBytes,
                            "availableBytes", availableBytes));
        }
    }

    public void validateRbdStorageQuota(String namespaceName,
                                        DeploymentFormMixture request,
                                        List<Storage> newStorageList) {
        long newRbdBytes = newStorageList.stream()
                .filter(storage -> storage.getStorageType() == RBD)
                .mapToLong(Storage::getAmountBytes)
                .sum();

        if (newRbdBytes > 0) {
            long availableRbdBytes = namespaceStatsManagement.
                    getNamespaceStorageAvailableBytes(namespaceName, StorageType.RBD);
            if (availableRbdBytes < newRbdBytes) {
                throw new CcException(BackendReturnCodeNameConstants.FORM_DEPLOYMENT_CEPH_RBD_STORAGE_OVER_QUOTA,
                        ImmutableMap.of("namespaceName", namespaceName,
                                "deploymentName", request.getName(),
                                "newRbdBytes", newRbdBytes,
                                "availableRbdBytes", availableRbdBytes));
            }
        }
    }
}
