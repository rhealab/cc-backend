package cc.backend.kubernetes.batch.form.dto;

import cc.backend.kubernetes.Constants;
import cc.backend.kubernetes.utils.UnitUtils;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.fabric8.kubernetes.api.model.*;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static cc.backend.kubernetes.Constants.*;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/20.
 */
public class FormContainerDeprecated {
    private String containerImage;
    @Valid
    private List<FormVolumeMount> volumeMounts;
    @Valid
    private List<FormEnv> env;
    @NotNull
    @Size(max = CONTAINER_NAME_MAX, message = "container name must no more than " + CONTAINER_NAME_MAX + " characters.")
    @Pattern(regexp = Constants.RESOURCE_NAME_PATTERN)
    private String name;

    @JsonProperty("cpuRequirement")
    @DecimalMin(value = "0.1", message = "cpu must larger than 0.1")
    private double cpuRequests;
    @DecimalMin(value = "0.1", message = "cpu must larger than 0.1")
    private double cpuLimits;

    @JsonProperty("memoryRequirementBytes")
    @Min(value = 1024 * 1024 * 4, message = "memory at least 4M.")
    private long memoryRequestsBytes;
    @Min(value = 1024 * 1024 * 4, message = "memory at least 4M.")
    private long memoryLimitsBytes;

    @Deprecated
    private String containerCommand;
    @Deprecated
    private String containerCommandArgs;

    private boolean runAsPrivileged;

    private List<String> containerCommandList;
    private List<String> containerCommandArgsList;

    @Valid
    private List<FormContainerPort> containerPorts;

    private ImagePullPolicy imagePullPolicy;

    public String getContainerImage() {
        return containerImage;
    }

    public void setContainerImage(String containerImage) {
        this.containerImage = containerImage;
    }

    public List<FormVolumeMount> getVolumeMounts() {
        return volumeMounts;
    }

    public void setVolumeMounts(List<FormVolumeMount> volumeMounts) {
        this.volumeMounts = volumeMounts;
    }

    public List<FormEnv> getEnv() {
        return env;
    }

    public void setEnv(List<FormEnv> env) {
        this.env = env;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("cpuRequirement")
    public double getCpuRequests() {
        return cpuRequests;
    }

    @JsonProperty("cpuRequirement")
    public void setCpuRequests(double cpuRequests) {
        this.cpuRequests = cpuRequests;
    }

    public double getCpuLimits() {
        return cpuLimits;
    }

    public void setCpuLimits(double cpuLimits) {
        this.cpuLimits = cpuLimits;
    }

    @JsonProperty("memoryRequirementBytes")
    public long getMemoryRequestsBytes() {
        return memoryRequestsBytes;
    }

    @JsonProperty("memoryRequirementBytes")
    public void setMemoryRequestsBytes(long memoryRequestsBytes) {
        this.memoryRequestsBytes = memoryRequestsBytes;
    }

    public long getMemoryLimitsBytes() {
        return memoryLimitsBytes;
    }

    public void setMemoryLimitsBytes(long memoryLimitsBytes) {
        this.memoryLimitsBytes = memoryLimitsBytes;
    }

    public String getContainerCommand() {
        return containerCommand;
    }

    public void setContainerCommand(String containerCommand) {
        this.containerCommand = containerCommand;
    }

    public String getContainerCommandArgs() {
        return containerCommandArgs;
    }

    public void setContainerCommandArgs(String containerCommandArgs) {
        this.containerCommandArgs = containerCommandArgs;
    }

    public boolean isRunAsPrivileged() {
        return runAsPrivileged;
    }

    public void setRunAsPrivileged(boolean runAsPrivileged) {
        this.runAsPrivileged = runAsPrivileged;
    }

    public List<FormContainerPort> getContainerPorts() {
        return containerPorts;
    }

    public void setContainerPorts(List<FormContainerPort> containerPorts) {
        this.containerPorts = containerPorts;
    }

    public ImagePullPolicy getImagePullPolicy() {
        return imagePullPolicy;
    }

    public void setImagePullPolicy(ImagePullPolicy imagePullPolicy) {
        this.imagePullPolicy = imagePullPolicy;
    }

    public List<String> getContainerCommandList() {
        return containerCommandList;
    }

    public void setContainerCommandList(List<String> containerCommandList) {
        this.containerCommandList = containerCommandList;
    }

    public List<String> getContainerCommandArgsList() {
        return containerCommandArgsList;
    }

    public void setContainerCommandArgsList(List<String> containerCommandArgsList) {
        this.containerCommandArgsList = containerCommandArgsList;
    }

    @Override
    public String toString() {
        return "FormContainerDeprecated{" +
                "containerImage='" + containerImage + '\'' +
                ", volumeMounts=" + volumeMounts +
                ", env=" + env +
                ", name='" + name + '\'' +
                ", cpuRequests=" + cpuRequests +
                ", cpuLimits=" + cpuLimits +
                ", memoryRequestsBytes=" + memoryRequestsBytes +
                ", memoryLimitsBytes=" + memoryLimitsBytes +
                ", runAsPrivileged=" + runAsPrivileged +
                ", containerCommandList=" + containerCommandList +
                ", containerCommandArgsList=" + containerCommandArgsList +
                ", containerPorts=" + containerPorts +
                ", imagePullPolicy=" + imagePullPolicy +
                '}';
    }

    public static List<FormContainerDeprecated> from(List<Container> containers) {
        List<FormContainerDeprecated> formContainers = new ArrayList<>();
        if (containers != null) {
            for (Container container : containers) {
                formContainers.add(from(container));
            }
        }

        return formContainers;
    }

    public static FormContainerDeprecated from(Container container) {
        if (container == null) {
            return null;
        }

        FormContainerDeprecated formContainer = new FormContainerDeprecated();

        formContainer.setName(container.getName());
        formContainer.setContainerImage(container.getImage());

        if (!CollectionUtils.isEmpty(container.getCommand())) {
            formContainer.setContainerCommandList(container.getCommand());
        }

        if (!CollectionUtils.isEmpty(container.getArgs())) {
            formContainer.setContainerCommandArgsList(container.getArgs());
        }

        formContainer.setEnv(FormEnv.from(container.getEnv()));

        SecurityContext securityContext = container.getSecurityContext();
        if (securityContext != null && securityContext.getPrivileged() != null) {
            formContainer.setRunAsPrivileged(securityContext.getPrivileged());
        }

        formContainer.setContainerPorts(FormContainerPort.from(container.getPorts()));

        ResourceRequirements resources = container.getResources();
        if (resources != null) {
            Map<String, Quantity> limits = resources.getLimits();
            if (limits != null) {
                formContainer.setCpuLimits(UnitUtils.parseK8sCpuQuantity(limits.get(K_CPU)));
                formContainer.setMemoryLimitsBytes(UnitUtils.parseK8sMemoryQuantity(limits.get(K_MEMORY)));
            }

            Map<String, Quantity> requests = resources.getRequests();
            if (requests != null) {
                formContainer.setCpuRequests(UnitUtils.parseK8sCpuQuantity(requests.get(K_CPU)));
                formContainer.setMemoryRequestsBytes(UnitUtils.parseK8sMemoryQuantity(requests.get(K_MEMORY)));
            }
        }

        formContainer.setVolumeMounts(FormVolumeMount.from(container.getVolumeMounts()));

        if (container.getImagePullPolicy() != null) {
            ImagePullPolicy imagePullPolicy = ImagePullPolicy.valueOf(container.getImagePullPolicy());
            formContainer.setImagePullPolicy(imagePullPolicy);
        }

        return formContainer;
    }

    public static Container toContainer(FormContainerDeprecated formContainer, List<FormServicePort> servicePorts) {
        Container container = new Container();
        container.setName(formContainer.getName());
        container.setImage(formContainer.getContainerImage());
        if (!StringUtils.isEmpty(formContainer.getContainerCommandList())) {
            container.setCommand(formContainer.getContainerCommandList());
        }
        if (!StringUtils.isEmpty(formContainer.getContainerCommandArgsList())) {
            container.setArgs(formContainer.getContainerCommandArgsList());
        }

        if (formContainer.getEnv() != null) {
            container.setEnv(FormEnv.toEnvVar(formContainer.getEnv()));
        }

        if (formContainer.isRunAsPrivileged()) {
            SecurityContext securityContext = new SecurityContext();
            securityContext.setPrivileged(true);
            container.setSecurityContext(securityContext);
        }

        if (CollectionUtils.isEmpty(formContainer.getContainerPorts())) {
            //TODO should do this???
            container.setPorts(FormServicePort.toContainerPorts(servicePorts));
        } else {
            container.setPorts(FormContainerPort.toContainerPorts(formContainer.getContainerPorts()));
        }

        container.setVolumeMounts(FormVolumeMount.toVolumeMounts(formContainer.getVolumeMounts()));
        container.setResources(getResourceRequirements(formContainer));

        if (formContainer.getImagePullPolicy() != null) {
            container.setImagePullPolicy(formContainer.getImagePullPolicy().name());
        }

        return container;
    }

    public static List<Container> toContainers(List<FormContainerDeprecated> formContainers, List<FormServicePort> servicePorts) {
        List<Container> containers = new ArrayList<>();

        if (formContainers != null) {
            for (FormContainerDeprecated formContainer : formContainers) {
                containers.add(toContainer(formContainer, servicePorts));
            }
        }

        return containers;
    }

    public static ResourceRequirements getResourceRequirements(FormContainerDeprecated formContainer) {
        double cpuLimits = formContainer.getCpuLimits();
        double cpuRequests = formContainer.getCpuRequests();

        long memoryLimitsBytes = formContainer.getMemoryLimitsBytes();
        long memoryRequestsBytes = formContainer.getMemoryRequestsBytes();

        return new ResourceRequirementsBuilder()
                .addToLimits(K_CPU, UnitUtils.toK8sCpuQuantity(cpuLimits))
                .addToLimits(K_MEMORY, UnitUtils.toK8sMemoryQuantity(memoryLimitsBytes))
                .addToRequests(K_CPU, UnitUtils.toK8sCpuQuantity(cpuRequests))
                .addToRequests(K_MEMORY, UnitUtils.toK8sMemoryQuantity(memoryRequestsBytes))
                .build();
    }
}
