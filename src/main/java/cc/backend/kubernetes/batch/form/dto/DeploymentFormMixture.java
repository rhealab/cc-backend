package cc.backend.kubernetes.batch.form.dto;

import cc.backend.kubernetes.service.dto.EnnIngress;
import cc.backend.kubernetes.utils.ResourceLabelHelper;
import cc.backend.kubernetes.service.ServiceHelper;
import cc.backend.kubernetes.storage.domain.Storage;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.fabric8.kubernetes.api.model.*;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.DeploymentSpec;
import io.fabric8.kubernetes.api.model.extensions.DeploymentStrategy;
import io.fabric8.kubernetes.api.model.extensions.RollbackConfig;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static cc.backend.kubernetes.Constants.APP_LABEL;
import static cc.backend.kubernetes.Constants.CRITICAL_POD_ANNOTATION;

/**
 * a mixture of deployment service and storage
 *
 * @author NormanWang06@gmail.com (jinxin) on 2017/7/6.
 */
public class DeploymentFormMixture extends BaseFormMixture implements ConsoleMetadata {

    /**
     * null or false will not create a service
     */
    @JsonProperty("createService")
    protected Boolean serviceExists;

    @Min(0)
    private Integer minReadySeconds;

    private Boolean paused;

    @Min(0)
    private Long rollbackRevision;

    @Valid
    private FormDeploymentStrategy strategy;

    @JsonProperty("createService")
    public Boolean getServiceExists() {
        return serviceExists;
    }

    @JsonProperty("createService")
    public void setServiceExists(Boolean serviceExists) {
        this.serviceExists = serviceExists;
    }

    public Integer getMinReadySeconds() {
        return minReadySeconds;
    }

    public void setMinReadySeconds(Integer minReadySeconds) {
        this.minReadySeconds = minReadySeconds;
    }

    public Boolean getPaused() {
        return paused;
    }

    public void setPaused(Boolean paused) {
        this.paused = paused;
    }

    public Long getRollbackRevision() {
        return rollbackRevision;
    }

    public void setRollbackRevision(Long rollbackRevision) {
        this.rollbackRevision = rollbackRevision;
    }

    public FormDeploymentStrategy getStrategy() {
        return strategy;
    }

    public void setStrategy(FormDeploymentStrategy strategy) {
        this.strategy = strategy;
    }

    @Override
    public String toString() {
        return "DeploymentFormMixture{" +
                "minReadySeconds=" + minReadySeconds +
                ", paused=" + paused +
                ", rollbackRevision=" + rollbackRevision +
                ", strategy=" + strategy +
                ", name='" + name + '\'' +
                ", namespace='" + namespace + '\'' +
                ", labels=" + labels +
                ", annotations=" + annotations +
                ", replicas=" + replicas +
                ", revisionHistoryLimit=" + revisionHistoryLimit +
                ", criticalPod=" + criticalPod +
                ", imagePullSecrets=" + imagePullSecrets +
                ", podTemplateLabels=" + podTemplateLabels +
                ", podTemplateAnnotations=" + podTemplateAnnotations +
                ", containers=" + containers +
                ", newStorageList=" + newStorageList +
                ", existsStorageList=" + existsStorageList +
                ", serviceExists=" + serviceExists +
                ", serviceLabels=" + serviceLabels +
                ", serviceAnnotations=" + serviceAnnotations +
                ", serviceType=" + serviceType +
                ", serviceExternalIPs=" + serviceExternalIPs +
                ", servicePorts=" + servicePorts +
                ", serviceSelectors=" + serviceSelectors +
                '}';
    }

    /**
     * convert a deployment to Mixture
     *
     * @param deployment the deployment for converting
     * @param service    the service for converting
     * @return Mixture or null if deployment is null
     */
    public static DeploymentFormMixture from(Deployment deployment,
                                             Service service,
                                             List<Storage> storageList,
                                             String vipHost) {
        if (deployment == null) {
            return null;
        }

        ObjectMeta deploymentMetadata = deployment.getMetadata();
        DeploymentSpec deploymentSpec = deployment.getSpec();
        ObjectMeta podMetadata = deploymentSpec.getTemplate().getMetadata();
        PodSpec podSpec = deploymentSpec.getTemplate().getSpec();

        DeploymentFormMixture mixture = new DeploymentFormMixture();
        mixture.setName(deploymentMetadata.getName());
        mixture.setNamespace(deploymentMetadata.getNamespace());
        mixture.setLabels(FormLabel.from(deploymentMetadata.getLabels()));
        mixture.setAnnotations(FormAnnotation.from(deploymentMetadata.getAnnotations()));
        mixture.setReplicas(deploymentSpec.getReplicas());
        mixture.setMinReadySeconds(deploymentSpec.getMinReadySeconds());
        mixture.setRevisionHistoryLimit(deploymentSpec.getRevisionHistoryLimit());
        mixture.setPaused(deploymentSpec.getPaused());
        RollbackConfig rollbackTo = deploymentSpec.getRollbackTo();
        if (rollbackTo != null) {
            mixture.setRollbackRevision(rollbackTo.getRevision());
        }
        DeploymentStrategy strategy = deploymentSpec.getStrategy();
        mixture.setStrategy(FormDeploymentStrategy.from(strategy));

        List<LocalObjectReference> imagePullSecrets = podSpec.getImagePullSecrets();
        if (imagePullSecrets != null) {
            mixture.setImagePullSecrets(imagePullSecrets
                    .stream()
                    .map(LocalObjectReference::getName)
                    .collect(Collectors.toList()));
        }
        mixture.setPodTemplateLabels(FormLabel.from(podMetadata.getLabels()));
        Map<String, String> podAnnotations = podMetadata.getAnnotations();
        if (podAnnotations != null) {
            mixture.setCriticalPod("true".equalsIgnoreCase(podAnnotations.get(CRITICAL_POD_ANNOTATION)));
            mixture.setPodTemplateAnnotations(FormAnnotation.from(podAnnotations));
        }
        mixture.setContainers(FormContainer.from(podSpec.getContainers()));

        mixture.setExistsStorageList(FormStorage.from(storageList));

        if (service != null) {
            mixture.setServiceExists(Boolean.TRUE);
            fillService(service, mixture, vipHost);
        }

        return mixture;
    }

    public static Deployment toDeployment(String appName, DeploymentFormMixture mixture, List<Storage> storageList) {
        Deployment deployment = new Deployment();

        ObjectMeta metadata = composeMetadata(appName, mixture);
        deployment.setMetadata(metadata);

        DeploymentSpec spec = composeDeploymentSpec(mixture, storageList);
        deployment.setSpec(spec);

        return deployment;
    }

    private static DeploymentSpec composeDeploymentSpec(DeploymentFormMixture mixture,
                                                        List<Storage> storageList) {
        DeploymentSpec spec = new DeploymentSpec();
        spec.setStrategy(FormDeploymentStrategy.toStrategy(mixture.getStrategy()));
        spec.setMinReadySeconds(mixture.getMinReadySeconds());
        spec.setRevisionHistoryLimit(mixture.getRevisionHistoryLimit());
        spec.setPaused(mixture.getPaused());
        if (mixture.getRollbackRevision() != null) {
            RollbackConfig rollbackConfig = new RollbackConfig();
            rollbackConfig.setRevision(mixture.getRollbackRevision());
            spec.setRollbackTo(rollbackConfig);
        }
        spec.setReplicas(mixture.getReplicas());
        spec.setSelector(FormLabel.toSelector(mixture.getPodTemplateLabels()));

        //deployment spec pod template
        PodTemplateSpec template = composePodTemplateSpec(mixture, storageList);

        spec.setTemplate(template);
        return spec;
    }

    /**
     * compose a service from Mixture
     *
     * @param appName the app name
     * @param mixture the Mixture
     * @return composed service or null
     */
    public static Service toService(String appName, BaseFormMixture mixture) {
        Service service = new Service();

        ObjectMeta metadata = new ObjectMeta();
        metadata.setName(mixture.getName());
        metadata.setNamespace(mixture.getNamespace());
        metadata.setLabels(FormLabel.toMap(mixture.getServiceLabels()));

        Map<String, String> annotations = FormAnnotation.toMap(mixture.getServiceAnnotations());
        if (mixture.getEnnIngress() != null) {
            annotations.putAll(EnnIngress.toAnnotations(mixture.getEnnIngress()));
        }
        metadata.setAnnotations(annotations);

        ResourceLabelHelper.addLabel(service, APP_LABEL, appName);
        service.setMetadata(metadata);

        service.setSpec(ServiceHelper.composeServiceSpec(mixture));

        return service;
    }
}
