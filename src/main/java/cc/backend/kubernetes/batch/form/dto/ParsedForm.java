package cc.backend.kubernetes.batch.form.dto;

import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.extensions.Deployment;

import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/21.
 */
public class ParsedForm {
    private List<Service> services;
    private List<Deployment> deployments;

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public List<Deployment> getDeployments() {
        return deployments;
    }

    public void setDeployments(List<Deployment> deployments) {
        this.deployments = deployments;
    }

    @Override
    public String toString() {
        return "ParsedForm{" +
                "services=" + services +
                ", deployments=" + deployments +
                '}';
    }
}
