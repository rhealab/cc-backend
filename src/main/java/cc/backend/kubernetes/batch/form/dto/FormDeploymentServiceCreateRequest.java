package cc.backend.kubernetes.batch.form.dto;

import cc.backend.kubernetes.batch.form.deprecated.FormDeployment;
import cc.backend.kubernetes.service.dto.ServiceType;

import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/20.
 */
public class FormDeploymentServiceCreateRequest extends FormDeployment {
    private ServiceType serviceType;
    private List<String> externalIPs;
    private String clusterIP;

    private List<FormServicePort> portMappings;

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public List<String> getExternalIPs() {
        return externalIPs;
    }

    public void setExternalIPs(List<String> externalIPs) {
        this.externalIPs = externalIPs;
    }

    public String getClusterIP() {
        return clusterIP;
    }

    public void setClusterIP(String clusterIP) {
        this.clusterIP = clusterIP;
    }

    public List<FormServicePort> getPortMappings() {
        return portMappings;
    }

    public void setPortMappings(List<FormServicePort> portMappings) {
        this.portMappings = portMappings;
    }
}
