package cc.backend.kubernetes.batch.form.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/20.
 */
public class FormAnnotation {
    //TODO validate key
    /*@Pattern(regexp = Constants.ANNOTATION_NAME_PATTERN,
            message = "annotation key should match reg exp:" + Constants.ANNOTATION_NAME_PATTERN)*/
    private String key;
    private String value;

    public FormAnnotation() {
    }

    public FormAnnotation(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static List<FormAnnotation> from(Map<String, String> annotations) {
        if (annotations == null) {
            return new ArrayList<>();
        }

        List<FormAnnotation> formLabels = new ArrayList<>();
        annotations.forEach((key, value) -> formLabels.add(new FormAnnotation(key, value)));
        return formLabels;
    }

    public static Map<String, String> toMap(List<FormAnnotation> formAnnotations) {
        if (formAnnotations == null) {
            return new HashMap<>(0);
        }

        return formAnnotations
                .stream()
                .filter(annotation -> annotation.getKey() != null)
                .peek(a -> {
                    if (a.getValue() == null) {
                        a.setValue("");
                    }
                })
                .collect(Collectors.toMap(FormAnnotation::getKey, FormAnnotation::getValue));
    }
}
