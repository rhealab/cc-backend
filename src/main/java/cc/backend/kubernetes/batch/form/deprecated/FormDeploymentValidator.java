package cc.backend.kubernetes.batch.form.deprecated;

import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.kubernetes.batch.form.dto.FormStorage;
import cc.backend.kubernetes.namespaces.data.NamespaceExtras;
import cc.backend.kubernetes.namespaces.data.NamespaceExtrasRepository;
import cc.backend.kubernetes.namespaces.services.NamespaceStatsManagement;
import cc.backend.kubernetes.namespaces.validator.NamespaceValidator;
import cc.backend.kubernetes.storage.StorageRepository;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageType;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static cc.backend.kubernetes.storage.domain.StorageType.*;
import static com.google.common.collect.ImmutableMap.of;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/5/11.
 */
@Component
@Deprecated
public class FormDeploymentValidator {
    private static final Logger logger = LoggerFactory.getLogger(FormDeploymentValidator.class);

    @Inject
    private NamespaceValidator namespaceValidator;

    @Inject
    private NamespaceExtrasRepository namespaceExtrasRepository;

    @Inject
    private StorageRepository storageRepository;

    @Inject
    private NamespaceStatsManagement namespaceStatsManagement;

    public void validateCriticalPod(String namespaceName, FormDeployment formDeployment) {
        if (formDeployment != null && formDeployment.getCriticalPod() != null && formDeployment.getCriticalPod()) {
            NamespaceExtras namespaceExtras = namespaceExtrasRepository.findByNameAndDeletedIsFalse(namespaceName);
            if (namespaceExtras == null || !namespaceExtras.getAllowCriticalPod()) {
                throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_NOT_ALLOW_CRITICAL_POD,
                        ImmutableMap.of("namespace", namespaceName,
                                "deployment", formDeployment.getName()));
            }
        }
    }

    //validate and return all storage
    //already exists storage's id is not 0
    public List<Storage> validateStorageExists(String namespaceName,
                                               FormDeployment form) {
        List<FormStorage> existsFormStorageList = form.getExistsStorageList();
        List<FormStorage> newFormStorageList = form.getNewStorageList();
        if (existsFormStorageList == null) {
            existsFormStorageList = new ArrayList<>();
        }

        if (newFormStorageList == null) {
            newFormStorageList = new ArrayList<>();
        }

        List<FormStorage> storages = form.getStorages();
        if (storages != null) {
            for (FormStorage formStorage : storages) {
                if (formStorage.getStorageId() > 0) {
                    existsFormStorageList.add(formStorage);
                } else {
                    newFormStorageList.add(formStorage);
                }
            }
        }

        List<Long> storageIdList = form.getStorageIdList();
        if (storageIdList != null) {
            for (Long id : storageIdList) {
                if (id != null) {
                    FormStorage formStorage = new FormStorage();
                    formStorage.setStorageId(id);
                    existsFormStorageList.add(formStorage);
                }
            }
        }

        //check if the storage name need to be created is exists
        if (!newFormStorageList.isEmpty()) {
            List<String> storageNameList = new ArrayList<>();
            for (FormStorage formStorage : newFormStorageList) {
                storageNameList.add(formStorage.getStorageName());
            }

            List<Storage> storageList = new ArrayList<>();
            if (!storageNameList.isEmpty()) {
                storageList = storageRepository
                        .findByNamespaceNameAndStorageNameIn(namespaceName, storageNameList);
            }

            if (!CollectionUtils.isEmpty(storageList)) {
                List<String> storageNames = storageList.stream()
                        .map(Storage::getStorageName).collect(Collectors.toList());
                throw new CcException(BackendReturnCodeNameConstants.FORM_DEPLOYMENT_STORAGE_ALREADY_EXISTS,
                        ImmutableMap.of("namespaceName", namespaceName,
                                "storageNames", storageNames));
            }
        }

        List<Storage> storageList = FormStorage.to(namespaceName, newFormStorageList);
        //if the exists storage is exists
        if (!existsFormStorageList.isEmpty()) {
            List<Long> existsStorageIdList = new ArrayList<>();
            // FIXME add storage name support
            for (FormStorage formStorage : existsFormStorageList) {
                existsStorageIdList.add(formStorage.getStorageId());
            }

            List<Storage> existsStorageList = storageRepository.findAll(existsStorageIdList);
            if (existsStorageList.size() < existsStorageIdList.size()) {
                List<Long> notExistsStorageIdList = existsStorageIdList.stream()
                        .filter(id -> existsStorageList.stream().noneMatch(storage -> storage.getId() == id))
                        .collect(Collectors.toList());
                throw new CcException(BackendReturnCodeNameConstants.FORM_DEPLOYMENT_STORAGE_NOT_EXISTS,
                        ImmutableMap.of("namespaceName", namespaceName,
                                "storageIds", notExistsStorageIdList));
            }

            storageList.addAll(existsStorageList);
        }
        return storageList;
    }

    //legacy namespace not support cephfs, rbd storage and nfs
    public void validateStorageType(String namespaceName, List<Storage> storageList) {
        if (namespaceValidator.isLegacy(namespaceName)) {
            Map<String, StorageType> notSupportedStorage = new HashMap<>();
            for (Storage storage : storageList) {
                if (storage.getId() > 0) {
                    continue;
                }

                StorageType storageType = storage.getStorageType();
                if (storageType == CephFS || storageType == RBD
                        || storageType == NFS || storageType == EFS || storageType == EBS) {
                    notSupportedStorage.put(storage.getStorageName(), storageType);
                }
            }

            if (!notSupportedStorage.isEmpty()) {
                throw new CcException(BackendReturnCodeNameConstants.FORM_DEPLOYMENT_STORAGE_TYPE_NOT_SUPPORT,
                        of("namespaceName", namespaceName,
                                "notSupportedStorage", notSupportedStorage));
            }
        }
    }

    public void validateStorageQuotaForCreate(String namespaceName,
                                              List<Storage> newStorageList,
                                              FormDeployment formDeployment) {
        int replicas = formDeployment.getReplicas();
        if (replicas == 0) {
            return;
        }

        long newHostpathBytes = 0;
        long newRbdBytes = 0;

        for (Storage storage : newStorageList) {
            StorageType storageType = storage.getStorageType();
            if (storageType == StorageType.HostPath) {
                newHostpathBytes += storage.getAmountBytes();
            } else if (storage.getId() == 0 && storageType == RBD) {
                newRbdBytes += storage.getAmountBytes();
            }
        }

        if (newRbdBytes > 0) {
            long availableRbdBytes = namespaceStatsManagement.
                    getNamespaceStorageAvailableBytes(namespaceName, StorageType.RBD);
            if (availableRbdBytes < newRbdBytes) {
                throw new CcException(BackendReturnCodeNameConstants.FORM_DEPLOYMENT_CEPH_RBD_STORAGE_OVER_QUOTA,
                        ImmutableMap.of("deployment", formDeployment.getName(),
                                "newRbdBytes", newRbdBytes,
                                "availableRbdBytes", availableRbdBytes));
            }
        }
        newHostpathBytes *= replicas;
        if (newHostpathBytes == 0) {
            return;
        }

        long availableBytes = namespaceStatsManagement.getNamespaceStorageAvailableBytes(namespaceName, StorageType.HostPath);
        if (availableBytes < newHostpathBytes) {
            throw new CcException(BackendReturnCodeNameConstants.FORM_DEPLOYMENT_HOSTPATH_STORAGE_OVER_QUOTA,
                    ImmutableMap.of("deployment", formDeployment.getName(),
                            "newHostpathBytes", newHostpathBytes,
                            "availableBytes", availableBytes));
        }
    }
}
