package cc.backend.kubernetes.batch.form.dto;

import cc.backend.audit.request.EnnContext;
import cc.backend.kubernetes.storage.StorageUtils;
import cc.backend.kubernetes.storage.domain.AccessModeType;
import cc.backend.kubernetes.storage.domain.EbsStorageType;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageType;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static cc.backend.kubernetes.Constants.RESOURCE_NAME_MAX;
import static cc.backend.kubernetes.Constants.RESOURCE_NAME_PATTERN;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/20.
 */
public class FormStorage {
    private long storageId;

    @Pattern(regexp = RESOURCE_NAME_PATTERN)
    @Size(max = RESOURCE_NAME_MAX)
    @NotNull
    private String storageName;
    @NotNull
    private StorageType storageType;
    private AccessModeType accessMode;
    @Min(1024 * 1024)
    private long amountBytes;

    /**
     * only for hostpath
     * true: storage keep in the chosen node and info will not loss when pod was deleted
     * false: when pod was deleted the info will loss
     */
    private boolean persisted;

    /**
     * only for hostpath
     * true: each pod has there own storage (quota will be replicas * amountBytes)
     * false: all pod use the only one storage
     */
    private boolean unshared;

    /**
     * for nfs cephfs rbd pv read only option
     */
    private boolean readOnly;

    @JsonProperty("ebsType")
    private EbsStorageType ebsStorageType;

    public long getStorageId() {
        return storageId;
    }

    public void setStorageId(long storageId) {
        this.storageId = storageId;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public StorageType getStorageType() {
        return storageType;
    }

    public void setStorageType(StorageType storageType) {
        this.storageType = storageType;
    }

    public AccessModeType getAccessMode() {
        return accessMode;
    }

    public void setAccessMode(AccessModeType accessMode) {
        this.accessMode = accessMode;
    }

    public long getAmountBytes() {
        return amountBytes;
    }

    public void setAmountBytes(long amountBytes) {
        this.amountBytes = amountBytes;
    }

    public boolean isPersisted() {
        return persisted;
    }

    public void setPersisted(boolean persisted) {
        this.persisted = persisted;
    }

    public boolean isUnshared() {
        return unshared;
    }

    public void setUnshared(boolean unshared) {
        this.unshared = unshared;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public EbsStorageType getEbsStorageType() {
        return ebsStorageType;
    }

    public void setEbsStorageType(EbsStorageType ebsStorageType) {
        this.ebsStorageType = ebsStorageType;
    }

    @Override
    public String toString() {
        return "FormStorage{" +
                "storageId=" + storageId +
                ", storageName='" + storageName + '\'' +
                ", storageType=" + storageType +
                ", accessMode=" + accessMode +
                ", amountBytes=" + amountBytes +
                ", persisted=" + persisted +
                ", unshared=" + unshared +
                ", readOnly=" + readOnly +
                ", ebsStorageType=" + ebsStorageType +
                '}';
    }

    public static List<FormStorage> from(List<Storage> storageList) {
        List<FormStorage> formStorageList = new ArrayList<>();

        if (storageList != null) {
            for (Storage storage : storageList) {
                formStorageList.add(from(storage));
            }
        }

        return formStorageList;
    }

    public static FormStorage from(Storage storage) {
        if (storage == null) {
            return null;
        }

        FormStorage formStorage = new FormStorage();
        formStorage.setStorageId(storage.getId());
        formStorage.setStorageName(storage.getStorageName());
        formStorage.setStorageType(storage.getStorageType());
        formStorage.setAccessMode(storage.getAccessMode());
        formStorage.setAmountBytes(storage.getAmountBytes());
        formStorage.setPersisted(storage.isPersisted());
        formStorage.setUnshared(storage.isUnshared());
        formStorage.setReadOnly(storage.isReadOnly());
        formStorage.setEbsStorageType(storage.getEbsStorageType());

        return formStorage;
    }

    public static List<Storage> to(String namespaceName, List<FormStorage> formStorageList) {
        List<Storage> storageList = new ArrayList<>();

        if (formStorageList != null) {
            for (FormStorage formStorage : formStorageList) {
                storageList.add(toStorage(namespaceName, formStorage));
            }
        }

        return storageList;
    }

    public static Storage toStorage(String namespaceName, FormStorage formStorage) {
        /*Set<ConstraintViolation<FormStorage>> constraints = Validation
                .buildDefaultValidatorFactory()
                .getValidator().validate(formStorage, Default.class);
        if (constraints.size() > 0) {
            throw new ConstraintViolationException(constraints);
        }*/

        Storage storage = new Storage();
        storage.setId(formStorage.getStorageId());
        storage.setNamespaceName(namespaceName);
        storage.setStorageName(formStorage.getStorageName());
        storage.setStorageType(formStorage.getStorageType());
        storage.setCreatedBy(EnnContext.getUserId());
        storage.setAccessMode(StorageUtils.composeAccessMode(formStorage.getAccessMode(), formStorage.getStorageType()));
        long amountBytes = formStorage.getAmountBytes();
        storage.setAmountBytes(amountBytes);
        storage.setUnshared(formStorage.isUnshared());
        storage.setReadOnly(formStorage.isReadOnly());
        storage.setPersisted(formStorage.isPersisted());
        storage.setCreatedOn(new Date());
        storage.setStatus(Storage.Status.CREATE_PENDING);
        storage.setPvcName(StorageUtils.pvcName(namespaceName, formStorage.getStorageName()));
        storage.setPvName(StorageUtils.pvName(namespaceName, formStorage.getStorageName()));
        storage.setImageName(StorageUtils.cephImageName(formStorage.getStorageName()));
        storage.setEbsStorageType(formStorage.getEbsStorageType());

        return storage;
    }
}
