package cc.backend.kubernetes.batch.form.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/11/6.
 */
public interface ConsoleMetadata extends Serializable {

    /**
     * the name
     *
     * @return name
     */
    @JsonProperty("name")
    String getName();

    /**
     * the namespace
     *
     * @return namespace
     */
    @JsonProperty("namespace")
    String getNamespace();

    /**
     * the labels
     *
     * @return labels
     */
    @JsonProperty("labels")
    List<FormLabel> getLabels();

    /**
     * the annotations
     *
     * @return annotations
     */
    @JsonProperty("annotations")
    List<FormAnnotation> getAnnotations();
}
