package cc.backend.kubernetes.batch.form.deprecated;

import cc.backend.audit.operation.OperationAuditMessage;
import cc.backend.audit.operation.OperationAuditMessageProducer;
import cc.backend.audit.operation.OperationType;
import cc.backend.audit.operation.ResourceType;
import cc.backend.audit.request.EnnContext;
import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.event.data.EventSource;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.batch.form.dto.FormContainerDeprecated;
import cc.backend.kubernetes.batch.form.dto.FormLabel;
import cc.backend.kubernetes.batch.form.dto.FormServicePort;
import cc.backend.kubernetes.batch.form.dto.FormStorage;
import cc.backend.kubernetes.namespaces.data.NamespaceExtras;
import cc.backend.kubernetes.namespaces.data.NamespaceExtrasRepository;
import cc.backend.kubernetes.namespaces.services.NamespaceStatsManagement;
import cc.backend.kubernetes.namespaces.validator.NamespaceValidator;
import cc.backend.kubernetes.service.ServiceHelper;
import cc.backend.kubernetes.service.ServiceValidator;
import cc.backend.kubernetes.service.ServicesManagement;
import cc.backend.kubernetes.storage.StorageManagement;
import cc.backend.kubernetes.storage.StorageRepository;
import cc.backend.kubernetes.storage.domain.AccessModeType;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageType;
import cc.backend.kubernetes.utils.HostpathUtils;
import cc.backend.kubernetes.utils.ResourceLabelHelper;
import cc.backend.kubernetes.utils.UnitUtils;
import cc.backend.kubernetes.utils.VolumeClaimTemplatesUtils;
import cc.backend.kubernetes.workload.statefulset.StatefulSetManagement;
import cc.backend.kubernetes.workload.statefulset.StatefulSetValidator;
import cc.lib.retcode.CcException;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.*;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import io.fabric8.kubernetes.api.model.extensions.StatefulSetSpec;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static cc.backend.kubernetes.Constants.*;
import static cc.backend.kubernetes.storage.domain.StorageType.*;
import static com.google.common.collect.ImmutableSortedMap.of;

/**
 * @author yanzhixiang on 17-8-7.
 */
@Component
@Deprecated
public class StatefulSetFormManagement {
    @Inject
    private StatefulSetValidator statefulSetValidator;

    @Inject
    private ServiceValidator serviceValidator;

    @Inject
    private StorageManagement storageManagement;

    @Inject
    private ServicesManagement servicesManagement;

    @Inject
    private StatefulSetManagement statefulSetManagement;

    @Inject
    private StorageRepository storageRepository;

    @Inject
    private OperationAuditMessageProducer operationAuditMessageProducer;

    @Inject
    private WorkloadComputeQuotaValidator workloadComputeQuotaValidator;

    @Inject
    private KubernetesClientManager clientManager;

    @Inject
    private NamespaceExtrasRepository namespaceExtrasRepository;

    @Inject
    private NamespaceStatsManagement namespaceStatsManagement;

    @Inject
    private NamespaceValidator namespaceValidator;

    public void createFromForm(String namespaceName, String appName, FormStatefulSet request) {
        request.setNamespace(namespaceName);
        ResourceLabelHelper.removeAppLabel(request.getLabels());

        if (request.getServiceName() == null || request.getServiceName().trim().equals("")) {
            request.setServiceName(request.getName());
        }

        validateCriticalPod(namespaceName, request);

        statefulSetValidator.validateStatefulSetShouldNotExists(namespaceName, appName, request.getName());
        serviceValidator.validateServiceShouldNotExists(namespaceName, appName, request.getServiceName());

        List<Storage> storageList = validateStorageForCreateByForm(namespaceName, request);

        StatefulSet statefulSet = composeStatefulSetByForm(namespaceName, appName, request);

        workloadComputeQuotaValidator.validateStatefulSetComputeQuota(
                namespaceName, appName, statefulSet, WorkloadComputeQuotaValidator.ActionType.CREATE);

        createStorage(namespaceName, storageList);

        Service service = composeServiceByForm(namespaceName, request);
        servicesManagement.createWithAudit(namespaceName, appName, service);


        statefulSetManagement.create(namespaceName, appName, statefulSet);
    }

    private void createStorage(String namespaceName, List<Storage> storageList) {
        if (storageList != null) {
            for (Storage storage : storageList) {
                // new storage has no id and number is 0
                if (storage.getId() == 0) {
                    // storage has been validated, should not validate again
                    storage.setNamespaceName(namespaceName);
                    EventSource<Storage> eventSource = storageManagement.initStorageForCreate(storage);
                    storageManagement.createStorage(eventSource);
                }
            }
        }
    }


    public StatefulSet composeStatefulSetByForm(String namespaceName,
                                                String appName,
                                                FormStatefulSet formCreateRequest) {
        StatefulSet statefulSet = new StatefulSet();

        ObjectMeta metadata = composeStatefulSetMetadata(namespaceName, appName, formCreateRequest);
        statefulSet.setMetadata(metadata);

        StatefulSetSpec spec = composeStatefulSetSpec(namespaceName, formCreateRequest);
        statefulSet.setSpec(spec);

        return statefulSet;
    }

    private ObjectMeta composeStatefulSetMetadata(String namespaceName,
                                                  String appName,
                                                  FormStatefulSet formCreateRequest) {
        Map<String, String> labels = FormLabel.toMap(formCreateRequest.getLabels());
        labels.put(APP_LABEL, appName);

        ObjectMeta metadata = new ObjectMeta();
        metadata.setName(formCreateRequest.getName());
        metadata.setNamespace(namespaceName);
        metadata.setLabels(labels);
        Map<String, String> annotations = formCreateRequest.getAnnotations();
        if (!StringUtils.isEmpty(formCreateRequest.getDescription())) {
            if (annotations == null) {
                annotations = new HashMap<>(1);
            }
            annotations.put("description", formCreateRequest.getDescription());
        }
        metadata.setAnnotations(annotations);
        return metadata;
    }


    private StatefulSetSpec composeStatefulSetSpec(String namespaceName,
                                                   FormStatefulSet formCreateRequest) {
        StatefulSetSpec spec = new StatefulSetSpec();
        spec.setReplicas(formCreateRequest.getReplicas());
        spec.setSelector(FormLabel.toSelector(formCreateRequest.getLabels()));

        spec.setServiceName(formCreateRequest.getServiceName());

        List<PersistentVolumeClaim> pvcList = composeVolumeClaimTemplates(namespaceName, formCreateRequest);
        spec.setVolumeClaimTemplates(pvcList);
        //pod template
        PodTemplateSpec template = composePodTemplateSpec(namespaceName, formCreateRequest);

        spec.setTemplate(template);
        return spec;
    }

    private List<PersistentVolumeClaim> composeVolumeClaimTemplates(String namespaceName, FormStatefulSet formCreateRequest) {
        List<VolumeClaimTemplate> volumeClaimTemplates = formCreateRequest.getVolumeClaimTemplates();
        List<PersistentVolumeClaim> pvcList = new ArrayList<>();
        for (VolumeClaimTemplate volumeClaimTemplate : volumeClaimTemplates) {
            PersistentVolumeClaim pvc = new PersistentVolumeClaim();
            ObjectMeta objectMeta = new ObjectMeta();
            objectMeta.setName(volumeClaimTemplate.getStorageName());
            objectMeta.setAnnotations(ImmutableMap.of(STORAGE_CLASS_ANNOTATION, namespaceName));
            objectMeta.setLabels(FormLabel.toMap(formCreateRequest.getLabels()));
            pvc.setMetadata(objectMeta);

            PersistentVolumeClaimSpec spec = new PersistentVolumeClaimSpec();
            spec.setAccessModes(ImmutableList.of(volumeClaimTemplate.getAccessMode().name()));

            ResourceRequirements resourceRequirements = new ResourceRequirements();
            resourceRequirements.setRequests(volumeClaimTemplate.capacity());
            spec.setResources(resourceRequirements);

            pvc.setSpec(spec);

            pvcList.add(pvc);
        }
        return pvcList;
    }

    private PodTemplateSpec composePodTemplateSpec(String namespaceName, FormStatefulSet formCreateRequest) {
        PodTemplateSpec template = new PodTemplateSpec();
        ObjectMeta templateMetadata = new ObjectMeta();
        templateMetadata.setLabels(FormLabel.toMap(formCreateRequest.getLabels()));

        Map<String, String> annotations = formCreateRequest.getPodTemplateAnnotations();

        if (annotations == null) {
            annotations = new HashMap<>(2);
        }

        if (!StringUtils.isEmpty(formCreateRequest.getDescription())) {
            annotations.put("description", formCreateRequest.getDescription());
        }

        if (formCreateRequest.getCriticalPod() != null && formCreateRequest.getCriticalPod()) {
            annotations.put(CRITICAL_POD_ANNOTATION, "true");
        }
        templateMetadata.setAnnotations(annotations);

        template.setMetadata(templateMetadata);

        //deployment spec pod template spec
        PodSpec podSpec = new PodSpec();

        if (formCreateRequest.getContainers() != null) {
            List<Container> containers = FormContainerDeprecated.toContainers(formCreateRequest.getContainers(), new ArrayList<>());
            podSpec.setContainers(containers);
        }

        LocalObjectReference reference = new LocalObjectReference();
        reference.setName(formCreateRequest.getImagePullSecret());
        podSpec.setImagePullSecrets(ImmutableList.of(reference));

        podSpec.setVolumes(composePodVolumes(namespaceName, formCreateRequest));

        template.setSpec(podSpec);
        return template;
    }

    private List<Volume> composePodVolumes(String namespaceName, FormStatefulSet formCreateRequest) {
        List<Volume> volumes = new ArrayList<>();

        List<Storage> storageList = FormStorage.to(namespaceName, formCreateRequest.getStorages());
        if (formCreateRequest.getStorageIdList() != null) {
            storageList.addAll(storageManagement.getStorageList(namespaceName, formCreateRequest.getStorageIdList()));
        }

        for (Storage storage : storageList) {
            Volume volume = new Volume();
            volume.setName(storage.getStorageName());
            if (storage.getStorageType() != null) {
                PersistentVolumeClaimVolumeSource pvc = new PersistentVolumeClaimVolumeSource();
                pvc.setClaimName(storage.getPvcName());
                volume.setPersistentVolumeClaim(pvc);
            }

            volumes.add(volume);
        }

        return volumes;
    }

    private Service composeServiceByForm(String namespaceName, FormStatefulSet request) {
        Service service = new Service();

        ObjectMeta metadata = new ObjectMeta();

        metadata.setName(request.getServiceName());
        metadata.setNamespace(namespaceName);
        metadata.setLabels(FormLabel.toMap(request.getLabels()));

        Map<String, String> serviceAnnotations = request.getServiceAnnotations();
        if (!StringUtils.isEmpty(request.getDescription())) {

            if (serviceAnnotations == null) {
                serviceAnnotations = new HashMap<>(1);
            }
            serviceAnnotations.put("description", request.getDescription());
        }
        metadata.setAnnotations(serviceAnnotations);
        service.setMetadata(metadata);

        service.setSpec(ServiceHelper.composeServiceSpec(request));

        return service;
    }


    public FormStatefulSet composeFormStatefulSet(String namespaceName, StatefulSet statefulSet) {
        if (statefulSet == null) {
            return null;
        }

        List<Storage> storageList = new ArrayList<>();
        if (statefulSet.getSpec().getTemplate() != null
                && statefulSet.getSpec().getTemplate().getSpec() != null) {
            List<Volume> volumes = statefulSet.getSpec().getTemplate().getSpec().getVolumes();
            if (volumes != null) {
                for (Volume volume : volumes) {
                    PersistentVolumeClaimVolumeSource pvc = volume.getPersistentVolumeClaim();
                    if (pvc == null) {
                        continue;
                    }

                    Storage storage = storageRepository.findByNamespaceNameAndPvcName(namespaceName, pvc.getClaimName());
                    if (storage != null && storage.getStorageType() != StorageType.StorageClass) {
                        storageList.add(storage);
                    }
                }
            }
        }


        List<PersistentVolumeClaim> templates = statefulSet.getSpec().getVolumeClaimTemplates();

        List<VolumeClaimTemplate> claimTemplates = new ArrayList<>();
        for (PersistentVolumeClaim pvc : templates) {
            VolumeClaimTemplate volumeClaimTemplate = new VolumeClaimTemplate();
            volumeClaimTemplate.setStorageName(pvc.getMetadata().getName());
            if (!CollectionUtils.isEmpty(pvc.getSpec().getAccessModes())) {
                volumeClaimTemplate.setAccessMode(AccessModeType.valueOf(pvc.getSpec().getAccessModes().get(0)));
            }
            volumeClaimTemplate.setAmountBytes(UnitUtils.parseK8sStorageQuantity(pvc.getSpec().getResources().getRequests().get(K_STORAGE)));
            claimTemplates.add(volumeClaimTemplate);
        }

        FormStatefulSet formStatefulSet = FormStatefulSet.from(statefulSet);
        formStatefulSet.setStorages(FormStorage.from(storageList));
        formStatefulSet.setVolumeClaimTemplates(claimTemplates);

        String serviceName = statefulSet.getSpec().getServiceName();
        Service service = clientManager.getClient().services().inNamespace(namespaceName).withName(serviceName).get();

        if (service != null) {
            ServiceSpec spec = service.getSpec();
            formStatefulSet.setServiceType(ServiceHelper.getFormType(service));
            formStatefulSet.setExternalIPs(spec.getExternalIPs());
            formStatefulSet.setPortMappings(FormServicePort.from(spec.getPorts()));
        }

        return formStatefulSet;
    }


    public StatefulSet updateByForm(String namespaceName, String appName, FormStatefulSet form) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        ResourceLabelHelper.removeAppLabel(form.getLabels());

        String statefulSetName = form.getName();

        StatefulSet oldStatefulSet = statefulSetValidator.validateStatefulSetShouldExists(namespaceName, appName, statefulSetName);
        validateCriticalPod(namespaceName, form);

        StatefulSet statefulSet = composeStatefulSetByForm(namespaceName, appName, form);
        List<Storage> storageList = validateStorageForUpdateByForm(namespaceName, form, statefulSet, oldStatefulSet);

        workloadComputeQuotaValidator.validateStatefulSetComputeQuota(
                namespaceName, appName, statefulSet, WorkloadComputeQuotaValidator.ActionType.UPDATE);

        //create storage if needed
        createStorage(namespaceName, storageList);

        statefulSet = statefulSetManagement.updateInternal(namespaceName, appName, statefulSet, storageList);

        statefulSetManagement.updateStorageRelationship(namespaceName, appName, statefulSet, storageList);

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .userId(EnnContext.getUserId())
                .requestId(EnnContext.getRequestId())
                .elapsed(elapsed)
                .namespaceName(namespaceName)
                .operationType(OperationType.UPDATE)
                .resourceType(ResourceType.DEPLOYMENT)
                .resourceName(statefulSetName)
                .extras(ImmutableMap.of("origin", oldStatefulSet,
                        "current", statefulSet))
                .build();
        operationAuditMessageProducer.send(message);


        return statefulSet;
    }


    public List<Storage> validateStorageForCreateByForm(String namespaceName, FormStatefulSet form) {
        List<Storage> storageList = validateStorageExists(namespaceName, form);
        validateStorageType(namespaceName, storageList);
        validateStorageQuotaForCreate(namespaceName, storageList, form);
        return storageList;
    }

    //legacy namespace not support cephfs, rbd storage and nfs
    private void validateStorageType(String namespaceName, List<Storage> storageList) {
        if (namespaceValidator.isLegacy(namespaceName)) {
            Map<String, StorageType> notSupportedStorage = new HashMap<>();
            for (Storage storage : storageList) {
                if (storage.getId() > 0) {
                    continue;
                }

                StorageType storageType = storage.getStorageType();
                if (storageType == CephFS || storageType == RBD
                        || storageType == NFS || storageType == EFS || storageType == EBS) {
                    notSupportedStorage.put(storage.getStorageName(), storageType);
                }
            }

            if (!notSupportedStorage.isEmpty()) {
                throw new CcException(BackendReturnCodeNameConstants.FORM_STATEFULSET_STORAGE_TYPE_NOT_SUPPORT,
                        of("namespaceName", namespaceName,
                                "notSupportedStorage", notSupportedStorage));
            }
        }
    }

    private void validateStorageQuotaForCreate(String namespaceName,
                                               List<Storage> newStorageList,
                                               FormStatefulSet formStatefulSet) {
        int replicas = formStatefulSet.getReplicas();
        if (replicas == 0) {
            return;
        }

        long newHostpathBytes = 0;
        long newRbdBytes = 0;

        for (Storage storage : newStorageList) {
            StorageType storageType = storage.getStorageType();
            if (storageType == StorageType.HostPath) {
                newHostpathBytes += storage.getAmountBytes();
            } else if (storage.getId() == 0 && storageType == RBD) {
                newRbdBytes += storage.getAmountBytes();
            }
        }

        //different from deployment,statefulset should consider volume claim template
        List<VolumeClaimTemplate> volumeClaimTemplates = formStatefulSet.getVolumeClaimTemplates();
        for (VolumeClaimTemplate volumeClaimTemplate : volumeClaimTemplates) {
            newRbdBytes += volumeClaimTemplate.getAmountBytes();
        }
        newRbdBytes *= replicas;


        if (newRbdBytes > 0) {
            long availableRbdBytes = namespaceStatsManagement.
                    getNamespaceStorageAvailableBytes(namespaceName, RBD);
            if (availableRbdBytes < newRbdBytes) {
                throw new CcException(BackendReturnCodeNameConstants.FORM_STATEFULSET_CEPH_RBD_STORAGE_OVER_QUOTA,
                        ImmutableMap.of("deployment", formStatefulSet.getName(),
                                "newRbdBytes", newRbdBytes,
                                "availableRbdBytes", availableRbdBytes));
            }
        }
        newHostpathBytes *= replicas;
        if (newHostpathBytes == 0) {
            return;
        }

        long availableBytes = namespaceStatsManagement.getNamespaceStorageAvailableBytes(namespaceName, StorageType.HostPath);
        if (availableBytes < newHostpathBytes) {
            throw new CcException(BackendReturnCodeNameConstants.FORM_STATEFULSET_HOSTPATH_STORAGE_OVER_QUOTA,
                    ImmutableMap.of("deployment", formStatefulSet.getName(),
                            "newHostpathBytes", newHostpathBytes,
                            "availableBytes", availableBytes));
        }
    }

    //validate and return all storage
    //already exists storage's id is not 0
    private List<Storage> validateStorageExists(String namespaceName,
                                                FormStatefulSet form) {
        List<FormStorage> existsFormStorageList = form.getExistsStorageList();
        List<FormStorage> newFormStorageList = form.getNewStorageList();
        if (existsFormStorageList == null) {
            existsFormStorageList = new ArrayList<>();
        }

        if (newFormStorageList == null) {
            newFormStorageList = new ArrayList<>();
        }

        List<FormStorage> storages = form.getStorages();
        if (storages != null) {
            for (FormStorage formStorage : storages) {
                if (formStorage.getStorageId() > 0) {
                    existsFormStorageList.add(formStorage);
                } else {
                    newFormStorageList.add(formStorage);
                }
            }
        }

        List<Long> storageIdList = form.getStorageIdList();
        if (storageIdList != null) {
            for (Long id : storageIdList) {
                if (id != null) {
                    FormStorage formStorage = new FormStorage();
                    formStorage.setStorageId(id);
                    existsFormStorageList.add(formStorage);
                }
            }
        }

        //check if the storage name need to be created is exists
        if (!newFormStorageList.isEmpty()) {
            List<String> storageNameList = new ArrayList<>();
            for (FormStorage formStorage : newFormStorageList) {
                storageNameList.add(formStorage.getStorageName());
            }

            List<Storage> storageList = new ArrayList<>();
            if (!storageNameList.isEmpty()) {
                storageList = storageRepository
                        .findByNamespaceNameAndStorageNameIn(namespaceName, storageNameList);
            }

            if (!CollectionUtils.isEmpty(storageList)) {
                List<String> storageNames = storageList.stream()
                        .map(Storage::getStorageName).collect(Collectors.toList());
                throw new CcException(BackendReturnCodeNameConstants.FORM_STATEFULSET_STORAGE_ALREADY_EXISTS,
                        ImmutableMap.of("namespaceName", namespaceName,
                                "storageNames", storageNames));
            }
        }

        List<Storage> storageList = FormStorage.to(namespaceName, newFormStorageList);
        //if the exists storage is exists
        if (!existsFormStorageList.isEmpty()) {
            List<Long> existsStorageIdList = new ArrayList<>();
            for (FormStorage formStorage : existsFormStorageList) {
                existsStorageIdList.add(formStorage.getStorageId());
            }

            List<Storage> existsStorageList = storageRepository.findAll(existsStorageIdList);
            if (existsStorageList.size() < existsStorageIdList.size()) {
                List<Long> notExistsStorageIdList = existsStorageIdList.stream()
                        .filter(id -> existsStorageList.stream().noneMatch(storage -> storage.getId() == id))
                        .collect(Collectors.toList());
                throw new CcException(BackendReturnCodeNameConstants.FORM_STATEFULSET_STORAGE_NOT_EXISTS,
                        ImmutableMap.of("namespaceName", namespaceName,
                                "storageIds", notExistsStorageIdList));
            }

            storageList.addAll(existsStorageList);
        }
        return storageList;
    }

    public void validateCriticalPod(String namespaceName, FormStatefulSet statefulSet) {
        if (statefulSet != null && statefulSet.getCriticalPod() != null && statefulSet.getCriticalPod()) {
            NamespaceExtras namespaceExtras = namespaceExtrasRepository.findByNameAndDeletedIsFalse(namespaceName);
            if (namespaceExtras == null || !namespaceExtras.getAllowCriticalPod()) {
                throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_NOT_ALLOW_CRITICAL_POD,
                        ImmutableMap.of("namespace", namespaceName,
                                "deployment", statefulSet.getName()));
            }
        }
    }


    public List<Storage> validateStorageForUpdateByForm(String namespaceName, FormStatefulSet form,
                                                        StatefulSet statefulSet, StatefulSet oldStatefulSet) {
        List<Storage> storageList = validateStorageExists(namespaceName, form);
        validateStorageType(namespaceName, storageList);
        validateStorageQuotaForUpdate(namespaceName, storageList, statefulSet, oldStatefulSet);
        return storageList;
    }

    private void validateStorageQuotaForUpdate(String namespaceName,
                                               List<Storage> newStorageList,
                                               StatefulSet statefulSet,
                                               StatefulSet oldStatefulSet) {
        Integer replicas = statefulSet.getSpec().getReplicas();
        if (replicas == null || replicas == 0) {
            return;
        }

        long newHostpathBytes = 0;
        long newRbdBytes = 0;
        for (Storage storage : newStorageList) {
            StorageType storageType = storage.getStorageType();
            if (storageType == StorageType.HostPath) {
                newHostpathBytes += storage.getAmountBytes();
            } else if (storageType == RBD && storage.getId() == 0) {
                newRbdBytes += storage.getAmountBytes();
            }
        }

        List<PersistentVolumeClaim> pvcList = statefulSet.getSpec().getVolumeClaimTemplates();
        for (PersistentVolumeClaim pvc : pvcList) {
            if (pvc.getMetadata() != null && pvc.getMetadata().getName() != null) {
                for (int i = 0; i < replicas; i++) {
                    String storageName = VolumeClaimTemplatesUtils.getStorageName(pvc.getMetadata().getName(), statefulSet.getMetadata().getName(), i);
                    Storage storage = storageRepository.findByNamespaceNameAndStorageName(namespaceName, storageName);
                    if (storage == null) {
                        newRbdBytes += UnitUtils.parseK8sStorageQuantity(pvc.getSpec().getResources().getRequests().get(K_STORAGE));
                    }
                }
            }
        }


        if (newRbdBytes > 0) {
            long availableRbdBytes = namespaceStatsManagement.
                    getNamespaceStorageAvailableBytes(namespaceName, StorageType.RBD);
            if (availableRbdBytes < newRbdBytes) {
                throw new CcException(BackendReturnCodeNameConstants.STATEFULSET_CEPH_RBD_STORAGE_OVER_QUOTA,
                        ImmutableMap.of("statefulSet", oldStatefulSet.getMetadata().getName(),
                                "newRbdBytes", newRbdBytes,
                                "availableRbdBytes", availableRbdBytes));
            }
        }

        newHostpathBytes *= replicas;
        if (newHostpathBytes == 0) {
            return;
        }

        long oldHostpathBytes = HostpathUtils.
                getStatefulSetHostPathBytes(namespaceName, oldStatefulSet, storageRepository);
        if (oldHostpathBytes > newHostpathBytes) {
            return;
        }

        long availableBytes = namespaceStatsManagement.getNamespaceStorageAvailableBytes(namespaceName, StorageType.HostPath);
        if (availableBytes + oldHostpathBytes < newHostpathBytes) {
            throw new CcException(BackendReturnCodeNameConstants.STATEFULSET_HOSTPATH_STORAGE_OVER_QUOTA,
                    ImmutableMap.of("statefulSet", oldStatefulSet.getMetadata().getName(),
                            "newHostPathBytes", newHostpathBytes,
                            "availableBytes", availableBytes + oldHostpathBytes));
        }
    }
}
