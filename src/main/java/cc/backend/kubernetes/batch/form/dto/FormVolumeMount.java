package cc.backend.kubernetes.batch.form.dto;

import io.fabric8.kubernetes.api.model.VolumeMount;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/20.
 */
public class FormVolumeMount {
    @NotNull(message = "VolumeMount storageName can not null")
    private String storageName;
    @NotNull(message = "VolumeMount path can not null")
    private String path;
    private String subPath;
    private boolean readOnly;

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getSubPath() {
        return subPath;
    }

    public void setSubPath(String subPath) {
        this.subPath = subPath;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public static List<FormVolumeMount> from(List<VolumeMount> volumeMounts) {
        List<FormVolumeMount> formVolumeMounts = new ArrayList<>();

        if (volumeMounts != null) {
            for (VolumeMount volumeMount : volumeMounts) {
                FormVolumeMount formVolumeMount = new FormVolumeMount();

                formVolumeMount.setPath(volumeMount.getMountPath());
                formVolumeMount.setSubPath(volumeMount.getSubPath());
                formVolumeMount.setReadOnly(volumeMount.getReadOnly() == null ? false : volumeMount.getReadOnly());

                formVolumeMount.setStorageName(volumeMount.getName());

                formVolumeMounts.add(formVolumeMount);
            }
        }

        return formVolumeMounts;
    }

    public static List<VolumeMount> toVolumeMounts(List<FormVolumeMount> formVolumeMounts) {
        List<VolumeMount> volumeMounts = new ArrayList<>();

        if (formVolumeMounts != null) {
            for (FormVolumeMount formVolumeMount : formVolumeMounts) {
                VolumeMount volumeMount = new VolumeMount();
                volumeMount.setName(formVolumeMount.getStorageName());
                volumeMount.setMountPath(formVolumeMount.getPath());
                volumeMount.setSubPath(formVolumeMount.getSubPath());
                volumeMount.setReadOnly(formVolumeMount.isReadOnly());
                volumeMounts.add(volumeMount);
            }
        }

        return volumeMounts;
    }
}
