package cc.backend.kubernetes.batch.form.dto;

import cc.backend.kubernetes.Constants;
import io.fabric8.kubernetes.api.model.LabelSelector;
import org.springframework.util.StringUtils;

import javax.validation.constraints.Pattern;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/20.
 */
public class FormLabel {
    @Pattern(regexp = Constants.LABEL_NAME_PATTERN, message = "label key should match reg exp:" + Constants.LABEL_NAME_PATTERN)
    private String key;
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static List<FormLabel> from(Map<String, String> labels) {
        return Optional.ofNullable(labels)
                .orElseGet(HashMap::new)
                .entrySet()
                .stream()
                .map(FormLabel::from)
                .collect(Collectors.toList());
    }

    public static FormLabel from(Map.Entry<String, String> entry) {
        FormLabel formLabel = new FormLabel();
        formLabel.setKey(entry.getKey());
        formLabel.setValue(entry.getValue());
        return formLabel;
    }

    public static Map<String, String> toMap(List<FormLabel> formLabels) {
        Map<String, String> labels = new HashMap<>(Optional.ofNullable(formLabels).map(List::size).orElse(0));
        if (formLabels != null) {
            for (FormLabel formLabel : formLabels) {
                if (!StringUtils.isEmpty(formLabel.getKey())) {
                    labels.put(formLabel.getKey(), formLabel.getValue());
                }
            }
        }

        return labels;
    }

    public static LabelSelector toSelector(List<FormLabel> formLabels) {
        LabelSelector labelSelector = new LabelSelector();
        Map<String, String> matchLabels = new HashMap<>(Optional.ofNullable(formLabels).map(List::size).orElse(0));
        if (formLabels != null) {
            for (FormLabel formLabel : formLabels) {
                if (!StringUtils.isEmpty(formLabel.getKey())) {
                    matchLabels.put(formLabel.getKey(), formLabel.getValue());
                }
            }
        }

        labelSelector.setMatchLabels(matchLabels);

        return labelSelector;
    }
}
