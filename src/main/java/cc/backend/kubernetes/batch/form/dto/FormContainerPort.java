package cc.backend.kubernetes.batch.form.dto;

import cc.backend.kubernetes.Constants;
import io.fabric8.kubernetes.api.model.ContainerPort;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

import static cc.backend.kubernetes.Constants.CONTAINER_PORT_NAME_MAX;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/20.
 */
public class FormContainerPort {
    @Size(max = CONTAINER_PORT_NAME_MAX, message = "container port name must be no more than " + CONTAINER_PORT_NAME_MAX + " characters.")
    @Pattern(regexp = Constants.PORT_NAME_PATTERN)
    private String name;
    @Min(0)
    private int port;
    private ProtocolType protocol;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public ProtocolType getProtocol() {
        return protocol;
    }

    public void setProtocol(ProtocolType protocol) {
        this.protocol = protocol;
    }

    public static List<FormContainerPort> from(List<ContainerPort> ports) {
        List<FormContainerPort> formContainerPorts = new ArrayList<>();

        if (ports != null) {
            for (ContainerPort containerPort : ports) {
                FormContainerPort formContainerPort = new FormContainerPort();
                formContainerPort.setName(containerPort.getName());
                formContainerPort.setPort(containerPort.getContainerPort() == null ? 80 : containerPort.getContainerPort());

                if (containerPort.getProtocol() != null) {
                    formContainerPort.setProtocol(ProtocolType.valueOf(containerPort.getProtocol()));
                }

                formContainerPorts.add(formContainerPort);
            }
        }

        return formContainerPorts;
    }

    public static List<ContainerPort> toContainerPorts(List<FormContainerPort> ports) {
        List<ContainerPort> containerPorts = new ArrayList<>();

        if (ports != null) {
            for (FormContainerPort port : ports) {
                ContainerPort containerPort = new ContainerPort();
                containerPort.setName(port.getName());
                containerPort.setContainerPort(port.getPort());
                if (port.getProtocol() != null) {
                    containerPort.setProtocol(port.getProtocol().name());
                }
                containerPorts.add(containerPort);
            }
        }

        return containerPorts;
    }
}
