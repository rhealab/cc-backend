package cc.backend.kubernetes.batch.form.validator;

import cc.backend.EnnProperties;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.batch.form.dto.BaseFormMixture;
import cc.backend.kubernetes.batch.form.dto.FormStorage;
import cc.backend.kubernetes.namespaces.data.NamespaceExtras;
import cc.backend.kubernetes.namespaces.data.NamespaceExtrasRepository;
import cc.backend.kubernetes.namespaces.services.NamespaceStatsManagement;
import cc.backend.kubernetes.namespaces.validator.NamespaceValidator;
import cc.backend.kubernetes.storage.StorageRepository;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageType;
import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static cc.backend.kubernetes.storage.domain.StorageType.*;
import static com.google.common.collect.ImmutableMap.of;

/**
 * validator for mixture
 * validate service storage and deployment from the mixture
 * TODO validate service param
 *
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/11/7.
 */
@Component
public abstract class BaseMixtureValidator<T extends BaseFormMixture, W> {

    @Inject
    protected KubernetesClientManager clientManager;

    @Inject
    protected NamespaceValidator namespaceValidator;

    @Inject
    protected NamespaceExtrasRepository namespaceExtrasRepository;

    @Inject
    protected NamespaceStatsManagement namespaceStatsManagement;

    @Inject
    protected StorageRepository storageRepository;

    @Inject
    protected EnnProperties properties;

    /**
     * validate if the deployment support critical pod or not
     * only the namespace has set the allow flag the deployment will support a critical pod
     *
     * @param namespaceName the namespace
     */
    public void validateCanCriticalPod(String namespaceName) {
        NamespaceExtras namespaceExtras = namespaceExtrasRepository.findByNameAndDeletedIsFalse(namespaceName);
        if (namespaceExtras == null || !namespaceExtras.getAllowCriticalPod()) {
            throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_NOT_ALLOW_CRITICAL_POD,
                    ImmutableMap.of("namespaceName", namespaceName));
        }
    }

    /**
     * validate and return all storage for mixture create
     * fill exists form storage (form storage from frontend may have only a name or id)
     *
     * @param namespaceName the namespace
     * @param request       the request for validate
     * @return all storage, already exists storage will have id
     */
    public List<Storage> validateStoragesForCreate(String namespaceName,
                                                   T request) {
        List<Storage> allStorage = new ArrayList<>();

        validateStorageParam(request);

        List<Storage> existsStorageList = validateExistsStorageShouldExists(namespaceName,
                request.getExistsStorageList());

        List<Storage> newStorageList = validateNewStorageShouldNotExists(namespaceName,
                request.getNewStorageList());

        allStorage.addAll(existsStorageList);
        allStorage.addAll(newStorageList);

        validateStorageType(namespaceName, newStorageList);
        validateStorageQuotaForCreate(namespaceName, request, newStorageList, existsStorageList);

        return allStorage;
    }

    /**
     * validate storage params (new storage has been validated in annotations)
     * TODO validate storage duplicate and new storage id should be 0
     *
     * @param request the request
     */
    public void validateStorageParam(BaseFormMixture request) {
        if (request.getExistsStorageList() != null) {
            request.getExistsStorageList().forEach(formStorage -> {
                if (formStorage.getStorageId() <= 0 && StringUtils.isEmpty(formStorage.getStorageName())) {
                    throw new CcException(BackendReturnCodeNameConstants.PROPERTY_INVALID,
                            ImmutableMap.of("details", "exists storage id or name must be specified!"));
                }
            });
        }
    }

    /**
     * validate exists storage should exists
     * support name and id
     *
     * @param namespaceName         the namespace
     * @param existsFormStorageList the exists form storage list
     * @return the exists storage list
     */
    public List<Storage> validateExistsStorageShouldExists(String namespaceName,
                                                           List<FormStorage> existsFormStorageList) {
        //if the exists storage is exists
        if (CollectionUtils.isEmpty(existsFormStorageList)) {
            return new ArrayList<>();
        }

        List<Long> formStorageIdList = new ArrayList<>();
        List<String> formStorageNameList = new ArrayList<>();

        for (FormStorage formStorage : existsFormStorageList) {
            if (formStorage.getStorageId() > 0) {
                formStorageIdList.add(formStorage.getStorageId());
            } else if (!StringUtils.isEmpty(formStorage.getStorageName())) {
                formStorageNameList.add(formStorage.getStorageName());
            }
        }

        List<Storage> allStorages = new ArrayList<>();
        List<Storage> storagesFromId = new ArrayList<>();
        if (!formStorageIdList.isEmpty()) {
            storagesFromId = storageRepository.findAll(formStorageIdList);
        }

        List<Storage> storagesFromName = new ArrayList<>();
        if (!formStorageNameList.isEmpty()) {
            storagesFromName = storageRepository
                    .findByNamespaceNameAndStorageNameIn(namespaceName, formStorageNameList);
        }

        //FIXME why not validate storage from name
        if (storagesFromId.size() < formStorageIdList.size()) {
            List<Long> storageIdList = storagesFromId.stream()
                    .map(Storage::getId)
                    .collect(Collectors.toList());
            formStorageIdList.removeIf(storageIdList::contains);

            List<String> storageNameList = storagesFromName.stream()
                    .map(Storage::getStorageName)
                    .collect(Collectors.toList());
            formStorageNameList.removeIf(storageNameList::contains);

            throw new CcException(BackendReturnCodeNameConstants.FORM_DEPLOYMENT_STORAGE_NOT_EXISTS,
                    ImmutableMap.of("namespaceName", namespaceName,
                            "storageIds", formStorageIdList,
                            "storageNames", formStorageNameList));
        }

        allStorages.addAll(storagesFromId);
        allStorages.addAll(storagesFromName);
        return allStorages;
    }

    /**
     * validate the storage for create is not exists
     *
     * @param namespaceName      the namespace
     * @param newFormStorageList the form storage for create
     * @return the storage list composed by form storage and for create in next step
     */
    public List<Storage> validateNewStorageShouldNotExists(String namespaceName,
                                                           List<FormStorage> newFormStorageList) {
        //check if the storage name need to be created is exists
        if (StringUtils.isEmpty(newFormStorageList)) {
            return new ArrayList<>();
        }

        List<String> storageNameList = newFormStorageList
                .stream()
                .map(FormStorage::getStorageName)
                .collect(Collectors.toList());

        List<Storage> storageList = new ArrayList<>();
        if (!storageNameList.isEmpty()) {
            storageList = storageRepository
                    .findByNamespaceNameAndStorageNameIn(namespaceName, storageNameList);
        }

        if (!CollectionUtils.isEmpty(storageList)) {
            List<String> storageNames = storageList
                    .stream()
                    .map(Storage::getStorageName)
                    .collect(Collectors.toList());

            throw new CcException(BackendReturnCodeNameConstants.FORM_DEPLOYMENT_STORAGE_ALREADY_EXISTS,
                    ImmutableMap.of("namespaceName", namespaceName,
                            "storageNames", storageNames));
        }

        return FormStorage.to(namespaceName, newFormStorageList);
    }

    /**
     * validate storage type
     * <p>
     * legacy namespace only support HostPath type storage
     * ceph storage can be used only on ceph is enabled and initialized
     * nfs storage can be used only on nfs is enabled and initialized
     *
     * @param namespaceName  the namespace
     * @param newStorageList the storage for create
     */
    public void validateStorageType(String namespaceName, List<Storage> newStorageList) {
        NamespaceExtras namespaceExtras = namespaceExtrasRepository.findByNameAndDeletedIsFalse(namespaceName);

        List<Storage> notSupportedStorageList = new ArrayList<>();
        if (namespaceValidator.isLegacy(namespaceExtras)) {
            notSupportedStorageList = newStorageList.stream()
                    .filter(storage -> {
                        StorageType storageType = storage.getStorageType();
                        return storageType == CephFS || storageType == RBD
                                || storageType == NFS || storageType == EFS || storageType == EBS;
                    })
                    .collect(Collectors.toList());
        } else {
            if (!properties.getCurrentCeph().isEnabled() || !namespaceExtras.isCephInitialized()) {
                notSupportedStorageList = newStorageList.stream()
                        .filter(storage -> {
                            StorageType storageType = storage.getStorageType();
                            return storageType == CephFS || storageType == RBD;
                        })
                        .collect(Collectors.toList());
            }

            if (!properties.getCurrentEbs().isEnabled()) {
                notSupportedStorageList = newStorageList.stream()
                        .filter(storage -> {
                            StorageType storageType = storage.getStorageType();
                            return storageType == EBS;
                        })
                        .collect(Collectors.toList());
            }

            if (!properties.getCurrentNfs().isEnabled() || !namespaceExtras.isNfsInitialized()) {
                notSupportedStorageList.addAll(newStorageList.stream()
                        .filter(storage -> storage.getStorageType() == NFS || storage.getStorageType() == EFS)
                        .collect(Collectors.toList()));
            }
        }

        if (!CollectionUtils.isEmpty(notSupportedStorageList)) {
            throw new CcException(BackendReturnCodeNameConstants.FORM_DEPLOYMENT_STORAGE_TYPE_NOT_SUPPORT,
                    of("namespaceName", namespaceName,
                            "notSupportedStorageList", notSupportedStorageList));
        }
    }

    /**
     * validate hostpath and rbd storage for deployment and storage create
     *
     * @param namespaceName     the namespace name
     * @param request           the request
     * @param newStorageList    the storage list for create
     * @param existsStorageList the storage list already exists
     */
    public void validateStorageQuotaForCreate(String namespaceName,
                                              T request,
                                              List<Storage> newStorageList,
                                              List<Storage> existsStorageList) {

        validateRbdStorageQuotaForCreate(namespaceName, request, newStorageList);
        long newHostpathBytes = getNeededHostpathBytesForCreate(request, newStorageList, existsStorageList);

        long availableBytes = namespaceStatsManagement.getNamespaceStorageAvailableBytes(namespaceName, StorageType.HostPath);
        if (availableBytes < newHostpathBytes) {
            throw new CcException(BackendReturnCodeNameConstants.FORM_DEPLOYMENT_HOSTPATH_STORAGE_OVER_QUOTA,
                    ImmutableMap.of("namespaceName", namespaceName,
                            "deploymentName", request.getName(),
                            "hostpathBytes", newHostpathBytes,
                            "availableBytes", availableBytes));
        }
    }

    public long getNeededHostpathBytesForCreate(BaseFormMixture request,
                                                List<Storage> newStorageList,
                                                List<Storage> existsStorageList) {
        long totalBytes = getHostpathBytes(request, newStorageList, existsStorageList);
        long existsBytes = existsStorageList.stream().mapToLong(Storage::getAmountBytes).sum();

        return totalBytes - existsBytes;
    }

    public long getHostpathBytes(BaseFormMixture request,
                                 List<Storage> newStorageList,
                                 List<Storage> existsStorageList) {
        int replicas = request.getReplicas() == null ? 0 : request.getReplicas();
        if (replicas == 0) {
            return 0;
        }

        long newHostpathBytes = 0;

        newHostpathBytes += newStorageList.stream()
                .filter(storage -> storage.getStorageType() == HostPath)
                .mapToLong(Storage::getAmountBytes)
                .sum();

        newHostpathBytes += existsStorageList.stream()
                .filter(storage -> storage.getStorageType() == HostPath)
                .mapToLong(Storage::getAmountBytes)
                .sum();
        newHostpathBytes *= replicas;
        return newHostpathBytes;
    }

    /**
     * validate rbd storage quota for deployment update and create
     *
     * @param namespaceName  the namespace name
     * @param request        the request
     * @param newStorageList the storage list for create
     */
    public abstract void validateRbdStorageQuotaForCreate(String namespaceName,
                                                          T request,
                                                          List<Storage> newStorageList);

    /**
     * validate and return all storage for mixture update
     * fill exists form storage (form storage from frontend may have only a name or id)
     *
     * @param namespaceName the namespace
     * @param request       the request for validate
     * @param oldWorkLoad   the old workload
     * @return all storage, already exists storage will have id
     */
    public List<Storage> validateStoragesForUpdate(String namespaceName,
                                                   T request,
                                                   W oldWorkLoad) {
        List<Storage> allStorage = new ArrayList<>();

        validateStorageParam(request);

        List<Storage> existsStorageList = validateExistsStorageShouldExists(namespaceName,
                request.getExistsStorageList());
        List<Storage> newStorageList = validateNewStorageShouldNotExists(namespaceName,
                request.getNewStorageList());

        allStorage.addAll(existsStorageList);
        allStorage.addAll(newStorageList);

        validateStorageType(namespaceName, newStorageList);
        validateStorageQuotaForUpdate(namespaceName, request, newStorageList, existsStorageList, oldWorkLoad);

        return allStorage;
    }

    /**
     * validate hostpath and rbd storage for deployment update
     *
     * @param namespaceName     the namespace name
     * @param request           the request
     * @param newStorageList    the storage list for create
     * @param existsStorageList the storage list already exists
     * @param oldStatefulSet    the old stateful set
     */
    public abstract void validateStorageQuotaForUpdate(String namespaceName,
                                                       T request,
                                                       List<Storage> newStorageList,
                                                       List<Storage> existsStorageList,
                                                       W oldStatefulSet);
}
