package cc.backend.kubernetes.batch.form.validator;

import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.kubernetes.batch.form.dto.StatefulSetFormMixture;
import cc.backend.kubernetes.batch.form.dto.VolumeClaimTemplateForm;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageType;
import cc.backend.kubernetes.utils.ResourceLabelHelper;
import cc.backend.kubernetes.workload.statefulset.StatefulSetHelper;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;

import static cc.backend.kubernetes.Constants.APP_LABEL;
import static cc.backend.kubernetes.storage.domain.StorageType.RBD;

/**
 * validator for mixture
 * validate service storage and deployment from the mixture
 *
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/11/7.
 */
@Component
public class StatefulSetMixtureValidator extends BaseMixtureValidator<StatefulSetFormMixture, StatefulSet> {

    /**
     * validate the service for create should not exists
     * <p>
     * createService is null or false and serviceType is null will not create service
     *
     * @param namespaceName the namespace
     * @param request       the request
     */
    public void validateService(String namespaceName,
                                StatefulSetFormMixture request) {
        String serviceName = request.getStatefulSetServiceName();
        if (StringUtils.isEmpty(serviceName)) {
            serviceName = request.getName();
        }

        Service service = clientManager.getClient()
                .services()
                .inNamespace(namespaceName)
                .withName(serviceName)
                .get();

        if (service != null) {
            throw new CcException(BackendReturnCodeNameConstants.SERVICE_ALREADY_EXISTS,
                    ImmutableMap.of("namespaceName", namespaceName,
                            "serviceName", serviceName));
        }
    }

    /**
     * validate the stateful set for create should not exists
     *
     * @param namespaceName   the namespace
     * @param appName         the app
     * @param statefulSetName the stateful set name
     */
    public void validateStatefulSetShouldNotExists(String namespaceName,
                                                   String appName,
                                                   String statefulSetName) {
        StatefulSet statefulSet = clientManager.getClient()
                .apps()
                .statefulSets()
                .inNamespace(namespaceName)
                .withName(statefulSetName)
                .get();

        if (statefulSet != null) {
            throw new CcException(BackendReturnCodeNameConstants.STATEFULSET_ALREADY_EXISTS,
                    ImmutableMap.of("namespaceName", namespaceName,
                            "appName", appName,
                            "statefulSetName", statefulSetName));
        }
    }

    /**
     * validate the deployment should exists
     *
     * @param namespaceName   the namespace name
     * @param appName         the app name
     * @param statefulSetName the stateful set for check
     * @return the k8s stateful set
     */
    public StatefulSet validateStatefulSetShouldExists(String namespaceName,
                                                       String appName,
                                                       String statefulSetName) {
        StatefulSet statefulSet = clientManager.getClient()
                .apps()
                .statefulSets()
                .inNamespace(namespaceName)
                .withName(statefulSetName)
                .get();

        if (statefulSet != null) {
            String label = ResourceLabelHelper.getLabel(statefulSet, APP_LABEL);
            if (label != null && appName.equals(label)) {
                return statefulSet;
            }
        }

        throw new CcException(BackendReturnCodeNameConstants.STATEFULSET_NOT_EXISTS_IN_APP,
                ImmutableMap.of("namespaceName", namespaceName,
                        "appName", appName,
                        "statefulSetName", statefulSetName));
    }

    @Override
    public void validateRbdStorageQuotaForCreate(String namespaceName,
                                                 StatefulSetFormMixture request,
                                                 List<Storage> newStorageList) {
        long newRbdBytes = 0;

        //different from deployment,statefulset should consider volume claim template
        List<VolumeClaimTemplateForm> volumeClaimTemplates = request.getVolumeClaimTemplates();
        for (VolumeClaimTemplateForm volumeClaimTemplate : volumeClaimTemplates) {
            newRbdBytes += volumeClaimTemplate.getAmountBytes();
        }
        newRbdBytes *= request.getReplicas() == null ? 0 : request.getReplicas();
        newRbdBytes += newStorageList.stream()
                .filter(storage -> storage.getStorageType() == RBD)
                .mapToLong(Storage::getAmountBytes)
                .sum();

        if (newRbdBytes > 0) {
            long availableRbdBytes = namespaceStatsManagement.
                    getNamespaceStorageAvailableBytes(namespaceName, StorageType.RBD);
            if (availableRbdBytes < newRbdBytes) {
                throw new CcException(BackendReturnCodeNameConstants.FORM_STATEFULSET_CEPH_RBD_STORAGE_OVER_QUOTA,
                        ImmutableMap.of("namespaceName", namespaceName,
                                "deploymentName", request.getName(),
                                "newRbdBytes", newRbdBytes,
                                "availableRbdBytes", availableRbdBytes));
            }
        }
    }

    /**
     * validate hostpath and rbd storage for deployment update
     *
     * @param namespaceName     the namespace name
     * @param request           the request
     * @param newStorageList    the storage list for create
     * @param existsStorageList the storage list already exists
     * @param oldStatefulSet    the old stateful set
     */
    @Override
    public void validateStorageQuotaForUpdate(String namespaceName,
                                              StatefulSetFormMixture request,
                                              List<Storage> newStorageList,
                                              List<Storage> existsStorageList,
                                              StatefulSet oldStatefulSet) {

        validateRbdStorageQuotaForUpdate(namespaceName, request, newStorageList, oldStatefulSet);

        long newHostpathBytes = getHostpathBytes(request, newStorageList, existsStorageList);
        if (newHostpathBytes == 0) {
            return;
        }

        long oldHostpathBytes = StatefulSetHelper.getHostpathBytes(namespaceName, oldStatefulSet, storageRepository);
        if (oldHostpathBytes > newHostpathBytes) {
            return;
        }

        long availableBytes = namespaceStatsManagement.getNamespaceStorageAvailableBytes(namespaceName, StorageType.HostPath);
        if (availableBytes + oldHostpathBytes < newHostpathBytes) {
            throw new CcException(BackendReturnCodeNameConstants.FORM_DEPLOYMENT_HOSTPATH_STORAGE_OVER_QUOTA,
                    ImmutableMap.of("namespaceName", namespaceName,
                            "deploymentName", request.getName(),
                            "hostpathBytes", newHostpathBytes,
                            "availableBytes", availableBytes));
        }
    }

    /**
     * validate rbd storage quota for workload update and create
     *
     * @param namespaceName  the namespace name
     * @param request        the request
     * @param newStorageList the storage list for create
     */
    public void validateRbdStorageQuotaForUpdate(String namespaceName,
                                                 StatefulSetFormMixture request,
                                                 List<Storage> newStorageList,
                                                 StatefulSet oldStatefulSet) {
        Integer oldReplicas = oldStatefulSet.getSpec().getReplicas();
        if (oldReplicas == null) {
            oldReplicas = 0;
        }

        Integer newReplicas = request.getReplicas();

        long newRbdBytes = 0;

        if (newReplicas != null && oldReplicas < newReplicas) {
            //different from deployment,statefulset should consider volume claim template
            List<VolumeClaimTemplateForm> volumeClaimTemplates = request.getVolumeClaimTemplates();
            for (VolumeClaimTemplateForm volumeClaimTemplate : volumeClaimTemplates) {
                newRbdBytes += volumeClaimTemplate.getAmountBytes();
            }
            newRbdBytes *= newReplicas - oldReplicas;
        }

        newRbdBytes += newStorageList.stream()
                .filter(storage -> storage.getStorageType() == RBD)
                .mapToLong(Storage::getAmountBytes)
                .sum();

        if (newRbdBytes > 0) {
            long availableRbdBytes = namespaceStatsManagement.
                    getNamespaceStorageAvailableBytes(namespaceName, StorageType.RBD);
            if (availableRbdBytes < newRbdBytes) {
                throw new CcException(BackendReturnCodeNameConstants.FORM_STATEFULSET_CEPH_RBD_STORAGE_OVER_QUOTA,
                        ImmutableMap.of("namespaceName", namespaceName,
                                "deploymentName", request.getName(),
                                "newRbdBytes", newRbdBytes,
                                "availableRbdBytes", availableRbdBytes));
            }
        }
    }
}
