package cc.backend.kubernetes.batch.form.dto;

import cc.backend.kubernetes.Constants;
import cc.backend.kubernetes.service.ServiceHelper;
import cc.backend.kubernetes.service.dto.EnnIngress;
import cc.backend.kubernetes.service.dto.IFormService;
import cc.backend.kubernetes.service.dto.ServiceType;
import cc.backend.kubernetes.storage.domain.Storage;
import io.fabric8.kubernetes.api.model.*;
import org.springframework.util.CollectionUtils;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.*;
import java.util.stream.Collectors;

import static cc.backend.kubernetes.Constants.*;

/**
 * a mixture of base workload(like deployment statefulset) service and storage
 *
 * @author NormanWang06@gmail.com (jinxin) on 2017/7/6.
 */
public abstract class BaseFormMixture implements ConsoleMetadata {

    @NotNull
    @Size(max = RESOURCE_NAME_MAX,
            message = "deployment name must no more than " + RESOURCE_NAME_MAX + " characters.")
    @Pattern(regexp = Constants.RESOURCE_NAME_PATTERN)
    protected String name;

    protected String namespace;

    @Valid
    protected List<FormLabel> labels;

    @Valid
    protected List<FormAnnotation> annotations;

    @Min(0)
    protected Integer replicas;

    @Min(0)
    protected Integer revisionHistoryLimit;


    /**
     * will pod run as a critical pod
     */
    protected Boolean criticalPod;

    protected List<String> imagePullSecrets;

    @Valid
    protected List<FormLabel> podTemplateLabels;

    @Valid
    protected List<FormAnnotation> podTemplateAnnotations;

    @Valid
    protected List<FormContainer> containers;


    @Valid
    protected List<FormStorage> newStorageList;

    /**
     * must specify id or name
     */
    protected List<FormStorage> existsStorageList;

    @Valid
    protected List<FormLabel> serviceLabels;

    @Valid
    protected List<FormAnnotation> serviceAnnotations;

    /**
     * null will not create a service
     */
    protected ServiceType serviceType;

    protected List<String> serviceExternalIPs;

    protected String serviceClusterIP;

    @Valid
    protected List<FormServicePort> servicePorts;

    @Valid
    protected List<FormLabel> serviceSelectors;

    protected List<String> serviceLinks;

    protected EnnIngress ennIngress;

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    @Override
    public List<FormLabel> getLabels() {
        return labels;
    }

    public void setLabels(List<FormLabel> labels) {
        this.labels = labels;
    }

    @Override
    public List<FormAnnotation> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(List<FormAnnotation> annotations) {
        this.annotations = annotations;
    }

    public Integer getReplicas() {
        return replicas;
    }

    public void setReplicas(Integer replicas) {
        this.replicas = replicas;
    }

    public Integer getRevisionHistoryLimit() {
        return revisionHistoryLimit;
    }

    public void setRevisionHistoryLimit(Integer revisionHistoryLimit) {
        this.revisionHistoryLimit = revisionHistoryLimit;
    }

    public Boolean getCriticalPod() {
        return criticalPod;
    }

    public void setCriticalPod(Boolean criticalPod) {
        this.criticalPod = criticalPod;
    }

    public List<String> getImagePullSecrets() {
        return imagePullSecrets;
    }

    public void setImagePullSecrets(List<String> imagePullSecrets) {
        this.imagePullSecrets = imagePullSecrets;
    }

    public List<FormLabel> getPodTemplateLabels() {
        return podTemplateLabels;
    }

    public void setPodTemplateLabels(List<FormLabel> podTemplateLabels) {
        this.podTemplateLabels = podTemplateLabels;
    }

    public List<FormAnnotation> getPodTemplateAnnotations() {
        return podTemplateAnnotations;
    }

    public void setPodTemplateAnnotations(List<FormAnnotation> podTemplateAnnotations) {
        this.podTemplateAnnotations = podTemplateAnnotations;
    }

    public List<FormContainer> getContainers() {
        return containers;
    }

    public void setContainers(List<FormContainer> containers) {
        this.containers = containers;
    }

    public List<FormStorage> getNewStorageList() {
        return newStorageList;
    }

    public void setNewStorageList(List<FormStorage> newStorageList) {
        this.newStorageList = newStorageList;
    }

    public List<FormStorage> getExistsStorageList() {
        return existsStorageList;
    }

    public void setExistsStorageList(List<FormStorage> existsStorageList) {
        this.existsStorageList = existsStorageList;
    }

    public List<FormLabel> getServiceLabels() {
        return serviceLabels;
    }

    public void setServiceLabels(List<FormLabel> serviceLabels) {
        this.serviceLabels = serviceLabels;
    }

    public List<FormAnnotation> getServiceAnnotations() {
        return serviceAnnotations;
    }

    public void setServiceAnnotations(List<FormAnnotation> serviceAnnotations) {
        this.serviceAnnotations = serviceAnnotations;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public List<String> getServiceExternalIPs() {
        return serviceExternalIPs;
    }

    public void setServiceExternalIPs(List<String> serviceExternalIPs) {
        this.serviceExternalIPs = serviceExternalIPs;
    }

    public String getServiceClusterIP() {
        return serviceClusterIP;
    }

    public void setServiceClusterIP(String serviceClusterIP) {
        this.serviceClusterIP = serviceClusterIP;
    }

    public List<FormServicePort> getServicePorts() {
        return servicePorts;
    }

    public void setServicePorts(List<FormServicePort> servicePorts) {
        this.servicePorts = servicePorts;
    }

    public List<FormLabel> getServiceSelectors() {
        return serviceSelectors;
    }

    public void setServiceSelectors(List<FormLabel> serviceSelectors) {
        this.serviceSelectors = serviceSelectors;
    }

    public List<String> getServiceLinks() {
        return serviceLinks;
    }

    public void setServiceLinks(List<String> serviceLinks) {
        this.serviceLinks = serviceLinks;
    }

    public EnnIngress getEnnIngress() {
        return ennIngress;
    }

    public void setEnnIngress(EnnIngress ennIngress) {
        this.ennIngress = ennIngress;
    }

    @Override
    public String toString() {
        return "BaseFormMixture{" +
                "name='" + name + '\'' +
                ", namespace='" + namespace + '\'' +
                ", labels=" + labels +
                ", annotations=" + annotations +
                ", replicas=" + replicas +
                ", revisionHistoryLimit=" + revisionHistoryLimit +
                ", criticalPod=" + criticalPod +
                ", imagePullSecrets=" + imagePullSecrets +
                ", podTemplateLabels=" + podTemplateLabels +
                ", podTemplateAnnotations=" + podTemplateAnnotations +
                ", containers=" + containers +
                ", newStorageList=" + newStorageList +
                ", existsStorageList=" + existsStorageList +
                ", serviceLabels=" + serviceLabels +
                ", serviceAnnotations=" + serviceAnnotations +
                ", serviceType=" + serviceType +
                ", serviceExternalIPs=" + serviceExternalIPs +
                ", serviceClusterIP='" + serviceClusterIP + '\'' +
                ", servicePorts=" + servicePorts +
                ", serviceSelectors=" + serviceSelectors +
                ", serviceLinks=" + serviceLinks +
                ", ennIngress=" + ennIngress +
                '}';
    }

    public static void fillService(Service service, BaseFormMixture formMixture, String vipHost) {
        if (service != null) {
            ObjectMeta serviceMetadata = service.getMetadata();
            ServiceSpec serviceSpec = service.getSpec();

            formMixture.setServiceLabels(FormLabel.from(serviceMetadata.getLabels()));
            formMixture.setServiceType(ServiceHelper.getFormType(service));
            formMixture.setServiceExternalIPs(serviceSpec.getExternalIPs());
            formMixture.setServicePorts(FormServicePort.from(serviceSpec.getPorts()));
            formMixture.setServiceSelectors(FormLabel.from(serviceSpec.getSelector()));
            formMixture.setServiceLinks(ServiceHelper.composeLink(service, vipHost));

            Map<String, String> annotations = new HashMap<>(Optional.ofNullable(serviceMetadata.getAnnotations()).orElseGet(HashMap::new));
            formMixture.setEnnIngress(EnnIngress.from(annotations));

            IFormService.cleanAnnotations(annotations);
            formMixture.setServiceAnnotations(FormAnnotation.from(annotations));
        }
    }

    public static ObjectMeta composeMetadata(String appName,
                                             BaseFormMixture mixture) {
        Map<String, String> labels = FormLabel.toMap(mixture.getLabels());
        labels.put(APP_LABEL, appName);

        ObjectMeta metadata = new ObjectMeta();
        metadata.setName(mixture.getName());
        metadata.setNamespace(mixture.getNamespace());
        metadata.setLabels(labels);
        metadata.setAnnotations(FormAnnotation.toMap(mixture.getAnnotations()));

        return metadata;
    }

    public static PodTemplateSpec composePodTemplateSpec(BaseFormMixture mixture,
                                                         List<Storage> storageList) {
        PodTemplateSpec template = new PodTemplateSpec();
        ObjectMeta templateMetadata = new ObjectMeta();
        templateMetadata.setLabels(FormLabel.toMap(mixture.getPodTemplateLabels()));
        Map<String, String> annotations = FormAnnotation.toMap(mixture.getPodTemplateAnnotations());
        if (mixture.getCriticalPod() != null && mixture.getCriticalPod()) {
            annotations.put(CRITICAL_POD_ANNOTATION, "true");
        }
        templateMetadata.setAnnotations(annotations);
        template.setMetadata(templateMetadata);

        //deployment spec pod template spec
        PodSpec podSpec = new PodSpec();

        if (mixture.getContainers() != null) {
            List<Container> containers = FormContainer.toContainers(mixture.getContainers(), new ArrayList<>());
            podSpec.setContainers(containers);
        }

        if (!CollectionUtils.isEmpty(mixture.getImagePullSecrets())) {
            List<LocalObjectReference> localObjectReferenceList = mixture.getImagePullSecrets()
                    .stream()
                    .map(secret -> {
                        LocalObjectReference reference = new LocalObjectReference();
                        reference.setName(secret);
                        return reference;
                    }).collect(Collectors.toList());
            podSpec.setImagePullSecrets(localObjectReferenceList);
        }

        podSpec.setVolumes(composePodVolumes(storageList));

        template.setSpec(podSpec);
        return template;
    }

    public static List<Volume> composePodVolumes(List<Storage> storageList) {
        return storageList.stream()
                .map(storage -> {
                    Volume volume = new Volume();
                    volume.setName(storage.getStorageName());
                    PersistentVolumeClaimVolumeSource pvc = new PersistentVolumeClaimVolumeSource();
                    pvc.setClaimName(storage.getPvcName());
                    volume.setPersistentVolumeClaim(pvc);
                    return volume;
                })
                .collect(Collectors.toList());
    }
}
