package cc.backend.kubernetes.batch.form;

import cc.backend.kubernetes.apps.validator.AppValidator;
import cc.backend.kubernetes.batch.form.dto.DeploymentFormMixture;
import cc.backend.kubernetes.batch.form.dto.StatefulSetFormMixture;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * TODO validate deployment support privilege/hostpath in yaml
 *
 * @author normanwang06@gmail.com (wangjinxin) on 2017/11/14.
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes/namespaces/{namespaceName}/apps/{appName}/form/mixture")
@Api(value = "Form", description = "The form to create deployment.", produces = "application/json")
public class MixtureEndpoint {

    @Inject
    private AppValidator appValidator;

    @Inject
    private MixtureManagement management;

    /**
     * create deployment, service, storage by a form
     *
     * @param namespaceName namespace name
     * @return response
     */
    @POST
    @ApiOperation(value = "Create deployment storage and service by a form. " +
            "When use exists storage, you can just specify id or name")
    @Path("deployments")
    public Response createDeploymentMixture(@PathParam("namespaceName") String namespaceName,
                                            @PathParam("appName") String appName,
                                            @Valid DeploymentFormMixture request) {
        request.setNamespace(namespaceName);
        appValidator.validateAppShouldExists(namespaceName, appName);
        management.createDeploymentMixture(namespaceName, appName, request);
        return Response.ok().build();
    }

    /**
     * get deployment, service, storage in a form
     *
     * @param namespaceName  the namespace
     * @param appName        the app
     * @param deploymentName the deployment
     * @return the FormMixture
     */
    @GET
    @ApiOperation(value = "Get deployment storage and service in a form. ", response = DeploymentFormMixture.class)
    @Path("deployments/{deploymentName}")
    @ApiResponses(value = {@ApiResponse(code = 404, message = "namespace, app or deployment not exists")})
    public Response getDeploymentMixture(@PathParam("namespaceName") String namespaceName,
                                         @PathParam("appName") String appName,
                                         @PathParam("deploymentName") String deploymentName) {
        appValidator.validateAppShouldExists(namespaceName, appName);
        return Response.ok(management.getDeploymentMixture(namespaceName, appName, deploymentName)).build();
    }

    /**
     * update deployment, service in a form
     *
     * @param namespaceName  the namespace
     * @param appName        the app
     * @param deploymentName the deployment
     * @param request        the request
     * @return void
     */
    @PUT
    @ApiOperation(value = "Update deployment and service or create storage in a form. " +
            "When use exists storage, you can just specify id or name")
    @Path("deployments/{deploymentName}")
    public Response updateDeploymentMixture(@PathParam("namespaceName") String namespaceName,
                                            @PathParam("appName") String appName,
                                            @PathParam("deploymentName") String deploymentName,
                                            @Valid DeploymentFormMixture request) {
        appValidator.validateAppShouldExists(namespaceName, appName);
        request.setName(deploymentName);
        request.setNamespace(namespaceName);

        management.updateDeploymentMixture(namespaceName, appName, request);
        return Response.ok().build();
    }

    /**
     * create stateful set, service, storage by a form
     *
     * @param namespaceName namespace name
     * @return response
     */
    @POST
    @ApiOperation(value = "Create stateful set, storage and service by a form. " +
            "When use exists storage, you can just specify id or name")
    @Path("stateful_sets")
    public Response createStatefulSetMixture(@PathParam("namespaceName") String namespaceName,
                                             @PathParam("appName") String appName,
                                             @Valid StatefulSetFormMixture request) {
        request.setNamespace(namespaceName);
        appValidator.validateAppShouldExists(namespaceName, appName);
        management.createStatefulSetMixture(namespaceName, appName, request);
        return Response.ok().build();
    }

    /**
     * update stateful set, service by a form
     *
     * @param namespaceName namespace name
     * @return response
     */
    @PUT
    @ApiOperation(value = "update stateful set and service or create storage by form. " +
            "When use exists storage, you can just specify id or name")
    @Path("stateful_sets/{statefulSetName}")
    public Response updateStatefulSetMixture(@PathParam("namespaceName") String namespaceName,
                                             @PathParam("appName") String appName,
                                             @PathParam("statefulSetName") String statefulSetName,
                                             @Valid StatefulSetFormMixture request) {
        request.setName(statefulSetName);
        request.setNamespace(namespaceName);
        appValidator.validateAppShouldExists(namespaceName, appName);
        management.updateStatefulSetMixture(namespaceName, appName, request);
        return Response.ok().build();
    }

    /**
     * update stateful set, service by a form
     *
     * @param namespaceName namespace name
     * @return response
     */
    @GET
    @ApiOperation(value = "Get stateful set, storage and service in a form.", response = StatefulSetFormMixture.class)
    @Path("stateful_sets/{statefulSetName}")
    public Response getStatefulSetMixture(@PathParam("namespaceName") String namespaceName,
                                          @PathParam("appName") String appName,
                                          @PathParam("statefulSetName") String statefulSetName) {
        appValidator.validateAppShouldExists(namespaceName, appName);
        return Response.ok(management.getStatefulSetMixture(namespaceName, appName, statefulSetName)).build();
    }
}
