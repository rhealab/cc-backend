package cc.backend.kubernetes.batch.form.deprecated;

import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.namespaces.services.NamespaceStatsManagement;
import cc.backend.kubernetes.storage.StorageRepository;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageType;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.PersistentVolumeClaimVolumeSource;
import io.fabric8.kubernetes.api.model.Volume;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

/**
 * Created by NormanWang06@gmail.com (jinxin) on 2017/6/26.
 */
@Component
@Deprecated
public class FormValidator {
    @Inject
    private NamespaceStatsManagement namespaceStatsManagement;

    @Inject
    private StorageRepository storageRepository;

    @Inject
    private KubernetesClientManager clientManager;

    @Inject
    private WorkloadComputeQuotaValidator workloadComputeQuotaValidator;

    public void validateDeploymentForCreate(String namespaceName, String appName, Deployment deployment) {
        String deploymentName = deployment.getMetadata().getName();
        deployment.getMetadata().setNamespace(namespaceName);
        if (clientManager.getClient().extensions().deployments().
                inNamespace(namespaceName).withName(deploymentName).get() != null) {
            throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_ALREADY_EXISTS,
                    ImmutableMap.of("namespace", namespaceName,
                            "app", appName,
                            "deployment", deploymentName));
        }
//        validateComputeQuota(namespaceName, appName, deployment);
        workloadComputeQuotaValidator.validateDeploymentComputeQuota(
                namespaceName, appName, deployment, WorkloadComputeQuotaValidator.ActionType.CREATE);
//        validateStorageStatus(deployment);
        validateHostPathQuotaForCreateDeprecated(appName, deployment);
    }


    private void validateHostPathQuotaForCreateDeprecated(String appName, Deployment deployment) {
        Integer replicas = deployment.getSpec().getReplicas();
        if (replicas == null || replicas <= 1) {
            return;
        }

        //FIXME multi deployment use some storage has problem
        validateHostPathQuota(appName, deployment, replicas - 1);
    }


    private void validateHostPathQuota(String appName, Deployment deployment, int extrasReplicasNum) {
        String namespaceName = deployment.getMetadata().getNamespace();
        List<Volume> volumes = deployment.getSpec().getTemplate().getSpec().getVolumes();

        long unsharedHostPathBytes = 0;
        for (Volume volume : volumes) {
            PersistentVolumeClaimVolumeSource pvc = volume.getPersistentVolumeClaim();
            if (pvc != null) {
                String pvcName = pvc.getClaimName();
                Storage storage = storageRepository.findByNamespaceNameAndPvcName(namespaceName, pvcName);
                if (storage != null && storage.getStorageType() == StorageType.HostPath && storage.isUnshared()) {
                    unsharedHostPathBytes += storage.getAmountBytes();
                }
            }
        }

        long neededBytes = unsharedHostPathBytes * extrasReplicasNum;
        if (neededBytes > 0) {
            long availableByte = namespaceStatsManagement
                    .getNamespaceStorageAvailableBytes(namespaceName, StorageType.HostPath);

            if (neededBytes > availableByte) {
                String deploymentName = deployment.getMetadata().getName();
                Map<String, Object> map = new ImmutableMap.Builder<String, Object>()
                        .put("namespace", namespaceName)
                        .put("app", appName)
                        .put("deployment", deploymentName)
                        .put("neededBytes", neededBytes)
                        .put("limitsBytes", availableByte)
                        .build();

                throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_HOSTPATH_STORAGE_OVER_QUOTA, map);
            }
        }
    }
}