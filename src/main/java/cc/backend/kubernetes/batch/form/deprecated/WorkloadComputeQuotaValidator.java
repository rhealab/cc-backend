package cc.backend.kubernetes.batch.form.deprecated;

import cc.backend.audit.operation.ResourceType;
import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.namespaces.dto.NamespaceNaming;
import cc.backend.kubernetes.utils.UnitUtils;
import cc.backend.kubernetes.workload.deployment.validator.DeploymentsValidator;
import cc.backend.kubernetes.workload.statefulset.StatefulSetValidator;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.Container;
import io.fabric8.kubernetes.api.model.Quantity;
import io.fabric8.kubernetes.api.model.ResourceQuota;
import io.fabric8.kubernetes.api.model.ResourceRequirements;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.Map;

import static cc.backend.kubernetes.Constants.*;

/**
 * @author yanzhixiang on 17-7-21.
 */
@Named
@Deprecated
public class WorkloadComputeQuotaValidator {
    @Inject
    private KubernetesClientManager clientManager;

    @Inject
    private DeploymentsValidator deploymentsValidator;

    @Inject
    private StatefulSetValidator statefulSetValidator;

    public enum ActionType {
        CREATE, UPDATE
    }

    public void validateStatefulSetComputeQuota(String namespaceName, String appName,
                                                StatefulSet statefulSet, ActionType actionType) {
        Integer replicas = statefulSet.getSpec().getReplicas();
        String statefulSetName = statefulSet.getMetadata().getName();
        List<Container> containers = statefulSet.getSpec().getTemplate().getSpec().getContainers();
        validateComputeQuota(namespaceName, appName, statefulSetName,
                ResourceType.STATEFULSET, containers, actionType, replicas);
    }

    public void validateDeploymentComputeQuota(String namespaceName, String appName,
                                               Deployment deployment, ActionType actionType) {
        Integer replicas = deployment.getSpec().getReplicas();
        if (replicas == null) {
            replicas = 0;
        }

        String deploymentName = deployment.getMetadata().getName();
        List<Container> containers = deployment.getSpec().getTemplate().getSpec().getContainers();
        validateComputeQuota(namespaceName, appName, deploymentName,
                ResourceType.DEPLOYMENT, containers, actionType, replicas);
    }

    private void validateComputeQuota(String namespaceName, String appName,
                                      String workloadName, ResourceType resourceType,
                                      List<Container> newContainers, ActionType actionType,
                                      int replicas) {
        double cpuLimitsTotal = 0;
        double cpuRequestsTotal = 0;
        long memoryLimitsBytesTotal = 0;
        long memoryRequestsBytesTotal = 0;

        for (Container container : newContainers) {
            validateComputeProperties(namespaceName, appName, workloadName, resourceType, container);
            ResourceRequirements resources = container.getResources();
            if (resources != null) {
                Map<String, Quantity> limits = resources.getLimits();
                Map<String, Quantity> requests = resources.getRequests();

                double cpuLimits = UnitUtils.parseK8sCpuQuantity(limits.get(K_CPU));
                double cpuRequests = UnitUtils.parseK8sCpuQuantity(requests.get(K_CPU));

                cpuLimitsTotal += cpuLimits;
                cpuRequestsTotal += cpuRequests;

                if (cpuLimits < cpuRequests) {
                    Map<String, Object> map = new ImmutableMap.Builder<String, Object>()
                            .put("resourceType", resourceType)
                            .put("namespace", namespaceName)
                            .put("app", appName)
                            .put(resourceType.name().toLowerCase(), workloadName)
                            .put("requests", cpuRequests)
                            .put("limits", cpuLimits)
                            .build();
                    if (resourceType == ResourceType.DEPLOYMENT) {
                        throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_CPU_REQUESTS_MORE_THAN_LIMITS, map);
                    } else {
                        throw new CcException(BackendReturnCodeNameConstants.STATEFULSET_CPU_REQUESTS_MORE_THAN_LIMITS, map);
                    }
                }

                long memoryLimitsBytes = UnitUtils.parseK8sMemoryQuantity(limits.get(K_MEMORY));
                long memoryRequestsBytes = UnitUtils.parseK8sMemoryQuantity(requests.get(K_MEMORY));

                memoryLimitsBytesTotal += memoryLimitsBytes;
                memoryRequestsBytesTotal += memoryRequestsBytes;

                if (memoryLimitsBytes < memoryRequestsBytes) {
                    Map<String, Object> map = new ImmutableMap.Builder<String, Object>()
                            .put("resourceType", resourceType)
                            .put("namespace", namespaceName)
                            .put("app", appName)
                            .put(resourceType.name().toLowerCase(), workloadName)
                            .put("requestsBytes", memoryRequestsBytes)
                            .put("limitsBytes", memoryLimitsBytes)
                            .build();

                    if (resourceType == ResourceType.DEPLOYMENT) {
                        throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_MEMORY_REQUESTS_MORE_THAN_LIMITS, map);
                    } else {
                        throw new CcException(BackendReturnCodeNameConstants.STATEFULSET_MEMORY_REQUESTS_MORE_THAN_LIMITS, map);
                    }
                }


            }
        }

        memoryRequestsBytesTotal *= replicas;
        memoryLimitsBytesTotal *= replicas;


        cpuRequestsTotal *= replicas;
        cpuLimitsTotal *= replicas;

        ResourceQuota resourceQuota = clientManager.getClient().resourceQuotas()
                .inNamespace(namespaceName).withName(NamespaceNaming.quotaName(namespaceName)).get();
        Map<String, Quantity> hardMap = resourceQuota.getStatus().getHard();
        Map<String, Quantity> usedMap = resourceQuota.getStatus().getUsed();

        double cpuLimitsQuota = UnitUtils.parseK8sCpuQuantity(hardMap.get(K_NAMESPACE_CPU_LIMITS));
        double cpuLimitsUsed = UnitUtils.parseK8sCpuQuantity(usedMap.get(K_NAMESPACE_CPU_LIMITS));

        double cpuRequestsQuota = UnitUtils.parseK8sCpuQuantity(hardMap.get(K_NAMESPACE_CPU_REQUESTS));
        double cpuRequestsUsed = UnitUtils.parseK8sCpuQuantity(usedMap.get(K_NAMESPACE_CPU_REQUESTS));

        long memoryLimitsQuota = UnitUtils.parseK8sMemoryQuantity(hardMap.get(K_NAMESPACE_MEMORY_LIMITS));
        long memoryLimitsUsed = UnitUtils.parseK8sMemoryQuantity(usedMap.get(K_NAMESPACE_MEMORY_LIMITS));

        long memoryRequestsQuota = UnitUtils.parseK8sMemoryQuantity(hardMap.get(K_NAMESPACE_MEMORY_REQUESTS));
        long memoryRequestsUsed = UnitUtils.parseK8sMemoryQuantity(usedMap.get(K_NAMESPACE_MEMORY_REQUESTS));

        if (actionType == ActionType.UPDATE) {
            List<Container> oldContainers;
            Integer oldReplicas;
            if (resourceType == ResourceType.DEPLOYMENT) {
                Deployment oldDeployment = deploymentsValidator.validateDeploymentShouldExists(namespaceName, appName, workloadName);
                oldContainers = oldDeployment.getSpec().getTemplate().getSpec().getContainers();
                oldReplicas = oldDeployment.getSpec().getReplicas();
            } else {
                StatefulSet oldStatefulSet = statefulSetValidator.validateStatefulSetShouldExists(namespaceName, appName, workloadName);
                oldContainers = oldStatefulSet.getSpec().getTemplate().getSpec().getContainers();
                oldReplicas = oldStatefulSet.getSpec().getReplicas();
            }

            if (oldReplicas == null) {
                oldReplicas = 0;
            }

            if (oldContainers != null) {
                double oldCpuRequests = 0;
                double oldCpuLimits = 0;
                long oldMemoryRequests = 0;
                long oldMemoryLimits = 0;
                for (Container container : oldContainers) {
                    ResourceRequirements resources = container.getResources();
                    if (resources != null) {
                        Map<String, Quantity> limits = resources.getLimits();
                        Map<String, Quantity> requests = resources.getRequests();

                        oldCpuLimits += UnitUtils.parseK8sCpuQuantity(limits.get(K_CPU));
                        oldCpuRequests += UnitUtils.parseK8sCpuQuantity(requests.get(K_CPU));
                        oldMemoryLimits += UnitUtils.parseK8sMemoryQuantity(limits.get(K_MEMORY));
                        oldMemoryRequests += UnitUtils.parseK8sMemoryQuantity(requests.get(K_MEMORY));
                    }
                }

                oldCpuRequests *= oldReplicas;
                oldCpuLimits *= oldReplicas;
                oldMemoryRequests *= oldReplicas;
                oldMemoryLimits *= oldReplicas;

                cpuLimitsUsed -= oldCpuLimits;
                cpuRequestsUsed -= oldCpuRequests;

                memoryLimitsBytesTotal -= oldMemoryLimits;
                memoryRequestsBytesTotal -= oldMemoryRequests;
            }

        }

        double remainingCpuLimits = cpuLimitsQuota - cpuLimitsUsed;
        if (cpuLimitsTotal > remainingCpuLimits) {
            Map<String, Object> map = new ImmutableMap.Builder<String, Object>()
                    .put("resourceType", resourceType)
                    .put("namespace", namespaceName)
                    .put("app", appName)
                    .put(resourceType.name().toLowerCase(), workloadName)
                    .put("cpuLimitsOfContainers", cpuLimitsTotal)
                    .put("cpuLimitsQuota", cpuLimitsQuota)
                    .put("cpuLimitsUsed", cpuLimitsUsed)
                    .build();
            if (resourceType == ResourceType.DEPLOYMENT) {
                throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_CPU_LIMITS_OVER_NAMESPACE_QUOTA,
                        map);
            } else {
                throw new CcException(BackendReturnCodeNameConstants.STATEFULSET_CPU_LIMITS_OVER_NAMESPACE_QUOTA,
                        map);
            }
        }


        double remainingCpuRequests = cpuRequestsQuota - cpuRequestsUsed;
        if (cpuRequestsTotal > remainingCpuRequests) {
            Map<String, Object> map = new ImmutableMap.Builder<String, Object>()
                    .put("resourceType", resourceType)
                    .put("namespace", namespaceName)
                    .put("app", appName)
                    .put(resourceType.name().toLowerCase(), workloadName)
                    .put("cpuRequestsOfContainers", cpuRequestsTotal)
                    .put("cpuRequestsQuota", cpuRequestsQuota)
                    .put("cpuRequestsUsed", cpuRequestsUsed)
                    .build();
            if (resourceType == ResourceType.DEPLOYMENT) {
                throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_CPU_REQUESTS_OVER_NAMESPACE_QUOTA,
                        map);
            } else {
                throw new CcException(BackendReturnCodeNameConstants.STATEFULSET_CPU_REQUESTS_OVER_NAMESPACE_QUOTA,
                        map);
            }
        }


        long remainingMemoryLimits = memoryLimitsQuota - memoryLimitsUsed;
        if (memoryLimitsBytesTotal > remainingMemoryLimits) {
            Map<String, Object> map = new ImmutableMap.Builder<String, Object>()
                    .put("resourceType", resourceType)
                    .put("namespace", namespaceName)
                    .put("app", appName)
                    .put(resourceType.name().toLowerCase(), workloadName)
                    .put("memoryLimitsOfContainers", memoryLimitsBytesTotal)
                    .put("memoryLimitsQuota", memoryLimitsQuota)
                    .put("memoryLimitsUsed", memoryLimitsUsed)
                    .build();
            if (resourceType == ResourceType.DEPLOYMENT) {
                throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_MEMORY_LIMITS_OVER_NAMESPACE_QUOTA,
                        map);
            } else {
                throw new CcException(BackendReturnCodeNameConstants.STATEFULSET_MEMORY_LIMITS_OVER_NAMESPACE_QUOTA,
                        map);
            }
        }


        long remainingMemoryRequests = memoryRequestsQuota - memoryRequestsUsed;
        if (memoryRequestsBytesTotal > remainingMemoryRequests) {
            Map<String, Object> map = new ImmutableMap.Builder<String, Object>()
                    .put("resourceType", resourceType)
                    .put("namespace", namespaceName)
                    .put("app", appName)
                    .put(resourceType.name().toLowerCase(), workloadName)
                    .put("memoryRequestsOfContainers", memoryRequestsBytesTotal)
                    .put("memoryRequestsQuota", memoryRequestsQuota)
                    .put("memoryRequestsUsed", memoryRequestsUsed)
                    .build();
            if (resourceType == ResourceType.DEPLOYMENT) {
                throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_MEMORY_REQUESTS_OVER_NAMESPACE_QUOTA,
                        map);
            } else {
                throw new CcException(BackendReturnCodeNameConstants.STATEFULSET_MEMORY_REQUESTS_OVER_NAMESPACE_QUOTA,
                        map);
            }
        }

    }

    protected void validateComputeProperties(String namespaceName,
                                             String appName,
                                             String workloadName,
                                             ResourceType resourceType,
                                             Container container) {
        ResourceRequirements resources = container.getResources();
        if (resources == null) {
            throw composePropertyInvalidException(namespaceName, appName, workloadName,
                    resourceType, "resources", "must be specified");
        }

        Map<String, Quantity> limits = resources.getLimits();
        if (limits == null) {
            throw composePropertyInvalidException(namespaceName, appName, workloadName,
                    resourceType, "resources.limits", "must be specified");
        }

        Map<String, Quantity> requests = resources.getRequests();
        if (requests == null) {
            throw composePropertyInvalidException(namespaceName, appName, workloadName,
                    resourceType, "resources.requests", "must be specified");
        }

        if (requests.get(K_CPU) == null) {
            throw composePropertyInvalidException(namespaceName, appName, workloadName,
                    resourceType, "resources.requests.cpu", "must be specified");
        }

        if (requests.get(K_MEMORY) == null) {
            throw composePropertyInvalidException(namespaceName, appName, workloadName,
                    resourceType, "resources.requests.memory", "must be specified");
        }

        if (limits.get(K_CPU) == null) {
            throw composePropertyInvalidException(namespaceName, appName, workloadName,
                    resourceType, "resources.limits.cpu", "must be specified");
        }

        if (limits.get(K_MEMORY) == null) {
            throw composePropertyInvalidException(namespaceName, appName, workloadName,
                    resourceType, "resources.limits.memory", "must be specified");
        }
    }

    protected CcException composePropertyInvalidException(String namespaceName,
                                                          String appName,
                                                          String workloadName,
                                                          ResourceType resourceType,
                                                          String property,
                                                          String msg) {
        Map<String, Object> map = new ImmutableMap.Builder<String, Object>()
                .put("resourceType", resourceType)
                .put("namespace", namespaceName)
                .put("app", appName)
                .put(resourceType.name().toLowerCase(), workloadName)
                .put(property, msg)
                .build();

        return new CcException(BackendReturnCodeNameConstants.PROPERTY_INVALID, map);
    }
}
