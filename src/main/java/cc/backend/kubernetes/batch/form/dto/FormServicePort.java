package cc.backend.kubernetes.batch.form.dto;

import cc.backend.common.utils.Strings;
import io.fabric8.kubernetes.api.model.*;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/20.
 */
public class FormServicePort {
    private String name;
    private int port;
    private int targetPort;
    private ProtocolType protocol;
    private Integer nodePort;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getTargetPort() {
        return targetPort;
    }

    public void setTargetPort(int targetPort) {
        this.targetPort = targetPort;
    }

    public ProtocolType getProtocol() {
        return protocol;
    }

    public void setProtocol(ProtocolType protocol) {
        this.protocol = protocol;
    }

    public Integer getNodePort() {
        return nodePort;
    }

    public void setNodePort(Integer nodePort) {
        this.nodePort = nodePort;
    }

    public static List<ContainerPort> toContainerPorts(List<FormServicePort> formServicePorts) {
        return Optional.ofNullable(formServicePorts)
                .orElseGet(ArrayList::new)
                .stream()
                .map(FormServicePort::toContainerPort)
                .collect(Collectors.toList());
    }

    public static ContainerPort toContainerPort(FormServicePort formPort) {
        String protocol = Optional.ofNullable(formPort.getProtocol()).orElse(ProtocolType.TCP).name();
        return new ContainerPortBuilder()
                .withContainerPort(formPort.getTargetPort() == 0 ? null : formPort.getTargetPort())
                .withProtocol(protocol)
                .build();
    }

    public static List<ServicePort> toServicePorts(List<FormServicePort> formServicePorts) {
        return Optional.ofNullable(formServicePorts)
                .orElseGet(ArrayList::new)
                .stream()
                .map(FormServicePort::toServicePort)
                .collect(Collectors.toList());
    }

    public static ServicePort toServicePort(FormServicePort formPort) {
        String protocol = Optional.ofNullable(formPort.getProtocol()).orElse(ProtocolType.TCP).name();
        Integer port = formPort.getPort() == 0 ? null : formPort.getPort();

        Integer targetPort;
        if (formPort.getTargetPort() == 0) {
            targetPort = port;
        } else {
            targetPort = formPort.getTargetPort();
        }

        return new ServicePortBuilder()
                .withProtocol(protocol)
                .withPort(port)
                .withTargetPort(targetPort == null ? null : new IntOrString(targetPort))
                .withName(getServicePortName(formPort))
                .withNodePort(formPort.getNodePort())
                .build();
    }

    private static String getServicePortName(FormServicePort formPort) {
        if (StringUtils.isEmpty(formPort.getName())) {

            String name = Optional.ofNullable(formPort.getProtocol())
                    .orElse(ProtocolType.TCP)
                    .name()
                    .toLowerCase();
            if (formPort.getPort() > 0) {
                name = name + "-" + formPort.getPort();
                if (formPort.getTargetPort() <= 0) {
                    name = name + "-" + formPort.getPort();
                } else {
                    name = name + "-" + formPort.getTargetPort();
                }
            }
            return name + "-" + Strings.shortLowerRandomString();

        } else {
            return formPort.getName();
        }
    }

    public static List<FormServicePort> from(List<ServicePort> servicePorts) {
        return servicePorts
                .stream()
                .map(FormServicePort::from)
                .collect(Collectors.toList());
    }

    public static FormServicePort from(ServicePort servicePort) {
        FormServicePort formServicePort = new FormServicePort();

        formServicePort.setNodePort(servicePort.getNodePort());
        formServicePort.setPort(Optional.ofNullable(servicePort.getPort()).orElse(0));
        formServicePort.setName(servicePort.getName());
        if (servicePort.getProtocol() != null) {
            formServicePort.setProtocol(ProtocolType.valueOf(servicePort.getProtocol()));
        }

        if (servicePort.getTargetPort() != null || servicePort.getTargetPort().getIntVal() != null) {
            formServicePort.setTargetPort(servicePort.getTargetPort().getIntVal());
        } else {
            formServicePort.setTargetPort(formServicePort.getPort());
        }

        return formServicePort;
    }
}
