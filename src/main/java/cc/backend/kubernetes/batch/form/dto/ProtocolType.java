package cc.backend.kubernetes.batch.form.dto;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/20.
 */
public enum ProtocolType {
    TCP, UDP
}
