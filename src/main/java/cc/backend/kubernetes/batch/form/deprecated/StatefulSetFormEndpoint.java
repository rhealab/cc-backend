package cc.backend.kubernetes.batch.form.deprecated;

import cc.backend.kubernetes.apps.validator.AppValidator;
import cc.backend.kubernetes.workload.statefulset.StatefulSetEndpoint;
import cc.backend.kubernetes.workload.statefulset.StatefulSetManagement;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author yanzhixiang on 17-8-7.
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes/namespaces/{namespaceName}/apps/{appName}/statefulsets")
@Api(value = "StatefulSet", description = "Info about statefulsets.", produces = "application/json")
@Deprecated
public class StatefulSetFormEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(StatefulSetEndpoint.class);

    @Inject
    private AppValidator appValidator;

    @Inject
    private StatefulSetFormManagement statefulSetFormManagement;

    @Inject
    private StatefulSetManagement statefulSetManagement;

    @POST
    @Path("form")
    @ApiOperation(value = "Deprecated! Create statefulset by form.")
    @Deprecated
    public Response createFormStatefulSet(@PathParam("namespaceName") String namespaceName,
                                          @PathParam("appName") String appName,
                                          @Valid FormStatefulSet request) {
        appValidator.validateAppShouldExists(namespaceName, appName);
        statefulSetFormManagement.createFromForm(namespaceName, appName, request);
        return Response.ok().build();
    }

    @GET
    @Path("{statefulSetName}/form")
    @ApiOperation(value = "Deprecated! Get statefulset info of console form format.", response = FormStatefulSet.class)
    @Deprecated
    public Response getStatefulSetForm(@PathParam("namespaceName") String namespaceName,
                                       @PathParam("appName") String appName,
                                       @PathParam("statefulSetName") String statefulSetName) {
        StatefulSet statefulSet = statefulSetManagement.getStatefulSet(namespaceName, appName, statefulSetName);
        return Response.ok(statefulSetFormManagement.composeFormStatefulSet(namespaceName, statefulSet)).build();
    }


    @PUT
    @Path("{statefulSetName}/form")
    @ApiOperation(value = "Deprecated! Update statefulset by form.")
    @Deprecated
    public Response updateStatefulSetByForm(@PathParam("namespaceName") String namespace,
                                            @PathParam("appName") String appName,
                                            @PathParam("statefulSetName") String statefulSetName,
                                            @Valid FormStatefulSet formStatefulSet) {
        statefulSetFormManagement.updateByForm(namespace, appName, formStatefulSet);
        return Response.ok().build();
    }
}
