package cc.backend.kubernetes.batch.form.dto;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/10/23.
 * <p>
 * Defaults to Always if :latest tag is specified, or IfNotPresent
 */
public enum ImagePullPolicy {
    /**
     * always force a pull
     */
    Always,

    /**
     * never pull ??
     */
    Never,

    /**
     * skip pulling an image if it already exists
     */
    IfNotPresent;
}
