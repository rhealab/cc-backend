package cc.backend.kubernetes.batch.form.deprecated;

import cc.backend.kubernetes.Constants;
import cc.backend.kubernetes.batch.form.dto.*;
import cc.backend.kubernetes.storage.domain.Storage;
import com.google.common.collect.ImmutableList;
import io.fabric8.kubernetes.api.model.*;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.DeploymentSpec;
import io.fabric8.kubernetes.api.model.extensions.DeploymentStrategy;
import io.fabric8.kubernetes.api.model.extensions.RollbackConfig;
import org.springframework.util.StringUtils;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cc.backend.kubernetes.Constants.*;

/**
 * Deprecated please use DeploymentFormMixture
 *
 * @author NormanWang06@gmail.com (jinxin) on 2017/7/6.
 */
@Deprecated
public class FormDeployment {

    private String imagePullSecret;
    private List<String> imagePullSecrets;
    @NotNull
    @Size(max = RESOURCE_NAME_MAX, message = "deployment name must no more than " + RESOURCE_NAME_MAX + " characters.")
    @Pattern(regexp = Constants.RESOURCE_NAME_PATTERN)
    private String name;
    private String description;
    @Min(0)
    private int replicas;

    private String namespace;

    @Valid
    @NotNull
    private List<FormLabel> labels;
    @Min(0)
    private int minReadySeconds;
    @Min(0)
    private int revisionHistoryLimit;

    private boolean paused;
    @Min(0)
    private Long rollbackRevision;

    @Valid
    private FormDeploymentStrategy strategy;

    //storage for create
    @Valid
    private List<FormStorage> storages;

    /**
     * storage already exists
     */
    private List<Long> storageIdList;

    @Valid
    private List<FormStorage> newStorageList;

    @Valid
    private List<FormStorage> existsStorageList;

    @Valid
    private List<FormContainerDeprecated> containers;

    private Boolean criticalPod;

    /**
     * TODO add annotations key and value format check
     */
    private Map<String, String> annotations;

    private Map<String, String> podTemplateAnnotations;

    public String getImagePullSecret() {
        return imagePullSecret;
    }

    public void setImagePullSecret(String imagePullSecret) {
        this.imagePullSecret = imagePullSecret;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getReplicas() {
        return replicas;
    }

    public void setReplicas(int replicas) {
        this.replicas = replicas;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public List<FormLabel> getLabels() {
        return labels;
    }

    public void setLabels(List<FormLabel> labels) {
        this.labels = labels;
    }

    public int getMinReadySeconds() {
        return minReadySeconds;
    }

    public void setMinReadySeconds(int minReadySeconds) {
        this.minReadySeconds = minReadySeconds;
    }

    public int getRevisionHistoryLimit() {
        return revisionHistoryLimit;
    }

    public void setRevisionHistoryLimit(int revisionHistoryLimit) {
        this.revisionHistoryLimit = revisionHistoryLimit;
    }

    public boolean isPaused() {
        return paused;
    }

    public void setPaused(boolean paused) {
        this.paused = paused;
    }

    public Long getRollbackRevision() {
        return rollbackRevision;
    }

    public void setRollbackRevision(Long rollbackRevision) {
        this.rollbackRevision = rollbackRevision;
    }

    public FormDeploymentStrategy getStrategy() {
        return strategy;
    }

    public void setStrategy(FormDeploymentStrategy strategy) {
        this.strategy = strategy;
    }

    public List<FormStorage> getStorages() {
        return storages;
    }

    public void setStorages(List<FormStorage> storages) {
        this.storages = storages;
    }

    public List<Long> getStorageIdList() {
        return storageIdList;
    }

    public void setStorageIdList(List<Long> storageIdList) {
        this.storageIdList = storageIdList;
    }

    public List<String> getImagePullSecrets() {
        return imagePullSecrets;
    }

    public void setImagePullSecrets(List<String> imagePullSecrets) {
        this.imagePullSecrets = imagePullSecrets;
    }

    public List<FormStorage> getNewStorageList() {
        return newStorageList;
    }

    public void setNewStorageList(List<FormStorage> newStorageList) {
        this.newStorageList = newStorageList;
    }

    public List<FormStorage> getExistsStorageList() {
        return existsStorageList;
    }

    public void setExistsStorageList(List<FormStorage> existsStorageList) {
        this.existsStorageList = existsStorageList;
    }

    public List<FormContainerDeprecated> getContainers() {
        return containers;
    }

    public void setContainers(List<FormContainerDeprecated> containers) {
        this.containers = containers;
    }

    public Boolean getCriticalPod() {
        return criticalPod;
    }

    public void setCriticalPod(Boolean criticalPod) {
        this.criticalPod = criticalPod;
    }

    public Map<String, String> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(Map<String, String> annotations) {
        this.annotations = annotations;
    }

    public Map<String, String> getPodTemplateAnnotations() {
        return podTemplateAnnotations;
    }

    public void setPodTemplateAnnotations(Map<String, String> podTemplateAnnotations) {
        this.podTemplateAnnotations = podTemplateAnnotations;
    }

    @Override
    public String toString() {
        return "FormDeployment{" +
                "imagePullSecret='" + imagePullSecret + '\'' +
                ", imagePullSecrets=" + imagePullSecrets +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", replicas=" + replicas +
                ", namespace='" + namespace + '\'' +
                ", labels=" + labels +
                ", minReadySeconds=" + minReadySeconds +
                ", revisionHistoryLimit=" + revisionHistoryLimit +
                ", paused=" + paused +
                ", rollbackRevision=" + rollbackRevision +
                ", strategy=" + strategy +
                ", storages=" + storages +
                ", storageIdList=" + storageIdList +
                ", newStorageList=" + newStorageList +
                ", existsStorageList=" + existsStorageList +
                ", containers=" + containers +
                ", criticalPod=" + criticalPod +
                ", annotations=" + annotations +
                ", podTemplateAnnotations=" + podTemplateAnnotations +
                '}';
    }

    public static FormDeployment from(Deployment deployment, List<Storage> storageList) {

        ObjectMeta deploymentMetadata = deployment.getMetadata();
        DeploymentSpec deploymentSpec = deployment.getSpec();

        Map<String, String> annotations = deploymentMetadata.getAnnotations();

        PodSpec podSpec = deploymentSpec.getTemplate().getSpec();

        FormDeployment form = new FormDeployment();
        form.setName(deploymentMetadata.getName());

        if (annotations != null) {
            form.setAnnotations(annotations);
            form.setDescription(annotations.get("description"));
        }

        Map<String, String> podAnnotations = deployment.getSpec().getTemplate().getMetadata().getAnnotations();
        if (podAnnotations != null) {
            form.setPodTemplateAnnotations(podAnnotations);
            form.setCriticalPod("true".equalsIgnoreCase(podAnnotations.get(CRITICAL_POD_ANNOTATION)));
        }

        form.setReplicas(deploymentSpec.getReplicas() == null ? 0 : deploymentSpec.getReplicas());
        form.setNamespace(deploymentMetadata.getNamespace());
        form.setLabels(FormLabel.from(deploymentMetadata.getLabels()));
        form.setMinReadySeconds(deploymentSpec.getMinReadySeconds() == null ? 0 : deploymentSpec.getMinReadySeconds());
        form.setRevisionHistoryLimit(deploymentSpec.getRevisionHistoryLimit() == null ? 0 : deploymentSpec.getRevisionHistoryLimit());
        form.setPaused(deploymentSpec.getPaused() == null ? false : deploymentSpec.getPaused());

        RollbackConfig rollbackTo = deploymentSpec.getRollbackTo();
        if (rollbackTo != null) {
            form.setRollbackRevision(rollbackTo.getRevision() == null ? 0 : rollbackTo.getRevision());
        }

        DeploymentStrategy strategy = deploymentSpec.getStrategy();
        form.setStrategy(FormDeploymentStrategy.from(strategy));

        form.setContainers(FormContainerDeprecated.from(podSpec.getContainers()));
        form.setStorages(FormStorage.from(storageList));

        return form;
    }

    public static Deployment to(String namespaceName, String appName, FormDeployment form, List<Storage> storageList) {
        Deployment deployment = new Deployment();

        ObjectMeta metadata = composeDeploymentMetadata(namespaceName, appName, form);
        deployment.setMetadata(metadata);

        DeploymentSpec spec = composeDeploymentSpec(form, storageList);
        deployment.setSpec(spec);

        return deployment;
    }

    private static ObjectMeta composeDeploymentMetadata(String namespaceName,
                                                        String appName,
                                                        FormDeployment form) {
        Map<String, String> labels = FormLabel.toMap(form.getLabels());
        labels.put(APP_LABEL, appName);

        ObjectMeta metadata = new ObjectMeta();
        metadata.setName(form.getName());
        metadata.setNamespace(namespaceName);
        metadata.setLabels(labels);

        Map<String, String> annotations = form.getAnnotations();
        if (!StringUtils.isEmpty(form.getDescription())) {
            if (annotations == null) {
                annotations = new HashMap<>(1);
            }
            annotations.put("description", form.getDescription());
        }
        metadata.setAnnotations(annotations);

        return metadata;
    }

    private static DeploymentSpec composeDeploymentSpec(FormDeployment form,
                                                        List<Storage> storageList) {
        DeploymentSpec spec = new DeploymentSpec();
        FormDeploymentStrategy strategy = form.getStrategy();
        if (strategy != null) {
            spec.setStrategy(FormDeploymentStrategy.toStrategy(strategy));
        }
        spec.setMinReadySeconds(form.getMinReadySeconds());
        spec.setRevisionHistoryLimit(form.getRevisionHistoryLimit());
        spec.setPaused(form.isPaused());
        if (form.getRollbackRevision() != null) {
            RollbackConfig rollbackConfig = new RollbackConfig();
            rollbackConfig.setRevision(form.getRollbackRevision());
            spec.setRollbackTo(rollbackConfig);
        }
        spec.setReplicas(form.getReplicas());
        spec.setSelector(FormLabel.toSelector(form.getLabels()));

        //deployment spec pod template
        PodTemplateSpec template = composePodTemplateSpec(form, storageList);

        spec.setTemplate(template);
        return spec;
    }


    private static PodTemplateSpec composePodTemplateSpec(FormDeployment form,
                                                          List<Storage> storageList) {
        PodTemplateSpec template = new PodTemplateSpec();
        ObjectMeta templateMetadata = new ObjectMeta();
        templateMetadata.setLabels(FormLabel.toMap(form.getLabels()));

        Map<String, String> annotations = form.getPodTemplateAnnotations();

        if (annotations == null) {
            annotations = new HashMap<>(2);
        }

        if (!StringUtils.isEmpty(form.getDescription())) {
            annotations.put("description", form.getDescription());
        }

        if (form.getCriticalPod() != null && form.getCriticalPod()) {
            annotations.put(CRITICAL_POD_ANNOTATION, "true");
        }
        templateMetadata.setAnnotations(annotations);

        template.setMetadata(templateMetadata);

        //deployment spec pod template spec
        PodSpec podSpec = new PodSpec();

        if (form.getContainers() != null) {
            List<Container> containers = FormContainerDeprecated.toContainers(form.getContainers(), new ArrayList<>());
            podSpec.setContainers(containers);
        }

        LocalObjectReference reference = new LocalObjectReference();
        reference.setName(form.getImagePullSecret());
        podSpec.setImagePullSecrets(ImmutableList.of(reference));

        podSpec.setVolumes(BaseFormMixture.composePodVolumes(storageList));

        template.setSpec(podSpec);
        return template;
    }
}
