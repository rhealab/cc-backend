package cc.backend.kubernetes.batch;

import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.kubernetes.service.ServicesManagement;
import cc.backend.kubernetes.utils.NameUtils;
import cc.backend.kubernetes.workload.deployment.service.DeploymentsManagement;
import cc.backend.kubernetes.workload.deployment.validator.DeploymentsValidator;
import cc.backend.kubernetes.workload.statefulset.StatefulSetManagement;
import cc.backend.kubernetes.workload.statefulset.StatefulSetValidator;
import cc.lib.retcode.CcException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.error.YAMLException;

import javax.inject.Inject;
import java.io.IOException;
import java.util.*;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/20.
 */
@Component
public class BatchManagement {
    private static final Logger logger = LoggerFactory.getLogger(BatchManagement.class);

    @Inject
    private DeploymentsValidator deploymentsValidator;

    @Inject
    private DeploymentsManagement deploymentsManagement;

    @Inject
    private StatefulSetValidator statefulSetValidator;

    @Inject
    private StatefulSetManagement statefulSetManagement;

    @Inject
    private ServicesManagement servicesManagement;

    @Inject
    private BatchValidator validator;

    /**
     * create deployment service statefulset by json string
     * TODO validate volumes annotations and return creation results, convert deployment service to FormMixture may a good idea to deal with it
     *
     * @param namespaceName the namespace
     * @param appName       the app
     * @param jsonStr       the json string for create
     */
    public void createFromK8sJson(String namespaceName, String appName, String jsonStr) {
        ObjectMapper mapper = new ObjectMapper();
        List<Object> objects;
        try {
            objects = mapper.readValue(jsonStr, new TypeReference<List<Object>>() {
            });
        } catch (Exception e) {
            try {
                objects = new ArrayList<>();
                Object object = mapper.readValue(jsonStr, Object.class);
                objects.add(object);
            } catch (IOException e1) {
                throw new CcException(BackendReturnCodeNameConstants.PROPERTY_INVALID,
                        ImmutableMap.of("details", e.getMessage()));
            }
        }
        BatchItems batchItems = parseItems(objects);
        batchCreate(namespaceName, appName, batchItems);
    }

    /**
     * create deployment service statefulset by yaml string
     *
     * @param namespaceName the namespace
     * @param appName       the app
     * @param yamlStr       the yaml string for create
     */
    public void createFromK8sYaml(String namespaceName, String appName, String yamlStr) {
        Yaml parser = new Yaml();

        List<Object> objects;

        try {
            objects = Lists.newArrayList(parser.loadAll(yamlStr));
        } catch (YAMLException e) {
            throw new CcException(BackendReturnCodeNameConstants.PROPERTY_INVALID,
                    ImmutableMap.of("details", e.getMessage()));
        }

        BatchItems batchItems = parseItems(objects);
        logger.info("batch items parsed: {}", batchItems);
        batchCreate(namespaceName, appName, batchItems);
    }

    /**
     * parse objects to their real type
     *
     * @param objects the objects
     * @return the real type objects
     */
    private BatchItems parseItems(List<Object> objects) {
        Optional<Object> incorrectItem = objects.stream()
                .filter(Objects::nonNull)
                .filter(item -> !(item instanceof Map))
                .findAny();
        if (incorrectItem.isPresent()) {
            throw new CcException(BackendReturnCodeNameConstants.PROPERTY_INVALID,
                    ImmutableMap.of("msg", "not supported object",
                            "param", incorrectItem.get()));
        }

        ObjectMapper objectMapper = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        BatchItems batchItems = BatchItems.getOne();

        try {
            for (Object object : objects) {
                if (object != null) {
                    parse(batchItems, object, objectMapper);
                }
            }
            return batchItems;
        } catch (IOException e) {
            throw new CcException(BackendReturnCodeNameConstants.PROPERTY_INVALID,
                    ImmutableMap.of("details", e));
        }
    }

    private void parse(BatchItems batchItems, Object item, ObjectMapper objectMapper) throws IOException {
        Object kind = ((Map) item).get("kind");
        if (BatchItems.Kind.Service.name().equals(kind)) {
            byte[] serviceBytes = objectMapper.writeValueAsBytes(item);
            batchItems.getServices().add(objectMapper.readValue(serviceBytes, Service.class));
        } else if (BatchItems.Kind.Deployment.name().equals(kind)) {
            byte[] deploymentBytes = objectMapper.writeValueAsBytes(item);
            batchItems.getDeployments().add(objectMapper.readValue(deploymentBytes, Deployment.class));
        } else if (BatchItems.Kind.StatefulSet.name().equals(kind)) {
            byte[] statefulSetBytes = objectMapper.writeValueAsBytes(item);
            batchItems.getStatefulSets().add(objectMapper.readValue(statefulSetBytes, StatefulSet.class));
        } else {
            throw new CcException(BackendReturnCodeNameConstants.PROPERTY_INVALID,
                    ImmutableMap.of("msg", "not supported kind:" + kind,
                            "param", item));
        }
    }

    /**
     * batch create deployment service statefulset in k8s
     * TODO validate all at first
     *
     * @param namespaceName the namespace
     * @param appName       the app
     * @param batchItems    the items for create
     */
    public void batchCreate(String namespaceName, String appName, BatchItems batchItems) {
        List<Deployment> deployments = batchItems.getDeployments();
        List<Service> services = batchItems.getServices();
        List<StatefulSet> statefulSets = batchItems.getStatefulSets();

        validator.validateCriticalPod(namespaceName, batchItems);

        for (Deployment deployment : deployments) {
            NameUtils.validatePortName(namespaceName, appName, deployment);
            deploymentsManagement.createWithAudit(namespaceName, appName, deployment);
        }

        for (StatefulSet statefulSet : statefulSets) {
            NameUtils.validatePortName(namespaceName, appName, statefulSet);
            statefulSetManagement.createWithAudit(namespaceName, appName, statefulSet);
        }

        services.forEach(service -> servicesManagement.createWithAudit(namespaceName, appName, service));
    }
}
