package cc.backend.kubernetes.batch;

import cc.backend.kubernetes.apps.validator.AppValidator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/11/9.
 */

@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes/namespaces/{namespaceName}/apps/{appName}/batch")
@Api(value = "Batch", description = "batch operations for create deployment, service, statefulset.",
        produces = "application/json")
public class BatchEndpoint {

    @Inject
    private AppValidator appValidator;

    @Inject
    private BatchManagement management;

    @POST
    @Path("k8s_json")
    @ApiOperation(value = "Create deployment service statefulset by json.")
    public Response createFromK8sJson(@PathParam("namespaceName") String namespaceName,
                                      @PathParam("appName") String appName,
                                      String json) {
        appValidator.validateAppShouldExists(namespaceName, appName);
        management.createFromK8sJson(namespaceName, appName, json);

        return Response.ok().build();
    }

    @POST
    @Path("k8s_yaml")
    @ApiOperation(value = "Create deployment service statefulset by yaml.")
    public Response createFromK8sYaml(@PathParam("namespaceName") String namespaceName,
                                      @PathParam("appName") String appName,
                                      Object yaml) {
        appValidator.validateAppShouldExists(namespaceName, appName);
        management.createFromK8sYaml(namespaceName, appName, yaml.toString());

        return Response.ok().build();
    }
}
