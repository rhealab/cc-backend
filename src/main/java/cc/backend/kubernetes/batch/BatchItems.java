package cc.backend.kubernetes.batch;

import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;

import java.util.ArrayList;
import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/21.
 */
public class BatchItems {
    private List<Service> services;
    private List<Deployment> deployments;
    private List<StatefulSet> statefulSets;

    public static BatchItems getOne() {
        BatchItems batchItems = new BatchItems();
        batchItems.setServices(new ArrayList<>());
        batchItems.setDeployments(new ArrayList<>());
        batchItems.setStatefulSets(new ArrayList<>());
        return batchItems;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public List<Deployment> getDeployments() {
        return deployments;
    }

    public void setDeployments(List<Deployment> deployments) {
        this.deployments = deployments;
    }

    public List<StatefulSet> getStatefulSets() {
        return statefulSets;
    }

    public void setStatefulSets(List<StatefulSet> statefulSets) {
        this.statefulSets = statefulSets;
    }

    @Override
    public String toString() {
        return "BatchItems{" +
                "services=" + services +
                ", deployments=" + deployments +
                ", statefulSets=" + statefulSets +
                '}';
    }

    /**
     * @author NormanWang06@gmail.com (wangjinxin) on 2017/11/9.
     */
    public static enum Kind {
        Service,
        Deployment,
        StatefulSet
    }
}
