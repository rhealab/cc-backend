package cc.backend.kubernetes.storage;

import cc.backend.kubernetes.storage.domain.StorageWorkload;
import cc.backend.kubernetes.storage.domain.WorkloadType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/7/13.
 */
public interface StorageWorkloadRepository extends JpaRepository<StorageWorkload, Long> {

    List<StorageWorkload> findByStorageId(long storageId);

    List<StorageWorkload> findByStorageIdIn(List<Long> storageId);

    List<StorageWorkload> findByNamespaceNameAndWorkloadTypeAndWorkloadName(String namespaceName,
                                                                            WorkloadType workloadType,
                                                                            String workloadName);

    List<StorageWorkload> findByNamespaceNameAndAppName(String namespaceName, String appName);

    @Transactional
    void deleteByStorageId(long storageId);

    @Transactional
    void deleteByNamespaceName(String namespaceName);

    @Transactional
    void deleteByNamespaceNameAndWorkloadTypeAndWorkloadName(String namespaceName,
                                                             WorkloadType workloadType,
                                                             String workloadName);

    @Transactional
    void deleteByNamespaceNameAndAppName(String namespaceName, String appName);

    @Transactional
    void deleteByNamespaceNameAndWorkloadTypeAndWorkloadNameAndStorageNameIn
            (String namespaceName, WorkloadType workloadType, String workloadName, List<String> storageName);
}
