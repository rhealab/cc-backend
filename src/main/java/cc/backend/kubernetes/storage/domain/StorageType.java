package cc.backend.kubernetes.storage.domain;

/**
 * @author wangchunyang@gmail.com
 */
public enum StorageType {
    RBD, CephFS, HostPath, StorageClass, NFS, EFS, EBS
}
