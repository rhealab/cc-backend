package cc.backend.kubernetes.storage.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by xzy on 2017/3/29.
 */
public class HostPathUsage {
    @JsonProperty("NodeName")
    private String nodeName;

    @JsonProperty("MountInfos")
    private List<MountInfo> mountInfos;

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public List<MountInfo> getMountInfos() {
        return mountInfos;
    }

    public void setMountInfos(List<MountInfo> mountInfos) {
        this.mountInfos = mountInfos;
    }

    public static class MountInfo {
        @JsonProperty("HostPath")
        private String hostPath;
        @JsonProperty("VolumeQuotaSize")
        private long volumeQuotaSize;
        @JsonProperty("VolumeCurrentSize")
        private long volumeCurrentSize;
        @JsonProperty("VolumeCurrentFileNum")
        private long volumeCurrentFileNum;
        @JsonProperty("PodInfo")
        private PodInfo podInfo;

        public String getHostPath() {
            return hostPath;
        }

        public void setHostPath(String hostPath) {
            this.hostPath = hostPath;
        }

        public long getVolumeQuotaSize() {
            return volumeQuotaSize;
        }

        public void setVolumeQuotaSize(long volumeQuotaSize) {
            this.volumeQuotaSize = volumeQuotaSize;
        }

        public long getVolumeCurrentSize() {
            return volumeCurrentSize;
        }

        public void setVolumeCurrentSize(long volumeCurrentSize) {
            this.volumeCurrentSize = volumeCurrentSize;
        }

        public long getVolumeCurrentFileNum() {
            return volumeCurrentFileNum;
        }

        public void setVolumeCurrentFileNum(long volumeCurrentFileNum) {
            this.volumeCurrentFileNum = volumeCurrentFileNum;
        }

        public PodInfo getPodInfo() {
            return podInfo;
        }

        public void setPodInfo(PodInfo podInfo) {
            this.podInfo = podInfo;
        }

        public static class PodInfo {
            @JsonProperty("Info")
            private String info;

            public String getInfo() {
                return info;
            }

            public void setInfo(String info) {
                this.info = info;
            }
        }
    }
}
