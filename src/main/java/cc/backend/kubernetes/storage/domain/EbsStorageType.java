package cc.backend.kubernetes.storage.domain;

/**
 * @author wangchunyang@gmail.com
 */
public enum EbsStorageType {
    io1, gp2, sc1, st1
}
