package cc.backend.kubernetes.storage.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static cc.backend.kubernetes.Constants.RESOURCE_NAME_MAX;
import static cc.backend.kubernetes.Constants.RESOURCE_NAME_PATTERN;

/**
 * @author wangchunyang@gmail.com
 */
public class StorageCreateRequest implements StorageCapacity, StorageNaming {

    private String namespaceName;

    @NotNull
    private StorageType storageType;

    @Pattern(regexp = RESOURCE_NAME_PATTERN)
    @Size(max = RESOURCE_NAME_MAX)
    @NotNull
    private String storageName;

    @Min(1024 * 1024)
    private long amountBytes;

    private AccessModeType accessMode;

    @JsonProperty("ebsType")
    private EbsStorageType ebsStorageType;

    /**
     * only for hostpath
     * true: storage keep in the chosen node and info will not loss when pod was deleted
     * false: when pod was deleted the info will loss
     */
    private boolean persisted;

    /**
     * only for hostpath
     * true: each pod has there own storage (quota will be replicas * amountBytes)
     * false: all pod use the only one storage
     */
    private boolean unshared;

    /**
     * for nfs cephfs rbd pv read only option
     */
    private boolean readOnly;

    public StorageType getStorageType() {
        return storageType;
    }

    public void setStorageType(StorageType storageType) {
        this.storageType = storageType;
    }

    @Override
    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    @Override
    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    @Override
    public long getAmountBytes() {
        return amountBytes;
    }

    public void setAmountBytes(long amountBytes) {
        this.amountBytes = amountBytes;
    }

    public AccessModeType getAccessMode() {
        return accessMode;
    }

    public void setAccessMode(AccessModeType accessMode) {
        this.accessMode = accessMode;
    }

    public EbsStorageType getEbsStorageType() {
        return ebsStorageType;
    }

    public void setEbsStorageType(EbsStorageType ebsStorageType) {
        this.ebsStorageType = ebsStorageType;
    }

    public boolean isPersisted() {
        return persisted;
    }

    public void setPersisted(boolean persisted) {
        this.persisted = persisted;
    }

    public boolean isUnshared() {
        return unshared;
    }

    public void setUnshared(boolean unshared) {
        this.unshared = unshared;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    @Override
    public String toString() {
        return "StorageCreateRequest{" +
                "namespaceName='" + namespaceName + '\'' +
                ", storageType=" + storageType +
                ", storageName='" + storageName + '\'' +
                ", amountBytes=" + amountBytes +
                ", accessMode=" + accessMode +
                ", ebsStorageType=" + ebsStorageType +
                ", persisted=" + persisted +
                ", unshared=" + unshared +
                ", readOnly=" + readOnly +
                '}';
    }
}
