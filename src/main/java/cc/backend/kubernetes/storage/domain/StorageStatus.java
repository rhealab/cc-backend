package cc.backend.kubernetes.storage.domain;

/**
 * the storage status enum class
 *
 * @author wangjinxin
 */
public enum StorageStatus {

    /**
     * storage is creating, can not do any operation
     */
    CREATE_PENDING,

    /**
     * create storage failed, can be deleted
     */
    CREATE_FAILED,

    /**
     * storage has been created successfully, can be updated, deleted
     */
    CREATE_SUCCESS,

    /**
     * storage is updating, can not do any operation
     */
    UPDATE_PENDING,

    /**
     * update storage failed,  can be updated, deleted
     */
    UPDATE_FAILED,

    /**
     * storage has been updated successfully,  can be updated, deleted
     */
    UPDATE_SUCCESS,

    /**
     * storage is deleting, can not do any operation
     */
    DELETE_PENDING,

    /**
     * delete storage failed, can not do any operation
     */
    DELETE_FAILED,

    /**
     * storage has been deleted, can not do any operation
     */
    DELETE_SUCCESS
}