package cc.backend.kubernetes.storage.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Min;

/**
 * @author wangchunyang@gmail.com
 */
public class StorageUpdateRequest {
    private long id;

    private String name;

    private String namespaceName;

    @Min(1024 * 1024)
    private Long amountBytes;

    @JsonProperty("ebsType")
    private EbsStorageType ebsStorageType;

    /**
     * only for hostpath
     * true: storage keep in the chosen node and info will not loss when pod was deleted
     * false: when pod was deleted the info will loss
     */
    private Boolean persisted;

    /**
     * only for hostpath
     * true: each pod has there own storage (quota will be replicas * amountBytes)
     * false: all pod use the only one storage
     */
    private Boolean unshared;

    /**
     * for nfs cephfs rbd pv read only option
     */
    private Boolean readOnly;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    public Long getAmountBytes() {
        return amountBytes;
    }

    public void setAmountBytes(Long amountBytes) {
        this.amountBytes = amountBytes;
    }

    public EbsStorageType getEbsStorageType() {
        return ebsStorageType;
    }

    public void setEbsStorageType(EbsStorageType ebsStorageType) {
        this.ebsStorageType = ebsStorageType;
    }

    public Boolean isPersisted() {
        return persisted;
    }

    public void setPersisted(Boolean persisted) {
        this.persisted = persisted;
    }

    public Boolean isUnshared() {
        return unshared;
    }

    public void setUnshared(Boolean unshared) {
        this.unshared = unshared;
    }

    public Boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(Boolean readOnly) {
        this.readOnly = readOnly;
    }

    @Override
    public String toString() {
        return "StorageUpdateRequest{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", namespaceName='" + namespaceName + '\'' +
                ", amountBytes=" + amountBytes +
                ", ebsStorageType=" + ebsStorageType +
                ", persisted=" + persisted +
                ", unshared=" + unshared +
                ", readOnly=" + readOnly +
                '}';
    }
}
