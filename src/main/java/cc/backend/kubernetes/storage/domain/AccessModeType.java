package cc.backend.kubernetes.storage.domain;

/**
 * @author wangchunyang@gmail.com
 */
public enum AccessModeType {
    ReadWriteOnce, ReadWriteMany, ReadOnlyMany
}
