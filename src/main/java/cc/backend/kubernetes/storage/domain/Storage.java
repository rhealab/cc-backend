package cc.backend.kubernetes.storage.domain;

import cc.backend.audit.request.EnnContext;
import cc.backend.kubernetes.storage.StorageUtils;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author wangchunyang@gmail.com
 */
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"namespaceName", "storageName"})})
public class Storage implements StorageCapacity, StorageNaming {
    public enum Status {
        MOUNTED, UNMOUNTED,
        CREATE_PENDING, CREATE_FAILED, CREATE_SUCCESS,
        UPDATE_PENDING, UPDATE_FAILED, UPDATE_SUCCESS,
        DELETE_PENDING, DELETE_FAILED, DELETE_SUCCESS
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String namespaceName;
    private String storageName;
    @Enumerated(EnumType.STRING)
    private StorageType storageType;
    @Enumerated(EnumType.STRING)
    @NotNull
    private AccessModeType accessMode;
    private long amountBytes;

    /**
     * only for hostpath
     * true: storage keep in the chosen node and info will not loss when pod was deleted
     * false: when pod was deleted the info will loss
     */
    private boolean persisted;

    /**
     * only for hostpath
     * true: each pod has there own storage (quota will be replicas * amountBytes)
     * false: all pod use the only one storage
     */
    private boolean unshared;

    /**
     * for nfs cephfs rbd pv read only option
     */
    @Column(columnDefinition = "bit(1) default 0")
    private boolean readOnly;

    @Enumerated(EnumType.STRING)
    private Status status;

    private String pvcName;
    private String pvName;

    /**
     * only for rbd
     */
    private String imageName;

    private Date createdOn;
    private String createdBy;

    @JsonProperty("modifiedOn")
    private Date lastUpdatedOn;
    @JsonProperty("modifiedBy")
    private String lastUpdatedBy;

    /**
     * only for ebs
     */
    private EbsStorageType ebsStorageType;
    private String ebsVolumeId;

    @PreUpdate
    public void onUpdate() {
        lastUpdatedBy = EnnContext.getUserId();
        lastUpdatedOn = new Date();
    }

    @PrePersist
    public void onCreate() {
        createdBy = EnnContext.getUserId();
        createdOn = new Date();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public StorageType getStorageType() {
        return storageType;
    }

    public void setStorageType(StorageType storageType) {
        this.storageType = storageType;
    }

    @Override
    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    @Override
    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public AccessModeType getAccessMode() {
        return accessMode;
    }

    public void setAccessMode(AccessModeType accessMode) {
        this.accessMode = accessMode;
    }

    @Override
    public long getAmountBytes() {
        return amountBytes;
    }

    public void setAmountBytes(long amountBytes) {
        this.amountBytes = amountBytes;
    }

    public boolean isPersisted() {
        return persisted;
    }

    public void setPersisted(boolean persisted) {
        this.persisted = persisted;
    }

    public boolean isUnshared() {
        return unshared;
    }

    public void setUnshared(boolean unshared) {
        this.unshared = unshared;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getPvcName() {
        return pvcName;
    }

    public void setPvcName(String pvcName) {
        this.pvcName = pvcName;
    }

    public String getPvName() {
        return pvName;
    }

    public void setPvName(String pvName) {
        this.pvName = pvName;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(Date lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public EbsStorageType getEbsStorageType() {
        return ebsStorageType;
    }

    public void setEbsStorageType(EbsStorageType ebsStorageType) {
        this.ebsStorageType = ebsStorageType;
    }

    public String getEbsVolumeId() {
        return ebsVolumeId;
    }

    public void setEbsVolumeId(String ebsVolumeId) {
        this.ebsVolumeId = ebsVolumeId;
    }

    @Override
    public String toString() {
        return "Storage{" +
                "id=" + id +
                ", namespaceName='" + namespaceName + '\'' +
                ", storageName='" + storageName + '\'' +
                ", storageType=" + storageType +
                ", accessMode=" + accessMode +
                ", amountBytes=" + amountBytes +
                ", persisted=" + persisted +
                ", unshared=" + unshared +
                ", readOnly=" + readOnly +
                ", status=" + status +
                ", pvcName='" + pvcName + '\'' +
                ", pvName='" + pvName + '\'' +
                ", imageName='" + imageName + '\'' +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                ", lastUpdatedOn=" + lastUpdatedOn +
                ", lastUpdatedBy='" + lastUpdatedBy + '\'' +
                '}';
    }

    public static Storage newInstance(StorageCreateRequest request) {
        Storage storage = new Storage();
        storage.setNamespaceName(request.getNamespaceName());
        storage.setStorageType(request.getStorageType());
        storage.setStorageName(request.getStorageName());
        storage.setAccessMode(StorageUtils.composeAccessMode(request.getAccessMode(), request.getStorageType()));
        storage.setAmountBytes(request.getAmountBytes());
        storage.setPersisted(request.isPersisted());
        storage.setUnshared(request.isUnshared());
        storage.setReadOnly(request.isReadOnly());
        storage.setEbsStorageType(request.getEbsStorageType());
        storage.setStatus(Status.CREATE_PENDING);
        storage.setPvcName(StorageUtils.pvcName(request.getNamespaceName(), request.getStorageName()));
        storage.setPvName(StorageUtils.pvName(request.getNamespaceName(), request.getStorageName()));
        storage.setImageName(StorageUtils.cephImageName(storage.getStorageName()));
        return storage;
    }
}
