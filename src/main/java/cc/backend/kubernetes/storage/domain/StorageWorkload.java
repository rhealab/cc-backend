package cc.backend.kubernetes.storage.domain;

import javax.persistence.*;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/7/13.
 */
@Entity
public class StorageWorkload {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private long storageId;

    private String storageName;

    private String namespaceName;

    @Enumerated(value = EnumType.STRING)
    private WorkloadType workloadType;

    private String workloadName;

    private String appName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getStorageId() {
        return storageId;
    }

    public void setStorageId(long storageId) {
        this.storageId = storageId;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    public WorkloadType getWorkloadType() {
        return workloadType;
    }

    public void setWorkloadType(WorkloadType workloadType) {
        this.workloadType = workloadType;
    }

    public String getWorkloadName() {
        return workloadName;
    }

    public void setWorkloadName(String workloadName) {
        this.workloadName = workloadName;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    @Override
    public String toString() {
        return "StorageWorkload{" +
                "id=" + id +
                ", storageId=" + storageId +
                ", storageName='" + storageName + '\'' +
                ", namespaceName='" + namespaceName + '\'' +
                ", workloadName='" + workloadName + '\'' +
                ", workloadType=" + workloadType +
                ", appName='" + appName + '\'' +
                '}';
    }
}
