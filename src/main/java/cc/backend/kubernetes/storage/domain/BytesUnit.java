package cc.backend.kubernetes.storage.domain;

/**
 * @author wangchunyang@gmail.com
 */
public enum BytesUnit {
    Ki(1024),
    K (1000),
    Mi(1024 * 1024),
    M (1000 * 1000),
    Gi(1024L * 1024L * 1024L),
    G (1000L * 1000L * 1000L),
    Ti(1024L * 1024L * 1024L * 1024L),
    T (1000L * 1000L * 1000L * 1000L),
    Pi(1024L * 1024L * 1024L * 1024L * 1024L),
    P (1000L * 1000L * 1000L * 1000L * 1000L),
    Ei(1024L * 1024L * 1024L * 1024L * 1024L * 1024L),
    E (1000L * 1000L * 1000L * 1000L * 1000L * 1000L);

    long bytes;

    BytesUnit(long bytes) {
        this.bytes = bytes;
    }

    public long getBytes() {
        return bytes;
    }

    public static String getAppropriateUnit(long bytes) {

        if (bytes >= Pi.getBytes()) {
            return String.format("%.1fPi", 1.0 * bytes / Pi.getBytes());
        }

        if (bytes >= Ti.getBytes()) {
            return String.format("%.1fTi", 1.0 * bytes / Ti.getBytes());
        }

        if (bytes >= Gi.getBytes()) {
            return String.format("%.1fGi", 1.0 * bytes / Gi.getBytes());
        }

        if (bytes >= Mi.getBytes()) {
            return String.format("%.1fMi", 1.0 * bytes / Mi.getBytes());
        }

        return "0Ki";
    }
}