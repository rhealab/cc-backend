package cc.backend.kubernetes.storage.domain;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/7/13.
 */
public enum WorkloadType {
    Deployment, StatefulSet
}
