package cc.backend.kubernetes.storage.domain;

import cc.backend.kubernetes.utils.UnitUtils;
import io.fabric8.kubernetes.api.model.Quantity;

import java.util.Map;

import static com.google.common.collect.ImmutableMap.of;

/**
 * @author wangchunyang@gmail.com
 */
public interface StorageCapacity {
    long getAmountBytes();

    default Map<String, Quantity> capacity() {
        return capacity(getAmountBytes());
    }

    static Map<String, Quantity> capacity(long bytes) {
        return of("storage", UnitUtils.toK8sMemoryQuantity(bytes));
    }
}
