package cc.backend.kubernetes.storage.domain;

import cc.backend.kubernetes.storage.StorageUtils;

import java.util.Map;

import static cc.backend.kubernetes.Constants.PV_NAMESPACE_LABEL;
import static com.google.common.collect.ImmutableMap.of;

/**
 * @author wangchunyang@gmail.com
 */
public interface StorageNaming {

    /**
     * get the storage's namespace name
     *
     * @return the storage's namespace name
     */
    String getNamespaceName();

    /**
     * get the storage name
     *
     * @return the storage name
     */
    String getStorageName();

    /**
     * parse the pvc name to storage name
     * <p>
     * previously the storage to pvc mapping rule is
     * -------------------------------------
     * |  Storage name |     PVC name      |
     * -------------------------------------
     * |      s1       |  namespace-s1-pvc |
     * -------------------------------------
     * currently we use a mapping like this:
     * -------------------------------------
     * |  Storage name |     PVC name      |
     * -------------------------------------
     * |      s1       |         s1        |
     * -------------------------------------
     *
     * @param namespaceName the namespace name
     * @param pvcName       the pvc name for parse
     * @return the storage name parsed
     */
    static String parseStorageName(String namespaceName, String pvcName) {
        return pvcName;
    }

    default String pvHostPath() {
        return StorageUtils.pvHostPath(getNamespaceName(), getStorageName());
    }

    default String cephImageName() {
        return StorageUtils.cephImageName(getStorageName());
    }

    default Map<String, String> labels() {
        return of(PV_NAMESPACE_LABEL, getNamespaceName(), "storageName", getStorageName());
    }

    default String cephFsDir() {
        return StorageUtils.cephFsDir(getNamespaceName(), getStorageName());
    }

    default String nfsDir() {
        return StorageUtils.nfsDir(getNamespaceName(), getStorageName());
    }
}
