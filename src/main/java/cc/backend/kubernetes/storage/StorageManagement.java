package cc.backend.kubernetes.storage;

import cc.backend.EnnProperties;
import cc.backend.audit.operation.OperationAuditMessage;
import cc.backend.audit.operation.OperationAuditMessageProducer;
import cc.backend.audit.operation.OperationType;
import cc.backend.audit.operation.ResourceType;
import cc.backend.audit.request.EnnContext;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceRepository;
import cc.backend.event.data.EventSourceType;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.storage.checker.StorageCreationStatusChecker;
import cc.backend.kubernetes.storage.checker.StorageDeletionStatusChecker;
import cc.backend.kubernetes.storage.checker.StorageUpdateStatusChecker;
import cc.backend.kubernetes.storage.domain.*;
import cc.backend.kubernetes.storage.usage.entity.CephImageUsageRepository;
import cc.backend.kubernetes.storage.usage.entity.CephfsDirectoryUsageRepository;
import cc.backend.kubernetes.storage.usage.entity.NfsDirectoryUsageRepository;
import com.google.common.base.Stopwatch;
import io.fabric8.kubernetes.api.model.*;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static cc.backend.kubernetes.Constants.POD_PHASE_RUNNING;
import static cc.backend.kubernetes.storage.domain.StorageType.*;

/**
 * Maintained by both jx and cy.
 *
 * @author wjx
 * @author wangchunyang@gmail.com
 */
@Component
public class StorageManagement {
    private static final Logger logger = LoggerFactory.getLogger(StorageManagement.class);

    @Inject
    private StorageRepository storageRepository;

    @Inject
    private CephfsDirectoryUsageRepository cephfsDirectoryUsageRepository;

    @Inject
    private NfsDirectoryUsageRepository nfsDirectoryUsageRepository;

    @Inject
    private CephImageUsageRepository cephImageUsageRepository;

    @Inject
    private StorageValidator validator;

    @Inject
    private StorageWorkloadRepository storageWorkloadRepository;

    @Inject
    private KubernetesClientManager clientManager;

    @Inject
    private OperationAuditMessageProducer operationAuditMessageProducer;

    @Inject
    private EventSourceRepository eventSourceRepository;

    @Inject
    private StorageCreationStatusChecker storageCreationStatusChecker;

    @Inject
    private StorageDeletionStatusChecker storageDeletionStatusChecker;

    @Inject
    private StorageUpdateStatusChecker storageUpdateStatusChecker;

    @Inject
    private CephfsStorageManager cephfsStorageManager;

    @Inject
    private CephRbdStorageManager cephRbdStorageManager;

    @Inject
    private HostPathStorageManager hostPathStorageManager;

    @Inject
    private NfsStorageManager nfsStorageManager;

    @Inject
    private EbsStorageManager ebsStorageManager;

    @Inject
    private EnnProperties properties;


    public List<Storage> getStorageList(String namespaceName) {
        return storageRepository.findByNamespaceName(namespaceName);
    }

    public List<Storage> getStorageListByApp(String namespaceName, String appName) {
        List<StorageWorkload> storageWorkloadList = storageWorkloadRepository.
                findByNamespaceNameAndAppName(namespaceName, appName);
        return getStorageListByStorageWorkload(storageWorkloadList);
    }

    public List<Storage> getStorageListByStorageWorkload(List<StorageWorkload> storageWorkloadList) {
        List<Long> storageIdList = Optional.ofNullable(storageWorkloadList)
                .orElseGet(ArrayList::new)
                .stream()
                .map(StorageWorkload::getStorageId)
                .collect(Collectors.toList());
        return storageRepository.findAll(storageIdList);
    }

    public List<Storage> getStorageListByDeploymentName(String namespaceName, String deploymentName) {
        List<StorageWorkload> storageWorkloadList = storageWorkloadRepository.
                findByNamespaceNameAndWorkloadTypeAndWorkloadName(namespaceName, WorkloadType.Deployment, deploymentName);
        return getStorageListByStorageWorkload(storageWorkloadList);
    }

    public List<Storage> getStorageListByDeployment(String namespaceName, Deployment deployment) {
        PodTemplateSpec template = deployment.getSpec().getTemplate();

        List<String> pvcNames = Optional.ofNullable(template)
                .map(PodTemplateSpec::getSpec)
                .map(PodSpec::getVolumes)
                .orElseGet(ArrayList::new)
                .stream()
                .map(Volume::getPersistentVolumeClaim)
                .filter(Objects::nonNull)
                .map(PersistentVolumeClaimVolumeSource::getClaimName)
                .collect(Collectors.toList());

        if (pvcNames.isEmpty()) {
            return new ArrayList<>();
        }
        return storageRepository.findByNamespaceNameAndPvcNameIn(namespaceName, pvcNames);
    }

    public Storage getStorage(String namespaceName, long storageId) {
        return validator.ifPresent(namespaceName, storageId);
    }

    public List<Storage> getStorageList(String namespaceName, List<Long> storageIdList) {
        if (!storageIdList.isEmpty()) {
            return storageRepository.findByNamespaceNameAndIdIn(namespaceName, storageIdList);
        }
        return new ArrayList<>();
    }

    public Storage getStorageByName(String namespaceName, String storageName) {
        return validator.ifPresent(namespaceName, storageName);
    }

    public boolean isStorageFileOperable(Storage storage, String podName) {
        // check status
        Storage.Status status = storage.getStatus();
        if (status != Storage.Status.CREATE_SUCCESS
                && status != Storage.Status.UPDATE_PENDING
                && status != Storage.Status.UPDATE_SUCCESS
                && status != Storage.Status.UPDATE_FAILED) {
            return false;
        }

        // check storage type
        StorageType storageType = storage.getStorageType();
        if (storageType == CephFS || storageType == NFS || storageType == EFS) {
            return true;
        } else if (storageType == RBD || storageType == StorageClass) {
            return isRbdStorageFileOperable(storage);
        } else if (storageType == HostPath) {
            if (StringUtils.isEmpty(podName)) {
                return isHostPathStorageFileOperable(storage);
            }
            return isHostPathStorageFileOperable(storage, podName);
        }

        return false;
    }

    private boolean isRbdStorageFileOperable(Storage storage) {
        List<StorageWorkload> relatedWorkload = storageWorkloadRepository.findByStorageId(storage.getId());
        return relatedWorkload.size() <= 0;
    }

    public boolean isHostPathStorageFileOperable(Storage storage, String podName) {
        Pod pod = clientManager.getClient()
                .pods()
                .inNamespace(storage.getNamespaceName())
                .withName(podName)
                .get();
        String podPhase = Optional.ofNullable(pod)
                .map(Pod::getStatus)
                .map(PodStatus::getPhase)
                .orElse("");
        return POD_PHASE_RUNNING.equals(podPhase)
                && isPodMountedStorage(storage, pod);
    }

    private boolean isHostPathStorageFileOperable(Storage storage) {
        List<StorageWorkload> relatedWorkload = storageWorkloadRepository.findByStorageId(storage.getId());
        return relatedWorkload.size() > 0;
    }

    private boolean isPodMountedStorage(Storage storage, Pod pod) {
        String pvcName = storage.getPvcName();
        List<String> volumeNameList = Optional.ofNullable(pod)
                .map(Pod::getSpec)
                .map(PodSpec::getVolumes)
                .orElseGet(ArrayList::new)
                .stream()
                .filter(volume -> volume.getPersistentVolumeClaim() != null)
                .filter(volume -> pvcName.equals(volume.getPersistentVolumeClaim().getClaimName()))
                .map(Volume::getName)
                .collect(Collectors.toList());

        return volumeNameList.size() != 0
                && Optional.ofNullable(pod)
                .map(Pod::getSpec)
                .map(PodSpec::getContainers)
                .orElseGet(ArrayList::new)
                .stream()
                .map(Container::getVolumeMounts)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .filter(v -> !StringUtils.isEmpty(v.getMountPath()))
                .anyMatch(v -> volumeNameList.contains(v.getName()));
    }

    public String getCapacityUpdateState(long id, String namespaceName) {
        Storage storage = validator.ifPresent(namespaceName, id);
        if (HostPath == storage.getStorageType()) {
            String state = hostPathStorageManager.getUpdateCapacityState(storage.getPvcName());
            if (!StringUtils.isEmpty(state) || state.startsWith("ok")) {
                return "ok";
            } else {
                if (state.startsWith("err")) {
                    return "error";
                } else if (state.startsWith("pending")) {
                    return "pending";
                }
            }
        } else if (RBD == storage.getStorageType()) {
            return "ok";
        }

        return "error";
    }

    @Transactional(rollbackFor = Exception.class)
    public EventSource<Storage> initStorageForCreate(Storage storage) {
        logger.info("initialize storage for create - {}.", storage.getStorageName());
        String namespaceName = storage.getNamespaceName();

        storage.setStatus(Storage.Status.CREATE_PENDING);
        Storage savedStorage = storageRepository.save(storage);

        EventSource<Storage> eventSource = new EventSource.Builder<Storage>()
                .namespaceName(namespaceName)
                .sourceType(EventSourceType.STORAGE_CREATE)
                .sourceName(storage.getStorageName())
                .payload(savedStorage)
                .build();
        EventSource<Storage> savedEventSource = eventSourceRepository.save(eventSource);

        storageCreationStatusChecker.start(EnnContext.getClusterName(), namespaceName, eventSource.getPayload());
        return savedEventSource;
    }

    public void createStorage(EventSource<Storage> eventSource) {
        Storage storage = eventSource.getPayload();
        logger.info("Starting to create storage - {}", storage);

        StorageType storageType = storage.getStorageType();
        if (RBD == storageType) {
            cephRbdStorageManager.createStorage(eventSource);
        } else if (CephFS == storageType) {
            cephfsStorageManager.createStorage(eventSource);
        } else if (NFS == storageType || EFS == storageType) {
            nfsStorageManager.createStorage(eventSource);
        } else if (HostPath == storageType) {
            hostPathStorageManager.createStorage(eventSource);
        } else if (EBS == storageType) {
            ebsStorageManager.createStorage(eventSource);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public EventSource<StorageUpdateRequest> initStorageForUpdate(Storage storage, StorageUpdateRequest request) {
        logger.info("initialize storage for update - {}.", storage.getStorageName());
        String namespaceName = storage.getNamespaceName();
        EventSource<StorageUpdateRequest> eventSource = new EventSource.Builder<StorageUpdateRequest>()
                .namespaceName(namespaceName)
                .sourceType(EventSourceType.STORAGE_UPDATE)
                .sourceName(storage.getStorageName())
                .payload(request)
                .build();
        EventSource<StorageUpdateRequest> savedEventSource = eventSourceRepository.save(eventSource);
        storageRepository.updateStatus(namespaceName, storage.getStorageName(), Storage.Status.UPDATE_PENDING);
        storageUpdateStatusChecker.start(EnnContext.getClusterName(), namespaceName, storage);
        return savedEventSource;
    }

    /**
     * update storage
     * ceph fs and nfs no need to change size
     *
     * @param eventSource event source for update
     * @param storage     the origin storage
     */
    public void updateStorage(EventSource<StorageUpdateRequest> eventSource, Storage storage) {
        switch (storage.getStorageType()) {
            case RBD:
                cephRbdStorageManager.updateStorage(eventSource, storage);
                break;
            case CephFS:
                cephfsStorageManager.updateStorage(eventSource, storage);
                break;
            case NFS:
            case EFS:
                nfsStorageManager.updateStorage(eventSource, storage);
                break;
            case HostPath:
                hostPathStorageManager.updateStorage(eventSource, storage);
                break;
            case EBS:
                ebsStorageManager.updateStorage(eventSource, storage);
                break;
            default:
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public EventSource<Storage> initStorageForDelete(Storage storage) {
        logger.info("initialize storage for delete - {}.", storage.getStorageName());
        String namespaceName = storage.getNamespaceName();
        EventSource<Storage> eventSource = new EventSource.Builder<Storage>()
                .namespaceName(namespaceName)
                .sourceType(EventSourceType.STORAGE_DELETE)
                .sourceName(storage.getStorageName())
                .payload(storage)
                .build();
        EventSource<Storage> savedEventSource = eventSourceRepository.save(eventSource);
        storageRepository.updateStatus(namespaceName, storage.getStorageName(), Storage.Status.DELETE_PENDING);
        storageDeletionStatusChecker.start(EnnContext.getClusterName(), namespaceName, storage);
        return savedEventSource;
    }

    /**
     * delete storage
     *
     * @param eventSource the storage delete event source
     */
    public void deleteStorage(EventSource<Storage> eventSource) {
        switch (eventSource.getPayload().getStorageType()) {
            case RBD:
                cephRbdStorageManager.deleteStorage(eventSource);
                break;
            case CephFS:
                cephfsStorageManager.deleteStorage(eventSource);
                break;
            case NFS:
            case EFS:
                nfsStorageManager.deleteStorage(eventSource);
                break;
            case HostPath:
                hostPathStorageManager.deleteStorage(eventSource);
                break;
            case EBS:
                ebsStorageManager.deleteStorage(eventSource);
                break;
            default:
        }
    }

    /**
     * delete storage created by storage class
     * <p>
     * StorageClass type storage's pvc pv and ceph image is created by k8s storage class,
     * storage is created by listener, have no event or even source,
     * just delete pvc directly k8s will delete pv and image
     *
     * @param storage the storage to delete
     */
    public void deleteStorageClass(Storage storage) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        logger.info("Delete StorageClass PVC - pvcName={}, namespace={}", storage.getPvcName(), storage.getNamespaceName());

        clientManager.getClient()
                .persistentVolumeClaims()
                .inNamespace(storage.getNamespaceName())
                .withName(storage.getPvcName())
                .delete();

        storageRepository.delete(storage);
        storageWorkloadRepository.deleteByStorageId(storage.getId());

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage.Builder builder = new OperationAuditMessage.Builder();
        builder.elapsed(elapsed)
                .namespaceName(storage.getNamespaceName())
                .operationType(OperationType.DELETE)
                .resourceType(ResourceType.STORAGE_STORAGE_CLASS_PVC)
                .resourceName(storage.getPvcName());

        operationAuditMessageProducer.send(builder.build());
    }

    public void deleteStorageByNamespace(String namespaceName) {
        List<Storage> storageList = storageRepository.findByNamespaceName(namespaceName);
        //TODO should delete by label
        for (Storage storage : storageList) {
            deletePv(storage);
        }

        if (properties.getCurrentEbs().isEnabled()) {
            ebsStorageManager.deleteEbsBatch(storageList);
        }
        storageRepository.deleteByNamespaceName(namespaceName);
        cephImageUsageRepository.deleteByNamespaceName(namespaceName);
        cephfsDirectoryUsageRepository.deleteByNamespaceName(namespaceName);
        nfsDirectoryUsageRepository.deleteByNamespaceName(namespaceName);
        storageWorkloadRepository.deleteByNamespaceName(namespaceName);
    }

    private void deletePv(Storage storage) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        String namespaceName = storage.getNamespaceName();
        String pvName = storage.getPvName();
        logger.info("Delete PV - pvName={}", pvName);
        clientManager.getClient().persistentVolumes().withName(pvName).delete();

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage.Builder builder = new OperationAuditMessage.Builder();
        builder.elapsed(elapsed)
                .namespaceName(namespaceName)
                .operationType(OperationType.DELETE)
                .resourceType(ResourceType.STORAGE_PV)
                .resourceName(pvName);

        operationAuditMessageProducer.send(builder.build());
    }

    public List<StorageDto> composeStorageDtoList(String namespaceName, List<Storage> storageList) {
        Map<String, List<String>> pvcPodsMap = getPvcPodsMap(namespaceName);
        return Optional.ofNullable(storageList)
                .orElseGet(ArrayList::new)
                .stream()
                .map(storage -> composeStorageDto(storage, pvcPodsMap))
                .collect(Collectors.toList());
    }

    public Map<String, List<String>> getPvcPodsMap(String namespaceName) {
        List<Pod> items = clientManager.getClient()
                .pods()
                .inNamespace(namespaceName)
                .list()
                .getItems();

        return Optional.ofNullable(items)
                .orElseGet(ArrayList::new)
                .stream()
                .filter(pod -> pod.getSpec().getVolumes() != null)
                .map(this::getPvcMap)
                .flatMap(map -> map.entrySet().stream())
                .collect(Collectors.groupingBy(Map.Entry::getKey,
                        Collectors.mapping(Map.Entry::getValue, Collectors.toList())));
    }

    private Map<String, String> getPvcMap(Pod pod) {
        String name = pod.getMetadata().getName();
        List<Volume> volumes = pod.getSpec().getVolumes();
        return volumes
                .stream()
                .map(Volume::getPersistentVolumeClaim)
                .filter(Objects::nonNull)
                .map(PersistentVolumeClaimVolumeSource::getClaimName)
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(pvc -> pvc, pvc -> name));
    }

    public StorageDto composeStorageDto(Storage storage, Map<String, List<String>> pvcPodsMap) {
        List<StorageWorkload> storageWorkloadList = storageWorkloadRepository.findByStorageId(storage.getId());

        StorageDto.Builder builder = new StorageDto.Builder()
                .id(storage.getId())
                .namespaceName(storage.getNamespaceName())
                .storageName(storage.getStorageName())
                .storageType(storage.getStorageType())
                .accessMode(storage.getAccessMode())
                .amountBytes(storage.getAmountBytes())
                .unshared(storage.isUnshared())
                .persisted(storage.isPersisted())
                .readOnly(storage.isReadOnly())
                .relatedWorkloadList(storageWorkloadList)
                .status(storage.getStatus())
                .createdBy(storage.getCreatedBy())
                .createdOn(storage.getCreatedOn())
                .modifiedBy(storage.getLastUpdatedBy())
                .modifiedOn(storage.getLastUpdatedOn())
                .pods(pvcPodsMap.get(storage.getPvcName()))
                .ebsStorageType(storage.getEbsStorageType());
        if (storage.getStorageType() == HostPath) {
            builder.hostPathUsedBytesList(hostPathStorageManager.getHostpathPvUsedBytesList(storage.getPvName()));
        } else {
            builder.usedAmountBytes(validator.getStorageUsedBytes(storage));
        }

        return builder.build();
    }

    public void deleteStorage(String namespaceName, long storageId) {
        Storage storage = getStorage(namespaceName, storageId);
        validator.validateForDelete(storage);
        if (storage.getStorageType() == StorageType.StorageClass) {
            deleteStorageClass(storage);
        } else {
            EventSource<Storage> eventSource = initStorageForDelete(storage);
            deleteStorage(eventSource);
        }
    }
}
