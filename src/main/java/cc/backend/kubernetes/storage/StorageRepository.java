package cc.backend.kubernetes.storage;

import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author wangchunyang@gmail.com
 */
public interface StorageRepository extends JpaRepository<Storage, Long> {
    /**
     * find a storage in namespace by storage name
     *
     * @param namespaceName the namespace name
     * @param storageName   the storage name
     * @return the storage
     */
    Storage findByNamespaceNameAndStorageName(String namespaceName,
                                              String storageName);

    /**
     * find some storage in namespace by storage name list
     *
     * @param namespaceName   the namespace name
     * @param storageNameList the storage name list
     * @return the storage list
     */
    List<Storage> findByNamespaceNameAndStorageNameIn(String namespaceName,
                                                      List<String> storageNameList);

    /**
     * find a storage in namespace by pvc name
     *
     * @param namespaceName the namespace name
     * @param pvcName       the pvc name
     * @return the storage
     */
    Storage findByNamespaceNameAndPvcName(String namespaceName, String pvcName);

    /**
     * find a list storage in namespace by pvc name list
     *
     * @param namespaceName the namespace name
     * @param pvcNameList   the pvc name list
     * @return the storage list
     */
    List<Storage> findByNamespaceNameAndPvcNameIn(String namespaceName,
                                                  List<String> pvcNameList);

    /**
     * find a list storage in namespace by some id
     *
     * @param namespaceName the namespace name
     * @param idList        the id list
     * @return the storage list
     */
    List<Storage> findByNamespaceNameAndIdIn(String namespaceName,
                                             List<Long> idList);

    /**
     * find a namespace's all storage
     *
     * @param namespaceName the namespace name
     * @return the storage list
     */
    List<Storage> findByNamespaceName(String namespaceName);

    /**
     * find a namespace's specified type storage
     *
     * @param namespaceName the namespace name
     * @param type          the storage types
     * @return the storage list
     */
    List<Storage> findByNamespaceNameAndStorageType(String namespaceName,
                                                    StorageType type);

    /**
     * find a namespace's specified type storage by pvc name list
     *
     * @param namespaceName the namespace name
     * @param type          the storage types
     * @param pvcNameList   the pvc name list
     * @return the storage list
     */
    List<Storage> findByNamespaceNameAndStorageTypeAndPvcNameIn(String namespaceName,
                                                                StorageType type,
                                                                List<String> pvcNameList);

    /**
     * get a storage type total bytes
     *
     * @param storageType the type string
     * @return the total bytes (may be null, if type is not exists)
     */
    @Query(value = "SELECT sum(amount_bytes) FROM storage WHERE storage_type=:storageType", nativeQuery = true)
    Long sumByStorageType(@Param("storageType") String storageType);

    /**
     * delete a namespace's all storage
     *
     * @param namespaceName the namespace name
     */
    @Transactional(rollbackFor = Exception.class)
    void deleteByNamespaceName(String namespaceName);

    /**
     * update a storage's status
     *
     * @param namespaceName the namespace name
     * @param storageName   the storage name
     * @param status        the status want to update to
     */
    @Modifying(clearAutomatically = true)
    @Query("UPDATE Storage a SET a.status=:status " +
            "WHERE a.namespaceName=:namespaceName AND a.storageName=:storageName")
    @Transactional(rollbackFor = Exception.class)
    void updateStatus(@Param("namespaceName") String namespaceName,
                      @Param("storageName") String storageName,
                      @Param("status") Storage.Status status);
}
