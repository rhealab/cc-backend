package cc.backend.kubernetes.storage;

import cc.backend.kubernetes.storage.domain.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author wangchunyang@gmail.com
 */
public class StorageDto implements StorageCapacity, StorageNaming {
    private long id;

    private String namespaceName;
    private String storageName;
    private StorageType storageType;
    private AccessModeType accessMode;
    private long amountBytes;

    /**
     * only for hostpath
     * true: storage keep in the chosen node and info will not loss when pod was deleted
     * false: when pod was deleted the info will loss
     */
    private boolean persisted;

    /**
     * only for hostpath
     * true: each pod has there own storage (quota will be replicas * amountBytes)
     * false: all pod use the only one storage
     */
    private boolean unshared;

    /**
     * for nfs cephfs rbd pv read only option
     */
    private boolean readOnly;

    private Storage.Status status;

    @Deprecated
    private String deploymentName = "";
    @Deprecated
    private String appName = "";

    private long usedAmountBytes;
    private Date createdOn;
    private String createdBy;
    private Date modifiedOn;
    private String modifiedBy;

    private List<Long> hostPathUsedBytesList;

    private List<StorageWorkload> relatedWorkloadList;

    private List<String> pods;

    private boolean mounted;

    @JsonProperty("ebsType")
    private EbsStorageType ebsStorageType;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    @Override
    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public StorageType getStorageType() {
        return storageType;
    }

    public void setStorageType(StorageType storageType) {
        this.storageType = storageType;
    }

    public AccessModeType getAccessMode() {
        return accessMode;
    }

    public void setAccessMode(AccessModeType accessMode) {
        this.accessMode = accessMode;
    }

    @Override
    public long getAmountBytes() {
        return amountBytes;
    }

    public void setAmountBytes(long amountBytes) {
        this.amountBytes = amountBytes;
    }

    public boolean isPersisted() {
        return persisted;
    }

    public void setPersisted(boolean persisted) {
        this.persisted = persisted;
    }

    public boolean isUnshared() {
        return unshared;
    }

    public void setUnshared(boolean unshared) {
        this.unshared = unshared;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public Storage.Status getStatus() {
        return status;
    }

    public void setStatus(Storage.Status status) {
        this.status = status;
    }

    public String getDeploymentName() {
        return deploymentName;
    }

    public void setDeploymentName(String deploymentName) {
        this.deploymentName = deploymentName;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public long getUsedAmountBytes() {
        return usedAmountBytes;
    }

    public void setUsedAmountBytes(long usedAmountBytes) {
        this.usedAmountBytes = usedAmountBytes;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public List<Long> getHostPathUsedBytesList() {
        return hostPathUsedBytesList;
    }

    public void setHostPathUsedBytesList(List<Long> hostPathUsedBytesList) {
        this.hostPathUsedBytesList = hostPathUsedBytesList;
    }

    public List<StorageWorkload> getRelatedWorkloadList() {
        return relatedWorkloadList;
    }

    public void setRelatedWorkloadList(List<StorageWorkload> relatedWorkloadList) {
        this.relatedWorkloadList = relatedWorkloadList;
    }

    public List<String> getPods() {
        return pods;
    }

    public void setPods(List<String> pods) {
        this.pods = pods;
    }

    public boolean isMounted() {
        return mounted;
    }

    public void setMounted(boolean mounted) {
        this.mounted = mounted;
    }

    public EbsStorageType getEbsStorageType() {
        return ebsStorageType;
    }

    public void setEbsStorageType(EbsStorageType ebsStorageType) {
        this.ebsStorageType = ebsStorageType;
    }

    @Override
    public String toString() {
        return "StorageDto{" +
                "id=" + id +
                ", namespaceName='" + namespaceName + '\'' +
                ", storageName='" + storageName + '\'' +
                ", storageType=" + storageType +
                ", accessMode=" + accessMode +
                ", amountBytes=" + amountBytes +
                ", persisted=" + persisted +
                ", unshared=" + unshared +
                ", readOnly=" + readOnly +
                ", status=" + status +
                ", deploymentName='" + deploymentName + '\'' +
                ", appName='" + appName + '\'' +
                ", usedAmountBytes=" + usedAmountBytes +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                ", modifiedOn=" + modifiedOn +
                ", modifiedBy='" + modifiedBy + '\'' +
                ", hostPathUsedBytesList=" + hostPathUsedBytesList +
                ", relatedWorkloadList=" + relatedWorkloadList +
                ", mounted=" + mounted +
                '}';
    }


    public static class Builder {
        private long id;

        private String namespaceName;
        private String storageName;
        private StorageType storageType;
        private AccessModeType accessMode;
        private long amountBytes;

        private boolean persisted;
        private boolean unshared;
        private boolean readOnly;

        private long usedAmountBytes;
        private Date createdOn;
        private String createdBy;
        private Date modifiedOn;
        private String modifiedBy;

        private List<Long> hostPathUsedBytesList;
        private List<StorageWorkload> relatedWorkloadList;
        private List<String> pods;
        private Storage.Status status;
        private EbsStorageType ebsStorageType;

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder namespaceName(String namespaceName) {
            this.namespaceName = namespaceName;
            return this;
        }

        public Builder storageName(String storageName) {
            this.storageName = storageName;
            return this;
        }

        public Builder storageType(StorageType storageType) {
            this.storageType = storageType;
            return this;
        }

        public Builder accessMode(AccessModeType accessMode) {
            this.accessMode = accessMode;
            return this;
        }

        public Builder amountBytes(long amountBytes) {
            this.amountBytes = amountBytes;
            return this;
        }

        public Builder persisted(boolean persisted) {
            this.persisted = persisted;
            return this;
        }

        public Builder unshared(boolean unshared) {
            this.unshared = unshared;
            return this;
        }

        public Builder readOnly(boolean readOnly) {
            this.readOnly = readOnly;
            return this;
        }

        public Builder usedAmountBytes(long usedAmountBytes) {
            this.usedAmountBytes = usedAmountBytes;
            return this;
        }

        public Builder hostPathUsedBytesList(List<Long> hostPathUsedBytesList) {
            this.hostPathUsedBytesList = hostPathUsedBytesList;
            return this;
        }

        public Builder createdBy(String createdBy) {
            this.createdBy = createdBy;
            return this;
        }

        public Builder createdOn(Date createdOn) {
            this.createdOn = createdOn;
            return this;
        }

        public Builder modifiedBy(String modifiedBy) {
            this.modifiedBy = modifiedBy;
            return this;
        }

        public Builder modifiedOn(Date modifiedOn) {
            this.modifiedOn = modifiedOn;
            return this;
        }

        public Builder relatedWorkloadList(List<StorageWorkload> relatedWorkloadList) {
            this.relatedWorkloadList = relatedWorkloadList;
            return this;
        }

        public Builder pods(List<String> pods) {
            this.pods = pods;
            return this;
        }

        public Builder status(Storage.Status status) {
            this.status = status;
            return this;
        }

        public Builder ebsStorageType(EbsStorageType ebsStorageType) {
            this.ebsStorageType = ebsStorageType;
            return this;
        }

        public StorageDto build() {
            StorageDto dto = new StorageDto();

            dto.setId(id);
            dto.setNamespaceName(namespaceName);
            dto.setStorageName(storageName);
            dto.setStorageType(storageType);
            dto.setAccessMode(accessMode);
            dto.setAmountBytes(amountBytes);
            dto.setPersisted(persisted);
            dto.setUnshared(unshared);
            dto.setMounted(!CollectionUtils.isEmpty(relatedWorkloadList));
            dto.setReadOnly(readOnly);

            if (relatedWorkloadList != null) {
                dto.setDeploymentName(relatedWorkloadList
                        .stream()
                        .map(StorageWorkload::getWorkloadName)
                        .collect(Collectors.joining("/")));

                dto.setAppName(relatedWorkloadList
                        .stream()
                        .map(StorageWorkload::getAppName)
                        .collect(Collectors.joining("/")));
            }

            dto.setStatus(status);
            dto.setUsedAmountBytes(usedAmountBytes);
            if (hostPathUsedBytesList != null) {
                dto.setHostPathUsedBytesList(hostPathUsedBytesList);
                dto.setUsedAmountBytes(sum(hostPathUsedBytesList));
            } else {
                dto.setHostPathUsedBytesList(new ArrayList<>());
            }
            dto.setCreatedBy(createdBy);
            dto.setCreatedOn(createdOn);
            dto.setModifiedBy(modifiedBy);
            dto.setModifiedOn(modifiedOn);
            dto.setRelatedWorkloadList(relatedWorkloadList);
            dto.setPods(pods);
            dto.setEbsStorageType(ebsStorageType);

            return dto;
        }

        private long sum(List<Long> list) {
            long sum = 0;
            for (Long num : list) {
                sum += num;
            }

            return sum;
        }
    }
}
