package cc.backend.kubernetes.storage.usage.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * some operation for ceph image usage
 *
 * @author wangchunyang@gmail.com
 */
public interface CephImageUsageRepository extends JpaRepository<CephImageUsage, CephImageUsage.ImageUsageId> {

    /**
     * find some image usage in a namespace
     *
     * @param namespaceName the namespace name
     * @param imageNameList the image list to query
     * @return the usage list
     */
    List<CephImageUsage> findByNamespaceNameAndImageNameIn(String namespaceName, List<String> imageNameList);

    /**
     * find a image usage in a namespace
     *
     * @param namespaceName the namespace name
     * @param imageName     the image name
     * @return the image usage
     */
    CephImageUsage findByNamespaceNameAndImageName(String namespaceName, String imageName);

    /**
     * find a namespace's all image usage info
     *
     * @param namespaceName the namespace name
     * @return the image usage info list
     */
    List<CephImageUsage> findByNamespaceName(String namespaceName);

    /**
     * find a cluster's ceph rbd provisioned bytes
     *
     * @return the total provisioned bytes in a cluster
     */
    @Query(value = "SELECT sum(bytes_provisioned) FROM ceph_image_usage u", nativeQuery = true)
    Long findSumOfBytesProvisioned();

    /**
     * find a cluster's ceph rbd used bytes
     *
     * @return the total used bytes in a cluster
     */
    @Query(value = "SELECT sum(bytes_used) FROM ceph_image_usage u", nativeQuery = true)
    Long findSumOfBytesUsed();

    /**
     * delete a list of image in specified namespace
     *
     * @param namespaceName the namespace name
     * @param imageNameList the image list for deleting
     */
    @Transactional(rollbackFor = Exception.class)
    void deleteByNamespaceNameAndImageName(String namespaceName, String imageNameList);

    /**
     * delete a namespace's all images
     *
     * @param namespaceName the namespace name
     */
    @Transactional(rollbackFor = Exception.class)
    void deleteByNamespaceName(String namespaceName);
}
