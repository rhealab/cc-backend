package cc.backend.kubernetes.storage.usage.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author wangchunyang@gmail.com
 */
public interface CephfsDirectoryUsageRepository extends JpaRepository<CephfsDirectoryUsage, String> {
    CephfsDirectoryUsage findByNamespaceNameAndDirectoryName(String namespaceName, String directoryName);

    List<CephfsDirectoryUsage> findByNamespaceNameAndDirectoryNameIn(String namespaceName, List<String> directoryName);

    @Transactional
    void deleteByNamespaceName(String namespaceName);

    @Transactional
    void deleteByNamespaceNameAndDirectoryName(String namespaceName, String directoryName);
}