package cc.backend.kubernetes.storage.usage.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/9/14.
 */
@Entity
public class NfsDirectoryUsage {
    @Id
    private String directoryName;
    private String namespaceName;
    private long bytesUsed;
    private Date timestamp;

    public String getDirectoryName() {
        return directoryName;
    }

    public void setDirectoryName(String directoryName) {
        this.directoryName = directoryName;
    }

    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    public long getBytesUsed() {
        return bytesUsed;
    }

    public void setBytesUsed(long bytesUsed) {
        this.bytesUsed = bytesUsed;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return "NfsDirectoryUsage{" +
                ", namespaceName='" + namespaceName + '\'' +
                ", directoryName='" + directoryName + '\'' +
                ", bytesUsed=" + bytesUsed +
                ", timestamp=" + timestamp +
                '}';
    }
}
