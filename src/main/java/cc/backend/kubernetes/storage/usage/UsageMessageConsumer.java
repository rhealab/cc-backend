package cc.backend.kubernetes.storage.usage;

import cc.backend.audit.request.EnnContext;
import cc.backend.kubernetes.storage.usage.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

import static cc.backend.RabbitConfig.*;

/**
 * @author wangchunyang@gmail.com
 */
@Component
public class UsageMessageConsumer {
    private static final Logger logger = LoggerFactory.getLogger(UsageMessageConsumer.class);

    @Inject
    private CephPoolUsageRepository cephPoolUsageRepository;

    @Inject
    private CephImageUsageRepository cephImageUsageRepository;

    @Inject
    private CephGlobalUsageRepository cephGlobalUsageRepository;

    @Inject
    private CephfsDirectoryUsageRepository cephfsDirectoryUsageRepository;

    @Inject
    private NfsGlobalUsageRepository nfsGlobalUsageRepository;

    @Inject
    private NfsDirectoryUsageRepository nfsDirectoryUsageRepository;

    //FIXME get cluster name
    @RabbitListener(queues = CEPH_IMAGE_USAGE_Q)
    public void gotImageUsages(List<CephImageUsage> usages) {
        logger.info("RabbitMQ: received usages - {}", usages);
        cephImageUsageRepository.save(usages);
    }

    @RabbitListener(queues = CEPH_POOL_USAGE_Q)
    public void gotCephUsageMessage(CephUsageMessage message) {
        logger.info("RabbitMQ: received message - {}", message);
        EnnContext.setClusterName(message.getClusterName());

        CephGlobalUsage global = mapGlobalUsage(message);
        cephGlobalUsageRepository.save(global);

        message.getPoolUsages().forEach(pu -> pu.setTimestamp(message.getTimestamp()));
        cephPoolUsageRepository.save(message.getPoolUsages());

        message.getDirectoryUsages().forEach(du -> du.setTimestamp(message.getTimestamp()));
        cephfsDirectoryUsageRepository.save(message.getDirectoryUsages());
    }

    @RabbitListener(queues = NFS_USAGE_Q)
    public void gotNfsUsageMessage(NfsUsageMessage message) {
        logger.info("RabbitMQ: received message - {}", message);
        EnnContext.setClusterName(message.getClusterName());

        NfsGlobalUsage global = mapNfsGlobalUsage(message);
        nfsGlobalUsageRepository.save(global);

        message.getDirectoryUsages().forEach(du -> du.setTimestamp(message.getTimestamp()));
        nfsDirectoryUsageRepository.save(message.getDirectoryUsages());
    }

    private CephGlobalUsage mapGlobalUsage(CephUsageMessage message) {
        CephGlobalUsage global = new CephGlobalUsage();
        global.setCephfsAvailBytes(message.getCephfsAvailBytes());
        global.setCephfsUsedBytes(message.getCephfsUsedBytes());
        global.setCephfsTotalBytes(message.getCephfsTotalBytes());
        global.setTimestamp(message.getTimestamp());
        global.setTotalAvailBytes(message.getTotalAvailBytes());
        global.setTotalBytes(message.getTotalBytes());
        global.setTotalUsedBytes(message.getTotalUsedBytes());
        return global;
    }

    private NfsGlobalUsage mapNfsGlobalUsage(NfsUsageMessage message) {
        NfsGlobalUsage global = new NfsGlobalUsage();
        global.setTimestamp(message.getTimestamp());
        global.setTotalAvailBytes(message.getTotalAvailBytes());
        global.setTotalBytes(message.getTotalBytes());
        global.setTotalUsedBytes(message.getTotalUsedBytes());
        return global;
    }
}
