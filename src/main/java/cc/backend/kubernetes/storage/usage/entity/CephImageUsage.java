package cc.backend.kubernetes.storage.usage.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;
import java.util.Date;

/**
 * @author wangchunyang@gmail.com
 */
@Entity
@IdClass(CephImageUsage.ImageUsageId.class)
public class CephImageUsage {
    @Id
    private String poolName;
    @Id
    private String imageName;
    private String namespaceName;
    private Date timestamp;
    private long bytesUsed;
    private long bytesProvisioned;

    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    public String getPoolName() {
        return poolName;
    }

    public void setPoolName(String poolName) {
        this.poolName = poolName;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public long getBytesUsed() {
        return bytesUsed;
    }

    public void setBytesUsed(long bytesUsed) {
        this.bytesUsed = bytesUsed;
    }

    public long getBytesProvisioned() {
        return bytesProvisioned;
    }

    public void setBytesProvisioned(long bytesProvisioned) {
        this.bytesProvisioned = bytesProvisioned;
    }

    @Override
    public String toString() {
        return "CephImageUsage{" +
                "poolName='" + poolName + '\'' +
                ", imageName='" + imageName + '\'' +
                ", namespaceName='" + namespaceName + '\'' +
                ", timestamp=" + timestamp +
                ", bytesUsed=" + bytesUsed +
                ", bytesProvisioned=" + bytesProvisioned +
                '}';
    }

    public static class ImageUsageId implements Serializable {
        String poolName;
        String imageName;

        public ImageUsageId() {
        }

        public ImageUsageId(String poolName, String imageName) {
            this.poolName = poolName;
            this.imageName = imageName;
        }

        public String getPoolName() {
            return poolName;
        }

        public void setPoolName(String poolName) {
            this.poolName = poolName;
        }

        public String getImageName() {
            return imageName;
        }

        public void setImageName(String imageName) {
            this.imageName = imageName;
        }
    }
}
