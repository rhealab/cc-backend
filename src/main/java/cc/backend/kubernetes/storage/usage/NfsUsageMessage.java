package cc.backend.kubernetes.storage.usage;

import cc.backend.kubernetes.storage.usage.entity.NfsDirectoryUsage;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author wangchunyang@gmail.com
 */
public class NfsUsageMessage {
    private String clusterName;
    private Date timestamp;
    private long totalBytes;
    private long totalUsedBytes;
    private long totalAvailBytes;

    private List<NfsDirectoryUsage> directoryUsages = new ArrayList<>();

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public long getTotalBytes() {
        return totalBytes;
    }

    public void setTotalBytes(long totalBytes) {
        this.totalBytes = totalBytes;
    }

    public long getTotalUsedBytes() {
        return totalUsedBytes;
    }

    public void setTotalUsedBytes(long totalUsedBytes) {
        this.totalUsedBytes = totalUsedBytes;
    }

    public long getTotalAvailBytes() {
        return totalAvailBytes;
    }

    public void setTotalAvailBytes(long totalAvailBytes) {
        this.totalAvailBytes = totalAvailBytes;
    }

    public List<NfsDirectoryUsage> getDirectoryUsages() {
        return directoryUsages;
    }

    public void setDirectoryUsages(List<NfsDirectoryUsage> directoryUsages) {
        this.directoryUsages = directoryUsages;
    }

    @Override
    public String toString() {
        return "NfsUsageMessage{" +
                "timestamp=" + timestamp +
                ", totalBytes=" + totalBytes +
                ", totalUsedBytes=" + totalUsedBytes +
                ", totalAvailBytes=" + totalAvailBytes +
                ", directoryUsages=" + directoryUsages +
                '}';
    }
}
