package cc.backend.kubernetes.storage.usage.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/9/14.
 */
public interface NfsDirectoryUsageRepository extends JpaRepository<NfsDirectoryUsage, String> {
    NfsDirectoryUsage findByNamespaceNameAndDirectoryName(String namespaceName, String directoryName);

    List<NfsDirectoryUsage> findByNamespaceNameAndDirectoryNameIn(String namespaceName, List<String> directoryName);

    @Transactional
    void deleteByNamespaceName(String namespaceName);

    @Transactional
    void deleteByNamespaceNameAndDirectoryName(String namespaceName, String directoryName);
}