package cc.backend.kubernetes.storage.usage.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * @author wangchunyang@gmail.com
 */
@Entity
public class CephGlobalUsage {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private Date timestamp;
    private long totalBytes;
    private long totalUsedBytes;
    private long totalAvailBytes;
    private long cephfsUsedBytes;
    private long cephfsAvailBytes;
    private long cephfsTotalBytes;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public long getTotalBytes() {
        return totalBytes;
    }

    public void setTotalBytes(long totalBytes) {
        this.totalBytes = totalBytes;
    }

    public long getTotalUsedBytes() {
        return totalUsedBytes;
    }

    public void setTotalUsedBytes(long totalUsedBytes) {
        this.totalUsedBytes = totalUsedBytes;
    }

    public long getTotalAvailBytes() {
        return totalAvailBytes;
    }

    public void setTotalAvailBytes(long totalAvailBytes) {
        this.totalAvailBytes = totalAvailBytes;
    }

    public long getCephfsUsedBytes() {
        return cephfsUsedBytes;
    }

    public void setCephfsUsedBytes(long cephfsUsedBytes) {
        this.cephfsUsedBytes = cephfsUsedBytes;
    }

    public long getCephfsAvailBytes() {
        return cephfsAvailBytes;
    }

    public void setCephfsAvailBytes(long cephfsAvailBytes) {
        this.cephfsAvailBytes = cephfsAvailBytes;
    }

    public long getCephfsTotalBytes() {
        return cephfsTotalBytes;
    }

    public void setCephfsTotalBytes(long cephfsTotalBytes) {
        this.cephfsTotalBytes = cephfsTotalBytes;
    }

    @Override
    public String toString() {
        return "CephGlobalUsage{" +
                "id=" + id +
                ", timestamp=" + timestamp +
                ", totalBytes=" + totalBytes +
                ", totalUsedBytes=" + totalUsedBytes +
                ", totalAvailBytes=" + totalAvailBytes +
                ", cephfsUsedBytes=" + cephfsUsedBytes +
                ", cephfsAvailBytes=" + cephfsAvailBytes +
                ", cephfsTotalBytes=" + cephfsTotalBytes +
                '}';
    }
}
