package cc.backend.kubernetes.storage.usage.entity;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author wangchunyang@gmail.com
 */
public interface CephPoolUsageRepository extends JpaRepository<CephPoolUsage, String> {
    CephPoolUsage findByNamespaceName(String namespace);

}
