package cc.backend.kubernetes.storage.usage;

import cc.backend.kubernetes.storage.usage.entity.CephPoolUsage;
import cc.backend.kubernetes.storage.usage.entity.CephfsDirectoryUsage;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author wangchunyang@gmail.com
 */
public class CephUsageMessage {
    private String clusterName;
    private Date timestamp;
    private long totalBytes;
    private long totalUsedBytes;
    private long totalAvailBytes;
    private long cephfsUsedBytes;
    private long cephfsAvailBytes;
    private long cephfsTotalBytes;

    private List<CephPoolUsage> poolUsages = new ArrayList<>();
    private List<CephfsDirectoryUsage> directoryUsages = new ArrayList<>();

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public long getTotalBytes() {
        return totalBytes;
    }

    public void setTotalBytes(long totalBytes) {
        this.totalBytes = totalBytes;
    }

    public long getTotalUsedBytes() {
        return totalUsedBytes;
    }

    public void setTotalUsedBytes(long totalUsedBytes) {
        this.totalUsedBytes = totalUsedBytes;
    }

    public long getTotalAvailBytes() {
        return totalAvailBytes;
    }

    public void setTotalAvailBytes(long totalAvailBytes) {
        this.totalAvailBytes = totalAvailBytes;
    }

    public List<CephPoolUsage> getPoolUsages() {
        return poolUsages;
    }

    public void setPoolUsages(List<CephPoolUsage> poolUsages) {
        this.poolUsages = poolUsages;
    }

    public List<CephfsDirectoryUsage> getDirectoryUsages() {
        return directoryUsages;
    }

    public void setDirectoryUsages(List<CephfsDirectoryUsage> directoryUsages) {
        this.directoryUsages = directoryUsages;
    }

    public void addPoolUsage(CephPoolUsage poolUsage) {
        poolUsages.add(poolUsage);
    }

    public long getCephfsUsedBytes() {
        return cephfsUsedBytes;
    }

    public void setCephfsUsedBytes(long cephfsUsedBytes) {
        this.cephfsUsedBytes = cephfsUsedBytes;
    }

    public long getCephfsAvailBytes() {
        return cephfsAvailBytes;
    }

    public void setCephfsAvailBytes(long cephfsAvailBytes) {
        this.cephfsAvailBytes = cephfsAvailBytes;
    }

    public long getCephfsTotalBytes() {
        return cephfsTotalBytes;
    }

    public void setCephfsTotalBytes(long cephfsTotalBytes) {
        this.cephfsTotalBytes = cephfsTotalBytes;
    }

    @Override
    public String toString() {
        return "CephUsageMessage{" +
                "timestamp=" + timestamp +
                ", totalBytes=" + totalBytes +
                ", totalUsedBytes=" + totalUsedBytes +
                ", totalAvailBytes=" + totalAvailBytes +
                ", cephfsUsedBytes=" + cephfsUsedBytes +
                ", cephfsAvailBytes=" + cephfsAvailBytes +
                ", poolUsages=" + poolUsages +
                ", directoryUsages=" + directoryUsages +
                '}';
    }

    public void addDirectoryUsage(CephfsDirectoryUsage directoryUsage) {
        directoryUsages.add(directoryUsage);
    }
}
