package cc.backend.kubernetes.storage.usage.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

/**
 * @author wangchunyang@gmail.com
 */
@Entity
public class CephPoolUsage {
    @Id
    private String poolName;
    private String namespaceName;
    private Date timestamp;
    private long bytesUsed;

    public String getPoolName() {
        return poolName;
    }

    public void setPoolName(String poolName) {
        this.poolName = poolName;
    }

    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public long getBytesUsed() {
        return bytesUsed;
    }

    public void setBytesUsed(long bytesUsed) {
        this.bytesUsed = bytesUsed;
    }

    @Override
    public String toString() {
        return "CephPoolUsage{" +
                "poolName='" + poolName + '\'' +
                ", namespaceName='" + namespaceName + '\'' +
                ", timestamp=" + timestamp +
                ", bytesUsed=" + bytesUsed +
                '}';
    }
}
