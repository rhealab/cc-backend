package cc.backend.kubernetes.storage.usage.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author wangchunyang@gmail.com
 */
public interface NfsGlobalUsageRepository extends JpaRepository<NfsGlobalUsage, Long> {
    @Query(value = "SELECT * FROM nfs_global_usage ORDER BY id DESC LIMIT 1", nativeQuery = true)
    NfsGlobalUsage findLatest();
}
