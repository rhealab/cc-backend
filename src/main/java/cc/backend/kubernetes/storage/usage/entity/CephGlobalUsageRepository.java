package cc.backend.kubernetes.storage.usage.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author wangchunyang@gmail.com
 */
public interface CephGlobalUsageRepository extends JpaRepository<CephGlobalUsage, Long> {
    @Query(value = "SELECT * FROM ceph_global_usage ORDER BY id DESC LIMIT 1", nativeQuery = true)
    CephGlobalUsage findLatest();
}
