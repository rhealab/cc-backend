package cc.backend.kubernetes.storage.usage.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/9/20.
 */
@Entity
public class NfsGlobalUsage {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private Date timestamp;
    private long totalBytes;
    private long totalUsedBytes;
    private long totalAvailBytes;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public long getTotalBytes() {
        return totalBytes;
    }

    public void setTotalBytes(long totalBytes) {
        this.totalBytes = totalBytes;
    }

    public long getTotalUsedBytes() {
        return totalUsedBytes;
    }

    public void setTotalUsedBytes(long totalUsedBytes) {
        this.totalUsedBytes = totalUsedBytes;
    }

    public long getTotalAvailBytes() {
        return totalAvailBytes;
    }

    public void setTotalAvailBytes(long totalAvailBytes) {
        this.totalAvailBytes = totalAvailBytes;
    }

    @Override
    public String toString() {
        return "CephGlobalUsage{" +
                "id=" + id +
                ", timestamp=" + timestamp +
                ", totalBytes=" + totalBytes +
                ", totalUsedBytes=" + totalUsedBytes +
                ", totalAvailBytes=" + totalAvailBytes +
                '}';
    }
}
