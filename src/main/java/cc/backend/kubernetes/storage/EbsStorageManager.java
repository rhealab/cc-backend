package cc.backend.kubernetes.storage;

import cc.backend.EnnProperties;
import cc.backend.audit.request.EnnContext;
import cc.backend.event.data.EventSource;
import cc.backend.kubernetes.AwsClientManager;
import cc.backend.kubernetes.storage.checker.StorageUpdateStatusChecker;
import cc.backend.kubernetes.storage.domain.EbsStorageType;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageType;
import cc.backend.kubernetes.storage.domain.StorageUpdateRequest;
import cc.backend.kubernetes.storage.processor.PvProcessor;
import cc.backend.kubernetes.storage.processor.PvcProcessor;
import com.amazonaws.services.ec2.model.CreateVolumeRequest;
import com.amazonaws.services.ec2.model.CreateVolumeResult;
import com.amazonaws.services.ec2.model.DeleteVolumeRequest;
import com.amazonaws.services.ec2.model.ModifyVolumeRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * @author wangchunyang@gmail.com
 */
@Named
public class EbsStorageManager {
    private static final Logger logger = LoggerFactory.getLogger(CephRbdStorageManager.class);

    @Inject
    private PvcProcessor pvcProcessor;

    @Inject
    private PvProcessor pvProcessor;

    @Inject
    private StorageUpdateStatusChecker storageUpdateStatusChecker;

    @Inject
    private AwsClientManager awsClientManager;

    @Inject
    private EnnProperties properties;

    @Inject
    private StorageRepository storageRepository;

    public void createStorage(EventSource<Storage> eventSource) {
        createEbs(eventSource.getPayload());
        pvcProcessor.create(eventSource);
    }

    private void createEbs(Storage storage) {
        int amountGigabytes = (int) (storage.getAmountBytes() / 1024 / 1024 / 1024);
        String volumeType = storage.getEbsStorageType() == null ?
                EbsStorageType.sc1.name() : storage.getEbsStorageType().name();
        CreateVolumeRequest request = new CreateVolumeRequest()
                .withAvailabilityZone(properties.getCurrentEbs().getZone())
                .withSize(amountGigabytes)
                .withVolumeType(volumeType);

        CreateVolumeResult response = awsClientManager.getEC2Client().createVolume(request);
        String volumeId = response.getVolume().getVolumeId();
        storage.setEbsVolumeId(volumeId);
        storageRepository.save(storage);
    }


    public void updateStorage(EventSource<StorageUpdateRequest> eventSource, Storage storage) {
        updateEbs(eventSource.getPayload(), storage);

        pvProcessor.update(eventSource);
        storageUpdateStatusChecker.check(EnnContext.getClusterName(), eventSource.getNamespaceName(), storage);
    }

    private void updateEbs(StorageUpdateRequest request, Storage storage) {
        if (request.getAmountBytes() == null && request.getEbsStorageType() == null) {
            return;
        }

        ModifyVolumeRequest ebsRequest = new ModifyVolumeRequest().withVolumeId(storage.getEbsVolumeId());

        if (request.getAmountBytes() != null) {
            ebsRequest.withSize((int) (request.getAmountBytes() / 1024 / 1024 / 1024));
        }

        if (request.getEbsStorageType() != null) {
            ebsRequest.withVolumeType(request.getEbsStorageType().name());
        }

        awsClientManager.getEC2Client().modifyVolume(ebsRequest);
    }

    public void deleteEbsBatch(List<Storage> storageList) {
        storageList.stream()
                .filter(storage -> storage.getStorageType() == StorageType.EBS)
                .forEach(storage -> deleteEbs(storage.getEbsVolumeId()));
    }

    public void deleteStorage(EventSource<Storage> eventSource) {
        deleteEbs(eventSource.getPayload().getEbsVolumeId());
        pvcProcessor.delete(eventSource);
    }

    private void deleteEbs(String volumeId) {
        DeleteVolumeRequest request = new DeleteVolumeRequest().withVolumeId(volumeId);
        awsClientManager.getEC2Client().deleteVolume(request);
    }
}
