package cc.backend.kubernetes.storage.messages;

import cc.backend.kubernetes.namespaces.messages.IMessage;

/**
 * @author wangjinxin
 */
public class ImageDeletedMessage implements IMessage{
    private String clusterName;
    private String namespaceName;
    private String storageName;

    private String userId;
    private String requestId;

    @Override
    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    @Override
    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @Override
    public String toString() {
        return "ImageDeletedMessage{" +
                "namespaceName='" + namespaceName + '\'' +
                ", storageName='" + storageName + '\'' +
                ", userId='" + userId + '\'' +
                ", requestId='" + requestId + '\'' +
                '}';
    }

    public static ImageDeletedMessage from(ImageDeleteMessage message) {
        ImageDeletedMessage deletedMessage = new ImageDeletedMessage();
        deletedMessage.setRequestId(message.getRequestId());
        deletedMessage.setUserId(message.getUserId());
        deletedMessage.setClusterName(message.getClusterName());
        deletedMessage.setNamespaceName(message.getNamespaceName());
        deletedMessage.setStorageName(message.getStorageName());

        return deletedMessage;
    }
}
