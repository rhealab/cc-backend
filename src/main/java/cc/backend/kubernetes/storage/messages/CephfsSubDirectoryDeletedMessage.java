package cc.backend.kubernetes.storage.messages;

import cc.backend.kubernetes.namespaces.messages.IMessage;

/**
 * @author wangjinxin
 */
public class CephfsSubDirectoryDeletedMessage implements IMessage {
    private String requestId;
    private String userId;
    private String clusterName;
    private String namespaceName;
    private String storageName;

    @Override
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    @Override
    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    @Override
    public String toString() {
        return "CephfsSubDirectoryDeletedMessage{" +
                "requestId='" + requestId + '\'' +
                ", userId='" + userId + '\'' +
                ", clusterName='" + clusterName + '\'' +
                ", namespaceName='" + namespaceName + '\'' +
                ", storageName='" + storageName + '\'' +
                '}';
    }

    public static CephfsSubDirectoryDeletedMessage from(CephfsSubDirectoryDeleteMessage message) {
        CephfsSubDirectoryDeletedMessage deletedMessage = new CephfsSubDirectoryDeletedMessage();
        deletedMessage.setRequestId(message.getRequestId());
        deletedMessage.setUserId(message.getUserId());
        deletedMessage.setClusterName(message.getClusterName());
        deletedMessage.setNamespaceName(message.getNamespaceName());
        deletedMessage.setStorageName(message.getStorageName());

        return deletedMessage;
    }
}
