package cc.backend.kubernetes.storage.messages;

import cc.backend.kubernetes.namespaces.messages.IMessage;

/**
 * @author wangchunyang@gmail.com
 */
public class CephfsSubDirectoryCreatedMessage implements IMessage{
    private String requestId;
    private String userId;
    private String clusterName;
    private String namespaceName;
    private String storageName;

    @Override
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    @Override
    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    @Override
    public String toString() {
        return "CephfsSubDirectoryCreatedMessage{" +
                "requestId='" + requestId + '\'' +
                ", userId='" + userId + '\'' +
                ", clusterName='" + clusterName + '\'' +
                ", namespaceName='" + namespaceName + '\'' +
                ", storageName='" + storageName + '\'' +
                '}';
    }

    public static CephfsSubDirectoryCreatedMessage from(CephfsSubDirectoryCreateMessage message) {
        CephfsSubDirectoryCreatedMessage createdMessage = new CephfsSubDirectoryCreatedMessage();
        createdMessage.setRequestId(message.getRequestId());
        createdMessage.setUserId(message.getUserId());
        createdMessage.setClusterName(message.getClusterName());
        createdMessage.setNamespaceName(message.getNamespaceName());
        createdMessage.setStorageName(message.getStorageName());

        return createdMessage;
    }
}
