package cc.backend.kubernetes.storage.messages;


/**
 * @author wangchunyang@gmail.com
 */
public class NfsSubDirectoryCreateMessage {
    private String clusterName;
    private String requestId;
    private String userId;
    private String namespaceName;
    private String storageName;

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    @Override
    public String toString() {
        return "NfsSubDirectoryCreateMessage{" +
                "clusterName='" + clusterName + '\'' +
                ", requestId='" + requestId + '\'' +
                ", userId='" + userId + '\'' +
                ", namespaceName='" + namespaceName + '\'' +
                ", storageName='" + storageName + '\'' +
                '}';
    }
}
