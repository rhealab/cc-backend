package cc.backend.kubernetes.storage.messages;

import cc.backend.kubernetes.namespaces.messages.IMessage;

/**
 * @author wangjinxin
 */
public class ImageCreatedUpdatedMessage implements IMessage {
    private String clusterName;
    private String namespaceName;
    private String storageName;
    private long amountBytes;

    private String userId;
    private String requestId;

    @Override
    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    @Override
    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public long getAmountBytes() {
        return amountBytes;
    }

    public void setAmountBytes(long amountBytes) {
        this.amountBytes = amountBytes;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @Override
    public String toString() {
        return "ImageCreatedUpdatedMessage{" +
                "namespaceName='" + namespaceName + '\'' +
                ", storageName='" + storageName + '\'' +
                ", amountBytes=" + amountBytes +
                ", userId='" + userId + '\'' +
                ", requestId='" + requestId + '\'' +
                '}';
    }

    public static ImageCreatedUpdatedMessage from(ImageCreateUpdateMessage message) {
        ImageCreatedUpdatedMessage createdUpdatedMessage = new ImageCreatedUpdatedMessage();
        createdUpdatedMessage.setRequestId(message.getRequestId());
        createdUpdatedMessage.setUserId(message.getUserId());
        createdUpdatedMessage.setClusterName(message.getClusterName());
        createdUpdatedMessage.setNamespaceName(message.getNamespaceName());
        createdUpdatedMessage.setStorageName(message.getStorageName());
        createdUpdatedMessage.setAmountBytes(message.getAmountBytes());

        return createdUpdatedMessage;
    }
}
