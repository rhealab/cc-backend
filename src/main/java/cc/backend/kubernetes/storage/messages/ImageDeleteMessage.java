package cc.backend.kubernetes.storage.messages;

/**
 * @author wangchunyang@gmail.com
 */
public class ImageDeleteMessage {
    private String clusterName;
    private String namespaceName;
    private String storageName;

    private String userId;
    private String requestId;

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getNamespaceName() {
        return namespaceName;
    }

    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @Override
    public String toString() {
        return "ImageDeleteMessage{" +
                "clusterName='" + clusterName + '\'' +
                ", namespaceName='" + namespaceName + '\'' +
                ", storageName='" + storageName + '\'' +
                ", userId='" + userId + '\'' +
                ", requestId='" + requestId + '\'' +
                '}';
    }
}
