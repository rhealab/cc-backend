package cc.backend.kubernetes.storage;

import cc.backend.RabbitConfig;
import cc.backend.audit.request.EnnContext;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceRepository;
import cc.backend.event.data.EventSourceType;
import cc.backend.kubernetes.storage.checker.StorageCreationStatusChecker;
import cc.backend.kubernetes.storage.checker.StorageDeletionStatusChecker;
import cc.backend.kubernetes.storage.checker.StorageUpdateStatusChecker;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageUpdateRequest;
import cc.backend.kubernetes.storage.messages.ImageCreateUpdateMessage;
import cc.backend.kubernetes.storage.messages.ImageCreatedUpdatedMessage;
import cc.backend.kubernetes.storage.messages.ImageDeleteMessage;
import cc.backend.kubernetes.storage.messages.ImageDeletedMessage;
import cc.backend.kubernetes.storage.processor.CephImageProcessor;
import cc.backend.kubernetes.storage.processor.PvProcessor;
import cc.backend.kubernetes.storage.processor.PvcProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;

import javax.inject.Inject;
import javax.inject.Named;

import static cc.backend.RabbitConfig.*;

/**
 * @author wangchunyang@gmail.com
 */
@Named
public class CephRbdStorageManager {
    private static final Logger logger = LoggerFactory.getLogger(CephRbdStorageManager.class);

    @Inject
    private RabbitMessagingTemplate messagingTemplate;

    @Inject
    private PvcProcessor pvcProcessor;

    @Inject
    private PvProcessor pvProcessor;

    @Inject
    private StorageCreationStatusChecker storageCreationStatusChecker;

    @Inject
    private StorageUpdateStatusChecker storageUpdateStatusChecker;

    @Inject
    private StorageDeletionStatusChecker storageDeletionStatusChecker;

    @Inject
    private StorageRepository storageRepository;

    @Inject
    private CephImageProcessor cephImageProcessor;

    @Inject
    private EventSourceRepository eventSourceRepository;

    public void createStorage(EventSource<Storage> eventSource) {
        Storage storage = eventSource.getPayload();

        pvcProcessor.create(eventSource);
        sendCreateStorageMessage(storage);
    }

    private void sendCreateStorageMessage(Storage storage) {
        ImageCreateUpdateMessage message = new ImageCreateUpdateMessage();
        message.setUserId(EnnContext.getUserId());
        message.setRequestId(EnnContext.getRequestId());
        message.setAmountBytes(storage.getAmountBytes());
        message.setClusterName(EnnContext.getClusterName());
        message.setNamespaceName(storage.getNamespaceName());
        message.setStorageName(storage.getStorageName());
        messagingTemplate.convertAndSend(CEPH_IMAGE_CREATE_Q, message);
        logger.info("RabbitMQ: sent message - q={}, message={}", CEPH_IMAGE_CREATE_Q, message);
    }

    @RabbitListener(queues = RabbitConfig.CEPH_IMAGE_CREATED_Q)
    public void onCephImageCreated(ImageCreatedUpdatedMessage message) {
        EnnContext.setContext(message);

        String namespaceName = message.getNamespaceName();
        logger.info("RabbitMQ: received ImageCreatedUpdatedMessage namespace={}, storage={}, bytes={}",
                namespaceName, message.getStorageName(), message.getAmountBytes());

        Storage storage = storageRepository.findByNamespaceNameAndStorageName(namespaceName, message.getStorageName());

        if (storage == null || storage.getStatus() != Storage.Status.CREATE_PENDING) {
            return;
        }

        EventSource<Storage> eventSource = getStorageCreateEventSource(namespaceName,
                message.getStorageName(), storage);
        if (eventSource.getRequestId().equals(message.getRequestId())) {
            cephImageProcessor.create(eventSource);
            storageCreationStatusChecker.check(EnnContext.getClusterName(), namespaceName, storage);
        }
    }

    private <T> EventSource<T> getStorageCreateEventSource(String namespaceName,
                                                           String storageName,
                                                           T payload) {
        EventSource<T> eventSource = eventSourceRepository
                .findTopByNamespaceNameAndSourceTypeAndSourceNameOrderByIdDesc(namespaceName,
                        EventSourceType.STORAGE_CREATE, storageName);
        eventSource.setPayload(payload);
        return eventSource;
    }


    public void updateStorage(EventSource<StorageUpdateRequest> eventSource, Storage storage) {
        pvProcessor.update(eventSource);
        if (eventSource.getPayload().getAmountBytes() != null) {
            sendUpdateStorageMessage(eventSource);
        } else {
            storageUpdateStatusChecker.check(EnnContext.getClusterName(), eventSource.getNamespaceName(), storage);
        }
    }

    private void sendUpdateStorageMessage(EventSource<StorageUpdateRequest> eventSource) {
        ImageCreateUpdateMessage message = new ImageCreateUpdateMessage();
        message.setUserId(EnnContext.getUserId());
        message.setRequestId(EnnContext.getRequestId());
        message.setAmountBytes(eventSource.getPayload().getAmountBytes());
        message.setClusterName(EnnContext.getClusterName());
        message.setNamespaceName(eventSource.getNamespaceName());
        message.setStorageName(eventSource.getSourceName());
        messagingTemplate.convertAndSend(CEPH_IMAGE_RESIZE_Q, message);
        logger.info("RabbitMQ: sent message - q={}, message={}", CEPH_IMAGE_RESIZE_Q, message);
    }

    @RabbitListener(queues = RabbitConfig.CEPH_IMAGE_RESIZED_Q)
    public void onCephImageResized(ImageCreatedUpdatedMessage message) {
        EnnContext.setContext(message);

        String namespaceName = message.getNamespaceName();
        logger.info("RabbitMQ: received ImageCreatedUpdatedMessage namespace={}, storage={}, size={}",
                namespaceName, message.getStorageName(), message.getAmountBytes());

        Storage storage = storageRepository.findByNamespaceNameAndStorageName(namespaceName, message.getStorageName());

        if (storage == null || storage.getStatus() != Storage.Status.UPDATE_PENDING) {
            return;
        }

        StorageUpdateRequest request = new StorageUpdateRequest();
        request.setAmountBytes(message.getAmountBytes());
        EventSource<StorageUpdateRequest> eventSource = getStorageUpdateEventSource(namespaceName,
                message.getStorageName(), request);
        if (eventSource.getRequestId().equals(message.getRequestId())) {
            cephImageProcessor.update(eventSource);
            storageUpdateStatusChecker.check(EnnContext.getClusterName(), namespaceName, storage);
        }
    }

    private <T> EventSource<T> getStorageUpdateEventSource(String namespaceName,
                                                           String storageName,
                                                           T payload) {
        EventSource<T> eventSource = eventSourceRepository
                .findTopByNamespaceNameAndSourceTypeAndSourceNameOrderByIdDesc(namespaceName,
                        EventSourceType.STORAGE_UPDATE, storageName);
        eventSource.setPayload(payload);
        return eventSource;
    }

    public void deleteStorage(EventSource<Storage> eventSource) {
        pvcProcessor.delete(eventSource);
        sendDirectoryDeleteMessage(eventSource.getPayload());
    }

    public void sendDirectoryDeleteMessage(Storage storage) {
        ImageDeleteMessage message = new ImageDeleteMessage();
        message.setUserId(EnnContext.getUserId());
        message.setRequestId(EnnContext.getRequestId());
        message.setClusterName(EnnContext.getClusterName());
        message.setNamespaceName(storage.getNamespaceName());
        message.setStorageName(storage.getStorageName());
        messagingTemplate.convertAndSend(CEPH_IMAGE_DELETE_Q, message);
        logger.info("RabbitMQ: sent message - q={}, message={}", CEPH_IMAGE_DELETE_Q, message);
    }

    @RabbitListener(queues = RabbitConfig.CEPH_IMAGE_DELETED_Q)
    public void onCephImageDeleted(ImageDeletedMessage message) {
        EnnContext.setContext(message);

        String namespaceName = message.getNamespaceName();
        logger.info("RabbitMQ: received ImageDeletedMessage namespace={}, storage={}",
                namespaceName, message.getStorageName());

        Storage storage = storageRepository.findByNamespaceNameAndStorageName(namespaceName, message.getStorageName());

        if (storage == null || storage.getStatus() != Storage.Status.DELETE_PENDING) {
            return;
        }

        EventSource<Storage> eventSource = getStorageDeleteEventSource(namespaceName,
                message.getStorageName(), storage);
        if (eventSource.getRequestId().equals(message.getRequestId())) {
            cephImageProcessor.delete(eventSource);
            storageDeletionStatusChecker.check(EnnContext.getClusterName(), namespaceName, storage);
        }
    }

    private <T> EventSource<T> getStorageDeleteEventSource(String namespaceName,
                                                           String storageName,
                                                           T payload) {
        EventSource<T> eventSource = eventSourceRepository
                .findTopByNamespaceNameAndSourceTypeAndSourceNameOrderByIdDesc(namespaceName,
                        EventSourceType.STORAGE_DELETE, storageName);
        eventSource.setPayload(payload);
        return eventSource;
    }
}
