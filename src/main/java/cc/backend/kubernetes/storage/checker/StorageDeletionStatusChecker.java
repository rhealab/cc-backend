package cc.backend.kubernetes.storage.checker;

import cc.backend.event.AbstractRedisStatusChecker;
import cc.backend.event.EventErrorMessage;
import cc.backend.event.data.Event;
import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceType;
import cc.backend.kubernetes.storage.StorageRepository;
import cc.backend.kubernetes.storage.domain.Storage;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

import static cc.backend.kubernetes.Constants.REDIS_STORAGE_DELETE_STATUS_KEY_PREFIX;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/10/12.
 */
@Component
public class StorageDeletionStatusChecker extends AbstractRedisStatusChecker<Storage, Storage.Status> {

    @Inject
    private StorageRepository storageRepository;

    @Override
    public String composeRedisKey(String clusterName, String namespaceName, Storage storage) {
        return REDIS_STORAGE_DELETE_STATUS_KEY_PREFIX + clusterName + "." + namespaceName + "." + storage.getId();
    }

    @Override
    public long getStatusCheckPeriodMillis() {
        return properties.getStatusCheck().getPeriodMillis().getStorage();
    }

    @Override
    public Storage.Status check(String clusterName, String namespaceName, Storage storage) {
        storage = storageRepository.findOne(storage.getId());

        if (storage == null) {
            return null;
        }

        Storage.Status status = storage.getStatus();
        if (status != Storage.Status.DELETE_PENDING ||
                !isDeleteFinished(findEventSource(namespaceName, getEventSourceType(), storage.getStorageName()), storage)) {
            return status;
        }

        storage.setStatus(Storage.Status.DELETE_SUCCESS);
        //TODO set storage delete flag
        storageRepository.delete(storage);
        cancel(clusterName, namespaceName, storage);
        return Storage.Status.DELETE_SUCCESS;
    }

    @Override
    protected void doFinalCheck(String clusterName, String namespaceName, Storage resource) {
        Storage.Status status = check(clusterName, namespaceName, resource);
        if (status == Storage.Status.DELETE_PENDING) {
            resource.setStatus(Storage.Status.DELETE_FAILED);
            storageRepository.save(resource);
        }
    }

    @Override
    public void handleError(EventErrorMessage message) {
        Object payload = message.getPayload();
        if (payload instanceof Storage) {
            Storage storage = storageRepository.findOne(((Storage) payload).getId());
            storage.setStatus(Storage.Status.DELETE_FAILED);
            storageRepository.save(storage);
        }
    }

    @Override
    public boolean shouldHandleError(EventErrorMessage message) {
        EventSourceType sourceType = message.getEventSourceType();
        if (sourceType != EventSourceType.STORAGE_DELETE) {
            return false;
        }

        Storage storage = storageRepository.findByNamespaceNameAndStorageName(message.getNamespaceName(), message.getEventSourceName());
        return storage != null && isRunning(message.getClusterName(), message.getNamespaceName(), storage);
    }

    @Override
    public EventSourceType getEventSourceType() {
        return EventSourceType.STORAGE_DELETE;
    }

    private boolean isDeleteFinished(EventSource eventSource, Storage storage) {
        EventSource createEventSource = findEventSource(eventSource.getNamespaceName(),
                EventSourceType.STORAGE_CREATE, storage.getStorageName());
        List<Event> createEvents = findEvents(createEventSource);
        List<Event> deleteEvents = findEvents(eventSource);

        if (!isPvcDeleted(createEvents, deleteEvents) ||
                !isPvDeleted(createEvents, deleteEvents)) {
            return false;
        }

        switch (storage.getStorageType()) {
            case RBD:
                return isCephImageDeleted(createEvents, deleteEvents);
            case NFS:
            case EFS:
                return isNfsSubDirDeleted(createEvents, deleteEvents);
            case CephFS:
                return isCephFsSubDirDeleted(createEvents, deleteEvents);
            default:
                return true;
        }
    }

    private boolean isPvDeleted(List<Event> createEvents, List<Event> deleteEvents) {
        for (Event createEvent : createEvents) {
            if (createEvent.getEventName() == EventName.CREATE_PV) {
                for (Event deleteEvent : deleteEvents) {
                    if (deleteEvent.getEventName() == EventName.DELETE_PV) {
                        return true;
                    }
                }
                return false;
            }
        }

        return true;
    }

    private boolean isPvcDeleted(List<Event> createEvents, List<Event> deleteEvents) {
        for (Event createEvent : createEvents) {
            if (createEvent.getEventName() == EventName.CREATE_PVC) {
                for (Event deleteEvent : deleteEvents) {
                    if (deleteEvent.getEventName() == EventName.DELETE_PVC) {
                        return true;
                    }
                }
                return false;
            }
        }

        return true;
    }

    private boolean isCephImageDeleted(List<Event> createEvents, List<Event> deleteEvents) {
        for (Event createEvent : createEvents) {
            if (createEvent.getEventName() == EventName.CREATE_CEPH_IMAGE) {
                for (Event deleteEvent : deleteEvents) {
                    if (deleteEvent.getEventName() == EventName.DELETE_CEPH_IMAGE) {
                        return true;
                    }
                }
                return false;
            }
        }

        return true;
    }

    private boolean isNfsSubDirDeleted(List<Event> createEvents, List<Event> deleteEvents) {
        for (Event createEvent : createEvents) {
            if (createEvent.getEventName() == EventName.CREATE_NFS_SUB_DIR) {
                for (Event deleteEvent : deleteEvents) {
                    if (deleteEvent.getEventName() == EventName.DELETE_NFS_SUB_DIR) {
                        return true;
                    }
                }
                return false;
            }
        }

        return true;
    }

    private boolean isCephFsSubDirDeleted(List<Event> createEvents, List<Event> deleteEvents) {
        for (Event createEvent : createEvents) {
            if (createEvent.getEventName() == EventName.CREATE_CEPH_FS_SUB_DIR) {
                for (Event deleteEvent : deleteEvents) {
                    if (deleteEvent.getEventName() == EventName.DELETE_CEPH_FS_SUB_DIR) {
                        return true;
                    }
                }
                return false;
            }
        }

        return true;
    }
}
