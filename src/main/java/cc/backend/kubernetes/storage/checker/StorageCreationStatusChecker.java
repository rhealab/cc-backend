package cc.backend.kubernetes.storage.checker;

import cc.backend.event.AbstractRedisStatusChecker;
import cc.backend.event.EventErrorMessage;
import cc.backend.event.data.Event;
import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceType;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.storage.StorageRepository;
import cc.backend.kubernetes.storage.StorageWorkloadRepository;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageWorkload;
import cc.backend.kubernetes.storage.domain.WorkloadType;
import cc.backend.kubernetes.utils.AppUtils;
import io.fabric8.kubernetes.api.model.Volume;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static cc.backend.kubernetes.Constants.REDIS_STORAGE_CREATE_STATUS_KEY_PREFIX;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/10/12.
 */
@Component
public class StorageCreationStatusChecker extends AbstractRedisStatusChecker<Storage, Storage.Status> {
    @Inject
    private StorageRepository storageRepository;

    @Inject
    private StorageWorkloadRepository storageWorkloadRepository;

    @Inject
    private KubernetesClientManager clientManager;

    @Override
    public String composeRedisKey(String clusterName, String namespaceName, Storage storage) {
        return REDIS_STORAGE_CREATE_STATUS_KEY_PREFIX + clusterName + "." + namespaceName + "." + storage.getId();
    }

    @Override
    public long getStatusCheckPeriodMillis() {
        return properties.getStatusCheck().getPeriodMillis().getStorage();
    }

    @Override
    public Storage.Status check(String clusterName, String namespaceName, Storage storage) {
        storage = storageRepository.findOne(storage.getId());

        if (storage == null) {
            return null;
        }

        Storage.Status status = storage.getStatus();
        if (status != Storage.Status.CREATE_PENDING ||
                !isCreateFinished(findEventSource(namespaceName, getEventSourceType(), storage.getStorageName()), storage)) {
            return status;
        }

        storage.setStatus(Storage.Status.CREATE_SUCCESS);
        storageRepository.save(storage);
        storageWorkloadRepository.save(composeStorageWorkloadRelation(namespaceName, storage));
        cancel(clusterName, namespaceName, storage);
        return Storage.Status.CREATE_SUCCESS;
    }

    private List<StorageWorkload> composeStorageWorkloadRelation(String namespaceName, Storage storage) {
        List<Deployment> relatedDeployments = getRelatedDeployments(namespaceName, storage);

        List<StorageWorkload> storageDeploymentList = relatedDeployments.stream()
                .map(deployment -> {
                    StorageWorkload storageWorkload = new StorageWorkload();
                    storageWorkload.setAppName(AppUtils.getAppName(deployment));
                    storageWorkload.setNamespaceName(storage.getNamespaceName());
                    storageWorkload.setStorageId(storage.getId());
                    storageWorkload.setStorageName(storage.getStorageName());
                    storageWorkload.setWorkloadName(deployment.getMetadata().getName());
                    storageWorkload.setWorkloadType(WorkloadType.Deployment);
                    return storageWorkload;
                })
                .collect(Collectors.toList());

        List<StatefulSet> relatedStatefulSets = getRelatedStatefulSets(namespaceName, storage);

        List<StorageWorkload> storageStatefulSetList = relatedStatefulSets.stream()
                .map(statefulSet -> {
                    StorageWorkload storageWorkload = new StorageWorkload();
                    storageWorkload.setAppName(AppUtils.getAppName(statefulSet));
                    storageWorkload.setNamespaceName(storage.getNamespaceName());
                    storageWorkload.setStorageId(storage.getId());
                    storageWorkload.setStorageName(storage.getStorageName());
                    storageWorkload.setWorkloadName(statefulSet.getMetadata().getName());
                    storageWorkload.setWorkloadType(WorkloadType.StatefulSet);
                    return storageWorkload;
                })
                .collect(Collectors.toList());

        List<StorageWorkload> all = new ArrayList<>();

        all.addAll(storageDeploymentList);
        all.addAll(storageStatefulSetList);

        return all;
    }

    private List<Deployment> getRelatedDeployments(String namespaceName, Storage storage) {
        List<Deployment> deploymentList = clientManager.getClient()
                .extensions()
                .deployments()
                .inNamespace(namespaceName)
                .list()
                .getItems();

        return Optional.ofNullable(deploymentList)
                .orElseGet(ArrayList::new)
                .stream()
                .filter(deployment -> isDeploymentUseStorage(deployment, storage))
                .collect(Collectors.toList());
    }

    private boolean isDeploymentUseStorage(Deployment deployment, Storage storage) {
        List<Volume> volumes = deployment.getSpec().getTemplate().getSpec().getVolumes();
        return Optional.ofNullable(volumes)
                .orElseGet(ArrayList::new)
                .stream()
                .filter(volume -> volume.getPersistentVolumeClaim() != null)
                .anyMatch(volume -> storage.getPvcName().equals(volume.getPersistentVolumeClaim().getClaimName()));
    }

    private List<StatefulSet> getRelatedStatefulSets(String namespaceName, Storage storage) {
        List<StatefulSet> statefulSetList = clientManager.getClient()
                .apps()
                .statefulSets()
                .inNamespace(namespaceName)
                .list()
                .getItems();

        return Optional.ofNullable(statefulSetList)
                .orElseGet(ArrayList::new)
                .stream()
                .filter(statefulSet -> isStatefulSetUseStorage(statefulSet, storage))
                .collect(Collectors.toList());
    }

    private boolean isStatefulSetUseStorage(StatefulSet statefulSet, Storage storage) {
        List<Volume> volumes = statefulSet.getSpec().getTemplate().getSpec().getVolumes();
        return Optional.ofNullable(volumes)
                .orElseGet(ArrayList::new)
                .stream()
                .filter(volume -> volume.getPersistentVolumeClaim() != null)
                .anyMatch(volume -> storage.getPvcName().equals(volume.getPersistentVolumeClaim().getClaimName()));
    }


    @Override
    protected void doFinalCheck(String clusterName, String namespaceName, Storage resource) {
        Storage.Status status = check(clusterName, namespaceName, resource);
        if (status == Storage.Status.CREATE_PENDING) {
            resource.setStatus(Storage.Status.CREATE_FAILED);
            storageRepository.save(resource);
        }
    }

    @Override
    public void handleError(EventErrorMessage message) {
        Object payload = message.getPayload();
        if (payload instanceof Storage) {
            Storage storage = storageRepository.findOne(((Storage) payload).getId());
            storage.setStatus(Storage.Status.CREATE_FAILED);
            storageRepository.save(storage);
        }
    }

    @Override
    public boolean shouldHandleError(EventErrorMessage message) {
        EventSourceType sourceType = message.getEventSourceType();
        if (sourceType != EventSourceType.STORAGE_CREATE) {
            return false;
        }

        Storage storage = storageRepository.findByNamespaceNameAndStorageName(message.getNamespaceName(), message.getEventSourceName());
        return storage != null && isRunning(message.getClusterName(), message.getNamespaceName(), storage);
    }

    @Override
    public EventSourceType getEventSourceType() {
        return EventSourceType.STORAGE_CREATE;
    }

    private boolean isCreateFinished(EventSource eventSource, Storage storage) {
        List<Event> events = findEvents(eventSource);

        if (!isPvcCreated(events) ||
                !isPvCreated(events)) {
            return false;
        }

        switch (storage.getStorageType()) {
            case RBD:
                return isCephImageCreated(events);
            case NFS:
            case EFS:
                return isNfsSubDirCreated(events);
            case CephFS:
                return isCephFsSubDirCreated(events);
            default:
                return true;
        }
    }

    private boolean isPvCreated(List<Event> events) {
        for (Event event : events) {
            if (event.getEventName() == EventName.CREATE_PV) {
                return true;
            }
        }
        return false;
    }

    private boolean isPvcCreated(List<Event> events) {
        for (Event event : events) {
            if (event.getEventName() == EventName.CREATE_PVC) {
                return true;
            }
        }
        return false;
    }

    private boolean isCephImageCreated(List<Event> events) {
        for (Event event : events) {
            if (event.getEventName() == EventName.CREATE_CEPH_IMAGE) {
                return true;
            }
        }
        return false;
    }

    private boolean isNfsSubDirCreated(List<Event> events) {
        for (Event event : events) {
            if (event.getEventName() == EventName.CREATE_NFS_SUB_DIR) {
                return true;
            }
        }
        return false;
    }

    private boolean isCephFsSubDirCreated(List<Event> events) {
        for (Event event : events) {
            if (event.getEventName() == EventName.CREATE_CEPH_FS_SUB_DIR) {
                return true;
            }
        }
        return false;
    }
}
