package cc.backend.kubernetes.storage.checker;

import cc.backend.common.GsonFactory;
import cc.backend.event.AbstractRedisStatusChecker;
import cc.backend.event.EventErrorMessage;
import cc.backend.event.data.Event;
import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceType;
import cc.backend.kubernetes.storage.HostPathStorageManager;
import cc.backend.kubernetes.storage.StorageRepository;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageUpdateRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import java.util.List;

import static cc.backend.kubernetes.Constants.REDIS_STORAGE_UPDATE_STATUS_KEY_PREFIX;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/10/12.
 */
@Component
public class StorageUpdateStatusChecker extends AbstractRedisStatusChecker<Storage, Storage.Status> {
    @Inject
    private StorageRepository storageRepository;

    @Inject
    private HostPathStorageManager hostPathStorageManager;

    @Override
    public String composeRedisKey(String clusterName, String namespaceName, Storage storage) {
        return REDIS_STORAGE_UPDATE_STATUS_KEY_PREFIX + clusterName + "." + namespaceName + "." + storage.getId();
    }

    @Override
    public long getStatusCheckPeriodMillis() {
        return properties.getStatusCheck().getPeriodMillis().getStorage();
    }

    @Override
    public Storage.Status check(String clusterName, String namespaceName, Storage storage) {
        storage = storageRepository.findOne(storage.getId());

        if (storage == null) {
            return null;
        }

        Storage.Status status = storage.getStatus();
        if (status != Storage.Status.UPDATE_PENDING) {
            return status;
        }

        EventSource eventSource = findEventSource(namespaceName, getEventSourceType(), storage.getStorageName());
        List<Event> updateEvents = findEvents(eventSource);
        StorageUpdateRequest request = GsonFactory.getGson().
                fromJson(eventSource.getPayloadJson(), StorageUpdateRequest.class);

        if (!isUpdateFinished(updateEvents, request, storage)) {
            return status;
        }

        storage.setStatus(Storage.Status.UPDATE_SUCCESS);
        if (request.getAmountBytes() != null) {
            storage.setAmountBytes(request.getAmountBytes());
        }

        if (request.isPersisted() != null) {
            storage.setPersisted(request.isPersisted());
        }

        if (request.isUnshared() != null) {
            storage.setUnshared(request.isUnshared());
        }

        if (request.isReadOnly() != null) {
            storage.setReadOnly(request.isReadOnly());
        }

        storageRepository.save(storage);
        cancel(clusterName, namespaceName, storage);
        return Storage.Status.UPDATE_SUCCESS;
    }

    @Override
    protected void doFinalCheck(String clusterName, String namespaceName, Storage resource) {
        Storage.Status status = check(clusterName, namespaceName, resource);
        if (status == Storage.Status.UPDATE_PENDING) {
            resource.setStatus(Storage.Status.UPDATE_FAILED);
            storageRepository.save(resource);
        }
    }

    @Override
    public void handleError(EventErrorMessage message) {
        Object payload = message.getPayload();
        if (payload instanceof Storage) {
            Storage storage = storageRepository.findOne(((Storage) payload).getId());
            storage.setStatus(Storage.Status.UPDATE_FAILED);
            storageRepository.save(storage);
        }
    }

    @Override
    public boolean shouldHandleError(EventErrorMessage message) {
        EventSourceType sourceType = message.getEventSourceType();
        if (sourceType != EventSourceType.STORAGE_UPDATE) {
            return false;
        }

        Storage storage = storageRepository.findByNamespaceNameAndStorageName(message.getNamespaceName(), message.getEventSourceName());
        return storage != null && isRunning(message.getClusterName(), message.getNamespaceName(), storage);
    }

    @Override
    public EventSourceType getEventSourceType() {
        return EventSourceType.STORAGE_UPDATE;
    }

    private boolean isUpdateFinished(List<Event> updateEvents, StorageUpdateRequest request, Storage storage) {
        switch (storage.getStorageType()) {
            case RBD:
                if (request.getAmountBytes() != null) {
                    return isCephImageUpdated(updateEvents) && isPvUpdated(updateEvents);
                } else if (request.isReadOnly() != null) {
                    return isPvUpdated(updateEvents);
                }
            case CephFS:
            case NFS:
            case EBS:
            case EFS:
                if (request.getAmountBytes() != null || request.isReadOnly() != null) {
                    return isPvUpdated(updateEvents);
                }
            case HostPath:
                if (request.getAmountBytes() != null) {
                    return isHostPathCapacityUpdated(storage) && isPvUpdated(updateEvents);
                } else if (request.isUnshared() != null || request.isPersisted() != null) {
                    return isPvUpdated(updateEvents);
                }
            default:
                return true;
        }
    }

    private boolean isPvUpdated(List<Event> updateEvents) {
        for (Event updateEvent : updateEvents) {
            if (updateEvent.getEventName() == EventName.UPDATE_PV) {
                return true;
            }
        }
        return false;
    }

    /*private boolean isPvcUpdated(List<Event> updateEvents) {
        for (Event deleteEvent : deleteEvents) {
            if (deleteEvent.getEventName() == EventName.UPDATE_PVC) {
                return true;
            }
        }
        return false;
    }*/

    private boolean isCephImageUpdated(List<Event> updateEvents) {
        for (Event updateEvent : updateEvents) {
            if (updateEvent.getEventName() == EventName.UPDATE_CEPH_IMAGE) {
                return true;
            }
        }
        return false;
    }

    /**
     * check if host path pv has been updated successfully
     * <p>
     * enn has changed the k8s HostPath pv logic
     * use bytes in annotations for a pv's real size
     * use state in annotations for  a pv's size is updated or not
     * <p>
     * k8s will take about 20s to set the state
     * <p>
     * TODO k8s will not set state correctly when pv is not used
     *
     * @param storage the storage for check
     * @return is updated
     */
    private boolean isHostPathCapacityUpdated(Storage storage) {
        String state = hostPathStorageManager.getUpdateCapacityState(storage.getPvName());
        if (StringUtils.isEmpty(state) || state.startsWith("ok")) {
            return true;
        } else if (state.startsWith("err") || state.startsWith("pending")) {
            //FIXME should set to false, wait k8s team to fix the issue
            return true;
        }

        return true;
    }
}
