package cc.backend.kubernetes.storage;

import cc.backend.RabbitConfig;
import cc.backend.audit.request.EnnContext;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceRepository;
import cc.backend.event.data.EventSourceType;
import cc.backend.kubernetes.storage.checker.StorageCreationStatusChecker;
import cc.backend.kubernetes.storage.checker.StorageDeletionStatusChecker;
import cc.backend.kubernetes.storage.checker.StorageUpdateStatusChecker;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageUpdateRequest;
import cc.backend.kubernetes.storage.messages.CephfsSubDirectoryCreateMessage;
import cc.backend.kubernetes.storage.messages.CephfsSubDirectoryCreatedMessage;
import cc.backend.kubernetes.storage.messages.CephfsSubDirectoryDeleteMessage;
import cc.backend.kubernetes.storage.messages.CephfsSubDirectoryDeletedMessage;
import cc.backend.kubernetes.storage.processor.CephfsSubDirProcessor;
import cc.backend.kubernetes.storage.processor.PvProcessor;
import cc.backend.kubernetes.storage.processor.PvcProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;

import javax.inject.Inject;
import javax.inject.Named;

import static cc.backend.RabbitConfig.CEPHFS_SUB_DIRECTORY_CREATE_Q;
import static cc.backend.RabbitConfig.CEPHFS_SUB_DIRECTORY_DELETE_Q;

/**
 * @author wangchunyang@gmail.com
 */
@Named
public class CephfsStorageManager {
    private static final Logger logger = LoggerFactory.getLogger(CephfsStorageManager.class);

    @Inject
    private RabbitMessagingTemplate messagingTemplate;

    @Inject
    private PvProcessor pvProcessor;

    @Inject
    private PvcProcessor pvcProcessor;

    @Inject
    private StorageCreationStatusChecker storageCreationStatusChecker;

    @Inject
    private StorageUpdateStatusChecker storageUpdateStatusChecker;

    @Inject
    private StorageDeletionStatusChecker storageDeletionStatusChecker;

    @Inject
    private StorageRepository storageRepository;

    @Inject
    private EventSourceRepository eventSourceRepository;

    @Inject
    private CephfsSubDirProcessor cephfsSubDirProcessor;

    public void createStorage(EventSource<Storage> eventSource) {
        Storage storage = eventSource.getPayload();

        pvcProcessor.create(eventSource);
        sendCreateStorageMessage(storage);
    }

    private void sendCreateStorageMessage(Storage storage) {
        CephfsSubDirectoryCreateMessage message = new CephfsSubDirectoryCreateMessage();
        message.setRequestId(EnnContext.getRequestId());
        message.setUserId(EnnContext.getUserId());
        message.setClusterName(EnnContext.getClusterName());
        message.setNamespaceName(storage.getNamespaceName());
        message.setStorageName(storage.getStorageName());
        messagingTemplate.convertAndSend(CEPHFS_SUB_DIRECTORY_CREATE_Q, message);
    }

    @RabbitListener(queues = RabbitConfig.CEPHFS_SUB_DIRECTORY_CREATED_Q)
    public void onCephFsSubDirCreated(CephfsSubDirectoryCreatedMessage message) {
        EnnContext.setContext(message);

        String namespaceName = message.getNamespaceName();
        logger.info("RabbitMQ: received CephfsSubDirectoryCreatedMessage namespace={}, storage={}",
                namespaceName, message.getStorageName());

        Storage storage = storageRepository.findByNamespaceNameAndStorageName(namespaceName, message.getStorageName());

        if (storage == null || storage.getStatus() != Storage.Status.CREATE_PENDING) {
            return;
        }

        EventSource<Storage> eventSource = getStorageCreateEventSource(namespaceName,
                message.getStorageName(), storage);
        if (eventSource.getRequestId().equals(message.getRequestId())) {
            cephfsSubDirProcessor.create(eventSource);
            storageCreationStatusChecker.check(EnnContext.getClusterName(), namespaceName, storage);
        }
    }

    private <T> EventSource<T> getStorageCreateEventSource(String namespaceName,
                                                           String storageName,
                                                           T payload) {
        EventSource<T> eventSource = eventSourceRepository
                .findTopByNamespaceNameAndSourceTypeAndSourceNameOrderByIdDesc(namespaceName,
                        EventSourceType.STORAGE_CREATE, storageName);
        eventSource.setPayload(payload);
        return eventSource;
    }

    public void updateStorage(EventSource<StorageUpdateRequest> eventSource, Storage storage) {
        pvProcessor.update(eventSource);
        storageUpdateStatusChecker.check(EnnContext.getClusterName(), eventSource.getNamespaceName(), storage);
    }

    public void deleteStorage(EventSource<Storage> eventSource) {
        pvcProcessor.delete(eventSource);
        sendDirectoryDeleteMessage(eventSource.getPayload());
    }

    public void sendDirectoryDeleteMessage(Storage storage) {
        CephfsSubDirectoryDeleteMessage message = new CephfsSubDirectoryDeleteMessage();
        message.setClusterName(EnnContext.getClusterName());
        message.setRequestId(EnnContext.getRequestId());
        message.setUserId(EnnContext.getUserId());
        message.setNamespaceName(storage.getNamespaceName());
        message.setStorageName(storage.getStorageName());
        messagingTemplate.convertAndSend(CEPHFS_SUB_DIRECTORY_DELETE_Q, message);
    }

    @RabbitListener(queues = RabbitConfig.CEPHFS_SUB_DIRECTORY_DELETED_Q)
    public void onCephFsSubDirDeleted(CephfsSubDirectoryDeletedMessage message) {
        EnnContext.setContext(message);

        String namespaceName = message.getNamespaceName();
        logger.info("RabbitMQ: received CephfsSubDirectoryDeletedMessage namespace={}, storage={}",
                namespaceName, message.getStorageName());

        Storage storage = storageRepository.findByNamespaceNameAndStorageName(namespaceName, message.getStorageName());

        if (storage == null || storage.getStatus() != Storage.Status.DELETE_PENDING) {
            return;
        }

        EventSource<Storage> eventSource = getStorageDeleteEventSource(namespaceName,
                message.getStorageName(), storage);
        if (eventSource.getRequestId().equals(message.getRequestId())) {
            cephfsSubDirProcessor.delete(eventSource);
            storageDeletionStatusChecker.check(message.getClusterName(), namespaceName, storage);
        }
    }

    private <T> EventSource<T> getStorageDeleteEventSource(String namespaceName,
                                                           String storageName,
                                                           T payload) {
        EventSource<T> eventSource = eventSourceRepository
                .findTopByNamespaceNameAndSourceTypeAndSourceNameOrderByIdDesc(namespaceName,
                        EventSourceType.STORAGE_DELETE, storageName);
        eventSource.setPayload(payload);
        return eventSource;
    }
}
