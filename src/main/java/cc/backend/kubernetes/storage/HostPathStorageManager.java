package cc.backend.kubernetes.storage;

import cc.backend.audit.operation.OperationAuditMessage;
import cc.backend.audit.operation.OperationAuditMessageProducer;
import cc.backend.audit.operation.OperationType;
import cc.backend.audit.operation.ResourceType;
import cc.backend.audit.request.EnnContext;
import cc.backend.event.data.EventSource;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.storage.checker.StorageCreationStatusChecker;
import cc.backend.kubernetes.storage.checker.StorageDeletionStatusChecker;
import cc.backend.kubernetes.storage.checker.StorageUpdateStatusChecker;
import cc.backend.kubernetes.storage.domain.HostPathUsage;
import cc.backend.kubernetes.storage.domain.HostPathUsage.MountInfo;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageUpdateRequest;
import cc.backend.kubernetes.storage.processor.PvProcessor;
import cc.backend.kubernetes.storage.processor.PvcProcessor;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Stopwatch;
import io.fabric8.kubernetes.api.model.PersistentVolume;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static cc.backend.kubernetes.Constants.HOSTPATH_PV_STATE_ANNOTATION;
import static cc.backend.kubernetes.Constants.HOSTPATH_PV_USAGE_ANNOTATION;

/**
 * @author wangchunyang@gmail.com
 */
@Named
public class HostPathStorageManager {
    private static final Logger logger = LoggerFactory.getLogger(HostPathStorageManager.class);
    @Inject
    private KubernetesClientManager kubernetesClientManager;

    @Inject
    private OperationAuditMessageProducer operationAuditMessageProducer;

    @Inject
    private PvcProcessor pvcProcessor;

    @Inject
    private PvProcessor pvProcessor;

    @Inject
    private StorageCreationStatusChecker storageCreationStatusChecker;

    @Inject
    private StorageDeletionStatusChecker storageDeletionStatusChecker;

    @Inject
    private StorageUpdateStatusChecker storageUpdateStatusChecker;

    public void createStorage(EventSource<Storage> eventSource) {
        Storage storage = eventSource.getPayload();

        pvcProcessor.create(eventSource);
        storageCreationStatusChecker.check(EnnContext.getClusterName(), eventSource.getNamespaceName(), storage);
    }

    public void updateStorage(EventSource<StorageUpdateRequest> eventSource, Storage storage) {
        pvProcessor.update(eventSource);
        storageUpdateStatusChecker.check(EnnContext.getClusterName(), eventSource.getNamespaceName(), storage);
    }

    public void deleteStorage(EventSource<Storage> eventSource) {
        pvcProcessor.delete(eventSource);
        storageDeletionStatusChecker.check(EnnContext.getClusterName(), eventSource.getNamespaceName(), eventSource.getPayload());
    }

    /**
     * pvc size can not edit since create!
     *
     * @param storage storage object
     */
    @Deprecated
    public void updatePvc(Storage storage) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        kubernetesClientManager.getClient().persistentVolumeClaims()
                .inNamespace(storage.getNamespaceName())
                .withName(storage.getPvcName())
                .edit()
                .editSpec()
                .editResources()
                .withRequests(storage.capacity())
                .endResources()
                .endSpec()
                .done();

        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .userId(EnnContext.getUserId())
                .requestId(EnnContext.getRequestId())
                .elapsed(elapsed)
                .namespaceName(storage.getNamespaceName())
                .operationType(OperationType.UPDATE)
                .resourceType(ResourceType.STORAGE_HOSTPATH_PVC)
                .resourceName(storage.getPvcName())
                .extra("amountBytes", storage.getAmountBytes())
                .build();
        operationAuditMessageProducer.send(message);
    }

    public List<Long> getHostpathPvUsedBytesList(String pvName) {
        PersistentVolume pv = kubernetesClientManager.getClient().persistentVolumes().withName(pvName).get();
        return getHostpathPvUsedBytesList(pv);
    }

    public List<Long> getHostpathPvUsedBytesList(PersistentVolume pv) {
        List<Long> usageBytesList = new ArrayList<>();

        List<HostPathUsage> hostPathUsageList = getHostPathPvUsage(pv);
        for (HostPathUsage hostPathUsage : hostPathUsageList) {
            List<MountInfo> mountInfos = hostPathUsage.getMountInfos();
            if (mountInfos != null) {
                for (MountInfo mountInfo : mountInfos) {
                    usageBytesList.add(mountInfo.getVolumeCurrentSize());
                }
            }
        }

        return usageBytesList;
    }

    public long getHostpathPvUsedBytes(String pvName) {
        PersistentVolume pv = kubernetesClientManager.getClient().persistentVolumes().withName(pvName).get();
        return getHostpathPvUsedBytes(pv);
    }

    public long getHostpathPvUsedBytes(PersistentVolume pv) {
        long usedBytes = 0;

        List<HostPathUsage> hostPathPvUsage = getHostPathPvUsage(pv);
        for (HostPathUsage hostPathUsage : hostPathPvUsage) {
            List<MountInfo> mountInfoList = hostPathUsage.getMountInfos();
            if (mountInfoList != null) {
                for (MountInfo mountInfo : mountInfoList) {
                    usedBytes += mountInfo.getVolumeCurrentSize();
                }
            }
        }

        return usedBytes;
    }

    public List<MountInfo> getHostPathMountInfo(String pvName) {
        PersistentVolume pv = kubernetesClientManager.getClient().persistentVolumes().withName(pvName).get();
        List<HostPathUsage> usageList = getHostPathPvUsage(pv);
        List<MountInfo> mountInfoList = new ArrayList<>();

        for (HostPathUsage usage : usageList) {
            if (usage.getMountInfos() != null) {
                mountInfoList.addAll(usage.getMountInfos());
            }
        }

        return mountInfoList;
    }

    public List<HostPathUsage> getHostPathPvUsage(PersistentVolume pv) {
        List<HostPathUsage> usageList = new ArrayList<>();
        if (pv == null || pv.getMetadata().getAnnotations() == null) {
            return usageList;
        }
        String usageStr = pv
                .getMetadata()
                .getAnnotations()
                //该值记录Node当前可用磁盘情况，由kubelet模块在启动的时候自动检测
                .get(HOSTPATH_PV_USAGE_ANNOTATION);
        if (!StringUtils.isEmpty(usageStr)) {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            try {
                usageList = mapper.readValue(usageStr, new TypeReference<List<HostPathUsage>>() {
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return usageList;
    }

    public String getUpdateCapacityState(String pvName) {
        return kubernetesClientManager.getClient().persistentVolumes().withName(pvName).get()
                .getMetadata()
                .getAnnotations()
                //该值记录Node当前可用磁盘情况，由kubelet模块在启动的时候自动检测
                .get(HOSTPATH_PV_STATE_ANNOTATION);
    }
}
