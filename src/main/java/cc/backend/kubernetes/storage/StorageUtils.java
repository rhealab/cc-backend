package cc.backend.kubernetes.storage;

import cc.backend.kubernetes.storage.domain.AccessModeType;
import cc.backend.kubernetes.storage.domain.StorageType;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/7/10.
 */
public class StorageUtils {
    public static String pvName(String namespaceName, String storageName) {
        return String.format("%s-%s-pv", namespaceName, storageName);
    }

    /**
     * change from {namespace}-{storage}-pvc to storage for StatefulSet and better experience
     *
     * @param namespaceName the namespace name
     * @param storageName   the storage name
     * @return the pvc name
     */
    public static String pvcName(String namespaceName, String storageName) {
        return storageName;
    }

    public static String pvHostPath(String namespaceName, String storageName) {
        return String.format("/k8s_storage/%s/%s", namespaceName, storageName);
    }

    /**
     * currently, we made a agreement that ceph image name should same as storage name
     *
     * @param storageName the storage name
     * @return the ceph image name
     */
    public static String cephImageName(String storageName) {
        return storageName;
    }

    public static String cephFsDir(String namespaceName, String storageName) {
        return namespaceName + "/" + storageName;
    }


    public static String nfsDir(String namespaceName, String storageName) {
        return namespaceName + "/" + storageName;
    }

    public static String parseStorageNameFromPvcName(String namespaceName, String pvcName) {
        String prefix = namespaceName + "-";
        if (pvcName.startsWith(prefix) && pvcName.endsWith("-pvc")) {
            return pvcName.substring(prefix.length(), pvcName.length() - 4);
        }

        return "";
    }

    public static String parseStorageNameFromImageName(String cephImageName) {
        return cephImageName;
    }

    public static String parseStorageNameFromPvName(String namespaceName, String pvName) {
        String prefix = namespaceName + "-";
        if (pvName.startsWith(prefix) && pvName.endsWith("-pv")) {
            return pvName.substring(prefix.length(), pvName.length() - 3);
        }

        return "";
    }

    public static AccessModeType composeAccessMode(AccessModeType accessModeType, StorageType storageType) {
        if (accessModeType == null) {
            if (storageType == StorageType.EBS
                    || storageType == StorageType.RBD) {
                return AccessModeType.ReadWriteOnce;
            } else {
                return AccessModeType.ReadWriteMany;
            }
        } else {
            return accessModeType;
        }
    }
}
