package cc.backend.kubernetes.storage;

import cc.backend.event.data.EventSource;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageCreateRequest;
import cc.backend.kubernetes.storage.domain.StorageType;
import cc.backend.kubernetes.storage.domain.StorageUpdateRequest;
import com.google.common.collect.ImmutableMap;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * @author wangchunyang@gmail.com
 */
@Component
@Path("kubernetes/namespaces/{namespaceName}/storage")
@Api(value = "Storage", description = "Info about statefulsets.", produces = "application/json")
@Produces(MediaType.APPLICATION_JSON)
public class StorageEndpoint {

    private final StorageManagement management;

    private final StorageValidator validator;

    @Inject
    public StorageEndpoint(StorageManagement management, StorageValidator validator) {
        this.management = management;
        this.validator = validator;
    }

    @GET
    @ApiOperation(value = "List storages in namespace.", responseContainer = "List", response = StorageDto.class)
    public Response getStorageList(@PathParam("namespaceName") String namespaceName) {
        List<Storage> storageList = management.getStorageList(namespaceName);
        return Response.ok(management.composeStorageDtoList(namespaceName, storageList)).build();
    }

    @GET
    @Path("{storageId}")
    @ApiOperation(value = "Get storage info by id.", response = StorageDto.class)
    public Response getStorageById(@PathParam("namespaceName") String namespaceName,
                                   @PathParam("storageId") long storageId) {
        Storage storage = management.getStorage(namespaceName, storageId);
        Map<String, List<String>> pvcPodsMap = management.getPvcPodsMap(namespaceName);
        return Response.ok(management.composeStorageDto(storage, pvcPodsMap)).build();
    }

    @GET
    @Path("name/{storageName}")
    @ApiOperation(value = "Get storage info by name.", response = StorageDto.class)
    public Response getStorageByName(@PathParam("namespaceName") String namespaceName,
                                     @PathParam("storageName") String storageName) {
        Storage storage = management.getStorageByName(namespaceName, storageName);
        Map<String, List<String>> pvcPodsMap = management.getPvcPodsMap(namespaceName);
        return Response.ok(management.composeStorageDto(storage, pvcPodsMap)).build();
    }

    @POST
    @ApiOperation(value = "Create storage.")
    public Response createStorage(@PathParam("namespaceName") String namespaceName,
                                  @Valid StorageCreateRequest request) {
        request.setNamespaceName(namespaceName);

        Storage storage = validator.validateStorageForCreate(request);

        EventSource<Storage> eventSource = management.initStorageForCreate(storage);
        management.createStorage(eventSource);

        return Response.ok().entity(ImmutableMap.of("id", eventSource.getPayload().getId(),
                "namespaceName", namespaceName,
                "storageName", request.getStorageName())).build();
    }

    @PUT
    @Path("{id}")
    @ApiOperation(value = "Update storage by id.")
    public Response updateStorage(@PathParam("id") long id,
                                  @PathParam("namespaceName") String namespaceName,
                                  @Valid StorageUpdateRequest request) {
        request.setId(id);
        request.setNamespaceName(namespaceName);

        Storage storage = validator.validateStorageForUpdateById(request);
        EventSource<StorageUpdateRequest> eventSource = management.initStorageForUpdate(storage, request);
        management.updateStorage(eventSource, storage);
        return Response.ok().build();
    }

    @PUT
    @Path("name/{storageName}")
    @ApiOperation(value = "Update storage by name.")
    public Response updateStorageByName(@PathParam("storageName") String storageName,
                                        @PathParam("namespaceName") String namespaceName,
                                        @Valid StorageUpdateRequest request) {
        request.setName(storageName);
        request.setNamespaceName(namespaceName);
        Storage storage = validator.validateStorageForUpdateByName(request);

        EventSource<StorageUpdateRequest> eventSource = management.initStorageForUpdate(storage, request);
        management.updateStorage(eventSource, storage);
        return Response.ok().build();
    }

    @GET
    @Path("{id}/updateState")
    @ApiOperation(value = "Get storage update state.", responseContainer = "Map", response = String.class)
    public Response getUpdateState(@PathParam("id") long id, @PathParam("namespaceName") String namespaceName) {
        String state = management.getCapacityUpdateState(id, namespaceName);
        return Response.ok(ImmutableMap.of("status", state)).build();
    }

    @GET
    @Path("{id}/file_operable")
    @ApiOperation(value = "Is the storage support file operation, currently.", responseContainer = "Map", response = String.class)
    public Response isFileOperable(@PathParam("id") long id,
                                   @PathParam("namespaceName") String namespaceName,
                                   @QueryParam("podName") String podName) {
        Storage storage = validator.ifPresent(namespaceName, id);

        boolean isFileOperable = management.isStorageFileOperable(storage, podName);

        return Response.ok(ImmutableMap.of("fileOperable", String.valueOf(isFileOperable))).build();
    }

    @DELETE
    @Path("{storageId}")
    @ApiOperation(value = "Delete specified storage by id.")
    public Response deleteStorageById(@PathParam("namespaceName") String namespaceName,
                                      @PathParam("storageId") long storageId) {
        Storage storage = management.getStorage(namespaceName, storageId);
        validator.validateForDelete(storage);
        if (storage.getStorageType() == StorageType.StorageClass) {
            management.deleteStorageClass(storage);
        } else {
            EventSource<Storage> eventSource = management.initStorageForDelete(storage);
            management.deleteStorage(eventSource);
        }
        return Response.ok().build();
    }

    @DELETE
    @Path("name/{storageName}")
    @ApiOperation(value = "Delete specified storage by name.")
    public Response deleteStorageByName(@PathParam("namespaceName") String namespaceName,
                                        @PathParam("storageName") String storageName) {
        Storage storage = management.getStorageByName(namespaceName, storageName);
        validator.validateForDelete(storage);
        if (storage.getStorageType() == StorageType.StorageClass) {
            management.deleteStorageClass(storage);
        } else {
            EventSource<Storage> eventSource = management.initStorageForDelete(storage);
            management.deleteStorage(eventSource);
        }
        return Response.ok().build();
    }
}
