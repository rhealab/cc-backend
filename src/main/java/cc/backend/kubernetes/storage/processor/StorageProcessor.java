package cc.backend.kubernetes.storage.processor;

import cc.backend.event.data.EventSource;
import cc.backend.event.processor.AbstractProcessor;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageUpdateRequest;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * @author wangjinxin
 * Created by NormanWang06@gmail.com (jinxin) on 2017/6/2.
 */
@Component
public class StorageProcessor extends AbstractProcessor<Storage, StorageUpdateRequest, Storage> {

    @Inject
    private PvcProcessor pvcProcessor;

    @Inject
    private PvProcessor pvProcessor;

    @Override
    public void create(EventSource<Storage> eventSource) {
        pvcProcessor.create(eventSource);
    }

    @Override
    public void update(EventSource<StorageUpdateRequest> eventSource) {
        //TODO pvc can not update, how to change size
        pvProcessor.update(eventSource);
    }

    @Override
    public void delete(EventSource<Storage> eventSource) {
        pvcProcessor.delete(eventSource);
    }
}
