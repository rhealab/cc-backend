package cc.backend.kubernetes.storage.processor;

import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;
import cc.backend.event.processor.AbstractProcessor;
import cc.backend.kubernetes.storage.StorageWorkloadRepository;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageType;
import cc.backend.kubernetes.storage.usage.entity.CephImageUsageRepository;
import cc.backend.kubernetes.storage.usage.entity.CephfsDirectoryUsageRepository;
import cc.backend.kubernetes.storage.usage.entity.NfsDirectoryUsageRepository;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/2.
 */
@Component
public class StorageRelatedProcessor extends AbstractProcessor<Void, Void, Storage> {

    @Inject
    private StorageWorkloadRepository storageWorkloadRepository;

    @Inject
    private CephImageUsageRepository cephImageUsageRepository;

    @Inject
    private CephfsDirectoryUsageRepository cephfsDirectoryUsageRepository;

    @Inject
    private NfsDirectoryUsageRepository nfsDirectoryUsageRepository;

    @Override
    protected void doDelete(EventSource<Storage> eventSource) {
        Storage storage = eventSource.getPayload();
        StorageType storageType = storage.getStorageType();
        if (storageType == StorageType.RBD || storageType == StorageType.StorageClass) {
            cephImageUsageRepository.deleteByNamespaceNameAndImageName(storage.getNamespaceName(), storage.getStorageName());
        } else if (storageType == StorageType.CephFS) {
            cephfsDirectoryUsageRepository.deleteByNamespaceNameAndDirectoryName(storage.getNamespaceName(), storage.cephFsDir());
        } else if (storageType == StorageType.NFS || storageType == StorageType.EFS) {
            nfsDirectoryUsageRepository.deleteByNamespaceNameAndDirectoryName(storage.getNamespaceName(), storage.nfsDir());
        }
        storageWorkloadRepository.deleteByStorageId(storage.getId());
    }

    @Override
    public boolean needToDelete(EventSource<Storage> eventSource) {
        return true;
    }

    @Override
    public EventName getDeleteEventName() {
        return EventName.DELETE_STORAGE_RELATED;
    }
}
