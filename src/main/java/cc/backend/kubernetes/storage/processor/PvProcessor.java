package cc.backend.kubernetes.storage.processor;

import cc.backend.EnnProperties;
import cc.backend.audit.operation.OperationAuditMessage;
import cc.backend.audit.operation.OperationAuditMessageProducer;
import cc.backend.audit.operation.OperationType;
import cc.backend.audit.operation.ResourceType;
import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceType;
import cc.backend.event.processor.AbstractProcessor;
import cc.backend.event.processor.IDeleteProcessor;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.namespaces.dto.NamespaceNaming;
import cc.backend.kubernetes.storage.HostPathStorageManager;
import cc.backend.kubernetes.storage.StorageRepository;
import cc.backend.kubernetes.storage.domain.*;
import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.lib.retcode.CcException;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.DoneablePersistentVolume;
import io.fabric8.kubernetes.api.model.LocalObjectReference;
import io.fabric8.kubernetes.api.model.PersistentVolumeFluent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static cc.backend.audit.operation.ResourceType.*;
import static cc.backend.kubernetes.Constants.*;
import static cc.backend.kubernetes.storage.domain.StorageType.HostPath;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/2.
 */
@Component
public class PvProcessor extends AbstractProcessor<Storage, StorageUpdateRequest, Storage> {
    private static final Logger logger = LoggerFactory.getLogger(PvProcessor.class);

    @Inject
    private StorageRelatedProcessor storageRelatedProcessor;

    @Inject
    private StorageRepository storageRepository;

    @Inject
    private OperationAuditMessageProducer operationAuditMessageProducer;

    @Inject
    private KubernetesClientManager kubernetesClientManager;

    @Inject
    private EnnProperties properties;

    @Inject
    private HostPathStorageManager hostPathStorageManager;

    @Override
    protected void doCreate(EventSource<Storage> eventSource) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        Storage storage = eventSource.getPayload();
        StorageType storageType = storage.getStorageType();

        switch (storageType) {
            case RBD:
                createRbdPv(storage);
                break;
            case CephFS:
                createCephFsPv(storage);
                break;
            case HostPath:
                createHostPathPv(storage);
                break;
            case NFS:
            case EFS:
                createNfsPv(storage);
                break;
            case EBS:
                createEbsPv(storage);
                break;
            default:
        }

        sendAuditMessage(stopwatch, OperationType.CREATE, storage);
    }

    /**
     * create a rbd pv by storage
     * currently default is xfs
     *
     * @param storage the storage
     */
    private void createRbdPv(Storage storage) {
        String namespaceName = storage.getNamespaceName();

        LocalObjectReference secretRef = new LocalObjectReference();
        secretRef.setName(getSecretName(namespaceName));

        kubernetesClientManager.getClient().persistentVolumes().createNew()
                .withNewMetadata()
                .withName(storage.getPvName())
                .withLabels(storage.labels())
                .endMetadata()
                .withNewSpec()
                .withCapacity(storage.capacity())
                .withAccessModes(storage.getAccessMode().name())
                .withNewRbd()
                .addToMonitors(properties.getCurrentCeph().getMonitorArray())
                .withFsType("xfs")
                .withImage(storage.cephImageName())
                .withPool(NamespaceNaming.cephPoolName(namespaceName))
                .withUser(NamespaceNaming.cephUserName(namespaceName))
                .withNewSecretRef(NamespaceNaming.cephSecretName(namespaceName))
                .withReadOnly(storage.isReadOnly())
                .endRbd()
                .endSpec()
                .done();
    }

    public void createCephFsPv(Storage storage) {
        String namespaceName = storage.getNamespaceName();

        LocalObjectReference secretRef = new LocalObjectReference();
        secretRef.setName(getSecretName(namespaceName));

        kubernetesClientManager.getClient()
                .persistentVolumes()
                .createNew()
                .withNewMetadata()
                .withName(storage.getPvName())
                .withLabels(storage.labels())
                .endMetadata()
                .withNewSpec()
                .withCapacity(storage.capacity())
                .withAccessModes(storage.getAccessMode().name())
                .withNewCephfs()
                .addToMonitors(properties.getCurrentCeph().getMonitorArray())
                .withPath(properties.getCurrentCeph().getDirPrefix() + storage.cephFsDir())
                .withUser(namespaceName)
                .withReadOnly(false)
                .withSecretRef(secretRef)
                .withReadOnly(storage.isReadOnly())
                .endCephfs()
                .endSpec()
                .done();
    }

    public void createHostPathPv(Storage storage) {
        Map<String, String> annotations = new HashMap<>(2);

        //(keep/none）被scheduler模块使用，
        // 当设为keep的时候,当该PV被某个Node所挂载之后,下次再有Pod要使用该PV时,只能被调度到这个Node。
        annotations.put(HOSTPATH_PV_PERSIST_ANNOTATION, storage.isPersisted() ? "keep" : "none");

        //为true表示:该PV为每个绑定的Pod在Node上单独创建一个私有目录
        annotations.put(HOSTPATH_PV_ONE_POD_ONE_STORAGE_ANNOTATION, String.valueOf(storage.isUnshared()));

        kubernetesClientManager.getClient()
                .persistentVolumes()
                .createNew()
                .withNewMetadata()
                .withName(storage.getPvName())
                .withLabels(storage.labels())
                .withAnnotations(annotations)
                .endMetadata()
                .withNewSpec()
                .withCapacity(storage.capacity())
                .withAccessModes(storage.getAccessMode().name())
                .withNewHostPath().withPath(storage.pvHostPath()).endHostPath()
                .endSpec()
                .done();
    }

    public void createNfsPv(Storage storage) {
        EnnProperties.Cluster.Nfs nfs = properties.getCurrentNfs();
        kubernetesClientManager.getClient()
                .persistentVolumes()
                .createNew()
                .withNewMetadata()
                .withName(storage.getPvName())
                .withLabels(storage.labels())
                .endMetadata()
                .withNewSpec()
                .withCapacity(storage.capacity())
                .withAccessModes(storage.getAccessMode().name())
                .withNewNfs()
                .withPath(nfs.getServerDir() + nfs.getDirPrefix() + storage.nfsDir())
                .withReadOnly(false)
                .withServer(nfs.getAddress())
                .withReadOnly(storage.isReadOnly())
                .endNfs()
                .endSpec()
                .done();

    }

    public void createEbsPv(Storage storage) {
        kubernetesClientManager.getClient()
                .persistentVolumes()
                .createNew()
                .withNewMetadata()
                .withName(storage.getPvName())
                .withLabels(storage.labels())
                .endMetadata()
                .withNewSpec()
                .withCapacity(storage.capacity())
                .withAccessModes(AccessModeType.ReadWriteOnce.name())
                .withNewAwsElasticBlockStore()
                .withFsType("xfs")
                .withReadOnly(storage.isReadOnly())
                .withVolumeID(storage.getEbsVolumeId())
                .endAwsElasticBlockStore()
                .endSpec()
                .done();

    }

    /**
     * get the secret name for ceph by namespace name
     *
     * @param namespaceName the namespace name
     * @return the secret name
     */
    public static String getSecretName(String namespaceName) {
        return namespaceName;
    }

    @Override
    protected void doUpdate(EventSource<StorageUpdateRequest> eventSource) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        Storage storage = storageRepository.findByNamespaceNameAndStorageName(eventSource.getNamespaceName(), eventSource.getSourceName());
        updatePv(storage, eventSource.getPayload());

        sendAuditMessage(stopwatch, OperationType.UPDATE, storage);
    }

    public void updatePv(Storage storage, StorageUpdateRequest request) {
        DoneablePersistentVolume editor = kubernetesClientManager.getClient()
                .persistentVolumes()
                .withName(storage.getPvName())
                .edit();
        if (request.getAmountBytes() != null) {
            editor.editSpec()
                    .withCapacity(StorageCapacity.capacity(request.getAmountBytes()))
                    .endSpec();
        }

        if (storage.getStorageType() == HostPath) {
            Map<String, String> annotations = new HashMap<>(4);

            if (request.getAmountBytes() != null && request.getAmountBytes() != storage.getAmountBytes()) {
                editor.editSpec()
                        .withCapacity(StorageCapacity.capacity(request.getAmountBytes()))
                        .endSpec();

                annotations.put(HOSTPATH_PV_CAPACITY_ANNOTATION, String.valueOf(request.getAmountBytes()));
                annotations.put(HOSTPATH_PV_STATE_ANNOTATION, "pending to " + request.getAmountBytes());
            }

            if (request.isPersisted() != null && request.isPersisted() != storage.isPersisted()) {
                annotations.put(HOSTPATH_PV_PERSIST_ANNOTATION, request.isPersisted() ? "keep" : "none");
            }

            if (request.isUnshared() != null && request.isUnshared() != storage.isUnshared()) {
                //when change keep from false to true, previous pod will still use their own storage
                //and the actual quota will not change, this is not correct
                //so only a pv has only 1 replicas or less can change this
                if (!request.isUnshared()) {
                    List<Long> list = hostPathStorageManager.getHostpathPvUsedBytesList(storage.getPvName());
                    if (list != null && list.size() > 1) {
                        throw new CcException(BackendReturnCodeNameConstants.HOSTPATH_STORAGE_UPDATE_UNSHARED_NOT_SUPPORT);
                    }
                }
                annotations.put(HOSTPATH_PV_ONE_POD_ONE_STORAGE_ANNOTATION, String.valueOf(request.isUnshared()));
            }

            if (!annotations.isEmpty()) {
                editor.editMetadata()
                        .addToAnnotations(annotations)
                        .endMetadata();
            }
        }

        if (request.isReadOnly() != null) {
            PersistentVolumeFluent.SpecNested<DoneablePersistentVolume> editSpec = editor.editSpec();
            switch (storage.getStorageType()) {
                case RBD:
                    editSpec.editRbd().withReadOnly(request.isReadOnly()).endRbd();
                    break;
                case CephFS:
                    editSpec.editCephfs().withReadOnly(request.isReadOnly()).endCephfs();
                    break;
                case NFS:
                case EFS:
                    editSpec.editNfs().withReadOnly(request.isReadOnly()).endNfs();
                    break;
                case EBS:
                    editSpec.editAwsElasticBlockStore().withReadOnly(request.isReadOnly()).endAwsElasticBlockStore();
                    break;
                default:
                    break;
            }
            editSpec.endSpec();
        }

        editor.done();
    }

    @Override
    protected void doDelete(EventSource<Storage> eventSource) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        Storage storage = eventSource.getPayload();
        logger.info("Delete PV - pvName={}, namespace={}", storage.getPvName(), storage.getNamespaceName());

        kubernetesClientManager.getClient()
                .persistentVolumes()
                .withName(storage.getPvName())
                .delete();

        sendAuditMessage(stopwatch, OperationType.DELETE, storage);
    }

    private void sendAuditMessage(Stopwatch stopwatch, OperationType operationType, Storage storage) {
        StorageType storageType = storage.getStorageType();
        ResourceType resourceType = null;
        switch (storageType) {
            case RBD:
                resourceType = STORAGE_RBD_PV;
                break;
            case CephFS:
                resourceType = STORAGE_CEPHFS_PV;
                break;
            case HostPath:
                resourceType = STORAGE_HOSTPATH_PV;
                break;
            case NFS:
            case EFS:
                resourceType = STORAGE_NFS_PV;
                break;
            case EBS:
                resourceType = STORAGE_EBS_PV;
                break;
            default:
                break;
        }

        if (resourceType != null) {
            operationAuditMessageProducer.send(new OperationAuditMessage.Builder()
                    .elapsed(stopwatch.stop().elapsed(TimeUnit.MILLISECONDS))
                    .namespaceName(storage.getNamespaceName())
                    .operationType(operationType)
                    .resourceType(resourceType)
                    .resourceName(storage.getPvName())
                    .extras(ImmutableMap.of("amountBytes", storage.getAmountBytes()))
                    .build());
        }
    }

    @Override
    public boolean needToDelete(EventSource<Storage> eventSource) {
        return isCreateEventExists(eventSource, EventSourceType.STORAGE_CREATE) || isPvExists(eventSource);
    }

    private boolean isPvExists(EventSource<Storage> eventSource) {
        Storage storage = eventSource.getPayload();
        return kubernetesClientManager.getClient()
                .persistentVolumes()
                .withName(storage.getPvName())
                .get() != null;
    }

    @Override
    public EventName getCreateEventName() {
        return EventName.CREATE_PV;
    }

    @Override
    public EventName getUpdateEventName() {
        return EventName.UPDATE_PV;
    }

    @Override
    public EventName getDeleteEventName() {
        return EventName.DELETE_PV;
    }

    @Override
    public IDeleteProcessor<Storage> getNextDeleteProcessor() {
        return storageRelatedProcessor;
    }
}
