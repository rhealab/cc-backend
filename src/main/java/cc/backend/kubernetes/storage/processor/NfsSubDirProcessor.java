package cc.backend.kubernetes.storage.processor;

import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceType;
import cc.backend.event.processor.AbstractProcessor;
import cc.backend.kubernetes.storage.domain.Storage;
import org.springframework.stereotype.Component;

/**
 * Created by NormanWang06@gmail.com (jinxin) on 2017/6/2.
 */
@Component
public class NfsSubDirProcessor extends AbstractProcessor<Storage, Storage, Storage> {

    @Override
    public boolean needToDelete(EventSource<Storage> eventSource) {
        return getEvent(eventSource.getNamespaceName(),
                EventSourceType.STORAGE_CREATE,
                eventSource.getSourceName(),
                getCreateEventName()) != null;
    }

    @Override
    public EventName getCreateEventName() {
        return EventName.CREATE_NFS_SUB_DIR;
    }

    @Override
    public EventName getDeleteEventName() {
        return EventName.DELETE_NFS_SUB_DIR;
    }
}
