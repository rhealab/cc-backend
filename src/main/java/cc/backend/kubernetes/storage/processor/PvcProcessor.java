package cc.backend.kubernetes.storage.processor;

import cc.backend.audit.operation.OperationAuditMessage;
import cc.backend.audit.operation.OperationAuditMessageProducer;
import cc.backend.audit.operation.OperationType;
import cc.backend.audit.operation.ResourceType;
import cc.backend.event.data.EventName;
import cc.backend.event.data.EventSource;
import cc.backend.event.data.EventSourceType;
import cc.backend.event.processor.AbstractProcessor;
import cc.backend.event.processor.ICreateProcessor;
import cc.backend.event.processor.IDeleteProcessor;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageType;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.concurrent.TimeUnit;

import static cc.backend.audit.operation.ResourceType.*;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/6/2.
 */
@Component
public class PvcProcessor extends AbstractProcessor<Storage, Storage, Storage> {
    private static final Logger logger = LoggerFactory.getLogger(PvcProcessor.class);

    @Inject
    private PvProcessor pvProcessor;

    @Inject
    private OperationAuditMessageProducer operationAuditMessageProducer;

    @Inject
    private KubernetesClientManager kubernetesClientManager;

    @Override
    protected void doCreate(EventSource<Storage> eventSource) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        Storage storage = eventSource.getPayload();
        createPvc(storage);
        sendAuditMessage(stopwatch, OperationType.CREATE, storage);
    }

    private void createPvc(Storage storage) {
        kubernetesClientManager.getClient()
                .persistentVolumeClaims()
                .inNamespace(storage.getNamespaceName())
                .createNew()
                .withNewMetadata()
                .withName(storage.getPvcName())
                .endMetadata()
                .withNewSpec()
                .withAccessModes(storage.getAccessMode().name())
                .withNewResources().withRequests(storage.capacity()).endResources()
                .withNewSelector().addToMatchLabels(storage.labels()).endSelector()
                .endSpec()
                .done();
    }

    @Override
    protected void doDelete(EventSource<Storage> eventSource) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        Storage storage = eventSource.getPayload();

        logger.info("Delete PVC - pvcName={}, namespace={}", storage.getPvcName(), storage.getNamespaceName());

        kubernetesClientManager.getClient()
                .persistentVolumeClaims()
                .inNamespace(storage.getNamespaceName())
                .withName(storage.getPvcName())
                .delete();

        sendAuditMessage(stopwatch, OperationType.DELETE, storage);
    }


    private void sendAuditMessage(Stopwatch stopwatch, OperationType operationType, Storage storage) {
        StorageType storageType = storage.getStorageType();
        ResourceType resourceType = null;
        switch (storageType) {
            case RBD:
                resourceType = STORAGE_RBD_PVC;
                break;
            case CephFS:
                resourceType = STORAGE_CEPHFS_PVC;
                break;
            case HostPath:
                resourceType = STORAGE_HOSTPATH_PVC;
                break;
            case NFS:
            case EFS:
                resourceType = STORAGE_NFS_PVC;
                break;
            case EBS:
                resourceType = STORAGE_EBS_PVC;
                break;
            default:
                break;
        }

        if (resourceType != null) {
            operationAuditMessageProducer.send(new OperationAuditMessage.Builder()
                    .elapsed(stopwatch.stop().elapsed(TimeUnit.MILLISECONDS))
                    .namespaceName(storage.getNamespaceName())
                    .operationType(operationType)
                    .resourceType(resourceType)
                    .resourceName(storage.getPvcName())
                    .extras(ImmutableMap.of("amountBytes", storage.getAmountBytes()))
                    .build());
        }
    }

    @Override
    public boolean needToDelete(EventSource<Storage> eventSource) {
        return isCreateEventExists(eventSource, EventSourceType.STORAGE_CREATE) || isPvcExists(eventSource);
    }

    private boolean isPvcExists(EventSource<Storage> eventSource) {
        Storage storage = eventSource.getPayload();
        return kubernetesClientManager.getClient()
                .persistentVolumeClaims()
                .inNamespace(eventSource.getNamespaceName())
                .withName(storage.getPvName())
                .get() != null;
    }

    @Override
    public EventName getCreateEventName() {
        return EventName.CREATE_PVC;
    }

    @Override
    public EventName getDeleteEventName() {
        return EventName.DELETE_PVC;
    }

    @Override
    public ICreateProcessor<Storage> getNextCreateProcessor() {
        return pvProcessor;
    }

    @Override
    public IDeleteProcessor<Storage> getNextDeleteProcessor() {
        return pvProcessor;
    }
}
