package cc.backend.kubernetes.storage;

import cc.backend.EnnProperties;
import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.apps.dto.StorageTopological;
import cc.backend.kubernetes.namespaces.services.NamespaceStatsManagement;
import cc.backend.kubernetes.namespaces.validator.NamespaceValidator;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageCreateRequest;
import cc.backend.kubernetes.storage.domain.StorageType;
import cc.backend.kubernetes.storage.domain.StorageUpdateRequest;
import cc.backend.kubernetes.storage.usage.entity.*;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.PersistentVolumeClaimVolumeSource;
import io.fabric8.kubernetes.api.model.Volume;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static cc.backend.kubernetes.Constants.EBS_HDD_MIN_BYTES;
import static cc.backend.kubernetes.storage.domain.StorageType.*;
import static com.google.common.collect.ImmutableMap.of;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/7/10.
 */
@Component
public class StorageValidator {
    private static final Logger logger = LoggerFactory.getLogger(StorageValidator.class);

    @Inject
    private StorageRepository storageRepository;

    @Inject
    private NamespaceValidator namespaceValidator;

    @Inject
    private NamespaceStatsManagement namespaceStatsManagement;

    @Inject
    private CephfsDirectoryUsageRepository cephfsDirectoryUsageRepository;

    @Inject
    private NfsDirectoryUsageRepository nfsDirectoryUsageRepository;

    @Inject
    private HostPathStorageManager hostPathStorageManager;

    @Inject
    private CephImageUsageRepository cephImageUsageRepository;

    @Inject
    private StorageWorkloadRepository storageWorkloadRepository;

    @Inject
    private EnnProperties properties;

    @Inject
    private KubernetesClientManager clientManager;

    public Storage validateStorageForCreate(StorageCreateRequest request) {
        Storage storage = Storage.newInstance(request);

        validateStorageShouldNotExist(request.getNamespaceName(), request.getStorageName());
        validateStorageParams(storage);
        validateStorageType(request.getNamespaceName(), storage);
        validateQuotaForCreate(request.getNamespaceName(), storage);

        return storage;
    }

    public Storage validateStorageForUpdateById(StorageUpdateRequest request) {
        Storage storage = ifPresent(request.getNamespaceName(), request.getId());
        validateStorageForUpdate(storage);
        return storage;
    }

    public Storage validateStorageForUpdateByName(StorageUpdateRequest request) {
        Storage storage = ifPresent(request.getNamespaceName(), request.getName());
        validateStorageForUpdate(storage);
        return storage;
    }

    public void validateStorageForUpdate(Storage storage) {
        validateTypeForUpdate(storage);
        validateStatusForUpdate(storage);
        validateQuotaForUpdate(storage, storage.getAmountBytes());
    }


    public Storage ifPresent(String namespaceName, long storageId) {
        logger.debug("storageId={}, namespaceName={}", storageId, namespaceName);
        Storage storage = storageRepository.findOne(storageId);
        if (storage == null || !namespaceName.equals(storage.getNamespaceName())) {
            throw new CcException(BackendReturnCodeNameConstants.STORAGE_WITH_ID_NOT_EXISTS,
                    ImmutableMap.of("namespaceName", namespaceName,
                            "storageId", storageId));
        }
        logger.debug("storage={}", storage);
        return storage;
    }

    public Storage ifPresent(String namespaceName, String storageName) {
        logger.debug("storageName={}, namespaceName={}", storageName, namespaceName);
        Storage storage = storageRepository.findByNamespaceNameAndStorageName(namespaceName, storageName);
        if (storage == null || !namespaceName.equals(storage.getNamespaceName())) {
            throw new CcException(BackendReturnCodeNameConstants.STORAGE_WITH_NAME_NOT_EXISTS,
                    ImmutableMap.of("namespaceName", namespaceName,
                            "storageName", storageName));
        }
        logger.debug("storage={}", storage);
        return storage;
    }

    public void validateStorageShouldNotExist(String namespaceName, String storageName) {
        Storage storage = storageRepository.findByNamespaceNameAndStorageName(namespaceName, storageName);
        if (storage != null) {
            throw new CcException(BackendReturnCodeNameConstants.STORAGE_ALREADY_EXISTS,
                    ImmutableMap.of("namespaceName", namespaceName,
                            "storageName", storageName));
        }
    }


    public void validateStorageParams(Storage storage) {
        if (storage.getStorageType() == EBS && storage.getAmountBytes() < EBS_HDD_MIN_BYTES) {
            throw new CcException(BackendReturnCodeNameConstants.PROPERTY_INVALID,
                    of("msg", "ebs storage at least 500Gi"));
        }

        if (storage.getAmountBytes() < 1024 * 1024) {
            throw new CcException(BackendReturnCodeNameConstants.PROPERTY_INVALID,
                    of("msg", "storage at least 1M"));
        }
    }

    public void validateStorageType(String namespaceName, Storage storage) {
        StorageType storageType = storage.getStorageType();
        if (storageType == StorageClass) {
            throw new CcException(BackendReturnCodeNameConstants.STORAGE_TYPE_NOT_SUPPORT,
                    of("namespaceName", namespaceName,
                            "storageType", storageType));
        }

        if (storageType == CephFS || storageType == RBD) {
            if (!properties.getCurrentCeph().isEnabled() || namespaceValidator.isLegacy(namespaceName)) {
                throw new CcException(BackendReturnCodeNameConstants.STORAGE_TYPE_NOT_SUPPORT,
                        of("namespaceName", namespaceName,
                                "storageType", storageType));
            }
        }

        if (storageType == NFS || storageType == EFS) {
            if (!properties.getCurrentNfs().isEnabled() || namespaceValidator.isLegacy(namespaceName)) {
                throw new CcException(BackendReturnCodeNameConstants.STORAGE_TYPE_NOT_SUPPORT,
                        of("namespaceName", namespaceName,
                                "storageType", storageType));
            }
        }

        if (storageType == EBS) {
            if (!properties.getCurrentNfs().isEnabled() || namespaceValidator.isLegacy(namespaceName)) {
                throw new CcException(BackendReturnCodeNameConstants.STORAGE_TYPE_NOT_SUPPORT,
                        of("namespaceName", namespaceName,
                                "storageType", storageType));
            }
        }
    }

    /**
     * update create storage
     * <p>
     * ceph fs and nfs has no quota
     *
     * @param namespaceName the namespace name
     * @param storage       the storage for create
     */
    public void validateQuotaForCreate(String namespaceName, Storage storage) {
        if (CephFS == storage.getStorageType() || NFS == storage.getStorageType()
                || EFS == storage.getStorageType()) {
            return;
        }

        long availBytes = namespaceStatsManagement.getNamespaceStorageAvailableBytes(namespaceName, storage.getStorageType());
        long neededBytes = storage.getAmountBytes();

        if (storage.getStorageType() == HostPath && storage.isUnshared()) {
            long realBytes = getUnsharedHostpathStorageRealBytes(namespaceName, storage.getPvcName(), storage.getAmountBytes());
            neededBytes = Math.max(realBytes, neededBytes);
        }

        if (neededBytes > availBytes) {
            throw new CcException(BackendReturnCodeNameConstants.STORAGE_OVER_QUOTA,
                    of("namespaceName", namespaceName,
                            "neededBytes", neededBytes,
                            "availBytes", availBytes));
        }
    }

    public void validateStatusForUpdate(Storage storage) {
        if (storage.getStatus() != Storage.Status.CREATE_SUCCESS &&
                storage.getStatus() != Storage.Status.UPDATE_SUCCESS &&
                storage.getStatus() != Storage.Status.UPDATE_FAILED) {
            throw new CcException(BackendReturnCodeNameConstants.STORAGE_STATUS_NOT_SUPPORT_UPDATE,
                    ImmutableMap.of("namespace", storage.getNamespaceName(),
                            "storage", storage.getStorageName(),
                            "status", storage.getStatus()));
        }
    }

    public void validateTypeForUpdate(Storage storage) {
        if (storage.getStorageType() == StorageClass) {
            throw new CcException(BackendReturnCodeNameConstants.STORAGE_TYPE_NOT_SUPPORT_UPDATE,
                    ImmutableMap.of("namespace", storage.getNamespaceName(),
                            "storage", storage.getStorageName(),
                            "type", storage.getStorageType()));
        }
    }

    /**
     * update storage
     * <p>
     * ceph fs and nfs has no quota not support update
     *
     * @param storage      the storage for update
     * @param requestBytes the bytes want to change
     */
    public void validateQuotaForUpdate(Storage storage, long requestBytes) {
        if (requestBytes < storage.getAmountBytes()) {
            throw new CcException(BackendReturnCodeNameConstants.STORAGE_CAN_NOT_CHANGE_SMALL);
        } else if (CephFS != storage.getStorageType() && NFS != storage.getStorageType()) {
            validateQuotaForChangeLarger(storage, requestBytes);
        }
    }

    private void validateQuotaForChangeSmall(Storage storage, long requestBytes) {
        String namespaceName = storage.getNamespaceName();

        // make sure it doesn't decrease too low
        long bytesUsed = 0;
        //Hostpath Usage is the most one
        if (storage.getStorageType() == HostPath) {
            List<Long> bytesList = hostPathStorageManager.getHostpathPvUsedBytesList(storage.getPvName());
            if (bytesList != null) {
                for (long bytes : bytesList) {
                    if (bytesUsed < bytes) {
                        bytesUsed = bytes;
                    }
                }
            }
        } else {
            bytesUsed = getStorageUsedBytes(storage);
        }

        if (requestBytes < bytesUsed * 1.1) {
            throw new CcException(BackendReturnCodeNameConstants.STORAGE_QUOTA_TOO_LOW,
                    ImmutableMap.of("namespaceName", namespaceName,
                            "requestBytes", requestBytes,
                            "bytesUsed", bytesUsed));
        }
    }

    private void validateQuotaForChangeLarger(Storage storage, long requestBytes) {
        String namespaceName = storage.getNamespaceName();
        long availableBytes = namespaceStatsManagement.getNamespaceStorageAvailableBytes(namespaceName, storage.getStorageType());

        long needBytes = requestBytes - storage.getAmountBytes();
        if (storage.getStorageType() == HostPath && storage.isUnshared()) {
            List<Deployment> deploymentList = clientManager.getClient()
                    .extensions()
                    .deployments()
                    .inNamespace(namespaceName)
                    .list()
                    .getItems();

            List<StatefulSet> statefulSetList = clientManager.getClient()
                    .apps()
                    .statefulSets()
                    .inNamespace(namespaceName)
                    .list()
                    .getItems();

            long realNeedBytes = getUnsharedHostpathStorageRealBytes(deploymentList, statefulSetList, storage.getPvcName(), requestBytes)
                    - getUnsharedHostpathStorageRealBytes(deploymentList, statefulSetList, storage.getPvcName(), storage.getAmountBytes());
            needBytes = Math.max(realNeedBytes, needBytes);
        }

        if (availableBytes < needBytes) {
            throw new CcException(BackendReturnCodeNameConstants.STORAGE_OVER_QUOTA,
                    of("namespaceName", namespaceName,
                            "neededBytes", needBytes,
                            "availableBytes", availableBytes));
        }
    }

    private long getUnsharedHostpathStorageRealBytes(String namespaceName,
                                                     String hostpathPvcName,
                                                     long hostpathBytes) {
        List<Deployment> deploymentList = clientManager.getClient()
                .extensions()
                .deployments()
                .inNamespace(namespaceName)
                .list()
                .getItems();

        List<StatefulSet> statefulSetList = clientManager.getClient()
                .apps()
                .statefulSets()
                .inNamespace(namespaceName)
                .list()
                .getItems();

        return getUnsharedHostpathStorageRealBytes(deploymentList, statefulSetList, hostpathPvcName, hostpathBytes);
    }

    private long getUnsharedHostpathStorageRealBytes(List<Deployment> deploymentList,
                                                     List<StatefulSet> statefulSetList,
                                                     String hostpathPvcName,
                                                     long hostpathBytes) {
        long deploymentBytes = Optional.ofNullable(deploymentList)
                .orElseGet(ArrayList::new)
                .stream()
                .mapToLong(deployment -> getDeploymentNeedBytes(deployment, hostpathPvcName, hostpathBytes))
                .sum();

        long statefulSetBytes = Optional.ofNullable(statefulSetList)
                .orElseGet(ArrayList::new)
                .stream()
                .mapToLong(statefulSet -> getStatefulSetNeedBytes(statefulSet, hostpathPvcName, hostpathBytes))
                .sum();

        return deploymentBytes + statefulSetBytes;
    }

    private long getDeploymentNeedBytes(Deployment deployment, String pvcName, long amountBytes) {
        Integer replicas = deployment.getSpec().getReplicas();
        List<String> pvcNames = Optional.ofNullable(deployment.getSpec().getTemplate().getSpec().getVolumes())
                .orElseGet(ArrayList::new)
                .stream()
                .map(Volume::getPersistentVolumeClaim)
                .filter(Objects::nonNull)
                .map(PersistentVolumeClaimVolumeSource::getClaimName)
                .filter(pvcName::equals)
                .collect(Collectors.toList());
        if (!pvcNames.isEmpty()) {
            return amountBytes * replicas;
        }
        return 0;
    }

    private long getStatefulSetNeedBytes(StatefulSet statefulSet, String pvcName, long amountBytes) {
        Integer replicas = statefulSet.getSpec().getReplicas();
        List<String> pvcNames = Optional.ofNullable(statefulSet.getSpec().getTemplate().getSpec().getVolumes())
                .orElseGet(ArrayList::new)
                .stream()
                .map(Volume::getPersistentVolumeClaim)
                .filter(Objects::nonNull)
                .map(PersistentVolumeClaimVolumeSource::getClaimName)
                .filter(pvcName::equals)
                .collect(Collectors.toList());
        if (!pvcNames.isEmpty()) {
            return amountBytes * replicas;
        }
        return 0;
    }

    /**
     * validate storage for delete
     * <p>
     * StorageClass type storage is created by k8s plugins, no event. when delete ,only need to delete pvc
     *
     * @param storage the storage for delete
     */
    public void validateForDelete(Storage storage) {
        if (storage.getStorageType() == StorageClass) {
            return;
        }

        if (storage.getStatus() != Storage.Status.CREATE_SUCCESS &&
                storage.getStatus() != Storage.Status.CREATE_FAILED &&
                storage.getStatus() != Storage.Status.UPDATE_FAILED &&
                storage.getStatus() != Storage.Status.UPDATE_SUCCESS) {
            throw new CcException(BackendReturnCodeNameConstants.STORAGE_STATUS_NOT_SUPPORT_DELETE,
                    ImmutableMap.of("namespace", storage.getNamespaceName(),
                            "storage", storage.getStorageName(),
                            "status", storage.getStatus()));
        }
    }

    public long getStorageUsedBytes(Storage storage) {
        long bytesUsed = 0;
        if (storage.getStorageType() == RBD || storage.getStorageType() == StorageClass) {
            CephImageUsage imageUsage = cephImageUsageRepository.
                    findByNamespaceNameAndImageName(storage.getNamespaceName(), storage.getImageName());
            if (imageUsage != null) {
                bytesUsed = imageUsage.getBytesUsed();
            }
        } else if (storage.getStorageType() == HostPath) {
            bytesUsed = hostPathStorageManager.getHostpathPvUsedBytes(storage.getPvName());
        } else if (storage.getStorageType() == CephFS) {
            CephfsDirectoryUsage directoryUsage = cephfsDirectoryUsageRepository.
                    findByNamespaceNameAndDirectoryName(storage.getNamespaceName(), storage.cephFsDir());
            if (directoryUsage != null) {
                bytesUsed = directoryUsage.getBytesUsed();
            }
        } else if (storage.getStorageType() == NFS || storage.getStorageType() == EFS) {
            NfsDirectoryUsage nfsDirectoryUsage = nfsDirectoryUsageRepository.
                    findByNamespaceNameAndDirectoryName(storage.getNamespaceName(), storage.nfsDir());
            if (nfsDirectoryUsage != null) {
                bytesUsed = nfsDirectoryUsage.getBytesUsed();
            }
        }
        return bytesUsed;
    }

    public long getStorageUsedBytes(StorageTopological storage) {
        long bytesUsed = 0;
        if (storage.getStorageType() == RBD || storage.getStorageType() == StorageClass) {
            CephImageUsage imageUsage = cephImageUsageRepository.
                    findByNamespaceNameAndImageName(storage.getNamespaceName(), storage.getStorageName());
            if (imageUsage != null) {
                bytesUsed = imageUsage.getBytesUsed();
            }
        } else if (storage.getStorageType() == HostPath) {
            bytesUsed = hostPathStorageManager.getHostpathPvUsedBytes(storage.getPvName());
        } else if (storage.getStorageType() == CephFS) {
            String cephFsDir = StorageUtils.cephFsDir(storage.getNamespaceName(), storage.getStorageName());
            CephfsDirectoryUsage directoryUsage = cephfsDirectoryUsageRepository.
                    findByNamespaceNameAndDirectoryName(storage.getNamespaceName(), cephFsDir);
            if (directoryUsage != null) {
                bytesUsed = directoryUsage.getBytesUsed();
            }
        } else if (storage.getStorageType() == NFS || storage.getStorageType() == EFS) {
            String nfsDir = StorageUtils.nfsDir(storage.getNamespaceName(), storage.getStorageName());
            NfsDirectoryUsage nfsDirectoryUsage = nfsDirectoryUsageRepository.
                    findByNamespaceNameAndDirectoryName(storage.getNamespaceName(), nfsDir);
            if (nfsDirectoryUsage != null) {
                bytesUsed = nfsDirectoryUsage.getBytesUsed();
            }
        }
        return bytesUsed;
    }
}
