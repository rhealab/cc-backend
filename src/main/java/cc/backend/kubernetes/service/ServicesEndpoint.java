package cc.backend.kubernetes.service;

import cc.backend.EnnProperties;
import cc.backend.kubernetes.apps.validator.AppValidator;
import cc.backend.kubernetes.service.dto.IFormService;
import cc.backend.kubernetes.service.dto.ServiceCreateRequest;
import cc.backend.kubernetes.service.dto.ServiceDto;
import cc.backend.kubernetes.service.dto.ServiceUpdateRequest;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author wangchunyang@gmail.com
 * @author yanzhixiang modified on 2017-6-7
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes/namespaces/{namespaceName}/apps/{appName}/services")
@Api(value = "Service", description = "Operation about service.", produces = "application/json")
public class ServicesEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(ServicesEndpoint.class);

    @Inject
    private ServicesManagement management;

    @Inject
    private AppValidator appValidator;

    @Inject
    private EnnProperties properties;

    @GET
    @ApiOperation(value = "Get the app's service list info.", responseContainer = "List", response = ServiceDto.class)
    public Response getServiceList(@PathParam("namespaceName") String namespaceName,
                                   @PathParam("appName") String appName) {
        appValidator.validateAppShouldExists(namespaceName, appName);
        List<Service> services = management.getServiceList(namespaceName, appName);
        return Response.ok(ServiceDto.from(services, properties.getCurrentKubernetes().getVipHost())).build();
    }

    @GET
    @Path("{serviceName}")
    @ApiOperation(value = "Get service info.", response = ServiceDto.class)
    public Response getService(@PathParam("namespaceName") String namespaceName,
                               @PathParam("appName") String appName,
                               @PathParam("serviceName") String serviceName) {
        appValidator.validateAppShouldExists(namespaceName, appName);
        Service service = management.getService(namespaceName, appName, serviceName);
        return Response.ok(ServiceDto.from(service, properties.getCurrentKubernetes().getVipHost())).build();
    }

    @GET
    @Path("{serviceName}/json")
    @ApiOperation(value = "Get service info of k8s original format.", response = Service.class)
    public Response getServiceJson(@PathParam("namespaceName") String namespaceName,
                                   @PathParam("appName") String appName,
                                   @PathParam("serviceName") String serviceName) {
        Service service = management.getService(namespaceName, appName, serviceName);
        return Response.ok(service).build();
    }

    @GET
    @Path("{serviceName}/form")
    @ApiOperation(value = "Get service info of console form format.", response = ServiceUpdateRequest.class)
    public Response getServiceForm(@PathParam("namespaceName") String namespaceName,
                                   @PathParam("appName") String appName,
                                   @PathParam("serviceName") String serviceName) {
        Service service = management.getService(namespaceName, appName, serviceName);
        return Response.ok(ServiceUpdateRequest.from(namespaceName, service, properties.getCurrentKubernetes().getVipHost())).build();
    }

    @POST
    @Path("form")
    @ApiOperation(value = "Deprecated, use POST kubernetes/namespaces/{namespaceName}/apps/{appName}/services. " +
            "Create service by form.", response = ServiceDto.class)
    @Deprecated
    public Response create(@PathParam("namespaceName") String namespaceName,
                           @PathParam("appName") String appName,
                           @Valid ServiceCreateRequest request) {
        appValidator.validateAppShouldExists(namespaceName, appName);

        Service service = IFormService.to(namespaceName, request);
        Service createdService = management.createWithAudit(namespaceName, appName, service);
        return Response.ok(ServiceDto.from(createdService, properties.getCurrentKubernetes().getVipHost())).build();
    }

    @POST
    @ApiOperation(value = "Create service by form.", response = ServiceDto.class)
    public Response createByForm(@PathParam("namespaceName") String namespaceName,
                                 @PathParam("appName") String appName,
                                 @Valid ServiceCreateRequest request) {
        appValidator.validateAppShouldExists(namespaceName, appName);
        Service service = IFormService.to(namespaceName, request);
        Service createdService = management.createWithAudit(namespaceName, appName, service);
        return Response.ok(ServiceDto.from(createdService, properties.getCurrentKubernetes().getVipHost())).build();
    }

    @PUT
    @Path("{serviceName}")
    @ApiOperation(value = "Update service by json.", response = ServiceDto.class)
    public Response updateServiceByJson(@PathParam("namespaceName") String namespaceName,
                                        @PathParam("appName") String appName,
                                        @PathParam("serviceName") String serviceName,
                                        Service service) {
        appValidator.validateAppShouldExists(namespaceName, appName);

        if (service != null && service.getMetadata() != null) {
            service.getMetadata().setName(serviceName);
        }

        Service updatedService = management.updateWithAudit(namespaceName, appName, service);
        return Response.ok(ServiceDto.from(updatedService, properties.getCurrentKubernetes().getVipHost())).build();
    }

    @PUT
    @Path("{serviceName}/form")
    @ApiOperation(value = "Update service by form.", response = ServiceDto.class)
    public Response updateServiceByForm(@PathParam("namespaceName") String namespaceName,
                                        @PathParam("appName") String appName,
                                        @PathParam("serviceName") String serviceName,
                                        ServiceUpdateRequest serviceUpdateRequest) {
        appValidator.validateAppShouldExists(namespaceName, appName);

        Service service = IFormService.to(namespaceName, serviceUpdateRequest);

        Service updatedService = management.updateWithAudit(namespaceName, appName, service);
        return Response.ok(ServiceDto.from(updatedService, properties.getCurrentKubernetes().getVipHost())).build();
    }

    @DELETE
    @Path("{serviceName}")
    @ApiOperation(value = "Delete specified service.")
    public Response delete(@PathParam("namespaceName") String namespaceName,
                           @PathParam("appName") String appName,
                           @PathParam("serviceName") String serviceName) {
        appValidator.validateAppShouldExists(namespaceName, appName);

        boolean succeed = management.delete(namespaceName, appName, serviceName);

        return Response.ok(ImmutableMap.of("status", succeed ? "ok" : "error")).build();
    }
}