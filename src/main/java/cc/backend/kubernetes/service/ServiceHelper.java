package cc.backend.kubernetes.service;

import cc.backend.kubernetes.batch.form.dto.BaseFormMixture;
import cc.backend.kubernetes.batch.form.dto.FormDeploymentServiceCreateRequest;
import cc.backend.kubernetes.batch.form.dto.FormLabel;
import cc.backend.kubernetes.batch.form.dto.FormServicePort;
import cc.backend.kubernetes.service.dto.IFormService;
import cc.backend.kubernetes.service.dto.ServiceType;
import cc.backend.kubernetes.batch.form.deprecated.FormStatefulSet;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.ServicePort;
import io.fabric8.kubernetes.api.model.ServiceSpec;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/14.
 */
public class ServiceHelper {

    public static List<String> composeLink(Service service, String vipHost) {
        Set<String> links = new HashSet<>();
        String serviceType = service.getSpec().getType();
        List<ServicePort> ports = Optional
                .ofNullable(service.getSpec().getPorts())
                .orElseGet(ArrayList::new);

        if (ServiceType.NodePort.name().equals(serviceType)) {
            Set<String> nodePortLinks = ports
                    .stream()
                    .map(ServicePort::getNodePort)
                    .filter(Objects::nonNull)
                    .map(p -> vipHost + ":" + p)
                    .collect(Collectors.toSet());
            links.addAll(nodePortLinks);
        }

        List<String> externalIPs = service.getSpec().getExternalIPs();
        if (!CollectionUtils.isEmpty(externalIPs)) {
            Set<String> externalIpLinks = externalIPs
                    .stream()
                    .map(ip -> ports.stream()
                            .map(ServicePort::getPort)
                            .filter(Objects::nonNull)
                            .map(p -> ip + ":" + p)
                            .collect(Collectors.toList()))
                    .flatMap(Collection::stream)
                    .collect(Collectors.toSet());
            links.addAll(externalIpLinks);
        }

        return new ArrayList<>(links);
    }

    public static ServiceSpec composeServiceSpec(IFormService request) {
        ServiceSpec spec = new ServiceSpec();
        spec.setSelector(FormLabel.toMap(request.getSelectors()));
        spec.setPorts(FormServicePort.toServicePorts(request.getPorts()));
        spec.setExternalIPs(request.getExternalIPs());
        spec.setType(ServiceHelper.getK8sType(request));
        spec.setClusterIP(request.getClusterIP());
        if (request.getType() == ServiceType.None) {
            spec.setClusterIP(ServiceType.None.name());
        }

        return spec;
    }

    public static ServiceSpec composeServiceSpec(BaseFormMixture request) {
        ServiceSpec spec = new ServiceSpec();
        spec.setSelector(FormLabel.toMap(request.getServiceSelectors()));
        spec.setPorts(FormServicePort.toServicePorts(request.getServicePorts()));
        spec.setExternalIPs(request.getServiceExternalIPs());
        spec.setType(ServiceHelper.getK8sType(request));
        spec.setClusterIP(request.getServiceClusterIP());
        if (request.getServiceType() == ServiceType.None) {
            spec.setClusterIP(ServiceType.None.name());
        }

        return spec;
    }

    public static ServiceSpec composeServiceSpec(FormDeploymentServiceCreateRequest request) {
        ServiceSpec spec = new ServiceSpec();
        spec.setSelector(FormLabel.toMap(request.getLabels()));
        spec.setPorts(FormServicePort.toServicePorts(request.getPortMappings()));
        spec.setExternalIPs(request.getExternalIPs());
        spec.setType(ServiceHelper.getK8sType(request));
        spec.setClusterIP(request.getClusterIP());
        if (request.getServiceType() == ServiceType.None) {
            spec.setClusterIP(ServiceType.None.name());
        }

        return spec;
    }

    public static ServiceSpec composeServiceSpec(FormStatefulSet request) {
        ServiceSpec spec = new ServiceSpec();
        spec.setSelector(FormLabel.toMap(request.getLabels()));
        spec.setPorts(FormServicePort.toServicePorts(request.getPortMappings()));
        spec.setExternalIPs(request.getExternalIPs());

        spec.setType(Optional.of(request)
                .map(FormStatefulSet::getServiceType)
                .map(type -> {
                    if (ServiceType.None == type || ServiceType.External == type) {
                        return ServiceType.ClusterIP.name();
                    }

                    return type.name();
                })
                .orElse(null));

        spec.setClusterIP(request.getClusterIP());
        if (request.getServiceType() == ServiceType.None) {
            spec.setClusterIP(ServiceType.None.name());
        }

        return spec;
    }

    public static ServiceType getFormType(Service service) {
        return Optional.ofNullable(service)
                .map(Service::getSpec)
                .map(ServiceSpec::getType)
                .map(ServiceType::valueOf)
                .orElse(null);
    }

    public static String getK8sType(IFormService request) {
        return Optional.ofNullable(request)
                .map(IFormService::getType)
                .map(type -> {
                    if (ServiceType.None == type || ServiceType.External == type) {
                        return ServiceType.ClusterIP.name();
                    }

                    return type.name();
                })
                .orElse(null);
    }

    public static String getK8sType(BaseFormMixture request) {
        return Optional.ofNullable(request)
                .map(BaseFormMixture::getServiceType)
                .map(type -> {
                    if (ServiceType.None == type || ServiceType.External == type) {
                        return ServiceType.ClusterIP.name();
                    }

                    return type.name();
                })
                .orElse(null);
    }

    public static String getK8sType(FormDeploymentServiceCreateRequest request) {
        return Optional.ofNullable(request)
                .map(FormDeploymentServiceCreateRequest::getServiceType)
                .map(type -> {
                    if (ServiceType.None == type || ServiceType.External == type) {
                        return ServiceType.ClusterIP.name();
                    }

                    return type.name();
                })
                .orElse(null);
    }
}
