package cc.backend.kubernetes.service;

import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.check.ResourceCheckManagement;
import cc.backend.kubernetes.check.domain.ExternalIpNodePortCheckRes;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.ServicePort;
import io.fabric8.kubernetes.api.model.ServiceSpec;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

import static cc.backend.kubernetes.Constants.APP_LABEL;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/7/6.
 */

@Component
public class ServiceValidator {

    @Inject
    protected KubernetesClientManager clientManager;

    @Inject
    private ResourceCheckManagement portCheckManagement;

    public Service validateServiceShouldExists(String namespaceName, String appName, String serviceName) {
        Service service = clientManager.getClient()
                .services()
                .inNamespace(namespaceName)
                .withName(serviceName)
                .get();
        if (service != null) {
            Map<String, String> labels = service.getMetadata().getLabels();
            if (labels != null && appName.equals(labels.get(APP_LABEL))) {
                return service;
            }
        }

        throw new CcException(BackendReturnCodeNameConstants.SERVICE_NOT_EXISTS_IN_APP,
                ImmutableMap.of("namespace", namespaceName,
                        "app", appName,
                        "service", serviceName));
    }

    public void validateServiceShouldNotExists(String namespaceName, String appName, String serviceName) {
        Service service = clientManager.getClient()
                .services()
                .inNamespace(namespaceName)
                .withName(serviceName)
                .get();
        if (service != null) {
            throw new CcException(BackendReturnCodeNameConstants.SERVICE_ALREADY_EXISTS,
                    ImmutableMap.of("namespace", namespaceName,
                            "app", appName,
                            "service", serviceName));
        }
    }

    public void validateSvcNodePortAndExternalIp(Service service) {
        if (service == null) {
            return;
        }

        ExternalIpNodePortCheckRes res = portCheckManagement.validateSvcExternalIpAndNodePort(service);
        if (!res.isAssignable()) {
            Map<String, Object> map = new HashMap<>(6);
            if (res.getPort() != null) {
                map.put("port", res.getPort());
            }
            if (res.getExternalIP() != null) {
                map.put("externalIP", res.getExternalIP());
            }
            if (res.getNodePort() != null) {
                map.put("nodePort", res.getNodePort());
            }
            map.put("conflictService", res.getConflictSvc());
            map.put("conflictServiceNamespace", res.getConflictSvcNs());
            map.put("conflictWithNodePort", res.isConflictWithNodePort());
            if (res.isNotInRange()) {
                throw new CcException(BackendReturnCodeNameConstants.NODE_PORT_NOT_IN_RIGHT_RANGE, map);
            } else if (res.isNodePortConflict()) {
                throw new CcException(BackendReturnCodeNameConstants.NODE_PORT_CONFLICT, map);
            } else {
                throw new CcException(BackendReturnCodeNameConstants.EXTERNAL_IP_CONFLICT, map);
            }
        }
    }

    /**
     * validate a service's cluster ip is valid
     * TODO
     *
     * @param service the service for valid
     */
    public void validateClusterIP(Service service) {

    }

    public void validatePortProtocol(String namespaceName, String appName, Service service) {
        List<String> protocolList = Optional.ofNullable(service)
                .map(Service::getSpec)
                .map(ServiceSpec::getPorts)
                .orElseGet(ArrayList::new)
                .stream()
                .map(ServicePort::getProtocol)
                .distinct()
                .collect(Collectors.toList());

        if (protocolList.size() > 1) {
            throw new CcException(BackendReturnCodeNameConstants.PROPERTY_INVALID,
                    ImmutableMap.of("namespace", namespaceName,
                            "app", appName,
                            "service", service.getMetadata().getName(),
                            "spec.ports.protocol", "only support one type protocol in a service"));
        }
    }
}
