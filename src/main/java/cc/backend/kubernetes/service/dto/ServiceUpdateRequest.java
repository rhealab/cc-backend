package cc.backend.kubernetes.service.dto;

import cc.backend.kubernetes.batch.form.dto.FormAnnotation;
import cc.backend.kubernetes.batch.form.dto.FormLabel;
import cc.backend.kubernetes.batch.form.dto.FormServicePort;
import cc.backend.kubernetes.service.ServiceHelper;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.ServiceSpec;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/27.
 */
public class ServiceUpdateRequest implements IFormService {
    @NotNull
    private String name;

    private String namespaceName;

    @Valid
    private List<FormLabel> labels;

    @Valid
    private List<FormAnnotation> annotations;

    @Valid
    private List<FormLabel> selectors;
    @Valid
    private List<FormServicePort> ports;
    private List<String> externalIPs;
    @NotNull
    private ServiceType type;

    private String clusterIP;
    private List<String> links;

    private EnnIngress ennIngress;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getNamespaceName() {
        return namespaceName;
    }

    @Override
    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    @Override
    public List<FormLabel> getLabels() {
        return labels;
    }

    @Override
    public void setLabels(List<FormLabel> labels) {
        this.labels = labels;
    }

    @Override
    public List<FormAnnotation> getAnnotations() {
        return annotations;
    }

    @Override
    public void setAnnotations(List<FormAnnotation> annotations) {
        this.annotations = annotations;
    }

    @Override
    public List<FormLabel> getSelectors() {
        return selectors;
    }

    @Override
    public void setSelectors(List<FormLabel> selectors) {
        this.selectors = selectors;
    }

    @Override
    public List<FormServicePort> getPorts() {
        return ports;
    }

    @Override
    public void setPorts(List<FormServicePort> ports) {
        this.ports = ports;
    }

    @Override
    public List<String> getExternalIPs() {
        return externalIPs;
    }

    @Override
    public void setExternalIPs(List<String> externalIPs) {
        this.externalIPs = externalIPs;
    }

    @Override
    public ServiceType getType() {
        return type;
    }

    @Override
    public void setType(ServiceType type) {
        this.type = type;
    }

    @Override
    public String getClusterIP() {
        return clusterIP;
    }

    @Override
    public void setClusterIP(String clusterIP) {
        this.clusterIP = clusterIP;
    }

    @Override
    public EnnIngress getEnnIngress() {
        return ennIngress;
    }

    @Override
    public void setEnnIngress(EnnIngress ennIngress) {
        this.ennIngress = ennIngress;
    }

    public List<String> getLinks() {
        return links;
    }

    public void setLinks(List<String> links) {
        this.links = links;
    }

    @Override
    public String toString() {
        return "ServiceUpdateRequest{" +
                "name='" + name + '\'' +
                ", namespaceName='" + namespaceName + '\'' +
                ", labels=" + labels +
                ", annotations=" + annotations +
                ", selectors=" + selectors +
                ", ports=" + ports +
                ", externalIPs=" + externalIPs +
                ", type=" + type +
                ", clusterIP='" + clusterIP + '\'' +
                ", links=" + links +
                ", ennIngress=" + ennIngress +
                '}';
    }

    public static ServiceUpdateRequest from(String namespaceName, Service service, String vipHost) {
        if (service == null) {
            return null;
        }

        ObjectMeta metadata = service.getMetadata();
        ServiceSpec spec = service.getSpec();

        ServiceUpdateRequest form = new ServiceUpdateRequest();

        form.setName(metadata.getName());
        form.setNamespaceName(namespaceName);
        form.setLabels(FormLabel.from(metadata.getLabels()));

        Map<String, String> annotations = new HashMap<>(Optional.ofNullable(metadata.getAnnotations()).orElse(new HashMap<>()));
        form.setEnnIngress(EnnIngress.from(annotations));

        IFormService.cleanAnnotations(annotations);
        form.setAnnotations(FormAnnotation.from(annotations));

        form.setSelectors(FormLabel.from(spec.getSelector()));
        form.setPorts(FormServicePort.from(spec.getPorts()));
        form.setExternalIPs(spec.getExternalIPs());
        form.setType(ServiceHelper.getFormType(service));
        form.setClusterIP(spec.getClusterIP());

        form.setLinks(ServiceHelper.composeLink(service, vipHost));

        return form;
    }
}
