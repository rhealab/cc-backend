package cc.backend.kubernetes.service.dto;

import io.fabric8.kubernetes.api.model.ServicePort;

import java.util.ArrayList;
import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/2/22.
 */
public class ServicePortDto {
    private String name;
    private Integer port;
    private String protocol;
    private Integer nodePort;
    private String targetPort;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public Integer getNodePort() {
        return nodePort;
    }

    public void setNodePort(Integer nodePort) {
        this.nodePort = nodePort;
    }

    public String getTargetPort() {
        return targetPort;
    }

    public void setTargetPort(String targetPort) {
        this.targetPort = targetPort;
    }

    @Override
    public String toString() {
        return "ServicePortDto{" +
                "name='" + name + '\'' +
                ", port=" + port +
                ", protocol='" + protocol + '\'' +
                ", nodePort=" + nodePort +
                ", targetPort='" + targetPort + '\'' +
                '}';
    }

    public static List<ServicePortDto> from(List<ServicePort> ports) {
        List<ServicePortDto> dtos = new ArrayList<>();

        if (ports != null) {
            for (ServicePort port : ports) {
                ServicePortDto dto = new ServicePortDto();
                dto.setName(port.getName());
                dto.setPort(port.getPort());
                dto.setProtocol(port.getProtocol());
                dto.setNodePort(port.getNodePort());
                if (port.getTargetPort() != null) {
                    if (port.getTargetPort().getIntVal() != null) {
                        dto.setTargetPort(String.valueOf(port.getTargetPort().getIntVal()));
                    } else {
                        dto.setTargetPort(String.valueOf(port.getTargetPort().getStrVal()));
                    }
                }

                dtos.add(dto);
            }
        }

        return dtos;
    }
}
