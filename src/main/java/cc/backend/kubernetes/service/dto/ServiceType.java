package cc.backend.kubernetes.service.dto;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/21.
 */
public enum ServiceType {
    NodePort,
    LoadBalancer,
    ClusterIP,
    ExternalName,
    External,
    None
}
