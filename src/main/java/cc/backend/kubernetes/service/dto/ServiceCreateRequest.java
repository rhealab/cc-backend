package cc.backend.kubernetes.service.dto;

import cc.backend.kubernetes.Constants;
import cc.backend.kubernetes.batch.form.dto.FormAnnotation;
import cc.backend.kubernetes.batch.form.dto.FormLabel;
import cc.backend.kubernetes.batch.form.dto.FormServicePort;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

import static cc.backend.kubernetes.Constants.SERVICE_NAME_MAX;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/27.
 */
public class ServiceCreateRequest implements IFormService {
    @NotNull
    @Size(max = SERVICE_NAME_MAX, message = "service name must no more than " + SERVICE_NAME_MAX + " characters.")
    @Pattern(regexp = Constants.RESOURCE_NAME_PATTERN)
    private String name;

    private String namespaceName;

    @Valid
    private List<FormLabel> labels;

    @Valid
    private List<FormAnnotation> annotations;

    @Valid
    private List<FormLabel> selectors;
    @Valid
    private List<FormServicePort> ports;
    private List<String> externalIPs;
    private ServiceType type;

    private String clusterIP;

    private EnnIngress ennIngress;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getNamespaceName() {
        return namespaceName;
    }

    @Override
    public void setNamespaceName(String namespaceName) {
        this.namespaceName = namespaceName;
    }

    @Override
    public List<FormLabel> getLabels() {
        return labels;
    }

    @Override
    public void setLabels(List<FormLabel> labels) {
        this.labels = labels;
    }

    @Override
    public List<FormAnnotation> getAnnotations() {
        return annotations;
    }

    @Override
    public void setAnnotations(List<FormAnnotation> annotations) {
        this.annotations = annotations;
    }

    @Override
    public List<FormLabel> getSelectors() {
        return selectors;
    }

    @Override
    public void setSelectors(List<FormLabel> selectors) {
        this.selectors = selectors;
    }

    @Override
    public List<FormServicePort> getPorts() {
        return ports;
    }

    @Override
    public void setPorts(List<FormServicePort> ports) {
        this.ports = ports;
    }

    @Override
    public List<String> getExternalIPs() {
        return externalIPs;
    }

    @Override
    public void setExternalIPs(List<String> externalIPs) {
        this.externalIPs = externalIPs;
    }

    @Override
    public ServiceType getType() {
        return type;
    }

    @Override
    public void setType(ServiceType type) {
        this.type = type;
    }

    @Override
    public String getClusterIP() {
        return clusterIP;
    }

    @Override
    public void setClusterIP(String clusterIP) {
        this.clusterIP = clusterIP;
    }

    @Override
    public EnnIngress getEnnIngress() {
        return ennIngress;
    }

    @Override
    public void setEnnIngress(EnnIngress ennIngress) {
        this.ennIngress = ennIngress;
    }

    @Override
    public String toString() {
        return "ServiceCreateRequest{" +
                "name='" + name + '\'' +
                ", namespaceName='" + namespaceName + '\'' +
                ", labels=" + labels +
                ", annotations=" + annotations +
                ", selectors=" + selectors +
                ", ports=" + ports +
                ", externalIPs=" + externalIPs +
                ", type=" + type +
                ", clusterIP='" + clusterIP + '\'' +
                ", ennIngress=" + ennIngress +
                '}';
    }
}
