package cc.backend.kubernetes.service.dto;

import cc.backend.kubernetes.Constants;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2018/2/7.
 */
public class EnnIngress {
    /**
     * needed
     */
    private Boolean enabled;
    //TODO check port name is valid or not
    private String mappedPortName;

    /**
     * optional
     */
    private String subDomain;
    private String fullDomain;
    private Boolean tlsEnabled;
    private Boolean backendTlsEnabled;
    private IngressType type;

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getMappedPortName() {
        return mappedPortName;
    }

    public void setMappedPortName(String mappedPortName) {
        this.mappedPortName = mappedPortName;
    }

    public String getSubDomain() {
        return subDomain;
    }

    public void setSubDomain(String subDomain) {
        this.subDomain = subDomain;
    }

    public String getFullDomain() {
        return fullDomain;
    }

    public void setFullDomain(String fullDomain) {
        this.fullDomain = fullDomain;
    }

    public Boolean getTlsEnabled() {
        return tlsEnabled;
    }

    public void setTlsEnabled(Boolean tlsEnabled) {
        this.tlsEnabled = tlsEnabled;
    }

    public Boolean getBackendTlsEnabled() {
        return backendTlsEnabled;
    }

    public void setBackendTlsEnabled(Boolean backendTlsEnabled) {
        this.backendTlsEnabled = backendTlsEnabled;
    }

    public IngressType getType() {
        return type;
    }

    public void setType(IngressType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "EnnIngress{" +
                "enabled=" + enabled +
                ", mappedPortName='" + mappedPortName + '\'' +
                ", subDomain='" + subDomain + '\'' +
                ", fullDomain='" + fullDomain + '\'' +
                ", tlsEnabled=" + tlsEnabled +
                ", backendTlsEnabled=" + backendTlsEnabled +
                ", type=" + type +
                '}';
    }

    public static Map<String, String> toAnnotations(EnnIngress ingress) {
        Map<String, String> annotations = new HashMap<>(7);
        if (ingress.getEnabled()) {
            annotations.put(Constants.ENN_INGRESS_ENABLE, "true");
            annotations.put(Constants.ENN_INGRESS_PORT_NAME, ingress.getMappedPortName());

            if (!StringUtils.isEmpty(ingress.getSubDomain())) {
                annotations.put(Constants.ENN_INGRESS_SUB_DOMAIN, ingress.getSubDomain());
            }

            if (!StringUtils.isEmpty(ingress.getFullDomain())) {
                annotations.put(Constants.ENN_INGRESS_FULL_DOMAIN, ingress.getFullDomain());
            }

            if (ingress.getTlsEnabled() != null) {
                annotations.put(Constants.ENN_INGRESS_TLS_ENABLE, String.valueOf(ingress.getTlsEnabled()));
            }

            if (ingress.getBackendTlsEnabled() != null) {
                annotations.put(Constants.ENN_INGRESS_BACKEND_TLS_ENABLE, String.valueOf(ingress.getBackendTlsEnabled()));
            }

            if (ingress.getType() != null) {
                annotations.put(Constants.ENN_INGRESS_TYPE, ingress.getType().name());
            }
        }

        return annotations;
    }

    public static EnnIngress from(Map<String, String> annotations) {
        if (annotations == null || !"true".equals(annotations.get(Constants.ENN_INGRESS_ENABLE))) {
            return null;
        }
        
        EnnIngress ingress = new EnnIngress();
        ingress.setEnabled(true);
        ingress.setMappedPortName(annotations.get(Constants.ENN_INGRESS_PORT_NAME));
        ingress.setSubDomain(annotations.get(Constants.ENN_INGRESS_SUB_DOMAIN));
        ingress.setFullDomain(annotations.get(Constants.ENN_INGRESS_FULL_DOMAIN));

        String tlsEnabled = annotations.get(Constants.ENN_INGRESS_TLS_ENABLE);
        if (!StringUtils.isEmpty(tlsEnabled)) {
            ingress.setTlsEnabled("true".equals(tlsEnabled));
        }

        String backendTlsEnabled = annotations.get(Constants.ENN_INGRESS_BACKEND_TLS_ENABLE);
        if (!StringUtils.isEmpty(backendTlsEnabled)) {
            ingress.setBackendTlsEnabled("true".equals(backendTlsEnabled));
        }

        String type = annotations.get(Constants.ENN_INGRESS_TYPE);
        if (!StringUtils.isEmpty(type)) {
            try {
                ingress.setType(IngressType.valueOf(type));
            } catch (IllegalArgumentException e) {
                // ignore
            }
        }
        return ingress;
    }

    public enum IngressType {
        inside, outside
    }
}
