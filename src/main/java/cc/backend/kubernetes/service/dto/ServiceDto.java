package cc.backend.kubernetes.service.dto;

import cc.backend.kubernetes.service.ServiceHelper;
import cc.backend.kubernetes.utils.ResourceAnnotationsHelper;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.ServiceSpec;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/2/22.
 * @author yanzhixiang modified on 2017-6-7
 */
public class ServiceDto {
    private String name;
    private String namespace;
    private String uid;
    private Map<String, String> labels;
    private Map<String, String> selector;
    private String clusterIP;
    private List<String> externalIPs;
    private List<ServicePortDto> ports;
    private List<IngressDto> ingress;
    private String type;
    private Date createdOn;
    private String createdBy;
    private Long age;
    private List<String> links;
    private EnnIngress ennIngress;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    public void setLabels(Map<String, String> labels) {
        this.labels = labels;
    }

    public Map<String, String> getSelector() {
        return selector;
    }

    public void setSelector(Map<String, String> selector) {
        this.selector = selector;
    }

    public String getClusterIP() {
        return clusterIP;
    }

    public void setClusterIP(String clusterIP) {
        this.clusterIP = clusterIP;
    }

    public List<String> getExternalIPs() {
        return externalIPs;
    }

    public void setExternalIPs(List<String> externalIPs) {
        this.externalIPs = externalIPs;
    }

    public List<ServicePortDto> getPorts() {
        return ports;
    }

    public void setPorts(List<ServicePortDto> ports) {
        this.ports = ports;
    }

    public List<IngressDto> getIngress() {
        return ingress;
    }

    public void setIngress(List<IngressDto> ingress) {
        this.ingress = ingress;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public List<String> getLinks() {
        return links;
    }

    public void setLinks(List<String> links) {
        this.links = links;
    }

    public EnnIngress getEnnIngress() {
        return ennIngress;
    }

    public void setEnnIngress(EnnIngress ennIngress) {
        this.ennIngress = ennIngress;
    }

    @Override
    public String toString() {
        return "ServiceDto{" +
                "name='" + name + '\'' +
                ", namespace='" + namespace + '\'' +
                ", uid='" + uid + '\'' +
                ", labels=" + labels +
                ", selector=" + selector +
                ", clusterIP='" + clusterIP + '\'' +
                ", externalIPs=" + externalIPs +
                ", ports=" + ports +
                ", ingress=" + ingress +
                ", type='" + type + '\'' +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                ", age=" + age +
                ", links=" + links +
                ", ennIngress=" + ennIngress +
                '}';
    }

    public static List<ServiceDto> from(List<Service> services, String vipHost) {
        return Optional.ofNullable(services)
                .orElseGet(ArrayList::new)
                .stream()
                .map(service -> from(service, vipHost))
                .collect(Collectors.toList());
    }

    public static ServiceDto from(Service service, String vipHost) {
        ServiceDto dto = new ServiceDto();
        ObjectMeta metadata = service.getMetadata();
        ServiceSpec spec = service.getSpec();

        dto.setName(metadata.getName());
        dto.setNamespace(metadata.getNamespace());
        dto.setUid(metadata.getUid());
        dto.setLabels(metadata.getLabels());
        dto.setSelector(spec.getSelector());
        dto.setClusterIP(spec.getClusterIP());
        dto.setExternalIPs(spec.getExternalIPs());
        dto.setPorts(ServicePortDto.from(spec.getPorts()));
        if (service.getStatus() != null && service.getStatus().getLoadBalancer() != null) {
            dto.setIngress(IngressDto.from(service.getStatus().getLoadBalancer().getIngress()));
        }
        dto.setType(spec.getType());
        dto.setLinks(ServiceHelper.composeLink(service, vipHost));
        dto.setEnnIngress(EnnIngress.from(metadata.getAnnotations()));

        ResourceAnnotationsHelper.addExtrasInfo(service, dto);
        return dto;
    }
}