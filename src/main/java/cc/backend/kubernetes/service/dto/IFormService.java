package cc.backend.kubernetes.service.dto;

import cc.backend.kubernetes.Constants;
import cc.backend.kubernetes.batch.form.dto.FormAnnotation;
import cc.backend.kubernetes.batch.form.dto.FormLabel;
import cc.backend.kubernetes.batch.form.dto.FormServicePort;
import cc.backend.kubernetes.service.ServiceHelper;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.Service;

import java.util.List;
import java.util.Map;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/14.
 */
public interface IFormService {
    /**
     * get tht service name
     *
     * @return the service name
     */
    String getName();

    /**
     * set the service name
     *
     * @param name the service name
     */
    void setName(String name);

    /**
     * get the namespace name
     *
     * @return the namespace name
     */
    String getNamespaceName();

    /**
     * set the namespace name
     *
     * @param namespaceName the namespace name
     */
    void setNamespaceName(String namespaceName);

    /**
     * get the service labels
     *
     * @return the service form labels
     */
    List<FormLabel> getLabels();

    /**
     * set the service labels
     *
     * @param labels the form labels
     */
    void setLabels(List<FormLabel> labels);

    /**
     * get the service annotations
     *
     * @return the form annotations
     */
    List<FormAnnotation> getAnnotations();

    /**
     * set the annotations
     *
     * @param annotations the form annotations
     */
    void setAnnotations(List<FormAnnotation> annotations);

    /**
     * get the selectors
     *
     * @return the form selectors
     */
    List<FormLabel> getSelectors();

    /**
     * set the selectors
     *
     * @param selectors the form selectors
     */
    void setSelectors(List<FormLabel> selectors);

    /**
     * get the service ports
     *
     * @return the form service ports
     */
    List<FormServicePort> getPorts();

    /**
     * set the service ports
     *
     * @param ports the form service ports
     */
    void setPorts(List<FormServicePort> ports);

    /**
     * get the service external ip list
     *
     * @return the external ip list
     */
    List<String> getExternalIPs();

    /**
     * set the service external ip list
     *
     * @param externalIPs the external ip list
     */
    void setExternalIPs(List<String> externalIPs);

    /**
     * get the service type
     *
     * @return the service type
     */
    ServiceType getType();

    /**
     * set the service type
     *
     * @param type the service type
     */
    void setType(ServiceType type);

    /**
     * get the service cluster ip
     *
     * @return the cluster ip
     */
    String getClusterIP();

    /**
     * set the service cluster ip
     *
     * @param clusterIP the cluster ip
     */
    void setClusterIP(String clusterIP);

    /**
     * get the enn ingress
     *
     * @return the enn ingress
     */
    EnnIngress getEnnIngress();

    /**
     * set the enn ingress
     *
     * @param ennIngress the enn ingress
     */
    void setEnnIngress(EnnIngress ennIngress);

    /**
     * convert form service to k8s service
     *
     * @param namespaceName the namespace
     * @param request       the form request
     * @return the k8s service
     */
    static Service to(String namespaceName, IFormService request) {
        Service service = new Service();

        ObjectMeta meta = new ObjectMeta();
        meta.setName(request.getName());
        meta.setLabels(FormLabel.toMap(request.getLabels()));

        Map<String, String> annotations = FormAnnotation.toMap(request.getAnnotations());
        if (request.getEnnIngress() != null) {
            annotations.putAll(EnnIngress.toAnnotations(request.getEnnIngress()));
        }
        meta.setAnnotations(annotations);

        meta.setNamespace(namespaceName);
        service.setMetadata(meta);

        service.setSpec(ServiceHelper.composeServiceSpec(request));

        return service;
    }

    /**
     * clean annotations about ingress
     *
     * @param annotations the annotations to clean
     */
    static void cleanAnnotations(Map<String, String> annotations) {
        annotations.remove(Constants.ENN_INGRESS_ENABLE);
        annotations.remove(Constants.ENN_INGRESS_PORT_NAME);
        annotations.remove(Constants.ENN_INGRESS_SUB_DOMAIN);
        annotations.remove(Constants.ENN_INGRESS_FULL_DOMAIN);
        annotations.remove(Constants.ENN_INGRESS_TLS_ENABLE);
        annotations.remove(Constants.ENN_INGRESS_BACKEND_TLS_ENABLE);
        annotations.remove(Constants.ENN_INGRESS_TYPE);
    }
}
