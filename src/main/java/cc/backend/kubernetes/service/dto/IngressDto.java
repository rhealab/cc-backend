package cc.backend.kubernetes.service.dto;

import io.fabric8.kubernetes.api.model.LoadBalancerIngress;

import java.util.ArrayList;
import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/2/22.
 */
public class IngressDto {
    private String ip;
    private String hostname;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    @Override
    public String toString() {
        return "IngressDto{" +
                "ip='" + ip + '\'' +
                ", hostname='" + hostname + '\'' +
                '}';
    }

    public static List<IngressDto> from(List<LoadBalancerIngress> ingresses) {
        List<IngressDto> dtos = new ArrayList<>();

        for (LoadBalancerIngress balancerIngress : ingresses) {
            IngressDto dto = new IngressDto();
            dto.setIp(balancerIngress.getIp());
            dto.setHostname(balancerIngress.getHostname());
            dtos.add(dto);
        }

        return dtos;
    }
}
