package cc.backend.kubernetes.service;

import cc.backend.audit.operation.OperationAuditMessage;
import cc.backend.audit.operation.OperationAuditMessageProducer;
import cc.backend.audit.operation.OperationType;
import cc.backend.audit.operation.ResourceType;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.utils.ResourceAnnotationsHelper;
import cc.backend.kubernetes.utils.ResourceLabelHelper;
import com.google.common.base.Stopwatch;
import io.fabric8.kubernetes.api.model.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static cc.backend.kubernetes.Constants.APP_LABEL;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/3/3.
 * @author yanzhxiang modified on 2017-6-7
 */
@Component
public class ServicesManagement {
    private static final Logger logger = LoggerFactory.getLogger(ServicesManagement.class);
    @Inject
    private KubernetesClientManager clientManager;

    @Inject
    private OperationAuditMessageProducer operationAuditMessageProducer;

    @Inject
    private ServiceValidator validator;

    public List<Service> getServiceList(String namespaceName, String appName) {
        List<Service> services = clientManager.getClient()
                .services()
                .inNamespace(namespaceName)
                .withLabel(APP_LABEL, appName)
                .list()
                .getItems();
        return Optional.ofNullable(services).orElseGet(ArrayList::new);
    }

    public Service getService(String namespaceName, String appName, String serviceName) {
        return validator.validateServiceShouldExists(namespaceName, appName, serviceName);
    }

    public Service createWithAudit(String namespaceName, String appName, Service service) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        service = create(namespaceName, appName, service);

        sendAudit(appName, service.getMetadata().getName(), OperationType.CREATE, stopwatch);
        return service;
    }

    public Service create(String namespaceName, String appName, Service service) {
        validator.validateServiceShouldNotExists(namespaceName, appName, service.getMetadata().getName());
        validator.validateClusterIP(service);
        validator.validatePortProtocol(namespaceName, appName, service);
        validator.validateSvcNodePortAndExternalIp(service);

        return createInternal(namespaceName, appName, service);
    }

    private Service createInternal(String namespaceName, String appName, Service service) {
        ResourceLabelHelper.addLabel(service, APP_LABEL, appName);
        ResourceAnnotationsHelper.setExtrasInfoForCreate(service);

        return clientManager.getClient()
                .services()
                .inNamespace(namespaceName)
                .create(service);
    }

    public Service updateWithAudit(String namespaceName, String appName, Service service) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        service = update(namespaceName, appName, service);

        sendAudit(appName, service.getMetadata().getName(), OperationType.UPDATE, stopwatch);
        return service;
    }

    public Service update(String namespaceName, String appName, Service service) {
        String serviceName = service.getMetadata().getName();

        validator.validateServiceShouldExists(namespaceName, appName, serviceName);
        validator.validateClusterIP(service);
        validator.validatePortProtocol(namespaceName, appName, service);
        validator.validateSvcNodePortAndExternalIp(service);

        return updateInternal(namespaceName, appName, service);
    }

    private Service updateInternal(String namespaceName, String appName, Service service) {
        String serviceName = service.getMetadata().getName();
        ResourceLabelHelper.addLabel(service, APP_LABEL, appName);
        ResourceAnnotationsHelper.setExtrasInfoForUpdate(service);

        return clientManager.getClient()
                .services()
                .inNamespace(namespaceName)
                .withName(serviceName)
                .replace(service);
    }

    public Boolean delete(String namespaceName, String appName, String serviceName) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        validator.validateServiceShouldExists(namespaceName, appName, serviceName);

        Boolean succeed = clientManager.getClient()
                .services()
                .inNamespace(namespaceName)
                .withName(serviceName)
                .delete();

        sendAudit(appName, serviceName, OperationType.DELETE, stopwatch);

        return succeed;
    }

    private void sendAudit(String appName,
                           String serviceName,
                           OperationType operationType,
                           Stopwatch stopwatch) {
        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .elapsed(elapsed)
                .operationType(operationType)
                .resourceType(ResourceType.SERVICE)
                .resourceName(serviceName)
                .extra("appName", appName)
                .build();
        operationAuditMessageProducer.send(message);
    }
}
