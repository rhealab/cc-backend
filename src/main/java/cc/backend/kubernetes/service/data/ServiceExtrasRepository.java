package cc.backend.kubernetes.service.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author yanzhxiang
 */
public interface ServiceExtrasRepository extends JpaRepository<ServiceExtras, Long> {
    ServiceExtras findByNamespaceAndName(String namespace, String name);

    @Transactional
    Long deleteByNamespaceAndName(String namespace, String name);

    @Transactional
    void deleteByNamespaceAndAppName(String namespace, String appName);

    @Transactional
    void deleteByNamespace(String namespace);

    long countByNamespace(String namespaceName);
}
