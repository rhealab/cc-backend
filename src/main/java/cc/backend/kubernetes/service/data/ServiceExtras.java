package cc.backend.kubernetes.service.data;

import javax.persistence.*;
import java.util.Date;

/**
 * @author yanzhixiang
 */
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"namespace","name"})})
public class ServiceExtras {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String namespace;

    private String appName;

    private String name;

    private String createdBy;

    private Date createdOn;

    private String lastUpdatedBy;

    private Date lastUpdatedOn;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public Date getLastUpdatedOn() {
        return lastUpdatedOn;
    }

    public void setLastUpdatedOn(Date lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    @Override
    public String toString(){
        return "App{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", namespace='" + namespace + '\'' +
                ", appName='" + appName + '\'' +
                ", lastUpdatedOn=" + lastUpdatedOn +
                ", lastUpdatedBy='" + lastUpdatedBy + '\'' +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                '}';
    }

}
