package cc.backend.kubernetes.workload.deployment.validator;

import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.namespaces.data.NamespaceExtras;
import cc.backend.kubernetes.namespaces.data.NamespaceExtrasRepository;
import cc.backend.kubernetes.namespaces.services.NamespaceStatsManagement;
import cc.backend.kubernetes.storage.StorageRepository;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageType;
import cc.backend.kubernetes.workload.utils.WorkloadUtils;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.PodTemplateSpec;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.DeploymentSpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static cc.backend.kubernetes.Constants.APP_LABEL;
import static cc.backend.kubernetes.Constants.CRITICAL_POD_ANNOTATION;
import static cc.backend.kubernetes.storage.domain.StorageType.HostPath;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/5/11.
 */
@Component
public class DeploymentsValidator {
    private static final Logger logger = LoggerFactory.getLogger(DeploymentsValidator.class);

    @Inject
    protected KubernetesClientManager clientManager;

    @Inject
    protected StorageRepository storageRepository;

    @Inject
    protected NamespaceStatsManagement namespaceStatsManagement;

    @Inject
    protected NamespaceExtrasRepository namespaceExtrasRepository;

    public void validateDeploymentReplicas(String namespaceName,
                                           String appName,
                                           Deployment deployment,
                                           int reduceReplicasNum) {
        int replicas = WorkloadUtils.getReplicas(deployment);
        if (replicas < reduceReplicasNum) {
            String deploymentName = deployment.getMetadata().getName();
            throw new CcException(BackendReturnCodeNameConstants.PROPERTY_INVALID,
                    ImmutableMap.of("namespace", namespaceName,
                            "app", appName,
                            "deployment", deploymentName,
                            "currentReplicas", replicas,
                            "reduceNum", reduceReplicasNum));
        }
    }

    public void validateDeploymentShouldNotExists(String namespaceName, String deploymentName) {
        Deployment deployment = clientManager.getClient()
                .extensions()
                .deployments()
                .inNamespace(namespaceName)
                .withName(deploymentName)
                .get();
        if (deployment != null) {
            throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_ALREADY_EXISTS,
                    ImmutableMap.of("namespace", namespaceName,
                            "deployment", deploymentName));
        }
    }

    public Deployment validateDeploymentShouldExists(String namespaceName,
                                                     String appName,
                                                     String deploymentName) {
        Deployment deployment = clientManager.getClient()
                .extensions()
                .deployments()
                .inNamespace(namespaceName)
                .withName(deploymentName)
                .get();
        if (StringUtils.isEmpty(appName) && deployment != null) {
            return deployment;
        }

        String deployAppName = Optional.ofNullable(deployment)
                .map(Deployment::getMetadata)
                .map(ObjectMeta::getLabels)
                .orElseGet(HashMap::new)
                .get(APP_LABEL);
        if (appName.equals(deployAppName)) {
            return deployment;
        }

        throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_NOT_EXISTS_IN_APP,
                ImmutableMap.of("namespace", namespaceName,
                        "app", appName,
                        "deployment", deploymentName));
    }

    public void validateHostpathQuotaForCreate(String namespaceName,
                                               String appName,
                                               Deployment deployment) {
        long neededBytes = getHostpathNeededBytes(namespaceName, deployment);
        validateHostPathQuota(namespaceName, appName, deployment.getMetadata().getName(), neededBytes);
    }

    /**
     * validate the new deployment used hostpath is over quota or not
     *
     * @param namespaceName the namespace name
     * @param replicas      the new deployment replicas
     * @param storageList   the new deployment used storage list
     * @param oldDeployment the old deployment
     */
    public void validateHostpathQuotaForUpdate(String namespaceName,
                                               String appName,
                                               int replicas,
                                               List<Storage> storageList,
                                               Deployment oldDeployment) {
        if (replicas <= 1) {
            return;
        }

        long newNeededBytes = storageList.stream()
                .filter(storage -> storage.getStorageType() == StorageType.HostPath && storage.isUnshared())
                .mapToLong(Storage::getAmountBytes)
                .sum() * (replicas - 1);

        if (newNeededBytes == 0) {
            return;
        }

        long oldNeededBytes = getHostpathNeededBytes(namespaceName, oldDeployment);

        validateHostPathQuota(namespaceName, appName, oldDeployment.getMetadata().getName(),
                newNeededBytes - oldNeededBytes);
    }

    public void validateHostPathQuotaForUpdate(String namespaceName,
                                               String appName,
                                               Deployment deployment,
                                               int addReplicasNum) {
        if (addReplicasNum > 0) {
            long neededBytes = WorkloadUtils.getExistsStorageList(namespaceName, deployment, storageRepository)
                    .stream()
                    .filter(storage -> storage.getStorageType() == HostPath && storage.isUnshared())
                    .mapToLong(Storage::getAmountBytes)
                    .sum() * addReplicasNum;

            validateHostPathQuota(namespaceName, appName, deployment.getMetadata().getName(), neededBytes);
        }
    }

    private long getHostpathNeededBytes(String namespaceName, Deployment deployment) {
        int replicas = WorkloadUtils.getReplicas(deployment);
        if (replicas <= 1) {
            return 0;
        }

        return WorkloadUtils.getExistsStorageList(namespaceName, deployment, storageRepository)
                .stream()
                .filter(storage -> storage.getStorageType() == HostPath && storage.isUnshared())
                .mapToLong(Storage::getAmountBytes)
                .sum() * (replicas - 1);
    }

    private void validateHostPathQuota(String namespaceName,
                                       String appName,
                                       String deploymentName,
                                       long neededBytes) {
        if (neededBytes > 0) {
            long availableByte = namespaceStatsManagement
                    .getNamespaceStorageAvailableBytes(namespaceName, StorageType.HostPath);

            if (neededBytes > availableByte) {
                Map<String, Object> map = new ImmutableMap.Builder<String, Object>()
                        .put("namespace", namespaceName)
                        .put("app", appName)
                        .put("deployment", deploymentName)
                        .put("neededBytes", neededBytes)
                        .put("limitsBytes", availableByte)
                        .build();

                throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_HOSTPATH_STORAGE_OVER_QUOTA, map);
            }
        }
    }

    public void validateCriticalPod(String namespaceName, Deployment deployment, Deployment oldDeployment) {
        Optional<Map<String, String>> oldAnnotations = Optional.ofNullable(oldDeployment)
                .map(Deployment::getSpec)
                .map(DeploymentSpec::getTemplate)
                .map(PodTemplateSpec::getMetadata)
                .map(ObjectMeta::getAnnotations);
        if (oldAnnotations.isPresent()
                && "true".equalsIgnoreCase(oldAnnotations.get().get(CRITICAL_POD_ANNOTATION))) {
            return;
        }

        validateCriticalPod(namespaceName, deployment);
    }

    public void validateCriticalPod(String namespaceName, Deployment deployment) {
        Optional<Map<String, String>> annotations = Optional.ofNullable(deployment)
                .map(Deployment::getSpec)
                .map(DeploymentSpec::getTemplate)
                .map(PodTemplateSpec::getMetadata)
                .map(ObjectMeta::getAnnotations);
        if (annotations.isPresent()) {
            if ("true".equalsIgnoreCase(annotations.get().get(CRITICAL_POD_ANNOTATION))) {
                NamespaceExtras namespaceExtras = namespaceExtrasRepository.findByNameAndDeletedIsFalse(namespaceName);
                if (namespaceExtras == null || !namespaceExtras.getAllowCriticalPod()) {
                    throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_NOT_ALLOW_CRITICAL_POD,
                            ImmutableMap.of("namespace", namespaceName,
                                    "deployment", deployment.getMetadata().getName()));
                }
            }
        }
    }

    public void validateCriticalPod(String namespaceName,
                                    NamespaceExtras namespaceExtras,
                                    List<Deployment> deploymentList) {
        for (Deployment deployment : deploymentList) {
            Optional<Map<String, String>> annotations = Optional.ofNullable(deployment)
                    .map(Deployment::getSpec)
                    .map(DeploymentSpec::getTemplate)
                    .map(PodTemplateSpec::getMetadata)
                    .map(ObjectMeta::getAnnotations);
            if (!annotations.isPresent()) {
                continue;
            }

            if ("true".equalsIgnoreCase(annotations.get().get(CRITICAL_POD_ANNOTATION))) {
                if (namespaceExtras == null || !namespaceExtras.getAllowCriticalPod()) {
                    throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_NOT_ALLOW_CRITICAL_POD,
                            ImmutableMap.of("namespace", namespaceName,
                                    "deployment", deployment.getMetadata().getName()));
                }
            }
        }
    }
}
