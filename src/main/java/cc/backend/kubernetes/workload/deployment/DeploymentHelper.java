package cc.backend.kubernetes.workload.deployment;

import cc.backend.kubernetes.storage.StorageRepository;
import cc.backend.kubernetes.utils.CommonUtils;
import cc.backend.kubernetes.utils.HostpathUtils;
import io.fabric8.kubernetes.api.model.Volume;
import io.fabric8.kubernetes.api.model.extensions.Deployment;

import java.util.List;

/**
 * a util class for deployment
 *
 * @author NormanWang06@gmail.com (jinxin) on 2017/7/6.
 */
public class DeploymentHelper {
    /**
     * judge a deployment is critical or not
     *
     * @param deployment the deployment for check
     * @return is critical or not
     */
    public static boolean isCriticalPod(Deployment deployment) {
        return CommonUtils.isCriticalPod(deployment.getSpec().getTemplate());
    }


    public static long getHostpathBytes(String namespaceName,
                                        Deployment deployment,
                                        StorageRepository storageRepository) {
        if (deployment == null
                || deployment.getSpec().getReplicas() == null
                || deployment.getSpec().getReplicas() == 0) {
            return 0;
        }

        List<Volume> volumes = deployment.getSpec().getTemplate().getSpec().getVolumes();
        Integer replicas = deployment.getSpec().getReplicas();
        return HostpathUtils.getControllerHostPathBytes(namespaceName, replicas, volumes, storageRepository);
    }
}
