package cc.backend.kubernetes.workload.deployment;

import cc.backend.kubernetes.workload.deployment.service.DeploymentsNameManagement;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author yanzhixiang on 17-6-22.
 */
@Component
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes/namespaces/{namespaceName}/apps/{appName}/deployments_name")
@Api(value = "Deployment", description = "Info about deployments.", produces = "application/json")
public class DeploymentsNameEndpoint {
    @Inject
    private DeploymentsNameManagement management;

    @GET
    @ApiOperation(value = "List all deployments name in app.", responseContainer = "List", response = String.class)
    public Response getDeploymentNamesOfApp(@PathParam("namespaceName") String namespaceName,
                                            @PathParam("appName") String appName) {
        List<String> deploymentNames = management.getDeploymentsName(namespaceName, appName);
        return Response.ok(deploymentNames).build();
    }
}
