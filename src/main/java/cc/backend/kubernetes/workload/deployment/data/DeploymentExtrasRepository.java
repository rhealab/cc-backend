package cc.backend.kubernetes.workload.deployment.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author yanzhixiang
 */
public interface DeploymentExtrasRepository extends JpaRepository<DeploymentExtras, Long> {
    DeploymentExtras findByNamespaceAndName(String namespace, String name);

    @Transactional
    Long deleteByNamespaceAndName(String namespace, String name);

    @Transactional
    void deleteByNamespaceAndAppName(String namespace, String appName);

    @Transactional
    void deleteByNamespace(String namespace);

    long countByNamespace(String namespaceName);
}
