package cc.backend.kubernetes.workload.deployment.service;

import cc.backend.kubernetes.KubernetesClientManager;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static cc.backend.kubernetes.Constants.APP_LABEL;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/12/21.
 */
@Component
public class DeploymentsNameManagement {
    private static final Logger logger = LoggerFactory.getLogger(DeploymentsNameManagement.class);

    @Inject
    private KubernetesClientManager clientManager;

    public List<String> getDeploymentsName(String namespaceName, String appName) {
        List<Deployment> deployments = clientManager.getClient()
                .extensions()
                .deployments()
                .inNamespace(namespaceName)
                .withLabel(APP_LABEL, appName)
                .list()
                .getItems();

        return Optional.ofNullable(deployments)
                .orElseGet(ArrayList::new)
                .stream()
                .map(deployment -> deployment.getMetadata().getName())
                .collect(Collectors.toList());
    }
}
