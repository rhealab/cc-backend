package cc.backend.kubernetes.workload.deployment.service;

import cc.backend.audit.operation.OperationAuditMessage;
import cc.backend.audit.operation.OperationAuditMessageProducer;
import cc.backend.audit.operation.OperationType;
import cc.backend.audit.operation.ResourceType;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.storage.StorageRepository;
import cc.backend.kubernetes.storage.StorageWorkloadRepository;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageWorkload;
import cc.backend.kubernetes.storage.domain.WorkloadType;
import cc.backend.kubernetes.utils.CommonUtils;
import cc.backend.kubernetes.utils.ResourceAnnotationsHelper;
import cc.backend.kubernetes.utils.ResourceLabelHelper;
import cc.backend.kubernetes.workload.WorkloadValidator;
import cc.backend.kubernetes.workload.deployment.validator.DeploymentsValidator;
import cc.backend.kubernetes.workload.utils.WorkloadUtils;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.PodTemplateSpec;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.ReplicaSet;
import io.fabric8.kubernetes.client.KubernetesClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

import static cc.backend.kubernetes.Constants.APP_LABEL;

/**
 * Created by xzy on 2017/2/23.
 *
 * @author yanzhixiang modified on 2017-6-7
 */
@Component
public class DeploymentsManagement {
    private static final Logger logger = LoggerFactory.getLogger(DeploymentsManagement.class);

    @Inject
    private KubernetesClientManager clientManager;

    @Inject
    private DeploymentsValidator validator;

    @Inject
    private StorageRepository storageRepository;

    @Inject
    private OperationAuditMessageProducer operationAuditMessageProducer;

    @Inject
    private StorageWorkloadRepository storageWorkloadRepository;

    @Inject
    private WorkloadValidator workloadValidator;

    public List<Deployment> getDeploymentList(String namespaceName, String appName) {
        Map<String, String> labels = new HashMap<>(1);
        if (!StringUtils.isEmpty(appName)) {
            labels.put(APP_LABEL, appName);
        }

        List<Deployment> deployments = clientManager.getClient()
                .extensions()
                .deployments()
                .inNamespace(namespaceName)
                .withLabels(labels)
                .list()
                .getItems();

        return Optional.ofNullable(deployments).orElseGet(ArrayList::new);
    }

    public Deployment getDeployment(String namespaceName, String appName, String deploymentName) {
        return validator.validateDeploymentShouldExists(namespaceName, appName, deploymentName);
    }

    public Deployment createWithAudit(String namespaceName,
                                      String appName,
                                      Deployment deployment) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        create(namespaceName, appName, deployment);

        sendAudit(deployment.getMetadata().getName(), OperationType.CREATE, stopwatch, ImmutableMap.of("appName", appName));
        return deployment;
    }

    public Deployment create(String namespaceName,
                             String appName,
                             Deployment deployment) {
        String deploymentName = deployment.getMetadata().getName();

        validator.validateCriticalPod(namespaceName, deployment);
        validator.validateDeploymentShouldNotExists(namespaceName, deploymentName);
        validator.validateHostpathQuotaForCreate(namespaceName, appName, deployment);
        workloadValidator.validateComputeQuotaForCreate(namespaceName, appName, deployment);

        return createInternal(namespaceName, appName, deployment);
    }

    private Deployment createInternal(String namespaceName,
                                      String appName,
                                      Deployment deployment) {
        ResourceLabelHelper.addLabel(deployment, APP_LABEL, appName);
        ResourceLabelHelper.removeAppLabelOfSelector(deployment);
        //add save deploymentInfo when create new deployment
        ResourceAnnotationsHelper.setExtrasInfoForCreate(deployment);

        deployment = clientManager.getClient()
                .extensions()
                .deployments()
                .inNamespace(namespaceName)
                .create(deployment);

        createStorageRelationship(namespaceName, appName, deployment);

        return deployment;
    }

    private void createStorageRelationship(String namespaceName, String appName, Deployment deployment) {
        List<Storage> storageList = WorkloadUtils.getExistsStorageList(namespaceName, deployment, storageRepository);
        String deploymentName = deployment.getMetadata().getName();
        List<StorageWorkload> storageWorkloadList = WorkloadUtils
                .composeStorageWorkloadList(appName, deploymentName, WorkloadType.Deployment, storageList);

        storageWorkloadRepository.save(storageWorkloadList);
    }

    public Deployment updateWithAudit(String namespaceName,
                                      String appName,
                                      Deployment deployment) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        deployment = update(namespaceName, appName, deployment);

        sendAudit(deployment.getMetadata().getName(), OperationType.UPDATE, stopwatch, new HashMap<>(0));
        return deployment;
    }

    public Deployment update(String namespaceName,
                             String appName,
                             Deployment deployment) {
        String deploymentName = deployment.getMetadata().getName();

        Deployment oldDeployment = validator.validateDeploymentShouldExists(namespaceName, appName, deploymentName);
        validator.validateCriticalPod(namespaceName, deployment, oldDeployment);

        int replicas = WorkloadUtils.getReplicas(deployment);
        List<Storage> storageList = WorkloadUtils.getExistsStorageList(namespaceName, deployment, storageRepository);

        validator.validateHostpathQuotaForUpdate(namespaceName, appName, replicas, storageList, oldDeployment);

        workloadValidator.validateComputeQuotaForUpdate(namespaceName, appName,
                deployment, oldDeployment);

        return updateInternal(namespaceName, appName, deployment, storageList);
    }

    public Deployment updateInternal(String namespaceName,
                                     String appName,
                                     Deployment deployment,
                                     List<Storage> storageList) {
        String deploymentName = deployment.getMetadata().getName();

        ResourceLabelHelper.addLabel(deployment, APP_LABEL, appName);
        ResourceLabelHelper.removeAppLabelOfSelector(deployment);
        ResourceAnnotationsHelper.setExtrasInfoForUpdate(deployment);
        deployment = clientManager.getClient()
                .extensions()
                .deployments()
                .inNamespace(namespaceName)
                .withName(deploymentName)
                .replace(deployment);
        updateStorageRelationship(namespaceName, appName, deploymentName, storageList);

        return deployment;
    }

    private void updateStorageRelationship(String namespaceName,
                                           String appName,
                                           String deploymentName,
                                           List<Storage> storageList) {
        // clear relation first
        storageWorkloadRepository.deleteByNamespaceNameAndWorkloadTypeAndWorkloadName(namespaceName,
                WorkloadType.Deployment, deploymentName);

        //add relation
        List<StorageWorkload> storageWorkloadList = WorkloadUtils
                .composeStorageWorkloadList(appName, deploymentName, WorkloadType.Deployment, storageList);
        storageWorkloadRepository.save(storageWorkloadList);
    }

    public void deleteWithAudit(String namespaceName, String appName, String deploymentName) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        delete(namespaceName, appName, deploymentName);

        sendAudit(deploymentName, OperationType.DELETE, stopwatch, new HashMap<>(0));
    }

    public void delete(String namespaceName, String appName, String deploymentName) {
        validator.validateDeploymentShouldExists(namespaceName, appName, deploymentName);
        deleteK8sDeploymentAndRs(namespaceName, deploymentName);
        storageWorkloadRepository.deleteByNamespaceNameAndWorkloadTypeAndWorkloadName(namespaceName,
                WorkloadType.Deployment, deploymentName);
    }

    private void deleteK8sDeploymentAndRs(String namespaceName, String deploymentName) {
        Deployment deployment = clientManager.getClient()
                .extensions()
                .deployments()
                .inNamespace(namespaceName)
                .withName(deploymentName)
                .edit()
                .editSpec()
                .withReplicas(0)
                .endSpec()
                .done();

        if (deployment.getStatus() != null && deployment.getStatus().getObservedGeneration() != null) {
            Long observedGeneration = deployment.getStatus().getObservedGeneration();
            waitForObservedGeneration(namespaceName, deploymentName, observedGeneration);
        }

        clientManager.getClient()
                .extensions()
                .deployments()
                .inNamespace(namespaceName)
                .withName(deploymentName)
                .cascading(false)
                .delete();

        List<ReplicaSet> replicaSets = clientManager.getClient()
                .extensions()
                .replicaSets()
                .inNamespace(namespaceName)
                .list()
                .getItems();

        PodTemplateSpec deploymentTemplate = deployment.getSpec().getTemplate();
        List<ReplicaSet> relatedReplicaSets = Optional.ofNullable(replicaSets)
                .orElseGet(ArrayList::new)
                .stream()
                .filter(replicaSet -> CommonUtils.equalIgnoreHash(deploymentTemplate, replicaSet.getSpec().getTemplate()))
                .collect(Collectors.toList());

        clientManager.getClient()
                .extensions()
                .replicaSets()
                .inNamespace(namespaceName)
                .delete(relatedReplicaSets);
    }

    private void waitForObservedGeneration(final String namespaceName,
                                           final String deploymentName,
                                           final long observedGeneration) {
        final CountDownLatch countDownLatch = new CountDownLatch(1);

        final Runnable deploymentPoller = () -> {
            Deployment deployment = clientManager.getClient()
                    .extensions()
                    .deployments()
                    .inNamespace(namespaceName)
                    .withName(deploymentName)
                    .get();
            if (observedGeneration <= deployment.getStatus().getObservedGeneration()) {
                countDownLatch.countDown();
            }
        };

        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        ScheduledFuture poller = executor.scheduleWithFixedDelay(deploymentPoller, 0, 10, TimeUnit.MILLISECONDS);
        try {
            countDownLatch.await(1, TimeUnit.MINUTES);
            executor.shutdown();
        } catch (InterruptedException e) {
            poller.cancel(true);
            executor.shutdown();
            throw KubernetesClientException.launderThrowable(e);
        }
    }

    public void addReplicas(String namespaceName,
                            String appName,
                            String deploymentName,
                            int addReplicasNum) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        Deployment deployment = validator.validateDeploymentShouldExists(namespaceName, appName, deploymentName);
        workloadValidator.validateComputeQuotaForUpdate(namespaceName, appName, deployment, addReplicasNum);
        validator.validateHostPathQuotaForUpdate(namespaceName, appName, deployment, addReplicasNum);

        int replicas = WorkloadUtils.getReplicas(deployment);
        deployment.getSpec().setReplicas(replicas + addReplicasNum);

        clientManager.getClient()
                .extensions()
                .deployments()
                .inNamespace(namespaceName)
                .withName(deploymentName)
                .edit()
                .editSpec()
                .withReplicas(replicas + addReplicasNum)
                .endSpec()
                .done();

        sendAudit(deploymentName, OperationType.UPDATE, stopwatch,
                ImmutableMap.of("originReplicas", replicas,
                        "currentReplicas", replicas + addReplicasNum));
    }

    public void reduceReplicas(String namespaceName,
                               String appName,
                               String deploymentName,
                               int reduceReplicasNum) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        Deployment deployment = validator.validateDeploymentShouldExists(namespaceName, appName, deploymentName);
        validator.validateDeploymentReplicas(namespaceName, appName, deployment, reduceReplicasNum);

        int replicas = WorkloadUtils.getReplicas(deployment);

        clientManager.getClient().extensions().deployments().
                inNamespace(namespaceName).
                withName(deploymentName).
                edit().
                editSpec().
                withReplicas(replicas - reduceReplicasNum).
                endSpec().
                done();

        sendAudit(deploymentName, OperationType.UPDATE, stopwatch,
                ImmutableMap.of("originReplicas", replicas,
                        "currentReplicas", replicas - reduceReplicasNum));
    }

    private void sendAudit(String deploymentName,
                           OperationType operationType,
                           Stopwatch stopwatch,
                           Map<String, Object> extras) {
        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .elapsed(elapsed)
                .operationType(operationType)
                .resourceType(ResourceType.DEPLOYMENT)
                .resourceName(deploymentName)
                .extras(extras)
                .build();
        operationAuditMessageProducer.send(message);
    }
}
