package cc.backend.kubernetes.workload.deployment.dto;

import cc.backend.kubernetes.workload.pod.dto.PodDto;
import cc.backend.kubernetes.workload.dto.PodInfo;
import cc.backend.kubernetes.utils.CommonUtils;
import cc.backend.kubernetes.utils.ResourceAnnotationsHelper;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.DeploymentSpec;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author yanzhixiang
 */
public class DeploymentDto {
    private String name;
    private String namespace;
    private String uid;
    private Map<String, String> labels;
    private Map<String, String> selector;
    private PodDto template;
    private Integer replicas;
    private PodInfo podInfo;
    private Date createdOn;
    private String createdBy;
    private long age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    public void setLabels(Map<String, String> labels) {
        this.labels = labels;
    }

    public Map<String, String> getSelector() {
        return selector;
    }

    public void setSelector(Map<String, String> selector) {
        this.selector = selector;
    }

    public PodDto getTemplate() {
        return template;
    }

    public void setTemplate(PodDto template) {
        this.template = template;
    }

    public Integer getReplicas() {
        return replicas;
    }

    public void setReplicas(Integer replicas) {
        this.replicas = replicas;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public PodInfo getPodInfo() {
        return podInfo;
    }

    public void setPodInfo(PodInfo podInfo) {
        this.podInfo = podInfo;
    }

    @Override
    public String toString() {
        return "DeploymentDto{" +
                "name='" + name + '\'' +
                ", namespace='" + namespace + '\'' +
                ", uid='" + uid + '\'' +
                ", labels=" + labels +
                ", selector=" + selector +
                ", template=" + template +
                ", replicas=" + replicas +
                ", podInfo=" + podInfo +
                ", createdOn=" + createdOn +
                ", createdBy=" + createdBy +
                ", age=" + age +
                '}';
    }

    public static List<DeploymentDto> from(List<Deployment> deployments, Map<String, List<Pod>> podsMap) {
        return Optional.ofNullable(deployments)
                .orElseGet(ArrayList::new)
                .stream()
                .map(deployment -> from(deployment, podsMap.get(deployment.getMetadata().getName())))
                .collect(Collectors.toList());
    }

    public static DeploymentDto from(Deployment deployment, List<Pod> podList) {
        ObjectMeta metadata = deployment.getMetadata();
        DeploymentSpec spec = deployment.getSpec();

        DeploymentDto dto = new DeploymentDto();
        dto.setName(metadata.getName());
        dto.setNamespace(metadata.getNamespace());
        dto.setUid(metadata.getUid());
        dto.setLabels(metadata.getLabels());
        dto.setSelector(spec.getSelector().getMatchLabels());
        dto.setTemplate(PodDto.from(spec.getTemplate()));
        dto.setReplicas(spec.getReplicas());
        dto.setPodInfo(CommonUtils.composePodInfo(deployment, podList));

        ResourceAnnotationsHelper.addExtrasInfo(deployment, dto);

        return dto;
    }
}
