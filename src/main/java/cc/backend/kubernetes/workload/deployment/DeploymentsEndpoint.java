package cc.backend.kubernetes.workload.deployment;

import cc.backend.kubernetes.apps.validator.AppValidator;
import cc.backend.kubernetes.batch.form.MixtureManagement;
import cc.backend.kubernetes.batch.form.deprecated.FormDeployment;
import cc.backend.kubernetes.storage.StorageManagement;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.workload.deployment.dto.DeploymentDto;
import cc.backend.kubernetes.workload.deployment.service.DeploymentsManagement;
import cc.backend.kubernetes.batch.form.deprecated.FormDeploymentValidator;
import cc.backend.kubernetes.workload.pod.services.PodsManagement;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * @author wangchunyang@gmail.com
 * @author yanzhixiang modified on 2017-6-7
 */

@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes/namespaces/{namespaceName}/apps/{appName}/deployments")
@Api(value = "Deployment", description = "Info about deployments.", produces = "application/json")
public class DeploymentsEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(DeploymentsEndpoint.class);

    @Inject
    private DeploymentsManagement management;

    @Inject
    private PodsManagement podsManagement;

    @Inject
    private StorageManagement storageManagement;

    @Inject
    private AppValidator appValidator;

    @Inject
    private FormDeploymentValidator validator;

    @Inject
    private MixtureManagement mixtureManagement;

    @GET
    @ApiOperation(value = "List deployments in app.", responseContainer = "List", response = DeploymentDto.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "namespace or app not exists")})
    public Response getDeploymentList(@PathParam("namespaceName") String namespaceName,
                                      @PathParam("appName") String appName) {
        appValidator.validateAppShouldExists(namespaceName, appName);

        List<Deployment> deployments = management.getDeploymentList(namespaceName, appName);
        Map<String, List<Pod>> podsMap = podsManagement.getDeploymentPodsMap(namespaceName, deployments);

        return Response.ok(DeploymentDto.from(deployments, podsMap)).build();
    }

    @GET
    @Path("{deploymentName}")
    @ApiOperation(value = "Get deployment info.", response = DeploymentDto.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "namespace,app or deployment not exists")})
    public Response getDeploymentDetail(@PathParam("namespaceName") String namespaceName,
                                        @PathParam("appName") String appName,
                                        @PathParam("deploymentName") String deploymentName) {
        appValidator.validateAppShouldExists(namespaceName, appName);

        Deployment deployment = management.getDeployment(namespaceName, appName, deploymentName);
        List<Pod> podList = podsManagement.getPodsByDeployment(deployment);

        return Response.ok(DeploymentDto.from(deployment, podList)).build();
    }

    @GET
    @Path("{deploymentName}/json")
    @ApiOperation(value = "Get deployment info.", response = Deployment.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "namespace,app or deployment not exists")})
    public Response getDeploymentJson(@PathParam("namespaceName") String namespaceName,
                                      @PathParam("appName") String appName,
                                      @PathParam("deploymentName") String deploymentName) {
        appValidator.validateAppShouldExists(namespaceName, appName);

        Deployment deployment = management.getDeployment(namespaceName, appName, deploymentName);

        return Response.ok(deployment).build();
    }

    @GET
    @Path("{deploymentName}/form")
    @ApiOperation(value = "Deprecated! Get from deployment.", response = FormDeployment.class)
    @ApiResponses(value = {@ApiResponse(code = 404, message = "namespace,app or deployment not exists")})
    @Deprecated
    public Response getDeploymentForm(@PathParam("namespaceName") String namespaceName,
                                      @PathParam("appName") String appName,
                                      @PathParam("deploymentName") String deploymentName) {
        appValidator.validateAppShouldExists(namespaceName, appName);

        Deployment deployment = management.getDeployment(namespaceName, appName, deploymentName);
        List<Storage> storageList = storageManagement.getStorageListByDeployment(namespaceName, deployment);

        return Response.ok(FormDeployment.from(deployment, storageList)).build();
    }

    @GET
    @Path("{deploymentName}/storage")
    @ApiOperation(value = "List storage in deployment.", responseContainer = "List", response = Storage.class)
    public Response getStorageList(@PathParam("namespaceName") String namespaceName,
                                   @PathParam("appName") String appName,
                                   @PathParam("deploymentName") String deploymentName) {
        List<Storage> storageList = storageManagement.getStorageListByDeploymentName(namespaceName, deploymentName);
        return Response.ok(storageList).build();
    }

    @PUT
    @Path("{deploymentName}")
    @ApiOperation(value = "Update deployment by json.")
    @ApiResponses(value = {@ApiResponse(code = 404, message = "namespace,app or deployment not exists")})
    public Response updateDeploymentByJson(@PathParam("namespaceName") String namespaceName,
                                           @PathParam("appName") String appName,
                                           @PathParam("deploymentName") String deploymentName,
                                           Deployment deployment) {
        management.updateWithAudit(namespaceName, appName, deployment);
        return Response.ok().build();
    }

    @PUT
    @Path("{deploymentName}/form")
    @ApiOperation(value = "Deprecated! Update deployment by json.")
    @ApiResponses(value = {@ApiResponse(code = 404, message = "namespace,app or deployment not exists")})
    @Deprecated
    public Response updateDeploymentByForm(@PathParam("namespaceName") String namespaceName,
                                           @PathParam("appName") String appName,
                                           @PathParam("deploymentName") String deploymentName,
                                           @Valid FormDeployment form) {
        /*
           TODO validate deployment support privilege/hostpath/vip
           validator.validateComputeQuotaForUpdate(namespaceName, appName, form);
         */

        List<Storage> storageList = validator.validateStorageExists(namespaceName, form);
        Deployment deployment = FormDeployment.to(namespaceName, appName, form, storageList);
        mixtureManagement.createStorage(namespaceName, storageList);
        management.updateWithAudit(namespaceName, appName, deployment);
        return Response.ok().build();
    }

    @DELETE
    @Path("{deploymentName}")
    @ApiOperation(value = "Delete specified deployment.")
    @ApiResponses(value = {@ApiResponse(code = 404, message = "namespace,app or deployment not exists")})
    public Response deleteDeployment(@PathParam("namespaceName") String namespaceName,
                                     @PathParam("appName") String appName,
                                     @PathParam("deploymentName") String deploymentName) {
        management.deleteWithAudit(namespaceName, appName, deploymentName);
        logger.info("Delete deployment: name={}", deploymentName);
        return Response.ok(ImmutableMap.of("status", "ok",
                "storageDeleteList", "")).build();
    }

    @PUT
    @Path("{deploymentName}/replicas/{podNum}")
    @ApiOperation(value = "Add deployment replicas.")
    @ApiResponses(value = {@ApiResponse(code = 404, message = "namespace,app or deployment not exists")})
    public Response addReplicas(@PathParam("namespaceName") String namespaceName,
                                @PathParam("appName") String appName,
                                @PathParam("deploymentName") String deploymentName,
                                @PathParam("podNum") int addReplicasNum) {
        management.addReplicas(namespaceName, appName, deploymentName, addReplicasNum);
        return Response.ok().build();
    }

    @DELETE
    @Path("{deploymentName}/replicas/{podNum}")
    @ApiOperation(value = "Reduce deployment replicas.")
    @ApiResponses(value = {@ApiResponse(code = 404, message = "namespace,app or deployment not exists")})
    public Response reduceReplicas(@PathParam("namespaceName") String namespaceName,
                                   @PathParam("appName") String appName,
                                   @PathParam("deploymentName") String deploymentName,
                                   @PathParam("podNum") int reduceReplicasNum) {
        management.reduceReplicas(namespaceName, appName, deploymentName, reduceReplicasNum);
        return Response.ok().build();
    }
}
