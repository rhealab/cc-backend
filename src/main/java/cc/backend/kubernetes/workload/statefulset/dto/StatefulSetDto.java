package cc.backend.kubernetes.workload.statefulset.dto;


import cc.backend.kubernetes.utils.CommonUtils;
import cc.backend.kubernetes.utils.ResourceAnnotationsHelper;
import cc.backend.kubernetes.workload.dto.PodInfo;
import io.fabric8.kubernetes.api.model.Container;
import io.fabric8.kubernetes.api.model.LabelSelector;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import io.fabric8.kubernetes.api.model.extensions.StatefulSetSpec;
import io.fabric8.kubernetes.api.model.extensions.StatefulSetStatus;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Created by mrzihan.tang@gmail.com on 05/07/2017.
 */
public class StatefulSetDto {
    private String name;
    private String uid;
    private String namespace;
    private long age;
    private LabelSelector selector;
    private StatusInfo statusInfo;
    private Map<String, String> labels;

    private List<String> images;
    private PodInfo podInfo;
    private Date createdOn;
    private String createdBy;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public LabelSelector getSelector() {
        return selector;
    }

    public void setSelector(LabelSelector selector) {
        this.selector = selector;
    }

    public StatusInfo getStatusInfo() {
        return statusInfo;
    }

    public void setStatusInfo(StatusInfo statusInfo) {
        this.statusInfo = statusInfo;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    public void setLabels(Map<String, String> labels) {
        this.labels = labels;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public PodInfo getPodInfo() {
        return podInfo;
    }

    public void setPodInfo(PodInfo podInfo) {
        this.podInfo = podInfo;
    }

    @Override
    public String toString() {
        return "StatefulSetDto{" +
                "name='" + name + '\'' +
                ", uid='" + uid + '\'' +
                ", namespace='" + namespace + '\'' +
                ", age=" + age +
                ", selector=" + selector +
                ", statusInfo=" + statusInfo +
                ", labels=" + labels +
                ", images=" + images +
                ", createdOn=" + createdOn +
                '}';
    }

    public static class StatusInfo {
        private int replicas;
        private long observedGeneration;

        public int getReplicas() {
            return replicas;
        }

        public void setReplicas(int replicas) {
            this.replicas = replicas;
        }

        public long getObservedGeneration() {
            return observedGeneration;
        }

        public void setObservedGeneration(long observedGeneration) {
            this.observedGeneration = observedGeneration;
        }

        public static StatusInfo from(StatefulSetStatus status) {
            StatusInfo ret = new StatusInfo();
            ret.setReplicas(Optional.ofNullable(status.getReplicas()).orElse(0));
            ret.setObservedGeneration(Optional.ofNullable(status.getObservedGeneration()).orElse(0L));
            return ret;
        }
    }

    public static List<StatefulSetDto> from(List<StatefulSet> statefulSets, Map<String, List<Pod>> podsMap) {
        return Optional.ofNullable(statefulSets)
                .orElseGet(ArrayList::new)
                .stream()
                .map(statefulSet -> from(statefulSet, podsMap.get(statefulSet.getMetadata().getName())))
                .collect(Collectors.toList());
    }

    public static StatefulSetDto from(StatefulSet statefulSet, List<Pod> podList) {
        ObjectMeta metadata = statefulSet.getMetadata();
        StatefulSetSpec spec = statefulSet.getSpec();
        StatefulSetStatus status = statefulSet.getStatus();

        StatefulSetDto dto = new StatefulSetDto();

        dto.setName(metadata.getName());
        dto.setNamespace(metadata.getNamespace());
        dto.setUid(metadata.getUid());
        dto.setLabels(metadata.getLabels());
        dto.setSelector(spec.getSelector());

        List<String> imageList = Optional.ofNullable(spec.getTemplate().getSpec().getContainers())
                .orElseGet(ArrayList::new)
                .stream()
                .map(Container::getImage)
                .collect(Collectors.toList());
        dto.setImages(imageList);

        dto.setPodInfo(CommonUtils.composePodInfo(statefulSet, podList));
        dto.setStatusInfo(StatusInfo.from(status));

        ResourceAnnotationsHelper.addExtrasInfo(statefulSet, dto);
        return dto;
    }
}
