package cc.backend.kubernetes.workload.statefulset;

import cc.backend.audit.operation.OperationAuditMessage;
import cc.backend.audit.operation.OperationAuditMessageProducer;
import cc.backend.audit.operation.OperationType;
import cc.backend.audit.operation.ResourceType;
import cc.backend.kubernetes.CustomKubernetesClient;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.storage.StorageManagement;
import cc.backend.kubernetes.storage.StorageRepository;
import cc.backend.kubernetes.storage.StorageWorkloadRepository;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageWorkload;
import cc.backend.kubernetes.storage.domain.WorkloadType;
import cc.backend.kubernetes.utils.ResourceAnnotationsHelper;
import cc.backend.kubernetes.utils.ResourceLabelHelper;
import cc.backend.kubernetes.workload.WorkloadValidator;
import cc.backend.kubernetes.workload.utils.WorkloadUtils;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.PersistentVolumeClaim;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static cc.backend.kubernetes.Constants.APP_LABEL;
import static cc.backend.kubernetes.Constants.STORAGE_CLASS_ANNOTATION;

/**
 * @author yanzhixiang on 17-7-12.
 * @author NormanWang06@gmail.com (wangjinxin) modified on 2018/1/12.
 */
@Component
public class StatefulSetManagement {
    private Logger logger = LoggerFactory.getLogger(StatefulSetManagement.class);

    @Inject
    private StatefulSetValidator validator;

    @Inject
    private StorageManagement storageManagement;

    @Inject
    private KubernetesClientManager clientManager;

    @Inject
    private OperationAuditMessageProducer operationAuditMessageProducer;

    @Inject
    private StorageRepository storageRepository;

    @Inject
    private StorageWorkloadRepository storageWorkloadRepository;

    @Inject
    private CustomKubernetesClient customKubernetesClient;

    @Inject
    private WorkloadValidator workloadValidator;

    public List<StatefulSet> getStatefulSets(String namespaceName, String appName) {
        validator.validateStatefulSetSupported();

        List<StatefulSet> statefulSets = clientManager.getClient()
                .apps()
                .statefulSets()
                .inNamespace(namespaceName)
                .withLabel(APP_LABEL, appName)
                .list()
                .getItems();

        return Optional.ofNullable(statefulSets).orElseGet(ArrayList::new);
    }

    public StatefulSet getStatefulSet(String namespaceName, String appName, String statefulSetName) {
        validator.validateStatefulSetSupported();

        return validator.validateStatefulSetShouldExists(namespaceName, appName, statefulSetName);
    }

    public List<Storage> getStorageList(String namespaceName, String statefulSetName) {
        validator.validateStatefulSetSupported();
        List<StorageWorkload> storageWorkloadList = storageWorkloadRepository.
                findByNamespaceNameAndWorkloadTypeAndWorkloadName(namespaceName, WorkloadType.StatefulSet, statefulSetName);
        return storageManagement.getStorageListByStorageWorkload(storageWorkloadList);
    }

    public void createWithAudit(String namespaceName,
                                String appName,
                                StatefulSet statefulSet) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        create(namespaceName, appName, statefulSet);

        sendAudit(statefulSet.getMetadata().getName(), OperationType.CREATE, stopwatch,
                ImmutableMap.of("appName", appName));
    }

    public void create(String namespaceName,
                       String appName,
                       StatefulSet statefulSet) {
        validator.validateStatefulSetSupported();
        validator.validateCriticalPod(namespaceName, statefulSet);
        validator.validateStatefulSetShouldNotExists(namespaceName, appName, statefulSet.getMetadata().getName());
        workloadValidator.validateComputeQuotaForCreate(namespaceName, appName, statefulSet);
        validator.validateHostPathQuotaForCreate(namespaceName, appName, statefulSet);
        validator.validateRbdQuotaForCreate(namespaceName, appName, statefulSet);

        createInternal(namespaceName, appName, statefulSet);
    }

    private StatefulSet createInternal(String namespaceName, String appName, StatefulSet statefulSet) {
        ResourceLabelHelper.addLabel(statefulSet, APP_LABEL, appName);
        ResourceLabelHelper.removeAppLabelOfSelector(statefulSet);

        //add save extras info
        ResourceAnnotationsHelper.setExtrasInfoForCreate(statefulSet);

        setStorageClass(namespaceName, statefulSet);
        statefulSet = clientManager.getClient()
                .apps()
                .statefulSets()
                .inNamespace(namespaceName)
                .create(statefulSet);

        createStorageRelationship(namespaceName, appName, statefulSet);

        return statefulSet;
    }

    private void createStorageRelationship(String namespaceName, String appName, StatefulSet statefulSet) {
        List<Storage> storageList = WorkloadUtils.getExistsStorageList(namespaceName, statefulSet, storageRepository);

        String statefulSetName = statefulSet.getMetadata().getName();
        List<StorageWorkload> storageWorkloadList = WorkloadUtils
                .composeStorageWorkloadList(appName, statefulSetName, WorkloadType.StatefulSet, storageList);
        storageWorkloadRepository.save(storageWorkloadList);
    }


    public StatefulSet updateWithAudit(String namespaceName,
                                       String appName,
                                       StatefulSet statefulSet) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        String statefulSetName = statefulSet.getMetadata().getName();

        update(namespaceName, appName, statefulSet);

        sendAudit(statefulSetName, OperationType.UPDATE, stopwatch, new HashMap<>(0));
        return statefulSet;
    }

    public StatefulSet update(String namespaceName, String appName, StatefulSet statefulSet) {
        String statefulSetName = statefulSet.getMetadata().getName();
        StatefulSet oldStatefulSet = validator.validateStatefulSetShouldExists(namespaceName, appName, statefulSetName);

        validator.validateCriticalPod(namespaceName, statefulSet, oldStatefulSet);
        workloadValidator.validateComputeQuotaForUpdate(namespaceName, appName, statefulSet, oldStatefulSet);

        validator.validateHostPathQuotaForUpdate(namespaceName, appName, statefulSet, oldStatefulSet);
        validator.validateRbdQuotaForUpdate(namespaceName, appName, statefulSet, oldStatefulSet);

        List<Storage> storageList = WorkloadUtils.getExistsStorageList(namespaceName, statefulSet, storageRepository);

        statefulSet = updateInternal(namespaceName, appName, statefulSet, storageList);

        return statefulSet;
    }

    public StatefulSet updateInternal(String namespaceName,
                                      String appName,
                                      StatefulSet statefulSet,
                                      List<Storage> storageList) {
        ResourceLabelHelper.addLabel(statefulSet, APP_LABEL, appName);
        ResourceLabelHelper.removeAppLabelOfSelector(statefulSet);
        ResourceAnnotationsHelper.setExtrasInfoForUpdate(statefulSet);
        updateStorageRelationship(namespaceName, appName, statefulSet, storageList);

        setStorageClass(namespaceName, statefulSet);

        return customKubernetesClient.updateStatefulSet(namespaceName, statefulSet.getMetadata().getName(), statefulSet);
    }

    public void updateStorageRelationship(String namespaceName,
                                          String appName,
                                          StatefulSet statefulSet,
                                          List<Storage> storageList) {
        String statefulSetName = statefulSet.getMetadata().getName();

        // clear relation first
        storageWorkloadRepository.deleteByNamespaceNameAndWorkloadTypeAndWorkloadName(namespaceName,
                WorkloadType.StatefulSet, statefulSetName);

        //add relation
        List<StorageWorkload> storageWorkloadList = WorkloadUtils
                .composeStorageWorkloadList(appName, statefulSetName, WorkloadType.StatefulSet, storageList);
        storageWorkloadRepository.save(storageWorkloadList);
    }

    private void setStorageClass(String namespaceName, StatefulSet statefulSet) {
        Optional.ofNullable(statefulSet.getSpec().getVolumeClaimTemplates())
                .orElseGet(ArrayList::new)
                .stream()
                .filter(Objects::nonNull)
                .map(PersistentVolumeClaim::getMetadata)
                .map(ObjectMeta::getAnnotations)
                .filter(Objects::nonNull)
                .forEach(annotations -> annotations.put(STORAGE_CLASS_ANNOTATION, namespaceName));
    }

    public void deleteWithAudit(String namespaceName, String appName, String statefulSetName) {
        Stopwatch stopwatch = Stopwatch.createStarted();

        delete(namespaceName, appName, statefulSetName);

        sendAudit(statefulSetName, OperationType.DELETE, stopwatch, new HashMap<>(0));
    }

    public void delete(String namespaceName, String appName, String statefulSetName) {
        StatefulSet statefulSet = validator.validateStatefulSetShouldExists(namespaceName, appName, statefulSetName);

        Integer originReplicas = statefulSet.getSpec().getReplicas();
        if (originReplicas == null) {
            originReplicas = 0;
        }
        statefulSet.getSpec().setReplicas(0);
        statefulSet = customKubernetesClient.updateStatefulSet(namespaceName, statefulSetName, statefulSet);

        Integer currentReplicas = statefulSet.getSpec().getReplicas();
        if (currentReplicas == null || currentReplicas == 0) {
            for (int i = 0; i < originReplicas; i++) {
                String podName = statefulSetName + "-" + String.valueOf(i);
                clientManager.getClient()
                        .pods()
                        .inNamespace(namespaceName)
                        .withName(podName)
                        .delete();
            }
            customKubernetesClient.deleteStatefulSet(namespaceName, statefulSetName);
        }

        storageWorkloadRepository.deleteByNamespaceNameAndWorkloadTypeAndWorkloadName(namespaceName,
                WorkloadType.StatefulSet, statefulSetName);
    }

    public void addReplicas(String namespaceName, String appName, String statefulSetName, int addReplicasNum) {
        if (addReplicasNum <= 0) {
            return;
        }
        Stopwatch stopwatch = Stopwatch.createStarted();

        StatefulSet statefulSet = validator.validateStatefulSetShouldExists(namespaceName, appName, statefulSetName);
        int replicas = WorkloadUtils.getReplicas(statefulSet);
        statefulSet.getSpec().setReplicas(replicas + addReplicasNum);

        validator.validateHostPathQuotaForUpdate(namespaceName, appName, statefulSet, addReplicasNum);
        validator.validateRbdQuotaForUpdate(namespaceName, appName, statefulSet);

        workloadValidator.validateComputeQuotaForUpdate(namespaceName, appName, statefulSet, addReplicasNum);

        statefulSet = customKubernetesClient.updateStatefulSet(namespaceName, statefulSetName, statefulSet);

        updateStorageRelationship(namespaceName, appName, statefulSet, replicas, replicas + addReplicasNum);

        sendAudit(statefulSetName, OperationType.UPDATE, stopwatch,
                ImmutableMap.of("originReplicas", replicas,
                        "currentReplicas", replicas + addReplicasNum));
    }

    public void reduceReplicas(String namespaceName, String appName, String statefulSetName, int reduceReplicasNum) {
        if (reduceReplicasNum <= 0) {
            return;
        }
        Stopwatch stopwatch = Stopwatch.createStarted();

        StatefulSet statefulSet = validator.validateStatefulSetShouldExists(namespaceName, appName, statefulSetName);
        validator.validateStatefulSetReplicas(namespaceName, appName, statefulSet, reduceReplicasNum);

        int replicas = WorkloadUtils.getReplicas(statefulSet);
        statefulSet.getSpec().setReplicas(replicas - reduceReplicasNum);

        statefulSet = customKubernetesClient.updateStatefulSet(namespaceName, statefulSetName, statefulSet);

        updateStorageRelationship(namespaceName, appName, statefulSet, replicas, replicas - reduceReplicasNum);

        sendAudit(statefulSetName, OperationType.UPDATE, stopwatch,
                ImmutableMap.of("originReplicas", replicas,
                        "currentReplicas", replicas - reduceReplicasNum));
    }

    private void updateStorageRelationship(String namespaceName,
                                           String appName,
                                           StatefulSet statefulSet,
                                           int oldReplicas,
                                           int newReplicas) {
        String statefulSetName = statefulSet.getMetadata().getName();
        int min = Math.min(oldReplicas, newReplicas);
        int max = Math.max(oldReplicas, newReplicas);
        List<String> pvcNameList = Optional.ofNullable(statefulSet.getSpec().getVolumeClaimTemplates())
                .orElseGet(ArrayList::new)
                .stream()
                .map(p -> p.getMetadata().getName())
                .filter(Objects::nonNull)
                .map(claimName -> IntStream.range(min, max)
                        .mapToObj(num -> StatefulSetHelper.getClaimTemplatePvcName(statefulSetName, claimName, num))
                        .collect(Collectors.toList()))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        List<Storage> storageList = new ArrayList<>();
        if (!pvcNameList.isEmpty()) {
            storageList = storageRepository.findByNamespaceNameAndPvcNameIn(namespaceName, pvcNameList);
        }
        List<String> storageNameList = storageList
                .stream()
                .map(Storage::getStorageName)
                .collect(Collectors.toList());
        if (oldReplicas > newReplicas) {
            if (!storageNameList.isEmpty()) {
                storageWorkloadRepository.deleteByNamespaceNameAndWorkloadTypeAndWorkloadNameAndStorageNameIn
                        (namespaceName, WorkloadType.StatefulSet, statefulSetName, storageNameList);
            }
        } else {
            List<StorageWorkload> storageWorkloadList = WorkloadUtils
                    .composeStorageWorkloadList(appName, statefulSetName, WorkloadType.StatefulSet, storageList);
            storageWorkloadRepository.save(storageWorkloadList);
        }
    }

    private void sendAudit(String statefulSetName,
                           OperationType operationType,
                           Stopwatch stopwatch,
                           Map<String, Object> extras) {
        long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
        OperationAuditMessage message = new OperationAuditMessage.Builder()
                .elapsed(elapsed)
                .operationType(operationType)
                .resourceType(ResourceType.STATEFULSET)
                .resourceName(statefulSetName)
                .extras(extras)
                .build();
        operationAuditMessageProducer.send(message);
    }
}
