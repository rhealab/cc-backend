package cc.backend.kubernetes.workload.statefulset;

import cc.backend.EnnProperties;
import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.namespaces.data.NamespaceExtras;
import cc.backend.kubernetes.namespaces.data.NamespaceExtrasRepository;
import cc.backend.kubernetes.namespaces.services.NamespaceStatsManagement;
import cc.backend.kubernetes.storage.StorageRepository;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageType;
import cc.backend.kubernetes.utils.UnitUtils;
import cc.backend.kubernetes.workload.utils.WorkloadUtils;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.PersistentVolumeClaim;
import io.fabric8.kubernetes.api.model.PodTemplateSpec;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import io.fabric8.kubernetes.api.model.extensions.StatefulSetSpec;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.*;

import static cc.backend.kubernetes.Constants.*;
import static cc.backend.kubernetes.storage.domain.StorageType.HostPath;

/**
 * @author yangzhixiang on 17-7-12.
 */
@Component
public class StatefulSetValidator {
    @Inject
    private KubernetesClientManager clientManager;

    @Inject
    private StorageRepository storageRepository;

    @Inject
    private NamespaceStatsManagement namespaceStatsManagement;

    @Inject
    private EnnProperties properties;

    @Inject
    private NamespaceExtrasRepository namespaceExtrasRepository;

    public void validateStatefulSetReplicas(String namespaceName,
                                            String appName,
                                            StatefulSet statefulSet,
                                            int reduceReplicasNum) {
        int replicas = WorkloadUtils.getReplicas(statefulSet);
        if (replicas < reduceReplicasNum) {
            String statefulSetName = statefulSet.getMetadata().getName();
            throw new CcException(BackendReturnCodeNameConstants.PROPERTY_INVALID,
                    ImmutableMap.of("namespace", namespaceName,
                            "app", appName,
                            "statefulSet", statefulSetName,
                            "currentReplicas", replicas,
                            "reduceNum", reduceReplicasNum));
        }
    }

    //TODO legacy namespace should not support
    public void validateStatefulSetSupported() {
        if (!properties.getCurrentCeph().isEnabled()) {
            throw new CcException(BackendReturnCodeNameConstants.STATEFULSET_NOT_SUPPORTED);
        }
    }

    public void validateCriticalPod(String namespaceName, StatefulSet statefulSet) {
        NamespaceExtras namespaceExtras = namespaceExtrasRepository.findByNameAndDeletedIsFalse(namespaceName);
        Optional<Map<String, String>> annotations = Optional.ofNullable(statefulSet)
                .map(StatefulSet::getSpec)
                .map(StatefulSetSpec::getTemplate)
                .map(PodTemplateSpec::getMetadata)
                .map(ObjectMeta::getAnnotations);

        if (annotations.isPresent()) {
            if ("true".equalsIgnoreCase(annotations.get().get(CRITICAL_POD_ANNOTATION))) {
                if (namespaceExtras == null || !namespaceExtras.getAllowCriticalPod()) {
                    throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_NOT_ALLOW_CRITICAL_POD,
                            ImmutableMap.of("namespace", namespaceName,
                                    "statefulSet", statefulSet.getMetadata().getName()));
                }
            }
        }
    }

    public void validateCriticalPod(String namespaceName,
                                    StatefulSet statefulSet,
                                    StatefulSet oldStatefulSet) {
        Optional<Map<String, String>> oldAnnotations = Optional.ofNullable(oldStatefulSet)
                .map(StatefulSet::getSpec)
                .map(StatefulSetSpec::getTemplate)
                .map(PodTemplateSpec::getMetadata)
                .map(ObjectMeta::getAnnotations);
        if (oldAnnotations.isPresent()
                && "true".equalsIgnoreCase(oldAnnotations.get().get(CRITICAL_POD_ANNOTATION))) {
            return;
        }

        validateCriticalPod(namespaceName, statefulSet);
    }

    public void validateCriticalPod(String namespaceName,
                                    NamespaceExtras namespaceExtras,
                                    List<StatefulSet> statefulSetList) {
        for (StatefulSet statefulSet : statefulSetList) {
            Optional<Map<String, String>> annotations = Optional.ofNullable(statefulSet)
                    .map(StatefulSet::getSpec)
                    .map(StatefulSetSpec::getTemplate)
                    .map(PodTemplateSpec::getMetadata)
                    .map(ObjectMeta::getAnnotations);

            if (!annotations.isPresent()) {
                continue;
            }
            if ("true".equalsIgnoreCase(annotations.get().get(CRITICAL_POD_ANNOTATION))) {
                if (namespaceExtras == null || !namespaceExtras.getAllowCriticalPod()) {
                    throw new CcException(BackendReturnCodeNameConstants.NAMESPACE_NOT_ALLOW_CRITICAL_POD,
                            ImmutableMap.of("namespace", namespaceName,
                                    "statefulSet", statefulSet.getMetadata().getName()));
                }
            }
        }
    }

    public void validateStatefulSetShouldNotExists(String namespaceName, String appName, String statefulSetName) {
        StatefulSet statefulSet = clientManager.getClient()
                .apps()
                .statefulSets()
                .inNamespace(namespaceName)
                .withName(statefulSetName)
                .get();
        if (statefulSet != null) {
            throw new CcException(BackendReturnCodeNameConstants.STATEFULSET_ALREADY_EXISTS,
                    ImmutableMap.of("namespace", namespaceName,
                            "app", appName,
                            "statefulset", statefulSetName));
        }
    }

    public StatefulSet validateStatefulSetShouldExists(String namespaceName, String appName, String statefulSetName) {
        StatefulSet statefulSet = clientManager.getClient().apps().statefulSets().
                inNamespace(namespaceName).withName(statefulSetName).get();
        if (statefulSet != null) {
            Map<String, String> labels = statefulSet.getMetadata().getLabels();
            if (labels != null && appName.equals(labels.get(APP_LABEL))) {
                return statefulSet;
            }
        }

        throw new CcException(BackendReturnCodeNameConstants.STATEFULSET_NOT_EXISTS_IN_APP,
                ImmutableMap.of("namespace", namespaceName,
                        "app", appName,
                        "statefulSet", statefulSetName));
    }

    public void validateHostPathQuotaForCreate(String namespaceName, String appName, StatefulSet statefulSet) {
        long neededHostpathBytes = getHostpathNeededBytes(namespaceName, statefulSet);
        validateHostPathQuota(namespaceName, appName, statefulSet.getMetadata().getName(), neededHostpathBytes);
    }

    public void validateRbdQuotaForCreate(String namespaceName, String appName, StatefulSet statefulSet) {
        long neededBytes = getRbdNeededBytes(namespaceName, statefulSet);
        validateRbdQuota(namespaceName, appName, statefulSet.getMetadata().getName(), neededBytes);
    }

    public void validateHostPathQuotaForUpdate(String namespaceName,
                                               String appName,
                                               StatefulSet statefulSet,
                                               StatefulSet oldStatefulSet) {
        long newNeededBytes = getHostpathNeededBytes(namespaceName, statefulSet);
        long oldNeededBytes = getHostpathNeededBytes(namespaceName, oldStatefulSet);

        long neededBytes = newNeededBytes - oldNeededBytes;
        validateHostPathQuota(namespaceName, appName, statefulSet.getMetadata().getName(), neededBytes);
    }

    public void validateHostPathQuotaForUpdate(String namespaceName,
                                               String appName,
                                               StatefulSet statefulSet,
                                               int addReplicas) {
        long neededBytes = WorkloadUtils.getExistsStorageList(namespaceName, statefulSet, storageRepository)
                .stream()
                .filter(storage -> storage.getStorageType() == HostPath && storage.isUnshared())
                .mapToLong(Storage::getAmountBytes)
                .sum() * addReplicas;

        validateHostPathQuota(namespaceName, appName, statefulSet.getMetadata().getName(), neededBytes);
    }

    public void validateRbdQuotaForUpdate(String namespaceName,
                                          String appName,
                                          StatefulSet statefulSet,
                                          StatefulSet oldStatefulSet) {
        long newNeededBytes = getRbdNeededBytes(namespaceName, statefulSet);
        long oldNeededBytes = getRbdNeededBytes(namespaceName, oldStatefulSet);

        long neededBytes = newNeededBytes - oldNeededBytes;

        validateRbdQuota(namespaceName, appName, statefulSet.getMetadata().getName(), neededBytes);
    }

    public void validateRbdQuotaForUpdate(String namespaceName,
                                          String appName,
                                          StatefulSet statefulSet) {
        validateRbdQuotaForCreate(namespaceName, appName, statefulSet);
    }

    //FIXME multi StatefulSet use same hostpath
    private long getHostpathNeededBytes(String namespaceName, StatefulSet statefulSet) {
        int replicas = WorkloadUtils.getReplicas(statefulSet);
        if (replicas <= 1) {
            return 0;
        }

        return WorkloadUtils.getExistsStorageList(namespaceName, statefulSet, storageRepository)
                .stream()
                .filter(storage -> storage.getStorageType() == HostPath && storage.isUnshared())
                .mapToLong(Storage::getAmountBytes)
                .sum() * (replicas - 1);
    }

    private long getRbdNeededBytes(String namespaceName, StatefulSet statefulSet) {
        int replicas = WorkloadUtils.getReplicas(statefulSet);
        if (replicas <= 0) {
            return 0;
        }

        String statefulSetName = statefulSet.getMetadata().getName();
        List<PersistentVolumeClaim> templateList = statefulSet.getSpec().getVolumeClaimTemplates();

        return templateList.stream()
                .filter(Objects::nonNull)
                .filter(template -> template.getMetadata().getName() != null)
                .mapToLong(template -> {
                    String templateName = template.getMetadata().getName();
                    List<String> templatePvcNameList = StatefulSetHelper
                            .getClaimTemplatePvcNameList(statefulSetName, templateName, replicas);
                    List<Storage> storageList = new ArrayList<>();
                    if (!templatePvcNameList.isEmpty()) {
                        storageList = storageRepository
                                .findByNamespaceNameAndPvcNameIn(namespaceName, templatePvcNameList);
                    }
                    int needStorageNum = templatePvcNameList.size() - storageList.size();
                    if (needStorageNum > 0) {
                        return needStorageNum * UnitUtils
                                .parseK8sStorageQuantity(template.getSpec().getResources().getRequests().get(K_STORAGE));
                    }
                    return 0;
                })
                .sum();
    }

    private void validateHostPathQuota(String namespaceName,
                                       String appName,
                                       String statefulSetName,
                                       long neededBytes) {
        if (neededBytes > 0) {
            long availableByte = namespaceStatsManagement
                    .getNamespaceStorageAvailableBytes(namespaceName, StorageType.HostPath);

            if (neededBytes > availableByte) {
                Map<String, Object> map = new ImmutableMap.Builder<String, Object>()
                        .put("namespace", namespaceName)
                        .put("app", appName)
                        .put("statefulSet", statefulSetName)
                        .put("neededBytes", neededBytes)
                        .put("limitsBytes", availableByte)
                        .build();

                throw new CcException(BackendReturnCodeNameConstants.STATEFULSET_HOSTPATH_STORAGE_OVER_QUOTA, map);
            }
        }
    }

    private void validateRbdQuota(String namespaceName,
                                  String appName,
                                  String statefulSetName,
                                  long neededBytes) {
        if (neededBytes > 0) {
            long availableByte = namespaceStatsManagement.getNamespaceStorageAvailableBytes(namespaceName, StorageType.RBD);
            if (neededBytes > availableByte) {
                Map<String, Object> map = new ImmutableMap.Builder<String, Object>()
                        .put("namespace", namespaceName)
                        .put("app", appName)
                        .put("statefulSet", statefulSetName)
                        .put("neededBytes", neededBytes)
                        .put("limitsBytes", availableByte)
                        .build();

                throw new CcException(BackendReturnCodeNameConstants.STATEFULSET_CEPH_RBD_STORAGE_OVER_QUOTA, map);
            }
        }
    }
}
