package cc.backend.kubernetes.workload.statefulset.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by xzy on 17-7-13.
 */
public interface StatefulSetExtrasRepository extends JpaRepository<StatefulSetExtras, Long> {
    @Transactional
    Long deleteByNamespaceAndName(String namespace, String name);

    StatefulSetExtras findByNamespaceAndName(String namespace, String name);
}
