package cc.backend.kubernetes.workload.statefulset;

import cc.backend.kubernetes.storage.StorageRepository;
import cc.backend.kubernetes.utils.CommonUtils;
import cc.backend.kubernetes.utils.HostpathUtils;
import cc.backend.kubernetes.workload.utils.WorkloadUtils;
import io.fabric8.kubernetes.api.model.PersistentVolumeClaim;
import io.fabric8.kubernetes.api.model.Volume;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * a util class for stateful set
 *
 * @author NormanWang06@gmail.com (jinxin) on 2017/7/6.
 */
public class StatefulSetHelper {
    /**
     * judge a stateful set is critical or not
     *
     * @param statefulSet the stateful set for check
     * @return is critical or not
     */
    public static boolean isCriticalPod(StatefulSet statefulSet) {
        return CommonUtils.isCriticalPod(statefulSet.getSpec().getTemplate());
    }


    public static long getHostpathBytes(String namespaceName,
                                        StatefulSet statefulSet,
                                        StorageRepository storageRepository) {
        if (statefulSet == null
                || statefulSet.getSpec().getReplicas() == null
                || statefulSet.getSpec().getReplicas() == 0) {
            return 0;
        }

        List<Volume> volumes = statefulSet.getSpec().getTemplate().getSpec().getVolumes();
        Integer replicas = statefulSet.getSpec().getReplicas();
        return HostpathUtils.getControllerHostPathBytes(namespaceName, replicas, volumes, storageRepository);
    }

    public static List<String> getClaimTemplatePvcNameList(StatefulSet statefulSet) {
        int replicas = WorkloadUtils.getReplicas(statefulSet);
        String statefulSetName = statefulSet.getMetadata().getName();
        List<PersistentVolumeClaim> volumeClaimTemplates = statefulSet.getSpec().getVolumeClaimTemplates();
        return Optional.ofNullable(volumeClaimTemplates)
                .orElseGet(ArrayList::new)
                .stream()
                .map(template -> template.getMetadata().getName())
                .map(claimName -> getClaimTemplatePvcNameList(statefulSetName, claimName, replicas))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    public static List<String> getClaimTemplatePvcNameList(String statefulSetName, String claimTemplateName, int replicas) {
        return IntStream.range(0, replicas)
                .mapToObj(num -> getClaimTemplatePvcName(statefulSetName, claimTemplateName, num))
                .collect(Collectors.toList());
    }

    public static String getClaimTemplatePvcName(String statefulSetName, String claimTemplateName, int num) {
        return claimTemplateName + "-" + statefulSetName + "-" + num;
    }
}
