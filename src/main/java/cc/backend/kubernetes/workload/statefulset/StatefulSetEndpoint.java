package cc.backend.kubernetes.workload.statefulset;

import cc.backend.kubernetes.apps.validator.AppValidator;
import cc.backend.kubernetes.batch.BatchManagement;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.workload.pod.services.PodsManagement;
import cc.backend.kubernetes.workload.statefulset.dto.StatefulSetDto;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * @author yanzhixiang on 17-7-12.
 * @author NormanWang06@gmail.com (wangjinxin) modified on 2018/1/12.
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes/namespaces/{namespaceName}/apps/{appName}/statefulsets")
@Api(value = "StatefulSet", description = "Info about statefulsets.", produces = "application/json")
public class StatefulSetEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(StatefulSetEndpoint.class);

    @Inject
    private AppValidator appValidator;

    @Inject
    private PodsManagement podsManagement;

    @Inject
    private StatefulSetManagement management;

    @Inject
    private BatchManagement batchManagement;

    @GET
    @ApiOperation(value = "List statefulsets in app.", responseContainer = "List", response = StatefulSetDto.class)
    public Response getStatefulSets(@PathParam("namespaceName") String namespaceName,
                                    @PathParam("appName") String appName) {
        appValidator.validateAppShouldExists(namespaceName, appName);
        List<StatefulSet> statefulSets = management.getStatefulSets(namespaceName, appName);
        Map<String, List<Pod>> podsMap = podsManagement.getStatefulSetPodsMap(namespaceName, statefulSets);

        return Response.ok(StatefulSetDto.from(statefulSets, podsMap)).build();
    }

    @GET
    @Path("{statefulSetName}")
    @ApiOperation(value = "Get statefulset info.", response = StatefulSetDto.class)
    public Response getStatefulSet(@PathParam("namespaceName") String namespaceName,
                                   @PathParam("appName") String appName,
                                   @PathParam("statefulSetName") String statefulSetName) {
        appValidator.validateAppShouldExists(namespaceName, appName);
        StatefulSet statefulSet = management.getStatefulSet(namespaceName, appName, statefulSetName);
        List<Pod> podList = podsManagement.getPodsByStatefulSet(namespaceName, appName, statefulSetName);

        return Response.ok(StatefulSetDto.from(statefulSet, podList)).build();
    }

    @GET
    @Path("{statefulSetName}/json")
    @ApiOperation(value = "Get statefulset info of k8s origin format.", response = StatefulSet.class)
    public Response getStatefulSetJson(@PathParam("namespaceName") String namespaceName,
                                       @PathParam("appName") String appName,
                                       @PathParam("statefulSetName") String statefulSetName) {
        StatefulSet statefulSet = management.getStatefulSet(namespaceName, appName, statefulSetName);
        return Response.ok(statefulSet).build();
    }


    @GET
    @Path("{statefulSetName}/storage")
    @ApiOperation(value = "List storage in deployment.", responseContainer = "List", response = Storage.class)
    public Response getStorageList(@PathParam("namespaceName") String namespaceName,
                                   @PathParam("appName") String appName,
                                   @PathParam("statefulSetName") String statefulSetName) {
        List<Storage> storageList = management.getStorageList(namespaceName, statefulSetName);
        return Response.ok(storageList).build();
    }

    @PUT
    @Path("{statefulSetName}")
    @ApiOperation(value = "Get statefulset info of k8s original format.", response = StatefulSet.class)
    public Response updateStatefulSetByJson(@PathParam("namespaceName") String namespaceName,
                                            @PathParam("appName") String appName,
                                            @PathParam("statefulSetName") String statefulSetName,
                                            StatefulSet statefulSet) {
        management.updateWithAudit(namespaceName, appName, statefulSet);
        return Response.ok().build();
    }

    @DELETE
    @Path("{statefulSetName}")
    @ApiOperation(value = "Delete specified statefulset.")
    public Response deleteStatefulSet(@PathParam("namespaceName") String namespace,
                                      @PathParam("appName") String appName,
                                      @PathParam("statefulSetName") String statefulSetName) {
        management.deleteWithAudit(namespace, appName, statefulSetName);
        logger.info("Delete statefulSet: name={}", statefulSetName);
        return Response.ok(ImmutableMap.of("status", "ok",
                "storageDeleteList", "")).build();
    }

    @PUT
    @Path("{statefulSetName}/replicas/{podNum}")
    @ApiOperation(value = "Add statefulset replicas.")
    public Response addReplicas(@PathParam("namespaceName") String namespaceName,
                                @PathParam("appName") String appName,
                                @PathParam("statefulSetName") String statefulSetName,
                                @PathParam("podNum") int podNum) {
        appValidator.validateAppShouldExists(namespaceName, appName);
        management.addReplicas(namespaceName, appName, statefulSetName, podNum);
        return Response.ok().build();
    }

    @DELETE
    @Path("{statefulSetName}/replicas/{podNum}")
    @ApiOperation(value = "Reduce statefulset replicas.")
    public Response reduceReplicas(@PathParam("namespaceName") String namespaceName,
                                   @PathParam("appName") String appName,
                                   @PathParam("statefulSetName") String statefulSetName,
                                   @PathParam("podNum") int podNum) {
        appValidator.validateAppShouldExists(namespaceName, appName);
        management.reduceReplicas(namespaceName, appName, statefulSetName, podNum);
        return Response.ok().build();
    }

    @POST
    @Path("yml")
    @ApiOperation(value = "Deprecated! Create statefulset by yml.")
    public Response createFormYml(@PathParam("namespaceName") String namespaceName,
                                  @PathParam("appName") String appName,
                                  Object ymlStr) {
        appValidator.validateAppShouldExists(namespaceName, appName);

        batchManagement.createFromK8sYaml(namespaceName, appName, ymlStr.toString());
        return Response.ok().build();
    }

    @POST
    @Path("json")
    @ApiOperation(value = "Deprecated! Create statefulset by json.")
    public Response createFromJson(@PathParam("namespaceName") String namespaceName,
                                   @PathParam("appName") String appName,
                                   Object json) {
        appValidator.validateAppShouldExists(namespaceName, appName);

        batchManagement.createFromK8sJson(namespaceName, appName, json.toString());
        return Response.ok().build();
    }
}
