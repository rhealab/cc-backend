package cc.backend.kubernetes.workload;

import cc.backend.common.utils.ErrorExtrasUtils;
import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.namespaces.utils.NamespaceUtils;
import cc.backend.kubernetes.storage.domain.WorkloadType;
import cc.backend.kubernetes.utils.UnitUtils;
import cc.backend.kubernetes.workload.utils.WorkloadUtils;
import cc.lib.retcode.CcException;
import io.fabric8.kubernetes.api.model.*;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.DeploymentSpec;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import io.fabric8.kubernetes.api.model.extensions.StatefulSetSpec;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.*;
import java.util.function.Supplier;

import static cc.backend.retcode.ReturnCodeResourceType.DEPLOYMENT;
import static cc.backend.retcode.ReturnCodeResourceType.STATEFUL_SET;
import static cc.backend.kubernetes.Constants.*;

/**
 * TODO validate workload field
 *
 * @author NormanWang06@gmail.com (wangjinxin) on 2018/1/4.
 */
@Component
public class WorkloadValidator {

    @Inject
    private KubernetesClientManager clientManager;

    public void validateComputeQuotaForCreate(String namespaceName,
                                              String appName,
                                              Deployment deployment) {
        Optional<DeploymentSpec> deploymentSpec = Optional.ofNullable(deployment)
                .map(Deployment::getSpec);

        int replicas = deploymentSpec.map(DeploymentSpec::getReplicas).orElse(0);
        if (replicas == 0) {
            return;
        }

        List<Container> containers = deploymentSpec
                .map(DeploymentSpec::getTemplate)
                .map(PodTemplateSpec::getSpec)
                .map(PodSpec::getContainers)
                .orElseGet(ArrayList::new);
        if (containers.isEmpty()) {
            return;
        }

        String deploymentName = deployment.getMetadata().getName();

        validateComputeQuotaFormat(WorkloadType.Deployment, containers,
                () -> ErrorExtrasUtils.composeErrorExtras(namespaceName, appName, deploymentName, DEPLOYMENT));

        Map<String, Number> computeNeedMap = WorkloadUtils.getComputeQuotaMap(containers, replicas);
        Map<String, Number> computeAvailableMap = NamespaceUtils.getNamespaceComputeAvailable(clientManager, namespaceName);
        compare(WorkloadType.Deployment, computeNeedMap, computeAvailableMap,
                () -> ErrorExtrasUtils.composeErrorExtras(namespaceName, appName, deploymentName, DEPLOYMENT));
    }

    public void validateComputeQuotaForUpdate(String namespaceName,
                                              String appName,
                                              Deployment newDeployment,
                                              Deployment oldDeployment) {
        DeploymentSpec newDeploymentSpec = newDeployment.getSpec();

        int newReplicas = WorkloadUtils.getReplicas(newDeploymentSpec);
        if (newReplicas == 0) {
            return;
        }

        List<Container> newContainers = WorkloadUtils.getContainers(newDeploymentSpec);
        if (newContainers.isEmpty()) {
            return;
        }

        String deploymentName = newDeployment.getMetadata().getName();

        validateComputeQuotaFormat(WorkloadType.Deployment, newContainers,
                () -> ErrorExtrasUtils.composeErrorExtras(namespaceName, appName, deploymentName, DEPLOYMENT));

        DeploymentSpec oldDeploymentSpec = oldDeployment.getSpec();
        int oldReplicas = WorkloadUtils.getReplicas(oldDeploymentSpec);
        List<Container> oldContainers = WorkloadUtils.getContainers(oldDeploymentSpec);

        Map<String, Number> newComputeMap = WorkloadUtils.getComputeQuotaMap(newContainers, newReplicas);
        Map<String, Number> oldComputeMap = WorkloadUtils.getComputeQuotaMap(oldContainers, oldReplicas);

        Map<String, Number> computeNeedMap = composeNeededComputeMap(newComputeMap, oldComputeMap);
        Map<String, Number> computeAvailableMap = NamespaceUtils.getNamespaceComputeAvailable(clientManager, namespaceName);

        compare(WorkloadType.Deployment, computeNeedMap, computeAvailableMap,
                () -> ErrorExtrasUtils.composeErrorExtras(namespaceName, appName, deploymentName, DEPLOYMENT));
    }

    public void validateComputeQuotaForUpdate(String namespaceName,
                                              String appName,
                                              Deployment oldDeployment,
                                              int addReplicas) {
        DeploymentSpec deploymentSpec = oldDeployment.getSpec();

        List<Container> containers = WorkloadUtils.getContainers(deploymentSpec);
        if (containers.isEmpty()) {
            return;
        }

        Map<String, Number> computeNeedMap = WorkloadUtils.getComputeQuotaMap(containers, addReplicas);
        Map<String, Number> computeAvailableMap = NamespaceUtils.getNamespaceComputeAvailable(clientManager, namespaceName);

        String deploymentName = oldDeployment.getMetadata().getName();
        compare(WorkloadType.Deployment, computeNeedMap, computeAvailableMap,
                () -> ErrorExtrasUtils.composeErrorExtras(namespaceName, appName, deploymentName, DEPLOYMENT));
    }

    public void validateComputeQuotaForCreate(String namespaceName,
                                              String appName,
                                              StatefulSet statefulSet) {
        Optional<StatefulSetSpec> statefulSetSpec = Optional.ofNullable(statefulSet)
                .map(StatefulSet::getSpec);

        int replicas = statefulSetSpec.map(StatefulSetSpec::getReplicas).orElse(0);
        if (replicas == 0) {
            return;
        }

        List<Container> containers = statefulSetSpec
                .map(StatefulSetSpec::getTemplate)
                .map(PodTemplateSpec::getSpec)
                .map(PodSpec::getContainers)
                .orElseGet(ArrayList::new);
        if (containers.isEmpty()) {
            return;
        }

        String statefulName = statefulSet.getMetadata().getName();

        validateComputeQuotaFormat(WorkloadType.StatefulSet, containers,
                () -> ErrorExtrasUtils.composeErrorExtras(namespaceName, appName, statefulName, STATEFUL_SET));

        Map<String, Number> computeNeedMap = WorkloadUtils.getComputeQuotaMap(containers, replicas);
        Map<String, Number> computeAvailableMap = NamespaceUtils.getNamespaceComputeAvailable(clientManager, namespaceName);
        compare(WorkloadType.Deployment, computeNeedMap, computeAvailableMap,
                () -> ErrorExtrasUtils.composeErrorExtras(namespaceName, appName, statefulName, STATEFUL_SET));
    }

    public void validateComputeQuotaForUpdate(String namespaceName,
                                              String appName,
                                              StatefulSet newStatefulSet,
                                              StatefulSet oldStatefulSet) {
        StatefulSetSpec newStatefulSetSpec = newStatefulSet.getSpec();

        int newReplicas = WorkloadUtils.getReplicas(newStatefulSetSpec);
        if (newReplicas == 0) {
            return;
        }

        List<Container> newContainers = WorkloadUtils.getContainers(newStatefulSetSpec);
        if (newContainers.isEmpty()) {
            return;
        }

        String statefulSetName = newStatefulSet.getMetadata().getName();

        validateComputeQuotaFormat(WorkloadType.StatefulSet, newContainers,
                () -> ErrorExtrasUtils.composeErrorExtras(namespaceName, appName, statefulSetName, STATEFUL_SET));

        StatefulSetSpec oldStatefulSetSpec = oldStatefulSet.getSpec();
        int oldReplicas = WorkloadUtils.getReplicas(oldStatefulSetSpec);
        List<Container> oldContainers = WorkloadUtils.getContainers(oldStatefulSetSpec);

        Map<String, Number> newComputeMap = WorkloadUtils.getComputeQuotaMap(newContainers, newReplicas);
        Map<String, Number> oldComputeMap = WorkloadUtils.getComputeQuotaMap(oldContainers, oldReplicas);

        Map<String, Number> computeNeedMap = composeNeededComputeMap(newComputeMap, oldComputeMap);
        Map<String, Number> computeAvailableMap = NamespaceUtils.getNamespaceComputeAvailable(clientManager, namespaceName);

        compare(WorkloadType.StatefulSet, computeNeedMap, computeAvailableMap,
                () -> ErrorExtrasUtils.composeErrorExtras(namespaceName, appName, statefulSetName, STATEFUL_SET));
    }

    public void validateComputeQuotaForUpdate(String namespaceName,
                                              String appName,
                                              StatefulSet oldStatefulSet,
                                              int addReplicas) {
        StatefulSetSpec statefulSetSpec = oldStatefulSet.getSpec();

        List<Container> containers = WorkloadUtils.getContainers(statefulSetSpec);
        if (containers.isEmpty()) {
            return;
        }

        Map<String, Number> computeNeedMap = WorkloadUtils.getComputeQuotaMap(containers, addReplicas);
        Map<String, Number> computeAvailableMap = NamespaceUtils.getNamespaceComputeAvailable(clientManager, namespaceName);

        String statefulSetName = oldStatefulSet.getMetadata().getName();
        compare(WorkloadType.StatefulSet, computeNeedMap, computeAvailableMap,
                () -> ErrorExtrasUtils.composeErrorExtras(namespaceName, appName, statefulSetName, STATEFUL_SET));
    }

    private Map<String, Number> composeNeededComputeMap(Map<String, Number> newComputeMap,
                                                        Map<String, Number> oldComputeMap) {
        Map<String, Number> computeNeedMap = new HashMap<>(4);
        double neededCpuLimits = (Double) newComputeMap.get(K_QUOTA_CPU_LIMITS) - (Double) oldComputeMap.get(K_QUOTA_CPU_LIMITS);
        computeNeedMap.put(K_QUOTA_CPU_LIMITS, neededCpuLimits);

        double neededCpuRequests = (Double) newComputeMap.get(K_QUOTA_CPU_REQUESTS) - (Double) oldComputeMap.get(K_QUOTA_CPU_REQUESTS);
        computeNeedMap.put(K_QUOTA_CPU_REQUESTS, neededCpuRequests);

        long neededMemoryLimits = (Long) newComputeMap.get(K_QUOTA_MEMORY_LIMITS) - (Long) oldComputeMap.get(K_QUOTA_MEMORY_LIMITS);
        computeNeedMap.put(K_QUOTA_MEMORY_LIMITS, neededMemoryLimits);

        long neededMemoryRequests = (Long) newComputeMap.get(K_QUOTA_MEMORY_REQUESTS) - (Long) oldComputeMap.get(K_QUOTA_MEMORY_REQUESTS);
        computeNeedMap.put(K_QUOTA_MEMORY_REQUESTS, neededMemoryRequests);

        return computeNeedMap;
    }

    private void validateComputeQuotaFormat(WorkloadType type,
                                            List<Container> containers,
                                            Supplier<Map<String, Object>> errorExtras) {
        for (Container container : containers) {
            ResourceRequirements requirements = container.getResources();
            if (requirements == null) {
                continue;
            }

            Map<String, Quantity> requests = Optional.ofNullable(requirements.getRequests()).orElseGet(HashMap::new);
            Map<String, Quantity> limits = Optional.ofNullable(requirements.getLimits()).orElseGet(HashMap::new);

            double cpuLimits = UnitUtils.parseK8sCpuQuantity(limits.get(K_CPU));
            double cpuRequests = UnitUtils.parseK8sCpuQuantity(requests.get(K_CPU));
            validateCpuFormat(container, type, cpuLimits, cpuRequests, errorExtras);

            long memoryLimits = UnitUtils.parseK8sMemoryQuantity(limits.get(K_MEMORY));
            long memoryRequests = UnitUtils.parseK8sMemoryQuantity(requests.get(K_MEMORY));
            validateMemoryFormat(container, type, memoryLimits, memoryRequests, errorExtras);
        }
    }

    private void validateCpuFormat(Container container,
                                   WorkloadType type,
                                   double cpuLimits,
                                   double cpuRequests,
                                   Supplier<Map<String, Object>> errorExtras) {
        if (cpuLimits < CONTAINER_MIN_CPU || cpuRequests < CONTAINER_MIN_CPU) {
            Map<String, Object> extrasMap = errorExtras.get();
            extrasMap.put("container", Optional.ofNullable(container.getName()).orElse(""));
            extrasMap.put("requests", cpuRequests);
            extrasMap.put("limits", cpuLimits);

            throw new CcException(BackendReturnCodeNameConstants.CONTAINER_CPU_NOT_VALID, extrasMap);
        }

        if (cpuLimits < cpuRequests) {
            Map<String, Object> extrasMap = errorExtras.get();
            extrasMap.put("container", Optional.ofNullable(container.getName()).orElse(""));
            extrasMap.put("requests", cpuRequests);
            extrasMap.put("limits", cpuLimits);

            if (type == WorkloadType.Deployment) {
                throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_CPU_REQUESTS_MORE_THAN_LIMITS, extrasMap);
            } else {
                throw new CcException(BackendReturnCodeNameConstants.STATEFULSET_CPU_REQUESTS_MORE_THAN_LIMITS, extrasMap);
            }
        }
    }

    private void validateMemoryFormat(Container container,
                                      WorkloadType type,
                                      long memoryLimits,
                                      long memoryRequests,
                                      Supplier<Map<String, Object>> errorExtras) {
        if (memoryLimits < CONTAINER_MIN_MEMORY_BYTES || memoryRequests < CONTAINER_MIN_MEMORY_BYTES) {
            Map<String, Object> extrasMap = errorExtras.get();
            extrasMap.put("container", Optional.ofNullable(container.getName()).orElse(""));
            extrasMap.put("requests", memoryRequests);
            extrasMap.put("limits", memoryLimits);

            throw new CcException(BackendReturnCodeNameConstants.CONTAINER_MEMORY_NOT_VALID, extrasMap);
        }

        if (memoryLimits < memoryRequests) {
            Map<String, Object> extrasMap = errorExtras.get();
            extrasMap.put("container", Optional.ofNullable(container.getName()).orElse(""));
            extrasMap.put("requests", memoryRequests);
            extrasMap.put("limits", memoryLimits);

            if (type == WorkloadType.Deployment) {
                throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_MEMORY_REQUESTS_MORE_THAN_LIMITS, extrasMap);
            } else {
                throw new CcException(BackendReturnCodeNameConstants.STATEFULSET_MEMORY_REQUESTS_MORE_THAN_LIMITS, extrasMap);
            }
        }
    }

    private void compare(WorkloadType type,
                         Map<String, Number> computeNeedMap,
                         Map<String, Number> computeAvailableMap,
                         Supplier<Map<String, Object>> errorExtras) {
        Double cpuLimitsNeed = (Double) computeNeedMap.get(K_QUOTA_CPU_LIMITS);
        Double cpuLimitsAvailable = (Double) computeAvailableMap.get(K_QUOTA_CPU_LIMITS);
        if (cpuLimitsNeed > cpuLimitsAvailable) {
            Map<String, Object> extrasMap = errorExtras.get();
            extrasMap.put("cpuLimitsNeed", cpuLimitsNeed);
            extrasMap.put("cpuLimitsAvailable", cpuLimitsAvailable);
            if (type == WorkloadType.Deployment) {
                throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_CPU_LIMITS_OVER_NAMESPACE_QUOTA, extrasMap);
            } else {
                throw new CcException(BackendReturnCodeNameConstants.STATEFULSET_CPU_LIMITS_OVER_NAMESPACE_QUOTA, extrasMap);
            }
        }

        Double cpuRequestsNeed = (Double) computeNeedMap.get(K_QUOTA_CPU_REQUESTS);
        Double cpuRequestsAvailable = (Double) computeAvailableMap.get(K_QUOTA_CPU_REQUESTS);
        if (cpuRequestsNeed > cpuRequestsAvailable) {
            Map<String, Object> extrasMap = errorExtras.get();
            extrasMap.put("cpuRequestsNeed", cpuRequestsNeed);
            extrasMap.put("cpuRequestsAvailable", cpuRequestsAvailable);
            if (type == WorkloadType.Deployment) {
                throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_CPU_REQUESTS_OVER_NAMESPACE_QUOTA, extrasMap);
            } else {
                throw new CcException(BackendReturnCodeNameConstants.STATEFULSET_CPU_REQUESTS_OVER_NAMESPACE_QUOTA, extrasMap);
            }
        }

        Long memoryLimitsNeed = (Long) computeNeedMap.get(K_QUOTA_MEMORY_LIMITS);
        Long memoryLimitsAvailable = (Long) computeAvailableMap.get(K_QUOTA_MEMORY_LIMITS);
        if (memoryLimitsNeed > memoryLimitsAvailable) {
            Map<String, Object> extrasMap = errorExtras.get();
            extrasMap.put("memoryLimitsNeed", memoryLimitsNeed);
            extrasMap.put("memoryLimitsAvailable", memoryLimitsAvailable);
            if (type == WorkloadType.Deployment) {
                throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_MEMORY_LIMITS_OVER_NAMESPACE_QUOTA, extrasMap);
            } else {
                throw new CcException(BackendReturnCodeNameConstants.STATEFULSET_MEMORY_LIMITS_OVER_NAMESPACE_QUOTA, extrasMap);
            }
        }

        Long memoryRequestsNeed = (Long) computeNeedMap.get(K_QUOTA_MEMORY_REQUESTS);
        Long memoryRequestsAvailable = (Long) computeAvailableMap.get(K_QUOTA_MEMORY_REQUESTS);
        if (memoryRequestsNeed > memoryRequestsAvailable) {
            Map<String, Object> extrasMap = errorExtras.get();
            extrasMap.put("memoryRequestsNeed", memoryRequestsNeed);
            extrasMap.put("memoryRequestsAvailable", memoryRequestsAvailable);
            if (type == WorkloadType.Deployment) {
                throw new CcException(BackendReturnCodeNameConstants.DEPLOYMENT_MEMORY_REQUESTS_OVER_NAMESPACE_QUOTA, extrasMap);
            } else {
                throw new CcException(BackendReturnCodeNameConstants.STATEFULSET_MEMORY_REQUESTS_OVER_NAMESPACE_QUOTA, extrasMap);
            }
        }
    }
}
