package cc.backend.kubernetes.workload.utils;

import cc.backend.kubernetes.storage.StorageRepository;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageWorkload;
import cc.backend.kubernetes.storage.domain.WorkloadType;
import cc.backend.kubernetes.utils.UnitUtils;
import cc.backend.kubernetes.workload.statefulset.StatefulSetHelper;
import com.google.common.collect.ImmutableMap;
import io.fabric8.kubernetes.api.model.*;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.DeploymentSpec;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import io.fabric8.kubernetes.api.model.extensions.StatefulSetSpec;

import java.util.*;
import java.util.stream.Collectors;

import static cc.backend.kubernetes.Constants.*;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2018/1/11.
 */
public class WorkloadUtils {

    public static Map<String, Number> getComputeQuotaMap(List<Container> containers) {
        return getComputeQuotaMap(containers, 1);
    }

    public static Map<String, Number> getComputeQuotaMap(List<Container> containers, int replicas) {
        double cpuLimitsTotal = 0;
        double cpuRequestsTotal = 0;
        long memoryLimitsBytesTotal = 0;
        long memoryRequestsBytesTotal = 0;

        List<ResourceRequirements> requirementsList = containers.stream()
                .map(Container::getResources)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        for (ResourceRequirements requirements : requirementsList) {
            Map<String, Quantity> requests = Optional.ofNullable(requirements.getRequests()).orElseGet(HashMap::new);
            Map<String, Quantity> limits = Optional.ofNullable(requirements.getLimits()).orElseGet(HashMap::new);
            cpuLimitsTotal += UnitUtils.parseK8sCpuQuantity(limits.get(K_CPU));
            cpuRequestsTotal += UnitUtils.parseK8sCpuQuantity(requests.get(K_CPU));

            memoryLimitsBytesTotal += UnitUtils.parseK8sMemoryQuantity(limits.get(K_MEMORY));
            memoryRequestsBytesTotal += UnitUtils.parseK8sMemoryQuantity(requests.get(K_MEMORY));
        }

        cpuLimitsTotal *= replicas;
        cpuRequestsTotal *= replicas;
        memoryLimitsBytesTotal *= replicas;
        memoryRequestsBytesTotal *= replicas;

        return ImmutableMap.<String, Number>builder()
                .put(K_QUOTA_CPU_LIMITS, cpuLimitsTotal)
                .put(K_QUOTA_CPU_REQUESTS, cpuRequestsTotal)
                .put(K_QUOTA_MEMORY_LIMITS, memoryLimitsBytesTotal)
                .put(K_QUOTA_MEMORY_REQUESTS, memoryRequestsBytesTotal)
                .build();
    }

    public static int getReplicas(Deployment deployment) {
        return getReplicas(deployment.getSpec());
    }

    public static int getReplicas(DeploymentSpec spec) {
        return Optional.ofNullable(spec.getReplicas()).orElse(0);
    }

    public static int getReplicas(StatefulSet statefulSet) {
        return getReplicas(statefulSet.getSpec());
    }

    public static int getReplicas(StatefulSetSpec spec) {
        return Optional.ofNullable(spec.getReplicas()).orElse(0);
    }

    public static List<Container> getContainers(Deployment deployment) {
        return getContainers(deployment.getSpec());
    }

    public static List<Container> getContainers(DeploymentSpec spec) {
        return Optional.ofNullable(spec
                .getTemplate()
                .getSpec()
                .getContainers())
                .orElseGet(ArrayList::new);
    }

    public static List<Container> getContainers(StatefulSet statefulSet) {
        return getContainers(statefulSet.getSpec());
    }

    public static List<Container> getContainers(StatefulSetSpec spec) {
        return Optional.ofNullable(spec
                .getTemplate()
                .getSpec()
                .getContainers())
                .orElseGet(ArrayList::new);
    }

    public static List<Storage> getExistsStorageList(String namespaceName,
                                                     StatefulSet statefulSet,
                                                     StorageRepository repository) {
        List<String> pvcNameList = getPvcNameList(statefulSet);
        if (pvcNameList.isEmpty()) {
            return new ArrayList<>();
        }
        return repository.findByNamespaceNameAndPvcNameIn(namespaceName, pvcNameList);
    }

    public static List<Storage> getExistsStorageList(String namespaceName,
                                                     Deployment deployment,
                                                     StorageRepository repository) {
        List<String> pvcNameList = getPvcNameList(deployment);
        if (pvcNameList.isEmpty()) {
            return new ArrayList<>();
        }
        return repository.findByNamespaceNameAndPvcNameIn(namespaceName, pvcNameList);
    }

    /**
     * get the stateful set related pvc name list (include volumes and volumeClaimTemplate)
     *
     * @param statefulSet the stateful set
     * @return the pvc name list
     */
    public static List<String> getPvcNameList(StatefulSet statefulSet) {
        String statefulSetName = statefulSet.getMetadata().getName();
        StatefulSetSpec spec = statefulSet.getSpec();
        List<Volume> volumes = spec.getTemplate().getSpec().getVolumes();
        List<String> volumesPvcNameList = Optional.ofNullable(volumes)
                .orElseGet(ArrayList::new)
                .stream()
                .map(Volume::getPersistentVolumeClaim)
                .filter(Objects::nonNull)
                .map(PersistentVolumeClaimVolumeSource::getClaimName)
                .collect(Collectors.toList());

        List<String> allPvcNameList = new ArrayList<>(volumesPvcNameList);

        int replicas = Optional.ofNullable(spec.getReplicas()).orElse(0);
        if (replicas > 0) {
            List<String> templatePvcNameList = Optional.ofNullable(spec.getVolumeClaimTemplates())
                    .orElseGet(ArrayList::new)
                    .stream()
                    .map(template -> template.getMetadata().getName())
                    .map(templateName -> StatefulSetHelper.getClaimTemplatePvcNameList(statefulSetName, templateName, replicas))
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());
            allPvcNameList.addAll(templatePvcNameList);
        }

        return allPvcNameList;
    }

    public static List<String> getPvcNameList(Deployment deployment) {
        List<Volume> volumes = deployment.getSpec().getTemplate().getSpec().getVolumes();
        return Optional.ofNullable(volumes)
                .orElseGet(ArrayList::new)
                .stream()
                .map(Volume::getPersistentVolumeClaim)
                .filter(Objects::nonNull)
                .map(PersistentVolumeClaimVolumeSource::getClaimName)
                .collect(Collectors.toList());
    }

    public static List<StorageWorkload> composeStorageWorkloadList(String appName,
                                                                   String workloadName,
                                                                   WorkloadType workloadType,
                                                                   List<Storage> storageList) {
        return storageList
                .stream()
                .map(storage -> {
                    StorageWorkload storageWorkload = new StorageWorkload();
                    storageWorkload.setStorageId(storage.getId());
                    storageWorkload.setStorageName(storage.getStorageName());
                    storageWorkload.setNamespaceName(storage.getNamespaceName());
                    storageWorkload.setAppName(appName);
                    storageWorkload.setWorkloadType(workloadType);
                    storageWorkload.setWorkloadName(workloadName);
                    return storageWorkload;
                })
                .collect(Collectors.toList());
    }
}
