package cc.backend.kubernetes.workload.pod;

import cc.backend.kubernetes.apps.validator.AppValidator;
import cc.backend.kubernetes.workload.pod.dto.PodDto;
import cc.backend.kubernetes.workload.pod.services.PodsManagement;
import io.fabric8.kubernetes.api.model.Pod;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author wangchunyang@gmail.com
 */
@Named
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes/namespaces/{namespaceName}/apps/{appName}/pods")
@Api(value = "Pod", description = "Info about pods.", produces = "application/json")
public class PodsEndpoint {

    @Inject
    private PodsManagement management;

    @Inject
    private AppValidator appValidator;

    @GET
    @ApiOperation(value = "Get the app's pod list info.", responseContainer = "List", response = PodDto.class)
    public Response getPodsByApp(@PathParam("namespaceName") String namespaceName,
                                 @PathParam("appName") String appName) {
        appValidator.validateAppShouldExists(namespaceName, appName);
        List<Pod> podList = management.getPodsByApp(namespaceName, appName);
        return Response.ok(PodDto.from(podList)).build();
    }

    @GET
    @Path("by_deployment/{deploymentName}")
    @ApiOperation(value = "Get the deployment's pod list info.", responseContainer = "List", response = PodDto.class)
    public Response getPodsByDeployment(@PathParam("namespaceName") String namespaceName,
                                        @PathParam("appName") String appName,
                                        @PathParam("deploymentName") String deploymentName) {
        appValidator.validateAppShouldExists(namespaceName, appName);
        List<Pod> pods = management.getPodsByDeployment(namespaceName, appName, deploymentName);
        return Response.ok(PodDto.from(pods)).build();
    }

    @GET
    @Path("by_service/{serviceName}")
    @ApiOperation(value = "Get the service's pod list info.", responseContainer = "List", response = PodDto.class)
    public Response getPodsByService(@PathParam("namespaceName") String namespace,
                                     @PathParam("appName") String appName,
                                     @PathParam("serviceName") String serviceName) {
        appValidator.validateAppShouldExists(namespace, appName);
        List<Pod> pods = management.getPodsByService(namespace, appName, serviceName);
        return Response.ok(PodDto.from(pods)).build();
    }

    @GET
    @Path("by_statefulset/{statefulSetName}")
    @ApiOperation(value = "Get the statefulset's pod list info.", responseContainer = "List", response = PodDto.class)
    public Response getPodsByStatefulSet(@PathParam("namespaceName") String namespace,
                                         @PathParam("appName") String appName,
                                         @PathParam("statefulSetName") String statefulSetName) {
        appValidator.validateAppShouldExists(namespace, appName);
        List<Pod> pods = management.getPodsByStatefulSet(namespace, appName, statefulSetName);
        return Response.ok(PodDto.from(pods)).build();
    }

    @GET
    @Path("{podName}/cmd/{cmdName}")
    @ApiOperation(value = "Exec pod command and return result.", response = String.class)
    public Response getCmdResponseByPodName(@PathParam("namespaceName") String namespace,
                                            @PathParam("appName") String appName,
                                            @PathParam("podName") String podName,
                                            @PathParam("cmdName") String cmdName) {
        return Response.ok(management.execPodCommand(podName, cmdName).toString()).build();
    }
}
