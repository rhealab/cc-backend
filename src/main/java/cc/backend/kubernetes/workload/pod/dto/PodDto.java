package cc.backend.kubernetes.workload.pod.dto;

import cc.backend.common.utils.DateUtils;
import cc.backend.kubernetes.workload.pod.PodHelper;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.fabric8.kubernetes.api.model.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/2/22.
 * @author yanzhixiang modified on 2017-6-7
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PodDto {
    private String name;
    private String namespace;
    private Map<String, String> labels;
    private String nodeName;
    private List<ContainerDto> containers;
    private List<VolumeDto> volumes;
    private Date startTime;
    private Date createdOn;
    private long age;
    private String podIP;
    private int restartCount;
    private String status;
    private String uid;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    public void setLabels(Map<String, String> labels) {
        this.labels = labels;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public List<ContainerDto> getContainers() {
        return containers;
    }

    public void setContainers(List<ContainerDto> containers) {
        this.containers = containers;
    }

    public List<VolumeDto> getVolumes() {
        return volumes;
    }

    public void setVolumes(List<VolumeDto> volumes) {
        this.volumes = volumes;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public String getPodIP() {
        return podIP;
    }

    public void setPodIP(String podIP) {
        this.podIP = podIP;
    }

    public int getRestartCount() {
        return restartCount;
    }

    public void setRestartCount(int restartCount) {
        this.restartCount = restartCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @Override
    public String toString() {
        return "PodDto{" +
                "name='" + name + '\'' +
                ", namespace='" + namespace + '\'' +
                ", labels=" + labels +
                ", nodeName='" + nodeName + '\'' +
                ", containers=" + containers +
                ", volumes=" + volumes +
                ", startTime=" + startTime +
                ", createdOn=" + createdOn +
                ", age=" + age +
                ", podIP='" + podIP + '\'' +
                ", restartCount=" + restartCount +
                ", status='" + status + '\'' +
                ", uid='" + uid + '\'' +
                '}';
    }

    public static List<PodDto> from(List<Pod> pods) {
        return Optional.ofNullable(pods)
                .orElseGet(ArrayList::new)
                .stream()
                .map(PodDto::from)
                .collect(Collectors.toList());
    }

    public static PodDto from(Pod pod) {
        ObjectMeta metadata = pod.getMetadata();
        PodSpec spec = pod.getSpec();
        PodStatus status = pod.getStatus();

        PodDto dto = new PodDto();

        dto.setName(metadata.getName());
        dto.setNamespace(metadata.getNamespace());
        dto.setCreatedOn(DateUtils.parseK8sDate(metadata.getCreationTimestamp()));
        dto.setAge(DateUtils.getAge(dto.getCreatedOn()));
        dto.setLabels(metadata.getLabels());

        List<ContainerStatus> containerStatuses = Optional.ofNullable(status.getContainerStatuses())
                .orElseGet(ArrayList::new);

        int totalRestartCount = containerStatuses
                .stream()
                .mapToInt(ContainerStatus::getRestartCount)
                .sum();
        dto.setRestartCount(totalRestartCount);

        dto.setPodIP(status.getPodIP());
        dto.setStartTime(DateUtils.parseK8sDate(status.getStartTime()));
        dto.setNodeName(spec.getNodeName());
        dto.setContainers(ContainerDto.from(spec.getContainers(), containerStatuses));
        dto.setVolumes(VolumeDto.from(spec.getVolumes()));
        dto.setStatus(PodHelper.getDisplayStatus(pod));

        return dto;
    }

    public static PodDto from(PodTemplateSpec template) {
        if (template == null) {
            return null;
        }

        ObjectMeta metadata = template.getMetadata();
        PodSpec spec = template.getSpec();

        PodDto dto = new PodDto();
        dto.setName(metadata.getName());
        dto.setNamespace(metadata.getNamespace());
        dto.setUid(metadata.getUid());
        dto.setLabels(metadata.getLabels());
        dto.setContainers(ContainerDto.from(spec.getContainers()));
        dto.setVolumes(VolumeDto.from(spec.getVolumes()));
        return dto;
    }
}
