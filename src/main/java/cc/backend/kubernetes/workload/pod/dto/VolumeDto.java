package cc.backend.kubernetes.workload.pod.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.fabric8.kubernetes.api.model.Volume;

import java.util.ArrayList;
import java.util.List;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/2/22.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VolumeDto {
    private String name;
    private PersistentVolumeClaimDto persistentVolumeClaim;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PersistentVolumeClaimDto getPersistentVolumeClaim() {
        return persistentVolumeClaim;
    }

    public void setPersistentVolumeClaim(PersistentVolumeClaimDto persistentVolumeClaim) {
        this.persistentVolumeClaim = persistentVolumeClaim;
    }

    @Override
    public String toString() {
        return "VolumeDto{" +
                "name='" + name + '\'' +
                ", persistentVolumeClaim=" + persistentVolumeClaim +
                '}';
    }

    public static List<VolumeDto> from(List<Volume> volumes) {
        List<VolumeDto> dtos = new ArrayList<>();

        if (volumes != null) {
            for (Volume volume : volumes) {
                if (volume.getPersistentVolumeClaim() != null) {
                    VolumeDto dto = new VolumeDto();
                    dto.setName(volume.getName());
                    PersistentVolumeClaimDto persistentVolumeClaimDto = new PersistentVolumeClaimDto();
                    persistentVolumeClaimDto.setClaimName(volume.getPersistentVolumeClaim().getClaimName());
                    dto.setPersistentVolumeClaim(persistentVolumeClaimDto);
                    dtos.add(dto);
                }
            }
        }

        return dtos;
    }
}
