package cc.backend.kubernetes.workload.pod.dto;

import java.util.List;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/2/22.
 */
public class RbdDto {
    private String pool;
    private String image;
    private List<String> monitors;
    private String fsType;
    private String user;
    private String secretName;
    private String keyring;
    private boolean readOnly;

    public String getPool() {
        return pool;
    }

    public void setPool(String pool) {
        this.pool = pool;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<String> getMonitors() {
        return monitors;
    }

    public void setMonitors(List<String> monitors) {
        this.monitors = monitors;
    }

    public String getFsType() {
        return fsType;
    }

    public void setFsType(String fsType) {
        this.fsType = fsType;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getSecretName() {
        return secretName;
    }

    public void setSecretName(String secretName) {
        this.secretName = secretName;
    }

    public String getKeyring() {
        return keyring;
    }

    public void setKeyring(String keyring) {
        this.keyring = keyring;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    @Override
    public String toString() {
        return "RbdDto{" +
                "pool='" + pool + '\'' +
                ", image='" + image + '\'' +
                ", monitors=" + monitors +
                ", fsType='" + fsType + '\'' +
                ", user='" + user + '\'' +
                ", secretName='" + secretName + '\'' +
                ", keyring='" + keyring + '\'' +
                ", readOnly=" + readOnly +
                '}';
    }
}
