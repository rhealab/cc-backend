package cc.backend.kubernetes.workload.pod.dto;

import cc.backend.common.utils.DateUtils;
import cc.backend.kubernetes.workload.pod.ContainerUtils;
import io.fabric8.kubernetes.api.model.Container;
import io.fabric8.kubernetes.api.model.ContainerStatus;
import io.fabric8.kubernetes.api.model.EnvVar;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/2/22.
 * @author yanzhixiang modified on 2017-6-7
 */
public class ContainerDto {
    private String name;
    private String image;
    private String status;
    private Date startTime;
    private List<EnvVar> env;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public List<EnvVar> getEnv() {
        return env;
    }

    public void setEnv(List<EnvVar> env) {
        this.env = env;
    }

    @Override
    public String toString() {
        return "ContainerDto{" +
                "name='" + name + '\'' +
                ", image='" + image + '\'' +
                ", status='" + status + '\'' +
                ", startTime=" + startTime +
                ", env=" + env +
                '}';
    }

    public static List<ContainerDto> from(List<Container> containers, List<ContainerStatus> statusList) {
        Map<String, ContainerStatus> statusMap = Optional.ofNullable(statusList)
                .orElseGet(ArrayList::new)
                .stream()
                .collect(Collectors.toMap(ContainerStatus::getName, status -> status));

        return Optional.ofNullable(containers)
                .orElseGet(ArrayList::new)
                .stream()
                .map(container -> from(container, statusMap))
                .collect(Collectors.toList());
    }

    public static ContainerDto from(Container container, Map<String, ContainerStatus> statusMap) {
        ContainerDto dto = new ContainerDto();
        dto.setName(container.getName());
        dto.setImage(container.getImage());

        ContainerStatus containerStatus = statusMap.get(container.getName());
        String status = ContainerUtils.composeContainerStatus(containerStatus);
        if ("running".equals(status)) {
            dto.setStartTime(DateUtils.parseK8sDate(containerStatus.getState().getRunning().getStartedAt()));
        }

        dto.setEnv(container.getEnv());
        return dto;
    }

    public static List<ContainerDto> from(List<Container> containers) {
        return Optional.ofNullable(containers)
                .orElseGet(ArrayList::new)
                .stream()
                .map(container -> {
                    ContainerDto dto = new ContainerDto();
                    dto.setName(container.getName());
                    dto.setImage(container.getImage());
                    return dto;
                })
                .collect(Collectors.toList());
    }
}
