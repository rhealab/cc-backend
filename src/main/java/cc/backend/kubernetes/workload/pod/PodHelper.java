package cc.backend.kubernetes.workload.pod;

import io.fabric8.kubernetes.api.model.ContainerStateTerminated;
import io.fabric8.kubernetes.api.model.ContainerStateWaiting;
import io.fabric8.kubernetes.api.model.ContainerStatus;
import io.fabric8.kubernetes.api.model.Pod;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * some utils for pod
 *
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/11/8.
 */
public class PodHelper {
    public static String NODE_UNREACHABLE_POD_REASON = "NodeLost";

    /**
     * get the pod's status for display. pod should not be null
     * rule is from kubectl
     * https://github.com/kubernetes/kubernetes/blob/master/pkg/printers/internalversion/printers.go#L523
     *
     * @param pod the pod
     * @return the status for display
     */
    public static String getDisplayStatus(Pod pod) {
        String status = pod.getStatus().getPhase();

        if (!StringUtils.isEmpty(pod.getStatus().getReason())) {
            status = pod.getStatus().getReason();
        }

        boolean initializing = false;

        int initContainerSize = pod.getSpec().getInitContainers().size();
        List<ContainerStatus> initContainerStatuses = pod.getStatus().getInitContainerStatuses();
        if (initContainerStatuses != null) {
            for (int i = 0; i < initContainerStatuses.size(); i++) {
                ContainerStatus containerStatus = initContainerStatuses.get(i);
                ContainerStateTerminated terminated = containerStatus.getState().getTerminated();
                if (terminated != null) {
                    if (terminated.getExitCode() != null && terminated.getExitCode() == 0) {
                        continue;
                    } else {
                        if (StringUtils.isEmpty(terminated.getReason())) {
                            if (terminated.getSignal() != null && terminated.getSignal() != 0) {
                                status = "Init:Signal:" + terminated.getSignal();
                            } else {
                                status = "Init:ExitCode:" + terminated.getExitCode();
                            }
                        } else {
                            status = "Init:" + terminated.getReason();
                        }
                        initializing = true;
                        continue;
                    }
                }

                ContainerStateWaiting waiting = containerStatus.getState().getWaiting();
                if (waiting != null
                        && !StringUtils.isEmpty(waiting.getReason())
                        && !"PodInitializing".equals(waiting.getReason())) {
                    status = "Init:" + waiting.getReason();
                    initializing = true;
                    continue;
                }

                status = "Init:" + i + "/" + initContainerSize;
                initializing = true;
                break;
            }
        }

        if (!initializing) {
            List<ContainerStatus> containerStatuses = pod.getStatus().getContainerStatuses();
            if (containerStatuses != null) {
                for (int i = containerStatuses.size() - 1; i >= 0; i--) {
                    ContainerStatus containerStatus = containerStatuses.get(i);
                    ContainerStateWaiting waiting = containerStatus.getState().getWaiting();
                    ContainerStateTerminated terminated = containerStatus.getState().getTerminated();

                    if (waiting != null && !StringUtils.isEmpty(waiting.getReason())) {
                        status = waiting.getReason();
                    } else if (terminated != null && !StringUtils.isEmpty(terminated.getReason())) {
                        status = terminated.getReason();
                    } else if (terminated != null && StringUtils.isEmpty(terminated.getReason())) {
                        if (terminated.getSignal() != null && terminated.getSignal() != 0) {
                            status = "Signal:" + terminated.getSignal();
                        } else {
                            status = "ExitCode:" + terminated.getExitCode();
                        }
                    }
                }
            }
        }

        if (pod.getMetadata().getDeletionTimestamp() != null) {
            if (NODE_UNREACHABLE_POD_REASON.equals(pod.getStatus().getReason())) {
                status = "Unknown";
            } else {
                status = "Terminating";
            }
        }

        return status;
    }
}
