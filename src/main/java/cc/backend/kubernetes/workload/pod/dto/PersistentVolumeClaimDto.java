package cc.backend.kubernetes.workload.pod.dto;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/2/22.
 */
public class PersistentVolumeClaimDto {
    private String claimName;

    public String getClaimName() {
        return claimName;
    }

    public void setClaimName(String claimName) {
        this.claimName = claimName;
    }

    @Override
    public String toString() {
        return "PersistentVolumeClaimDto{" +
                "claimName='" + claimName + '\'' +
                '}';
    }
}
