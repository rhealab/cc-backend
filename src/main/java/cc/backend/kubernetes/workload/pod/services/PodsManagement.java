package cc.backend.kubernetes.workload.pod.services;

import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.service.ServiceValidator;
import cc.backend.kubernetes.workload.deployment.validator.DeploymentsValidator;
import cc.backend.kubernetes.workload.statefulset.StatefulSetValidator;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.PodList;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.*;
import java.util.stream.Collectors;

import static cc.backend.kubernetes.Constants.APP_LABEL;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/2/23.
 */
@Component
public class PodsManagement {
    @Inject
    private KubernetesClientManager clientManager;

    @Inject
    private DeploymentsValidator deploymentsValidator;

    @Inject
    private ServiceValidator serviceValidator;

    @Inject
    private StatefulSetValidator statefulSetValidator;

    public List<Pod> getPodsByApp(String namespaceName, String appName) {
        KubernetesClient client = clientManager.getClient();
        List<Deployment> deploymentList = client
                .extensions()
                .deployments()
                .inNamespace(namespaceName)
                .withLabel(APP_LABEL, appName)
                .list()
                .getItems();

        List<Pod> deploymentPods = Optional.ofNullable(deploymentList)
                .orElseGet(ArrayList::new)
                .stream()
                .map(deployment -> deployment.getSpec().getTemplate().getMetadata().getLabels())
                .map(labels -> client.pods()
                        .inNamespace(namespaceName)
                        .withLabels(labels)
                        .list()
                        .getItems())
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        List<StatefulSet> statefulSetList = client
                .apps()
                .statefulSets()
                .inNamespace(namespaceName)
                .withLabel(APP_LABEL, appName)
                .list()
                .getItems();

        List<Pod> statefulSetPods = Optional.ofNullable(statefulSetList)
                .orElseGet(ArrayList::new)
                .stream()
                .map(statefulSet -> statefulSet.getSpec().getTemplate().getMetadata().getLabels())
                .map(labels -> client.pods()
                        .inNamespace(namespaceName)
                        .withLabels(labels)
                        .list()
                        .getItems())
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        List<Pod> allPods = new ArrayList<>();
        allPods.addAll(deploymentPods);
        allPods.addAll(statefulSetPods);

        return allPods;
    }

    public Map<String, List<Pod>> getDeploymentPodsMap(String namespaceName, List<Deployment> deploymentList) {
        List<Pod> pods = Optional
                .ofNullable(clientManager.getClient().pods().inNamespace(namespaceName).list().getItems())
                .orElseGet(ArrayList::new);

        return Optional.ofNullable(deploymentList)
                .orElseGet(ArrayList::new)
                .stream()
                .collect(Collectors.toMap(deployment -> deployment.getMetadata().getName(),
                        deployment -> getPods(deployment, pods)));
    }

    private List<Pod> getPods(Deployment deployment, List<Pod> allPods) {
        Map<String, String> podLabels = deployment.getSpec().getTemplate().getMetadata().getLabels();
        return allPods.stream()
                .filter(pod -> pod.getMetadata().getLabels() != null)
                .filter(pod -> pod.getMetadata().getLabels().entrySet().containsAll(podLabels.entrySet()))
                .collect(Collectors.toList());
    }

    public Map<String, List<Pod>> getDeploymentPodsMap(List<Deployment> deploymentList) {
        return Optional.ofNullable(deploymentList)
                .orElseGet(ArrayList::new)
                .stream()
                .collect(Collectors.toMap(deployment -> deployment.getMetadata().getName(),
                        this::getPodsByDeployment));
    }

    public List<Pod> getPodsByDeployment(Deployment deployment) {
        Map<String, String> podLabels = deployment.getSpec().getTemplate().getMetadata().getLabels();
        List<Pod> pods = clientManager.getClient()
                .pods()
                .inNamespace(deployment.getMetadata().getNamespace())
                .withLabels(podLabels)
                .list()
                .getItems();

        return Optional.ofNullable(pods).orElseGet(ArrayList::new);
    }

    public Map<String, List<Pod>> getAppStatefulSetPodsMap(String namespaceName, String appName) {
        Map<String, List<Pod>> statefulSetPodsMap = new HashMap<>();
        KubernetesClient client = clientManager.getClient();
        List<StatefulSet> statefulSetList = client
                .apps()
                .statefulSets()
                .inNamespace(namespaceName)
                .withLabel(APP_LABEL, appName)
                .list()
                .getItems();

        if (statefulSetList != null) {
            for (StatefulSet statefulSet : statefulSetList) {
                Map<String, String> podLabels = statefulSet.getSpec().getTemplate().getMetadata().getLabels();
                PodList podList = client.pods().inNamespace(namespaceName).withLabels(podLabels).list();
                statefulSetPodsMap.put(statefulSet.getMetadata().getName(), podList.getItems());
            }
        }
        return statefulSetPodsMap;
    }

    public List<Pod> getPodsByDeployment(String namespaceName, String appName, String deploymentName) {
        Deployment deployment = deploymentsValidator.validateDeploymentShouldExists(namespaceName, appName, deploymentName);

        Map<String, String> podLabels = deployment.getSpec().getTemplate().getMetadata().getLabels();

        List<Pod> pods = clientManager.getClient().pods().inNamespace(namespaceName).withLabels(podLabels).list().getItems();
        return pods == null ? new ArrayList<>() : pods;
    }

    public List<Pod> getPodsByService(String namespaceName, String appName, String serviceName) {
        Service service = serviceValidator.validateServiceShouldExists(namespaceName, appName, serviceName);
        Map<String, String> podLabels = service.getSpec().getSelector();

        List<Pod> pods = clientManager.getClient()
                .pods()
                .inNamespace(namespaceName)
                .withLabels(podLabels)
                .list()
                .getItems();
        return Optional.ofNullable(pods).orElseGet(ArrayList::new);
    }

    public Map<String, List<Pod>> getStatefulSetPodsMap(String namespaceName, List<StatefulSet> statefulSetList) {
        List<Pod> pods = Optional
                .ofNullable(clientManager.getClient().pods().inNamespace(namespaceName).list().getItems())
                .orElseGet(ArrayList::new);

        return Optional.ofNullable(statefulSetList)
                .orElseGet(ArrayList::new)
                .stream()
                .collect(Collectors.toMap(statefulSet -> statefulSet.getMetadata().getName(),
                        statefulSet -> getPods(statefulSet, pods)));
    }

    private List<Pod> getPods(StatefulSet statefulSet, List<Pod> allPods) {
        Map<String, String> podLabels = statefulSet.getSpec().getTemplate().getMetadata().getLabels();
        return allPods.stream()
                .filter(pod -> pod.getMetadata().getLabels() != null)
                .filter(pod -> pod.getMetadata().getLabels().entrySet().containsAll(podLabels.entrySet()))
                .collect(Collectors.toList());
    }

    public List<Pod> getPodsByStatefulSet(String namespaceName, String appName, String statefulSetName) {
        StatefulSet statefulSet = statefulSetValidator.validateStatefulSetShouldExists(namespaceName, appName, statefulSetName);
        Map<String, String> podLabels = statefulSet.getSpec().getSelector().getMatchLabels();

        List<Pod> pods = clientManager.getClient()
                .pods()
                .inNamespace(namespaceName)
                .withLabels(podLabels)
                .list()
                .getItems();
        return Optional.ofNullable(pods).orElseGet(ArrayList::new);
    }

    public OutputStream execPodCommand(String podName, String cmdName) {
        ByteArrayOutputStream out = new ByteArrayOutputStream(1024);
        clientManager.getClient()
                .pods()
                .withName(podName)
                .writingOutput(out)
                .exec(cmdName);
        try {
            Thread.sleep(100);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return out;
    }
}
