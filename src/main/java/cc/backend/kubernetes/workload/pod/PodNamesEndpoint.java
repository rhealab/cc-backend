package cc.backend.kubernetes.workload.pod;

import cc.backend.kubernetes.workload.pod.services.PodsNameManagement;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author yanzhixiang on 17-6-22.
 */
@Component
@Produces(MediaType.APPLICATION_JSON)
@Path("kubernetes/namespaces/{namespaceName}/apps/{appName}/deployments")
@Api(value = "Pod", description = "Info about pods.", produces = "application/json")
public class PodNamesEndpoint {
    @Inject
    private PodsNameManagement management;

    @GET
    @Path("{deploymentName}/pods_name")
    @ApiOperation(value = "List all pods name in deployment.", responseContainer = "List", response = String.class)
    public Response getByDeployment(@PathParam("namespaceName") String namespaceName,
                                    @PathParam("appName") String appName,
                                    @PathParam("deploymentName") String deploymentName) {
        List<String> podsName = management.getPodsNameByDeployment(namespaceName, appName, deploymentName);
        return Response.ok(podsName).build();
    }
}
