package cc.backend.kubernetes.workload.pod.services;

import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2018/1/12.
 */
@Component
public class PodsNameManagement {
    @Inject
    private PodsManagement podsManagement;

    public List<String> getPodsNameByDeployment(String namespaceName, String appName, String deploymentName) {
        return podsManagement
                .getPodsByDeployment(namespaceName, appName, deploymentName)
                .stream()
                .map(pod -> pod.getMetadata().getName())
                .collect(Collectors.toList());
    }
}
