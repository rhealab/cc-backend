package cc.backend.kubernetes.workload.pod;

import io.fabric8.kubernetes.api.model.ContainerStateRunning;
import io.fabric8.kubernetes.api.model.ContainerStatus;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2018/1/12.
 */
public class ContainerUtils {
    public static String composeContainerStatus(ContainerStatus status) {
        String statusStr = null;
        if (status != null && status.getState() != null) {
            ContainerStateRunning running = status.getState().getRunning();
            if (running != null) {
                statusStr = "running";
            }
            if (status.getState().getWaiting() != null) {
                statusStr = "waiting";
            }

            if (status.getState().getTerminated() != null) {
                statusStr = "terminated";
            }
        }

        return statusStr;
    }
}
