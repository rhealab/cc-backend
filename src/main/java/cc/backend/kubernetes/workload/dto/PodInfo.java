package cc.backend.kubernetes.workload.dto;

/**
 * @author xzy on 17-8-4.
 */
public class PodInfo {
    private int desired;
    private int current;
    private int pending;
    private int running;
    private int failed;
    private int succeeded;

    public int getDesired() {
        return desired;
    }

    public void setDesired(int desired) {
        this.desired = desired;
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public int getPending() {
        return pending;
    }

    public void setPending(int pending) {
        this.pending = pending;
    }

    public int getRunning() {
        return running;
    }

    public void setRunning(int running) {
        this.running = running;
    }

    public int getFailed() {
        return failed;
    }

    public void setFailed(int failed) {
        this.failed = failed;
    }

    public int getSucceeded() {
        return succeeded;
    }

    public void setSucceeded(int succeeded) {
        this.succeeded = succeeded;
    }
}
