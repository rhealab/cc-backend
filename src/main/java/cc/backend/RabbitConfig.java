package cc.backend;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

import static com.google.common.collect.ImmutableMap.of;

/**
 * @author wangchunyang@gmail.com
 */
@Configuration
public class RabbitConfig {
    public static final String NAMESPACE_CREATED_Q = "namespace_created_q";
    public static final String NAMESPACE_RESIZE_Q = "namespace_resize_q";
    public static final String NAMESPACE_DELETED_Q = "namespace_deleted_q";

    public static final String NAMESPACE_INIT_NFS_Q = "namespace_init_nfs_q";
    public static final String NAMESPACE_RELEASE_NFS_Q = "namespace_release_nfs_q";
    public static final String NFS_DIR_CREATED_Q = "nfs_dir_created_q";
    public static final String NFS_DIR_DELETED_Q = "nfs_dir_deleted_q";
    public static final String NFS_USAGE_Q = "nfs_usage_q";
    public static final String NFS_SUB_DIRECTORY_CREATE_Q = "nfs_sub_directory_create_q";
    public static final String NFS_SUB_DIRECTORY_CREATED_Q = "nfs_sub_directory_created_q";
    public static final String NFS_SUB_DIRECTORY_DELETE_Q = "nfs_sub_directory_delete_q";
    public static final String NFS_SUB_DIRECTORY_DELETED_Q = "nfs_sub_directory_deleted_q";

    public static final String NAMESPACE_INIT_CEPH_Q = "namespace_init_ceph_q";
    public static final String NAMESPACE_RESIZE_CEPH_Q = "namespace_resize_ceph_q";
    public static final String NAMESPACE_RELEASE_CEPH_Q = "namespace_release_ceph_q";

    public static final String CEPH_POOL_CREATED_Q = "ceph_pool_created_q";
    public static final String CEPH_FS_DIR_CREATED_Q = "ceph_fs_dir_created_q";
    public static final String CEPH_USER_CREATED_Q = "ceph_user_created_q";
    public static final String CEPH_POOL_DELETED_Q = "ceph_pool_deleted_q";
    public static final String CEPH_FS_DIR_DELETED_Q = "ceph_fs_dir_deleted_q";
    public static final String CEPH_USER_DELETED_Q = "ceph_user_deleted_q";
    public static final String CEPH_POOL_RESIZED_Q = "ceph_pool_resized_q";

    public static final String CEPH_IMAGE_CREATE_Q = "ceph_image_create_q";
    public static final String CEPH_IMAGE_CREATED_Q = "ceph_image_created_q";
    public static final String CEPH_IMAGE_RESIZE_Q = "ceph_image_resize_q";
    public static final String CEPH_IMAGE_RESIZED_Q = "ceph_image_resized_q";
    public static final String CEPH_IMAGE_DELETE_Q = "ceph_image_delete_q";
    public static final String CEPH_IMAGE_DELETED_Q = "ceph_image_deleted_q";

    public static final String CEPH_IMAGE_USAGE_Q = "ceph_image_usage_q";
    public static final String CEPH_POOL_USAGE_Q = "ceph_pool_usage_q";

    public static final String CEPHFS_SUB_DIRECTORY_CREATE_Q = "cephfs_sub_directory_create_q";
    public static final String CEPHFS_SUB_DIRECTORY_CREATED_Q = "cephfs_sub_directory_created_q";
    public static final String CEPHFS_SUB_DIRECTORY_DELETE_Q = "cephfs_sub_directory_delete_q";
    public static final String CEPHFS_SUB_DIRECTORY_DELETED_Q = "cephfs_sub_directory_deleted_q";

    public static final String OPERATION_AUDIT_Q = "operation_audit_q";
    public static final String REQUEST_AUDIT_Q = "request_audit_q";

    public static final String EVENT_ERROR_Q = "event_error_q";

    public static final String APP_STATUS_Q = "app_status_q";

    @Bean
    public Queue getNamespaceCreatedQueue() {
        return new Queue(NAMESPACE_CREATED_Q, true);
    }

    @Bean
    public Queue getNamespaceInitNfsQueue() {
        return new Queue(NAMESPACE_INIT_NFS_Q, true);
    }

    @Bean
    public Queue getNamespaceReleaseNfsQueue() {
        return new Queue(NAMESPACE_RELEASE_NFS_Q, true);
    }

    @Bean
    public Queue getNamespaceDeletedQueue() {
        return new Queue(NAMESPACE_DELETED_Q, true);
    }

    @Bean
    public Queue getNamespaceResizeQueue() {
        return new Queue(NAMESPACE_RESIZE_Q, true);
    }

    @Bean
    public Queue getNfsDirCreatedQueue() {
        return new Queue(NFS_DIR_CREATED_Q, true);
    }

    @Bean
    public Queue getNfsDirDeletedQueue() {
        return new Queue(NFS_DIR_DELETED_Q, true);
    }

    @Bean
    public Queue getNfsUsageQueue() {
        return new Queue(NFS_USAGE_Q, false, false, false,
                of("x-message-ttl", TimeUnit.SECONDS.toMillis(60)));
    }

    @Bean
    public Queue getNfsSubDirCreateQueue() {
        return new Queue(NFS_SUB_DIRECTORY_CREATE_Q, true);
    }

    @Bean
    public Queue getNfsSubDirCreatedQueue() {
        return new Queue(NFS_SUB_DIRECTORY_CREATED_Q, true);
    }

    @Bean
    public Queue getNfsSubDirDeleteQueue() {
        return new Queue(NFS_SUB_DIRECTORY_DELETE_Q, true);
    }

    @Bean
    public Queue getNfsSubDirDeletedQueue() {
        return new Queue(NFS_SUB_DIRECTORY_DELETED_Q, true);
    }

    @Bean
    public Queue getCephPoolCreatedQueue() {
        return new Queue(CEPH_POOL_CREATED_Q, true);
    }

    @Bean
    public Queue getCephFsDirCreatedQueue() {
        return new Queue(CEPH_FS_DIR_CREATED_Q, true);
    }

    @Bean
    public Queue getCephUserDeletedQueue() {
        return new Queue(CEPH_USER_DELETED_Q, true);
    }

    @Bean
    public Queue getCephPoolDeletedQueue() {
        return new Queue(CEPH_POOL_DELETED_Q, true);
    }

    @Bean
    public Queue getCephFsDirDeletedQueue() {
        return new Queue(CEPH_FS_DIR_DELETED_Q, true);
    }

    @Bean
    public Queue getCephUserCreatedQueue() {
        return new Queue(CEPH_USER_CREATED_Q, true);
    }

    @Bean
    public Queue getCephPoolResizedQueue() {
        return new Queue(CEPH_POOL_RESIZED_Q, true);
    }

    @Bean
    public Queue getCephImageToCreateQueue() {
        return new Queue(CEPH_IMAGE_CREATE_Q, true);
    }

    @Bean
    public Queue getCephImageCreatedQueue() {
        return new Queue(CEPH_IMAGE_CREATED_Q, true);
    }

    @Bean
    public Queue getCephImageQuotaUpdateQueue() {
        return new Queue(CEPH_IMAGE_RESIZE_Q, true);
    }

    @Bean
    public Queue getCephImageQuotaUpdatedQueue() {
        return new Queue(CEPH_IMAGE_RESIZED_Q, true);
    }

    @Bean
    public Queue getCephImageDeleteQueue() {
        return new Queue(CEPH_IMAGE_DELETE_Q, true);
    }

    @Bean
    public Queue getCephImageDeletedQueue() {
        return new Queue(CEPH_IMAGE_DELETED_Q, true);
    }

    @Bean
    public Queue getCephImageUsageQueue() {
        return new Queue(CEPH_IMAGE_USAGE_Q, false, false, false, of("x-message-ttl", TimeUnit.SECONDS.toMillis(60)));
    }

    @Bean
    public Queue getCephPoolsUsageQueue() {
        return new Queue(CEPH_POOL_USAGE_Q, false, false, false, of("x-message-ttl", TimeUnit.SECONDS.toMillis(60)));
    }

    @Bean
    public Queue getCephfsDirectoryCreateQueue() {
        return new Queue(CEPHFS_SUB_DIRECTORY_CREATE_Q, true);
    }

    @Bean
    public Queue getCephfsDirectoryCreatedQueue() {
        return new Queue(CEPHFS_SUB_DIRECTORY_CREATED_Q, true);
    }

    @Bean
    public Queue getCephfsDirectoryDeleteQueue() {
        return new Queue(CEPHFS_SUB_DIRECTORY_DELETE_Q, true);
    }

    @Bean
    public Queue getCephfsDirectoryDeletedQueue() {
        return new Queue(CEPHFS_SUB_DIRECTORY_DELETED_Q, true);
    }

    @Bean
    public Queue getOperationAuditQueue() {
        return new Queue(OPERATION_AUDIT_Q, true);
    }

    @Bean
    public Queue getRequestAuditQueue() {
        return new Queue(REQUEST_AUDIT_Q, true);
    }

    @Bean
    public MessageConverter getMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public Queue getEventErrorQueue() {
        return new Queue(EVENT_ERROR_Q, true);
    }

    @Bean
    public Queue getAppStatusQueue() {
        return new Queue(APP_STATUS_Q, true);
    }
}