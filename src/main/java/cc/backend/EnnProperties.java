package cc.backend;

import cc.backend.audit.request.EnnContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/12/19.
 */
@ConfigurationProperties(prefix = "enn")
@RefreshScope
@Component
public class EnnProperties {
    private Map<String, Hibernate> hibernate;
    private boolean authEnabled;
    private StatusCheck statusCheck;
    private Security security;
    private String defaultCluster;
    private String defaultHibernate;
    private Http http;
    private Map<String, Cluster> clusters;

    @PostConstruct
    private void init() {
        clusters.values().forEach(cluster -> {
            List<String> monitors = cluster.getCeph().getMonitors();
            if (monitors != null) {
                String monitorsString = monitors.stream().collect(Collectors.joining(","));
                String[] monitorsArray = monitors.toArray(new String[monitors.size()]);
                cluster.getCeph().setMonitorsString(monitorsString);
                cluster.getCeph().setMonitorArray(monitorsArray);
            }

            Cluster.Ali ali = cluster.getAli();
            String aliStr = ali.getNodeAutoScale().getUsername() + ":" + ali.getNodeAutoScale().getPassword();
            ali.getNodeAutoScale().setBasicToken("Basic " + Base64.getEncoder().encodeToString(aliStr.getBytes()));

            Cluster.Scheduler scheduler = cluster.getScheduler();
            String schedulerStr = scheduler.getUsername() + ":" + scheduler.getPassword();
            scheduler.setBasicToken("Basic " + Base64.getEncoder().encodeToString(schedulerStr.getBytes()));
        });
    }

    public Map<String, Hibernate> getHibernate() {
        return hibernate;
    }

    public void setHibernate(Map<String, Hibernate> hibernate) {
        this.hibernate = hibernate;
    }

    public boolean isAuthEnabled() {
        return authEnabled;
    }

    public void setAuthEnabled(boolean authEnabled) {
        this.authEnabled = authEnabled;
    }

    public StatusCheck getStatusCheck() {
        return statusCheck;
    }

    public void setStatusCheck(StatusCheck statusCheck) {
        this.statusCheck = statusCheck;
    }

    public Security getSecurity() {
        return security;
    }

    public void setSecurity(Security security) {
        this.security = security;
    }

    public String getDefaultCluster() {
        return defaultCluster;
    }

    public void setDefaultCluster(String defaultCluster) {
        this.defaultCluster = defaultCluster;
    }

    public String getDefaultHibernate() {
        return defaultHibernate;
    }

    public void setDefaultHibernate(String defaultHibernate) {
        this.defaultHibernate = defaultHibernate;
    }

    public Http getHttp() {
        return http;
    }

    public void setHttp(Http http) {
        this.http = http;
    }

    public Map<String, Cluster> getClusters() {
        return clusters;
    }

    public void setClusters(Map<String, Cluster> clusters) {
        this.clusters = clusters;
    }

    public Cluster getCurrentCluster() {
        return getCluster(EnnContext.getClusterName());
    }

    public Cluster.Kubernetes getCurrentKubernetes() {
        return getCluster(EnnContext.getClusterName()).getKubernetes();
    }

    public Cluster.Ceph getCurrentCeph() {
        return getCluster(EnnContext.getClusterName()).getCeph();
    }

    public Cluster.Nfs getCurrentNfs() {
        return getCluster(EnnContext.getClusterName()).getNfs();
    }

    public Cluster.Opentsdb getCurrentOpentsdb() {
        return getCluster(EnnContext.getClusterName()).getOpentsdb();
    }

    public Cluster.Cc getCurrentCc() {
        return getCluster(EnnContext.getClusterName()).getCc();
    }

    public Cluster.Ebs getCurrentEbs() {
        return getCluster(EnnContext.getClusterName()).getEbs();
    }

    public Cluster getCluster(String clusterName) {
        return clusters.get(clusterName);
    }

    @Override
    public String toString() {
        return "EnnProperties{" +
                "hibernate=" + hibernate +
                ", authEnabled=" + authEnabled +
                ", statusCheck=" + statusCheck +
                ", security=" + security +
                ", defaultCluster='" + defaultCluster + '\'' +
                ", defaultHibernate='" + defaultHibernate + '\'' +
                ", http=" + http +
                ", clusters=" + clusters +
                '}';
    }

    public static class Hibernate {
        private String name;
        private String baseUrl;
        private String db;
        private String username;
        private String password;
        private String driverClassName;
        private boolean showSql;
        private String ddlAuto = "update";
        private String dialect = "org.hibernate.dialect.MySQLDialect";

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getBaseUrl() {
            return baseUrl;
        }

        public void setBaseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
        }

        public String getDb() {
            return db;
        }

        public void setDb(String db) {
            this.db = db;
        }

        public String getUrl() {
            return baseUrl + "/" + db;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getDriverClassName() {
            return driverClassName;
        }

        public void setDriverClassName(String driverClassName) {
            this.driverClassName = driverClassName;
        }

        public boolean isShowSql() {
            return showSql;
        }

        public void setShowSql(boolean showSql) {
            this.showSql = showSql;
        }

        public String getDdlAuto() {
            return ddlAuto;
        }

        public void setDdlAuto(String ddlAuto) {
            this.ddlAuto = ddlAuto;
        }

        public String getDialect() {
            return dialect;
        }

        public void setDialect(String dialect) {
            this.dialect = dialect;
        }

        @Override
        public String toString() {
            return "Hibernate{" +
                    "name='" + name + '\'' +
                    ", baseUrl='" + baseUrl + '\'' +
                    ", db='" + db + '\'' +
                    ", username='" + username + '\'' +
                    ", password='" + password + '\'' +
                    ", driverClassName='" + driverClassName + '\'' +
                    ", showSql=" + showSql +
                    ", ddlAuto='" + ddlAuto + '\'' +
                    ", dialect='" + dialect + '\'' +
                    '}';
        }
    }

    public static class StatusCheck {
        private PeriodMillis periodMillis;

        public PeriodMillis getPeriodMillis() {
            return periodMillis;
        }

        public void setPeriodMillis(PeriodMillis periodMillis) {
            this.periodMillis = periodMillis;
        }

        @Override
        public String toString() {
            return "StatusCheck{" +
                    "periodMillis=" + periodMillis +
                    '}';
        }

        public static class PeriodMillis {
            @Value("${default}")
            private long defaultPeriod = 61000;
            private long namespace;
            private long storage;

            public long getDefaultPeriod() {
                return defaultPeriod;
            }

            public void setDefaultPeriod(long defaultPeriod) {
                this.defaultPeriod = defaultPeriod;
            }

            public long getNamespace() {
                return namespace;
            }

            public void setNamespace(long namespace) {
                this.namespace = namespace;
            }

            public long getStorage() {
                return storage;
            }

            public void setStorage(long storage) {
                this.storage = storage;
            }

            @Override
            public String toString() {
                return "PeriodMillis{" +
                        "defaultPeriod=" + defaultPeriod +
                        ", namespace=" + namespace +
                        ", storage=" + storage +
                        '}';
            }
        }
    }

    public static class Http {
        private long connectTimeoutMillis;
        private long readTimeoutMillis;
        private long writeTimeoutMillis;

        public long getConnectTimeoutMillis() {
            return connectTimeoutMillis;
        }

        public void setConnectTimeoutMillis(long connectTimeoutMillis) {
            this.connectTimeoutMillis = connectTimeoutMillis;
        }

        public long getReadTimeoutMillis() {
            return readTimeoutMillis;
        }

        public void setReadTimeoutMillis(long readTimeoutMillis) {
            this.readTimeoutMillis = readTimeoutMillis;
        }

        public long getWriteTimeoutMillis() {
            return writeTimeoutMillis;
        }

        public void setWriteTimeoutMillis(long writeTimeoutMillis) {
            this.writeTimeoutMillis = writeTimeoutMillis;
        }

        @Override
        public String toString() {
            return "Http{" +
                    "connectTimeoutMillis=" + connectTimeoutMillis +
                    ", readTimeoutMillis=" + readTimeoutMillis +
                    ", writeTimeoutMillis=" + writeTimeoutMillis +
                    '}';
        }
    }

    public static class Security {
        private boolean enable;
        private String receive;

        public boolean isEnable() {
            return enable;
        }

        public void setEnable(boolean enable) {
            this.enable = enable;
        }

        public String getReceive() {
            return receive;
        }

        public void setReceive(String receive) {
            this.receive = receive;
        }

        @Override
        public String toString() {
            return "Security{" +
                    "enable=" + enable +
                    ", receive='" + receive + '\'' +
                    '}';
        }
    }


    public static class Cluster {
        private Kubernetes kubernetes;
        private Keystone keystone;
        private Ceph ceph;
        private Nfs nfs;
        private Opentsdb opentsdb;
        private Scheduler scheduler;
        private Cc cc;
        private Ali ali;
        private Ebs ebs;

        public Kubernetes getKubernetes() {
            return kubernetes;
        }

        public void setKubernetes(Kubernetes kubernetes) {
            this.kubernetes = kubernetes;
        }

        public Keystone getKeystone() {
            return keystone;
        }

        public void setKeystone(Keystone keystone) {
            this.keystone = keystone;
        }

        public Ceph getCeph() {
            return ceph;
        }

        public void setCeph(Ceph ceph) {
            this.ceph = ceph;
        }

        public Nfs getNfs() {
            return nfs;
        }

        public void setNfs(Nfs nfs) {
            this.nfs = nfs;
        }

        public Opentsdb getOpentsdb() {
            return opentsdb;
        }

        public void setOpentsdb(Opentsdb opentsdb) {
            this.opentsdb = opentsdb;
        }

        public Scheduler getScheduler() {
            return scheduler;
        }

        public void setScheduler(Scheduler scheduler) {
            this.scheduler = scheduler;
        }

        public Cc getCc() {
            return cc;
        }

        public void setCc(Cc cc) {
            this.cc = cc;
        }

        public Ali getAli() {
            return ali;
        }

        public void setAli(Ali ali) {
            this.ali = ali;
        }

        public Ebs getEbs() {
            return ebs;
        }

        public void setEbs(Ebs ebs) {
            this.ebs = ebs;
        }

        @Override
        public String toString() {
            return "Cluster{" +
                    "kubernetes=" + kubernetes +
                    ", keystone=" + keystone +
                    ", ceph=" + ceph +
                    ", nfs=" + nfs +
                    ", opentsdb=" + opentsdb +
                    ", scheduler=" + scheduler +
                    ", cc=" + cc +
                    ", ali=" + ali +
                    ", ebs=" + ebs +
                    '}';
        }

        public static class Kubernetes {
            private String vipHost;
            private String masterUrl;
            private Admin admin;
            private List<String> systemNamespaces;
            private boolean loginWithToken;
            private String token;

            public String getVipHost() {
                return vipHost;
            }

            public void setVipHost(String vipHost) {
                this.vipHost = vipHost;
            }

            public String getMasterUrl() {
                return masterUrl;
            }

            public void setMasterUrl(String masterUrl) {
                this.masterUrl = masterUrl;
            }

            public Admin getAdmin() {
                return admin;
            }

            public void setAdmin(Admin admin) {
                this.admin = admin;
            }

            public List<String> getSystemNamespaces() {
                return systemNamespaces;
            }

            public void setSystemNamespaces(List<String> systemNamespaces) {
                this.systemNamespaces = systemNamespaces;
            }

            public boolean isLoginWithToken() {
                return loginWithToken;
            }

            public void setLoginWithToken(boolean loginWithToken) {
                this.loginWithToken = loginWithToken;
            }

            public String getToken() {
                return token;
            }

            public void setToken(String token) {
                this.token = token;
            }

            @Override
            public String toString() {
                return "Kubernetes{" +
                        "vipHost='" + vipHost + '\'' +
                        ", masterUrl='" + masterUrl + '\'' +
                        ", admin=" + admin +
                        ", systemNamespaces=" + systemNamespaces +
                        ", loginWithToken=" + loginWithToken +
                        ", token='" + token + '\'' +
                        '}';
            }

            public static class Admin {
                private String username;
                private String password;

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                public String getPassword() {
                    return password;
                }

                public void setPassword(String password) {
                    this.password = password;
                }

                @Override
                public String toString() {
                    return "Admin{" +
                            "username='" + username + '\'' +
                            ", password='" + password + '\'' +
                            '}';
                }
            }
        }

        public static class Keystone {
            private String url;
            private Admin admin;
            private boolean cacheEnabled;
            private int tokenExpireMinutes;

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public Admin getAdmin() {
                return admin;
            }

            public void setAdmin(Admin admin) {
                this.admin = admin;
            }

            public boolean isCacheEnabled() {
                return cacheEnabled;
            }

            public void setCacheEnabled(boolean cacheEnabled) {
                this.cacheEnabled = cacheEnabled;
            }

            public int getTokenExpireMinutes() {
                return tokenExpireMinutes;
            }

            public void setTokenExpireMinutes(int tokenExpireMinutes) {
                this.tokenExpireMinutes = tokenExpireMinutes;
            }

            @Override
            public String toString() {
                return "Keystone{" +
                        "url='" + url + '\'' +
                        ", admin=" + admin +
                        ", cacheEnabled=" + cacheEnabled +
                        ", tokenExpireMinutes=" + tokenExpireMinutes +
                        '}';
            }

            public static class Admin {
                private String username;
                private String password;
                private String project;

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                public String getPassword() {
                    return password;
                }

                public void setPassword(String password) {
                    this.password = password;
                }

                public String getProject() {
                    return project;
                }

                public void setProject(String project) {
                    this.project = project;
                }

                @Override
                public String toString() {
                    return "Admin{" +
                            "username='" + username + '\'' +
                            ", password='" + password + '\'' +
                            ", project='" + project + '\'' +
                            '}';
                }
            }
        }

        public static class Ceph {
            private boolean enabled;
            private String dirPrefix;
            private List<String> monitors;
            private String monitorsString;
            private String[] monitorArray;

            public boolean isEnabled() {
                return enabled;
            }

            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }

            public String getDirPrefix() {
                return dirPrefix;
            }

            public void setDirPrefix(String dirPrefix) {
                this.dirPrefix = dirPrefix;
            }

            public List<String> getMonitors() {
                return monitors;
            }

            public void setMonitors(List<String> monitors) {
                this.monitors = monitors;
            }

            public String getMonitorsString() {
                return monitorsString;
            }

            public void setMonitorsString(String monitorsString) {
                this.monitorsString = monitorsString;
            }

            public String[] getMonitorArray() {
                return monitorArray;
            }

            public void setMonitorArray(String[] monitorArray) {
                this.monitorArray = monitorArray;
            }

            @Override
            public String toString() {
                return "Ceph{" +
                        "enabled=" + enabled +
                        ", dirPrefix='" + dirPrefix + '\'' +
                        ", monitors=" + monitors +
                        ", monitorsString='" + monitorsString + '\'' +
                        ", monitorArray=" + Arrays.toString(monitorArray) +
                        '}';
            }
        }

        public static class Nfs {
            private String address;
            private String serverDir;
            private boolean enabled;
            private String dirPrefix;

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getServerDir() {
                return serverDir;
            }

            public void setServerDir(String serverDir) {
                this.serverDir = serverDir;
            }

            public boolean isEnabled() {
                return enabled;
            }

            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }

            public String getDirPrefix() {
                return dirPrefix;
            }

            public void setDirPrefix(String dirPrefix) {
                this.dirPrefix = dirPrefix;
            }

            @Override
            public String toString() {
                return "Nfs{" +
                        "address='" + address + '\'' +
                        ", serverDir='" + serverDir + '\'' +
                        ", enabled=" + enabled +
                        ", dirPrefix='" + dirPrefix + '\'' +
                        '}';
            }
        }

        public static class Opentsdb {
            private String url;
            private String clusterName;
            private int delaySeconds;

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getClusterName() {
                return clusterName;
            }

            public void setClusterName(String clusterName) {
                this.clusterName = clusterName;
            }

            public int getDelaySeconds() {
                return delaySeconds;
            }

            public void setDelaySeconds(int delaySeconds) {
                this.delaySeconds = delaySeconds;
            }

            @Override
            public String toString() {
                return "Opentsdb{" +
                        "url='" + url + '\'' +
                        ", clusterName='" + clusterName + '\'' +
                        ", delaySeconds=" + delaySeconds +
                        '}';
            }
        }

        public static class Scheduler {
            private String host;
            private int port;
            private String username;
            private String password;
            private String basicToken;

            public String getHost() {
                return host;
            }

            public void setHost(String host) {
                this.host = host;
            }

            public int getPort() {
                return port;
            }

            public void setPort(int port) {
                this.port = port;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public String getBasicToken() {
                return basicToken;
            }

            public void setBasicToken(String basicToken) {
                this.basicToken = basicToken;
            }

            @Override
            public String toString() {
                return "Scheduler{" +
                        "host='" + host + '\'' +
                        ", port=" + port +
                        ", username='" + username + '\'' +
                        ", password='" + password + '\'' +
                        ", basicToken='" + basicToken + '\'' +
                        '}';
            }
        }

        public static class Cc {
            private String dependencyChecker;
            private Template template;

            public String getDependencyChecker() {
                return dependencyChecker;
            }

            public void setDependencyChecker(String dependencyChecker) {
                this.dependencyChecker = dependencyChecker;
            }

            public Template getTemplate() {
                return template;
            }

            public void setTemplate(Template template) {
                this.template = template;
            }

            @Override
            public String toString() {
                return "Cc{" +
                        "dependencyChecker='" + dependencyChecker + '\'' +
                        ", template=" + template +
                        '}';
            }

            public static class Template {
                private String url;

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                @Override
                public String toString() {
                    return "Template{" +
                            "url='" + url + '\'' +
                            '}';
                }
            }
        }

        public static class Ali {
            private NodeAutoScale nodeAutoScale;

            public NodeAutoScale getNodeAutoScale() {
                return nodeAutoScale;
            }

            public void setNodeAutoScale(NodeAutoScale nodeAutoScale) {
                this.nodeAutoScale = nodeAutoScale;
            }

            @Override
            public String toString() {
                return "Ali{" +
                        "nodeAutoScale=" + nodeAutoScale +
                        '}';
            }

            public static class NodeAutoScale {
                private String url;
                private String username;
                private String password;

                private String basicToken;

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                public String getPassword() {
                    return password;
                }

                public void setPassword(String password) {
                    this.password = password;
                }

                public String getBasicToken() {
                    return basicToken;
                }

                public void setBasicToken(String basicToken) {
                    this.basicToken = basicToken;
                }

                @Override
                public String toString() {
                    return "Ali{" +
                            "url='" + url + '\'' +
                            ", username='" + username + '\'' +
                            ", password='" + password + '\'' +
                            ", basicToken='" + basicToken + '\'' +
                            '}';
                }
            }
        }

        public static class Ebs {
            private boolean enabled;
            private long totalBytes;
            private String accessKey;
            private String secretKey;
            private String region;
            private String zone;

            public boolean isEnabled() {
                return enabled;
            }

            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }

            public long getTotalBytes() {
                return totalBytes;
            }

            public void setTotalBytes(long totalBytes) {
                this.totalBytes = totalBytes;
            }

            public String getAccessKey() {
                return accessKey;
            }

            public void setAccessKey(String accessKey) {
                this.accessKey = accessKey;
            }

            public String getSecretKey() {
                return secretKey;
            }

            public void setSecretKey(String secretKey) {
                this.secretKey = secretKey;
            }

            public String getRegion() {
                return region;
            }

            public void setRegion(String region) {
                this.region = region;
            }

            public String getZone() {
                return zone;
            }

            public void setZone(String zone) {
                this.zone = zone;
            }

            @Override
            public String toString() {
                return "Ebs{" +
                        "enabled=" + enabled +
                        ", totalBytes=" + totalBytes +
                        ", accessKey='" + accessKey + '\'' +
                        ", secretKey='" + secretKey + '\'' +
                        ", region='" + region + '\'' +
                        ", zone='" + zone + '\'' +
                        '}';
            }
        }
    }
}
