package cc.backend.kubernetes;

import cc.backend.kubernetes.node.dto.NodePodDto;
import cc.backend.kubernetes.node.services.NodesManagement;
import io.fabric8.kubernetes.api.model.Pod;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/13.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class NodesManagementTest {
    private static final Logger logger = LoggerFactory.getLogger(KubernetesClientManagerTest.class);

    @Inject
    private NodesManagement nodesManagement;

    @Test
    public void getNodes() throws Exception {

    }

    @Test
    public void getAllPods() throws Exception {

    }

    @Test
    public void getNode() throws Exception {

    }

    @Test
    public void getNodePods() throws Exception {
        List<Pod> nodePods = nodesManagement.getNodePods("10.19.132.119");
        List<NodePodDto> dtos = NodePodDto.from(nodePods);
        logger.info(dtos.toString());
    }

    @Test
    public void getNodeSum() throws Exception {

    }

}