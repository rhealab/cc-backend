package cc.backend.kubernetes.service;

import cc.backend.kubernetes.KubernetesClientManager;
import cc.lib.retcode.CcException;
import com.google.common.collect.ImmutableList;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.ServicePort;
import io.fabric8.kubernetes.api.model.ServiceSpec;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by xzy on 17-11-14.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceValidateTest {

    @Inject
    private ServiceValidator serviceValidator;

    @Inject
    private KubernetesClientManager clientManager;

    private static final Logger logger = LoggerFactory.getLogger(ServiceValidateTest.class);


    @Test
    public void testNodePort() {
        Service service = new Service();
        ServicePort servicePort1 = new ServicePort();
        servicePort1.setNodePort(32200);

        List<ServicePort> servicePortList = new ArrayList<>();

        servicePortList.add(servicePort1);

        ServiceSpec serviceSpec = new ServiceSpec();
        serviceSpec.setType("NodePort");
        serviceSpec.setPorts(servicePortList);

        ObjectMeta objectMeta = new ObjectMeta();
        objectMeta.setName("cc-backend");
        objectMeta.setNamespace("console");
        service.setMetadata(objectMeta);

        service.setSpec(serviceSpec);

        try {
            serviceValidator.validateSvcNodePortAndExternalIp(service);
        } catch (CcException e) {
            e.printStackTrace();
            logger.info("{}", e.getPayload());
        }
    }


    @Test
    public void nodePortConfliction() {
        Service service = new Service();
        ServicePort servicePort1 = new ServicePort();
        servicePort1.setNodePort(32233);

        ServicePort servicePort2 = new ServicePort();
        servicePort2.setNodePort(32200);

        List<ServicePort> servicePortList = new ArrayList<>();

        servicePortList.add(servicePort1);
        servicePortList.add(servicePort2);

        ServiceSpec serviceSpec = new ServiceSpec();
        serviceSpec.setType("NodePort");
        serviceSpec.setPorts(servicePortList);

        service.setSpec(serviceSpec);

        try {
            serviceValidator.validateSvcNodePortAndExternalIp(service);
        } catch (CcException e) {
            e.printStackTrace();
            logger.info("{}", e.getPayload());
        }
    }

    @Test
    public void nodePortConflictWithExternalIP() {
        Service service = new Service();
        ServicePort servicePort1 = new ServicePort();
        servicePort1.setNodePort(32283);

        ServicePort servicePort2 = new ServicePort();
        servicePort2.setNodePort(32236);

        List<ServicePort> servicePortList = new ArrayList<>();

        servicePortList.add(servicePort1);
        servicePortList.add(servicePort2);

        ServiceSpec serviceSpec = new ServiceSpec();
        serviceSpec.setType("NodePort");
        serviceSpec.setPorts(servicePortList);

        service.setSpec(serviceSpec);

        try {
            serviceValidator.validateSvcNodePortAndExternalIp(service);
        } catch (CcException e) {
            e.printStackTrace();
            logger.info("{}", e.getPayload());
        }
    }


    @Test
    public void testExternalIP() {
        Service service = new Service();
        ServicePort servicePort1 = new ServicePort();
        servicePort1.setPort(32273);

        ServicePort servicePort2 = new ServicePort();
        servicePort2.setPort(32266);

        List<ServicePort> servicePortList = new ArrayList<>();

        servicePortList.add(servicePort1);
        servicePortList.add(servicePort2);

        ServiceSpec serviceSpec = new ServiceSpec();
        serviceSpec.setPorts(servicePortList);
        serviceSpec.setExternalIPs(ImmutableList.of("10.19.132.11", "10.19.138.51"));

        service.setSpec(serviceSpec);

        try {
            serviceValidator.validateSvcNodePortAndExternalIp(service);
        } catch (CcException e) {
            e.printStackTrace();
            logger.info("{}", e.getPayload());
        }
    }

    @Test
    public void externalIPConflictWithNodePort() {
        Service service = new Service();
        ServicePort servicePort1 = new ServicePort();
        servicePort1.setPort(32233);

        ServicePort servicePort2 = new ServicePort();
        servicePort2.setPort(32200);

        List<ServicePort> servicePortList = new ArrayList<>();

        servicePortList.add(servicePort1);
        servicePortList.add(servicePort2);

        ServiceSpec serviceSpec = new ServiceSpec();
        serviceSpec.setPorts(servicePortList);
        serviceSpec.setExternalIPs(ImmutableList.of("10.19.132.11", "10.19.138.51"));

        service.setSpec(serviceSpec);

        try {
            serviceValidator.validateSvcNodePortAndExternalIp(service);
        } catch (CcException e) {
            e.printStackTrace();
            logger.info("{}", e.getPayload());
        }
    }

    @Test
    public void externalIPConfliction() {
        Service service = new Service();
        ServicePort servicePort1 = new ServicePort();
        servicePort1.setPort(32233);

        ServicePort servicePort2 = new ServicePort();
        servicePort2.setPort(32236);

        List<ServicePort> servicePortList = new ArrayList<>();

        servicePortList.add(servicePort1);
        servicePortList.add(servicePort2);

        ServiceSpec serviceSpec = new ServiceSpec();
        serviceSpec.setPorts(servicePortList);
        serviceSpec.setExternalIPs(ImmutableList.of("10.19.132.11", "10.19.138.100"));

        service.setSpec(serviceSpec);

        try {
            serviceValidator.validateSvcNodePortAndExternalIp(service);
        } catch (CcException e) {
            e.printStackTrace();
            logger.info("{}", e.getPayload());
        }
    }


    @Test
    public void test() {
        List<Service> existSvcList = clientManager.getClient().services().inAnyNamespace().list().getItems();

        logger.info("{}", existSvcList);
    }
}
