package cc.backend.kubernetes.service;

import cc.backend.audit.request.EnnContext;
import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.batch.form.dto.FormLabel;
import cc.backend.kubernetes.batch.form.dto.FormServicePort;
import cc.backend.kubernetes.batch.form.dto.ProtocolType;
import cc.backend.kubernetes.service.dto.ServiceCreateRequest;
import cc.backend.kubernetes.service.dto.ServiceType;
import com.google.common.collect.ImmutableList;
import io.fabric8.kubernetes.api.model.Service;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static cc.backend.kubernetes.TestConstants.*;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/27.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServicesManagementTest {
    @Inject
    private ServicesManagement servicesManagement;

    @Inject
    private KubernetesClientManager kubernetesClientManager;

    @Test
    public void serviceFieldTest() {
        List<Service> serviceList = kubernetesClientManager.getClient()
                .services()
                .inNamespace("cc-dev")
                .withField("spec.ports[*].nodePort", "32206")
                .list()
                .getItems();
        assert serviceList != null;
    }

    @Test
    public void createByForm() throws Exception {
        EnnContext.setUserId("dummy");
        ServiceCreateRequest request = new ServiceCreateRequest();
        request.setName("test");

        List<FormLabel> labels = new ArrayList<>();
        FormLabel formLabel1 = new FormLabel();
        formLabel1.setKey("l1");
        formLabel1.setValue("v1");
        FormLabel formLabel2 = new FormLabel();
        formLabel2.setKey("l2");
        formLabel2.setValue("v2");
        labels.add(formLabel1);
        labels.add(formLabel2);
        request.setLabels(labels);

        List<FormServicePort> ports = new ArrayList<>();
        FormServicePort requestPort = new FormServicePort();
        requestPort.setPort(80);
        requestPort.setTargetPort(80);
        requestPort.setProtocol(ProtocolType.TCP);
        ports.add(requestPort);
        request.setPorts(ports);
        request.setSelectors(labels);
        request.setType(ServiceType.External);
        request.setExternalIPs(ImmutableList.of("10.19.137.141"));
//        servicesManagement.createByForm(TEST_NAMESPACE_NAME, TEST_APP_NAME, request);
    }

    @Test
    public void delete() throws Exception {
        EnnContext.setUserId("dummy");
        servicesManagement.delete(TEST_NAMESPACE_NAME, TEST_APP_NAME, TEST_SERVICE_NAME);
    }

}