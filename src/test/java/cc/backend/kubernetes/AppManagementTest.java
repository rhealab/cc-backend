package cc.backend.kubernetes;

import cc.backend.audit.request.EnnContext;
import cc.backend.kubernetes.apps.data.App;
import cc.backend.kubernetes.apps.services.AppManagement;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.List;

import static cc.backend.kubernetes.TestConstants.TEST_APP_NAME;
import static cc.backend.kubernetes.TestConstants.TEST_NAMESPACE_NAME;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/1.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AppManagementTest {
    @Test
    public void validateAppName() {
    }

    @Test
    public void getAppStorageList() {
    }


    @Test
    public void getResourcesDetails() {
    }

    @Test
    public void getResourcesSummary() {
    }

    private static final Logger logger = LoggerFactory.getLogger(AppManagementTest.class);

    @Inject
    private AppManagement appManagement;

    @Test
    public void getSingleApp() {
        App testApp = new App();
        testApp.setName(TEST_APP_NAME);
        testApp.setNamespace(TEST_NAMESPACE_NAME);
        App app = appManagement.createApp(testApp, TEST_NAMESPACE_NAME);

        App singleApp = appManagement.getApp(app.getNamespace(), app.getName());

        appManagement.deleteApp(app.getNamespace(), app.getName());

        assert singleApp != null
                && singleApp.getId() == app.getId()
                && TEST_APP_NAME.equals(singleApp.getName())
                && singleApp.getCreatedBy() != null
                && singleApp.getCreatedOn() != null;
    }

    @Test
    public void getAllApps() {
        App testApp = new App();
        testApp.setName(TEST_APP_NAME);
        testApp.setNamespace(TEST_NAMESPACE_NAME);
        App app = appManagement.createApp(testApp, TEST_NAMESPACE_NAME);

        List<App> allApps = appManagement.getAllApps(TEST_NAMESPACE_NAME);

        App foundedApp = null;
        for (App a : allApps) {
            if (a.getId() == app.getId()) {
                foundedApp = a;
            }
        }
        appManagement.deleteApp(app.getNamespace(), app.getName());

        assert foundedApp != null
                && TEST_APP_NAME.equals(foundedApp.getName())
                && TEST_NAMESPACE_NAME.equals(foundedApp.getNamespace())
                && foundedApp.getCreatedBy() != null
                && foundedApp.getCreatedOn() != null;
    }

    @Test
    public void createApp() {
        EnnContext.setUserId("test");

        App testApp = new App();
        testApp.setName(TEST_APP_NAME);
        testApp.setNamespace(TEST_NAMESPACE_NAME);

        App app = appManagement.createApp(testApp, TEST_NAMESPACE_NAME);

        assert app != null
                && app.getId() != 0
                && TEST_APP_NAME.equals(app.getName())
                && app.getCreatedBy() != null
                && app.getCreatedOn() != null;
    }

    @Test
    public void deleteApp() {
        EnnContext.setUserId("test");
        appManagement.deleteApp(TEST_NAMESPACE_NAME, TEST_APP_NAME);
    }
}
