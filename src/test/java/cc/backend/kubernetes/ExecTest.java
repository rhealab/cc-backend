package cc.backend.kubernetes;

import io.fabric8.kubernetes.client.dsl.ExecListener;
import io.fabric8.kubernetes.client.dsl.ExecWatch;
import okhttp3.Response;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/7/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ExecTest {
    @Inject
    private KubernetesClientManager clientManager;

    @Test
    public void test() throws InterruptedException {

        try (ExecWatch watch = clientManager.getClient()
                .pods()
                .inNamespace("cc-dev")
                .withName("cc-account-1465376345-5p3k7")
                .readingInput(System.in)
                .writingOutput(System.out)
                .writingError(System.err)
                .withTTY()
                .usingListener(new SimpleListener())
                .exec()) {
            Thread.sleep(100 * 1000);
        }
    }

    private static class SimpleListener implements ExecListener {

        @Override
        public void onOpen(Response response) {
            System.out.println("The shell will remain open for 100 seconds.");
        }

        @Override
        public void onFailure(Throwable t, Response response) {
            System.err.println("shell barfed");
        }

        @Override
        public void onClose(int code, String reason) {
            System.out.println("The shell will now close.");
        }
    }

}
