package cc.backend.kubernetes;

import cc.backend.kubernetes.admin.services.SysAdminManagement;
import cc.backend.role.assignment.RoleAssignmentDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/1.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SysAdminManagementTest {
    public static final String TEST_USER = "cxq";
    private static final Logger logger = LoggerFactory.getLogger(SysAdminManagementTest.class);

    @Inject
    private SysAdminManagement sysAdminManagement;

    @Test
    public void getSysAdmins() {
        List<String> userList = new ArrayList<>();
        userList.add(TEST_USER);
        sysAdminManagement.batchGrantSysAdmin(userList);

        List<RoleAssignmentDto> sysAdmins = sysAdminManagement.getSysAdmins();

        RoleAssignmentDto assignedUser = null;
        if (sysAdmins != null) {
            for (RoleAssignmentDto dto : sysAdmins) {
                if (TEST_USER.equals(dto.getUserId())) {
                    assignedUser = dto;
                    break;
                }
            }
        }

        assert assignedUser != null
                && assignedUser.getUserId() != null
                && assignedUser.getRoleName() != null
                && assignedUser.getAssignedBy() != null
                && assignedUser.getAssignedDate() != null;
    }

    @Test
    public void assignSysAdmin() {
        List<String> userList = new ArrayList<>();
        userList.add(TEST_USER);
        sysAdminManagement.batchGrantSysAdmin(userList);
        sysAdminManagement.batchRevokeSysAdmin(userList);

        List<RoleAssignmentDto> sysAdmins = sysAdminManagement.getSysAdmins();

        RoleAssignmentDto assignedUser = null;
        if (sysAdmins != null) {
            for (RoleAssignmentDto dto : sysAdmins) {
                if (TEST_USER.equals(dto.getUserId())) {
                    assignedUser = dto;
                    break;
                }
            }
        }

        assert assignedUser != null
                && assignedUser.getUserId() != null
                && assignedUser.getRoleName() != null
                && assignedUser.getAssignedBy() != null
                && assignedUser.getAssignedDate() != null;
    }

    @Test
    public void unAssignSysAdmin() {
        List<String> userList = new ArrayList<>();
        userList.add(TEST_USER);
        sysAdminManagement.batchGrantSysAdmin(userList);
        sysAdminManagement.batchRevokeSysAdmin(userList);

        boolean contains = false;

        List<RoleAssignmentDto> sysAdmins = sysAdminManagement.getSysAdmins();

        if (sysAdmins != null) {
            for (RoleAssignmentDto dto : sysAdmins) {
                if (TEST_USER.equals(dto.getUserId())) {
                    contains = true;
                    break;
                }
            }
        }

        assert !contains;
    }
}
