package cc.backend.kubernetes;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/11.
 */
public interface TestConstants {
    String TEST_NAMESPACE_NAME = "cc-wjx-local";
    String TEST_LEGACY_NAMESPACE_NAME = "cc-wjx-dev";

    String TEST_NAMESPACE_FOR_DELETE_NAME = "cc-wjx-test-delete";

    String TEST_APP_NAME = "test-app";
    String TEST_APP_NAME2 = "test-app2";

    String TEST_DEPLOYMENT_NAME = "cc-mysql";
    String TEST_CONTAINER_NAME = "cc-mysql";
    String TEST_CONTAINER_MOUNT_PATH = "/var/lib/mysql";
    String TEST_SERVICE_NAME = "cc-mysql";
    int TEST_SERVICE_PORT = 3306;
    int TEST_SERVICE_TARGET_PORT = 3306;
    int TEST_SERVICE_NODE_PORT = 32366;
    String TEST_STORAGE_NAME = "cephfs-unit-test";

    String AUTHOR_ID = "jinxinwang";
    String TEST_NS_ADMIN_USER_ID = "ns-admin";
    String TEST_DEVELOPER_USER_ID = "developer";

    String TEST_FAULT_NAMESPACE_NAME = "cc-dev-xxx";
    String TEST_FAULT_SERVICE_NAME = "cc-mysql-xxx";

    String TEST_IMAGE_NAME = "10.19.140.200:30100/console/mysql:5.7";
}
