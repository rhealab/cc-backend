package cc.backend.kubernetes.namespaces.event;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2017/7/12.
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class K8sStorageClassProcessorTest {
    @Inject
    K8sStorageClassProcessor k8sStorageClassProcessor;

    @Test
    public void createStorageClass() throws Exception {
        boolean succeed = k8sStorageClassProcessor.createStorageClass("cc-wjx-dev");
        Assert.assertTrue(succeed);
    }

    @Test
    public void deleteStorageClass() throws Exception {
        boolean succeed = k8sStorageClassProcessor.deleteStorageClass("cc-wjx-dev");
        Assert.assertTrue(succeed);
    }
}