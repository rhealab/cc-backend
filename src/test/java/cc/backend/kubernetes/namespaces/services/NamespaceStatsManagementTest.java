package cc.backend.kubernetes.namespaces.services;

import cc.backend.event.data.EventSource;
import cc.backend.kubernetes.namespaces.dto.NamespaceDetailDto;
import cc.backend.kubernetes.namespaces.dto.NamespaceUpdateRequest;
import cc.backend.kubernetes.namespaces.validator.NamespaceStatsValidator;
import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.ResourceQuota;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

import static cc.backend.kubernetes.Constants.NAMESPACE_ALLOW_HOSTPATH_ANNOTATION;
import static cc.backend.kubernetes.Constants.NAMESPACE_ALLOW_PRIVILEGE_ANNOTATION;
import static cc.backend.kubernetes.TestConstants.TEST_LEGACY_NAMESPACE_NAME;
import static cc.backend.kubernetes.TestConstants.TEST_NAMESPACE_NAME;
import static cc.backend.kubernetes.storage.domain.BytesUnit.Gi;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/5/9.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class NamespaceStatsManagementTest {

    @Inject
    private NamespaceStatsManagement namespaceStatsManagement;

    @Inject
    private NamespaceStatsValidator namespaceStatsValidator;

    @Inject
    private MyNamespacesManagement namespacesManagement;

    @Test
    public void createQuotaForLegacy() throws Exception {
        NamespaceUpdateRequest request = new NamespaceUpdateRequest();

        request.setName("cc-stage");
        request.setCpuRequest(20.0);
        request.setCpuLimit(20.0);
        request.setMemoryRequestBytes(20 * Gi.getBytes());
        request.setMemoryLimitBytes(20 * Gi.getBytes());
        request.setHostPathStorageBytes(10 * Gi.getBytes());
        request.setPoolBytes(100 * Gi.getBytes());

        namespaceStatsValidator.validateQuotaForLegacyCreate(request);
        namespaceStatsManagement.createQuotaForLegacy(request);
    }

    @Test
    public void updateNamespaceQuota() throws Exception {
        NamespaceUpdateRequest request = new NamespaceUpdateRequest();
        request.setName(TEST_NAMESPACE_NAME);
        request.setPoolBytes(1024L * 1024 * 1024 * 100);
        request.setHostPathStorageBytes(1024L * 1024 * 1024 * 100);
        request.setMemoryLimitBytes(1024L * 1024 * 1024 * 10);
        request.setMemoryRequestBytes(1024L * 1024 * 1024 * 10);
        request.setCpuLimit(10.0);
        request.setCpuRequest(10.0);

        ResourceQuota resourceQuota = namespaceStatsValidator.validateNamespaceForUpdate(request);
        EventSource<NamespaceUpdateRequest> eventSource = namespaceStatsManagement.initForUpdate(request);
        namespaceStatsManagement.updateNamespaceQuota(request, eventSource, resourceQuota);
    }

    @Test
    public void updateLegacyNamespaceHostPathAndPrivilege() throws Exception {
        NamespaceUpdateRequest request = new NamespaceUpdateRequest();
        request.setName(TEST_LEGACY_NAMESPACE_NAME);
        request.setAllowHostpath(false);
        request.setAllowPrivilege(true);

        ResourceQuota resourceQuota = namespaceStatsValidator.validateNamespaceForUpdate(request);
        EventSource<NamespaceUpdateRequest> eventSource = namespaceStatsManagement.initForUpdate(request);
        namespaceStatsManagement.updateNamespaceQuota(request, eventSource, resourceQuota);
        NamespaceDetailDto detail = namespacesManagement.getNamespaceDetail(TEST_NAMESPACE_NAME);
        assert !detail.getAllowHostpath();
        assert detail.getAllowPrivilege();
    }

    @Test
    public void updateNamespaceHostPathAndPrivilege() throws Exception {
        NamespaceUpdateRequest request = new NamespaceUpdateRequest();
        request.setName(TEST_NAMESPACE_NAME);
        request.setAllowHostpath(false);
        request.setAllowPrivilege(true);

        ResourceQuota resourceQuota = namespaceStatsValidator.validateNamespaceForUpdate(request);
        EventSource<NamespaceUpdateRequest> eventSource = namespaceStatsManagement.initForUpdate(request);
        namespaceStatsManagement.updateNamespaceQuota(request, eventSource, resourceQuota);
        NamespaceDetailDto detail = namespacesManagement.getNamespaceDetail(TEST_NAMESPACE_NAME);
        assert !detail.getAllowHostpath();
        assert detail.getAllowPrivilege();

        Namespace namespace = namespacesManagement.getNamespace(TEST_NAMESPACE_NAME);
        assert !"true".equals(namespace.getMetadata().getAnnotations().get(NAMESPACE_ALLOW_HOSTPATH_ANNOTATION));
        assert "true".equals(namespace.getMetadata().getAnnotations().get(NAMESPACE_ALLOW_PRIVILEGE_ANNOTATION));
    }

    @Test
    public void updateNamespaceValidationTest() {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        NamespaceUpdateRequest request = new NamespaceUpdateRequest();
        request.setName("123");
        request.setCpuLimit(0.0);
        String s = request.toString();

        Set<ConstraintViolation<NamespaceUpdateRequest>> violations = validator.validate(request);

        assert !violations.isEmpty();
    }
}