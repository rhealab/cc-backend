package cc.backend.kubernetes.namespaces.services;

import cc.backend.audit.request.EnnContext;
import cc.backend.event.data.EventSource;
import cc.backend.kubernetes.namespaces.dto.NamespaceCreateRequest;
import cc.backend.kubernetes.namespaces.dto.NamespaceDto;
import cc.backend.kubernetes.namespaces.dto.ResourceCount;
import cc.keystone.client.domain.AssignInfo;
import cc.keystone.client.domain.AssignResult;
import cc.keystone.client.domain.RoleType;
import com.google.common.collect.ImmutableList;
import io.fabric8.kubernetes.api.model.Namespace;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static cc.backend.kubernetes.TestConstants.*;
import static cc.backend.kubernetes.storage.domain.BytesUnit.Gi;
import static org.junit.Assert.*;

/**
 * @author wangchunyang@gmail.com
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class NamespacesManagementTest {
    @Inject
    private MyNamespacesManagement namespacesManagement;

    @Inject
    private SysAdminNamespacesManagement sysAdminNamespacesManagement;

    @Inject
    private NamespaceRoleAssignmentManagement namespaceRoleAssignmentManagement;

    @Test
    public void getNamespace_not_exist() throws Exception {
        assertNull(namespacesManagement.getNamespace("xxx"));
    }

    @Test
    public void getNamespace_invalid_name() throws Exception {
        assertNull(namespacesManagement.getNamespace("^%&*("));
    }

    @Test
    public void getNamespace_exist() throws Exception {
        Namespace namespace = namespacesManagement.getNamespace("cc-dev");
        assertNotNull(namespace);
        assertEquals("cc-dev", namespace.getMetadata().getName());
    }

    @Test
    public void getNamespaces() throws Exception {
        List<NamespaceDto> namespaceDtoList = namespacesManagement.getMyNamespaceList();
        Assert.notNull(namespaceDtoList);
    }

    @Test
    public void getNamespace() throws Exception {

    }

    @Test
    public void getNamespaceDetail() throws Exception {

    }

    @Test
    public void createNamespace() throws Exception {
        NamespaceCreateRequest request = new NamespaceCreateRequest();

        request.setName(TEST_NAMESPACE_NAME);
        request.setAdminList(ImmutableList.of(TEST_NS_ADMIN_USER_ID));
        request.setCpuRequest(1.2);
        request.setCpuLimit(1.3);
        request.setMemoryRequestBytes(Gi.getBytes());
        request.setMemoryLimitBytes(2 * Gi.getBytes());
        request.setHostPathStorageBytes((long) (15.2 * Gi.getBytes()));
        request.setPoolBytes((long) (20.1 * Gi.getBytes()));

        EventSource<NamespaceCreateRequest> eventSource = sysAdminNamespacesManagement.initNamespaceForCreate(request);
        sysAdminNamespacesManagement.createNamespace(request, eventSource);
    }

    @Test
    public void deleteNamespace() throws Exception {
        EnnContext.setUserId("dummy");
//        sysAdminNamespacesManagement.deleteNamespace(TEST_NAMESPACE_NAME);
    }

    @Test
    public void updateNsUserRoles() throws Exception {
        EnnContext.setUserId("dummy");
        List<AssignInfo> assignInfoList = new ArrayList<>();
        AssignInfo assignInfo = new AssignInfo();
        assignInfo.setRole(RoleType.DEVELOPER);
        assignInfo.setUserId("cxq");
        assignInfoList.add(assignInfo);
        namespaceRoleAssignmentManagement.updateNsUserRoles(TEST_NAMESPACE_NAME, assignInfoList);
    }

    private List<AssignInfo> composeAssignInfoList(RoleType roleType) {
        List<AssignInfo> assignInfoList = new ArrayList<>();

        AssignInfo assignInfo1 = new AssignInfo();
        assignInfo1.setUserId(TEST_NS_ADMIN_USER_ID);
        assignInfo1.setRole(roleType);
        assignInfoList.add(assignInfo1);

        AssignInfo assignInfo2 = new AssignInfo();
        assignInfo2.setUserId(TEST_DEVELOPER_USER_ID);
        assignInfo2.setRole(roleType);
        assignInfoList.add(assignInfo1);

        return assignInfoList;
    }

    @Test
    public void assignNsAdmin() throws Exception {
        EnnContext.setUserId("dummy");

        List<AssignResult> results = namespaceRoleAssignmentManagement.grantRoles(TEST_NAMESPACE_NAME,
                composeAssignInfoList(RoleType.NS_ADMIN));

        assert results.size() == 2;
        assert results.get(0).getRole() == RoleType.NS_ADMIN;
        assert results.get(1).getRole() == RoleType.NS_ADMIN;
    }

    @Test
    public void getNamespaceResourceCount() throws Exception {
        ResourceCount resourceCount = namespacesManagement.getNamespaceResourceCount(TEST_NAMESPACE_NAME);
        assert resourceCount != null;
    }

    @Test
    public void unAssignNsAdmin() throws Exception {
        EnnContext.setUserId("dummy");
        List<AssignResult> results = namespaceRoleAssignmentManagement.revokeRoles(TEST_NAMESPACE_NAME,
                composeAssignInfoList(RoleType.NS_ADMIN));

        assert results.size() == 2;
        assert results.get(0).getRole() == RoleType.NS_ADMIN;
        assert results.get(1).getRole() == RoleType.NS_ADMIN;
    }

    @Test
    public void assignDeveloper() throws Exception {

    }

    @Test
    public void unAssignDeveloper() throws Exception {

    }

    @Test
    public void setAccessInfo() throws Exception {

    }

    @Test
    public void isCreatedByConsole() throws Exception {

    }
}