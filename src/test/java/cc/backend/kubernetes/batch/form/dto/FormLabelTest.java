package cc.backend.kubernetes.batch.form.dto;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2018/3/14.
 */
public class FormLabelTest {

    @Test
    public void toMap() {
        FormLabel l1 = new FormLabel();
        l1.setKey("k");
//        l1.setValue("v1");

        FormLabel l2 = new FormLabel();
        l2.setKey("k");
//        l2.setValue("v2");

        List<FormLabel> list = new ArrayList<>();
        list.add(l1);
        list.add(l2);

        Map<String, String> map = FormLabel.toMap(list);
        assert map.size() == 1;
    }
}