package cc.backend.kubernetes.batch.form;

import cc.backend.audit.request.EnnContext;
import cc.backend.kubernetes.batch.form.deprecated.FormManagement;
import cc.backend.kubernetes.batch.form.dto.*;
import cc.backend.kubernetes.storage.domain.AccessModeType;
import cc.backend.kubernetes.storage.domain.StorageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static cc.backend.kubernetes.TestConstants.*;
import static cc.backend.kubernetes.storage.domain.BytesUnit.Gi;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/21.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class FormManagementTest {
    private static final Logger logger = LoggerFactory.getLogger(FormManagementTest.class);

    @Inject
    private FormManagement formManagement;

    @Test
    public void createWithDescIsNull() throws Exception {
        EnnContext.setUserId("test");
        formManagement.create(TEST_NAMESPACE_NAME, TEST_APP_NAME, composeFormCreateRequest());
    }

    @Test
    public void create() throws Exception {
        EnnContext.setUserId("test");
        formManagement.create(TEST_NAMESPACE_NAME, TEST_APP_NAME, composeFormCreateRequest());
    }

    String yml = "apiVersion: v1\n" +
            "kind: Service\n" +
            "metadata:\n" +
            "  name: yaml-wordpress\n" +
            "  labels:\n" +
            "    app: wordpress\n" +
            "spec:\n" +
            "  ports:\n" +
            "    - port: 80\n" +
            "      targetPort: 80\n" +
            "  selector:\n" +
            "    app: wordpress\n" +
            "    tier: frontend\n" +
            "  type: NodePort\n" +
            "---\n" +
            "apiVersion: v1\n" +
            "kind: PersistentVolumeClaim\n" +
            "metadata:\n" +
            "  name: wangjinxin-wp-pv-claim\n" +
            "  labels:\n" +
            "    app: wordpress\n" +
            "spec:\n" +
            "  accessModes:\n" +
            "    - ReadWriteOnce\n" +
            "  resources:\n" +
            "    requests:\n" +
            "      storage: 20Gi\n" +
            "---\n" +
            "apiVersion: extensions/v1beta1\n" +
            "kind: Deployment\n" +
            "metadata:\n" +
            "  name: yaml-wordpress\n" +
            "  labels:\n" +
            "    app: wordpress\n" +
            "spec:\n" +
            "  strategy:\n" +
            "    type: Recreate\n" +
            "  template:\n" +
            "    metadata:\n" +
            "      labels:\n" +
            "        app: wordpress\n" +
            "        tier: frontend\n" +
            "    spec:\n" +
            "      containers:\n" +
            "      - image: wordpress:4.6.1-apache\n" +
            "        name: wordpress\n" +
            "        env:\n" +
            "        - name: WORDPRESS_DB_HOST\n" +
            "          value: wangjinxin-wordpress-mysql\n" +
            "        - name: WORDPRESS_DB_PASSWORD\n" +
            "          valueFrom:\n" +
            "            secretKeyRef:\n" +
            "              name: mysql-pass\n" +
            "              key: password.txt\n" +
            "        ports:\n" +
            "        - containerPort: 80\n" +
            "          name: wordpress\n" +
            "        volumeMounts:\n" +
            "        - name: wordpress-persistent-storage\n" +
            "          mountPath: /var/www/html\n" +
            "      volumes:\n" +
            "      - name: wordpress-persistent-storage\n" +
            "        persistentVolumeClaim:\n" +
            "          claimName: wangjinxin-wp-pv-claim\n";

    @Test
    public void createFromYml() throws Exception {
        formManagement.createFromYml(TEST_NAMESPACE_NAME, TEST_APP_NAME, yml);
    }

    @Test
    public void parseYml() throws Exception {
//        ParsedForm parsedForm = formManagement.parseYml(yml);
//        logger.info(parsedForm.toString());
    }

    @Test
    public void parseJson() throws Exception {
        File jsonFile = new File("/home/xzy/kube-demo/cc-backend.json");
        String jsonStr = com.google.common.io.Files.toString(jsonFile, StandardCharsets.UTF_8);
        logger.info(jsonStr);
        formManagement.createFromJson(TEST_NAMESPACE_NAME, TEST_APP_NAME, jsonStr);

        File ymlFile = new File("/home/xzy/kube-demo/cc-backend.yml");
        String ymlStr = com.google.common.io.Files.toString(ymlFile, StandardCharsets.UTF_8);
        logger.info(ymlStr);
        formManagement.createFromYml(TEST_NAMESPACE_NAME, TEST_APP_NAME, ymlStr);

    }

    private FormDeploymentServiceCreateRequest composeFormCreateRequest() {
        FormDeploymentServiceCreateRequest formCreateRequest = new FormDeploymentServiceCreateRequest();

        formCreateRequest.setName(TEST_DEPLOYMENT_NAME);
        formCreateRequest.setPortMappings(composeFormPortMappings());
        formCreateRequest.setReplicas(2);
        formCreateRequest.setNamespace(TEST_NAMESPACE_NAME);
        formCreateRequest.setLabels(composeFormLabels());
        formCreateRequest.setMinReadySeconds(1);
        formCreateRequest.setRevisionHistoryLimit(0);
        formCreateRequest.setPaused(false);
        formCreateRequest.setRollbackRevision(100L);
        formCreateRequest.setStrategy(composeFormStrategy());
        formCreateRequest.setStorages(composeFormStorages());
        formCreateRequest.setContainers(composeFormContainers());

        return formCreateRequest;
    }

    private List<FormServicePort> composeFormPortMappings() {
        List<FormServicePort> formPortMappings = new ArrayList<>();
        FormServicePort formPortMapping = new FormServicePort();

        formPortMapping.setPort(TEST_SERVICE_PORT);
        formPortMapping.setProtocol(ProtocolType.TCP);
        formPortMapping.setTargetPort(TEST_SERVICE_TARGET_PORT);

        formPortMappings.add(formPortMapping);
        return formPortMappings;
    }

    private List<FormLabel> composeFormLabels() {
        List<FormLabel> formLabels = new ArrayList<>();
        FormLabel formLabel = new FormLabel();

        formLabel.setKey("app");
        formLabel.setValue("cc");
        formLabel.setKey("tier");
        formLabel.setValue("mysql");

        formLabels.add(formLabel);
        return formLabels;
    }

    private FormDeploymentStrategy composeFormStrategy() {
        FormDeploymentStrategy formDeploymentStrategy = new FormDeploymentStrategy();

        formDeploymentStrategy.setMaxSurge(1);
        formDeploymentStrategy.setMaxUnavailable(1);
        formDeploymentStrategy.setType(StrategyType.RollingUpdate);

        return formDeploymentStrategy;
    }

    private List<FormStorage> composeFormStorages() {
        List<FormStorage> formStorages = new ArrayList<>();
        FormStorage formStorage = new FormStorage();
        formStorage.setAccessMode(AccessModeType.ReadWriteOnce);
        formStorage.setAmountBytes(Gi.getBytes());
        formStorage.setStorageName(TEST_STORAGE_NAME);
        formStorage.setStorageType(StorageType.CephFS);
        formStorages.add(formStorage);
        return formStorages;
    }

    private List<FormContainerDeprecated> composeFormContainers() {
        List<FormContainerDeprecated> formContainers = new ArrayList<>();
        FormContainerDeprecated formContainer = new FormContainerDeprecated();

        formContainer.setContainerCommand("ls");
        formContainer.setContainerCommandArgs("-al");
        formContainer.setContainerImage(TEST_IMAGE_NAME);
        formContainer.setCpuLimits(1.1);
        formContainer.setCpuRequests(1);
        formContainer.setMemoryLimitsBytes(Gi.getBytes());
        formContainer.setMemoryRequestsBytes(Gi.getBytes());
        formContainer.setRunAsPrivileged(false);
        formContainer.setName(TEST_CONTAINER_NAME);
        formContainer.setEnv(composeFormEnvs());
        formContainer.setVolumeMounts(composeFormVolumeMounts());

        formContainers.add(formContainer);
        return formContainers;
    }

    private List<FormEnv> composeFormEnvs() {
        List<FormEnv> formEnvs = new ArrayList<>();
        FormEnv formEnv1 = new FormEnv();
        FormEnv formEnv2 = new FormEnv();

        formEnv1.setName("MYSQL_ROOT_PASSWORD");
        formEnv1.setValue("root");

        formEnv2.setName("MYSQL_DATABASE");
        formEnv2.setValue("cc");

        formEnvs.add(formEnv1);
        formEnvs.add(formEnv2);
        return formEnvs;
    }

    private List<FormVolumeMount> composeFormVolumeMounts() {
        List<FormVolumeMount> formVolumeMounts = new ArrayList<>();
        FormVolumeMount formVolumeMount = new FormVolumeMount();

        formVolumeMount.setPath(TEST_CONTAINER_MOUNT_PATH);
        formVolumeMount.setStorageName(TEST_STORAGE_NAME);

        formVolumeMounts.add(formVolumeMount);
        return formVolumeMounts;
    }
}