package cc.backend.kubernetes.workload.deployment.services;

import cc.backend.audit.request.EnnContext;
import cc.backend.kubernetes.workload.deployment.service.DeploymentsManagement;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

import static cc.backend.kubernetes.TestConstants.*;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/5/9.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DeploymentsManagementTest {
    @Inject
    DeploymentsManagement deploymentsManagement;

    @Test
    public void create() throws Exception {
        Deployment deployment = new Deployment();
        deploymentsManagement.create(TEST_NAMESPACE_NAME, TEST_APP_NAME, deployment);
    }

    @Test
    public void deleteDeployment() throws Exception {
        EnnContext.setUserId("dummy");
        deploymentsManagement.delete(TEST_NAMESPACE_NAME, TEST_APP_NAME,TEST_DEPLOYMENT_NAME);
    }
}