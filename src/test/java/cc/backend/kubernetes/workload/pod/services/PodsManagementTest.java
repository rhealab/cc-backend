package cc.backend.kubernetes.workload.pod.services;

import cc.backend.kubernetes.KubernetesClientManager;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.client.KubernetesClientException;
import io.fabric8.kubernetes.client.Watch;
import io.fabric8.kubernetes.client.Watcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by NormanWang06@gmail.com (jinxin) on 2017/6/27.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PodsManagementTest {
    Logger logger = LoggerFactory.getLogger(PodsManagementTest.class);

    @Inject
    private KubernetesClientManager kubernetesClientManager;

    @Test
    public void podWatch() throws Exception {
        final CountDownLatch closeLatch = new CountDownLatch(1);
        try (Watch watch = kubernetesClientManager.getClient().pods().inAnyNamespace().watch(new Watcher<Pod>() {
            @Override
            public void eventReceived(Action action, Pod resource) {
                logger.info("{}: {}", action, resource);
            }

            @Override
            public void onClose(KubernetesClientException cause) {
                logger.debug("Watcher onClose");
                if (cause != null) {
                    logger.error(cause.getMessage(), cause);
                    closeLatch.countDown();
                }
            }
        })) {
            closeLatch.await(1000000, TimeUnit.DAYS);
        } catch (KubernetesClientException | InterruptedException e) {
            logger.error("Could not watch resources", e);
        }
    }
}