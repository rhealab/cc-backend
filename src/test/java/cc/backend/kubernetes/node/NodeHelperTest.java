package cc.backend.kubernetes.node;

import cc.backend.kubernetes.node.dto.NodeDiskQuotaState;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2018/2/7.
 */
public class NodeHelperTest {
    String json = "{\"Capacity\":1999421038592,\"CurUseSize\":1697035189,\"CurQuotaSize\":0,\"AvaliableSize\":\"1.82TB, 1.82TB\",\"Disabled\":false,\"DiskStatus\":[{\"Capacity\":1999421038592,\"CurUseSize\":1697035189,\"CurQuotaSize\":0,\"AvaliableSize\":\"1.82TB, 1.82TB\",\"MountPath\":\"/xfs/disk1/\"}]}";

    @Test
    public void getDiskQuotaState() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        NodeDiskQuotaState value = mapper.readValue(json, NodeDiskQuotaState.class);
        assert value != null;
    }
}