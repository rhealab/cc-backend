package cc.backend.kubernetes;

import io.fabric8.kubernetes.api.model.extensions.DeploymentList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

/**
 * @author wangchunyang@gmail.com
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class KubernetesClientManagerTest {
    private static final Logger logger = LoggerFactory.getLogger(KubernetesClientManagerTest.class);

    @Inject
    private KubernetesClientManager clientManager;

    @Test
    public void getDeployment() {
        DeploymentList list = clientManager.getClient().extensions().deployments().list();
        logger.debug("deployments.size={}", list.getItems().size());
    }
}