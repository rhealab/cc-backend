package cc.backend.kubernetes.storage;

import cc.backend.kubernetes.storage.domain.AccessModeType;
import cc.backend.kubernetes.storage.domain.Storage;
import cc.backend.kubernetes.storage.domain.StorageCreateRequest;
import cc.backend.kubernetes.storage.domain.StorageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

import static org.junit.Assert.*;

/**
 * @author NormanWang06@gmail.com (wangjinxin) on 2018/1/3.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class StorageManagementTest {
    @Inject
    private StorageManagement storageManagement;

    @Test
    public void initStorageForCreate() {
        StorageCreateRequest request = new StorageCreateRequest();
        request.setNamespaceName("cc-wjx-local");
        request.setAccessMode(AccessModeType.ReadWriteMany);
        request.setAmountBytes(1024 * 1024);
        request.setStorageName("test");
        request.setStorageType(StorageType.NFS);

        storageManagement.initStorageForCreate(Storage.newInstance(request));
    }

    @Test
    public void createStorage() {
    }
}