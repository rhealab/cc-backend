package cc.backend.kubernetes.storage.usage;

import cc.backend.kubernetes.storage.usage.entity.CephImageUsage;
import cc.backend.kubernetes.storage.usage.entity.CephPoolUsage;
import cc.backend.kubernetes.storage.usage.entity.CephfsDirectoryUsage;

import java.util.Date;

/**
 * @author wangchunyang@gmail.com
 */
public class UsageTestHelper {
    public static CephImageUsage buildImageUsage1() {
        CephImageUsage usage = new CephImageUsage();
        usage.setPoolName("p1");
        usage.setImageName("img1");
        usage.setBytesUsed(2000);
        usage.setBytesProvisioned(3000);
        usage.setTimestamp(new Date());
        return usage;
    }

    public static CephImageUsage buildImageUsage2() {
        CephImageUsage usage = new CephImageUsage();
        usage.setPoolName("p2");
        usage.setImageName("img2");
        usage.setBytesUsed(20000);
        usage.setBytesProvisioned(30000);
        usage.setTimestamp(new Date());
        return usage;
    }

    public static CephUsageMessage buildCephUsageMessage() {
        CephUsageMessage m = new CephUsageMessage();
        m.setTimestamp(new Date());
        m.setTotalAvailBytes(1000);
        m.setTotalUsedBytes(2000);
        m.setTotalBytes(3000);
        m.setCephfsAvailBytes(400);
        m.setCephfsUsedBytes(300);
        return m;
    }

    public static CephPoolUsage buildPoolUsage1() {
        CephPoolUsage pu = new CephPoolUsage();
        pu.setBytesUsed(200);
        pu.setPoolName("k8s.p1");
        pu.setNamespaceName("p1");
        return pu;
    }

    public static CephPoolUsage buildPoolUsage2() {
        CephPoolUsage pu = new CephPoolUsage();
        pu.setBytesUsed(500);
        pu.setPoolName("k8s.p2");
        pu.setNamespaceName("p2");
        return pu;
    }

    public static CephfsDirectoryUsage buildDirectoryUsage1() {
        CephfsDirectoryUsage u = new CephfsDirectoryUsage();
        u.setBytesUsed(200);
        u.setNamespaceName("p1");
        u.setDirectoryName("p1");
        return u;
    }

    public static CephfsDirectoryUsage buildDirectoryUsage2() {
        CephfsDirectoryUsage u = new CephfsDirectoryUsage();
        u.setBytesUsed(600);
        u.setNamespaceName("p2");
        u.setDirectoryName("p2");
        return u;
    }
}
