package cc.backend.kubernetes.storage.usage;

import cc.backend.RabbitConfig;
import cc.backend.kubernetes.storage.usage.entity.*;
import com.google.common.collect.ImmutableList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author wangchunyang@gmail.com
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UsageConsumerTest {
    @Inject
    private RabbitMessagingTemplate messagingTemplate;

    @Inject
    private CephImageUsageRepository imageUsageRepository;

    @Inject
    private CephPoolUsageRepository poolUsageRepository;

    @Inject
    private CephGlobalUsageRepository globalUsageRepository;

    @Inject
    private CephfsDirectoryUsageRepository directoryUsageRepository;

    private CephImageUsage iu1;
    private CephImageUsage iu2;
    private CephUsageMessage cephUsageMessage;
    private CephPoolUsage pu1;
    private CephPoolUsage pu2;
    private CephfsDirectoryUsage du1;
    private CephfsDirectoryUsage du2;

    @Before
    public void prepare() {
        iu1 = UsageTestHelper.buildImageUsage1();
        iu2 = UsageTestHelper.buildImageUsage2();
        pu1 = UsageTestHelper.buildPoolUsage1();
        pu2 = UsageTestHelper.buildPoolUsage2();
        du1 = UsageTestHelper.buildDirectoryUsage1();
        du2 = UsageTestHelper.buildDirectoryUsage2();
        cephUsageMessage = UsageTestHelper.buildCephUsageMessage();
        cephUsageMessage.addPoolUsage(pu1);
        cephUsageMessage.addPoolUsage(pu2);
        cephUsageMessage.addDirectoryUsage(du1);
        cephUsageMessage.addDirectoryUsage(du2);
    }

    @After
    public void cleanup() {
        imageUsageRepository.deleteAll();
        poolUsageRepository.deleteAll();
        globalUsageRepository.deleteAll();
        directoryUsageRepository.deleteAll();
    }

    @Test
    public void gotImageUsages() throws Exception {
        List<CephImageUsage> usages = ImmutableList.of(iu1, iu2);
        messagingTemplate.convertAndSend(RabbitConfig.CEPH_IMAGE_USAGE_Q, usages);
        Thread.sleep(3000);
        CephImageUsage found = imageUsageRepository.findOne(new CephImageUsage.ImageUsageId(iu1.getPoolName(), iu1.getImageName()));
        assertEquals(iu1.getBytesUsed(), found.getBytesUsed());

        found = imageUsageRepository.findOne(new CephImageUsage.ImageUsageId(iu2.getPoolName(), iu2.getImageName()));
        assertEquals(iu2.getBytesUsed(), found.getBytesUsed());
    }

    @Test
    public void gotCephUsages() throws Exception {
        messagingTemplate.convertAndSend(RabbitConfig.CEPH_POOL_USAGE_Q, cephUsageMessage);
        Thread.sleep(3000);


        CephPoolUsage found = poolUsageRepository.findOne(pu1.getPoolName());
        assertEquals(pu1.getBytesUsed(), found.getBytesUsed());
        assertNotNull(found.getTimestamp());

        found = poolUsageRepository.findOne(pu2.getPoolName());
        assertEquals(pu2.getBytesUsed(), found.getBytesUsed());
        assertNotNull(found.getTimestamp());

        CephGlobalUsage storedGU = globalUsageRepository.findAll().get(0);
        assertEquals(cephUsageMessage.getTotalBytes(), storedGU.getTotalBytes());

        CephfsDirectoryUsage storedDU1 = directoryUsageRepository.findOne(du1.getDirectoryName());
        assertEquals(du1.getBytesUsed(), storedDU1.getBytesUsed());
        assertNotNull(storedDU1.getTimestamp());
    }


}