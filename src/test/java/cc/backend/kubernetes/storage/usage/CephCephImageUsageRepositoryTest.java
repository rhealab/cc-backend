package cc.backend.kubernetes.storage.usage;

import cc.backend.kubernetes.storage.usage.entity.CephImageUsage;
import cc.backend.kubernetes.storage.usage.entity.CephImageUsageRepository;
import com.google.common.collect.ImmutableList;
import org.exparity.hamcrest.date.DateMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * @author wangchunyang@gmail.com
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CephCephImageUsageRepositoryTest {

    @Inject
    private CephImageUsageRepository repository;
    private CephImageUsage u1;
    private CephImageUsage u2;

    @Before
    public void prepare() {
        u1 = UsageTestHelper.buildImageUsage1();
        u2 = UsageTestHelper.buildImageUsage2();
    }

    @After
    public void cleanup() {
        repository.deleteAll();
    }

    @Test
    public void create() {
        repository.save(u1);
        CephImageUsage found = repository.findOne(new CephImageUsage.ImageUsageId("p1", "img1"));
        assertEquals(2000, found.getBytesUsed());
    }


    @Test
    public void batchCreate() {
        repository.save(ImmutableList.of(u1, u2));

        CephImageUsage found = repository.findOne(new CephImageUsage.ImageUsageId("p1", "img1"));
        assertEquals(2000, found.getBytesUsed());

        found = repository.findOne(new CephImageUsage.ImageUsageId("p2", "img2"));
        assertEquals(20000, found.getBytesUsed());
    }

    @Test
    public void update() {
        repository.save(u1);

        CephImageUsage u = new CephImageUsage();
        u.setPoolName(u1.getPoolName());
        u.setImageName(u1.getImageName());
        u.setBytesUsed(100);
        u.setBytesProvisioned(500);
        Date oneMinuteAgo = Date.from(Instant.now().minusSeconds(60));
        u.setTimestamp(oneMinuteAgo);

        repository.save(u);

        assertEquals(1, repository.findAll().size());

        CephImageUsage found = repository.findOne(new CephImageUsage.ImageUsageId(u1.getPoolName(), u1.getImageName()));

        assertThat(oneMinuteAgo, DateMatchers.within(1, ChronoUnit.SECONDS, found.getTimestamp()));
        assertEquals(u.getBytesUsed(), found.getBytesUsed());
        assertEquals(u.getBytesProvisioned(), found.getBytesProvisioned());
    }

    @Test
    public void batchUpdate() {
        repository.save(ImmutableList.of(u1, u2));

        CephImageUsage u11 = new CephImageUsage();
        u11.setPoolName(u1.getPoolName());
        u11.setImageName(u1.getImageName());
        u11.setBytesUsed(100);
        u11.setBytesProvisioned(500);
        Date oneMinuteAgo = Date.from(Instant.now().minusSeconds(60));
        u11.setTimestamp(oneMinuteAgo);

        CephImageUsage u22 = new CephImageUsage();
        u22.setPoolName(u2.getPoolName());
        u22.setImageName(u2.getImageName());
        u22.setBytesUsed(800);
        u22.setBytesProvisioned(1500);
        u22.setTimestamp(oneMinuteAgo);

        repository.save(ImmutableList.of(u11, u22));

        assertEquals(2, repository.findAll().size());

        CephImageUsage foundU1 = repository.findOne(new CephImageUsage.ImageUsageId(u1.getPoolName(), u1.getImageName()));

        assertThat(oneMinuteAgo, DateMatchers.within(1, ChronoUnit.SECONDS, foundU1.getTimestamp()));
        assertEquals(u11.getBytesUsed(), foundU1.getBytesUsed());
        assertEquals(u11.getBytesProvisioned(), foundU1.getBytesProvisioned());

        CephImageUsage foundU2 = repository.findOne(new CephImageUsage.ImageUsageId(u2.getPoolName(), u2.getImageName()));
        assertThat(oneMinuteAgo, DateMatchers.within(1, ChronoUnit.SECONDS, foundU2.getTimestamp()));
        assertEquals(u22.getBytesUsed(), foundU2.getBytesUsed());
        assertEquals(u22.getBytesProvisioned(), foundU2.getBytesProvisioned());
    }
}