package cc.backend.kubernetes.storage;

import cc.backend.audit.request.EnnContext;
import cc.backend.kubernetes.storage.domain.AccessModeType;
import cc.backend.kubernetes.storage.domain.Storage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

import static cc.backend.kubernetes.storage.domain.BytesUnit.Gi;

/**
 * @author wangchunyang@gmail.com
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CephfsStorageManagerTest {
    @Inject
    private CephfsStorageManager cephfsStorageManager;
    private Storage storage;

    @Before
    public void setUp() {
        storage = new Storage();
        storage.setAccessMode(AccessModeType.ReadWriteOnce);
        storage.setNamespaceName("cc-sandbox");
        storage.setStorageName("cephfs-unit-test");
        storage.setAmountBytes(10 * Gi.getBytes());
    }

    @Test
    public void createDirectory() throws Exception {
        EnnContext.setUserId("user1");
        EnnContext.setRequestId("request1");
        cephfsStorageManager.sendDirectoryDeleteMessage(storage);
    }

    @Test
    public void createPvc() throws Exception {
//        cephfsStorageManager.createPvc(storage);
    }

    @Test
    public void createPv() throws Exception {
//        cephfsStorageManager.createPv(storage);
    }

}