package cc.backend.kubernetes.storage;

import cc.backend.kubernetes.KubernetesClientManager;
import cc.backend.kubernetes.storage.domain.Storage;
import io.fabric8.kubernetes.api.model.PersistentVolumeClaim;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

/**
 * Created by NormanWang06@gmail.com (jinxin) on 2017/7/4.
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class CephRbdStorageManagerTest {

    @Inject
    private KubernetesClientManager kubernetesClientManager;

    @Test
    public void updatePvc() throws Exception {
        Storage storage = new Storage();
        storage.setNamespaceName("cc-wjx-dev");
        storage.setStorageName("rbd-rwo-test");
        storage.setAmountBytes(1024L * 1024 * 1024 * 2);
        PersistentVolumeClaim pvc = kubernetesClientManager.getClient()
                .persistentVolumeClaims()
                .inNamespace(storage.getNamespaceName())
                .withName("rbd-rwo-test")
                .edit()
                .editSpec()
                .editResources()
                .withRequests(storage.capacity())
                .endResources()
                .endSpec()
                .done();

        assert pvc != null;
    }
}