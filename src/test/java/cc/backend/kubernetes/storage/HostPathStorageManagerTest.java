package cc.backend.kubernetes.storage;

import cc.backend.kubernetes.storage.domain.Storage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

import static cc.backend.kubernetes.storage.domain.BytesUnit.Gi;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/13.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class HostPathStorageManagerTest {
    private static final Logger logger = LoggerFactory.getLogger(HostPathStorageManagerTest.class);
    @Inject
    private HostPathStorageManager hostPathStorageManager;

    @Test
    public void createPvc() throws Exception {

    }

    @Test
    public void createPv() throws Exception {

    }

    @Test
    public void delete() throws Exception {

    }

    @Test
    public void updatePv() throws Exception {
        Storage storage = new Storage();
        storage.setStorageName("teststorage2");
        storage.setAmountBytes(Gi.getBytes());
        storage.setNamespaceName("cxq5");
//        hostPathStorageManager.updatePv(storage);
    }

    @Test
    public void updatePvc() throws Exception {
        Storage storage = new Storage();
        storage.setStorageName("teststorage2");
        storage.setAmountBytes(Gi.getBytes() * 2);
        storage.setNamespaceName("cxq5");
//        hostPathStorageManager.updatePvc(storage);
    }

    @Test
    public void getPvBytesUsed() throws Exception {

    }

    @Test
    public void getUpdateCapacityState() throws Exception {
        Storage storage = new Storage();
        storage.setStorageName("teststorage2");
        storage.setAmountBytes(Gi.getBytes() * 3);
        storage.setNamespaceName("cxq5");
        String state = hostPathStorageManager.getUpdateCapacityState(storage.getPvName());
        logger.info(state);
    }
}