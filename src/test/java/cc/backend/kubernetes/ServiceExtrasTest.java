package cc.backend.kubernetes;

import cc.backend.audit.request.EnnContext;
import cc.backend.kubernetes.batch.form.dto.FormLabel;
import cc.backend.kubernetes.batch.form.dto.FormServicePort;
import cc.backend.kubernetes.batch.form.dto.ProtocolType;
import cc.backend.kubernetes.service.ServicesManagement;
import cc.backend.kubernetes.service.data.ServiceExtras;
import cc.backend.kubernetes.service.data.ServiceExtrasRepository;
import cc.backend.kubernetes.service.dto.ServiceCreateRequest;
import cc.backend.kubernetes.service.dto.ServiceType;
import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * @author  yanzhxiang
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceExtrasTest {

    @Inject
    private ServicesManagement servicesManagement;


    @Test
    public void testCreateService(){
        EnnContext.setUserId("dummy");
        ServiceCreateRequest request = new ServiceCreateRequest();
        request.setName("test");

        List<FormLabel> labels = new ArrayList<>();
        FormLabel formLabel1 = new FormLabel();
        formLabel1.setKey("l1");
        formLabel1.setValue("v1");
        FormLabel formLabel2 = new FormLabel();
        formLabel2.setKey("l2");
        formLabel2.setValue("v2");
        labels.add(formLabel1);
        labels.add(formLabel2);
        request.setLabels(labels);

        List<FormServicePort> ports = new ArrayList<>();
        FormServicePort requestPort = new FormServicePort();
        requestPort.setPort(80);
        requestPort.setTargetPort(80);
        requestPort.setProtocol(ProtocolType.TCP);
        ports.add(requestPort);
        request.setPorts(ports);
        request.setSelectors(labels);
        request.setType(ServiceType.External);
        request.setExternalIPs(ImmutableList.of("10.19.137.141"));
//        servicesManagement.createByForm("cxq5", "test-app", request);
    }

    @Inject
    ServiceExtrasRepository serviceExtrasRepository;

    @Test
    public void testGetService(){
        ServiceExtras serviceExtras = serviceExtrasRepository.findByNamespaceAndName("cxq5","test1");
        System.out.println("mysql time"+ serviceExtras.getCreatedOn());
    }


    @Test
    public void testDeleteService(){
        servicesManagement.delete("cxq5","test-app1","test1");
    }
}
