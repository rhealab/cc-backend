package cc.backend.kubernetes;

import cc.backend.kubernetes.workload.statefulset.StatefulSetManagement;
import io.fabric8.kubernetes.api.model.extensions.StatefulSet;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

/**
 * Created by xzy on 17-7-18.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class StatefulSetManagementTest {
    @Inject
    private StatefulSetManagement statefulSetManagement;

    @Inject
    private KubernetesClientManager clientManager;

    @Test
    public void testAddReplicas() {
        statefulSetManagement.addReplicas("cc-yzx-dev", "wordpress", "nginx", 2);
    }

    @Test
    public void testUpdate() {
        StatefulSet statefulSet = statefulSetManagement.getStatefulSet("cc-yzx-dev", "wordpress", "nginx");
        statefulSet.getMetadata().getAnnotations().put("description", "test update by json");
        statefulSet.getMetadata().getAnnotations().remove("annotations");
        statefulSetManagement.update("cc-yzx-dev", "wordpress", statefulSet);
    }

    @Test
    public void test(){
        boolean flag=clientManager.getClient().extensions().deployments().inNamespace("cc-yzx-dev").withName("test").delete();
        System.out.println(flag);
        Assert.assertFalse(flag);
    }

}
