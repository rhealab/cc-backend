package cc.backend.kubernetes;

import com.google.common.io.Resources;
import io.fabric8.kubernetes.api.model.HasMetadata;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.io.IOException;
import java.util.List;

/**
 * @author wangchunyang@gmail.com
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AllInOneTest {
    private static final Logger logger = LoggerFactory.getLogger(KubernetesClientManagerTest.class);

    @Inject
    private KubernetesClientManager clientManager;

    @Test
    @Ignore
    public void create() throws IOException {
        // DOESN'T WORK!!!
        String resourceName = "guestbook/all-in-one/guestbook-all-in-one.yaml";
        try (KubernetesClient client = clientManager.getClient()) {
            List<HasMetadata> result = client.load(Resources.getResource(resourceName).openStream())
                    .inNamespace("guestbook").createOrReplace();
            logger.debug(result.toString());
        }
    }
}
