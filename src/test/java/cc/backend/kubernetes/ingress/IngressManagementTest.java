package cc.backend.kubernetes.ingress;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by xzy on 18-3-29.
 */
public class IngressManagementTest {
  @Test
  public void testHostnameValidation() {
    IngressManagement management = new IngressManagement();
    Assert.assertTrue(management.validateHost("a.b.c.d"));
    Assert.assertTrue(management.validateHost("1.2.2.3"));
    Assert.assertTrue(management.validateHost("a1.b2.c3.d4"));
    Assert.assertTrue(management.validateHost("a"));
    Assert.assertTrue(management.validateHost("1a.b2.c3c.4d4"));
    Assert.assertFalse(management.validateHost("a.b.c."));
    Assert.assertFalse(management.validateHost("a#.b.c.d"));
    Assert.assertFalse(management.validateHost(""));
    Assert.assertFalse(management.validateHost(null));
  }
}
