package cc.backend.kubernetes.scheduling.services;

import cc.backend.kubernetes.scheduling.NodeSelector;
import cc.backend.kubernetes.scheduling.SchedulingValidator;
import cc.lib.retcode.CcException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by xzy on 17-9-21.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SchedulingServiceTest {
    private static final Logger logger = LoggerFactory.getLogger(SchedulingServiceTest.class);
    @Inject
    SchedulingValidator schedulingValidator;

    @Test(expected = CcException.class)
    public void testValidateLabelsConfig() {
        List<NodeSelector.KeyValue> list = new ArrayList<>();
        list.add(new NodeSelector.KeyValue("a", "b"));
        list.add(new NodeSelector.KeyValue("c", "b"));
        list.add(new NodeSelector.KeyValue("a", "b"));
        list.add(new NodeSelector.KeyValue("d", "b"));
        list.add(new NodeSelector.KeyValue("b", "b"));
        List<NodeSelector.KeyValue> expected = new ArrayList<>();
        expected.add(new NodeSelector.KeyValue("c", "b"));
        expected.add(new NodeSelector.KeyValue("a", "b"));
        expected.add(new NodeSelector.KeyValue("d", "b"));
        expected.add(new NodeSelector.KeyValue("b", "b"));
        Assert.assertArrayEquals(expected.toArray(), schedulingValidator.validateList(list).toArray());


        list.clear();
        list.add(new NodeSelector.KeyValue("a", "b"));
        list.add(new NodeSelector.KeyValue("a", "c"));
        list.add(new NodeSelector.KeyValue("b", "b"));
        schedulingValidator.validateList(list);
    }

    @Test
    public void testRemoveDuplicate() {
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("a");
        list.add("c");
        list.add("b");
        list.add("a");
        list.add("d");
        list.add("b");

        List<String> expected = new ArrayList<>();
        expected.add("a");
        expected.add("b");
        expected.add("c");
        expected.add("d");

        Assert.assertArrayEquals(expected.toArray(), schedulingValidator.removeDuplicate(list).toArray());
    }
}
