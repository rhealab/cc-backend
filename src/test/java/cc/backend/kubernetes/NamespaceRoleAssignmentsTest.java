package cc.backend.kubernetes;

import cc.backend.audit.request.EnnContext;
import cc.backend.kubernetes.namespaces.services.NamespaceRoleAssignmentManagement;
import cc.backend.role.assignment.RoleAssignmentDto;
import cc.backend.role.assignment.data.AssignmentExtrasRepository;
import cc.backend.user.data.User;
import cc.backend.user.data.UserRepository;
import cc.keystone.client.domain.AssignInfo;
import cc.keystone.client.domain.RoleType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yanzhixiang
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class NamespaceRoleAssignmentsTest {
    @Autowired
    NamespaceRoleAssignmentManagement management;

    Logger logger = LoggerFactory.getLogger(NamespaceRoleAssignmentsTest.class);

    @Test
    public void testUpdateNsUserRoles() {


        EnnContext.setUserId("yzx12");
        List<AssignInfo> updateList = new ArrayList<>();
        AssignInfo assignInfo1 = new AssignInfo();
        assignInfo1.setUserId("yzx12");
        assignInfo1.setRole(RoleType.DEVELOPER);

//        AssignInfo assignInfo2=new AssignInfo();
//        assignInfo2.setUserId("lbsheng");
//        assignInfo2.setRole(RoleType.DEVELOPER);
//
//        AssignInfo assignInfo3=new AssignInfo();
//        assignInfo3.setUserId("chiang");
//        assignInfo3.setRole(RoleType.NS_ADMIN);

        updateList.add(assignInfo1);
//        updateList.add(assignInfo2);
//        updateList.add(assignInfo3);

        logger.info(management.updateNsUserRoles("cxq9", updateList).toString());

    }

    @Inject
    private KubernetesClientManager kubernetesClientManager;

    @Inject
    private AssignmentExtrasRepository assignmentExtrasRepository;

    @Inject
    private UserRepository userRepository;

    @Test
    public void testCountBy() {
//        List<Pod> pods=kubernetesClientManager.getClient().pods().list().getItems();
//        for (Pod pod: pods){
//            logger.info(pod.toString());
//        }
//        if(pods!=null){
//            logger.info(String.valueOf(pods.size()));
//        }
//        logger.info(String.valueOf(assignmentInfoRepository.countByUserId()));
    }

    @Test
    public void testGetRoleAssigns(){
        List<String> userIds=new ArrayList<>();
        userIds.add("zhtangsh");
        userIds.add("yzx12");
        List<User> users=userRepository.findByUserIdIn(userIds);
        for(User user:users){
            logger.info("user {}",user);
        }

        List<RoleAssignmentDto> dtos=management.getNamespaceRoleAssignment("cc-zh-dev-test","zhtangsh");
        for (RoleAssignmentDto dto:dtos){
            logger.info(dto.toString());
        }
    }
}
