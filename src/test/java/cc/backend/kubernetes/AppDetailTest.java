package cc.backend.kubernetes;

import cc.backend.kubernetes.apps.dto.AppResourcesDetails;
import cc.backend.kubernetes.apps.services.AppManagement;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

/**
 * @author yanzhixiang
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AppDetailTest {
    @Inject
    AppManagement appManagement;

    @Test
    public void testAppDetail(){
        AppResourcesDetails details=appManagement.getResourcesDetails("cxq5","test-app");
        Logger logger= LoggerFactory.getLogger(AppDetailTest.class);
        logger.info(details.toString());
    }
}
