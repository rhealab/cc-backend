package cc.backend.kubernetes.utils;

import cc.backend.kubernetes.Constants;
import org.junit.Test;

import java.util.regex.Pattern;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author wangchunyang@gmail.com
 */
public class ConstantsTest {
    @Test
    public void namePattern() {
        assertTrue(Pattern.matches(Constants.RESOURCE_NAME_PATTERN, "my-abc"));
        assertTrue(Pattern.matches(Constants.RESOURCE_NAME_PATTERN, "my-123"));
        assertTrue(Pattern.matches(Constants.RESOURCE_NAME_PATTERN, "12-123"));
        assertTrue(Pattern.matches(Constants.RESOURCE_NAME_PATTERN, "123"));

        assertFalse(Pattern.matches(Constants.RESOURCE_NAME_PATTERN, "-abc"));
        assertFalse(Pattern.matches(Constants.RESOURCE_NAME_PATTERN, "abc-"));
        assertFalse(Pattern.matches(Constants.RESOURCE_NAME_PATTERN, "abc "));
        assertFalse(Pattern.matches(Constants.RESOURCE_NAME_PATTERN, "abc x"));
    }

}