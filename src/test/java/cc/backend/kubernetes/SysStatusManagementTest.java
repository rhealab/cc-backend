package cc.backend.kubernetes;

import cc.backend.sys.resource.service.ResourceCountManagement;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SysStatusManagementTest {
    @Autowired
    private ResourceCountManagement sysStatusManagement;

    private Logger logger = LoggerFactory.getLogger(SysStatusManagementTest.class);

    @Test
    public void testResourceCount() {
        logger.debug("System resource count={}", sysStatusManagement.getSystemResourceCount().toString());
    }
}
