package cc.backend.kubernetes;

import cc.backend.audit.request.EnnContext;
import cc.backend.kubernetes.workload.deployment.dto.DeploymentDto;
import cc.backend.kubernetes.workload.deployment.service.DeploymentsManagement;
import cc.backend.kubernetes.workload.pod.services.PodsManagement;
import io.fabric8.kubernetes.api.model.*;
import io.fabric8.kubernetes.api.model.extensions.Deployment;
import io.fabric8.kubernetes.api.model.extensions.DeploymentSpec;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yanzhxiang
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DeploymentExtrasTest {
    @Inject
    DeploymentsManagement deploymentsManagement;

    @Inject
    private PodsManagement podsManagement;

    Logger logger = LoggerFactory.getLogger(DeploymentExtrasTest.class);

    @Test
    public void testCreateDeployment() {
        EnnContext.setUserId("yzx12");
        Deployment deployment = new Deployment();
        ObjectMeta objectMeta = new ObjectMeta();
        objectMeta.setName("test1");
        deployment.setMetadata(objectMeta);

        DeploymentSpec deploymentSpec = new DeploymentSpec();
        deploymentSpec.setReplicas(2);

        PodTemplateSpec podTemplateSpec = new PodTemplateSpec();

        ObjectMeta podMeta = new ObjectMeta();
        Map<String, String> labels = new HashMap<String, String>();
        labels.put("app", "nginx");
        podMeta.setLabels(labels);

        podTemplateSpec.setMetadata(podMeta);

        PodSpec podSpec = new PodSpec();
        Container container = new Container();
        container.setName("nginx");
        container.setImage("index.tenxcloud.com/docker_library/nginx:1.7.9");

        List<ContainerPort> containerPorts = new ArrayList<>();
        ContainerPort containerPort = new ContainerPort();
        containerPort.setContainerPort(80);
        containerPorts.add(containerPort);
        container.setPorts(containerPorts);

        List<Container> containers = new ArrayList<>();
        containers.add(container);
        podSpec.setContainers(containers);

        podTemplateSpec.setSpec(podSpec);

        deploymentSpec.setTemplate(podTemplateSpec);
        deployment.setSpec(deploymentSpec);
        deploymentsManagement.create("cxq5", "test-app", deployment);
    }

    @Test
    public void testGetDeployment() {
        Deployment deployment = deploymentsManagement.getDeployment("cxq5", "test-app", "test");
        if (deployment != null) {
            List<Pod> podList = podsManagement.getPodsByDeployment("cxq5", "test-app", deployment.getMetadata().getName());
            logger.info(DeploymentDto.from(deployment, podList).toString());
        }
    }


    @Test
    public void testDeleteDeployment() {
        deploymentsManagement.deleteWithAudit("cxq5", "test-app", "test1");
    }
}
