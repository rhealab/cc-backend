package cc.backend.kubernetes;

import cc.backend.audit.request.EnnContext;
import cc.backend.kubernetes.namespaces.data.NamespaceExtras;
import cc.backend.kubernetes.namespaces.data.NamespaceExtrasRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

/**
 * Created by NormanWang06@gmail.com (jinxin) on 2017/6/22.
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class RepositoryTest {
    @Inject
    private NamespaceExtrasRepository namespaceExtrasRepository;

    @Test
    public void testPreAnnotation() {
        EnnContext.setUserId("jx");
        NamespaceExtras namespaceExtras = new NamespaceExtras();
        namespaceExtras.setName("test");
        namespaceExtrasRepository.save(namespaceExtras);
        NamespaceExtras test = namespaceExtrasRepository.findByNameAndDeletedIsFalse("test");
        assert test.getCreatedOn() != null;
        assert test.getCreatedBy() != null;
        assert test.getLastUpdatedOn() == null;
        assert test.getLastUpdatedBy() == null;

        namespaceExtras.setPoolBytes(100000);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        EnnContext.setUserId("jx2");
        namespaceExtrasRepository.save(namespaceExtras);
        test = namespaceExtrasRepository.findByNameAndDeletedIsFalse("test");
        assert test.getLastUpdatedOn() != null;
        assert test.getLastUpdatedBy() != null;
        assert !test.getCreatedBy().equals("jx2");

        namespaceExtrasRepository.delete(test.getId());
    }
}