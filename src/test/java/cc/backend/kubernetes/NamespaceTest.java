package cc.backend.kubernetes;

import cc.backend.kubernetes.namespaces.dto.NamespaceCreateRequest;
import cc.backend.kubernetes.namespaces.validator.NamespaceValidator;
import cc.backend.kubernetes.node.dto.NodeSumDto;
import cc.backend.kubernetes.node.services.NodesManagement;
import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.NamespaceBuilder;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

/**
 * @author wangchunyang@gmail.com
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class NamespaceTest {
    private static final Logger logger = LoggerFactory.getLogger(KubernetesClientManagerTest.class);

    @Inject
    private KubernetesClientManager clientManager;

    @Inject
    private NodesManagement nodesManagement;

    @Inject
    private NamespaceValidator namespaceValidator;

    @Test
    @Ignore
    public void createNamespace() {
        KubernetesClient client = clientManager.getClient();
        Namespace ns = new NamespaceBuilder().withNewMetadata().withName("guestbook").endMetadata().build();
        Namespace createdNs = client.namespaces().create(ns);
        logger.debug("Created namespace", createdNs);
    }

    @Test
    public void validateNamespaceForCreate() {
        NodeSumDto nodeSumDto = nodesManagement.getNodeSum();
        logger.info(nodeSumDto.toString());

        NamespaceCreateRequest namespaceCreateRequest = new NamespaceCreateRequest();
        namespaceCreateRequest.setName("test");
        namespaceCreateRequest.setCpuRequest(nodeSumDto.getCpuSum() - nodeSumDto.getCpuRequests());
        namespaceCreateRequest.setCpuLimit(nodeSumDto.getCpuSum() - nodeSumDto.getCpuRequests() + 2);
        namespaceCreateRequest.setMemoryRequestBytes(4 * 1024 * 1024);
        namespaceCreateRequest.setMemoryLimitBytes(4 * 1024 * 1024);

        logger.info("{}", namespaceCreateRequest.getHostPathStorageBytes());
    }
}
