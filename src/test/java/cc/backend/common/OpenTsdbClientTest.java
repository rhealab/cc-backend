package cc.backend.common;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.List;

/**
 * @author normanwang06@gmail.com (wangjinxin) on 2017/4/7.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class OpenTsdbClientTest {

    private static final Logger logger = LoggerFactory.getLogger(OpenTsdbClientTest.class);

    @Inject
    private OpenTsdbClient openTsdbClient;

    @Test
    public void getMetrics() {
        List<Metrics> metrics = openTsdbClient.getNamespaceMetrics(MetricsType.CPU, "cc-deployment");

        logger.info(metrics == null ? "null" : metrics.toString());
    }
}
