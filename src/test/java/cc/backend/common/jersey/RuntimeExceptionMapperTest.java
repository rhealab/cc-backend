package cc.backend.common.jersey;

import cc.backend.audit.operation.ResourceType;
import cc.backend.retcode.BackendReturnCodeNameConstants;
import cc.lib.retcode.CcException;
import cc.lib.retcode.ReturnCode;
import cc.lib.retcode.ReturnCodeManager;
import cc.lib.retcode.mapper.RuntimeExceptionMapper;
import io.fabric8.kubernetes.api.model.Status;
import io.fabric8.kubernetes.client.KubernetesClientException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import java.util.Map;

import static cc.backend.audit.operation.ResourceType.STORAGE;
import static com.google.common.collect.ImmutableMap.of;
import static javax.ws.rs.core.Response.Status.*;
import static org.junit.Assert.assertEquals;

/**
 * @author wangchunyang@gmail.com
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RuntimeExceptionMapperTest {
    @Inject
    private RuntimeExceptionMapper mapper;

    @Test
    public void testResourceNotFoundException() throws Exception {
        Response response = mapper.toResponse(
                new CcException(BackendReturnCodeNameConstants.STORAGE_WITH_NAME_NOT_EXISTS,
                        of("namespaceName", "ns1",
                                "resourceType", ResourceType.STORAGE,
                                "storageName", "s1")));
        ReturnCode code = ReturnCodeManager.getReturnCodeMap().get(BackendReturnCodeNameConstants.STORAGE_WITH_NAME_NOT_EXISTS);
        assertEquals(NOT_FOUND.getStatusCode(), response.getStatus());
        Map<String, Object> entity = (Map) response.getEntity();
        assertEquals(code.getCode(), entity.get("code"));
        assertEquals(code.getMessage(), entity.get("message"));
        Map payload = (Map) entity.get("payload");
        assertEquals(STORAGE, payload.get("resourceType"));
        assertEquals("ns1", payload.get("namespaceName"));
        assertEquals("s1", payload.get("storageName"));
    }

    @Test
    public void testBackendException() {
        Response response = mapper.toResponse(new CcException(BackendReturnCodeNameConstants.PROPERTY_INVALID, of("deploymentName", "$abc")));
        ReturnCode code = ReturnCodeManager.getReturnCodeMap().get(BackendReturnCodeNameConstants.PROPERTY_INVALID);
        assertEquals(BAD_REQUEST.getStatusCode(), response.getStatus());
        Map<String, Object> entity = (Map) response.getEntity();
        assertEquals(code.getCode(), entity.get("code"));
        assertEquals(code.getMessage(), entity.get("message"));
        Map payload = (Map) entity.get("payload");
        assertEquals("$abc", payload.get("deploymentName"));
    }

    @Test
    public void testWebApplicationException() {
        Response response = mapper.toResponse(new NotFoundException());
        assertEquals(NOT_FOUND.getStatusCode(), response.getStatus());
        Map<String, Object> entity = (Map) response.getEntity();
        Map payload = (Map) entity.get("payload");
        assertEquals("HTTP 404 Not Found", payload.get("details"));
    }

    @Test
    public void testKubernetesClientException_Conflict() {
        Response response = mapper.toResponse(new KubernetesClientException("msg", 409, new Status()));
        ReturnCode code = ReturnCodeManager.getReturnCodeMap().get(BackendReturnCodeNameConstants.KUBERNETES_RESOURCE_ALREADY_EXISTS);

        assertEquals(CONFLICT.getStatusCode(), response.getStatus());
        Map<String, Object> entity = (Map) response.getEntity();
        assertEquals(code.getCode(), entity.get("code"));
        assertEquals(code.getMessage(), entity.get("message"));
        Map payload = (Map) entity.get("payload");
        assertEquals("msg", payload.get("details"));
    }

    @Test
    public void testKubernetesClientException_GeneralError() {
        Response response = mapper.toResponse(new KubernetesClientException("msg", 405, new Status()));

        ReturnCode code = ReturnCodeManager.getReturnCodeMap().get(BackendReturnCodeNameConstants.KUBERNETES_SERVER_ERROR);

        assertEquals(METHOD_NOT_ALLOWED.getStatusCode(), response.getStatus());
        Map<String, Object> entity = (Map) response.getEntity();
        assertEquals(code.getCode(), entity.get("code"));
        assertEquals(code.getMessage(), entity.get("message"));
        Map payload = (Map) entity.get("payload");
        assertEquals("msg", payload.get("details"));
    }

    @Test
    public void testKubernetesClientException_422_not_included_in_jersey() {
        Response response = mapper.toResponse(new KubernetesClientException("msg", 422, new Status()));

        ReturnCode code = ReturnCodeManager.getReturnCodeMap().get(BackendReturnCodeNameConstants.KUBERNETES_SERVER_ERROR);

        assertEquals(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());
        Map<String, Object> entity = (Map) response.getEntity();
        assertEquals(code.getCode(), entity.get("code"));
        assertEquals(code.getMessage(), entity.get("message"));
        Map payload = (Map) entity.get("payload");
        assertEquals("msg", payload.get("details"));
    }

    @Test
    public void testOtherException() {
        Response response = mapper.toResponse(new RuntimeException("msg"));

        ReturnCode code = ReturnCodeManager.getReturnCodeMap().get(BackendReturnCodeNameConstants.SERVER_ERROR);

        assertEquals(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());
        Map<String, Object> entity = (Map) response.getEntity();
        assertEquals(code.getCode(), entity.get("code"));
        assertEquals(code.getMessage(), entity.get("message"));
        Map payload = (Map) entity.get("payload");
        assertEquals("msg", payload.get("details"));
    }
}