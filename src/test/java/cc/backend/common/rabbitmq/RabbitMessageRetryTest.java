package cc.backend.common.rabbitmq;

import cc.backend.RabbitConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;
import org.springframework.boot.autoconfigure.amqp.RabbitProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

/**
 * @author NormanWang06@gmail.com (jinxin) on 2017/5/12.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RabbitMessageRetryTest {
    public static Logger logger = LoggerFactory.getLogger(RabbitMessageRetryTest.class);

    @Inject
    private RabbitMessagingTemplate messagingTemplate;

    @Inject
    private RabbitProperties properties;

    private int counter;

    @Test
    public void send() throws InterruptedException {
        long initialInterval = properties.getListener().getRetry().getInitialInterval();
        double multiplier = properties.getListener().getRetry().getMultiplier();
        long maxInterval = properties.getListener().getRetry().getMaxInterval();
        int maxAttempts = properties.getListener().getRetry().getMaxAttempts();

        assert counter == 0;
        messagingTemplate.convertAndSend(RabbitConfig.REQUEST_AUDIT_Q, "abcd");
        Thread.sleep(10000);
//        Thread.sleep((long) (initialInterval * multiplier * (1 - Math.pow(multiplier, maxAttempts)) / (1 - multiplier) + 5000));
        assert counter == maxAttempts;
    }

    @RabbitListener(queues = RabbitConfig.REQUEST_AUDIT_Q)
    public void receive(String s) {
        logger.info("received message - " + s);
        counter++;
        throw new RuntimeException();
    }
}