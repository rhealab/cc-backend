package cc.backend.common.rabbitmq;

import cc.backend.RabbitConfig;
import cc.backend.kubernetes.namespaces.messages.NamespaceCreatedMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

/**
 * @author wangchunyang@gmail.com
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RabbitMessageProducerTest {

    @Inject
    private RabbitMessagingTemplate messagingTemplate;

    @Test
    public void send() throws InterruptedException {
        NamespaceCreatedMessage quota = new NamespaceCreatedMessage();
        /*quota.setName("ns1");
        quota.setLimitsCpu("4");
        quota.setRequestsCpu("1");
        quota.setRequestsMemory("100Mi");
        quota.setLimitsMemory("2Gi");
        quota.setRbdRequestsStorage("2Gi");
        quota.setHostPathRequestsStorage("100Mi");*/

        messagingTemplate.convertAndSend(RabbitConfig.NAMESPACE_CREATED_Q, quota);

        Thread.sleep(3000);
    }
}